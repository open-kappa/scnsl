@mainpage

SCNSL
=====

## Introduction

Welcome to SCNSL official documentation.
SCNSL is a SystemC Network Simulation Library, licensed under the LGPL license.
It is a library which extends SystemC with capabilities for performing simulations
of networked systems.

* Official sources site: https://gitlab.com/open-kappa/scnsl
* Official documentation: https://open-kappa.gitlab.io/scnsl

## User documentation

Please refer to the Related Pages section for some user-level documentation
The API reference is reported in the other usual Doxygen sections.

* [License](@ref page_license)
* [Copyright](@ref page_copyright)
* [HOWTO's](@ref page_howtos)
* [Other documentation](@ref page_other)

**WARNING**: Please note that the Latex manual inside the "doc" directory is outdated

## Contacts

Please report suggestions, request, bugs:

* Directly on GitLab
* Contacting Davide Quaglia davide.quaglia<AT>univr.it
* Contacting Francesco Stefanni francesco.stefanni.work<AT>gmail.com
