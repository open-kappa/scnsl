@page page_tests_description Brief tests description

Follows a brief explanation of some contained tests.

~~~~~~~~~~~~~{.txt}

**************************************************
*  test_unidirectional_tlm                       *
**************************************************


This is a basic example of communication between two nodes at TLM level.
The channel is unidirectional, so the communication takes place always in one way.
The communication starts from the TaskSender t1, which sends packets to TaskReceiver t2
until the simulation is stopped by the user.


**************************************************
*  test_unidirectional_rtl                       *
**************************************************

The same of test_unidirectional_tlm, but at RTL level.


**************************************************
*  test_fullduplex_tlm                           *
**************************************************


This is a basic example of communication between two nodes at TLM level.
The channel is fullduplex, so the communication takes place in both directions, without collisions.
The communication sending starts at the same time for task1 and task2, and it sends packets until the simulation is stopped by user.
Then task2 receive packets from task1 and viceversa.



**************************************************
*  test_fullduplex_rtl                           *
**************************************************


The same of test_fullduplex_tlm, but at RTL level.


**************************************************
*  test_halfduplex_tlm                           *
**************************************************

This is a basic example of communication between two nodes at TLM level.
The channel is halfduplex, so the communication takes place in both directions, but it senses collisions.
When a task sends a packet and there are one or more packets in the other direction,
all such packets collide, they are marked as broken, and the destination task will not receive them.
There are 4 types of scenario:

	1 - Task1 sends packets, while Task2 only receives them.

	2 - Task1 sends some packets and then, after 34ms, also Task2 starts sending 2 packets, so the starting packets will be received
		by Task2 but from 34ms there will be only collisions until Task2 stops the communication.

	3 - Task1 and Task2 are synchronized, so there can be only one packet on the channel.
		Task1 sends a packet and waits until Task2 has received it. Then the roles are inverted.
		When a task sends and receives a packet at the same time, the order of the method calls are SEND > RECEIVE

	4 - The same scenario of case 3, but the order of the method calls are RECEIVE > SEND



**************************************************
*  test_halfduplex_rtl                           *
**************************************************

The same of test_halfduplex_tlm, but at RTL level.

**************************************************
*  test_shared_tlm                               *
**************************************************

TLM basic test for shared channel.

	1 - Task0 sends packets, while Task1 only receives them.
	2 - Task0 sends packets. Task1 initially receives, then starts to send for a while, and finally receives only.
	3 - Task0 and Task1 are synchronized, so there can be only one packet on the channel. As a packet is received,
	    a "response" packet is sent.

**************************************************
*  test_802_15_4_tlm                             *
**************************************************

The same of test_shared_tlm, but with also the 802.15.4 protocol.

**************************************************
*  test_cbr                                      *
**************************************************

A basic example of communication between two nodes at TLM level.
The channel type is unidirectional. The sender task sends packets with constant bit-rate for 100ms, then it's disabled
for 200ms and re-enabled until the simulation is stopped by user.


**************************************************
*  test_on_off                                   *
**************************************************

A basic example of communication between two nodes at TLM level.
The channel type is unidirectional. The sender task sends packets with constant bit-rate during the on period,
while during the off period it's in pause.
As in the CBR test the sender task is enabled for 100ms, disabled for 200ms and re-enabled until the simulation is stopped
by user.


**************************************************
*  test_queue                                    *
**************************************************

The same as fullduplex test. At the sender side there is a queue communicator with two fifo queues between task proxy and node.
The packets must be enqueued in the sender queue before they reach the node for the send.
While for receive the packets must be enqueued in the receiver queue before they reach the task proxy.



**************************************************
*  test_shared_mobility_tlm                      *
**************************************************

TLM basic test for shared channel with nodes mobility.

    1 - Task0 sends two packets, while Task1 only receives them. Task1 initially is reachable, thus the first
        packet is received. After Task1 change its position and becomes unreachable, thus the second packet is
        not received.

    2 - Task0 sends packets, while Task1 only receives them. Initially Task1 is reachable, but during the
        encoding of the packet it goes out of the transmission range of Task0.

    3 - Task0 sends packets, while Task1 only receives them. Initially Task1 is not reachable, then it enters
        the transmission range of Task0. Task1 encodes only a part of the packet, thus the packet is corrupted.

    4 - Task0 sends packets, while Task1 only receives them. Initially Task1 is reachable, then it goes out and
        in the transmission range of Task0 during the encoding.


**************************************************
*  test_shared_mobility_out_in_out_tlm           *
**************************************************

TLM basic test for shared channel with nodes mobility.
In this test there are four Tasks. Task0 sends packets to Task2, while Task1 sends packets to Task3.
Initially the transmission ranges of Task0 and Task1 aren't intersected. Then Task0 and Task2 change their
positions so that the transmission ranges are intersected. Now all the packets received are collided. Afterwards
Task0 and Task2 return to their initial positions without collisions.


**************************************************
*  test_shared_mobility_hidden_exposed_tlm       *
**************************************************

This test show the problem of hidden node and exposed node whit node mobility in a shared channel.


**************************************************
*  test_delayedshared_tlm                        *
**************************************************

TLM basic test for delayed shared channel.

	1 - Task0 sends packets, while Task1 and Task2 only receive them.

	2 - Task0 sends packets. Task1 and Task2 initially receive, then start to send for a while, and finally receive only.


**************************************************
*  test_delayedshared_collision_tlm              *
**************************************************

This is a simple test to check collisions between four nodes at TLM level.
The type of channel is delayed shared and the propagation speed is 1480 m/s.
Task0 sends packet at zero time. Than Task3 sends a packet after 900 ms.
Due to difference distance from the sender tasks, Task1 receive all the packets while Task2 senses collision.


**************************************************
*  test_delayedshared_exposed_tlm                *
**************************************************

This is a simple test to show the exposed node problem.
The type of channel is delayed shared and the propagation speed is 1480 m/s.
Task0 sends packets at zero time. Task1 starts sending packets after 1 second.
Task2 and Task3 receive only.


**************************************************
*  test_delayedshared_hidden_tlm                 *
**************************************************

This is a simple test to show the hidden node problem.
The type of channel is delayed shared and the propagation speed is 1480 m/s.


**************************************************
*  test_delayedshared_mobility_tlm               *
**************************************************

TLM basic test for delayed shared channel with nodes mobility.

    1 - Task0 sends two packets, while Task1 only receives them. Task1 initially is reachable, thus the first
        packet is received. After Task1 change its position and becomes unreachable, thus the second packet is
        not received.

    2 - Task0 sends packets, while Task1 only receives them. Initially Task1 is reachable, but during the
        encoding of the packet it goes out of the transmission range of Task0.

    3 - Task0 sends packets, while Task1 only receives them. Initially Task1 is not reachable, then it enters
        the transmission range of Task0. Task1 encodes only a part of the packet, thus the packet is corrupted.

    4 - Task0 sends packets, while Task1 only receives them. Initially Task1 is reachable, then it goes out and
        in the transmission range of Task0 during the encoding.


**************************************************
*  test_delayedshared_mobility_out_in_out_tlm    *
**************************************************

TLM basic test for delayed shared channel with nodes mobility.
In this test there are four Tasks. Task0 sends packets to Task2, while Task1 sends packets to Task3.
Initially the transmission ranges of Task0 and Task1 aren't intersected. Then Task0 and Task2 change their
positions so that the transmission ranges are intersected. Now some packets are lost because there are:

    -  collisions when the receiver Tasks receive packets from Task0 and Task1;
    -  corruptions when the receiver Tasks enters in the transmission range of the sender Tasks;
    -  receiver Tasks that enter in transmission ranges when the packets has reached the maximum transmission range.

Afterwards Task0 and Task2 return to their initial positions.


*****************************************************
*  test_delayedshared_mobility_hidden_exposed_tlm   *
*****************************************************

This test show the problem of hidden node and exposed node whit node mobility in a delayed shared channel.


**************************************************
*  test_queue_priority                           *
**************************************************

The same as queue test. At the sender side there is a queue communicator with two priority queues between task proxy and node.
Each queue handles two different priority. The packets are enququed according to the priority specified in the label and dequeued
with strong priority algorithm. This algorithm empties the queue with the highest priority while it contains packets and then manages
lower priorities. The lower is the label the highest is the priority.
The policy for dequeue a packet is strong priority.


**************************************************
*  test_queue_priority_wrr                       *
**************************************************

The same as queue test priority, but the policy for dequeue a packet is weighted round robin.


**************************************************
*  test_pit                                      *
**************************************************

The same as unidirectional test. At the receiver side there is a pit task. This task is only responsible for receiving packets.


**************************************************
*  test_bottleneck                               *
**************************************************

This is the network configuration

                 T1 ---      --- T2
                      |      |
                      |      |
                      T5 --- T6
                      |      |
                      |      |
                 T3 ---      --- T4

T1 and T3 are the source tasks that generate packets. T1 generates packets as a CBR task with highest priority and sends them to T2, that is a pit task.
T3 generates packets as a ON_OFF task with low priority and sends them to T4, that is a pit task.
T5 and T6 are the central routers, with priority queues.
All the channels are fullduplex.


******************************************************
*  test_802_15_4_tlm_2_router                        *
******************************************************

We have a scenario with 2 nodes (with relative tasks) and 2 router. We have one shared channel:

n1 binded router 1 by ch
n2 binded router 2 by ch
router 1 binded router 2 by ch.

1)Task 1 sends packet to router 1 with destination task t2; while the simulation stop
2)Router 1 receive packet, look into it, looking for the right channel to forward the packet from routing table, and send it.
3)Router 2 same behavion of router 1
4)Task 2 receive packets.

In this folder there is "output.txt" that explain this functionality with an example.


******************************************************
*  test_802_15_4_tlm_router_2_task_on_node           *
******************************************************

We have a scenario with 3 nodes, n1, n2, n3. n1 have 2 task (t1 and t2), n2 have one task (t3) and n3 have one task (t4). We have one router with one shared channel, ch :

n1 binded router by ch
n2 binded router by ch
n3 binded router by ch

1)t1 and t2 sends packet to router with destination task respectively to t3 and t4
2)Router receive packet, look into it, looking for the right channel to forward the packet from routing table, and send it.
3)t3 and t4 receive packets


In this folder there is "output.txt" that explain this functionality with an example.



******************************************************
*  test_802_15_4_tlm_router_4_nodes                  *
******************************************************

We have a scenario with 4 nodes (with relative task t1, t2, t3, t4). We have one router with one shared channel:

n1 binded router by ch
n2 binded router by ch
n3 binded router by ch
n4 binded router by ch

1)t1 and t2 sends packet with destination node router and destination task respectively to t3 and t4
2)Router receive packet, look into it, looking for the right channel to forward the packet from routing table, and send it.
3)t3 and t4 receive packets

In this folder there is "output.txt" that explain this functionality with an example.



******************************************************
*  test_802_15_4_tlm_router_3_nodes                  *
******************************************************

We have a scenario with 2 nodes (with relative tasks t1, t2), one router and one shared channel:

n1 binded router by ch
n2 binded router by ch

1)t1 sends packets with destination node router and destination task t2
2)Router, receive packet, look into it, looking for the right channel to forward the packet from routing table, and send it.
3)t2 receive packets

In this folder there is "output.txt" that explain this functionality with an example.



******************************************************
*  test_802_15_4_tlm_router_3_nodes_task_on_router   *
******************************************************

We have a scenario with 2 nodes (with relative task t1, t2), one router with a task (tr) that sends packets to t2 and one shared channel:

n1 binded router by ch
n2 binded router by ch

1)t1 sends packets with destination node router and destination task t2
2)Router (through its task tr) sends packets to t2 and, when receive packet, look into it, looking for the right channel to forward the packet from routing table, and send it.
3)t2 receive packets.

In this folder there is "output.txt" that explain this functionality with an example.



******************************************************
*  test_fullduplex_tlm_2_router                      *
******************************************************

Same as test_802_15_4_tlm_2_router with more channels.



******************************************************
*  test_fullduplex_tlm_router_3_nodes                *
******************************************************

Same as test_802_15_4_tlm_3_nodes with more channels.



******************************************************
*  test_fullduplex_tlm_router_4_nodes                *
******************************************************

Same as test_802_15_4_tlm_4_nodes with more channels.

~~~~~~~~~~~~~
