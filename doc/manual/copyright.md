@page page_copyright Copyright and contributors

## Copyright

Copyright (C) 2008-2019
by Davide Quaglia and Francesco Stefanni.

SCNSL is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SCNSL is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SCNSL, in a file named LICENSE.txt.
If not, see http://www.gnu.org/licenses

## Contributors list

### Copyright owners

Copyright owners who are also the main contributors, and who have started  the
project:

  * Davide Quaglia
  * Francesco Stefanni

### Other contributors

* Initial debugging, wired channels, queues, traffic:
  * Giovanni Lorenzi
  * Luisa Repele
* Router model:
  * Mattia Carrarini
  * Daniele Marinello
* Wireless channels:
  * Giacomo Lanza
  * Eddy Marcolongo
* Documentation:
  * Luca Battisti
  * Emanuele Lucchi
* Some validation:
  * Andrea Poles
* Mesh topology:
  * Alessandro Danese
  * Francesco Martinelli
* Saboteur and first environment implementation:
  * Riccardo Reffato
* Saboteur and envoronment improvement:
  * Katia Maschi
  * Luca Piccolboni
* Identifier improvement
  * Alex Malfatti
* Directional antennas:
  * Marcello Tora
  * Riccardo Marin
  * Gabriele Miorandi
  * Luca Forte
* TCP implementation and OPC-UA integration
  * Elia Brentarolli

### Contributors to older SCNSL versions

* Francesco Alban
* Eddy Marcolongo
* Marco Soave
* Andrea Trentin

### Acknowledgements

We like to thanks the following people, institutions, groups, companies, for
their support to SCNSL:

* Ivan Minakov
* Jose' Edil Guimaraes de Medeiros
* EDALab
* ESD UNIVR Group
