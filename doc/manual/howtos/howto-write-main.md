@page page_howto_write_main HOWTO write a main

1. Create Nodes (N's).
1. Create Abstract Channels (AC's).
1. Associate N's and AC's, and init node properties.
1. Create Tasks (requires associated node).
1. Create Taskproxies (requires associated tasks and channel).
1. Set eventual events.
1. Run simulation (at start, implicitally check the correctness of the configuration).
