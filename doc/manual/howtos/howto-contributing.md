@page page_howto_contributing HOWTO Contribute to SCNSL

## Contributing to SCNSL


* Fork the project and perform the changes
* Ensure all original passing tests are still passing
* Add new tests to check your contributes
* Make a pull request
* Be ready to get feedbacks and perform some refinements before acceptance

## Copyright


* We do not require any formal copyright assignment or contributor license
  agreement
* Any contributions intentionally sent upstream are presumed to be offered
  under the terms of the LGPL license, with copyright assigned to SCNSL
  copyright holders
* The contributors names will be listed in the documentation
