@page page_howto_install HOWTO install SCNSL

## Easyest way

The easyest way to install SCNSL is to check the instructions stored in the
`scripts` directory. Please note that there is a very specific script, but feel
free to adapt it.

If somethiing more manual is preferred, please perform the following steps.

## Required libraries

In order to compile and use SCNSL, the followings are required:

- SystemC 2.3.0 or newer.
- CMake 3.10.2 or newer.
- A C++ compiler and a linker supporting a recent C++ standard.
- Doxygen (if it is desired to generate the documentation)

## HOWTO compile

### Preparing SystemC

SCNSL uses the SystemC library.
There are two ways to make SCNSL find the SystemC library.

The first and simpler chance is to download the SystemC sources from Accellera
(https://www.accellera.org/downloads/standards/systemc), and unpack them directly
into the third-parties/systemc sub-directory of SCNSL sources.
Please note that they must be set such that the CMakeLists.txt file of SystemC
will be directly inside the third-parties/systemc directory.

The second possibility is to download and compile the SystemC library,
and then to adjust the CMake variables such that :cmake:command:`find_package()`
will be able to locate the SystemC library (i.e. the SystemC build directory
or SystemC installed CMake module). For example:

 * By changing the :cmake:variable:`CMAKE_MODULE_PATH` at the beginning of SCNSL
   CMakeLists.txt file\
 * By passing to cmake '-DSystemCLanguage_DIR="<path to sysemc>"' option.

**WARNING**: In case of the second compling strategy, please note that SystemC
and SCNSL defaults to compile with a different C++ standard.
Please compile both of them with the same C++ standard!

 * Default for SystemC: 98 (older standard)
 * Default for SCNSL: 17 (newer standard)
 * For example:
 * cmake -DCMAKE_CXX_STARNDARD=17 <other params>

### Actual compiling


To compile the SCNSL library:

1) Create a directory, and go into it:

~~~~~~~~~~~~~{.txt}
> mkdir build
> cd build
~~~~~~~~~~~~~

2) Run CMake, with the path to this directory:

~~~~~~~~~~~~~{.txt}
> cmake ..
~~~~~~~~~~~~~

3) (OPTIONAL -- skip for usual install) Configure CMake:

~~~~~~~~~~~~~{.txt}
> ccmake ..
~~~~~~~~~~~~~

4) Compile using the environment system. For instance:

~~~~~~~~~~~~~{.txt}
> make
~~~~~~~~~~~~~

5) (OPTIONAL) Generate documentation. For instance:

~~~~~~~~~~~~~{.txt}
> make doc
~~~~~~~~~~~~~

6) (OPTIONAL) Generate tests/examples:

~~~~~~~~~~~~~{.txt}
> make scnsl-tests
~~~~~~~~~~~~~

7) Install the generated files:

~~~~~~~~~~~~~{.txt}
> make install
~~~~~~~~~~~~~

8) Run regression tsts

~~~~~~~~~~~~~{.txt}
> ctest
# or
> make test
~~~~~~~~~~~~~


Output library will be placed in to the lib or bin sub-directory.
Please remember to add to the LD_LIBRARY_PATH (or to equvalent var) environment
variable to the directory into which is located the SCNSL library in order to
run the examples by hand.
