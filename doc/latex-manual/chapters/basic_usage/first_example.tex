
\section{First example}
\label{chap:basic_usage:first_example_tlm}

For a basic usage, it is enough to assemble the different modules that make up a network scenario.
This is explained by commenting a test distributed with \scnsl{}, namely the \fileOrPath{first\_example\_tlm}.

\begin{figure}[!t]
\centering
\includegraphics[scale=0.5]{figures/first_example_tlm.eps}
\caption{The \fileOrPath{first\_example\_tlm} example.}
\label{fig:first_example_tlm}
\end{figure}

The scenario is depicted in Figure \ref{fig:first_example_tlm}.
there are two nodes, each one with a single task.
The first task will send random character as data to the second task.
The channel is a wired unidirectional model.
No transmission protocol is used.

The \lstinline{sc_main()} method is implemented in \fileOrPath{main.cc},
which is here reported and explained.
\begin{lstlisting}
// Including SCNSL library:
#include <scnsl.hh>

// Convenience using declarations:
using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace Scnsl::Tlm;

int sc_main( int argc, char * argv[] )
{
\end{lstlisting}

First of all, an instance of \scnsl{} simulator must be created.
The instance is a singleton.
\begin{lstlisting}[firstnumber=last]
Scnsl::Setup::Scnsl_t * sim =
    Scnsl::Setup::Scnsl_t::get_instance();
\end{lstlisting}
The instance provides the methods for creating the scenario components.
The two nodes are created:
\begin{lstlisting}[firstnumber=last]
//Nodes creation:
Scnsl::Core::Node_t * n1 = sim->createNode();
Scnsl::Core::Node_t * n2 = sim->createNode();
\end{lstlisting}
Node instances have been initialized with default parameters.
Instead, all other components factory methods require an arguments,
which is a struct containing the initialization parameters.
For example, the full duplex channel is created in this way:
\begin{lstlisting}[firstnumber=last]
// Struct of channel parameters:
CoreChannelSetup_t csb;

// Plugin providing the channel model:
csb.extensionId = "core";
// Channel type:
csb.channel_type = CoreChannelSetup_t::UNIDIRECTIONAL;
// Channel capacities, in bits per second:
csb.capacity = 10000;
// Channel transmission delay:
csb.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
// The name of the channel SystemC module instance:
csb.name = "channel_unidirectional";

Scnsl::Core::Channel_if_t* ch = sim->createChannel(csb);
\end{lstlisting}

Binding nodes to the channel.
\begin{lstlisting}[firstnumber=last]
// Struct of binding parameters:
BindSetup_base_t bsb1;

// The plugin providing the bind:
bsb1.extensionId = "core";

// Binding the first node:
sim->bind( n1, ch, bsb1 );

// Doing the same for the other node:
BindSetup_base_t bsb2;
bsb2.extensionId = "core";
sim->bind( n2, ch, bsb2 );
\end{lstlisting}

Creating the tasks, and binding them to their nodes.
The first task is a \ac{cbr}, and the second task is just a receiver.
\begin{lstlisting}[firstnumber=last]
CoreTaskSetup_t cts1;

// Plugin providing the task model:
cts1.extensionId = "core";
// Task type:
cts1.task_type = CoreTaskSetup_t::CBR;
// Task ID and node:
cts1.id = 1;
cts1.n = n1;
// Bytes of generated packet, and time generation:
(*@\label{code:formatterline}@*)cts1.pktSize = 2;
cts1.genTime = sc_core::sc_time(5, sc_core::SC_MS);
// The name of the task module:
cts1.name = "task1";

// Creation:
Scnsl::Core::Task_if_t * t1 = sim->createTask( cts1 );
// Activating the CBR traffic generator:
static_cast<Scnsl::Traffic::Traffic_if_t*>(t1)->enable();

// Creating the destination task:
CoreTaskSetup_t cts2;
cts2.extensionId = "core";
cts2.task_type = CoreTaskSetup_t::PIT;
cts2.id = 2;

// Binding the tasks:

BindSetup_base_t bsb_t1;
bsb_t1.extensionId = "core";
MyTask t1 ( "task1", 1, n1, 1 );
sim->bind( & t1, NULL, ch, bsb_t1, NULL );

BindSetup_base_t bsb_t2;
bsb_t2.extensionId = "core";
MyTask t2 ( "task2", 2, n2, 1 );
sim->bind( & t2, NULL, ch, bsb_t2, NULL );
\end{lstlisting}

Line \ref{code:formatterline} sets the packet size to 2 bytes,
since the transmitted data is a string, and thus, it is made by a random character,
plus the string terminating \lstinline{'\0'} character.
This assures that the transmitted payload can be safely printed.

\begin{figure*}[!b]
\notebox{No TaskProxy must be explicitly created: they are automatically created during task binds.}
\end{figure*}

To adding trace generation, a tracer object must be instantiated.
\begin{lstlisting}[firstnumber=last]
CoreTracingSetup_t cts;

// Plugin providing the tracer model:
cts.extensionId = "core";
(*@\label{code:formatterline}@*)cts.formatterExtensionId = "core";
(*@\label{code:filterline}@*)cts.filterExtensionId = "core";
cts.formatterName = "basic";
cts.filterName = "basic";

// Setting tracing to max verbosity:
cts.info = 5;
cts.debug = 5;
cts.log = 5;
cts.error = 5;
cts.warning = 5;
cts.fatal = 5;

// Creating the tracer:
Scnsl_t::Tracer_t * tracer = sim->createTracer( cts );
// Setting the output stream for the tracer:
tracer->addOutput( & std::cout );
\end{lstlisting}

Lines \ref{code:formatterline} and \ref{code:filterline} sets the plugin name for the type of output filter and formatter.
Briefely, the output collected by the tracer is filtered by a filter instance, and then passed to a formatter,
which prints the output. This can be useful to avoid to trace unuseful information, or to save in special formats
(for example, \ac{xml}). In this example, the default settings are used (no information is filtered out, the output format is
a plain print).

Finally, set SystemC simulation parameters:
\begin{lstlisting}[firstnumber=last]
sc_core::sc_start(sc_core::sc_time(100, sc_core::SC_MS));
sc_core::sc_stop();

return 0;
}
\end{lstlisting}
Compile and run the example.
The output should look like this:
\begin{verbatimshell}
             SystemC 2.2.0 --- Jun 29 2010 10:15:07
        Copyright (c) 1996-2006 by all Contributors
                    ALL RIGHTS RESERVED
0 s task1 data sent: I bytes: 2
0 s task1 src/tlm/TlmTask_if_t.cc:184 <> send().
0 s task1 <> send().
0 s task1 src/core/Task_if_t.cc:65 <> getNode().
0 s task1 src/core/Task_if_t.cc:65 <> getNode().
0 s task1 carrier: 1
0 s task2 carrier: 1
6600 us task1 data sent: L bytes: 2
6600 us task1 src/tlm/TlmTask_if_t.cc:184 <> send().
6600 us task1 <> send().
6600 us task1 src/core/Task_if_t.cc:65 <> getNode().
6600 us task1 src/core/Task_if_t.cc:65 <> getNode().
11600 us task2 data received: I bytes: 2 label: 0
...
\end{verbatimshell}
%% The data transmitted are randomly generated bytes with a fixed seed as specified in the \fileOrPath{MyTask.cc} file.
%% What the example actually does is to create a network consisting in two applications sending eachother packets of one byte as shown in Figure~\ref{fig:Figure 4}.
%% Both tasks are on different nodes and at ms 100 the simulation terminates as stated by the event \fileOrPath{sc\_start}. The figure shows the network simulated.
%% This example is very simple but contains the essential components for a simulation. In the next few sections it will be explained how to create your own simulation.

% EOF
