\section{Custom scenarios}
\label{chap:basic_usage:custom_scenarios}

In order to create a custom scenario it is necessary to instanciate traffic sources and destinations, nodes,
channels and all the several modules that compose the simulation. Anyway it is possible to reuse the classes written for other applications.
In general the steps to follow when creating a network scenario in \scnsl{} are:
\begin{enumerate}
\item Instanciate nodes.
\item Instanciate channels.
\item Bind nodes to channels, and set node's properties.
\item Instanciate tasks (e.g. traffic sources and dummy destinations).
\item Instantiate communicators (e.g. protocols and queues).
\item Bind tasks, communicators and channels.
\item Set events.
\item Set tracing features.
\end{enumerate}
The order of the above steps follows the development of the scenario, starting from the network deployment, and then moving the focus to the application.

This Section describes the main configuration parameters of \scnsl{} module, to allow users to quickly create custom scenarios.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Nodes}
\label{chap:basic_usage:custom_scenarios:nodes}

Nodes are the objects that state the whereabouts of tasks.
Nodes are \lstinline{Node_t} objects and do not have methods invocable by the user, since they are managed directly by the \scnsl{} simulator.

To instaciate a node it is enough to call the method \lstinline{createNode()} as shown below.
\begin{lstlisting}
Node_t * NODE_NAME = sim->createNode();
\end{lstlisting}

Nodes must be binded to channels, to create the network topology.
Binding parameters are passed to the method \lstinline{bind()} by using a \lstinline{BindSetup_base_t} struct.
The possible properties are:
\begin{itemize}
\item \lstinline{extensionId}: the plugin providing the node class. It is \lstinline{"core"}.
\item \lstinline{destinationNode}: the destination of the packets. Can be \lstinline{NULL}.
\item \lstinline{node_binding.bitrate}: the bitrate of the transmission.
\item \lstinline{node_binding.transmission_power}: an integer stating the power of the transimission in Watt.
\item \lstinline{node_binding.receiving_threshold}: an integer stating the receiving threshold in Watt, under this threshold the packet is lost.
\item \lstinline{node_binding.x}: physical position on the x axis.
\item \lstinline{node_binding.y}: physical position on the y axis.
\item \lstinline{node_binding.z}: physical position on the z axis.
\end{itemize}
Bitrate, transmission power, receiving threshold and physical position are relevant only for wireless channels.
For example:
\begin{lstlisting}[firstnumber=last]
BindSetup_base_t BIND_SETUP_NAME;

BIND_SETUP_NAME.extensionId = "core";
BIND_SETUP_NAME.destinationNode = DESTINATION_NAME;
(*@\label{code:protocolbitrate}@*)BIND_SETUP_NAME.node_binding.bitrate =
	Scnsl::Protocols::YOUR_PROTOCOL::BITRATE;
BIND_SETUP_NAME.node_binding.transmission_power = 100;
BIND_SETUP_NAME.node_binding.receiving_threshold = 10;
BIND_SETUP_NAME.node_binding.x = 1;
BIND_SETUP_NAME.node_binding.y = 1;
BIND_SETUP_NAME.node_binding.z = 1;
\end{lstlisting}
Notice that Line \ref{code:protocolbitrate} sets the bitrate to the one required by the used protocol.
This is useful to avoid to set up nonsense scenarios.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Channels}
\label{chap:basic_usage:custom_scenarios:channels}

Channels represent the physical media of the communication between the nodes.
The provided types of channels are:
\begin{itemize}
\item \lstinline{UnidirectionalChannel_t}: a wired unidirectional channel. It allows the bindon of only two nodes: a sender and a receiver.
\item \lstinline{FullDuplexChannel_t}: a wired full-duplex channel. It allows the binding of two nodes, and both can send and receive packets.
\item \lstinline{HalfDuplexChannel_t}: a wired half-duplex channel. It allows the binding of two nodes, and both can send and receive packets, but not at the same time.
\item \lstinline{SharedChannel_t}: a simple wireless channel. It allows to bind any number of nodes. The propagation delay is approximated by a constant.
\item \lstinline{DelayedSharedChannel_t}: a wireless channel, which compute the actual propagation delay. It allows to bind any number of nodes.
\end{itemize}
In order to instanciate a channel create a \lstinline{CoreChannelSetup_t} object,
set the properties, and finally pass it to the \lstinline{createChannel()} method.
Available properties are:
\begin{itemize}
\item \lstinline{name}: the name of the channel module.
\item \lstinline{extensionId}: the name of the plugin providing the channel model.
\item \lstinline{channel_type_t}: an enumerative defining the type of channel to be created.
\item \lstinline{capacity}: expresses the channel capacity in bits per second. Used by \lstinline{UnidirectionalChannel}, \lstinline{FullDuplexChannel}, \lstinline{HalfDuplexChannel}.
\item \lstinline{capacity2}: the channel capacity, for the second node. Used by \linebreak \lstinline{FulDuplexChannel}.
\item \lstinline{delay}: represents the propagation delay. Used by \lstinline{UnidirectionalChannel}, \lstinline{FullDuplexChannel}, \lstinline{HalfDuplexChannel}, \lstinline{SharedChannel}.
\item \lstinline{propagation}: an enumerative representing the speed of the propagation speed depending on the media. Used by \lstinline{DelayedSharedChannel}.
\item \lstinline{custom_propagation}: whether propagation is set to \lstinline{CUSTOM_SPEED}, the value here provided is used as propagation speed. Used by \linebreak \lstinline{DelayedSharedChannel}.
\item \lstinline{alpha}: the signal degradation on the channel (i.e. the attenuation exponent). Used by \lstinline{SharedChannel}, \lstinline{DelayedSharedChannel}.
\item \lstinline{nodes_number}: the number of nodes which will be connected to the channel. Used by \lstinline{SharedChannel}, \lstinline{DelayedSharedChannel}.
\end{itemize}
For example, to create a full duplex channel:
\begin{lstlisting}
CoreChannelSetup_t NAME_OF_SETUP;

NAME_OF_SETUP.name = "full_duplex_channel"
NAME_OF_SETUP.extensionId = "core";
NAME_OF_SETUP.channel_type(
              CoreChannelSetup_t::FULL_DUPLEX );
NAME_OF_SETUP.capacity = 1000;
NAME_OF_SETUP.capacity2 = 1000;
NAME_OF_SETUP.delay=
              sc_core::sc_time(1,sc_core::SC_MS);

Scnsl::Core::Channel_if_t * CHANNEL_NAME =
	      sim->createChannel( NAME_OF_SETUP );
\end{lstlisting}

Once the channel is created it is possible to bind nodes and channels, as described in Section \ref{chap:basic_usage:custom_scenarios:nodes}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Traffic sources and destinations}
\label{chap:basic_usage:custom_scenarios:traffic}

Traffic is generated from tasks. Traffic sources are those tasks that produce packets and send them.
Destinations are those tasks that receive packets.
Sources and destinations implement interfaces defined into the \lstinline{Traffic} package.

Provided classes are:
\begin{itemize}
\item \lstinline{Traffic_if_t}: generic interface for traffic sources and destinations. It is implemented by the other classes in the traffic package.
\item \lstinline{Cbr_t}: produces a \ac{cbr} traffic. It is the simpler traffic source.
\item \lstinline{OnOff_t}: a variation of \lstinline{Cbr_t} class. Alternates bursts of \ac{cbr} traffic with periods of silence.
\item \lstinline{PitTask_t}: a dummy task, which simply receives packets and discards them. \lstinline{PitTask}s are only destinations.
\end{itemize}

In order to create an istance of any of these tasks, it is necessary to create a \lstinline{CoreTaskSetup_t} object, to configure it, and finally to pass it as parameter to
the \lstinline{createTask()} method.

For example:
\begin{lstlisting}
CoreTaskSetup_t NAME_OF_SETUP;
\end{lstlisting}
Replace \lstinline{NAME_OF_SETUP} with a name of your choice.

Then, specify the type of task that shall be created, its name, its identificative number (an integer),
the \lstinline{extensionId} of the plugin to which it belongs (\lstinline{"core"} for the previous described task types) and the associated node.
\begin{lstlisting}[firstnumber=last]
NAME_OF_SETUP.task_type = CoreTaskSetup_t::TASK_TYPE;
NAME_OF_SETUP.name = "TASK_NAME";
NAME_OF_SETUP.id = TASK_ID;
NAME_OF_SETUP.n = NODE_NAME;
NAME_OF_SETUP.extensionId = "core";
\end{lstlisting}

Then it is possible to create a \lstinline{Task_if_t} object with the following line of code:
\begin{lstlisting}[firstnumber=last]
Task_if_t* REFERENCE_NAME=sim->createTask(NAME_OF_SETUP);
\end{lstlisting}


Then, the task must be connnected with another task, which will receive its packets, by using a specified channel and specified communicator (protocol).
This is performed by calling the \lstinline{bind()} method.
\begin{lstlisting}[firstnumber=last]
BindSetup_base_t BIND_SETUP;
BIND_SETUP.extensionId = "core";
sim->bind( REFERENCE_NAME, DESTINATION_TASK,
           CHANNEL_REFERENCE, BIND_SETUP,
           COMMUNICATOR_REFERENCE );
\end{lstlisting}
The destination task can be \lstinline{NULL} whether the packet is sent broadcast, or  the task is a \lstinline{PitTask} task.
The communicator can be \lstinline{NULL}, whether no protocol is used.
The \lstinline{TaskProxy} is implicitally created by the \lstinline{bind()} method.

Every type of task has some particular fields to set except the \lstinline{PitTask}, since it only receives.
These parameters are:
\begin{itemize}
\item \lstinline{pktSize} (\lstinline{scnsl::size_t}): the size in bytes of generated packet. Used by \lstinline{CbrTask}, \lstinline{OnOffTask}.
\item \lstinline{genTime} (\lstinline{sc_time}): the time to wait befor transmitting the next packet. Used by \lstinline{CbrTask}, \lstinline{OnOffTask}.
\item \lstinline{on} \& \lstinline{off} (\lstinline{sc_time}): used to set the active/inactive transmittion time. Used by \lstinline{OnOffTask}.
\end{itemize}

All the traffic sources have two methods that allow the user to stop and restart the flow of packets dynamically.
The two methods are:
\begin{itemize}
\item \lstinline{enable()}: allows the task to send packets.
\item \lstinline{disable()}: forbids the task to send packets.
\end{itemize}
When created, sources are disabled by default.
Thus, they should be turned on, as in this example:
\begin{lstlisting}[firstnumber=last]
static_cast<Scnsl::Traffic::Traffic_if_t*>(t1)->enable();
\end{lstlisting}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Communicators}
\label{chap:basic_usage:custom_scenarios:communicators}

Communicators represent the rules stating how the communication between the nodes takes place.
They have been introducted to give users the possibility to personalize the communication stack.
Communicators are not mandatory for a simulation to work, althrough they are commonly used.
For example, if users want to test a new protocol, no communicator will be used in the scenario,
and the protocol to be tested will be implemnented as a task.

The library includes some communicator implementations.
These objects are placed in an intermediate level between taskproxies and nodes as shown in Figure \ref{fig:architecture}.

In order to instanciate a communicator it is required to create a \linebreak \lstinline{CoreCommunicatorSetup_t} object and to call the \lstinline{createCommunicator()} method.
The properties depend on the kind of communicator that shall be instantiated.
Neverless, there are some propertoes used by all communicators:
\begin{itemize}
\item extensionId: the plugin that provides the desired communicator implementation. Usually it is \lstinline{"core"}.
\item name: the name of the communicator module.
\item type: the type of communicator to be created.
\end{itemize}

Each communicator implements the interface \lstinline{Communicator_if_t},
which provides some methods to bind communicators together,
and three methods to manage the communication:
\begin{itemize}
\item \lstinline{setCarrier()}: sets the carrier flag to show whether the channel is free. This information is propagated from the channel up to tasks.
\item \lstinline{send()}: sends the packet, i.e. forwards a packet in task-to-channel direction.
\item \lstinline{receive()}: receives the packet, i.e. forwards a packet in channel-to-task direction.
\end{itemize}
Once a communicator is created, it must be binded to tasks and channels, as shown in Section \ref{chap:basic_usage:custom_scenarios:traffic}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Communicator stacks}
\label{chap:basic_usage:custom_scenarios:communicators:stacks}

Communicators can be binded together, to create \emph{communicator chains}.
For example, this can be useful to bind together protocol layers and queues, as shown in Figure \ref{fig:communicator_chain}.
Each communicator chain must have a head, represented by a \lstinline{TaskProxy}, and a tail, represented by a \lstinline{Node}.

\begin{figure*}[!b]
\warningbox{A communicator chain must be wrapped by using a \lstinline{CommunicatorStack_t}.
Otherwise it cannot be binded to tasks.}
\end{figure*}

\begin{figure}[!t]
\centering
\includegraphics[height=8cm]{figures/communicator_chain.eps}
\caption{A communicator chain used to instantiate a TCP/IP stack.}
\label{fig:communicator_chain}
\end{figure}

Since the bind method accepts only a single communicator instance,
\scnsl{} provides a wrapper utility, to encompass all the communicator chain into a single communicator wrapper.
\begin{lstlisting}
Communicator_if_t * REFERENCE_NEW_COMMUNICATOR =
        new Scnsl::Utils::CommunicatorStack_t(
        COMMUNICATOR_CHAIN_HEAD, COMMUNICATOR_CHAIN_TAIL);
\end{lstlisting}

The result is also a child of \lstinline{Communicator_if_t}, and thus, it can be passed to the \lstinline{bind()} method.
Therefore, the \lstinline{CommunicatorStack_t} class also implements all the communicator interface methods.

The only public interesting new method is \lstinline{acquireOwnership()}, which is used to allow the cleanup of the communicator
chain, during wrapper destruction.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Protocols}
\label{chap:basic_usage:custom_scenarios:communicators:protocols}

\scnsl{} comes with an implementation of \ac{ieee} standard 802.15.4 \ac{mac} protocol.
It is a protocol for low-power low-range wireless networks.

Some specific properties are available:
\begin{itemize}
\item \lstinline{ack_required}: boolean specifying whether the protocol must be confirmed.
\item \lstinline{short_addresses}: boolean specifing whether to use short addresses.
\item \lstinline{node}: the node of the communicator.
\end{itemize}

To instantiate it, follow the steps shown in this code snippet:
\begin{lstlisting}
CoreCommunicatorSetup_t COMMUNICATOR_SETUP;

COMMUNICATOR_SETUP.extensionId = "core";
COMMUNICATOR_SETUP.name = "the_communicaotr_name";
COMMUNICATOR_SETUP.type =
	CoreCommunicatorSetup_t::MAC_802_15_4;
COMMUNICATOR_SETUP.node = NODE_OF_THE_COMMUNICATOR;

// Eventually set here other properties...

Communicator_if_t * REFERENCE_PROTOCOL_COMMUNICATOR =
    sim->createCommunicator( COMMUNICATOR_SETUP );
\end{lstlisting}

During the bind of nodes to wireless channels, it is required to set its bitrate to the bitrate of the protocol (250 Kbit/s).
For convenience, a constant is provided by the MAC 802.15.4 implementation:
\begin{lstlisting}
...
bindConfig.node_binding.bitrate =
     Scnsl::Protocols::Mac_802_15_4::BITRATE;
...
sim->bind( node, channel, bindConfig );
\end{lstlisting}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Queues}
\label{chap:basic_usage:custom_scenarios:communicators:queues}

Queues are used to manage the traffic load on the hosts.
\scnsl{} implements two queue types, \ac{fifo} and priority:
\begin{itemize}
\item \textbf{\ac{fifo}}: packets are managed according with their arriving order.
\item \textbf{priority}: packets are managed accoording with their priority. In \scnsl{} the
priority is named \emph{label}, which is a integer. The lower the value, the higher is the priority.
\end{itemize}

The \lstinline{CoreCommunicatorSetup_t} fields ued to configure a \lstinline{QueueCommunicator_t} object are:
\begin{itemize}
\item \lstinline{queueSend}: an enumerative stating the type of queue for the packets going from task to channel.
\item \lstinline{queueReceive}: an enumerative stating the type of queue for packets going from channel to task.
\item \lstinline{capacitySend}: an integer number stating the capacity of the buffer in bytes for outgoing packets.
\item \lstinline{capacityReceive}: an integer number stating the capacity of the buffer in bytes for incoming packets.
\item \lstinline{numberSend}: the number of sender queues to create. Used by priority queues.
\item \lstinline{numberReceive}: the number of receiver queues to create. Used by priority queues.
\item \lstinline{policySend}: an enumerative value stating the policy to use to get packets from the queue, in case of proprity queues.
\item \lstinline{policyReceive}: an enumerative value stating the policy to use to get packets from the queue, in case of proprity queues.
\item \lstinline{weightSend}: an integer stating the weight associated to each sender queue, in case of \lstinline{W_ROUND_ROBIN} policy.
\item \lstinline{weightReceive}: an integer stating the weight associated to each receiver queue, in case of \lstinline{W_ROUND_ROBIN} policy.
\end{itemize}

For the priority queue, a queue must be created for each used label.
To get a packet from a proproty queue, there are two available policies:
\begin{itemize}
\item \lstinline{STRONG_PRIORITY}: packets are extracted according with their priority.
\item \lstinline{W_ROUND_ROBIN}: packets are extracted by using a weighted round robin strategy.
This avoids the possible starvation of low priority packets.
\end{itemize}


An example of instanciation of a fifo queue for packets sent and received is the following:
\begin{lstlisting}
CoreCommunicatorSetup_t SETUP;

SETUP.extensionId = "core";
SETUP.name = "queue_communicator";
SETUP.type = CoreCommunicatorSetup_t::QUEUE;
SETUP.queueSend = CoreCommunicatorSetup_t::FIFO;
SETUP.queueReceive = CoreCommunicatorSetup_t::FIFO;
SETUP.capacitySend = 5;
SETUP.capacityReceive = 5;

Communicator_if_t * QUEUE_NAME =
	sim->createCommunicator( SETUP );
\end{lstlisting}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Routers}
\label{chap:basic_usage:custom_scenarios:communicators:routers}


In \scnsl{} routers are implemented as communicators.
The main difference from the instanciation perspective between a normal communicator and a router is that routers require a routing table,
which is specified as an field of the \lstinline{CoreCommunicatorSetup_t} class.

In order to create a routing table it's necessary to declare a \lstinline{std::map} object with \lstinline{Node_t *} as key, and a \lstinline{std::pair<Channel_if_t*, Node_t*>} as value.
Such a map must be filled with data known by the considered router, storing a pair must be stored for each known destination node.
The idea is that given a packet which must arrive to the \lstinline{KEY_NODE},
the router will forward it to the associated channel, setting as destination the associated node.
The field representing the routing table in the \lstinline{CoreCommunicatorSetup_t} is named \lstinline{routingTable}.

\begin{lstlisting}
CoreCommunicatorSetup_t SETUP;

SETUP.extensionId = "core";
SETUP.name = "router_communicator";
SETUP.type = CoreCommunicatorSetup_t::ROUTER;

std::pair< Scnsl::Core::Channel_if_t * ,
	Scnsl::Core::Node_t * >
	TABLE_LINE (CHANNEL, DESTINATION_NODE);
SETUP.routingTable[KEY_NODE] = TABLE_LINE;
...

Communicator_if_t * ROUTER_NAME =
	sim->createCommunicator( SETUP );

\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Topologies}
\label{chap:basic_usage:custom_scenarios:topologies}

Predefined topologies are a fast way to implement common network scenarios.
In order to instanciate such a scenario it is necessary to create a \lstinline{CoreTopologySetup\_t} object,
and then setting the type of topology.


\begin{lstlisting}
CoreTopologySetup_t TOPOLOGY;
TOPOLOGY.topology_type =
	CoreTopologySetup_t::BOTTLENECK;
TOPOLOGY.extensionId = "core";
\end{lstlisting}
At the moment the only type of topology available is the bottleneck, which implements the scenario shown in the Figure XX.XX.
\begin{figure}
\centering
\includegraphics[scale=0.3]{figures/old/bottleneck.eps}
\caption{The scenario implemented by the bottleneck topology.}
\label{fig:Figure 7}
\end{figure}
The bottleneck topology requires the instanciation of five channels and six tasks.
Instead of binding every channel separately to the nodes to use the predefined topology it is necessary to call the method \fileOrPath{pushback} on the \fileOrPath{CoreChannelSetup\_t} objects after setting their fields. This method puts a reference of the channel in the \fileOrPath{channel\_list} field of the \fileOrPath{CoreTopologySetup\_t} object. It is necessary to call it for every channel.
\begin{lstlisting}
TOPOLOGY_SETUP.channel_list.push_back(& CHANNEL_SETUP);
\end{lstlisting}
In order to add nodes instanciate one \fileOrPath{BindSetup\_base\_t} object for every node and put in the field \fileOrPath{bind\_list}.
\begin{lstlisting}
TOPOLOGY_SETUP.bind_list.push_back( & BIND_SETUP );
\end{lstlisting}
Once all setup objects have a reference in the \fileOrPath{CoreTopologySetup\_t} object it is possible to call the method \fileOrPath{createTopology}.
\begin{lstlisting}
sim->createTopology( cts );
\end{lstlisting}
This instanciates the nodes and the channels allowing the tasks to be binded. It is now possible to retrieve a list of channels and a list of nodes by calling the methods \fileOrPath{getNodes} and \fileOrPath{getChannels}.
\begin{lstlisting}
std::vector< Node_t * > NODE_LIST =
	sim->getNodes();
std::vector< Channel_if_t * > CHANNEL_LIST =
	sim->getChannels();
\end{lstlisting}
To set the nodes associated to the tasks and to bind them to the channels recall nodes and channels from the list appropriately where needed.
\begin{lstlisting}
sim->bind( TASK, NULL, CHANNEL_LIST[0],
		BIND_SETUP, NULL );
\end{lstlisting}


% EOF
