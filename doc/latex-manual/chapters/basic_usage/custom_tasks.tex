
\section{Creating custom tasks}
\label{chap:basic_usage:custom_tasks}

\scnsl{} can be used also to test network application.
In this case, users will provide the application code into a custom task.

For example, let consider to implement a simple \lstinline{"Hello world!"} application.
Custom tasks must derive from \lstinline{Task_if_t} interface, and in particular, from \lstinline{TLMTask_t} or \lstinline{RTLTask_if_t}.
In this example, the application will be child of \lstinline{TLMTask_if_t}.

In file \fileOrPath{Hello\_t.hh}:
\begin{lstlisting}
...
#include <systemc>
#include <scnsl.hh>

class Hello_t :
    public Scnsl::Tlm::TlmTask_if_t
{
...
SC_HAS_PROCESS( Hello_t );
...
/// @brief Constructor.
///
/// @param name This module name.
/// @param id This module unique ID.
/// @param n The node on which this task is placed.
/// @param proxies The number of connected task proxies.
/// @param is_sender Switches this task behavior.
/// @throw std::invalid_argument If proxies is zero.
///
Hello_t( sc_core::sc_module_name name,
       const task_id_t id,
       Scnsl::Core::Node_t * n,
       const size_t proxies,
       const bool is_sender )
throw ( std::invalid_argument );
...
// The standard TLM blocking transport, used to receiving:
virtual void b_transport( tlm::tlm_generic_payload & p,
                          sc_core::sc_time & t );
...
// The routine sending the message.
void sendingRoutine();
};
\end{lstlisting}
The constructor accepts a flag, \lstinline{is_sender}, which will be used by the application to have a sender or receiver behavior.
All the other constructor parameters are required by the parent.

The implementation of the application could be the following, in a file named \fileOrPath{Hello\_t.cc}:
\begin{lstlisting}
#include "Hello_t.hh"
...
Hello_t::Hello_t( sc_core::sc_module_name name,
                  const task_id_t id,
                  Scnsl::Core::Node_t * n,
                  const size_t proxies,
                  const bool is_sender )
throw ( std::invalid_argument ):
    Scnsl::Tlm::TlmTask_if_t( name, id, n, proxies )
{
    if ( is_sender )
    {
(*@\label{line:thread_requirement}@*)        SC_THREAD( sendingRoutine );
    }
}
\end{lstlisting}
The constructor is very simple. Line \ref{line:thread_requirement} points out an important requirement: routines used
to send packets \emph{must} be \lstinline{SC_THREAD}s.
In this example, the thread is created only for a sender task.

\begin{figure*}[!b]
\warningbox{All sending routines must be \lstinline{SC_THREAD}s.}
\end{figure*}

The implementation of the routine could be like this:
\begin{lstlisting}
void Hello_t::sendingRoutine()
{
    // Setting up the message:
    const char * msg = "Hello world!";
    const std::size_t length = strlen( msg ) + 1;

    // Setting up the taskproxy to be used:
    const Scnsl::Core::id_t taskproxy = 0;

    while( true )
    {
(*@\label{line:first_send_example}@*)         TlmTask_if_t::send(
               taskproxy,
               static_cast<Scnsl::Core::byte_t*>( msg ),
               length );

         wait( 10, sc_core::SC_MS );
    }
}
\end{lstlisting}

The taskproxy ID is used to choose the taskproxy to be used for the send, since in general many task proxies
can be attached to a single task. This example will have just one taskproxy.
Line \ref{line:first_send_example} shows an high level method, used to simplify sending process,
but designers can choose to directly call the \lstinline{b_transport()} on the associated taskproxy
(for \ac{tlm} tasks) or to write on output ports (for \ac{rtl} tasks).

Finally, the \lstinline{b_transport()} method is implemented.
It will be called by the associated taskproxy, when a packet must be received:
\begin{lstlisting}
void Hello_t::b_transport( tlm::tlm_generic_payload & p,
                           sc_core::sc_time & t )
{
    if( p.get_command() == Scnsl::Tlm::PACKET_COMMAND )
    {
        // Packet received!
        char * msg = static_cast< char * >(
             p.get_data_ptr() );
        std::cout << msg << std::endl;
    }
    else if(p.get_command()==Scnsl::Tlm::CARRIER_COMMAND)
    {
        // Channel status changed, but we do not care...
    }
    else
    {
        // Uh! Bug found! '^_^
        assert( false );
    }
}
\end{lstlisting}

As for Communicators, tasks can receive two kind of information: actual exchanged data,
and the carrier signal, which signals whether the channel is free.
Since this application is ``high level'', it is possible to ignore this kind of information.
Conversely, whether the application under design is a new low level transmission protocol,
this information becomes very important to implement channel access policies.

Finally, inside the \lstinline{sc_main()} it suffices to instantiate the custom task,
instead of the ones provided by \scnsl{}:
\begin{lstlisting}
...
#include "Hello_t.hh"
...
Hello_t sender( "Sender", 0, n1, true );
Hello_t receiver( "Receiver", 1, n2, false );
...
\end{lstlisting}



% EOF
