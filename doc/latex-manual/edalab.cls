%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EDAlab document class for manuals and reports.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% @author Francesco Stefanni
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic class stuff.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\EDALABCLASSNAME{edalab}
\def\EDALABDEFFLENAME{edalab}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{\EDALABCLASSNAME}[2011/05/20 EDAlab document class]
\ClassInfo{\EDALABCLASSNAME}{EDAlab document template. Author: Francesco Stefanni.}
\typeout{------------------------------}%
\typeout{ EDAlab Document Class}%
\typeout{ @author Francesco Stefanni}%
\typeout{ @version 1.2 Beta}%
\typeout{------------------------------}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loading basic support packages.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{ifthen}
\RequirePackage{xifthen}
\RequirePackage{kvoptions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Options.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\SetupKeyvalOptions{%
  family=edalab,
  prefix=edalab@
}

% book, whitepaper, monopage
\DeclareStringOption[book]{booktype}

% red, blue
\DeclareStringOption[blue]{icons}

\DeclareStringOption{pdftitle}

\DeclareBoolOption{draftmark}
\DeclareBoolOption{confidential}
\DeclareBoolOption{nofigurelist}
\DeclareBoolOption{notablelist}
\DeclareBoolOption{nocolors}
\DeclareBoolOption{logo}
% only on first page
\DeclareBoolOption{background}
\DeclareBoolOption{nobackground}

\DeclareDefaultOption{%
  \ClassError{\EDALABCLASSNAME}{Unknown option: `\CurrentOption'.}
}

\ProcessKeyvalOptions*

\ifthenelse{\equal{edalab@booktype}{whitepaper}}{%
  \typeout{------------------------------}%
  \typeout{\EDALABCLASSNAME: document type = whitepaper.}
  \typeout{------------------------------}%

  \PassOptionsToClass{oneside,openany}{book}
}{%
\ifthenelse{\equal{edalab@booktype}{monopage}}{%
    \typeout{------------------------------}%
    \typeout{\EDALABCLASSNAME: document type = monopage.}
    \typeout{------------------------------}%

    \PassOptionsToClass{oneside,openany}{book}

  }{%
    \typeout{------------------------------}%
    \typeout{\EDALABCLASSNAME: document type = book.}
    \typeout{------------------------------}%
  }%
}%



\newcommand{\edalab@getFile}[2]{%
  \IfFileExists{#2}{%
    \newcommand{#1}{#2}%
  }{%
    \ClassError{\EDALABCLASSNAME}{no file #2 found}{check to have installed correctly the package}
  }%
}%


% Figures:
\edalab@getFile{\edalab@logoFile}{edalab_figures/edalab_logo.eps}
\edalab@getFile{\edalab@backgroundFile}{edalab_figures/edalab_background.eps}

\ifthenelse{\equal{\edalab@icons}{blue}}{%
  \edalab@getFile{\edalab@warningFile}{edalab_figures/blue/warning.eps}
  \edalab@getFile{\edalab@noteFile}{edalab_figures/blue/note.eps}
  \edalab@getFile{\edalab@suggestionFile}{edalab_figures/blue/suggestion.eps}
}{%
  \ifthenelse{\equal{\edalab@icons}{red}}{%
    \edalab@getFile{\edalab@warningFile}{edalab_figures/red/warning.eps}
    \edalab@getFile{\edalab@noteFile}{edalab_figures/red/note.eps}
    \edalab@getFile{\edalab@suggestionFile}{edalab_figures/red/suggestion.eps}
  }{%
    \ClassError{\EDALABCLASSNAME}{Invalid value for option icon}{}%
  }%
}%

\ifedalab@draftmark
  \setcounter{edalab@printDraft}{1}%
\fi%


\ifedalab@confidential
  \AtEndOfClass{%
    \usepackage{draftwatermark}%
    \SetWatermarkText{\edalab@watermarkText}%
    \SetWatermarkFontSize{3.8cm}%
    }%
\fi


\newcommand{\edalab@BackgroundPic}{%
  \put(0,0){%
    \parbox[b][\paperheight]{\paperwidth}{%
      \vfill%
      \centering%
      \includegraphics[width=\paperwidth,height=\paperheight]{\edalab@backgroundFile}%
      \vfill%
}}}%


\ifedalab@background
    \AtBeginDocument{\AddToShipoutPicture{\edalab@BackgroundPic}}
\else
    \ifedalab@nobackground
    \else
        \AtBeginDocument{\AddToShipoutPicture*{\edalab@BackgroundPic}}%
    \fi
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loading support class and other packages.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\LoadClass[12pt,a4paper]{book}%

\RequirePackage[usenames,dvipsnames]{xcolor}
\RequirePackage{suffix}

\RequirePackage[T1]{fontenc}
\RequirePackage{ae}
\RequirePackage{aecompl}

\RequirePackage{fancyhdr}
\RequirePackage{lastpage}

% For bckg image
\RequirePackage{eso-pic}
\RequirePackage[final,bookmarks,hyperindex]{hyperref}
\RequirePackage[hypcap=true]{caption}
\RequirePackage{breakurl}
\RequirePackage{listings}
\RequirePackage{graphicx}
\RequirePackage{acronym}
\RequirePackage{fancyvrb}
\RequirePackage{fancybox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\AtBeginDocument{%
  % Redefining ref to allow text formatting:
  \let\edalab@ref\ref%
  \renewcommand{\ref}[1]{\underline{\edalab@ref{#1}}}%
  % Redefining pageref to allow text formatting:
  \let\edalab@pageref\pageref%
  \renewcommand{\pageref}[1]{\underline{\edalab@pageref{#1}}}%
}%

% Redefining href to allow text formatting:
\let\edalab@href\href%
\renewcommand{\href}[2]{\edalab@href{#1}{\underline{#2}}}%

\newcommand{\edalab@draft}{\textbf{DRAFT}}%

\newcommand{\edalab@watermarkText}{CONFIDENTIAL}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convenience macros.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\edalab@printBanner}{\noindent\emph{\EDAlab~s.r.l. -- Networked Embedded Systems}\\%
  Sede legale e operativa:\\%
  C\`a Vignal 2, Strada Le Grazie, 15 -- 37134 Verona -- Italy\\%
  C.F./P.IVA/Iscr. Reg. Imprese di Verona n. 03706250234\\%
  Numero REA: VR -- 358813\\%
  Capitale sociale: 10000 euro i.v.\\%
  Web: \href{http://www.edalab.it}{http://www.edalab.it}\\%
  Email: \href{mailto:info@edalab.it}{info@edalab.it}\\[1.5cm]%
}%

\newcommand{\edalab@printWhiteBanner}{\emph{\EDAlab~s.r.l. -- Networked Embedded Systems}\\%
C\`a Vignal, 2 - Strada Le Grazie, 15 -- 37134 Verona (VR) -- Italy\\
C.F./P.IVA/Iscr. Reg. Imprese di Verona n. 03706250234\\
Nr REA: VR 358813 -- Cap. Soc. 10000 euro i.v.\\
\href{http://www.edalab.it}{http://www.edalab.it}\\
\href{mailto:info@edalab.it}{info@edalab.it}\\[1.5cm]%
}%

\newcommand{\edalab@printCopyright}{%
  \emph{Copyright and disclaimer notes}\\%
  The contents of this document is property of \EDAlab~-- Networked Embedded Systems (\EDAlab)
  and it can be used only under explicit permission of \EDAlab. If copied or reproduced in any
  form (with permission), the source must be cited. Trade marks, product names and pictures
  of third parties may occur for description purposes and belong to their owners.\\[.5cm]
  \noindent\copyright~\the\year~\EDAlab~s.r.l. -- Networked Embedded Systems%
}

\newcommand{\edalab@printbox}[2] {%
  \begin{lrbox}{\@tempboxa}
    \begin{minipage}{\linewidth}%
    \framebox[\textwidth]{%
      \vtop{\null\hbox{\includegraphics[width=.1\linewidth,height=.1\linewidth]{#2}}}%
      \vtop{\null\hbox{\ }}%
      \vtop{\null\hbox{%
          \begin{minipage}[t]{.86\textwidth}%
            \small
            #1
        \end{minipage}%
        }}\hfill%
    }%
    \end{minipage}%
  \end{lrbox}%
  \vspace{6pt}\par\noindent\usebox{\@tempboxa}\par\vspace{6pt}%
}%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sections.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Just using till subsubsections.
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page style.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\fancypagestyle{plain} {%

  \fancyhf{}%

  % default: textheight = 592pt
  \setlength{\textheight}{.9\textheight}%

  \renewcommand\headrulewidth{.1pt}%
  \renewcommand\footrulewidth{.1pt}%
  \setlength{\headwidth}{\textwidth}%
  \setlength{\headheight}{2cm}%

  \fancyhead{}%
  \fancyfoot{}%

  \ifedalab@draftmark
    \fancyhead[LO]{\edalab@draf}
  \fi

  \ifedalab@logo
    \fancyhead[R]{%
      \href{http://www.edalab.it}{\includegraphics[height=.7\headheight]{\edalab@logoFile}}}%
  \fi


  \ifthenelse{\equal{\edalab@booktype}{whiepaper}}%
  {%
    \fancyfoot[L]{\EDAlab~s.r.l. -- \href{www.edalab.it}{www.edalab.it}}%
    \fancyfoot[R]{\thepage/\pageref{LastPage}}
    \fancyhead[C]{\edalab@title}%
  } {%
    \ifthenelse{\equal{\edalab@booktype}{monopage}}%
    {%
      \fancyfoot[C]{\EDAlab~s.r.l. -- \href{mailto:www.edalab.it}{www.edalab.it}}%
      \fancyhead[C]{\edalab@title}%
    }{%
      \fancyfoot[C]{\thepage/\pageref{LastPage}}%
      \fancyhead[C]{\nouppercase{\leftmark}}%
    }%
  }%
}%

\pagestyle{plain}

% Titlepage matter:
\newcounter{edalab@useLongTitle}
\setcounter{edalab@useLongTitle}{0}%
\newcommand{\edalab@longTitle}{}%

\renewcommand{\title}[1]{
  \newcommand{\edalab@title}{#1}%
  \ifthenelse{\value{edalab@useLongTitle} = 0}{\renewcommand{\edalab@longTitle}{#1}}{}%
}%

\newcommand{\longtitle}[1]{%
  \renewcommand{\edalab@longTitle}{#1}%
  \setcounter{edalab@useLongTitle}{1}%
}%

\newcommand{\subtitle}[1]{\newcommand{\edalab@subtitle}{#1}}

\newcommand{\version}[1]{\newcommand{\edalab@version}{#1}}

\ifthenelse{\equal{\edalab@booktype}{monopage}} {%
  % For monopage only.
  \renewcommand{\maketitle}{%
    \begin{center}%
      \href{http://www.edalab.it}{\includegraphics[width=.5\textwidth]{\edalab@logoFile}}\\[1cm]%%
      {\Huge \bfseries \edalab@longTitle}%
      {\ifcsname edalab@subtitle\endcsname%
        \\[.5cm]{\Large \bfseries \edalab@subtitle}%
        \fi%
      }\\[1.5cm]%
      \end{center}%
    \parindent 0pt
    \parskip 12pt
  }
}{%
  % Manual & whitepaper.
  \renewcommand{\maketitle}{%
    \begin{titlepage}%
      \begin{center}%
        \vspace{1.5cm}%
        \href{http://www.edalab.it}{\includegraphics[width=.9\textwidth]{\edalab@logoFile}}\\[1.5cm]%%
        {\Huge \bfseries \edalab@longTitle}\\[1cm]%
        {\ifcsname edalab@subtitle\endcsname%
          {\Large \bfseries \edalab@subtitle}%
          \fi%
        }%
        \vfill%
        \ifthenelse{\equal{\edalab@booktype}{whitepaper}}{%
          \edalab@printWhiteBanner}{}%
        {\large Version: \edalab@version\\Last revision: \today}%
      \end{center}%
    \end{titlepage}%
    \newpage%
        \ifthenelse{\equal{\edalab@booktype}{book}}{%
      \edalab@printBanner}{}%
    \edalab@printCopyright%
    %% \newpage%
    \tableofcontents%
    \ifedalab@nofigurelist%
    \else
    \listoffigures%
    \fi
    \ifedalab@notablelist
    \else
    \listoftables%
    \fi
  }%
}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Macros for formatting.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% To highlight concepts, keywords, new definitions.
\newcommand{\keyword}[1]{\textit{#1}}
% Used for code, text file content, file names and file paths.
\newcommand{\fileOrPath}[1]{\texttt{#1}}
% Used for application commands.
\newcommand{\applicationCommand}[1]{\textbf{#1}}
% Used for product elements (e.g. HIFSuite).
\newcommand{\product}[1]{\textsc{#1}}
% Used for example text (e.g., <your name>).
\newcommand{\example}[1]{<#1>}
% Used for menu commands (e.g., File > Open).
\newcommand{\menuCommand}{>}
% The square brackets are used for enclosing the
% name of a key on the keyboard (E.g. [F9]).
\newcommand{\keyboard}[1]{\texttt{[#1]}}
% Plus sign between two or more keys requires to
% press them simultaneously, e.g. [Shift]+[F9].
\newcommand{\plusKeyboard}{+}
% Prints a warning box:
\newcommand{\warningbox}[1]{\edalab@printbox{#1}{\edalab@warningFile}}%
% Prints a warning box:
\newcommand{\notebox}[1]{\edalab@printbox{#1}{\edalab@noteFile}}%
% Prints a warning box:
\newcommand{\suggestionbox}[1]{\edalab@printbox{#1}{\edalab@suggestionFile}}%
% Prints a prompt symbol:
\newcommand{\prompt}{\$}

% Prints EDALab in a nice way:
\newcommand{\EDAlab}{\textsc{EDAlab}}



\newcommand{\printBasicEditingConventions}{%
 \par All \EDAlab~documents follow these standard editing conventions:
  \begin{center}
    \begin{tabular}{|c|p{10cm}|}
      \hline
      \keyword{Italic} & To highlight concepts, keywords, new definitions.\\
      \hline
      \fileOrPath{Monospace} & Used for code, text file content, file names and file paths.\\
      \hline
      \underline{Underlined} & Used for references and links.\\
      \hline
      \applicationCommand{Bold} & Used for application commands.\\
      \hline
      \menuCommand & Used for menu commands (e.g., \applicationCommand{File}~\menuCommand~\applicationCommand{Open}).\\
      \hline
      \product{Smallcaps} & Used for product elements (e.g. \product{HIFSuite}).\\
      \hline
      \example{XXX} & Used for example text (e.g., \example{your name}).\\
      \hline
      \keyboard{} & The square brackets are used for enclosing the name of a key on the keyboard (E.g. \keyboard{F9}).\\
      \hline
      \plusKeyboard & Plus sign between two or more keys requires to press them simultaneously, e.g. \keyboard{Shift}\plusKeyboard\keyboard{F9}.\\
      \hline
      \vtop{\null\vspace{-9pt}\hbox{\includegraphics[width=1cm,height=1cm]{\edalab@warningFile}}\vspace{1pt}}
      & \vtop{\null\vspace{-12pt}\hbox{Used to alert for particularly dangerous issues.}}\\
      \hline
      \vtop{\null\vspace{-9pt}\hbox{\includegraphics[width=1cm,height=1cm]{\edalab@noteFile}}\vspace{1pt}}
      & \vtop{\null\vspace{-12pt}\hbox{Used to supply important information.}}\\
      \hline
      \vtop{\null\vspace{-9pt}\hbox{\includegraphics[width=1cm,height=1cm]{\edalab@suggestionFile}}\vspace{1pt}}
      & \vtop{\null\vspace{-12pt}\hbox{Used to provide hints and suggestions.}}\\
      \hline
    \end{tabular}
  \end{center}

Shell commands and outputs are displayed into a black boxes.
Commands are listed in enumerated lines.
\begin{shell}
\item ls\\
dir1 dir2 dir3
\item rm dir2
\item ls\\
dir1 dir3
\end{shell}%

Code examples are listed into gray boxes, with highlighting and numbering.

\begin{lstlisting}^^J%
// This method returns 0.^^J%
int method1()^^J
\{^^J
\ \ \ \ return 0;^^J
\}^^J
^^J
int a = method1();^^J
\end{lstlisting}%
}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configuring support packages.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\hypersetup{
  %pdftitle={\edalab@title},     % title
  %% pdfauthor={},                 % author
  %% pdfsubject={}, % subject of the document
  %% pdfcreator={},                           % creator of the document
  %% pdfproducer={},                          % producer of the document
  %% pdfkeywords={},        % list of keywords
  %
  %    bookmarks=true,         % show bookmarks bar?
  %% hyperindex=true,        %
  pdfdisplaydoctitle=true,% whow title instead of file name
  unicode=false,          % non-Latin characters in Acrobats bookmarks
  pdftoolbar=true,        % show Acrobats toolbar?
  pdfmenubar=true,        % show Acrobats menu?
  pdffitwindow=false,     % window fit to page when opened
  pdfstartview={Fit},    % FitH fits the width of the page to the window, Fit fits the page, FitV
  pdfnewwindow=true,      % links in new window
  colorlinks=false,       % false: boxed links; true: colored links
  linkcolor=black,        % color of internal links
  anchorcolor=black,        % color of anchor
  citecolor=black,        % color of links to bibliography
  filecolor=black,      % color of file links
  menucolor=black,
  runcolor=black,
  urlcolor=black,         % color of external links
  %% allcolors=black,
  %%allbordercolors=black,
  breaklinks=true,         % links can span over multiple lines
  %% pdfborderstyle={/S/U/W 1}, % underline
  pdfborder={0 0 0}, % underline
  citebordercolor=black,
  filebordercolor=black,
  linkbordercolor=black,
  menubordercolor=black,
  urlbordercolor=black,
  runbordercolor=black
  %% hidelinks
  %pdfpagemode=FullScreen
}


\ifedalab@nocolors
\lstset{
  language=C++,
  tabsize=4,
  basicstyle=\ttfamily\color{black}\small,
  backgroundcolor=\color{white},
  keywordstyle=\bfseries,
  identifierstyle=\itshape,
  commentstyle=\sffamily,
  stringstyle=\scshape,
  showstringspaces=false,
  showtabs=false,
  showspaces=false,
  numbers=left,                   % where to put the line-numbers
  numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line
                                % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
%  frameround=tttt,
  frame=lines,                   % bottomline, topline, lines, single...
  escapeinside={(*@}{@*)}
}
\else
\lstset{
  language=C++,
  tabsize=4,
  basicstyle=\ttfamily\color{black}\small,
  backgroundcolor=\color{lightgray!30!white},
  keywordstyle=\color{Purple},
  identifierstyle=\color{OliveGreen},
  commentstyle=\color{darkgray}\itshape,
  stringstyle=\color{CarnationPink},
  showstringspaces=false,
  showtabs=false,
  showspaces=false,
  numbers=left,                   % where to put the line-numbers
  numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line
                                % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
%  frameround=tttt,
  frame=lines,                   % bottomline, topline, lines, single...
  escapeinside={(*@}{@*)}
}
\fi

% An environment for bash:
%% \newenvironment{bash}{%
%%    \begin{lrbox}{\@tempboxa}\begin{minipage}{\textwidth}\color{white}\ttfamily\footnotesize%
%% }{\end{minipage}\end{lrbox}\vspace{6pt}\par\noindent\colorbox{black}{\usebox{\@tempboxa}\par}\vspace{6pt}%
%% }

\newsavebox{\@tempshellbox}
\newcounter{shellcounter}

\newenvironment{shell}[1][.97\linewidth]
{
  \begin{lrbox}{\@tempshellbox}%
  \begin{minipage}{#1}%
    \begin{ttfamily}
      \small
      \color{white}
      \begin{list}
        {
          \arabic{shellcounter}\prompt%
        }
        {
          \usecounter{shellcounter}
          %% \setstretch{0.8}
          \setlength\itemindent{0ex}
          \setlength\listparindent{0ex}
          \setlength\parsep{0ex}
          \setlength\parskip{0cm}
          \setlength\itemsep{0ex}
          \setlength\topsep{0ex}
        }
}
{
    \end{list}
    \end{ttfamily}
  \end{minipage}%
  \end{lrbox}%
  \vspace{6pt}%
  \par\noindent\colorbox{black}{%
    \usebox{\@tempshellbox}%
  }\par%
  \vspace{6pt}%
}



%% \newenvironment{verbatimshell}[1][.97\linewidth]
%% {
%%   \color{white}
%%   \ttfamily
%%   \verbatim
%% }{
%%   \endverbatim
%% }

%% \def\verbatimshell{%
%%   \color{white}\small
%%   \def\verbatim@processline{%
%%     {\setbox0=\hbox{\the\verbatim@line}%
%%     \hsize=\wd0 \the\verbatim@line\par}}%
%%   \@minipagetrue
%%   \@tempswatrue
%%   \@totalleftmargin\z@
%%   \setbox0=\vbox\bgroup \verbatim
%% }
%% \def\endverbatimshell{%
%%   \endverbatim
%%   \unskip\setbox0=\lastbox
%%   \egroup
%%   \vspace{6pt}%
%%   \par\noindent
%%   \colorbox{black}{\box0}%
%%   \par%
%%   \vspace{6pt}%
%% }


%% \newenvironment{verbatimshell}%
%% {%
%% \definecolor{shadecolor}{named}{black}%
%% % supress ugly vertical space which is inserted by
%% % environment above and below text:
%% \topsep=0ex\relax
%% % start shaded and verbatim:
%% \shaded \verbatim \color{white}
%% }%
%% {%
%% \endverbatim
%% \endshaded
%% }%*

\newenvironment{verbatimshell}{\VerbatimEnvironment%
  \noindent
  %      {\columnwidth-\leftmargin-\rightmargin-2\fboxsep-2\fboxrule-4pt}
  \begin{Sbox}
  \begin{minipage}{\linewidth-2\fboxsep-2\fboxrule-4pt}
    \color{white}
  \begin{Verbatim}
}{%
  \end{Verbatim}
  \end{minipage}
  \end{Sbox}
  \vspace{6pt}%
  \par\noindent
  \colorbox{black}{\TheSbox}
  \par%
  \vspace{6pt}%
}



% EOF
