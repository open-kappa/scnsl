# SCNSL

Welcome to SCNSL repo.

SCNSL is a SystemC Network Simulation Library, licensed under the LGPL license.
It is a library which extends SystemC with capabilities for performing simulations
of networked systems.

## License

**SCNSL**

Copyright (C) 2008-2019 by Davide Quaglia and Francesco Stefanni.

SCNSL is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SCNSL is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SCNSL, in a file named LICENSE.txt.
If not, see <http://www.gnu.org/licenses/>.

## Documentation

Please see the official SCNSL documentation for quick-starts, guides,
references, etc.

https://open-kappa.gitlab.io/scnsl

For an easy installation, please check the `scripts` directory.

# Contacts

Please report suggestions, request, bugs:

* Directly on GitLab
* Contacting Davide Quaglia davide.quaglia<AT>univr.it
* Contacting Francesco Stefanni francesco.stefanni.work<AT>gmail.com
