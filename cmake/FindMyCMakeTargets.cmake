# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeTargets
------------------

Module to simplify the targets management.

The variables here contained can be overridden by command line, or into the
CMakeLists.txt before including *any* of MyCMake modules.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeTargets_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeTargets_VERSION

    This module version

Standard CMake variables
^^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: BUILD_SHARED_LIBS

    ON as default

.. cmake:variable:: CMAKE_C_EXTENSIONS

    Turns OFF compiler-specific extensions

.. cmake:variable:: CMAKE_CXX_EXTENSIONS

    Turns OFF compiler-specific extensions

.. cmake:variable:: CMAKE_C_STANDARD

    Set to the latest standard

.. cmake:variable:: CMAKE_CXX_STANDARD

    Set to the latest standard

.. cmake:variable:: CMAKE_C_VISIBILITY_PRESET

    "hidden" to force explicit public symbols visibility

.. cmake:variable:: CMAKE_CXX_VISIBILITY_PRESET

    "hidden" to force explicit public symbols visibility

.. cmake:variable:: CMAKE_INCLUDE_CURRENT_DIR

    ON to consider current dir as include path

.. cmake:variable:: CMAKE_LINK_WHAT_YOU_USE

    ON to force minimal linking

.. cmake:variable:: CMAKE_POSITION_INDEPENDENT_CODE

    ON to simplify binaries management. It adds -fPIC, since under Linux it is
    required when a static lib is linked with a shared lib

.. cmake:variable:: CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS

    OFF to manage symbols visibility explicitly

Specific MyCMake behavior variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY

    ON to force "default" visiility of exe symbols.
    OFF (default) to fallback to global project settings.

Definitions passed to compilers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_OS_BITWIDTH

    The bitwidth, as a number

.. cmake:variable:: MYCMAKE_OS_IS_BIGENDIAN

    The endianesss

.. cmake:variable:: MYCMAKE_PROJECT_VERSION

    String of the version

.. cmake:variable:: MYCMAKE_PROJECT_VERSION_MAJOR

    The major version number

.. cmake:variable:: MYCMAKE_PROJECT_VERSION_MINOR

    The minor version number

.. cmake:variable:: MYCMAKE_PROJECT_VERSION_PATCH

    The patch version number

.. cmake:variable:: MYCMAKE_PROJECT_VERSION_TWEAK

    The tweak version number


Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_add_additional_files()`
- :cmake:command:`mycmake_add_custom_target()`
- :cmake:command:`mycmake_add_executable()`
- :cmake:command:`mycmake_add_fake_target()`
- :cmake:command:`mycmake_add_library()`
- :cmake:command:`mycmake_add_rc_file()`
- :cmake:command:`mycmake_export()`
- :cmake:command:`mycmake_external_project()`
- :cmake:command:`mycmake_get_interface_link_libraries()`
- :cmake:command:`mycmake_install()`
- :cmake:command:`mycmake_target_compile_definitions()`
- :cmake:command:`mycmake_target_include_directories()`
- :cmake:command:`mycmake_target_link_libraries()`

#]=======================================================================]
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)

set(MyCMakeTargets_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeTargets
    FOUND_VAR
        MyCMakeTargets_FOUND
    REQUIRED_VARS
        MyCMakeBase_FOUND
        MyCMakeTargets_VERSION
    VERSION_VAR
        MyCMakeTargets_VERSION
)

if (MyCMakeTargets_FOUND)
    mycmake_include(GNUInstallDirs)
    mycmake_include(ExternalProject)
    mycmake_include(GenerateExportHeader)
    mycmake_include(CMakePackageConfigHelpers)

    mycmake_set_default_policies()
endif (MyCMakeTargets_FOUND)

# Base settings:
mycmake_try_option(BUILD_SHARED_LIBS "Build libraries as shared as opposed to static" ON MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_C_EXTENSIONS "C standard extensions" OFF MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_CXX_EXTENSIONS "C++ standard extensions" OFF  MYCMAKE_ADVANCED)
mycmake_try_set(CMAKE_C_STANDARD "11" CACHE STRING "C standard" MYCMAKE_ADVANCED)
mycmake_try_set(CMAKE_CXX_STANDARD "17" CACHE STRING "C++ standard" MYCMAKE_ADVANCED)
mycmake_try_set(CMAKE_C_VISIBILITY_PRESET "hidden" CACHE STRING "C default visibility policy" MYCMAKE_ADVANCED)
mycmake_try_set(CMAKE_CXX_VISIBILITY_PRESET "hidden" CACHE STRING "C++ default visibility policy" MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_INCLUDE_CURRENT_DIR "Default value for LINK_WHAT_YOU_USE" ON MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_LINK_WHAT_YOU_USE "Default value for LINK_WHAT_YOU_USE" ON MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_POSITION_INDEPENDENT_CODE "Creates PIC code" ON MYCMAKE_ADVANCED)
mycmake_try_option(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS "Disable dllexport need" OFF MYCMAKE_ADVANCED)
mark_as_advanced(CMAKE_C_VISIBILITY CMAKE_CXX_VISIBILITY)

# Specific MyCMake behavior variables
mycmake_try_option(MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY "Force default visibilityfor executables" OFF MYCMAKE_ADVANCED)

# Pre-defined macro settings:
add_definitions(
    -DMYCMAKE_OS_BITWIDTH=${MYCMAKE_OS_BITWIDTH}
    -DMYCMAKE_OS_IS_BIGENDIAN=${MYCMAKE_OS_IS_BIGENDIAN}
    -DMYCMAKE_PROJECT_VERSION="${PROJECT_VERSION}"
    -DMYCMAKE_PROJECT_VERSION_MAJOR=${PROJECT_VERSION_MAJOR}
    -DMYCMAKE_PROJECT_VERSION_MINOR=${PROJECT_VERSION_MINOR}
    -DMYCMAKE_PROJECT_VERSION_PATCH=${PROJECT_VERSION_PATCH}
    -DMYCMAKE_PROJECT_VERSION_TWEAK=${PROJECT_VERSION_TWEAK}
    )

function(_mycmake_get_single_interface_link_libraries OUT NAME)
    if (TARGET ${NAME})
        get_target_property(TGT_TYPE ${NAME} TYPE)
        if ((NOT ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY"))
            AND (NOT ("${TGT_TYPE}" STREQUAL "EXECUTABLE")))
            return()
        endif ((NOT ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY"))
            AND (NOT ("${TGT_TYPE}" STREQUAL "EXECUTABLE")))
        get_target_property(LIB_LIST ${NAME} INTERFACE_LINK_LIBRARIES)
        set(${OUT} ${LIB_LIST} PARENT_SCOPE)
    endif (TARGET ${NAME})
endfunction(_mycmake_get_single_interface_link_libraries)


## Internal support function to add linked targets include paths
## @param NAME The target name
## @param ARGN The list of linked libs
function(_mycmake_targets_add_includes NAME)
    if (NOT ARGN)
        return()
    endif (NOT ARGN)
    foreach(L ${ARGN})
        if (TARGET ${L})
            get_target_property(INCS ${L} INTERFACE_INCLUDE_DIRECTORIES)
            if (INCS)
                target_include_directories(
                        ${NAME}
                    PUBLIC
                        ${INCS}
                    )
            endif (INCS)
        elseif (TARGET ${L}${MYCMAKE_STATIC_SUFFIX})
            get_target_property(INCS ${L}${MYCMAKE_STATIC_SUFFIX} INTERFACE_INCLUDE_DIRECTORIES)
            if (INCS)
                target_include_directories(
                        ${NAME}
                    PUBLIC
                        ${INCS}
                    )
            endif (INCS)
    endif (TARGET ${L})
    endforeach(L ${LIBS})
endfunction(_mycmake_targets_add_includes)

function(_mycmake_target_include_directories NAME)
    cmake_parse_arguments(options "SYSTEM" "" "" ${ARGN})

    if ("${ARGN}" STREQUAL "")
        target_include_directories(${NAME}
            PUBLIC
                $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/${CMAKE_INSTALL_INCLUDEDIR}>
                $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
            PRIVATE
                src
            )
    else ("${ARGN}" STREQUAL "")
        # Reminder: ARGN must contain PUBLIC, PRIVATE, SYSTEM...
        target_include_directories(${NAME}
            ${ARGN}
            )

        # At the moment (VC++2019, CMake 3.10.2), VC++ supports "SYSTEM" headers
        # as experimental feature. CMake does not consider this..
        if (options_SYSTEM AND MYCMAKE_COMPILER_IS_MSVC)
            get_target_property(SYSTEM_INC_DIRS ${NAME} INTERFACE_SYSTEM_INCLUDE_DIRECTORIES)
            set(LIST)
            foreach(INC ${SYSTEM_INC_DIRS})
                set(LIST ${LIST} /external:I ${INC})
            endforeach(INC ${SYSTEM_INC_DIRS})
            target_compile_options(${NAME}
                BEFORE
                INTERFACE
                    /experimental:external
                    ${LIST}
                    /external:W0
                )
            set_target_properties(${NAME} PROPERTIES INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "")
        endif (options_SYSTEM AND MYCMAKE_COMPILER_IS_MSVC)
    endif ("${ARGN}" STREQUAL "")
endfunction(_mycmake_target_include_directories)

## Private function to add export .h file to install targets.
## @param NAME The target name
## @param TGT_TYPE The target type
## @param COMPONENT_MAIN The "COMPONENT <comp main>" string.
## @param COMPONENT_DEV The "COMPONENT <comp dev>" string.
function(_mycmake_install_bin_target NAME TGT_TYPE COMPONENT_MAIN COMPONENT_DEV IS_OPTIONAL)
    if (("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        AND ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY"))
        set(ARCHIVE_OUT ${CMAKE_INSTALL_BINDIR})
    else (("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        AND ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY"))
        set(ARCHIVE_OUT ${CMAKE_INSTALL_LIBDIR})
    endif (("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        AND ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY"))

    install(TARGETS ${NAME}
        ${IS_OPTIONAL}
        EXPORT ${PROJECT_NAME}Targets
        ARCHIVE DESTINATION ${ARCHIVE_OUT}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        FRAMEWORK DESTINATION ${CMAKE_INSTALL_BINDIR}
        DESTINATION error_tgt_shared
        ${COMPONENT_MAIN}
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
        ${COMPONENT_DEV}
        INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
        )
    #export(TARGETS ${NAME} FILE ${CMAKE_PROJECT_NAME}Targets.cmake)
endfunction(_mycmake_install_bin_target)

## Private function to add PDB file to install targets.
## @param NAME The target name
## @param TGT_TYPE The target type
## @param COMPONENT_DEV The "COMPONENT <comp dev>" string.
function(_mycmake_install_pdb NAME TGT_TYPE COMPONENT_DEV)
    if (NOT MYCMAKE_COMPILER_IS_MSVC)
        return()
    endif (NOT MYCMAKE_COMPILER_IS_MSVC)

    if (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE"))
        mycmake_debug_message("Adding install PDB file for ${NAME}")
        install(FILES $<TARGET_PDB_FILE:${NAME}>
            DESTINATION ${CMAKE_INSTALL_BINDIR}
            ${COMPONENT_DEV}
            OPTIONAL
            )
    elseif ("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")
        # No PDB files for static libs
    else (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE"))
        mycmake_error_message("Unexpected type for target ${NAME}: ${TGT_TYPE}")
    endif (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE"))
endfunction(_mycmake_install_pdb)

## Private function to add export .h file to install targets.
## @param NAME The target name
## @param TGT_TYPE The target type
## @param COMPONENT_DEV The "COMPONENT <comp dev>" string.
function(_mycmake_install_export_file NAME TGT_TYPE COMPONENT_DEV)
    if (NOT (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR ("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")))
        return()
    endif (NOT (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR ("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")))

    string(REPLACE "${MYCMAKE_STATIC_SUFFIX}" "" NO_STATIC_NAME ${NAME})
    if (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR (NOT ("${NAME}" STREQUAL "${NO_STATIC_NAME}${MYCMAKE_STATIC_SUFFIX}")))
        set(NO_STATIC_NAME ${NAME})
    endif (("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR (NOT ("${NAME}" STREQUAL "${NO_STATIC_NAME}${MYCMAKE_STATIC_SUFFIX}")))

    if (TARGET ${NO_STATIC_NAME})
        # OK: usual ${NAME}${MYCMAKE_STATIC_SUFFIX} convention for static libs and exists a shared lib
    elseif (TARGET ${NAME})
        # OK: actual target or static lib without shared lib
    else (TARGET ${NO_STATIC_NAME})
        mycmake_error_message("Unexpected NAME: ${NAME}")
    endif (TARGET ${NO_STATIC_NAME})

    get_property(BIN_DIR TARGET ${NAME} PROPERTY BINARY_DIR)
    mycmake_debug_message("Target ${NAME} has export file: ${BIN_DIR}/${NO_STATIC_NAME}${MYCMAKE_EXPORTFILE_SUFFIX}")
    if (EXISTS ${BIN_DIR}/${NO_STATIC_NAME}${MYCMAKE_EXPORTFILE_SUFFIX})
        mycmake_debug_message("Target ${NAME} has export file: ${BIN_DIR}/${NO_STATIC_NAME}${MYCMAKE_EXPORTFILE_SUFFIX}")
        install(FILES ${BIN_DIR}/${NO_STATIC_NAME}${MYCMAKE_EXPORTFILE_SUFFIX}
            DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${NO_STATIC_NAME}
            ${COMPONENT_DEV}
            )
    endif (EXISTS ${BIN_DIR}/${NO_STATIC_NAME}${MYCMAKE_EXPORTFILE_SUFFIX})
endfunction(_mycmake_install_export_file)

## Private function to add include dir to install targets.
## @param NAME The target name
## @param TGT_TYPE The target type
## @param COMPONENT_DEV The "COMPONENT <comp dev>" string.
function(_mycmake_install_include_dir NAME TGT_TYPE COMPONENT_DEV)
    if (NOT(("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR ("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")))
        return()
    endif (NOT(("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY") OR ("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")))

    get_property(SRC_DIR TARGET ${NAME} PROPERTY SOURCE_DIR)

    if (IS_DIRECTORY ${SRC_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
        mycmake_install(${COMPONENT_DEV} ${SRC_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
    endif (IS_DIRECTORY ${SRC_DIR}/${CMAKE_INSTALL_INCLUDEDIR})
endfunction(_mycmake_install_include_dir)


## @brief Generic install target dispatcher
function(_mycmake_install_target NAME COMPONENT_MAIN COMPONENT_DEV COMPONENT_DOC NO_DEFAULT_INSTALLS IS_OPTIONAL)
    get_target_property(TGT_TYPE ${NAME} TYPE)
    if (("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "MODULE_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE")
            )
        _mycmake_install_bin_target(${NAME} ${TGT_TYPE} "${COMPONENT_MAIN}" "${COMPONENT_DEV}" "${IS_OPTIONAL}")
        if (NOT NO_DEFAULT_INSTALLS)
            _mycmake_install_pdb(${NAME} ${TGT_TYPE} "${COMPONENT_DEV}")
            _mycmake_install_export_file(${NAME} ${TGT_TYPE} "${COMPONENT_DEV}")
            _mycmake_install_include_dir(${NAME} ${TGT_TYPE} "${COMPONENT_DEV}")
        endif (NOT NO_DEFAULT_INSTALLS)
    elseif (("${TGT_TYPE}" STREQUAL "OBJECT_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "INTERFACE_LIBRARY")
            )
        mycmake_error_message("Unsupported ${TGT_TYPE} by mycmake_install()")
    elseif("${TGT_TYPE}" STREQUAL "UTILITY")
            # Custom targets... for the moment, suppose it's a doc
            get_target_property(GENERATED_OUTPUT ${NAME} OUTPUT_NAME)
            get_target_property(LABELS ${NAME} LABELS)
            set(iS_DIRECTORY OFF)
            set(iS_FILE OFF)
            if (MYCMAKE_IS_DIRECTORY_ON IN_LIST LABELS)
                set (IS_DIRECTORY ON)
            elseif (MYCMAKE_IS_DIRECTORY_OFF IN_LIST LABELS)
                set (IS_FILE ON)
            endif (MYCMAKE_IS_DIRECTORY_ON IN_LIST LABELS)
            if ((IS_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT})
                OR IS_DIRECTORY)
                install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT}/
                    DESTINATION ${CMAKE_INSTALL_DOCDIR}/${NAME}
                    ${COMPONENT_DOC}
                    ${IS_OPTIONAL}
                    )
            elseif (IS_DIRECTORY ${GENERATED_OUTPUT})
                install(DIRECTORY ${GENERATED_OUTPUT}
                    DESTINATION ${CMAKE_INSTALL_DOCDIR}/${NAME}
                    ${COMPONENT_DOC}
                    ${IS_OPTIONAL}
                    )
            elseif ((EXISTS ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT})
                OR IS_FILE)
               install(FILES
                   ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT}
                   DESTINATION ${CMAKE_INSTALL_DOCDIR}/${NAME}
                   ${COMPONENT_DOC}
                   ${IS_OPTIONAL}
                   )
             elseif (EXISTS ${GENERATED_OUTPUT})
                install(FILES
                    ${GENERATED_OUTPUT}
                    DESTINATION ${CMAKE_INSTALL_DOCDIR}/${NAME}
                    ${COMPONENT_DOC}
                    ${IS_OPTIONAL}
                    )
             else ((IS_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT})
                OR IS_DIRECTORY)
                 mycmake_error_message("Unexpected custom target source: ${GENERATED_OUTPUT}")
             endif ((IS_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${GENERATED_OUTPUT})
                OR IS_DIRECTORY)
    else(("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "MODULE_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
            OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE")
            )
        mycmake_error_message("Unsupported ${TGT_TYPE} by mycmake_install()")
    endif(("${TGT_TYPE}" STREQUAL "STATIC_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "MODULE_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "SHARED_LIBRARY")
        OR ("${TGT_TYPE}" STREQUAL "EXECUTABLE")
        )
endfunction(_mycmake_install_target)

## Private function to add dependencies to install targets.
## @TODO Remove/move into cpack support or as option. Requires custom management of imported targets.
## @param NAME The target name
## @param TGT_TYPE The target type
## @param COMPONENT_MAIN The "COMPONENT <comp main>" string.
function(_mycmake_install_dependencies NAME TGT_TYPE COMPONENT_MAIN)
    if (NOT("${TGT_TYPE}" STREQUAL "EXECUTABLE"))
        return()
    endif (NOT("${TGT_TYPE}" STREQUAL "EXECUTABLE"))

    mycmake_get_interface_link_libraries(LIB_LIST ${NAME})
    if (LIB_LIST)
        foreach (LIB ${LIB_LIST})
            if (TARGET ${LIB})
                get_target_property(IS_IMPORTED ${LIB} IMPORTED)
                if (IS_IMPORTED)
                    get_target_property(LIB_PATH ${LIB} LOCATION)
                    mycmake_install(${COMPONENT_MAIN} ${LIB_PATH})
                endif (IS_IMPORTED)
            endif (TARGET ${LIB})
        endforeach(LIB)
    endif (LIB_LIST)
endfunction(_mycmake_install_dependencies)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_additional_files

    Adds additional files to be displayed by IDEs.

    .. code-block:: cmake

        mycmake_add_additional_files([ARGN])

    Parameters:

    * ``ARGN``: The list of files
#]=======================================================================]
function(mycmake_add_additional_files)
    if (NOT (TARGET ${MYCMAKE_ADDITIONAL_FILES_TARGET}))
        mycmake_add_custom_target(${MYCMAKE_ADDITIONAL_FILES_TARGET}
            OFF
            COMMENT "Additional files to be displayed")
        set(TMP )
        foreach(FILE ${ARGN})
            get_filename_component(F "${FILE}" ABSOLUTE BASE_DIR ${CMAKE_CURRENT_LIST_DIR})
            set(TMP ${TMP} ${F})
        endforeach(FILE ${ARGN})
        set_target_properties(${MYCMAKE_ADDITIONAL_FILES_TARGET}
            PROPERTIES
                SOURCES "${TMP}"
        )
    else (NOT (TARGET ${MYCMAKE_ADDITIONAL_FILES_TARGET}))
        get_target_property(SRC_LIST ${MYCMAKE_ADDITIONAL_FILES_TARGET} SOURCES)
        set(TMP )
        foreach(FILE ${ARGN})
            get_filename_component(F "${FILE}" ABSOLUTE BASE_DIR ${CMAKE_CURRENT_LIST_DIR})
            set(TMP ${TMP} ${F})
        endforeach(FILE ${ARGN})
        set_target_properties(${MYCMAKE_ADDITIONAL_FILES_TARGET}
            PROPERTIES
                SOURCES "${SRC_LIST};${TMP}"
            )
    endif (NOT (TARGET ${MYCMAKE_ADDITIONAL_FILES_TARGET}))
endfunction(mycmake_add_additional_files)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_custom_target

    Adds a custom target

    .. code-block:: cmake

        mycmake_add_custom_target(<TARGET> <OUTPUTNAME> <IS_DIRECTORY> [ARGN])

    Parameters:

    * ``<TARGET>``: The target name
    * ``<OUTPUTNAME>``: The target output name
    * ``<IS_DIRECTORY>``: ON if ouput is a directory, OFF if it is a file
    * ``ARGN``: Other :cmake:command:`add_custom_target()` parameters
#]=======================================================================]
macro(mycmake_add_custom_target TARGET OUTPUTNAME IS_DIRECTORY)
    # Defined in Base:
    _mycmake_add_custom_target(${TARGET} ${OUTPUTNAME} ${IS_DIRECTORY} ${ARGN})
endmacro(mycmake_add_custom_target)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_executable

    Adds an executable.

    .. code-block:: cmake

        mycmake_add_executable(<NAME> [NO_RC] [ARGN])

    Generates automatically a RC file.

    Parameters:

    * ``<NAME>``: The target name.
    * ``NO_RC``: Do not generate a RC resource file.
    * ``ARGN``: Standard :cmake:command:`add_executable()` parameters.
#]=======================================================================]
function(mycmake_add_executable NAME)
    cmake_parse_arguments(options
        "NO_RC"
        ""
        ""
        ${ARGN}
    )

    if (NOT options_NO_RC)
        mycmake_add_rc_file(RC_FILE)
    endif (NOT options_NO_RC)

    add_executable(${NAME} ${options_UNPARSED_ARGUMENTS})

    if (MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY)
        set_target_properties(${NAME}
            PROPERTIES
            C_VISIBILITY_PRESET "default"
            CXX_VISIBILITY_PRESET "default"
            )
    endif (MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY)

    if (NOT GENERATOR_IS_MULTI_CONFIG)
        #get_target_property(RTDIR ${NAME} RUNTIME_OUTPUT_DIRECTORY)
        set_target_properties(${NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${NAME}-build")
    endif (NOT GENERATOR_IS_MULTI_CONFIG)

    if (NOT NO_RC)
        target_sources(${NAME} PRIVATE ${RC_FILE})
    endif (NOT NO_RC)
endfunction(mycmake_add_executable)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_fake_target

    Adds a dummy install command, to avoid "make install" errors.

    .. code-block:: cmake

        mycmake_add_fake_target([ALL])

    Parameters:

    * ``ALL``: Additionally, add a fake custom target, to avoid "make all" errors.
#]=======================================================================]
macro(mycmake_add_fake_target)
    cmake_parse_arguments(options "ALL" "" "" ${ARGN})
    if (options_ALL)
        set(NAME ${MYCMAKE_FAKE_TARGET_NAME})
        if (NOT (TARGET ${NAME}))
            mycmake_add_custom_target(
                ${NAME}
                ${NAME}
                ON
                ALL
                COMMAND ${CMAKE_COMMAND} -E echo "Fake target execution"
                COMMENT "This is the fake target named ${NAME}"
                )
        endif (NOT (TARGET ${NAME}))
        unset(NAME)
    endif (options_ALL)
    # Dummy install to avoid make install failing:
    install(FILES mycmake_dummy.txt DESTINATION ${CMAKE_INSTALL_DOCDIR} OPTIONAL)
endmacro(mycmake_add_fake_target)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_library

    Adds a library target.

    .. code-block:: cmake

        mycmake_add_library(
            <NAME>
            [NO_VISIBILITY]
            [NO_EXPORT_HEADER]
            [NO_RC]
            [ARGN]
            )

    Note: this command can add both static and shared libs.
    Static lib targets will have a :cmake:variable:`MYCMAKE_STATIC_SUFFIX`
    suffix in the name.

    Generated export file name is: ${NAME}${MYCMAKE_EXPORTFILE_SUFFIX}

    Generates automatically a RC file.

    Parameters:

    * ``<NAME>``: The target name
    * ``NO_VISIBILITY``:  Do not manage visibility (VISIBILITY_INLINES_HIDDEN,
        <LANG>_VISIBILITY_PRESET)
    * ``NO_EXPORT_HEADER``: Do not generate export header (via
        :cmake:module:GenerateExportHeader)
    * ``NO_RC``: Do not generate a RC resource file.
    * ``ARGN``: The standard :cmake:command:`add_library()` options.
#]=======================================================================]
function(mycmake_add_library NAME)
    cmake_parse_arguments(options
        "NO_VISIBILITY;MODULE;SHARED;STATIC;NO_EXPORT_HEADER;NO_RC"
        ""
        ""
        ${ARGN}
    )
    # Setting specifiers according with BUILD_SHARED_LIBS,
    # if none is set, otherwise setting them according with
    # the user parameters:
    if((NOT options_SHARED) AND (NOT options_STATIC) AND (NOT options_MODULE))
        if(${BUILD_SHARED_LIBS})
            set(options_SHARED ON)
        else(${BUILD_SHARED_LIBS})
            set(options_STATIC ON)
        endif (${BUILD_SHARED_LIBS})
    endif((NOT options_SHARED) AND (NOT options_STATIC) AND (NOT options_MODULE))

    if (NOT options_NO_RC)
        mycmake_add_rc_file(RC_FILE)
    endif (NOT options_NO_RC)

    # Adding module lib:
    if(options_MODULE)
        mycmake_error_message("Unsupported building of modules. Target: ${NAME}")
    endif(options_MODULE)

    # Adding shared lib:
    if(options_SHARED)
        add_library(${NAME} SHARED ${options_UNPARSED_ARGUMENTS})
        set_property(TARGET ${NAME} PROPERTY VERSION ${PROJECT_VERSION})
        set_property(TARGET ${NAME} PROPERTY SOVERSION ${PROJECT_VERSION_MAJOR})
        set_property(TARGET ${NAME} APPEND PROPERTY COMPATIBLE_INTERFACE_STRING "${PROJECT_MAJOR_VERSION}")
        if (NOT options_NO_VISIBILITY)
            set_property(TARGET ${NAME} PROPERTY C_VISIBILITY_PRESET "hidden")
            set_property(TARGET ${NAME} PROPERTY CXX_VISIBILITY_PRESET "hidden")
            set_property(TARGET ${NAME} PROPERTY VISIBILITY_INLINES_HIDDEN ON)
        endif (NOT options_NO_VISIBILITY)
        if (NOT options_NO_EXPORT_HEADER)
            mycmake_debug_message("Generating export header for ${NAME}")
            GENERATE_EXPORT_HEADER(
                ${NAME}
                EXPORT_FILE_NAME ${NAME}${MYCMAKE_EXPORTFILE_SUFFIX}
                )
        endif (NOT options_NO_EXPORT_HEADER)
        if (RC_FILE)
            target_sources(${NAME} PRIVATE ${RC_FILE})
        endif (RC_FILE)
    endif(options_SHARED)

    # Adding static lib:
    if(options_STATIC)
        add_library(${NAME}${MYCMAKE_STATIC_SUFFIX} STATIC ${options_UNPARSED_ARGUMENTS})
        set_target_properties(${NAME}${MYCMAKE_STATIC_SUFFIX} PROPERTIES OUTPUT_NAME "${NAME}")
        set_target_properties(${NAME}${MYCMAKE_STATIC_SUFFIX} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY "${NAME}${MYCMAKE_STATIC_SUFFIX}")
        set_property(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX} PROPERTY VERSION ${PROJECT_VERSION})
        set_property(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX} PROPERTY SOVERSION ${PROJECT_VERSION_MAJOR})
        set_property(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX} APPEND PROPERTY COMPATIBLE_INTERFACE_STRING "${PROJECT_MAJOR_VERSION}")
        string(TOUPPER ${NAME} UPPER_NAME)
        target_compile_definitions(${NAME}${MYCMAKE_STATIC_SUFFIX} PUBLIC -D${UPPER_NAME}_STATIC_DEFINE)
        if (NOT options_NO_EXPORT_HEADER)
            mycmake_debug_message("Generating export header for ${NAME}")
            if (TARGET "${NAME}")
                set(TGT ${NAME})
            else (TARGET "${NAME}")
                set(TGT ${NAME}${MYCMAKE_STATIC_SUFFIX})
            endif (TARGET "${NAME}")
            GENERATE_EXPORT_HEADER(
                ${TGT}
                BASE_NAME ${NAME}
                EXPORT_FILE_NAME ${NAME}${MYCMAKE_EXPORTFILE_SUFFIX}
                )
        endif (NOT options_NO_EXPORT_HEADER)
    endif(options_STATIC)

    if (NOT options_NO_EXPORT_HEADER)
        mycmake_target_include_directories(${NAME} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)
    endif (NOT options_NO_EXPORT_HEADER)
endfunction(mycmake_add_library)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_add_rc_file

    Configures a Windows resource file.

    .. code-block:: cmake

        mycmake_add_rc_file(
            <OUT>
            [BIN_DIR <dir>]
            [NAME <name>]
            [FILE_VERSION <version>]
            [COPYRIGHT <name>]
            [ORIGINAL_FILENAME <name>]
            [PRODUCT_NAME <name>]
            [PRODUCT_VERSION <version>]
            [DESCRIPTION <description>]
            [ICON <filepath>]
            [LEGAL_COPYRIGHT <descr>]
            [LEGAL_TRADEMARKS <descr>]
            )

    See https://msdn.microsoft.com/library/aa381058

    Parameters:

    * ``<OUT>``: The output listThe output variable containing the full path to the generated rc file
    * ``BIN_DIR <dir>``: Where to generate the rc file. Default is :cmake:variable:`CMAKE_CURRENT_BINARY_DIR`.
    * ``NAME <name>``: The internal name of the target binary. Default is :cmake:variable:`PROJECT_NAME`.
    * ``FILE_VERSION <version>``: The current binary version. Defaults to :cmake:variable:`PROJECT_VERSION`.
    * ``COPYRIGHT <name>``: Copyright owner name. Default is :cmake:variable:`MYCMAKE_USER_COPYRIGHT` if set, or empty string.
    * ``ORIGINAL_FILENAME <name>``: The original filename with extension and without path. Defaults to ${NAME}
    * ``PRODUCT_NAME <name>``: The product name. Defaults to NAME.
    * ``PRODUCT_VERSION <version>``: The version. Defaults to FILE_VERSION.
    * ``DESCRIPTION <description>``: Binary brief description. Defaults to "${NAME} binary"
    * ``ICON <filepath>``: The binary icon. Defaults to ${CMAKE_LIB_DIR}/${PROJECT_NAME}/${MYCMAKE_ICON_NAME} if exists.
    * ``LEGAL_COPYRIGHT <descr>``: The legal copyright string.
    * ``LEGAL_TRADEMARKS <descr>``: Trademarks.
#]=======================================================================]
function(mycmake_add_rc_file OUT)
    cmake_parse_arguments(options
        ""
        "BIN_DIR;NAME;FILE_VERSION;COPYRIGHT;ORIGINAL_FILENAME;PRODUCT_NAME;PRODUCT_VERSION;DESCRIPTION;ICON;LEGAL_COPYRIGHT;LEGAL_TRADEMARKS"
        ""
        ${ARGN}
        )

    # Setup:
    if (options_BIN_DIR)
        set(BIN_DIR ${options_BIN_DIR})
    else (options_BIN_DIR)
        set(BIN_DIR ${CMAKE_CURRENT_BINARY_DIR})
    endif (options_BIN_DIR)

    # Mandatory:
    if (options_NAME)
        set(NAME ${options_NAME})
    else (options_NAME)
        set(NAME ${PROJECT_NAME})
    endif (options_NAME)

    if (options_FILE_VERSION)
        set(FILE_VERSION ${options_FILE_VERSION})
    else (options_FILE_VERSION)
        set(FILE_VERSION ${PROJECT_VERSION})
    endif (options_FILE_VERSION)
    mycmake_tag2version(BIN_FILE_VERSION ${FILE_VERSION})

    if (options_COPYRIGHT)
        set(COPYRIGHT "${options_COPYRIGHT}")
    else (options_COPYRIGHT)
        set(COPYRIGHT "")
    endif (options_COPYRIGHT)

    # @TODO: should contain extension, lib d etc.
    if (options_ORIGINAL_FILENAME)
        set(ORIGINAL_FILENAME "${options_ORIGINAL_FILENAME}")
    else (options_ORIGINAL_FILENAME)
        set(ORIGINAL_FILENAME "${NAME}")
    endif (options_ORIGINAL_FILENAME)

    if (options_PRODUCT_NAME)
        set(PRODUCT_NAME "${options_PRODUCT_NAME}")
    else (options_PRODUCT_NAME)
        set(PRODUCT_NAME "${NAME}")
    endif (options_PRODUCT_NAME)

    if (options_PRODUCT_VERSION)
        set(PRODUCT_VERSION ${options_PRODUCT_VERSION})
    else (options_PRODUCT_VERSION)
        set(PRODUCT_VERSION ${FILE_VERSION})
    endif (options_PRODUCT_VERSION)
    mycmake_tag2version(BIN_PRODUCT_VERSION ${PRODUCT_VERSION})

    if (options_DESCRIPTION)
        set(DESCRIPTION "${options_DESCRIPTION}")
    else (options_DESCRIPTION)
        set(DESCRIPTION "${NAME} binary")
    endif (options_DESCRIPTION)

    # Optional:
    if (options_ICON)
        set(ICON "id ICON \"ICON.ico\"")
    endif (options_ICON)

    if (options_LEGAL_COPYRIGHT)
        set(LEGAL_COPYRIGHT "VALUE \"LegalCopyright\", ${options_LEGAL_COPYRIGHT}")
    endif (options_LEGAL_COPYRIGHT)

    if (options_LEGAL_TRADEMARKS)
        set(LEGAL_TRADEMARKS "VALUE \"LegalTrademarks\", ${options_LEGAL_TRADEMARKS}")
    endif (options_LEGAL_TRADEMARKS)

    # Configuring:
    set(OUT_RC "${BIN_DIR}/resources.rc")
    configure_file("${MYCMAKE_SCRIPTS_EXTRA_DIR}/resources.in.rc" "${OUT_RC}" @ONLY)

    if(MYCMAKE_OS_IS_WINDOWS)
        enable_language(RC)
        set(${OUT} "${OUT_RC}" PARENT_SCOPE)
    else(MYCMAKE_OS_IS_WINDOWS)
        set(${OUT} "" PARENT_SCOPE)
    endif(MYCMAKE_OS_IS_WINDOWS)
endfunction(mycmake_add_rc_file )

#[=======================================================================[.rst:
.. cmake:command:: mycmake_export

    Exports the project configuration files.

    .. code-block:: cmake

        mycmake_export(<COMPONENT> [GLOBAL_EXPORT])

    Automatically create a namespace "${CMAKE_PROJECT_NAME}::"

    Parameters:

    * ``<COMPONENT>``: The reference component for installing the config files.
    * ``GLOBAL_EXPORT``: Set exporting to cmake module path.
#]=======================================================================]
function(mycmake_export COMPONENT)
    cmake_parse_arguments(options "GLOBAL_EXPORT" "" "" ${ARGN})
    write_basic_package_version_file(
        ${PROJECT_NAME}ConfigVersion.cmake
        COMPATIBILITY SameMajorVersion
        )

    export(EXPORT ${PROJECT_NAME}Targets
        FILE ${PROJECT_NAME}Targets.cmake
        NAMESPACE ${PROJECT_NAME}::
        )

    configure_file(${MYCMAKE_SCRIPTS_EXTRA_DIR}/ProjectConfig.cmake
      ${PROJECT_NAME}Config.cmake
      @ONLY
    )

    set(ConfigPackageLocation "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/cmake")
    install(EXPORT ${PROJECT_NAME}Targets
        FILE
            ${PROJECT_NAME}Targets.cmake
        NAMESPACE
            ${PROJECT_NAME}::
        DESTINATION
            ${ConfigPackageLocation}
        )

    install(
        FILES
            ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
            ${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
        DESTINATION
            ${ConfigPackageLocation}
        COMPONENT
            ${COMPONENT}
        )

    if (options_GLOBAL_EXPORT)
        export(PACKAGE ${PROJECT_NAME})
    endif (options_GLOBAL_EXPORT)
endfunction(mycmake_export)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_external_project

    Adds an extenral project.

    .. code-block:: cmake

        mycmake_external_project(<NAME> <SRC_DIR> <DEPENDENCIES> [ARGN])

    Parameters:

    * ``<NAME>``: The external project name.
    * ``<SRC_DIR>``: The external project dir containing its CMake.
    * ``<DEPENDENCIES>``: List of dependencies
    * ``ARGN``: Other ExternalProject_Add options
#]=======================================================================]
function(mycmake_external_project NAME SRC_DIR DEPENDENCIES)
    get_filename_component(ABS_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}" ABSOLUTE BASE_DIR ${CMAKE_INSTALL_PREFIX})
    ExternalProject_Add(
        ${NAME}
        DEPENDS ${DEPENDENCIES}
        SOURCE_DIR ${SRC_DIR}
        BUILD_ALWAYS 1
        CMAKE_ARGS
            -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
            -DCMAKE_C_STANDARD=${CMAKE_C_STANDARD}
            -DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}
            -DCMAKE_INSTALL_PREFIX=${ABS_INSTALL_DIR}
            -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            -DENABLE_GCC_SANITIZE=${ENABLE_GCC_SANITIZE}
        ${ARGN}
        )
    #ExternalProject_Get_Property(${NAME} BINARY_DIR)
    #set(${NAME}_DIR ${BINARY_DIR} CACHE "" INTERNAL)
endfunction(mycmake_external_project)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_get_interface_link_libraries

    Recursively calculates the list of interface link libraries.

    .. code-block:: cmake

        mycmake_get_interface_link_libraries(<OUT> <NAME>)

    Parameters:

    * ``<OUT>``: The output list
    * ``<NAME>``: The target name to analyze
#]=======================================================================]
function(mycmake_get_interface_link_libraries OUT NAME)
    _mycmake_get_single_interface_link_libraries(LIB_LIST ${NAME})
    set (TMP_LIST)
    if (LIB_LIST)
        foreach (L ${LIB_LIST})
            mycmake_get_interface_link_libraries(TMP ${L})
            mycmake_merge_lists(TMP_LIST "${TMP}")
            unset(TMP)
        endforeach (L ${LIB_LIST})
        mycmake_merge_lists(LIB_LIST "${TMP_LIST}")
        set(${OUT} ${LIB_LIST} PARENT_SCOPE)
    endif (LIB_LIST)
endfunction(mycmake_get_interface_link_libraries)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_install

     Adds install comands.

    .. code-block:: cmake

        mycmake_install(
            [COMPONENT <name>]
            [NO_DEFAULT_INSTALLS]
            [OPTIONAL]
            [ARGN]
            )

    Automatically installs both static and shared libs by just giving the
    shared target name. By default, installs targets, their headers, the support
    files (e.g. PDB) and GenerateExportHeader file. The :cmake:module:`GenerateExportHeader`
    file is installed into a directory called: ${CMAKE_INSTALL_INCLUDEDIR}/<target name>.
    Default compoenents are chosen by using the type.

    Parameters:

    * ``COMPONENT <name>``: The related component.
    * ``NO_DEFAULT_INSTALLS``: Do not install automatically the headers and
        support files (as PDB files) for given target lib.
    * ``OPTIONAL``: Marks components as optionals.
    * ``ARGN``: Targets, files and directories to be installed.
#]=======================================================================]
function(mycmake_install)
    cmake_parse_arguments(options "OPTIONAL;NO_DEFAULT_INSTALLS" "COMPONENT" "" ${ARGN})
    if (options_OPTIONAL)
        set(IS_OPTIONAL "OPTIONAL")
    endif (options_OPTIONAL)
    if (options_NO_DEFAULT_INSTALLS)
        set(NO_DEFAULT_INSTALLS "NO_DEFAULT_INSTALLS")
    endif (options_NO_DEFAULT_INSTALLS)
    if (options_COMPONENT)
        set (COMPONENT_MAIN COMPONENT ${options_COMPONENT})
        set (COMPONENT_DEV COMPONENT ${options_COMPONENT})
        set (COMPONENT_DOC COMPONENT ${options_COMPONENT})
    else (options_COMPONENT)
        set (COMPONENT_MAIN COMPONENT ${MYCMAKE_DEFAULT_COMPONENT_MAIN})
        set (COMPONENT_DEV COMPONENT ${MYCMAKE_DEFAULT_COMPONENT_DEV})
        set (COMPONENT_DOC COMPONENT ${MYCMAKE_DEFAULT_COMPONENT_DOC})
    endif (options_COMPONENT)

    foreach(NAME ${options_UNPARSED_ARGUMENTS})
        if (TARGET ${NAME})
            _mycmake_install_target(${NAME}
                "${COMPONENT_MAIN}"
                "${COMPONENT_DEV}"
                "${COMPONENT_DOC}"
                "${NO_DEFAULT_INSTALLS}"
                "${IS_OPTIONAL}"
                )
            if (TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
                mycmake_install(${NAME}${MYCMAKE_STATIC_SUFFIX}
                    ${COMPONENT_MAIN}
                    ${COMPONENT_DEV}
                    ${COMPONENT_DOC}
                    ${NO_DEFAULT_INSTALLS}
                    ${IS_OPTIONAL}
                    )
            endif (TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})

        elseif (TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
            mycmake_install(${NAME}${MYCMAKE_STATIC_SUFFIX}
                ${COMPONENT_MAIN}
                ${COMPONENT_DEV}
                ${COMPONENT_DOC}
                ${NO_DEFAULT_INSTALLS}
                ${IS_OPTIONAL}
                )
        elseif ((IS_DIRECTORY ${NAME})
                OR (IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}))
            install(DIRECTORY ${NAME}/
                DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
                ${COMPONENT_DEV}
                )
        elseif ((EXISTS ${NAME})
                OR (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}))
            install(FILES
                ${NAME}
                DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
                ${COMPONENT_DEV}
                )
        else (TARGET ${NAME})
            if ("${IS_OPTIONAL}" STREQUAL "")
                mycmake_error_message("Unexpected install type: ${NAME}")
            endif ("${IS_OPTIONAL}" STREQUAL "")
        endif (TARGET ${NAME})
    endforeach(NAME)
endfunction(mycmake_install)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_target_compile_definitions

    Adds compile definitions.

    .. code-block:: cmake

        mycmake_target_compile_definitions(<NAME> [ARGN])

    Automatically set definitions for both static and shared libs, by just giving
    the shared target name.

    Parameters:

    * ``<NAME>``: The target name.
    * ``ARGN``: Standard :cmake:command:`target_compile_definitions()` parameters.
#]=======================================================================]
function(mycmake_target_compile_definitions NAME)
    set (ERROR ON)
    if (TARGET ${NAME})
        target_compile_definitions(${NAME} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME})
    if (TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
        target_compile_definitions(${NAME}${MYCMAKE_STATIC_SUFFIX} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
    if (${ERROR})
        mycmake_error_message("Cannot find a target with name ${NAME} or ${NAME}${MYCMAKE_STATIC_SUFFIX}")
    endif (${ERROR})
endfunction(mycmake_target_compile_definitions)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_target_include_directories

    Adds includes.

    .. code-block:: cmake

        mycmake_target_include_directories(<NAME> [ARGN])

    Automatically set includes for both static and shared libs, by just giving
    the shared target name. In case of empty ARGN, default include and src
    headers are added.

    Parameters:

    * ``<NAME>``: The target name.
    * ``ARGN``: Standard :cmake:command:`target_include_directories()` parameters.
#]=======================================================================]
function(mycmake_target_include_directories NAME)
    set (ERROR ON)
    if (TARGET ${NAME})
        _mycmake_target_include_directories(${NAME} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME})
    if (TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
        _mycmake_target_include_directories(${NAME}${MYCMAKE_STATIC_SUFFIX} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
    if (${ERROR})
        mycmake_error_message("Cannot find a target with name ${NAME} or ${NAME}${MYCMAKE_STATIC_SUFFIX}")
    endif (${ERROR})
endfunction(mycmake_target_include_directories)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_target_link_libraries

    Sets link libraries.

    .. code-block:: cmake

        mycmake_target_link_libraries(<NAME> [ARGN])

    Automatically links both static and shared libs, by just giving the
    shared target name.

    Parameters:

    * ``<NAME>``: The target name.
    * ``ARGN``: Standard :cmake:command:`target_link_libraries()` parameters.
#]=======================================================================]
function(mycmake_target_link_libraries NAME)
    set (ERROR ON)
    if(TARGET ${NAME})
        target_link_libraries(${NAME} ${ARGN})
        _mycmake_targets_add_includes(${NAME} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME})
    if(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
        # No need of linking for static libs but this seems to track dependencies
        target_link_libraries(${NAME}${MYCMAKE_STATIC_SUFFIX} ${ARGN})
        _mycmake_targets_add_includes(${NAME}${MYCMAKE_STATIC_SUFFIX} ${ARGN})
        set (ERROR OFF)
    endif(TARGET ${NAME}${MYCMAKE_STATIC_SUFFIX})
    if (${ERROR})
        mycmake_error_message("Cannot find a target with name ${NAME} or ${NAME}${MYCMAKE_STATIC_SUFFIX}")
    endif (${ERROR})
endfunction(mycmake_target_link_libraries)

# EOF
