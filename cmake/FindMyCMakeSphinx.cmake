# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeSphinx
-----------------

Searches for `sphinx-build`.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeSphinx_EXECUTABLE

    The `sphinx-build` executable

.. cmake:variable:: MyCMakeSphinx_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeSphinx_VERSION

    This module version

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_sphinx_add_target()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)
find_package(MyCMakeTargets)

set(MyCMakeSphinx_VERSION ${MYCMAKE_VERSION})

if (MyCMakeBase_FOUND)
    mycmake_find_program(MyCMakeSphinx_EXECUTABLE
        NAMES sphinx-build
        DOC "The Shpinx build tool"
        )
endif (MyCMakeBase_FOUND)

find_package_handle_standard_args(
        MyCMakeSphinx
    FOUND_VAR
        MyCMakeSphinx_FOUND
    REQUIRED_VARS
        MyCMakeBase_FOUND
        MyCMakeTargets_FOUND
        MyCMakeSphinx_VERSION
        MyCMakeSphinx_EXECUTABLE
    VERSION_VAR
        MyCMakeSphinx_VERSION
)

if (MyCMakeSphinx_FOUND)
    mycmake_set_default_policies()
endif (MyCMakeSphinx_FOUND)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_sphinx_add_target

    Configures a custom target for generating documentation with Sphinx.

    .. code-block:: cmake

        mycmake_sphinx_add_target(
            <NAME>
            [OUTPUT_FORMAT <format>]
            [SRC_DIR <path>]
            [NO_DOC]
            [NO_INSTALL]
            [ARGN]
            [SPHINX_PARAMS <params ...>]
            )

    Please note that ``SPHINX_PARAMS <params ...>`` must be passed as last gument.

    Parameters:

    * ``<NAME>``: This custom target name
    * ``OUTPUT_FORMAT <format>``: The output format (-M). Default is html.
    * ``SRC_DIR <path>``: The source directory. Default is
        :cmake:variable:`${MYCMAKE_DOC_SOURCE_DIR}/${NAME}`.
    * ``NO_DOC``: Do not add this custom target to default target
        :cmake:variable:`MYCMAKE_DOC_TARGET_NAME`
    * ``NO_INSTALL``: Do not add output dir to install.It will be installed in
        :cmake:variable:`CMAKE_INSTALL_DOCDIR`/${NAME}
    * ``ARGN``: normal parameters for :cmake:command:`add_custom_target()`
    * ``SPHINX_PARAMS <params ...>``: Possible other flags for ``sphinx-build``
#]=======================================================================]
macro(mycmake_sphinx_add_target NAME)
    cmake_parse_arguments(options
        "NO_DOC;NO_INSTALL"
        "OUTPUT_FORMAT;SRC_DIR"
        "SPHINX_PARAMS"
        ${ARGN}
    )
    if (NOT options_OUTPUT_FORMAT)
        set(options_OUTPUT_FORMAT "html")
    endif (NOT options_OUTPUT_FORMAT)

    if (NOT options_SRC_DIR)
        set(options_SRC_DIR "${MYCMAKE_DOC_SOURCE_DIR}/${NAME}")
    endif (NOT options_SRC_DIR)

    mycmake_add_custom_target(
        ${NAME}
        doc/${NAME}/${options_OUTPUT_FORMAT}
        ON
        COMMAND
            ${MyCMakeSphinx_EXECUTABLE}
            -M ${options_OUTPUT_FORMAT}
            ${options_SRC_DIR}
            ${CMAKE_CURRENT_BINARY_DIR}/doc/${NAME}
            ${options_SPHINX_PARAMS}
        ${options_UNPARSED_ARGUMENTS}
        )
    if (NOT options_NO_DOC)
        add_dependencies(${MYCMAKE_DOC_TARGET_NAME} ${NAME})
    endif (NOT options_NO_DOC)

    if (NOT options_NO_INSTALL)
        mycmake_install(
            ${NAME}
            OPTIONAL
            )
    endif (NOT options_NO_INSTALL)

endmacro(mycmake_sphinx_add_target)

# EOF
