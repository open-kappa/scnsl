# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeSupportVariables
---------------------------

Settings of some global variables, not belonging to MyCMake,
but whoch can be useful for clients.
Provides also some support routines to find or include modules.
Finally performs some sanity checks.
This is an internal module, please prefer to nclude :cmake:module:`MyCMakeBase.`

The variables here contained can be overridden by command line, or into the
CMakeLists.txt before including *any* of MyCMake modules.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeSuppotVariables_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeSupportVariables_VERSION

    This module version


Operative system related variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_OS_WINDOWS_NAME

    The Windows OS name ("Windows")

.. cmake:variable:: MYCMAKE_OS_LINUX_NAME

    The Linux OS name ("Linux")

.. cmake:variable:: MYCMAKE_OS_OSX_NAME

    The OS X OS name ("Darwin")

.. cmake:variable:: MYCMAKE_OS_IS_<name>

    ON if the current OS is <name>

.. cmake:variable:: MYCMAKE_HOST_OS_IS_<name>

    ON if the current HOST OS is <name>

.. cmake:variable:: MYCMAKE_OS_LIUNX_KERNEL_4_8_0_58_GENERIC

    The name of that OS system version

.. cmake:variable:: MYCMAKE_OS_WINDOWS_7

    The name of that OS system version

.. cmake:variable:: MYCMAKE_OS_WINDOWS_8

    The name of that OS system version

.. cmake:variable:: MYCMAKE_OS_WINDOWS_8_1

    The name of that OS system version

.. cmake:variable:: MYCMAKE_OS_IS_<bits>

    ON if the OS is a <bits> bits version

.. cmake:variable:: MYCMAKE_OS_BITWIDTH

    The OS bits (e.g. 32, 64).

.. cmake:variable:: MYCMAKE_OS_IS_BIGENDIAN

    ON if the OS is bigendian. This property is set only if C or CXX languages
    are enabled.


Compilers variables
^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_COMPILER_GCC_ID

    The name of GCC compiler ("GNU")

.. cmake:variable:: MYCMAKE_COMPILER_CLANG_ID

    The name of Clang compiler ("Clang")

.. cmake:variable:: MYCMAKE_COMPILER_MSVC_ID

    The name of Microsoft VC++ compiler ("MSVC")

.. cmake:variable:: MYCMAKE_C_COMPILER_IS_<name>

    ON if the current C compiler name is <name>

.. cmake:variable:: MYCMAKE_CXX_COMPILER_IS_<name>

    ON if the current C++ compiler name is <name>

.. cmake:variable:: MYCMAKE_COMPILER_IS_<name>

    ON if the current compiler name is <name>.
    In case of both C and C++ compilers, assumes they are the same.

Environment variables
^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_OS_SHARED_LIBRARY_INSTALL_DIR

    Default location for installing shared libraries: "bin" on Windows,
    "lib" otherwise.

.. cmake:variable:: MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR

    Name of the environment variable to modify the runtime search path of shared
    libraries (PATH on Windows, LD_LIBRARY_PATH otherwise)

.. cmake:variable:: MYCMAKE_OS_ENV_PATH_SEPARATOR

    The separator to be used to modify the :cmake:variable:`MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR`
    environment variable (";" on Windows, ":" otherwise).

Generators variables
^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_COMPILER_UNIX_MAKEFILES

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_NMAKE_MAKEFILES_JOM

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_MINGW_MAKEFILES

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2019

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2017

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2017_64

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2015

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2015_64

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2013

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2013_64

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2012

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2012_64

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2010

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_2010_64

    The name of that CMake generator

.. cmake:variable:: MYCMAKE_COMPILER_VS_GENERATOR

    The name of the preferred Microsoft VC++ CMake generator

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_find_library()`
- :cmake:command:`mycmake_find_package()`
- :cmake:command:`mycmake_find_program()`
- :cmake:command:`mycmake_include()`

#]=======================================================================]
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakePolicy)
find_package(MyCMakeMessage)
find_package(MyCMakeGlobals)

set(MyCMakeSupportVariables_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeSupportVariables
    FOUND_VAR
        MyCMakeSupportVariables_FOUND
    REQUIRED_VARS
        MyCMakePolicy_FOUND
        MyCMakeMessage_FOUND
        MyCMakeGlobals_FOUND
        MyCMakeSupportVariables_VERSION
    VERSION_VAR
        MyCMakeSupportVariables_VERSION
)

if (MyCMakeSupportVariables_FOUND)
    mycmake_set_default_policies()
    include(TestBigEndian)
    include(GNUInstallDirs)

    mycmake_debug_message("CMAKE_SYSTEM: ${CMAKE_SYSTEM}")
    mycmake_debug_message("CMAKE_SYSTEM_PROCESSOR: ${CMAKE_SYSTEM_PROCESSOR}")
    mycmake_debug_message("CMAKE_C_COMPILER_ID: ${CMAKE_C_COMPILER_ID}")
    mycmake_debug_message("CMAKE_CXX_COMPILER_ID: ${CMAKE_CXX_COMPILER_ID}")
    mycmake_debug_message("CMAKE_GENERATOR: ${CMAKE_GENERATOR}")
endif (MyCMakeSupportVariables_FOUND)

## @brief Standard support paths
## @param OUTVAR The output list
## @param NAME The package name
function(_mycmake_get_additional_paths OUTVAR NAME)
    set(ADDITIONAL_PATHS
    # Other same-level projects:
    "${PROJECT_BINARY_DIR}/../../${NAME}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}"
    "${PROJECT_BINARY_DIR}/../../${NAME}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}/${NAME}/share/${NAME}/cmake"
    # @TODO: Project internal third-parties:
    #"${PROJECT_BINARY_DIR}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}"
    # Project external third-parties:
    "${PROJECT_BINARY_DIR}/../../${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}"
    "${PROJECT_BINARY_DIR}/../../${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/share/${NAME}/cmake"
    )
    set(${OUTVAR} ${ADDITIONAL_PATHS} PARENT_SCOPE)
endfunction(_mycmake_get_additional_paths)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_find_library

    Includes a package.

    .. code-block:: cmake

        mycmake_find_library([ARGN])

    Wraps standard :cmake:command:`find_library()`.

    Parameters:

    * ``ARGN``: The standard :cmake:command:`find_library()` options.
#]=======================================================================]
macro(mycmake_find_library)
    find_library(${ARGN})
endmacro(mycmake_find_library)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_find_package

    Finds a package.

    .. code-block:: cmake

        mycmake_find_package(<NAME> [CHECK_THIRD_PARTIES <dir name>] [ARGN])

    Wraps standard :cmake:command:`find_package()`, in order to add some
    default paths.

    The option ``CHECK_THIRD_PARTIES <dir name>`` enables a special behavior.
    Instead of searching for the package, the "third-parties/<dirname>" directory
    is searched for a CMakeLists.txt and the package is considered found if it
    exists. This allows to later consider such sources as an external project
    or wti include with :cmake:command:`add_subdirectory()`.
    If the CMakeLists.txt exists, the variable ``${NAME}_FOUND`` will be set to
    ON, and the ``${NAME}_THIRD_PARTIES_FOUND`` will be set to ON.
    Otherwise,  ``${NAME}_THIRD_PARTIES_FOUND`` will be set to OFF, and the actual
    call to :cmake:command:`find_packge()` will be performed.

    Parameters:

    * ``<NAME>``: The package name
    * ``CHECK_THIRD_PARTIES <dir name>``: Checks wheter the package sources exists in thitd-parties
    * ``ARGN``: The standard :cmake:command:`find_package()` options.
#]=======================================================================]
macro(mycmake_find_package NAME)
    cmake_parse_arguments(options "" "CHECK_THIRD_PARTIES" "" ${ARGN})
    _mycmake_get_additional_paths(MYCMAKE_ADDITIONAL_PATHS ${NAME})
    if ((options_CHECK_THIRD_PARTIES)
        AND (EXISTS "${CMAKE_SOURCE_DIR}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/${options_CHECK_THIRD_PARTIES}/CMakeLists.txt"))
        set(${NAME}_FOUND ON)
        set(${NAME}_THIRD_PARTIES_FOUND ON)
    else ((options_CHECK_THIRD_PARTIES)
        AND (EXISTS "${CMAKE_SOURCE_DIR}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/${options_CHECK_THIRD_PARTIES}/CMakeLists.txt"))
        set(${NAME}_THIRD_PARTIES_FOUND OFF)
        # For find of modules
        set(CMAKE_MODULE_PATH
            ${CMAKE_MODULE_PATH}
            ${MYCMAKE_ADDITIONAL_PATHS}
        )
        # For find of modules
        set(CMAKE_PREFIX_PATH
            ${CMAKE_PREFIX_PATH}
            ${MYCMAKE_ADDITIONAL_PATHS}
        )
        unset(MYCMAKE_ADDITIONAL_PATHS)
        find_package(${NAME} ${options_UNPARSED_ARGUMENTS})
    endif ((options_CHECK_THIRD_PARTIES)
        AND (EXISTS "${CMAKE_SOURCE_DIR}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/${options_CHECK_THIRD_PARTIES}/CMakeLists.txt"))


endmacro(mycmake_find_package)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_find_program

    Includes a package.

    .. code-block:: cmake

        mycmake_find_program([ARGN])

    Wraps standard :cmake:command:`find_program()`.

    Parameters:

    * ``ARGN``: The standard :cmake:command:`find_program()` options.
#]=======================================================================]
macro(mycmake_find_program)
    find_program(${ARGN})
endmacro(mycmake_find_program)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_include

    Includes a package.

    .. code-block:: cmake

        mycmake_include(<NAME> [ARGN])

    Wraps standard :cmake:command:`include()`, in order to add some
    default paths.

    Parameters:

    * ``<NAME>``: The package name
    * ``ARGN``: The standard :cmake:command:`include()` options.
#]=======================================================================]
macro(mycmake_include NAME)
    _mycmake_get_additional_paths(MYCMAKE_ADDITIONAL_PATHS ${NAME})
    set(CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${MYCMAKE_ADDITIONAL_PATHS}
    )
    set(CMAKE_PREFIX_PATH
        ${CMAKE_PREFIX_PATH}
        ${MYCMAKE_ADDITIONAL_PATHS}
    )
    unset(MYCMAKE_ADDITIONAL_PATHS)
    include(${NAME})
endmacro(mycmake_include)


## Private function to set a list of bool flags.
## @param PREFIX The flags prefix. E.g. MYCMAKE_OS_IS_
## @param SUFFIXES (List) The flags suffixes list. E.g. "WINDOWS;LINUX;OSX"
## @param CONDITION The variable name to be used to match. E.g. CMAKE_SYSTEM_NAME
## @param ALTERNATIVES (List) The match possible alternatives, in the same order of SUFFIXES. E.g. "Windows;Linux;Darwin"
## @tag COMPARATOR <equal comparator> (Optional) Comparator to be used as operator inside if()
## @tag NO_FATAL_EMPTY (Optional) Empty condition is not fatal.
## @tag NO_FATAL (Optional) Unknown value is not fatal.
function(_mycmake_supportvariables_set_bools PREFIX SUFFIXES CONDITION ALTERNATIVES)
    cmake_parse_arguments(options "NO_FATAL_EMPTY;NO_FATAL" "COMPARATOR" "" ${ARGN})
    if (NOT ${CONDITION})
        if (options_NO_FATAL_EMPTY)
            return()
        endif (options_NO_FATAL_EMPTY)
        mycmake_message_error("Empty ${CONDITION}")
    endif (NOT ${CONDITION})
    set(COMPARATOR STREQUAL)
    if (options_COMPARATOR)
        set(COMPARATOR ${options_COMPARATOR})
    endif (options_COMPARATOR)
    set(MISSING ON)
    foreach (ALT ${ALTERNATIVES})
        list(FIND ALTERNATIVES ${ALT} INDEX)
        list(GET SUFFIXES ${INDEX} SUF)
        if ("${${CONDITION}}" ${COMPARATOR} "${ALT}")
            set(${PREFIX}_${SUF} ON)
            set(MISSING OFF)
        else ("${${CONDITION}}" ${COMPARATOR} "${ALT}")
            set(${PREFIX}_${SUF} OFF)
        endif ("${${CONDITION}}" ${COMPARATOR} "${ALT}")
        set(${PREFIX}_${SUF} ${${PREFIX}_${SUF}} PARENT_SCOPE)
        mycmake_debug_message("${PREFIX}_${SUF}: ${${PREFIX}_${SUF}}")
    endforeach (ALT ${ALTERNATIVES})
    if (MISSING)
        if (options_NO_FATAL)
            mycmake_debug_error_message("Unknown ${CONDITION}: ${${CONDITION}}")
        else (options_NO_FATAL)
            mycmake_error_message("Unknown ${CONDITION}: ${${CONDITION}}")
        endif (options_NO_FATAL)
    endif (MISSING)
endfunction(_mycmake_supportvariables_set_bools)

## Private function to check an element is in given list.
## @param NAME The value name to use in debug print
## @param VALUE The value to check
## @param LIST The list variable
## @tag COMPARATOR <equal comparator> (Optional) Comparator to be used as operator inside if()
## @tag NO_FATAL_EMPTY (Optional) Empty value is not fatal.
## @tag NO_FATAL (Optional) Unknown value is not fatal.
function(_mycmake_check_in_list NAME VALUE LIST)
    cmake_parse_arguments(options "NO_FATAL_EMPTY;NO_FATAL" "COMPARATOR" "" ${ARGN})
    if (NOT VALUE)
        if (options_NO_FATAL_EMPTY)
            return()
        endif (options_NO_FATAL_EMPTY)
        mycmake_message_error("Empty ${VALUE}")
    endif (NOT VALUE)
    set(COMPARATOR STREQUAL)
    if (options_COMPARATOR)
        set(COMPARATOR ${options_COMPARATOR})
    endif (options_COMPARATOR)
    foreach(V ${${LIST}})
        if ("${VALUE}" ${COMPARATOR} "${V}")
            return()
        endif ("${VALUE}" ${COMPARATOR} "${V}")
    endforeach(V ${${LIST}})
    if (options_NO_FATAL)
        mycmake_debug_error_message("Unknown ${NAME}: ${VALUE}")
    else (options_NO_FATAL)
        mycmake_error_message("Unknown ${NAME}: ${VALUE}")
    endif (options_NO_FATAL)
endfunction(_mycmake_check_in_list)

if (NOT PROJECT_NAME)
    # In case of CMake scripts, some system variables are empty by default:
    set(_mycmake_check "NO_FATAL_EMPTY")
endif (NOT PROJECT_NAME)

mycmake_try_set(MYCMAKE_OS_WINDOWS_NAME "Windows")
mycmake_try_set(MYCMAKE_OS_LINUX_NAME "Linux")
mycmake_try_set(MYCMAKE_OS_OSX_NAME "Darwin")
_mycmake_supportvariables_set_bools(
    "MYCMAKE_OS_IS"
    "WINDOWS;LINUX;OSX"
    CMAKE_SYSTEM_NAME
    "${MYCMAKE_OS_WINDOWS_NAME};${MYCMAKE_OS_LINUX_NAME};${MYCMAKE_OS_OSX_NAME}"
    )
_mycmake_supportvariables_set_bools(
    "MYCMAKE_HOST_OS_IS"
    "WINDOWS;LINUX;OSX"
    CMAKE_HOST_SYSTEM_NAME
    "${MYCMAKE_OS_WINDOWS_NAME};${MYCMAKE_OS_LINUX_NAME};${MYCMAKE_OS_OSX_NAME}"
    )

mycmake_try_set(MYCMAKE_OS_LIUNX_KERNEL_4_8_0_58_GENERIC "4.8.0-58-generic")
mycmake_try_set(MYCMAKE_OS_WINDOWS_7 "6.1")
mycmake_try_set(MYCMAKE_OS_WINDOWS_8 "6.2")
mycmake_try_set(MYCMAKE_OS_WINDOWS_8_1 "6.3")
mycmake_try_set(MYCMAKE_OS_WINDOWS_10 "10.0")
set(_mycmake_support_vars_all_os_names
    "${MYCMAKE_OS_LIUNX_KERNEL_4_8_0_58_GENERIC}.*"
    "${MYCMAKE_OS_WINDOWS_7}.*"
    "${MYCMAKE_OS_WINDOWS_8}.*"
    "${MYCMAKE_OS_WINDOWS_9}.*"
    "${MYCMAKE_OS_WINDOWS_10}.*"
    )
_mycmake_check_in_list("CMAKE_SYSTEM_VERSION" "${CMAKE_SYSTEM_VERSION}" _mycmake_support_vars_all_os_names COMPARATOR MATCHES  ${_mycmake_check} NO_FATAL)
_mycmake_check_in_list("CMAKE_HOST_SYSTEM_VERSION" "${CMAKE_HOST_SYSTEM_VERSION}" _mycmake_support_vars_all_os_names COMPARATOR MATCHES ${_mycmake_check} NO_FATAL)
unset(_mycmake_support_vars_all_os_names)

_mycmake_supportvariables_set_bools(
    "MYCMAKE_OS_IS"
    "64;32"
    CMAKE_SIZEOF_VOID_P
    "8;4"
    ${_mycmake_check}
    )
if (MYCMAKE_OS_IS_64)
    mycmake_try_set(MYCMAKE_OS_BITWIDTH 64)
elseif (MYCMAKE_OS_IS_32)
    mycmake_try_set(MYCMAKE_OS_BITWIDTH 32)
else (MYCMAKE_OS_IS_64)
    mycmake_error_message("Unexpected OS bitwidth")
endif (MYCMAKE_OS_IS_64)

get_property(mycmake_languages GLOBAL PROPERTY ENABLED_LANGUAGES)
if (("C" IN_LIST mycmake_languages) OR ("CXX" IN_LIST mycmake_languages))
    TEST_BIG_ENDIAN(_MYCMAKE_OS_IS_BIGENDIAN)
    mycmake_try_set(MYCMAKE_OS_IS_BIGENDIAN ${_MYCMAKE_OS_IS_BIGENDIAN})
    unset(_MYCMAKE_OS_IS_BIGENDIAN)
endif (("C" IN_LIST mycmake_languages) OR ("CXX" IN_LIST mycmake_languages))
unset(mycmake_languages)

mycmake_try_set(MYCMAKE_COMPILER_GCC_ID "GNU")
mycmake_try_set(MYCMAKE_COMPILER_CLANG_ID "Clang")
mycmake_try_set(MYCMAKE_COMPILER_MSVC_ID "MSVC")
set(_mycmake_support_vars_all_compilers_names
    ${MYCMAKE_COMPILER_GCC_ID}
    ${MYCMAKE_COMPILER_CLANG_ID}
    ${MYCMAKE_COMPILER_MSVC_ID}
    )
_mycmake_supportvariables_set_bools(
    "MYCMAKE_C_COMPILER_IS"
    "GCC;CLANG;MSVC"
    CMAKE_C_COMPILER_ID
    "${_mycmake_support_vars_all_compilers_names}"
    NO_FATAL_EMPTY
    )
_mycmake_supportvariables_set_bools(
    "MYCMAKE_CXX_COMPILER_IS"
    "GCC;CLANG;MSVC"
    CMAKE_CXX_COMPILER_ID
    "${_mycmake_support_vars_all_compilers_names}"
    NO_FATAL_EMPTY
    )
unset(_mycmake_support_vars_all_compilers_names)
if (MYCMAKE_CXX_COMPILER_IS_GCC OR MYCMAKE_C_COMPILER_IS_GCC)
    mycmake_try_set(MYCMAKE_COMPILER_IS_GCC ON)
    mycmake_try_set(MYCMAKE_COMPILER_IS_CLANG OFF)
    mycmake_try_set(MYCMAKE_COMPILER_IS_MSVC OFF)
elseif (MYCMAKE_CXX_COMPILER_IS_CLANG OR MYCMAKE_C_COMPILER_IS_CLANG)
    mycmake_try_set(MYCMAKE_COMPILER_IS_GCC OFF)
    mycmake_try_set(MYCMAKE_COMPILER_IS_CLANG ON)
    mycmake_try_set(MYCMAKE_COMPILER_IS_MSVC OFF)
elseif (MYCMAKE_CXX_COMPILER_IS_MSVC OR MYCMAKE_C_COMPILER_IS_MSVC)
    set (MYCMAKE_COMPILER_IS_GCC OFF)
    set (MYCMAKE_COMPILER_IS_CLANG OFF)
    set (MYCMAKE_COMPILER_IS_MSVC ON)
else (MYCMAKE_CXX_COMPILER_IS_GCC OR MYCMAKE_C_COMPILER_IS_GCC)
    mycmake_error_message("Unknown compiler")
endif (MYCMAKE_CXX_COMPILER_IS_GCC OR MYCMAKE_C_COMPILER_IS_GCC)


if (MYCMAKE_OS_IS_WINDOWS)
    mycmake_try_set(MYCMAKE_OS_SHARED_LIBRARY_INSTALL_DIR ${CMAKE_INSTALL_BINDIR})
    mycmake_try_set(MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR "PATH")
    mycmake_try_set(MYCMAKE_OS_ENV_PATH_SEPARATOR "\;")
else (MYCMAKE_OS_IS_WINDOWS)
    mycmake_try_set(MYCMAKE_OS_SHARED_LIBRARY_INSTALL_DIR ${CMAKE_INSTALL_LIBDIR})
    mycmake_try_set(MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR "LD_LIBRARY_PATH")
    mycmake_try_set(MYCMAKE_OS_ENV_PATH_SEPARATOR ":")
endif (MYCMAKE_OS_IS_WINDOWS)

#set(MYCMAKE_COMPILER_MSYS_MAKEFILES "MSYS Makefiles")
#set(MYCMAKE_COMPILER_NMAKE_MAKEFILES "NMake Makefiles")
#set(MYCMAKE_COMPILER_NAMAKE_JOM_MAKEFILES "NMake Makefiles JOM")
#set(MYCMAKE_COMPILER_NINJA "Ninja")
mycmake_try_set(MYCMAKE_COMPILER_UNIX_MAKEFILES "Unix Makefiles")
mycmake_try_set(MYCMAKE_COMPILER_NMAKE_MAKEFILES_JOM "NMake Makefiles JOM")
mycmake_try_set(MYCMAKE_COMPILER_MINGW_MAKEFILES "MinGW Makefiles")
mycmake_try_set(MYCMAKE_COMPILER_VS_2019    "Visual Studio 16 2019")
mycmake_try_set(MYCMAKE_COMPILER_VS_2017    "Visual Studio 15 2017")
mycmake_try_set(MYCMAKE_COMPILER_VS_2017_64 "Visual Studio 15 2017 Win64")
mycmake_try_set(MYCMAKE_COMPILER_VS_2015    "Visual Studio 14 2015")
mycmake_try_set(MYCMAKE_COMPILER_VS_2015_64 "Visual Studio 14 2015 Win64")
mycmake_try_set(MYCMAKE_COMPILER_VS_2013    "Visual Studio 12 2013")
mycmake_try_set(MYCMAKE_COMPILER_VS_2013_64 "Visual Studio 12 2013 Win64")
mycmake_try_set(MYCMAKE_COMPILER_VS_2012    "Visual Studio 11 2012")
mycmake_try_set(MYCMAKE_COMPILER_VS_2012_64 "Visual Studio 11 2012 Win64")
mycmake_try_set(MYCMAKE_COMPILER_VS_2010    "Visual Studio 10 2013")
mycmake_try_set(MYCMAKE_COMPILER_VS_2010_64 "Visual Studio 10 2010 Win64")

if (MYCMAKE_OS_IS_64)
    mycmake_try_set(MYCMAKE_COMPILER_VS_GENERATOR "${MYCMAKE_COMPILER_VS_2019}")
elseif (MYCMAKE_OS_IS_32)
    mycmake_try_set(MYCMAKE_COMPILER_VS_GENERATOR "${MYCMAKE_COMPILER_VS_2019}")
else (MYCMAKE_OS_IS_64)
    mycmake_error_message("Unsupported OS")
endif (MYCMAKE_OS_IS_64)

if (NOT CMAKE_GENERATOR)
    if (MYCMAKE_OS_IS_WINDOWS)
        mycmake_try_set(CMAKE_GENERATOR ${MYCMAKE_COMPILER_VS_GENERATOR})
    else (MYCMAKE_OS_IS_WINDOWS)
        mycmake_error_message("Unsupported OS")
    endif (MYCMAKE_OS_IS_WINDOWS)
endif (NOT CMAKE_GENERATOR)

set(_mycmake_support_vars_all_generator_names
    ${MYCMAKE_COMPILER_UNIX_MAKEFILES}
    ${MYCMAKE_COMPILER_NMAKE_MAKEFILES_JOM}
    ${MYCMAKE_COMPILER_MINGW_MAKEFILES}
    ${MYCMAKE_COMPILER_VS_2019}
    ${MYCMAKE_COMPILER_VS_2017}
    ${MYCMAKE_COMPILER_VS_2017_64}
    ${MYCMAKE_COMPILER_VS_2015}
    ${MYCMAKE_COMPILER_VS_2015_64}
    ${MYCMAKE_COMPILER_VS_2013}
    ${MYCMAKE_COMPILER_VS_2013_64}
    ${MYCMAKE_COMPILER_VS_2012}
    ${MYCMAKE_COMPILER_VS_2012_64}
    ${MYCMAKE_COMPILER_VS_2010}
    ${MYCMAKE_COMPILER_VS_2010_64}
    )

_mycmake_check_in_list("CMAKE_GENERATOR" "${CMAKE_GENERATOR}" _mycmake_support_vars_all_generator_names)
unset(_mycmake_support_vars_all_generator_names)


unset(_mycmake_check)

# EOF
