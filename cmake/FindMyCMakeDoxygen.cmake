# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeDoxygen
------------------

Convenience wrapper for standard :cmake:module:FindDoxygen module.

Result Variables
^^^^^^^^^^^^^^^^

.. cmake:variable:: MyCMakeDoxygen_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeDoxygen_VERSION

    This module version

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_doxygen_add_generic_target()`
- :cmake:command:`mycmake_doxygen_add_target()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)
find_package(MyCMakeTargets)
find_package(Doxygen)

set(MyCMakeDoxygen_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeDoxygen
    FOUND_VAR
        MyCMakeDoxygen_FOUND
    REQUIRED_VARS
        MyCMakeBase_FOUND
        MyCMakeTargets_FOUND
        MyCMakeDoxygen_VERSION
        DOXYGEN_FOUND
    VERSION_VAR
        MyCMakeDoxygen_VERSION
)

if (MyCMakeDoxygen_FOUND)
    mycmake_set_default_policies()
endif (MyCMakeDoxygen_FOUND)

macro(_mycmake_doxygen_add_target NAME)
    cmake_parse_arguments(options
        "NO_DOC;NO_INSTALL"
        ""
        ""
        ${ARGN}
    )
    if (NOT options_OUTPUT_FORMAT)
        set(options_OUTPUT_FORMAT "html")
    endif (NOT options_OUTPUT_FORMAT)

    if (NOT options_SRC_DIR)
        set(options_SRC_DIR "${MYCMAKE_DOC_SOURCE_DIR}/${NAME}")
    endif (NOT options_SRC_DIR)

    doxygen_add_docs(
        ${NAME}
        ${options_UNPARSED_ARGUMENTS}
        )
    get_target_property(VARS ${NAME} LABELS)
    set_target_properties(
        ${NAME}
        PROPERTIES
            OUTPUT_NAME ${NAME}/html
            LABELS "MYCMAKE_IS_DIRECTORY_ON;${VARS}"
        )
    unset(VARS)
    if (NOT options_NO_DOC)
        add_dependencies(${MYCMAKE_DOC_TARGET_NAME} ${NAME})
    endif (NOT options_NO_DOC)

    if (NOT options_NO_INSTALL)
        mycmake_install(
            ${NAME}
            OPTIONAL
            )
    endif (NOT options_NO_INSTALL)

endmacro(_mycmake_doxygen_add_target)


#[=======================================================================[.rst:
.. cmake:command:: mycmake_doxygen_add_generic_target

    Configures a target to generate doxygen documentation.

    .. code-block:: cmake

        mycmake_doxygen_add_generic_target(
            <NAME>
            [ARGN]
            )

        It wraps :cmake:command:`mycmake_doxygen_add_target()`
        to set some default Doxygen configurations.

    Parameters:

    * ``<NAME>``: This custom target name
    * ``ARGN``: The parameters suitables for
        :cmake:command:`mycmake_doxygen_add_target()`
#]=======================================================================]
macro(mycmake_doxygen_add_generic_target NAME)
    if (EXISTS "${MYCMAKE_DOC_SOURCE_DIR}/${NAME}.ico")
        mycmake_try_set(DOXYGEN_PROJECT_LOGO "${MYCMAKE_DOC_SOURCE_DIR}/${NAME}.ico")
    endif (EXISTS "${MYCMAKE_DOC_SOURCE_DIR}/${NAME}.ico")
    mycmake_try_set(DOXYGEN_ALWAYS_DETAILED_SEC "YES")
    mycmake_try_set(DOXYGEN_FULL_PATH_NAMES "YES")
    mycmake_try_set(DOXYGEN_STRIP_FROM_PATH "${PROJECT_SOURCE_DIR}/include")
    mycmake_try_set(DOXYGEN_EXTRACT_ALL "NO")
    mycmake_try_set(DOXYGEN_EXTRACT_PRIVATE "NO")
    mycmake_try_set(DOXYGEN_EXTRACT_STATIC "NO")
    mycmake_try_set(DOXYGEN_EXTRACT_LOCAL_CLASSES "NO")
    mycmake_try_set(DOXYGEN_EXTRACT_LOCAL_METHODS "NO")
    mycmake_try_set(DOXYGEN_EXTRACT_ANON_NSPACES "NO")
    mycmake_try_set(DOXYGEN_HIDE_IN_BODY_DOCS "YES")
    mycmake_try_set(DOXYGEN_SHOW_INCLUDE_FILES "NO")
    mycmake_try_set(DOXYGEN_SORT_MEMBERS_DOCS "YES")
    mycmake_try_set(DOXYGEN_SORT_BRIEF_DOCS "YES")
    mycmake_try_set(DOXYGEN_SORT_MEMBERS_CTORS_1ST "YES")
    mycmake_try_set(DOXYGEN_SORT_BRIEF_DOCS "YES")
    mycmake_try_set(DOXYGEN_SORT_BY_SCOPE_NAME "YES")
    mycmake_try_set(DOXYGEN_GENERATE_TODOLIST "YES")
    mycmake_try_set(DOXYGEN_GENERATE_TESTLIST "YES")
    mycmake_try_set(DOXYGEN_GENERATE_BUGLIST "YES")
    mycmake_try_set(DOXYGEN_GENERATE_DEPRECATEDLIST "YES")
    mycmake_try_set(DOXYGEN_SHOW_USED_FILES "NO")
    mycmake_try_set(DOXYGEN_SHOW_FILES "NO")
    mycmake_try_set(DOXYGEN_SHOW_NAMESPACES "YES")
    mycmake_try_set(DOXYGEN_SOURCE_BROWSER "NO")
    mycmake_try_set(DOXYGEN_INLINE_SOURCES "NO")
    mycmake_try_set(DOXYGEN_STRIP_CODE_COMMENTS "NO")
    mycmake_try_set(DOXYGEN_REFERENCES_LINK_SOURCE "NO")
    mycmake_try_set(DOXYGEN_VERBATIM_HEADERS "NO")
    mycmake_try_set(DOXYGEN_OUTPUT_DIRECTORY "${NAME}")
    mycmake_try_set(DOXYGEN_HTML_OUTPUT "html")

    _mycmake_doxygen_add_target(${NAME} ${ARGN})
endmacro(mycmake_doxygen_add_generic_target)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_doxygen_add_target

    Configures a target to generate doxygen documentation.

    .. code-block:: cmake

        mycmake_doxygen_add_target(
            <NAME>
            [NO_DOC]
            [NO_INSTALL]
            [ARGN]
            )

        It wraps the standard command :cmake:command:`doxygen_add_docs()`
        provided by :cmake:module:`FindDoxygen` module.

    Parameters:

    * ``<NAME>``: This custom target name
    * ``NO_DOC``: Do not add this custom target to default target
        :cmake:variable:`MYCMAKE_DOC_TARGET_NAME`
    * ``NO_INSTALL``: Do not add output dir to install.It will be installed in
        :cmake:variable:`CMAKE_INSTALL_DOCDIR`/${NAME}
    * ``ARGN``: normal parameters for :cmake:command:`add_custom_target()`
#]=======================================================================]
macro(mycmake_doxygen_add_target NAME)
    _mycmake_doxygen_add_target(${NAME} ${ARGN})
endmacro(mycmake_doxygen_add_target)

# EOF
