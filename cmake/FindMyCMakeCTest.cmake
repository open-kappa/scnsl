# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeCTest
----------------

Support module for tests

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeCTest_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeCTest_VERSION

    This module version

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_ctest_add_test()`
- :cmake:command:`mycmake_ctest_check_script_parameters()`
- :cmake:command:`mycmake_ctest_configure()`
- :cmake:command:`mycmake_ctest_configure_package()`
- :cmake:command:`mycmake_ctest_set_ld_library_path()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)

set(MyCMakeCTest_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeCTest
    FOUND_VAR
        MyCMakeCTest_FOUND
    REQUIRED_VARS
        MyCMakeBase_FOUND
        MyCMakeCTest_VERSION
    VERSION_VAR
        MyCMakeCTest_VERSION
)

if (MyCMakeCTest_FOUND)
    mycmake_include(CTest)
    mark_as_advanced(BUILD_TESTING)
    mycmake_set_default_policies()
    # @TODO: check and manage properly
    set(CTEST_CONFIGURATION_TYPE "${CMAKE_BUILD_TYPE}")
endif (MyCMakeCTest_FOUND)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_ctest_add_test

    Adds a test.

    .. code-block:: cmake

        mycmake_ctest_add_test(
            [NAME <name>]
            COMMAND <commands...>
            [SOURCE_DIR <dir>]
            [BINARY_DIR <dir>]
            [INSTALL_DIR <dir>]
            [WORKING_DIRECTORY <dir>]
            [LOG_FILE <file>]
            [WILL_FAIL]
            [DEPENDS <test names...>]
            [CONFIGURATIONS <configs...>]
            [NO_PASS]
            [ENVIRONMENT <Var=Value...>]
            [LABELS <labels...>]
            )

    Additionally, wraps test execution to allow streams logging.
    If a CMAKE_COMMAND test script command is detected, then by default the test
    related variables are automatically passed to the called script. See the script
    TestRunner.in.cmake to see the passed variables.

    Sets the variables:

    .. cmake:variable:: MYCMAKE_CTEST_TEST_NAME

        Test name. Defaults to the name of containing dir
        (:cmake:variable:`CMAKE_CURRENT_LIST_DIR`)

    .. cmake:variable:: MYCMAKE_CTEST_TEST_SOURCE_DIR

        Source test dir. Defaults to :cmake:variable:`CMAKE_CURRENT_LIST_DIR`

    .. cmake:variable:: MYCMAKE_CTEST_TEST_BINARY_DIR

        Binary test dir. Defaults to a directoy with same name and relative path
        of this test package, inside the binary dir.

    .. cmake:variable:: MYCMAKE_CTEST_TEST_INSTALL_DIR

        Install test dir, usually as relative path.
        Defaults to :cmake:variable:`MYCMAKE_CTEST_TEST_NAME`

    .. cmake:variable:: MYCMAKE_CTEST_TEST_LOG_FILE

        Current log file.
        Defaults to ${BINARY_DIR}/:cmake:variable:`MYCMAKE_CTEST_DEFAULT_LOG_FILE_NAME`

    Parameters:

    * ``NAME <name>``: The test name
    * ``COMMAND <commands...>``: The commands to execute as in add_test()
    * ``SOURCE_DIR <dir>``: The source dir
    * ``BINARY_DIR <dir>``: The binary dir
    * ``INSTALL_DIR <dir>``: The install dir
    * ``WORKING_DIRECTORY <dir>``: The working directory.
        Defaults to the binary dir.
    * ``LOG_FILE <file>``: Where to log. Empty string means no log.
    * ``WILL_FAIL``: The test is expected to fail
    * ``DEPENDS <test names...>``: Test dependencies, a both execution order
        and execution success
    * ``CONFIGURATIONS <configs...>``: The configurations as in add_test()
    * ``NO_PASS``: In case of :cmake:variable:`CMAKE_COMMAND` command, do not
        pass config variables
    * ``ENVIRONMENT <Var=Value...>``: List of environment variables to be set
        before running the test
    * ``LABELS <labels...>``: List of labels to attach to the test. As default,
        package label is always attached.
#]=======================================================================]
function(mycmake_ctest_add_test)
    cmake_parse_arguments(options
        "WILL_FAIL;NO_PASS"
        "NAME;LOG_FILE;WORKING_DIRECTORY;SOURCE_DIR;BINARY_DIR;INSTALL_DIR"
        "DEPENDS;COMMAND;CONFIGURATIONS;ENVIRONMENT;LABELS"
        ${ARGN}
        )
    file(RELATIVE_PATH REL_SRC_DIR ${MYCMAKE_CTEST_PACKAGE_SOURCE_DIR} ${CMAKE_CURRENT_LIST_DIR})
    # Parsing options:
    if (NOT options_NAME)
        set(options_NAME ${REL_SRC_DIR})
    endif (NOT options_NAME)

    if (NOT options_SOURCE_DIR)
        set(options_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR})
    endif (NOT options_SOURCE_DIR)

    if (NOT options_BINARY_DIR)
        file(RELATIVE_PATH REL_BIN_DIR ${MYCMAKE_CTEST_PACKAGE_SOURCE_DIR} ${options_SOURCE_DIR})
        set(options_BINARY_DIR "${MYCMAKE_CTEST_PACKAGE_BINARY_DIR}/${REL_BIN_DIR}")
    endif (NOT options_BINARY_DIR)

    if (NOT options_INSTALL_DIR)
        set(options_INSTALL_DIR "${options_NAME}")
    endif (NOT options_INSTALL_DIR)

    if (NOT options_WORKING_DIRECTORY)
        set (WORKING_DIRECTORY "${options_BINARY_DIR}")
    endif (NOT options_WORKING_DIRECTORY)

    if (NOT options_LOG_FILE)
        set(LOG_FILE "${options_BINARY_DIR}/${MYCMAKE_CTEST_DEFAULT_LOG_FILE_NAME}")
    elseif (NOT ("${options_LOG_FILE}" STREQUAL ""))
        set (LOG_FILE "${options_LOG_FILE}")
    endif (NOT options_LOG_FILE)

    set(CONFIGURATIONS)
    if (options_CONFIGURATIONS)
        set (CONFIGURATIONS "CONFIGURATIONS ${options_CONFIGURATIONS}")
    endif (options_CONFIGURATIONS)

    if (NOT options_COMMAND)
        mycmake_error_message("Missing required COMMAND tag in mycmake_ctest_add_test()")
    endif (NOT options_COMMAND)
    set(COMMAND ${options_COMMAND})

    set (NO_PASS ON)
    if (NOT options_NO_PASS)
        if (("${CMAKE_COMMAND}" IN_LIST COMMAND) AND ("-P" IN_LIST COMMAND))
            set(NO_PASS OFF)
            list(REMOVE_ITEM COMMAND ${CMAKE_COMMAND})
        endif (("${CMAKE_COMMAND}" IN_LIST COMMAND) AND ("-P" IN_LIST COMMAND))
    endif (NOT options_NO_PASS)

    # Exporting support vars:
    set(MYCMAKE_CTEST_TEST_NAME "${options_NAME}")
    set(MYCMAKE_CTEST_TEST_NAME "${MYCMAKE_CTEST_TEST_NAME}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_TEST_SOURCE_DIR "${options_SOURCE_DIR}")
    set(MYCMAKE_CTEST_TEST_SOURCE_DIR "${MYCMAKE_CTEST_TEST_SOURCE_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_TEST_BINARY_DIR "${options_BINARY_DIR}")
    set(MYCMAKE_CTEST_TEST_BINARY_DIR "${MYCMAKE_CTEST_TEST_BINARY_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_TEST_INSTALL_DIR "${options_INSTALL_DIR}")
    set(MYCMAKE_CTEST_TEST_INSTALL_DIR "${MYCMAKE_CTEST_TEST_INSTALL_DIR}" PARENT_SCOPE)
    if (LOG_FILE)
        set(MYCMAKE_CTEST_TEST_LOG_FILE "${LOG_FILE}")
        set(MYCMAKE_CTEST_TEST_LOG_FILE "${MYCMAKE_CTEST_TEST_LOG_FILE}" PARENT_SCOPE)
    endif (LOG_FILE)

    # Adding wrapper test:
    add_test(
        NAME
            ${options_NAME}
        COMMAND
            ${CMAKE_COMMAND} -Wdev -Werror=dev -P ${options_BINARY_DIR}/TestRunner.cmake
        WORKING_DIRECTORY
            ${options_WORKING_DIRECTORY}
        ${CONFIGURATIONS}
        )
    # Setting test dependencies:
    if (options_DEPENDS)
        set_tests_properties(${options_NAME} PROPERTIES DEPENDS ${options_DEPENDS})
        set_tests_properties(${options_NAME} PROPERTIES FIXTURES_REQUIRED ${options_DEPENDS})
    endif (options_DEPENDS)
    # Managing WILL_FAIL property:
    if (options_WILL_FAIL)
        mycmake_debug_message("Test ${options_NAME} is expected to fail")
        set_tests_properties(${options_NAME} PROPERTIES WILL_FAIL ON)
    endif (options_WILL_FAIL)
    # Setting envitonment:
    if (options_ENVIRONMENT)
        set_tests_properties(${options_NAME} PROPERTIES ENVIRONMENT "${options_ENVIRONMENT}")
    endif (options_ENVIRONMENT)

    # Setting labels:
    if (MYCMAKE_CTEST_PACKAGE_NAME)
        set(LABELS "${MYCMAKE_CTEST_PACKAGE_NAME}")
    endif (MYCMAKE_CTEST_PACKAGE_NAME)
    if (options_LABELS)
        set(LABELS ${LABELS} ${options_LABELS})
    endif (options_LABELS)
    if (LABELS)
        set_tests_properties(${options_NAME} PROPERTIES LABELS "${LABELS}")
    endif (LABELS)

    # Ensuring test dir:
    file(MAKE_DIRECTORY "${options_WORKING_DIRECTORY}")

    # Configuring test runner:
    configure_file(
        ${MYCMAKE_SCRIPTS_EXTRA_DIR}/TestRunner.in.cmake
        ${options_BINARY_DIR}/TestRunner.cmake
        @ONLY
    )
endfunction(mycmake_ctest_add_test)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_ctest_check_script_parameters

    For all variables, check they are defined.

    .. code-block:: cmake

        mycmake_ctest_check_script_parameters([ARGN])

    Parameters:

    * ``ARGN``: The list of variables to be checked.
#]=======================================================================]
function(mycmake_ctest_check_script_parameters)
    foreach (VAR ${ARGN})
        if ("${${VAR}}" STREQUAL "")
            mycmake_error_message("Variable ${VAR} undefined")
        endif ("${${VAR}}" STREQUAL "")
    endforeach (VAR ${ARGN})
endfunction(mycmake_ctest_check_script_parameters)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_ctest_configure

    Basic configuration for regression tests.

    .. code-block:: cmake

        mycmake_ctest_configure([SOURCE_DIR <dir>] [BINARY_DIR <dir>])

    Sets the variables:

    .. cmake:variable:: MYCMAKE_CTEST_SOURCE_DIR

        Path to root source dir for regression.
        Defaults to :cmake:variable:`MYCMAKE_CTEST_DEFAULT_SOURCE_DIR`

    .. cmake:variable:: MYCMAKE_CTEST_BINARY_DIR

        Path to root build dir for regression.
        Defaults to :cmake:variable:`MYCMAKE_CTEST_DEFAULT_BINARY_DIR`

    Parameters:

    * ``SOURCE_DIR <dir>``: The source dir
    * ``BINARY_DIR <dir>``: The binary dir
#]=======================================================================]
function(mycmake_ctest_configure)
    cmake_parse_arguments(options "" "SOURCE_DIR;BINARY_DIR" "" ${ARGN})
    if (NOT options_SOURCE_DIR)
        set(options_SOURCE_DIR ${MYCMAKE_CTEST_DEFAULT_SOURCE_DIR})
    endif (NOT options_SOURCE_DIR)
    if (NOT options_BINARY_DIR)
        set(options_BINARY_DIR ${MYCMAKE_CTEST_DEFAULT_BINARY_DIR})
    endif (NOT options_BINARY_DIR)
    set(MYCMAKE_CTEST_SOURCE_DIR "${options_SOURCE_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_BINARY_DIR "${options_BINARY_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_PACKAGE_SOURCE_DIR "${options_SOURCE_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_PACKAGE_BINARY_DIR "${options_BINARY_DIR}" PARENT_SCOPE)
endfunction(mycmake_ctest_configure)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_ctest_configure_package

    Basic configuration for regression package.

    .. code-block:: cmake

        mycmake_ctest_configure_package(
            [NAME <name>]
            [SOURCE_DIR <dir>]
            [BINARY_DIR <dir>]
            )

    Sets the variables:

    .. cmake:variable:: MYCMAKE_CTEST_PACKAGE_NAME

        Current package name.
        Defaults to the current directory name prefixed with upper-packages names.

    .. cmake:variable:: MYCMAKE_CTEST_PACKAGE_SOURCE_DIR

        Path to root source package dir.
        Defaults to :cmake:variable:`CMAKE_CURRENT_LIST_DIR`

    .. cmake:variable:: MYCMAKE_CTEST_PACKAGE_BINARY_DIR

        Path to root binary package dir.
        Defaults to a directoy with same name of :cmake:variable:`CMAKE_CURRENT_LIST_DIR`
        into :cmake:variable:`MYCMAKE_CTEST_PACKAGE_BINARY_DIR`

    Parameters:

    * ``NAME <name>``: The package name.
    * ``SOURCE_DIR <dir>``: The source dir.
    * ``BINARY_DIR <dir>``: The binary dir.
#]=======================================================================]
function(mycmake_ctest_configure_package)
    cmake_parse_arguments(options "" "NAME;SOURCE_DIR;BINARY_DIR" "" ${ARGN})
    if (NOT options_NAME)
        get_filename_component(TMP ${CMAKE_CURRENT_LIST_DIR} NAME)
        if (MYCMAKE_CTEST_PACKAGE_NAME)
            set(options_NAME "${MYCMAKE_CTEST_PACKAGE_NAME}_${TMP}")
        else (MYCMAKE_CTEST_PACKAGE_NAME)
            set(options_NAME "${TMP}")
        endif (MYCMAKE_CTEST_PACKAGE_NAME)
    endif (NOT options_NAME)
    if (NOT options_SOURCE_DIR)
        set(options_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR})
    endif (NOT options_SOURCE_DIR)
    if (NOT options_BINARY_DIR)
        file(RELATIVE_PATH REL_SRC_DIR ${MYCMAKE_CTEST_PACKAGE_SOURCE_DIR} ${CMAKE_CURRENT_LIST_DIR})
        set(options_BINARY_DIR ${MYCMAKE_CTEST_PACKAGE_BINARY_DIR}/${REL_SRC_DIR})
    endif (NOT options_BINARY_DIR)
    set(MYCMAKE_CTEST_PACKAGE_NAME "${options_NAME}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_PACKAGE_SOURCE_DIR "${options_SOURCE_DIR}" PARENT_SCOPE)
    set(MYCMAKE_CTEST_PACKAGE_BINARY_DIR "${options_BINARY_DIR}" PARENT_SCOPE)
endfunction(mycmake_ctest_configure_package)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_ctest_set_ld_library_path

    Sets passed variable to previous target shared libraries install dir.

    .. code-block:: cmake

        mycmake_ctest_set_ld_library_path(<VAR>)

    Parameters:

    * ``<VAR>``: The variable to set
#]=======================================================================]
function(mycmake_ctest_set_ld_library_path VAR)
    set(TMP "${MYCMAKE_CTEST_TEST_BINARY_DIR}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}/${MYCMAKE_CTEST_TEST_INSTALL_DIR}/${MYCMAKE_OS_SHARED_LIBRARY_INSTALL_DIR}")
    file(TO_NATIVE_PATH "${TMP}" TMP)
    set(${VAR} "${TMP}" PARENT_SCOPE)
endfunction(mycmake_ctest_set_ld_library_path)

# EOF
