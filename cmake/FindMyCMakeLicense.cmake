# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeLicense
------------------

When included, print to the console informations about the MyCMake license.
Moreover, provides a target to display licence-related further information.

This is an internal module, useless for clients.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeLicense_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeLicense_VERSION

    This module version

Targets
^^^^^^^

- :cmake:command:`mycmake-show-license`

    Prints MyCMake license to standard output

#]=======================================================================]
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeGlobals)
find_package(MyCMakePolicy)

set(MyCMakeLicense_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeLicense
    FOUND_VAR
        MyCMakeLicense_FOUND
    REQUIRED_VARS
        MyCMakePolicy_FOUND
        MyCMakeGlobals_FOUND
        MyCMakeLicense_VERSION
    VERSION_VAR
        MyCMakeLicense_VERSION
)

if (MyCMakeLicense_FOUND)
    mycmake_set_default_policies()
endif (MyCMakeLicense_FOUND)

message("#####################################################################")
message("# MyCMake")
message("# © 2019 Francesco Stefanni.")
message("# https://gitlab.com/open-kappa/mycmake")
message("#")
message("# MyCMake is distributed under the MIT license.")
message("# Please see the license file or execute the \"mycmake-show-license\"")
message("# target to display the license.")
message("#####################################################################")

if ((NOT (TARGET mycmake-show-license)) AND (NOT CMAKE_SCRIPT_MODE_FILE))
    add_custom_target(mycmake-show-license
        COMMAND ${CMAKE_COMMAND} -E echo
        COMMAND ${CMAKE_COMMAND} -E echo "Copyright \\(c\\) 2019, Francesco Stefanni."
        COMMAND ${CMAKE_COMMAND} -E echo ""
        COMMAND ${CMAKE_COMMAND} -E echo "Permission is hereby granted, free of charge, to any person"
        COMMAND ${CMAKE_COMMAND} -E echo "obtaining a copy of this software and associated documentation"
        COMMAND ${CMAKE_COMMAND} -E echo "files \\(the \"Software\"\\), to deal in the Software without"
        COMMAND ${CMAKE_COMMAND} -E echo "restriction, including without limitation the rights to use,"
        COMMAND ${CMAKE_COMMAND} -E echo "copy, modify, merge, publish, distribute, sublicense, and/or sell"
        COMMAND ${CMAKE_COMMAND} -E echo "copies of the Software, and to permit persons to whom the"
        COMMAND ${CMAKE_COMMAND} -E echo "Software is furnished to do so, subject to the following"
        COMMAND ${CMAKE_COMMAND} -E echo "conditions:"
        COMMAND ${CMAKE_COMMAND} -E echo ""
        COMMAND ${CMAKE_COMMAND} -E echo "The above copyright notice and this permission notice shall be"
        COMMAND ${CMAKE_COMMAND} -E echo "included in all copies or substantial portions of the Software."
        COMMAND ${CMAKE_COMMAND} -E echo ""
        COMMAND ${CMAKE_COMMAND} -E echo "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,"
        COMMAND ${CMAKE_COMMAND} -E echo "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES"
        COMMAND ${CMAKE_COMMAND} -E echo "OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND"
        COMMAND ${CMAKE_COMMAND} -E echo "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT"
        COMMAND ${CMAKE_COMMAND} -E echo "HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,"
        COMMAND ${CMAKE_COMMAND} -E echo "WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING"
        COMMAND ${CMAKE_COMMAND} -E echo "FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR"
        COMMAND ${CMAKE_COMMAND} -E echo "OTHER DEALINGS IN THE SOFTWARE."
        COMMAND ${CMAKE_COMMAND} -E echo ""
        USES_TERMINAL
    )
endif ((NOT (TARGET mycmake-show-license)) AND (NOT CMAKE_SCRIPT_MODE_FILE))

# EOF
