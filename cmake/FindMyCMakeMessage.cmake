# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeMessage
------------------

Provides support message routines.
This is an inernal module, please prefer to include the
:cmake:module:`MyCMakeBase` to use these routines.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeMessage_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeMessage_VERSION

    This module version

Variables
^^^^^^^^^

.. cmake:variable:: MYCMAKE_DEBUG_MESSAGES
    Enables debug messages and checks. Default is OFF.

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_assert()`
- :cmake:command:`mycmake_assert_debug()`
- :cmake:command:`mycmake_debug_message()`
- :cmake:command:`mycmake_debug_error_message()`
- :cmake:command:`mycmake_error_message()`
- :cmake:command:`mycmake_info_message()`
- :cmake:command:`mycmake_unique_warning_message()`
- :cmake:command:`mycmake_warning_message()`

#]=======================================================================]
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeGlobals)
find_package(MyCMakePolicy)

set(MyCMakeMessage_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeMessage
    FOUND_VAR
        MyCMakeMessage_FOUND
    REQUIRED_VARS
        MyCMakeGlobals_FOUND
        MyCMakePolicy_FOUND
        MyCMakeMessage_VERSION
    VERSION_VAR
        MyCMakeMessage_VERSION
)

if (MyCMakeMessage_FOUND)
    mycmake_set_default_policies()
    mycmake_try_option(MYCMAKE_DEBUG_MESSAGES "Enables debug messages and checks" OFF MYCMAKE_ADVANCED)
endif (MyCMakeMessage_FOUND)

function(_mycmake_error_message)
    # REMINDER: when changing this implementtion, update the one
    # into FindMyCMakePolicy.cmake
    message(FATAL_ERROR "-- ERROR: " ${ARGN})
endfunction(_mycmake_error_message)

function(_mycmake_warning_message)
    message(WARNING "-- WARNING: " ${ARGN})
endfunction(_mycmake_warning_message)

function(_mycmake_debug_error_message)
    if (${MYCMAKE_DEBUG_MESSAGES})
        _mycmake_error_message(${ARGN})
    else (${MYCMAKE_DEBUG_MESSAGES})
        _mycmake_warning_message(${ARGN})
    endif (${MYCMAKE_DEBUG_MESSAGES})
endfunction(_mycmake_debug_error_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_assert

    Prints an error message in case of failing condition.

    .. code-block:: cmake

        mycmake_assert(FLAG [ARGN])

    Parameters:

    * ``FLAG``: The boolean condition for the assert
    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_assert FLAG)
    if (${FLAG})
        return()
    endif (${FLAG})
    _mycmake_error_message(${ARGN})
endfunction(mycmake_assert)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_assert_debug

    Prints a message in case of failing condition, calling
    :cmake:command:`mycmake_debug_error_message()`.

    .. code-block:: cmake

        mycmake_assert_debug(<FLAG> [ARGN])

    Parameters:

    * ``<FLAG>``: The boolean condition for the assert
    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_assert_debug FLAG)
    if (${FLAG})
        return()
    endif (${FLAG})
    _mycmake_debug_error_message(${ARGN})
endfunction(mycmake_assert_debug)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_debug_message

    Prints a debug message.
    The print is performed only if :cmake:variable:`MYCMAKE_DEBUG_MESSAGES` is ON.

    .. code-block:: cmake

        mycmake_debug_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_debug_message)
    if (${MYCMAKE_DEBUG_MESSAGES})
        message(STATUS "DEBUG: " ${ARGN})
    endif (${MYCMAKE_DEBUG_MESSAGES})
endfunction(mycmake_debug_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_debug_error_message

    Prints a warning message if :cmake:variable:`MYCMAKE_DEBUG_MESSAGES` is OFF,
    an error otherwise.

    .. code-block:: cmake

        mycmake_debug_error_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_debug_error_message)
    _mycmake_debug_error_message(${ARGN})
endfunction(mycmake_debug_error_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_error_message

    Prints an error message.

    .. code-block:: cmake

        mycmake_error_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_error_message)
    # REMINDER: when changing this implementtion, update the one
    # into FindMyCMakePolicy.cmake
    _mycmake_error_message(${ARGN})
endfunction(mycmake_error_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_info_message

    Prints an info message.

    .. code-block:: cmake

        mycmake_info_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_info_message)
    message("-- INFO: " ${ARGN})
endfunction(mycmake_info_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_unique_warning_message

    Prints a warning message, only once.

    .. code-block:: cmake

        mycmake_unique_warning_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_unique_warning_message)
    set(MY_LIST ${mycmake_BASE_UNIQUE_WARNING})
    list(FIND MY_LIST "${ARGN}" RES)
    if ("${RES}" STREQUAL "-1")
        _mycmake_warning_message(${ARGN})
        list(APPEND MY_LIST "${ARGN}")
        set(mycmake_BASE_UNIQUE_WARNING ${MY_LIST} CACHE INTERNAL "MyCMake var for unique warning printing." FORCE)
    endif ("${RES}" STREQUAL "-1")
endfunction(mycmake_unique_warning_message)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_warning_message

    Prints a warning message.

    .. code-block:: cmake

        mycmake_warning_message([ARGN])

    Parameters:

    * ``ARGN``: The strings to be print
#]=======================================================================]
function(mycmake_warning_message)
    _mycmake_warning_message(${ARGN})
endfunction(mycmake_warning_message)

# EOF
