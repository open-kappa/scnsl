# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeMSVC
---------------

Internal :cmake:module:`FindMyCMakeCompiler` module.
Manages compiler flags for Microsoft VC++ compiler.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeMSVC_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeMSVC_VERSION

    This module version

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)

set(MyCMakeMSVC_VERSION "1.0.0")

find_package_handle_standard_args(
        MyCMakeMSVC
    FOUND_VAR
        MyCMakeMSVC_FOUND
    REQUIRED_VARS
        MyCMakeMSVC_VERSION
        MyCMakeBase_FOUND
    VERSION_VAR
        MyCMakeMSVC_VERSION
)


if (MyCMakeMSVC_FOUND)
    mycmake_set_default_policies()

    if (MYCMAKE_COMPILER_RUNTIME_CHECKS)
        mycmake_compiler_add_c_flag("/RTC" "Debug")
        mycmake_compiler_add_cxx_flag("/RTC" "Debug")
    endif (MYCMAKE_COMPILER_RUNTIME_CHECKS)

    if (MYCMAKE_COMPILER_WARNINGS_AS_ERRORS)
        mycmake_compiler_add_c_flag("/WX" "Debug")
        mycmake_compiler_add_cxx_flag("/WX" "Debug")
        mycmake_compiler_add_linker_flag("/WX" "ALL")
    endif (MYCMAKE_COMPILER_WARNINGS_AS_ERRORS)

    if (MYCMAKE_COMPILER_NO_WARNINGS)
        mycmake_compiler_add_c_flag("/w" "ALL")
        mycmake_compiler_add_cxx_flag("/w" "ALL")
    endif (MYCMAKE_COMPILER_NO_WARNINGS)

    # VC++2017

    # ##########################################################################
    # C FLAGS
    # ##########################################################################

    mycmake_compiler_add_c_flag("/Wall" "ALL")
    mycmake_compiler_add_c_flag("/EHsc" "ALL")
    mycmake_compiler_add_c_flag("/wd4464" "ALL")
    mycmake_compiler_add_c_flag("/wd4571" "ALL")
    mycmake_compiler_add_c_flag("/wd5045" "ALL")
    mycmake_compiler_add_c_flag("/wd4619" "ALL")
    mycmake_compiler_add_c_flag("/wd4435" "ALL")
    mycmake_compiler_add_c_flag("/wd4820" "Debug")
    mycmake_compiler_add_c_flag("/wd4820" "Release")
    mycmake_compiler_add_c_flag("/wd4820" "RelWithDebInfo")
    mycmake_compiler_add_c_flag("/experimental:external" "ALL")
    mycmake_compiler_add_c_flag("/external:W0" "ALL")
    mycmake_compiler_add_c_flag("/external:anglebrackets" "ALL")

    # ##########################################################################
    # C++ FLAGS
    # ##########################################################################

    mycmake_compiler_add_cxx_flag("/Wall" "ALL")
    mycmake_compiler_add_cxx_flag("/EHsc" "ALL")
    mycmake_compiler_add_cxx_flag("/wd4464" "ALL")
    mycmake_compiler_add_cxx_flag("/wd4571" "ALL")
    mycmake_compiler_add_cxx_flag("/wd5045" "ALL")
    mycmake_compiler_add_cxx_flag("/wd4619" "ALL")
    mycmake_compiler_add_cxx_flag("/wd4435" "ALL")
    mycmake_compiler_add_cxx_flag("/wd4820" "Debug")
    mycmake_compiler_add_cxx_flag("/wd4820" "Release")
    mycmake_compiler_add_cxx_flag("/wd4820" "RelWithDebInfo")
    mycmake_compiler_add_cxx_flag("/experimental:external" "ALL")
    mycmake_compiler_add_cxx_flag("/external:W0" "ALL")
    mycmake_compiler_add_cxx_flag("/external:anglebrackets" "ALL")

    # ##########################################################################
    # LINKER FLAGS
    # ##########################################################################

    mycmake_compiler_add_linker_flag("/OPT:NOREF" "ALL")

endif (MyCMakeMSVC_FOUND)

# EOF
