# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakePolicy
-----------------

Provides functionalities to fix CMake warnings.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakePolicy_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakePolicy_VERSION

    This module version

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_get_policy()`
- :cmake:command:`mycmake_set_default_policies()`
- :cmake:command:`mycmake_set_policy()`

#]=======================================================================]
cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeGlobals)

set(MyCMakePolicy_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakePolicy
    FOUND_VAR
        MyCMakePolicy_FOUND
    REQUIRED_VARS
        MyCMakeGlobals_FOUND
        MyCMakePolicy_VERSION
    VERSION_VAR
        MyCMakePolicy_VERSION
)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_get_policy

    Gets the policy behavior, if the policy exists, otherwise returns "OLD".

    .. code-block:: cmake

        mycmake_get_policy(<POLICY> <OUT>)

    Parameters:

    * ``<POLICY>``: The policy (CMPxxxx)
    * ``<OUT>``: The output variable
#]=======================================================================]
macro(mycmake_get_policy POLICY OUT)
    set(TMP "OLD")
    if (POLICY ${POLICY})
        cmake_policy(GET ${POLICY} TMP)
    endif (POLICY ${POLICY})
    set(${OUT} ${TMP})
    unset(TMP)
endmacro(mycmake_get_policy)


#[=======================================================================[.rst:
.. cmake:command:: mycmake_set_default_policies

    Sets default policies.
    Performs also some settings and checks.

    .. code-block:: cmake

        mycmake_set_default_policies()

#]=======================================================================]
macro(mycmake_set_default_policies)
    if (POLICY CMP0010)
        # Bad variable reference syntax is an error? New = Yes, Old = Warning
        # See also CMP0053
        cmake_policy(SET CMP0010 NEW)
    endif (POLICY CMP0010)

    if (POLICY CMP0020)
        # OLD not automatic link to qtmain.lib, NEW autolink on Windows
        cmake_policy(SET CMP0020 NEW)
    endif (POLICY CMP0020)

    if (POLICY CMP0022)
        # INTERFACE_LINK_LIBRARIES defines the link interface.
        # New is recursive in calculating libs dependencies
        cmake_policy(SET CMP0022 NEW)
    endif (POLICY CMP0022)

    if (POLICY CMP0048)
        # project() supports VERSION specifier
        cmake_policy(SET CMP0048 NEW)
        if ((PROJECT_NAME) AND (NOT PROJECT_VERSION))
            message(FATAL_ERROR "-- ERROR: Missing PROJECT_VERSION variable.")
        endif ((PROJECT_NAME) AND (NOT PROJECT_VERSION))
    endif (POLICY CMP0048)

    if (POLICY CMP0053)
        # Simplify variable reference and escape sequence evaluation
        # New stricter evaluation mode and var names
        # See also CMP0010
        cmake_policy(SET CMP0053 NEW)
    endif (POLICY CMP0053)

    if (POLICY CMP0054)
        # Vars are not more automatically dereferenced
        cmake_policy(SET CMP0054 NEW)
    endif (POLICY CMP0054)

    if (POLICY CMP0057)
        # Consider IN_LIST tag inside if() as an operator. NEW = Yes, OLD = No
        cmake_policy(SET CMP0057 NEW)
    endif (POLICY CMP0057)

    if (POLICY CMP0063)
        # Consider <LANG>_VISIBILITY_PRESET property for all kind of targets?
        # NEW = Yes, OLD = No (depends on other factors)
        cmake_policy(SET CMP0063 NEW)
    endif (POLICY CMP0063)
endmacro(mycmake_set_default_policies)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_set_policy

    Sets the policy behavior, if the policy exists.

    .. code-block:: cmake

        mycmake_set_policy(<POLICY> <BEHAVIOR>)

    Parameters:

    * ``<POLICY>``: The policy (CMPxxxx)
    * ``<BEHAVIOR>``: The behavior: OLD or NEW
#]=======================================================================]
macro(mycmake_set_policy POLICY BEHAVIOR)
    if (POLICY ${POLICY})
        cmake_policy(SET ${POLICY} ${BEHAVIOR})
    endif (POLICY ${POLICY})
endmacro(mycmake_set_policy)

# EOF
