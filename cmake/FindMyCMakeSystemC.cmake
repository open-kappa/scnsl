# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeSystemC
------------------

Wrapper of plain SyetemC module.

Used to force SystemC includes as SYSTEM includes, since they raise a lot of
warnings.

It simply uses :cmake:command:`find_package()` to locate the SystemC exported
module.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeSystemC_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeSystemC_VERSION
    This module version

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)
find_package(MyCMakeTargets)

# Checking third-parties dir:
if (MyCMakeTargets_FOUND)
    mycmake_find_package(SystemCLanguage CHECK_THIRD_PARTIES systemc)
else (MyCMakeTargets_FOUND)
    set(SystemCLanguage_FOUND OFF)
endif (MyCMakeTargets_FOUND)

set(MyCMakeSystemC_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeSystemC
    FOUND_VAR
        MyCMakeSystemC_FOUND
    REQUIRED_VARS
        MyCMakeBase_FOUND
        MyCMakeTargets_FOUND
        MyCMakeSystemC_VERSION
        SystemCLanguage_FOUND
    VERSION_VAR
        MyCMakeSystemC_VERSION
)

if (MyCMakeSystemC_FOUND)
    mycmake_set_default_policies()

    if (SystemCLanguage_THIRD_PARTIES_FOUND)
        set(BCK_SHARED ${BUILD_SHARED_LIBS})
        set({BCK_C_VISIBILITY} ${CMAKE_C_VISIBILITY_PRESET})
        set(BCK_CXX_VISIBILITY ${CMAKE_CXX_VISIBILITY_PRESET})
        # At the moment no hsared libs allowed on Windows
        if (MYCMAKE_OS_IS_WINDOWS)
            set(BUILD_SHARED_LIBS OFF)
        endif (MYCMAKE_OS_IS_WINDOWS)
        set(CMAKE_C_VISIBILITY_PRESET "default")
        set(CMAKE_CXX_VISIBILITY_PRESET "default")
        add_subdirectory(
            ${CMAKE_SOURCE_DIR}/${MYCMAKE_DEFAULT_THIRD_PARTIES_DIRECTORY}/systemc
            )
        set(BUILD_SHARED_LIBS ${BCK_SHARED})
        set(CMAKE_C_VISIBILITY_PRESET ${BCK_C_VISIBILITY})
        set(CMAKE_CXX_VISIBILITY_PRESET ${BCK_CXX_VISIBILITY})
        unset(TMP)
        set(SYSTEMC_TGT_NAME systemc)
    else (SystemCLanguage_THIRD_PARTIES_FOUND)
        set(SYSTEMC_TGT_NAME SystemC::systemc)
    endif (SystemCLanguage_THIRD_PARTIES_FOUND)

    # Setting SystemC dirs as SYSTEM:
    get_target_property(SYSTEMC_INC_DIRS SystemC::systemc INTERFACE_INCLUDE_DIRECTORIES)
    set_target_properties(${NAME} PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "")

    mycmake_target_include_directories(${SYSTEMC_TGT_NAME}
        SYSTEM
        INTERFACE ${SYSTEMC_INC_DIRS}
        )
    unset(SYSTEMC_INC_DIRS)
    unset(SYSTEMC_TGT_NAME)
endif (MyCMakeSystemC_FOUND)

# EOF
