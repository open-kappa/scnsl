# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeBase
----------------

Base setup for MyCMake scripts and base support functions.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeBase_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeBase_VERSION

    This module version

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_configure_project()`
- :cmake:command:`mycmake_merge_lists()`
- :cmake:command:`mycmake_set_cache()`
- :cmake:command:`mycmake_set_once()`
- :cmake:command:`mycmake_tag2version()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeGlobals)
find_package(MyCMakeSupportVariables)
find_package(MyCMakePolicy)
find_package(MyCMakeMessage)
find_package(MyCMakeLicense)

set(MyCMakeBase_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeBase
    FOUND_VAR
        MyCMakeBase_FOUND
    REQUIRED_VARS
        MyCMakeGlobals_FOUND
        MyCMakeSupportVariables_FOUND
        MyCMakePolicy_FOUND
        MyCMakeMessage_FOUND
        MyCMakeLicense_FOUND
        MyCMakeBase_VERSION
    VERSION_VAR
        MyCMakeBase_VERSION
)

if (MyCMakeBase_FOUND)
    mycmake_set_default_policies()
endif (MyCMakeBase_FOUND)

macro(_mycmake_add_custom_target TARGET OUTPUTNAME IS_DIRECTORY)
    # Adding target:
    add_custom_target(${TARGET}
        ${ARGN}
        )
    # Custom targets do not set the OUTPUT_NAME property.
    # Let's set it to be able to correctly install the target.
    set_target_properties(${TARGET}
        PROPERTIES OUTPUT_NAME "${OUTPUTNAME}"
        )
    get_target_property(VARS ${TARGET} LABELS)
    set_target_properties(${TARGET}
        PROPERTIES
            LABELS "MYCMAKE_IS_DIRECTORY_${IS_DIRECTORY};${VARS}"
        )
    unset(VARS)
endmacro(_mycmake_add_custom_target)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_configure_project

    Configures basic general options.

    .. code-block:: cmake

        mycmake_configure_project(
            [BUILD_TYPE <type>]
            [INSTALL_PATH <path>]
            [USE_DEBUG_POSTFIX]
            [USE_IWYU]
            )

    Parameters:

    * ``BUILD_TYPE <type>``: Default build type. Default is Release.
    * ``INSTALL_PATH <path>``: Default CMAKE_INSTALL_PATH value.
    * ``USE_DEBUG_POSTFIX``: Adds a "d" suffix to debug-compled libs
    * ``USE_IWYU``: Set compiling with using of include-what-you-use tool.
      In this case sets also ``MYCMAKE_IWYU`` global var to found tool.
    * ``NO_DOC``: Do not add default target :cmake:variable:`MYCMAKE_DOC_TARGET_NAME`
#]=======================================================================]
macro(mycmake_configure_project)
    cmake_parse_arguments(options
        "USE_IWYU;USE_DEBUG_POSTFIX;NO_DOC"
        "BUILD_TYPE;INSTALL_PATH;GENERATOR"
        ""
        ${ARGN}
    )
    if (NOT options_BUILD_TYPE)
        set(options_BUILD_TYPE "Release")
    endif (NOT options_BUILD_TYPE)

    if ((NOT CMAKE_BUILD_TYPE) AND (NOT CMAKE_CONFIGURATION_TYPES))
        set (CMAKE_BUILD_TYPE "Debug" CACHE STRING "Choose the type of build" FORCE)
        mycmake_debug_message("Setting CMAKE_BUILD_TYPE to '${CMAKE_BUILD_TYPE}' as none was specified.")
        set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
    endif ((NOT CMAKE_BUILD_TYPE) AND (NOT CMAKE_CONFIGURATION_TYPES))

    if (options_USE_DEBUG_POSTFIX)
        set(CMAKE_DEBUG_POSTFIX "d")
        if (CMAKE_BUILD_TYPE STREQUAL "Debug")
            set(MYCMAKE_CURRENT_POSTFIX "${CMAKE_DEBUG_POSTFIX}")
        else (CMAKE_BUILD_TYPE STREQUAL "Debug")
            set(MYCMAKE_CURRENT_POSTFIX "")
        endif (CMAKE_BUILD_TYPE STREQUAL "Debug")
    endif (options_USE_DEBUG_POSTFIX)

    # Set default install prefix
    if (options_INSTALL_PATH)
        if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
            set (CMAKE_INSTALL_PREFIX "${options_INSTALL_PATH}" CACHE PATH "Default install path" FORCE)
            mycmake_debug_message("Setting CMAKE_INSTALL_PREFIX to ${CMAKE_INSTALL_PREFIX}")
        endif(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    endif (options_INSTALL_PATH)

    if (options_USE_IWYU)
        find_program(MYCMAKE_IWYU NAMES include-what-you-use iwyu)
        if (MYCMAKE_IWYU)
            mycmake_info_message("Found include-what-you-use: ${MYCMAKE_IWYU}")
            set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${MYCMAKE_IWYU} PARENT_SCOPE)
            set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${MYCMAKE_IWYU} PARENT_SCOPE)
            set(MYCMAKE_IWYU ${MYCMAKE_IWYU} PARENT_SCOPE)
        else (MYCMAKE_IWYU)
            mycmake_error_message("Cannot find program include-what-you-use")
        endif (MYCMAKE_IWYU)
    endif (options_USE_IWYU)

    if (NOT options_NO_DOC)
        _mycmake_add_custom_target(${MYCMAKE_DOC_TARGET_NAME} ${MYCMAKE_DOC_TARGET_NAME} ON)
    endif (NOT options_NO_DOC)

    # @TODO include(CPack)

endmacro(mycmake_configure_project)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_merge_lists

    Appends unique elements in the second list into the first.

    .. code-block:: cmake

        mycmake_merge_lists(<OUT> <LIST> [PUSH_FRONT])

    Parameters:

    * ``<OUT>``: The output list
    * ``<LIST>``: The second list
    * ``PUSH_FRONT``: Insert elements in front instead of back.
#]=======================================================================]
function(mycmake_merge_lists OUT LIST)
    cmake_parse_arguments(options "PUSH_FRONT" "" "" ${ARGN})
    set(TMP ${${OUT}})
    set(NEW_LIST)
    foreach(E ${LIST})
        if (NOT (${E} IN_LIST TMP))
            set (NEW_LIST ${NEW_LIST} ${E})
        endif (NOT (${E} IN_LIST TMP))
    endforeach(E ${LIST})
    if (options_PUSH_FRONT)
        set (TMP ${NEW_LIST} ${TMP})
    else (options_PUSH_FRONT)
        set (TMP ${TMP} ${NEW_LIST})
    endif (options_PUSH_FRONT)
    set(${OUT} ${TMP} PARENT_SCOPE)
endfunction(mycmake_merge_lists)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_set_cache

    Sets a variable in cache, with override, even if already set and without
    FORCE flag.

    .. code-block:: cmake

        mycmake_set_cache(<NAME> [ARGN])

    Parameters:

    * ``<NAME>``: The variable name.
    * ``ARGN``: The new value.
#]=======================================================================]
function(mycmake_set_cache NAME)
    get_property(TYPE CACHE ${NAME} PROPERTY TYPE SET)
    get_property(HELPSTRING CACHE ${NAME} PROPERTY HELPSTRING SET)
    get_property(ADVANCED CACHE ${NAME} PROPERTY ADVANCED SET)
    get_property(STRINGS CACHE ${NAME} PROPERTY STRINGS SET)

    unset(${NAME} CACHE)
    if ("${TYPE}" STREQUAL "1")
        set(TYPE "STRING")
    endif ("${TYPE}" STREQUAL "1")

    set(${NAME} ${ARGN} CACHE ${TYPE} ${HELPSTRING})
    if (ADVANCED)
        mark_as_advanced(${NAME})
    endif (ADVANCED)
    if (STRINGS)
        set_property(CACHE ${NAME} PROPERTY STRINGS ${STRINGS})
    endif (STRINGS)
endfunction(mycmake_set_cache)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_set_once

    Sets a variable once. The syntax matches the set() command.

    .. code-block:: cmake

        mycmake_set_once(<VAR_NAME> <VALUE> [ARGN])

    Parameters:

    * ``<VAR_NAME>``: The variable name.
    * ``<VALUE>``: The variable value.
    * ``ARGN``: Variable configuration.
#]=======================================================================]
macro(mycmake_set_once VAR_NAME VALUE)
    set (mycmake_set_once_TMP_VAR ${VAR_NAME})
    set (mycmake_set_once_TMP_VALUE ${VALUE})
    if ("${mycmake_set_once_cache_${mycmake_set_once_TMP_VAR}}" STREQUAL "")
        set (mycmake_set_once_cache_${mycmake_set_once_TMP_VAR} "${mycmake_set_once_TMP_VAR} ${mycmake_set_once_TMP_VALUE}" CACHE INTERNAL "mycmake_set_once() cache" FORCE)
        set ("${mycmake_set_once_TMP_VAR}" "${mycmake_set_once_TMP_VALUE}" ${ARGN})
    endif ("${mycmake_set_once_cache_${mycmake_set_once_TMP_VAR}}" STREQUAL "")
    unset (mycmake_set_once_TMP_VAR)
    unset (mycmake_set_once_TMP_VALUE)
endmacro(mycmake_set_once)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_tag2version

    Translates a string tag to a binary version.

    .. code-block:: cmake

        mycmake_tag2version(<OUT> <TAG>)

    This function recognizes also the special tag "stable".
    As convention "stable" is mapped to "0,0,0,0".
    Always trunkates the name after the first "-", e.g. "1.2.3.4-donald-duck"
    becomes "1,2,3,4".

    Parameters:

    * ``<OUT>``: The output variable.
    * ``<TAG>``: {String} The input tag.
#]=======================================================================]
function(mycmake_tag2version OUT TAG)
    if ("${TAG}" STREQUAL "stable")
        set(${OUT} "0,0,0,0" PARENT_SCOPE)
        return()
    endif ("${TAG}" STREQUAL "stable")

    string(REGEX REPLACE "-.*" ".0" TMP "${TAG}")
    # Dots are not allowed, commas are used instead:
    string(REPLACE "." "," TMP "${TMP}")

    string(REPLACE "A" "65"  TMP "${TMP}")
    string(REPLACE "B" "66"  TMP "${TMP}")
    string(REPLACE "C" "67"  TMP "${TMP}")
    string(REPLACE "D" "68"  TMP "${TMP}")
    string(REPLACE "E" "69"  TMP "${TMP}")
    string(REPLACE "F" "70"  TMP "${TMP}")
    string(REPLACE "G" "71"  TMP "${TMP}")
    string(REPLACE "H" "72"  TMP "${TMP}")
    string(REPLACE "I" "73"  TMP "${TMP}")
    string(REPLACE "J" "74"  TMP "${TMP}")
    string(REPLACE "K" "75"  TMP "${TMP}")
    string(REPLACE "L" "76"  TMP "${TMP}")
    string(REPLACE "M" "77"  TMP "${TMP}")
    string(REPLACE "N" "78"  TMP "${TMP}")
    string(REPLACE "O" "79"  TMP "${TMP}")
    string(REPLACE "P" "80"  TMP "${TMP}")
    string(REPLACE "Q" "81"  TMP "${TMP}")
    string(REPLACE "R" "82"  TMP "${TMP}")
    string(REPLACE "S" "83"  TMP "${TMP}")
    string(REPLACE "T" "84"  TMP "${TMP}")
    string(REPLACE "U" "85"  TMP "${TMP}")
    string(REPLACE "V" "86"  TMP "${TMP}")
    string(REPLACE "W" "87"  TMP "${TMP}")
    string(REPLACE "X" "88"  TMP "${TMP}")
    string(REPLACE "Y" "89"  TMP "${TMP}")
    string(REPLACE "Z" "90"  TMP "${TMP}")

    string(REPLACE "a" "97"  TMP "${TMP}")
    string(REPLACE "b" "98"  TMP "${TMP}")
    string(REPLACE "c" "99"  TMP "${TMP}")
    string(REPLACE "d" "100" TMP "${TMP}")
    string(REPLACE "e" "101" TMP "${TMP}")
    string(REPLACE "f" "102" TMP "${TMP}")
    string(REPLACE "g" "103" TMP "${TMP}")
    string(REPLACE "h" "104" TMP "${TMP}")
    string(REPLACE "i" "105" TMP "${TMP}")
    string(REPLACE "j" "106" TMP "${TMP}")
    string(REPLACE "k" "107" TMP "${TMP}")
    string(REPLACE "l" "108" TMP "${TMP}")
    string(REPLACE "m" "109" TMP "${TMP}")
    string(REPLACE "n" "110" TMP "${TMP}")
    string(REPLACE "o" "111" TMP "${TMP}")
    string(REPLACE "p" "112" TMP "${TMP}")
    string(REPLACE "q" "113" TMP "${TMP}")
    string(REPLACE "r" "114" TMP "${TMP}")
    string(REPLACE "s" "115" TMP "${TMP}")
    string(REPLACE "t" "116" TMP "${TMP}")
    string(REPLACE "u" "117" TMP "${TMP}")
    string(REPLACE "v" "118" TMP "${TMP}")
    string(REPLACE "w" "119" TMP "${TMP}")
    string(REPLACE "x" "120" TMP "${TMP}")
    string(REPLACE "y" "121" TMP "${TMP}")
    string(REPLACE "z" "122" TMP "${TMP}")

    set(${OUT} "${TMP}" PARENT_SCOPE)
endfunction(mycmake_tag2version)

# EOF
