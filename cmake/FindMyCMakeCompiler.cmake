# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeCompiler
-------------------

Support of various C/C++ compilers with many enabled waring flags.
Internally loads the appropriate compiler-specific sub-module.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeCompiler_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeCompiler_VERSION

    This module version


Environment Variables
^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: MYCMAKE_C_ENABLED

    ON if CXX is enabled

.. cmake:variable:: MYCMAKE_CXX_ENABLED

    ON if CXX is enabled

Options
^^^^^^^

.. cmake:variable:: MYCMAKE_COMPILER_NO_WARNINGS

    Suppress all compiler warnings. Defailt is OFF.

.. cmake:variable:: MYCMAKE_COMPILER_RUNTIME_CHECKS

    Adds runtime traps to catch unsafe code. Default is OFF.

.. cmake:variable:: MYCMAKE_COMPILER_WARNINGS_AS_ERRORS

    Treats warnings as errors. Default is OFF.


Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`mycmake_compiler_add_c_flag()`
- :cmake:command:`mycmake_compiler_add_cxx_flag()`
- :cmake:command:`mycmake_compiler_add_linker_flag()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

include(CheckCCompilerFlag)
include(CheckCXXCompilerFlag)
include(CheckCLinkerFlag)
include(CheckCXXLinkerFlag)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)

set(MyCMakeCompiler_VERSION ${MYCMAKE_VERSION})

find_package_handle_standard_args(
        MyCMakeCompiler
    FOUND_VAR
        MyCMakeCompiler_FOUND
    REQUIRED_VARS
        MyCMakeCompiler_VERSION
        MyCMakeBase_FOUND
    VERSION_VAR
        MyCMakeCompiler_VERSION
)

if (MyCMakeCompiler_FOUND)
    mycmake_set_default_policies()

    # @TODO:
    # COMPILER_C_STANDARD
    # COMPILER_CXX_STANDARD
    # COMPILER_USE_PROFILER

endif (MyCMakeCompiler_FOUND)

mycmake_try_option(MYCMAKE_COMPILER_NO_WARNINGS "Suppress all compiler warnings." OFF MYCMAKE_ADVANCED)
mycmake_try_option(MYCMAKE_COMPILER_RUNTIME_CHECKS "Adds runtime traps to catch unsafe code." OFF MYCMAKE_ADVANCED)
mycmake_try_option(MYCMAKE_COMPILER_WARNINGS_AS_ERRORS "Treats warnings as errors." OFF MYCMAKE_ADVANCED)

function(_mycmake_compiler_check_enabledlanguages)
    get_property(CURRENT_LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)
    if ("C" IN_LIST CURRENT_LANGUAGES)
        set(MYCMAKE_C_ENABLED ON)
    else("C" IN_LIST CURRENT_LANGUAGES)
        set(MYCMAKE_C_ENABLED OFF)
    endif("C" IN_LIST CURRENT_LANGUAGES)
    if ("CXX" IN_LIST CURRENT_LANGUAGES)
        set(MYCMAKE_CXX_ENABLED ON)
    else("CXX" IN_LIST CURRENT_LANGUAGES)
        set(MYCMAKE_CXX_ENABLED OFF)
    endif("CXX" IN_LIST CURRENT_LANGUAGES)
    set(MYCMAKE_C_ENABLED ${MYCMAKE_C_ENABLED} PARENT_SCOPE)
    set(MYCMAKE_CXX_ENABLED ${MYCMAKE_CXX_ENABLED} PARENT_SCOPE)
endfunction(_mycmake_compiler_check_enabledlanguages)
_mycmake_compiler_check_enabledlanguages()


function(_mycmake_compiler_check_flag FLAG LANG SUPPORTED)
    if ("${FLAG}" STREQUAL "-Werror")
        # With CMake 3.10.3 fails... what a bug!
        set(${LANG}_FLAG${FLAG} ON)
        set(${LANG}_FLAG${FLAG} ON CHACHE INTERNAL "Workaround flag")
        set(${SUPPORTED} ${${LANG}_FLAG${FLAG}} PARENT_SCOPE)
        return()
    endif ("${FLAG}" STREQUAL "-Werror")

    if (${LANG} STREQUAL "C")
        check_c_compiler_flag(${FLAG} ${LANG}_FLAG${FLAG})
    elseif (${LANG} STREQUAL "CXX")
        check_cxx_compiler_flag(${FLAG} ${LANG}_FLAG${FLAG})
    else (${LANG} STREQUAL "C")
        mycmake_error_message("Unexpected language: ${LANG}")
    endif (${LANG} STREQUAL "C")
    set(${SUPPORTED} ${${LANG}_FLAG${FLAG}} PARENT_SCOPE)
endfunction(_mycmake_compiler_check_flag)


function(_mycmake_compiler_add_flag FLAG KIND LANG)
    cmake_parse_arguments(options "NO_CHECK" "" "" ${ARGN})
    if (NOT MYCMAKE_${LANG}_ENABLED)
        return()
    endif (NOT MYCMAKE_${LANG}_ENABLED)
    # check_${LANG}_compiler_flag sets its argument variable as an internal
    # cache var: so we can exploit it to avoid multiple adding of flags!
    if (DEFINED ${LANG}_FLAG${FLAG})
        return()
    endif (DEFINED ${LANG}_FLAG${FLAG})
    if (options_NO_CHECK)
        set(${LANG}_FLAG${FLAG} ON)
        set(${LANG}_FLAG${FLAG} ON CACHE INTERNAL "Workaround flag")
        set(SUPPORTED_FLAG ON)
    else (options_NO_CHECK)
        _mycmake_compiler_check_flag(${FLAG} ${LANG} SUPPORTED_FLAG)
    endif (options_NO_CHECK)
    if (NOT SUPPORTED_FLAG)
        return()
    endif (NOT SUPPORTED_FLAG)
    if (KIND STREQUAL "ALL")
        mycmake_set_cache(CMAKE_${LANG}_FLAGS "${CMAKE_${LANG}_FLAGS} ${FLAG}")
    elseif (KIND STREQUAL "Debug")
        mycmake_set_cache(CMAKE_${LANG}_FLAGS_DEBUG "${CMAKE_${LANG}_FLAGS_DEBUG} ${FLAG}")
    elseif (KIND STREQUAL "Release")
        mycmake_set_cache(CMAKE_${LANG}_FLAGS_RELEASE "${CMAKE_${LANG}_FLAGS_RELEASE} ${FLAG}")
    elseif (KIND STREQUAL "MinSizeRel")
        mycmake_set_cache(CMAKE_${LANG}_FLAGS_MINSIZEREL "${CMAKE_${LANG}_FLAGS_MINSIZEREL} ${FLAG}")
    elseif (KIND STREQUAL "RelWithDebInfo")
        mycmake_set_cache(CMAKE_${LANG}_FLAGS_RELWITHDEBINFO "${CMAKE_${LANG}_FLAGS_RELWITHDEBINFO} ${FLAG}")
    else (KIND STREQUAL "ALL")
        mycmake_error_message("Unknown compiler build kind: ${KIND}")
    endif (KIND STREQUAL "ALL")
endfunction(_mycmake_compiler_add_flag)


function(_mycmake_compiler_check_linker_flag FLAG LANG SUPPORTED)
    if (LANG STREQUAL "C")
        check_c_linker_flag(${FLAG} SUPPORTED_FLAG)
    elseif (LANG STREQUAL "CXX")
        check_cxx_linker_flag(${FLAG} SUPPORTED_FLAG)
    else (LANG STREQUAL "C")
        mycmake_error_message("Unexpected language: ${LANG}")
    endif (LANG STREQUAL "C")
    set(${SUPPORTED} ${SUPPORTED_FLAG} PARENT_SCOPE)
endfunction(_mycmake_compiler_check_linker_flag)


function(_mycmake_compiler_add_linker_flag FLAG KIND LANG)
    cmake_parse_arguments(options "NO_CHECK" "" "" ${ARGN})
    if (NOT MYCMAKE_${LANG}_ENABLED)
        return()
    endif (NOT MYCMAKE_${LANG}_ENABLED)
    if (NOT options_NO_CHECK)
        _mycmake_compiler_check_linker_flag(${FLAG} ${LANG} SUPPORTED_FLAG)
        if (NOT SUPPORTED_FLAG)
            return()
        endif (NOT SUPPORTED_FLAG)
    endif (NOT options_NO_CHECK)

    if (KIND STREQUAL "ALL")
        set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} ${FLAG})
        set(CMAKE_SHARED_LINKER_FLAGS ${CMAKE_SHARED_LINKER_FLAGS} ${FLAG})
        set(CMAKE_STATIC_LINKER_FLAGS ${CMAKE_STATIC_LINKER_FLAGS} ${FLAG})
        set(CMAKE_MODULE_LINKER_FLAGS ${CMAKE_MODULE_LINKER_FLAGS} ${FLAG})
    elseif (KIND STREQUAL "EXE")
        set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} ${FLAG})
    elseif (KIND STREQUAL "SHARED")
        set(CMAKE_SHARED_LINKER_FLAGS ${CMAKE_SHARED_LINKER_FLAGS} ${FLAG})
    elseif (KIND STREQUAL "STATIC")
        set(CMAKE_STATIC_LINKER_FLAGS ${CMAKE_STATIC_LINKER_FLAGS} ${FLAG})
    elseif (KIND STREQUAL "MODULE")
        set(CMAKE_MODULE_LINKER_FLAGS ${CMAKE_MODULE_LINKER_FLAGS} ${FLAG})
    else (KIND STREQUAL "ALL")
        mycmake_error_message("Unknown linker build kind: ${KIND}")
    endif (KIND STREQUAL "ALL")
endfunction(_mycmake_compiler_add_linker_flag)


#[=======================================================================[.rst:
.. cmake:command:: mycmake_compiler_add_c_flag

    Adds a compiler flag if it passes a check to assess whether it is supported
    by the compiler.

    .. code-block:: cmake

        mycmake_compiler_add_c_flag(<FLAG> <KIND> [NO_CHECK])

    Parameters:

    * ``<FLAG>``: The flag.
    * ``<KIND>``: One of: ALL, Debug, Release, RelWithDebInfo, MiSizeRel.
    * ``NO_CHECK``: Do not perform compiler check.
#]=======================================================================]
function(mycmake_compiler_add_c_flag FLAG KIND)
    _mycmake_compiler_add_flag(${FLAG} ${KIND} "C" ${ARGN})
endfunction(mycmake_compiler_add_c_flag FLAG KIND)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_compiler_add_cxx_flag

    Adds a compiler flag if it passes a check to assess whether it is supported
    by the compiler.

    .. code-block:: cmake

        mycmake_compiler_add_cxx_flag(<FLAG> <KIND> [NO_CHECK])

    Parameters:

    * ``<FLAG>``: The flag.
    * ``<KIND>``: One of: ALL, Debug, Release, RelWithDebInfo, MiSizeRel.
    * ``NO_CHECK``: Do not perform compiler check.
#]=======================================================================]
function(mycmake_compiler_add_cxx_flag FLAG KIND)
    _mycmake_compiler_add_flag(${FLAG} ${KIND} "CXX" ${ARGN})
endfunction(mycmake_compiler_add_cxx_flag FLAG KIND)

#[=======================================================================[.rst:
.. cmake:command:: mycmake_compiler_add_linker_flag

    Adds a compiler flag if it passes a check to assess whether it is supported
    by the compiler.

    .. code-block:: cmake

        mycmake_compiler_add_linker_flag(<FLAG> <KIND> [NO_CHECK])

    Parameters:

    * ``<FLAG>``: The flag.
    * ``<KIND>``: One of: ALL, EXE, SHARED, STATIC, MODULE.
    * ``NO_CHECK``: Do not perform compiler check.
#]=======================================================================]
function(mycmake_compiler_add_linker_flag FLAG KIND)
    if (MYCMAKE_C_ENABLED)
        _mycmake_compiler_add_linker_flag(${FLAG} ${KIND} "C" ${ARGN})
    elseif (MYCMAKE_CXX_ENABLED)
        _mycmake_compiler_add_linker_flag(${FLAG} ${KIND} "CXX" ${ARGN})
    else (MYCMAKE_C_ENABLED)
        mycmake_message_error("No supported language enabled")
    endif (MYCMAKE_C_ENABLED)
endfunction(mycmake_compiler_add_linker_flag FLAG KIND)


# @TODO
#option (ENABLE_GCC_SANITIZE "Enable gcc sanitize flag" OFF)
#if ((ENABLE_GCC_SANITIZE) AND ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux") AND ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug"))
#    add_compile_options(-fsanitize=address -fno-omit-frame-pointer)
#    set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} -fsanitize=address")
#endif ((ENABLE_GCC_SANITIZE) AND ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux") AND ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug"))
#if ((ENABLE_GCC_SANITIZE) AND ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux") AND ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug"))
#    target_link_libraries(${PROJECT_NAME} PUBLIC asan)
#endif ((ENABLE_GCC_SANITIZE) AND ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux") AND ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug"))


if (MyCMakeCompiler_FOUND)
    if ((CMAKE_C_COMPILER_ID STREQUAL "Clang") OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang"))
        find_package(MyCMakeCLang REQUIRED)
    elseif ((CMAKE_C_COMPILER_ID STREQUAL "GNU") OR (CMAKE_CXX_COMPILER_ID STREQUAL "GNU"))
        find_package(MyCMakeGcc REQUIRED)
    elseif ((CMAKE_C_COMPILER_ID STREQUAL "MSVC") OR (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC"))
        find_package(MyCMakeMSVC REQUIRED)
    elseif ((CMAKE_C_COMPILER_ID STREQUAL "") AND (CMAKE_CXX_COMPILER_ID STREQUAL ""))
        # Both empty means no C or C++ lang enabled: ok no problem :)
    else ((CMAKE_C_COMPILER_ID STREQUAL "Clang") OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang"))
        mycmake_debug_error_message("Unsupported compiler - C: ${CMAKE_C_COMPILER_ID}, C++: ${CMAKE_CXX_COMPILER_ID}")
    endif ((CMAKE_C_COMPILER_ID STREQUAL "Clang") OR (CMAKE_CXX_COMPILER_ID STREQUAL "Clang"))
endif (MyCMakeCompiler_FOUND)

# EOF
