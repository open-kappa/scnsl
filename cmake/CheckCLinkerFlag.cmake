# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
CheckCLinkerFlag
----------------

Tests the validity of linker flags for C language.

#]=======================================================================]

include_guard(GLOBAL)
include(CheckCSourceRuns)

#[=======================================================================[.rst:
.. cmake:command:: check_c_linker_flag

    Checks whether a linker flag is supported.

    .. code-block:: cmake

        check_c_linker_flag(<FLAG> <RESULT>)

    * ``<FLAG>``: the flag to be tested
    * ``<RESULT>``: the variable into which store the check result
#]=======================================================================]
function(check_c_linker_flag FLAG RESULT)
    set(CMAKE_REQUIRED_FLAGS "${FLAG}")
    set(VAR "${FLAG}")
    string(REPLACE ":" "_" VAR ${VAR})
    unset("${VAR}" CACHE)
    CHECK_C_SOURCE_RUNS("int main() { return 0; }" "${VAR}")
    if ("${VAR}")
        set(${RESULT} ON PARENT_SCOPE)
    else ("${VAR}")
        set(${RESULT} OFF PARENT_SCOPE)
    endif ("${VAR}")
    unset("${VAR}" CACHE)
endfunction(check_c_linker_flag)

# EOF
