# MyCMake https://gitlab.com/open-kappa/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/mycmake for details.

#[=======================================================================[.rst:
FindMyCMakeClang
-----------------

Internal :cmake:module:`FindMyCMakeCompiler` module.
Manages compiler flags for CLang compiler.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: MyCMakeClang_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: MyCMakeClang_VERSION

    This module version

#]=======================================================================]

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)
include_guard(GLOBAL)

find_package(PackageHandleStandardArgs)
find_package(MyCMakeBase)

set(MyCMakeClang_VERSION "1.0.0")

find_package_handle_standard_args(
        MyCMakeClang
    FOUND_VAR
        MyCMakeClang_FOUND
    REQUIRED_VARS
        MyCMakeClang_VERSION
        MyCMakeBase_FOUND
    VERSION_VAR
        MyCMakeClang_VERSION
)


if (MyCMakeClang_FOUND)
    mycmake_set_default_policies()

    if (MYCMAKE_COMPILER_RUNTIME_CHECKS)
        # @TODO: find an equivalent...
        #mycmake_compiler_add_c_flag("-D_GLIBCXX_DEBUG" "Debug")
        #mycmake_compiler_add_cxx_flag("-D_GLIBCXX_DEBUG" "Debug")
    endif (MYCMAKE_COMPILER_RUNTIME_CHECKS)

    if (MYCMAKE_COMPILER_WARNINGS_AS_ERRORS)
        mycmake_compiler_add_c_flag("-Werror" "Debug")
        mycmake_compiler_add_cxx_flag("-Werror" "Debug")
    endif (MYCMAKE_COMPILER_WARNINGS_AS_ERRORS)

    if (MYCMAKE_COMPILER_NO_WARNINGS)
        mycmake_compiler_add_c_flag("-w" "ALL")
        mycmake_compiler_add_cxx_flag("-w" "ALL")
    endif (MYCMAKE_COMPILER_NO_WARNINGS)

    # ##########################################################################
    # C FLAGS
    # ##########################################################################

    mycmake_compiler_add_c_flag("-Weverything" "ALL")
    mycmake_compiler_add_c_flag("-Wno-long-long" "ALL")

    # ##########################################################################
    # C++ FLAGS
    # ##########################################################################

    mycmake_compiler_add_cxx_flag("-Weverything" "ALL")
    mycmake_compiler_add_cxx_flag("-Wno-long-long" "ALL")

endif (MyCMakeClang_FOUND)

# EOF
