// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A basic filter.



#include "scnsl/tracing/BasicFilter_t.hh"


using namespace Scnsl::Tracing;


// ////////////////////////////////////////////////////////////////
// Regular design pattern.
// ////////////////////////////////////////////////////////////////


BasicFilter_t::BasicFilter_t(
            const level_t info,
            const level_t log,
            const level_t debug,
            const level_t warning,
            const level_t error,
            const level_t fatal ):
    // Parents:
    Filter_if_t(),
    // Fields:
    _info( info ),
    _log( log ),
    _debug( debug ),
    _warning( warning ),
    _error( error ),
    _fatal( fatal )
{
    // Nothing to do.
}

BasicFilter_t::BasicFilter_t( const BasicFilter_t & f ):
    // Parents:
    Filter_if_t( f ),
    // Fields:
    _info( f._info ),
    _log( f._log ),
    _debug( f._debug ),
    _warning( f._warning ),
    _error( f._error ),
    _fatal( f._fatal )
{
    // Nohting to do.
}

BasicFilter_t::~BasicFilter_t()
{
    // Nothing to do.
}

BasicFilter_t & BasicFilter_t::operator = ( const BasicFilter_t & f )
{
    Filter_if_t::operator = ( f );

    // Fields:
    _info = f._info;
    _log = f._log;
    _debug = f._debug;
    _warning = f._warning;
    _error = f._error;
    _fatal = f._fatal;

    return *this;
}


// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


bool BasicFilter_t::haveToTraceInfo( const trace_data_t & data )
    const
{
    return _info >= data.level;
}

bool BasicFilter_t::haveToTraceLog( const trace_data_t & data )
    const
{
    return _log >= data.level;
}

bool BasicFilter_t::haveToTraceDebug( const trace_data_t & data )
    const
{
    return _debug >= data.level;
}

bool BasicFilter_t::haveToTraceWarning( const trace_data_t & data )
    const
{
    return _warning >= data.level;
}

bool BasicFilter_t::haveToTraceError( const trace_data_t & data )
    const
{
    return _error >= data.level;
}

bool BasicFilter_t::haveToTraceFatal( const trace_data_t & data )
    const
{
    return _fatal >= data.level;
}
