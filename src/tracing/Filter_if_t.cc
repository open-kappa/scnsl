// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The filter interface.



#include "scnsl/tracing/Filter_if_t.hh"


using namespace Scnsl::Tracing;



// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

Filter_if_t::~Filter_if_t()
{
    //  Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Regular design pattern support.
// ////////////////////////////////////////////////////////////////

Filter_if_t::Filter_if_t()
{
    // Nothing to do.
}

Filter_if_t::Filter_if_t( const Filter_if_t & /*f*/ )
{}


Filter_if_t & Filter_if_t::operator = ( const Filter_if_t & /*f*/ )
{
	return *this;
}
