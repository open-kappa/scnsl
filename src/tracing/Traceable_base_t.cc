// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// An object which is traceable.



#include "scnsl/tracing/Traceable_base_t.hh"
#include "scnsl/tracing/Tracer_t.hh"

using namespace Scnsl::Tracing;



// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void Traceable_base_t::registerTracer( Tracer_t * t )
{
    _tracers.push_back( t );
}


// ////////////////////////////////////////////////////////////////
// Regular design pattern.
// ////////////////////////////////////////////////////////////////

Traceable_base_t::Traceable_base_t( const char * name ):
    // Fields:
    _tracers(),
    _data()
{
    _data.name = name;
}

Traceable_base_t::Traceable_base_t( const Traceable_base_t & t ):
    _tracers( t._tracers ),
    _data( t._data )
{
    // Nothing to do.
}


Traceable_base_t::~Traceable_base_t()
{
    // Nothing to do.
}


Traceable_base_t & Traceable_base_t::operator = ( const Traceable_base_t & t )
{
    _tracers = t._tracers;
    _data = t._data;

    return *this;
}



// ////////////////////////////////////////////////////////////////
// Internal tracing methods.
// ////////////////////////////////////////////////////////////////

void Traceable_base_t::traceInfo( const sc_core::sc_time & ts,
                                  const char * const filename,
                                  const unsigned int fileline,
                                  const level_t level,
                                  const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceInfo( _data );
    }
}



void Traceable_base_t::traceInfo( const sc_core::sc_time & ts,
                                  const char * const filename,
                                  const unsigned int fileline,
                                  const level_t level,
                                  const Scnsl::Core::Packet_t & packet,
                                  const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceInfo( _data );
    }
}



void Traceable_base_t::traceInfo( const sc_core::sc_time & ts,
                                  const char * const filename,
                                  const unsigned int fileline,
                                  const level_t level,
                                  const Scnsl::Core::Channel_if_t * channel,
                                  const Scnsl::Core::carrier_t & carrier,
                                  const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceInfo( _data );
    }
}



void Traceable_base_t::traceLog( const sc_core::sc_time & ts,
                                 const char * const filename,
                                 const unsigned int fileline,
                                 const level_t level,
                                 const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceLog( _data );
    }
}



void Traceable_base_t::traceLog( const sc_core::sc_time & ts,
                                 const char * const filename,
                                 const unsigned int fileline,
                                 const level_t level,
                                 const Scnsl::Core::Packet_t & packet,
                                 const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceLog( _data );
    }
}



void Traceable_base_t::traceLog( const sc_core::sc_time & ts,
                                 const char * const filename,
                                 const unsigned int fileline,
                                 const level_t level,
                                 const Scnsl::Core::Channel_if_t * channel,
                                 const Scnsl::Core::carrier_t & carrier,
                                 const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceLog( _data );
    }
}



void Traceable_base_t::traceDebug( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceDebug( _data );
    }
}



void Traceable_base_t::traceDebug( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Packet_t & packet,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceDebug( _data );
    }
}


void Traceable_base_t::traceDebug( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Channel_if_t * channel,
                                   const Scnsl::Core::carrier_t & carrier,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceDebug( _data );
    }
}



void Traceable_base_t::traceWarning( const sc_core::sc_time & ts,
                                     const char * const filename,
                                     const unsigned int fileline,
                                     const level_t level,
                                     const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceWarning( _data );
    }
}



void Traceable_base_t::traceWarning( const sc_core::sc_time & ts,
                                     const char * const filename,
                                     const unsigned int fileline,
                                     const level_t level,
                                     const Scnsl::Core::Packet_t & packet,
                                     const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceWarning( _data );
    }
}



void Traceable_base_t::traceWarning( const sc_core::sc_time & ts,
                                     const char * const filename,
                                     const unsigned int fileline,
                                     const level_t level,
                                     const Scnsl::Core::Channel_if_t * channel,
                                     const Scnsl::Core::carrier_t & carrier,
                                     const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceWarning( _data );
    }
}



void Traceable_base_t::traceError( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceError( _data );
    }
}



void Traceable_base_t::traceError( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Packet_t & packet,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceError( _data );
    }
}

void Traceable_base_t::traceError( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Channel_if_t * channel,
                                   const Scnsl::Core::carrier_t & carrier,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceError( _data );
    }
}



void Traceable_base_t::traceFatal( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceFatal( _data );
    }
}



void Traceable_base_t::traceFatal( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Packet_t & packet,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = & packet;
    _data.carrier = false;
    _data.channel = nullptr;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceFatal( _data );
    }
}



void Traceable_base_t::traceFatal( const sc_core::sc_time & ts,
                                   const char * const filename,
                                   const unsigned int fileline,
                                   const level_t level,
                                   const Scnsl::Core::Channel_if_t * channel,
                                   const Scnsl::Core::carrier_t & carrier,
                                   const char * const message )
    const
{
    _data.timestamp = ts;
    _data.filename = filename;
    _data.fileline = fileline;
    _data.level = level;
    _data.message = message;
    _data.packet = nullptr;
    _data.carrier = carrier;
    _data.channel = channel;

    for ( TracersList_t::iterator i = _tracers.begin();
          i != _tracers.end();
        ++ i )
    {
        (*i)->traceFatal( _data );
    }
}
