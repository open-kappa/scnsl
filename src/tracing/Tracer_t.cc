// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A tracer.


#include "scnsl/tracing/Filter_if_t.hh"
#include "scnsl/tracing/Formatter_if_t.hh"
#include "scnsl/tracing/Tracer_t.hh"


using namespace Scnsl::Tracing;

// ////////////////////////////////////////////////////////////////
// Constructor & destructor.
// ////////////////////////////////////////////////////////////////

Tracer_t::Tracer_t( Filter_if_t * filter, Formatter_if_t * formatter ):
    // Fields:
    _filter( filter ),
    _formatter( formatter )
{
    // Nothing ot do.
}

Tracer_t::~Tracer_t()
{
    delete _filter;
    delete _formatter;
}



// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void Tracer_t::addOutput( std::ostream * os )
{
    _formatter->addOutput( os );
}


void Tracer_t::trace( Traceable_base_t * traceable )
{
    traceable->registerTracer( this );
}


// ////////////////////////////////////////////////////////////////
// Tracing support methods.
// ////////////////////////////////////////////////////////////////

void Tracer_t::traceInfo( const trace_data_t & data )
{
    if ( _filter->haveToTraceInfo( data ) )
    {
        _formatter->traceInfo( data );
    }
}

void Tracer_t::traceLog( const trace_data_t & data )
{
    if ( _filter->haveToTraceLog( data ) )
    {
        _formatter->traceLog( data );
    }
}

void Tracer_t::traceDebug( const trace_data_t & data )
{
    if ( _filter->haveToTraceDebug( data ) )
    {
        _formatter->traceDebug( data );
    }
}

void Tracer_t::traceWarning( const trace_data_t & data )
{
    if ( _filter->haveToTraceWarning( data ) )
    {
        _formatter->traceWarning( data );
    }
}

void Tracer_t::traceError( const trace_data_t & data )
{
    if ( _filter->haveToTraceError( data ) )
    {
        _formatter->traceError( data );
    }
}

void Tracer_t::traceFatal( const trace_data_t & data )
{
    if ( _filter->haveToTraceFatal( data ) )
    {
        _formatter->traceFatal( data );
    }
}
