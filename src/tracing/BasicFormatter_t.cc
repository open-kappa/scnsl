// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A basic formatter.

#include <iostream>
#include <ostream>

#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/Channel_if_t.hh"
#include "scnsl/tracing/BasicFormatter_t.hh"


using namespace Scnsl::Tracing;


// ////////////////////////////////////////////////////////////////
// Regular design pattern.
// ////////////////////////////////////////////////////////////////


BasicFormatter_t::BasicFormatter_t():
    // Parents:
    Formatter_if_t(),
    // Fields:
    _print_trace_type( false ),
    _print_trace_timestamp ( false )

{
    // Nothing to do.
}


BasicFormatter_t::BasicFormatter_t( const BasicFormatter_t & f ):
    // Parents:
    Formatter_if_t( f ),
    // Fields:
    _print_trace_type( f._print_trace_type ),
    _print_trace_timestamp( f._print_trace_timestamp )
{
    // Nothing to do.
}


BasicFormatter_t::~BasicFormatter_t()
{
    // Nohting to do.
}

BasicFormatter_t & BasicFormatter_t::operator = ( const BasicFormatter_t & f )
{
    Formatter_if_t::operator = ( f );

    _print_trace_type = f._print_trace_type;
    _print_trace_timestamp = f._print_trace_timestamp;

    return *this;
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void BasicFormatter_t::printTraceType( const bool b )
{
    _print_trace_type = b;
}

void BasicFormatter_t::printTraceTimestamp( const bool b )
{
    _print_trace_timestamp = b;
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////

void BasicFormatter_t::_traceInfo( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "INF", false );
}

void BasicFormatter_t::_traceLog( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "LOG", false );
}

void BasicFormatter_t::_traceDebug( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "DBG", true );
}

void BasicFormatter_t::_traceWarning( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "WRN", true );
}

void BasicFormatter_t::_traceError( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "ERR", true );
}

void BasicFormatter_t::_traceFatal( std::ostream & os, const trace_data_t & data )
{
    _basicTrace( os, data, "FTL", true );
}


// ////////////////////////////////////////////////////////////////
// Internal support methods.
// ////////////////////////////////////////////////////////////////

void BasicFormatter_t::_basicTrace( std::ostream & os,
                                    const trace_data_t & data,
                                    const char * const kind,
                                    const bool print_sourcefile_infos )
{
    // "level" field of "data" is not printed.

    if ( _print_trace_type )
    {
        os << kind<< " ";
    }

    if ( _print_trace_timestamp )
    {
        os << data.timestamp << " ";
    }
    os << data.name;

    if ( print_sourcefile_infos )
    {
        os << " " << data.filename << ":" << data.fileline;
    }

    if ( data.packet != nullptr )
    {
        os  << " " << "packet " << data.packet->getId();
    }

    if ( data.channel != nullptr )
    {
        os << " " << data.channel->name() << ": carrier=" << data.carrier;
    }

    if ( data.message != nullptr )
    {
        os << " " << data.message;
    }

    os << std::endl;
}
