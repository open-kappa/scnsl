// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Formatter interface.



#include "scnsl/tracing/Formatter_if_t.hh"


using namespace Scnsl::Tracing;



// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

Formatter_if_t::~Formatter_if_t()
{
    // Nohting to do.
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////


void Formatter_if_t::addOutput( std::ostream * os )
{
    _outputs.push_back( os );
}

void Formatter_if_t::traceInfo( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceInfo( **i, data );
    }
}

void Formatter_if_t::traceLog( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceLog( **i, data );
    }
}

void Formatter_if_t::traceDebug( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceDebug( **i, data );
    }
}

void Formatter_if_t::traceWarning( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceWarning( **i, data );
    }
}

void Formatter_if_t::traceError( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceError( **i, data );
    }
}

void Formatter_if_t::traceFatal( const trace_data_t & data )
{
    for ( OutputList_t::iterator i = _outputs.begin();
          i != _outputs.end();
          ++i )
    {
        _traceFatal( **i, data );
    }
}



// ////////////////////////////////////////////////////////////////
// Regular design pattern.
// ////////////////////////////////////////////////////////////////

Formatter_if_t::Formatter_if_t():
    _outputs()
{
    // Nothing to do.
}

Formatter_if_t::Formatter_if_t( const Formatter_if_t & f ):
    _outputs( f._outputs )
{
    // Nothing to do.
}

Formatter_if_t & Formatter_if_t::operator = ( const Formatter_if_t & f )
{
    _outputs = f._outputs;
    return *this;
}
