// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// tracing_data_types.hh methods implementation.



#include "scnsl/tracing/tracing_data_types.hh"

namespace Scnsl { namespace Tracing {


trace_data_t::trace_data_t():
    name(),
    timestamp( sc_core::SC_ZERO_TIME ),
    filename( nullptr ),
    fileline( 0 ),
    level( 0 ),
    message( nullptr ),
    packet( nullptr ),
    carrier( false ),
    channel( nullptr )
{
    // ntd
}

trace_data_t::trace_data_t(const Scnsl::Tracing::trace_data_t & d):
    name( d.name ),
    timestamp( d.timestamp ),
    filename( d.filename ),
    fileline( d.fileline ),
    level( d.level ),
    message( d.message ),
    packet( d.packet ),
    carrier( d.carrier ),
    channel( d.channel )
{
    // ntd
}

trace_data_t &trace_data_t::operator =(const trace_data_t & d)
{
    name = d.name;
    timestamp = d.timestamp;
    filename = d.filename;
    fileline = d.fileline;
    level = d.level;
    message = d.message;
    packet = d.packet;
    carrier = d.carrier;
    channel = d.channel;
    return *this;
}

} } // scnsl::tracing
