// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A shared model with constant delay.

#include <sstream>
#include <cmath>
#include <cassert>
#include "scnsl/channels/SharedChannel_t.hh"
#include "scnsl/utils/Environment_if_t.hh"

using namespace Scnsl::Channels;

// ////////////////////////////////////////////////////////////////
// Support macros
// ////////////////////////////////////////////////////////////////
// Used to write a cleaner code.

#define NOT_INTERSECT(t1,t2,t3,t4) ( (t2)<=(t3) || (t4)<=(t1) )
#define INTERSECT_SENDING_TIME(a,b)                                     \
    (! NOT_INTERSECT((a).sendStart,(a).sendEnd,(b).sendStart,(b).sendEnd) )

SharedChannel_t::packet_infos_t::packet_infos_t():
    collided( false ),
    corrupted( false ),
    corruptionTime( sc_core::SC_ZERO_TIME ),
    encodingTime( sc_core::SC_ZERO_TIME )
{
    // ntd
}


SharedChannel_t::single_packet_infos_t::single_packet_infos_t():
    packet(),
    sendStart( sc_core::SC_ZERO_TIME ),
    sendEnd( sc_core::SC_ZERO_TIME ),
    send_started( false )
{
    // ntd
}

SharedChannel_t::single_infos_t::single_infos_t():
    node( nullptr ),
    packetInfos(),
    sendStart(),
    sendEnd(),
    active_carriers( 0 ),
    old_carrier( false )
{
    // ntd
}

SharedChannel_t::transmission_infos_t::transmission_infos_t():
    packetInfos(),
    reachable( false ),
    power(0.0),
    same_encoding_speed( true )
{
    // ntd
}


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

SharedChannel_t::SharedChannel_t(
    const sc_core::sc_module_name modulename,
    const counter_t nodes_number,
    const double alpha,
    const delay_t delay )
     :
    // Parents:
    Channel_if_t( modulename ),
    // Fields:
    _transmissions( nullptr ),
    _single_infos( nullptr ),
    _NODES_NUMBER( nodes_number ),
    _DELAY( delay ),
    _ALPHA_EXPONENT( alpha ),
    _EPSILON( 0.00000001 ),
    _nodes_number( 0 ),
    _ids(),
    _eventsQueue( Scnsl::Utils::EventsQueue_t::get_instance() )
{
    if ( _NODES_NUMBER <= 0
         || _DELAY < sc_core::SC_ZERO_TIME
         || _ALPHA_EXPONENT < 0 )
        throw std::domain_error( "Invalid arguments to channel constructor." );

    _transmissions = new transmission_infos_t * [ _NODES_NUMBER ];
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        _transmissions[ i ] = new transmission_infos_t [ _NODES_NUMBER ];
    }

    _single_infos = new single_infos_t[ _NODES_NUMBER ];
}


SharedChannel_t::~SharedChannel_t()
{
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        delete [] _transmissions[ i ];
    }
    delete [] _transmissions;

    delete [] _single_infos;
}


// ////////////////////////////////////////////////////////////////
// Inherited channel interface methods.
// ////////////////////////////////////////////////////////////////


void SharedChannel_t::addNode( Node_t * n )

{
    SCNSL_TRACE_DBG( 5, "<> addNode()." );

    if ( _nodes_number >= _NODES_NUMBER )
        throw std::logic_error( "Registering more nodes than allowed." );

    _single_infos[ _nodes_number ].node = n;
    _ids[ n ] = _nodes_number ++;

    Channel_if_t::addNode( n );

    if ( _nodes_number >= _NODES_NUMBER )
    {
        SCNSL_TRACE_DBG( 3, "<> addNode(): initializing internal data." );

        // Initialization of transmission data.
        for( NodeList_t::iterator i = _nodes.begin();
             i != _nodes.end();
             ++i )
        {
            updateProperties( *i );
        }
    }
}

SharedChannel_t::errorcode_t SharedChannel_t::send( Node_t * n, const Packet_t & p )

{
    // 1- Scheduling the packet.
    // 2- Waiting the encoding delay.
    // 3- Check collisions due to send of pck, as internal_recv.

    // Phase 1:
    SCNSL_TRACE_LOG( 1, p, "<> send()." );
    SCNSL_TRACE_DBG( 1, p, ">> send()." );
    const Scnsl::Core::node_id_t ID = _ids[ n ];

    // - Pre-calculating all times:

    const Packet_t::size_t size( p.getSize() );
    const sc_core::sc_time encodingTime( size / n->getProperties( this ).bitrate, sc_core::SC_SEC );

    const sc_core::sc_time currentTime( sc_core::sc_time_stamp() );
    const sc_core::sc_time interferenceStart( currentTime + _DELAY );
    const sc_core::sc_time interferenceEnd( interferenceStart + encodingTime );
    const sc_core::sc_time sendEnd( currentTime + encodingTime );

#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss<< "<> send: start = " << interferenceStart
      << " end = " << interferenceEnd << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    // - Storing times & packet:

    single_packet_infos_t s_infos;
    s_infos.packet = p;
    s_infos.packet.setChannel( this );
    s_infos.sendStart = interferenceStart;
    s_infos.sendEnd = interferenceEnd;

    _single_infos[ ID ].packetInfos.push_back( s_infos );

    packet_infos_t infos;
    // infos.corrupted = false; // default by constr
    // infos.collided = false;  // default by constr
    infos.encodingTime = encodingTime;

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        _transmissions[ ID ][ i ].packetInfos.push_back( infos );
    }

    // Updating this source sending times:
    _single_infos[ ID ].sendStart = currentTime;
    _single_infos[ ID ].sendEnd = sendEnd;

    // - Registering callbacks:

    typedef void (SharedChannel_t::* mptr_t)( Node_t * );
    _eventsQueue->registerAction(
        interferenceStart,
        this,
        reinterpret_cast< mptr_t >( & SharedChannel_t::_startSending),
        n );
    _eventsQueue->registerAction(
        interferenceEnd,
        this,
        reinterpret_cast< mptr_t >( & SharedChannel_t::_completeSending ),
        n );

    // Phase 2:
    SCNSL_TRACE_DBG( 2, p, "send(): waiting encoding time." );

    wait( encodingTime );

    // Phase 3:
    SCNSL_TRACE_DBG( 2, p, "send(): updating collision infos." );

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID
             || ! _transmissions[ i ][ ID ].reachable
             || _single_infos[ i ].packetInfos.empty() )
            continue;

        if ( INTERSECT_SENDING_TIME(_single_infos[ ID ],
                                    _single_infos[ i ].packetInfos.front() ))
        {
            // collision!
            _transmissions[ i ][ ID ].packetInfos.front().collided = true;
        }
    }

    SCNSL_TRACE_DBG( 1, p, "<< send()." );
    return 0;
}



bool SharedChannel_t::isMultipoint()
    const
{
    SCNSL_TRACE_DBG( 5, "isMultipoint()." );
    return true;
}


void SharedChannel_t::updateProperties( const Node_t * n )
{
    SCNSL_TRACE_DBG( 1, "<> updateProperites()." );


	const Scnsl::Core::node_id_t ID = _ids[n];
    const Node_t::node_properties_t & np = n->getProperties( this );

    // Show the coordinates of the node.
#if (SCNSL_LOG >= 1)
    std::stringstream ss;
    ss<< "Position of Node" << ID << ": x = " << np.x << " y = " << np.y << " z = " << np.z << ".";
    SCNSL_TRACE_LOG( 5, ss.str().c_str() );
#endif

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        const Node_t::node_properties_t & n2p = _single_infos[ i ].node->getProperties( this );

        // Updating properties with n as sender.
        _updateSingleProperties( ID, i, np, n2p );

        // Updating properties with n as receiver.
        _updateSingleProperties( i, ID, n2p, np );
    }
}


// ////////////////////////////////////////////////////////////////
// Callbacks.
// ////////////////////////////////////////////////////////////////


void SharedChannel_t::_startSending( const Node_t * n )
{
    SCNSL_TRACE_DBG( 1, "<> _startSending()." );

    // Start tracking collisions.

	const Scnsl::Core::node_id_t ID = _ids[n];

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        _startSingleSending( ID, i );
    }

    _single_infos[ ID ].packetInfos.front().send_started = true;
}



void SharedChannel_t::_completeSending( const Node_t * n )
{
    SCNSL_TRACE_DBG( 1, "<> _completeSending()." );

    // Stop tracking collisions and forwarding the packet.

	const Scnsl::Core::node_id_t ID = _ids[n];

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        _completeSingleSending( ID, i );
        // Cleanup:
        _transmissions[ ID ][ i ].packetInfos.pop_front();
    }

    // Cleanup:
    _single_infos[ ID ].packetInfos.pop_front();
}


// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////


void SharedChannel_t::_startSingleSending(const Scnsl::Core::node_id_t ID, const Scnsl::Core::node_id_t recv_id)
{
    SCNSL_TRACE_DBG( 1, "<> _startSingleSending()." );

    if ( ID == recv_id || ! _transmissions[ ID ][ recv_id ].reachable )
    {
        return;
    }

    // Checking collisions:
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID || i == recv_id ) continue;

        if ( ! _transmissions[i][recv_id].reachable
             || _transmissions[i][recv_id].packetInfos.empty() )
            continue;

        if ( INTERSECT_SENDING_TIME(_single_infos[ i ].packetInfos.front(),
                                    _single_infos[ ID ].packetInfos.front() ))
        {
            // collision!
            if ( ! _transmissions[i][recv_id].packetInfos.front().collided )
                _transmissions[i][recv_id].packetInfos.front().collided = true;

            if ( ! _transmissions[ID][recv_id].packetInfos.front().collided )
                _transmissions[ID][recv_id].packetInfos.front().collided = true;
        }
    }

    // Carrier:
    ++ _single_infos[ recv_id ].active_carriers;
    if ( _single_infos[ recv_id ].active_carriers == 1
         && _single_infos[ recv_id ].old_carrier == false )
    {
        _single_infos[ recv_id ].old_carrier = true;
        _single_infos[ recv_id ].node->setCarrier( this, true );
    }
}


void SharedChannel_t::_completeSingleSending(const Scnsl::Core::node_id_t ID, const Scnsl::Core::node_id_t recv_id)
{
    SCNSL_TRACE_DBG( 1, "<> _completeSingleSending()." );

    // Updating transmission:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // Checking if there is a movement of the node recv_id out of the transmission
    // range of the node ID at the same time of the _completeSingleSending().
    // If true we receive the packet.
    if ( ID == recv_id || ( ( ! _transmissions[ ID ][ recv_id ].reachable )
    	 && ( _transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime != currentTime ) ) )
        return;

    SCNSL_TRACE_DBG( 3, "<> _completeSingleSending(): forwarding." );

    // Checking collisions:
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID || i == recv_id ) continue;

        if ( ! _transmissions[i][recv_id].reachable
             || _transmissions[i][recv_id].packetInfos.empty() ) continue;

        if ( INTERSECT_SENDING_TIME( _single_infos[ i ].packetInfos.front(),
                                     _single_infos[ ID ].packetInfos.front() ))
        {
            // collision!
            _transmissions[i][recv_id].packetInfos.front().collided = true;
            _transmissions[ID][recv_id].packetInfos.front().collided = true;
        }
    }

    // Checking with rx in sending mode:
    if ( INTERSECT_SENDING_TIME( _single_infos[ recv_id ],
                                 _single_infos[ ID ].packetInfos.front() ))
    {
        // collision!
        _transmissions[ID][recv_id].packetInfos.front().collided = true;
    }

    // If there was a movement of the node recv_id from outside to inside the transmission range
    // of the node ID at the same time of a _startSingleSending() the packet is not corrupted.
    // Alike, if there was a movement of the node recv_id from inside to outside the transmission range
    // of the node ID at the same time of a _completeSingleSending() the packet is not corrupted.
    if ( _transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime == currentTime
    	 ||  _transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime ==
    	 ( currentTime - _transmissions[ ID ][ recv_id ].packetInfos.front().encodingTime ) )
        _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = false;

    // Forwarding:
    if ( ! _transmissions[ ID ][ recv_id ].packetInfos.front().collided
         && ! _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted
         && _transmissions[ ID ][ recv_id ].same_encoding_speed )
    {
        Packet_t & p = _single_infos[ ID ].packetInfos.front().packet;
        p.setReceivedPower(_transmissions[ ID ][ recv_id ].power);
        _single_infos[ recv_id ].node->receive( p );
    }

    // Carrier:
    -- _single_infos[ recv_id ].active_carriers;
    if ( _single_infos[ recv_id ].active_carriers == 0 )
    {
        // Do we have a continuous packet?
        const bool continuous = ( _single_infos[ ID ].packetInfos.size() > 1 )
        &&( ( ++_single_infos[ ID ].packetInfos.begin() )->sendStart == _single_infos[ ID ].packetInfos.front().sendEnd );

        if ( ! continuous )
        {
            _single_infos[ recv_id ].old_carrier = false;
            _single_infos[ recv_id ].node->setCarrier( this, false );
        }
    }
}



void SharedChannel_t::_updateSingleProperties(
	const Scnsl::Core::node_id_t ID,
	const Scnsl::Core::node_id_t recv_id,
    const Node_t::node_properties_t & sp,
    const Node_t::node_properties_t & rp )

{
    SCNSL_TRACE_DBG( 3, "<> _updateSingleProperties()." );

    if ( ID == recv_id ) return;

    const bool old_reachable = _transmissions[ ID ][ recv_id ].reachable;

    // 1 - They must encode/decode at the same bitrate:

    _transmissions[ ID ][ recv_id ].same_encoding_speed = (fabs( sp.bitrate - rp.bitrate) < _EPSILON );

    // 2 - They must be inside the transmission maximum range:

    // The nodes have differents coordinates.
    Scnsl::Utils::Environment_if_t * env = Scnsl::Utils::Environment_if_t::getInstance();
    const double receivedPower = env->getReceiverPower(sp, rp);
    const bool pow_reac =  receivedPower >= rp.receiving_threshold;
    _transmissions[ ID ][ recv_id ].reachable = pow_reac;
    _transmissions[ ID ][ recv_id ].power = receivedPower;

    // 3 - Updating transmissions:

    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // The only packet possibly affected, is the one
    // currently under encoding.
    // Checking this packet:
    if (! _single_infos[ ID ].packetInfos.empty()  // This check should be unuseful...
        && currentTime <= ( _single_infos[ ID ].packetInfos.front().sendEnd ) // Not yet send completed
        && currentTime >= ( _single_infos[ ID ].packetInfos.front().sendStart ) // Recv started
        )
    {
        if ( _transmissions[ ID ][ recv_id ].reachable && ! old_reachable )
        {
            // Was NOT reachable, but now it is.
            // If the corruption time of the packet is at the same time of the movement,
        	// the packet is not corrupted.
        	if ( currentTime == _transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime
        	     && _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted == true )
        		 _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = false;
        	else
        	{
                // Since internal_recv already started, this pck is corrupted.
        		_transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = true;
            	// Storing the corruption time of the packet.
        	    _transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime = currentTime;
        	}
            // If required, updating sending infos:
            if ( _single_infos[ ID ].packetInfos.front().send_started )
            {
                _startSingleSending( ID, recv_id );
            }
        }
        else if ( ! _transmissions[ ID ][ recv_id ].reachable && old_reachable )
        {
            // Was reachable, but now it is no more.
        	// Since internal_recv already started, this pck is corrupted.
        	_transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = true;
        	// Storing the corruption time of the packet.
        	_transmissions[ ID ][ recv_id ].packetInfos.front().corruptionTime = currentTime;

        	 // Since it is no more reachable, we manage the carrier.
            -- _single_infos[ recv_id ].active_carriers;
            if (_single_infos[ recv_id ].active_carriers == 0 )
            {
                // Do we have a continuous packet?
                const bool continuous = (_single_infos[ ID ].packetInfos.size() > 1 )
                &&( (++_single_infos[ ID ].packetInfos.begin())->sendStart == _single_infos[ ID ].packetInfos.front().sendEnd );

                if ( ! continuous )
                {
                    _single_infos[ recv_id ].old_carrier = false;
                    _single_infos[ recv_id ].node->setCarrier( this, false );
                }
            }
        }
    }
}
