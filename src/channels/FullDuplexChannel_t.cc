// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A full duplex channel.

#include <string>

#include "scnsl/channels/FullDuplexChannel_t.hh"

#ifdef _MSC_VER
#pragma warning(disable:4355)
#endif

using namespace Scnsl::Channels;

// ////////////////////////////////////////////////////////////////
// Static members initialization.
// ////////////////////////////////////////////////////////////////

namespace Scnsl { namespace Channels {

    const FullDuplexChannel_t::counter_t FullDuplexChannel_t::_MAXIMUM_BOUNDED_NODES( 2 );

  } }

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

FullDuplexChannel_t::FullDuplexChannel_t( const sc_core::sc_module_name modulename,
                     const bitrate_t capacity1,
                     const bitrate_t capacity2,
                     const delay_t delay )
    :
    // Parents:
    ChannelWrapper_if_t( modulename ),
    // Fields:
    _channel1( (static_cast<const char *>(modulename) + std::string("_direction1")).c_str(), capacity1, delay ),
    _channel2( (static_cast<const char *>(modulename) + std::string("_direction2")).c_str(), capacity2, delay ),
	_bridge1( this ),
	_bridge2( this ),
	_flagCarrier1( true ),
	_flagCarrier2( true )
{
    // Nothing to do.
}

FullDuplexChannel_t::~FullDuplexChannel_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Inherited channel interface methods.
// ////////////////////////////////////////////////////////////////

void FullDuplexChannel_t::addNode( Node_t * n )

{
    if ( _nodes.size() >= _MAXIMUM_BOUNDED_NODES )
    {
        throw std::invalid_argument( "Already bounded all nodes." );
    }

    _channel1.addNode( & _bridge1 );
    _channel2.addNode( & _bridge2 );

    _nodes.push_back( n );
}

FullDuplexChannel_t::errorcode_t FullDuplexChannel_t::send( Node_t * n, const Packet_t & p )

{
    if ( n == _nodes.front() )
    {
		return _channel1.send(&_bridge1, p);
    }

	return _channel2.send(&_bridge2, p);
}

bool FullDuplexChannel_t::isMultipoint()
    const
{
    return false;
}



// ////////////////////////////////////////////////////////////////
// Methods for connection with the bridge.
// ////////////////////////////////////////////////////////////////



void FullDuplexChannel_t::bridgeSetCarrier( const Scnsl::Utils::ChannelBridge_t * cb, const carrier_t c )

{
	if ( cb == & _bridge1 )
	{
		if ( _flagCarrier1 )
		{
			_nodes.back()->setCarrier( this, c );
			_flagCarrier1 = false;
		}
		else
			_flagCarrier1 = true;
	}

	if ( cb == & _bridge2 )
	{
		if ( _flagCarrier2 )
		{
			_nodes.front()->setCarrier( this, c );
			_flagCarrier2 = false;
		}
		else
			_flagCarrier2 = true;
	}
}


FullDuplexChannel_t::errorcode_t FullDuplexChannel_t::bridgeReceive(
    const Scnsl::Utils::ChannelBridge_t * cb, const Packet_t & p )

{
	if ( cb == & _bridge1 )
	{
		return _nodes.back()->receive( p );
	}

	if ( cb == & _bridge2 )
	{
		return _nodes.front()->receive( p );
	}

	return 0;
}
