// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif


/// @file
/// An half duplex channel.



#include "scnsl/channels/HalfDuplexChannel_t.hh"


using namespace Scnsl::Channels;

// ////////////////////////////////////////////////////////////////
// Static members declaration.
// ////////////////////////////////////////////////////////////////

namespace Scnsl { namespace Channels {

const HalfDuplexChannel_t::counter_t HalfDuplexChannel_t::_MAXIMUM_BOUNDED_NODES( 2 );

} }

HalfDuplexChannel_t::transmission_infos_t::transmission_infos_t():
    time(),
    packet()
{
    // ntd
}

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

HalfDuplexChannel_t::HalfDuplexChannel_t( const sc_core::sc_module_name modulename,
                                          const bitrate_t capacity,
                                          const delay_t delay ):
    // Parents:
    Channel_if_t( modulename ),
    // Fields:
    _DELAY( delay ),
    _CAPACITY( capacity ),
    _queues(),
    _locks(),
    _infos(),
    _newPacket(),
    _ongoing_packets_number(),
    _completionTime()
{
    if ( capacity <= 0 || delay < sc_core::SC_ZERO_TIME )
        throw std::domain_error( "Invalid arguments to channel constructor." );

    _locks[ 0 ] = false;
    _locks[ 1 ] = false;

    _ongoing_packets_number[ 0 ] = 0;
    _ongoing_packets_number[ 1 ] = 0;

    _completionTime[ 0 ] = sc_core::SC_ZERO_TIME;
    _completionTime[ 1 ] = sc_core::SC_ZERO_TIME;

    SC_THREAD( _delayProcess1 );
    SC_THREAD( _delayProcess2 );
}


HalfDuplexChannel_t::~HalfDuplexChannel_t()
{
    // Nohting to do.
}


// ////////////////////////////////////////////////////////////////
// Inherited channel interface methods.
// ////////////////////////////////////////////////////////////////

void HalfDuplexChannel_t::addNode( Node_t * n )

{
    if ( _nodes.size() >= _MAXIMUM_BOUNDED_NODES )
    {
        throw std::logic_error( "Already bounded al available nodes." );
    }

    _nodes.push_back( n );
}

HalfDuplexChannel_t::errorcode_t HalfDuplexChannel_t::send( Node_t * n, const Packet_t & p )

{
    if ( n != _nodes.front() && n != _nodes.back() )
    {
        throw std::invalid_argument(
            "Unregistered node calls a send() on a channel." );
    }

    const counter_t NODE_ID = counter_t((n == _nodes.front())? 0 : 1);
    const counter_t DESTINATION_NODE_ID = counter_t((NODE_ID == 0)? 1 : 0);

    // A sort of protected section.
    if ( _locks[ NODE_ID ] ) return 1;
    _locks[ NODE_ID ] = true;
    // Preparing the packet:
    _infos[ NODE_ID ].packet = p;

    // Updating eventual collisions.
    checkCollisions( DESTINATION_NODE_ID );

    // Managing the carrier.
    // If the carrier is not set...
    if ( _ongoing_packets_number[ NODE_ID ] == 0 )
	{
		if ( NODE_ID )
			_nodes.front()->setCarrier( p.getChannel(), true );
		else
			_nodes.back()->setCarrier( p.getChannel(), true );
	}

    // Updating the number of ongoing transmissions:
    ++ ( _ongoing_packets_number[ NODE_ID ] );

    // Scheduling the packet.
    sc_core::sc_time t (p.getSize() / _CAPACITY, sc_core::SC_SEC );
    _completionTime[ NODE_ID ] = sc_core::sc_time_stamp() + t + _DELAY;
    wait( t );

    // Now sending with the delay.
    _infos[ NODE_ID ].time = _completionTime[ NODE_ID ];
    _queues[ NODE_ID ].push_back( _infos[ NODE_ID ] );
    _newPacket[ NODE_ID ].notify();

    _locks[ NODE_ID ] = false;
    return 0;
}


bool HalfDuplexChannel_t::isMultipoint()
    const
{
    return false;
}


// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////

void HalfDuplexChannel_t::checkCollisions( const counter_t destination_node_id )
{
	// If there are packet in the other direction:
    // - If there is just another packet that completes its transmission in
    //   this simulation time, it is not a collision.
    //
    if ( sc_core::sc_time_stamp() < _completionTime[ destination_node_id ] )
    {
        // Collision!
        setCollisions( destination_node_id );
    }
}


void HalfDuplexChannel_t::setCollisions( const counter_t destination_node_id )
{
    // Invalidating this packet and all packets in the other direction.
    _infos[ 0 ].packet.setValidity( false );
    _infos[ 1 ].packet.setValidity( false );

    const Queue_t::iterator end = _queues[ destination_node_id ].end();
	for ( Queue_t::iterator i = _queues[ destination_node_id ].begin();
          i != end;
           ++ i )
    {
        (*i).packet.setValidity( false );
    }

}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void HalfDuplexChannel_t::_delayProcess1()
{
    static const counter_t NODE_ID = 0;
    sc_core::sc_time t;
    transmission_infos_t * info;

	for (;;)
    {
        if ( _queues[ NODE_ID ].empty() )
            wait( _newPacket[ NODE_ID ] );

		t = _queues[ NODE_ID ].front().time - sc_core::sc_time_stamp();
		if ( t >= sc_core::SC_ZERO_TIME )
			wait( t );

        info = & _queues[ NODE_ID ].front();
        // Since the channel is a fifo,
        // a while for sending should be unuseful: it is impossible
        // that two packets will have the same arriving time!
        // Thus, it is simplified into:
		if ( info->packet.isValid() )
			_nodes.back()->receive( info->packet );
        -- ( _ongoing_packets_number[ NODE_ID ] );
        _queues[ NODE_ID ].pop_front();

        // Managing the carrier.
        // If no more packet...
		if ( _ongoing_packets_number[ NODE_ID ] == 0 )
		{
			_nodes.back()->setCarrier( info->packet.getChannel(), false );
		}

    }
}

void HalfDuplexChannel_t::_delayProcess2()
{
    static const counter_t NODE_ID = 1;
    sc_core::sc_time t;
    transmission_infos_t * info;

	for (;;)
    {
        if ( _queues[ NODE_ID ].empty() )
            wait( _newPacket[ NODE_ID ] );

		t = _queues[ NODE_ID ].front().time - sc_core::sc_time_stamp();
		if ( t >= sc_core::SC_ZERO_TIME )
			wait( t );

        info = & _queues[ NODE_ID ].front();
        // Since thie channel is a fifo,
        // a while for sending should be unuseful: it is impossible
        // that two packets will have the same arriving time!
        // Thus, it is simplified into:
        if ( info->packet.isValid() )
			_nodes.front()->receive( info->packet );
        -- ( _ongoing_packets_number[ NODE_ID ] );
        _queues[ NODE_ID ].pop_front();

        // Managing the carrier.
        // If no more packet...
		if ( _ongoing_packets_number[ NODE_ID ] == 0 )
		{
			_nodes.front()->setCarrier( info->packet.getChannel(), false );
		}
    }
}
