// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// An unidirectiona channel.



#include "scnsl/channels/UnidirectionalChannel_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using Scnsl::Channels::UnidirectionalChannel_t;

// ////////////////////////////////////////////////////////////////
// Static fields.
// ////////////////////////////////////////////////////////////////

namespace Scnsl { namespace Channels {

const UnidirectionalChannel_t::counter_t UnidirectionalChannel_t::_MAXIMUM_BOUNDED_NODES( 2 );


} }

UnidirectionalChannel_t::transmission_infos_t::transmission_infos_t():
    time(),
    packet()
{
    // ntd
}

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

UnidirectionalChannel_t::UnidirectionalChannel_t( const sc_core::sc_module_name modulename,
                                                  const bitrate_t capacity,
                                                  const delay_t delay )
    :
    // Parents:
    Channel_if_t( modulename ),
    // Fields:
    _DELAY( delay ),
    _CAPACITY( capacity ),
    _lock(false),
    _queue(),
    _info(),
    _newPacket()
{
    if ( capacity <= 0 || delay < sc_core::SC_ZERO_TIME )
        throw std::domain_error( "Invalid arguments to channel constructor." );

    SC_THREAD( _delayProcess );
}

UnidirectionalChannel_t::~UnidirectionalChannel_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Inherited channel interface methods.
// ////////////////////////////////////////////////////////////////

void UnidirectionalChannel_t::addNode( Node_t * n )

{
    if ( _nodes.size() >=  _MAXIMUM_BOUNDED_NODES )
    {
        throw std::logic_error( "Already bounded all available nodes." );
    }

    _nodes.push_back( n );
}

Scnsl::Core::errorcode_t UnidirectionalChannel_t::send( Node_t * n, const Packet_t & p )

{
    if ( n != _nodes.front() )
    {
        throw std::invalid_argument("Invalid sending node.");
    }

    // Managing the carrier.
    if ( isEmpty() )
    {
        _nodes.front()->setCarrier( p.getChannel(), true );
        _nodes.back()->setCarrier( p.getChannel(), true );
    }
    // A sort of protected section.
    if ( _lock ) return 1;
    _lock = true;

    // Scheduling the packet.
    wait( p.getSize() / _CAPACITY, sc_core::SC_SEC );

    // Now sending with the delay.
    _info.time = sc_core::sc_time_stamp() + _DELAY;
    _info.packet = p;
    _queue.push_back( _info );
    _newPacket.notify();

    _lock = false;
    return 0;
}


bool UnidirectionalChannel_t::isEmpty()
    const
{
    return _queue.empty() && ! _lock;
}


// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////

bool UnidirectionalChannel_t::isMultipoint()
    const
{
    return false;
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void UnidirectionalChannel_t::_delayProcess()
{
    sc_core::sc_time t;
    transmission_infos_t * info;
    const Channel_if_t * c;

	for (;;)
    {
        if ( _queue.empty() )
        {
            wait( _newPacket );
        }

		t = _queue.front().time - sc_core::sc_time_stamp();
		if ( t >= sc_core::SC_ZERO_TIME )
			wait( t );


        info = & _queue.front();

        // Since thie channel is a fifo,
        // this while should be unuseful: it is impossible
        // that two packets will have the same arriving time!
        // Thus the code is commented.
        //         while ( info.time <= t )
        //         {
        //             _nodes.back()->receive( info->packet );
        //             _queue.pop_front();
        //             if ( _queue.empty() ) break;
        //             info = & _queue.front();
        //         }
        // And it is simplified into:
        _nodes.back()->receive( info->packet );
        c = info->packet.getChannel();
        _queue.pop_front();

        // If no other packet is outgoing, reset the carrier.
        if ( isEmpty() )
        {
            _nodes.front()->setCarrier( c, false );
            _nodes.back()->setCarrier( c, false );
        }

    }
}
