// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A shared model with variable delay.

#include <sstream>
#include <cmath>
#include <cassert>
#include "scnsl/channels/DelayedSharedChannel_t.hh"
#include "scnsl/utils/EventsQueue_t.hh"
#include "scnsl/utils/Environment_if_t.hh"

using namespace Scnsl::Channels;

// ////////////////////////////////////////////////////////////////
// Support macros
// ////////////////////////////////////////////////////////////////
// Used to write a cleaner code.

#define NOT_INTERSECT(t1,t2,t3,t4) ( (t2)<=(t3) || (t4)<=(t1) )
#define INTERSECT_SENDING_TIME(a,b,c,d)                                     \
    (! NOT_INTERSECT((a),(b),(c),(d)) )

DelayedSharedChannel_t::single_infos_t::single_infos_t():
    node( nullptr ),
    sendStart(),
    sendEnd(),
    active_carriers( 0 ),
    old_carrier( false ),
    movementTime()
{}

DelayedSharedChannel_t::transmission_infos_t::transmission_infos_t():
    packetInfos(),
    startEventsIds(),
    completeEventsIds(),
    idsInfos(),
    reachable( false ),
    old_reachable( false ),
    same_encoding_speed( true ),
    channelDelay(),
    oldChannelDelay(),
    maxDelay(),
    power(0.0)
{}

DelayedSharedChannel_t::packet_infos_t::packet_infos_t():
    packet(),
    sendStart(),
    sendEnd(),
    encodingTime(),
    collided( false ),
    corrupted( false ),
    send_started( false )
{}


DelayedSharedChannel_t::transmission_ids_infos_t::transmission_ids_infos_t():
    sourceId( 0 ),
    destinationId( 0 )
{}


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

DelayedSharedChannel_t::DelayedSharedChannel_t(
    const sc_core::sc_module_name modulename,
    const counter_t nodes_number,
    const double alpha,
    const propagation_t propagation )
     :
    // Parents:
    Channel_if_t( modulename ),
    // Fields:
    _transmissions( nullptr ),
    _single_infos( nullptr ),
    _NODES_NUMBER( nodes_number ),
    _PROPAGATION( propagation ),
    _ALPHA_EXPONENT( alpha ),
    _EPSILON( 0.00000001 ),
    _nodes_number( 0 ),
    _ids(),
    _eventsQueue( Scnsl::Utils::EventsQueue_t::get_instance() )
{
    if ( _NODES_NUMBER <= 0
         || _PROPAGATION <= 0
         || _ALPHA_EXPONENT < 0 )
        throw std::domain_error( "Invalid arguments to channel constructor." );

    _transmissions = new transmission_infos_t * [ _NODES_NUMBER ];
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        _transmissions[ i ] = new transmission_infos_t [ _NODES_NUMBER ];
    }

    _single_infos = new single_infos_t[ _NODES_NUMBER ];
}


DelayedSharedChannel_t::~DelayedSharedChannel_t()
{
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        delete [] _transmissions[ i ];
    }
    delete [] _transmissions;

    delete [] _single_infos;
}


// ////////////////////////////////////////////////////////////////
// Inherited channel interface methods.
// ////////////////////////////////////////////////////////////////


void DelayedSharedChannel_t::addNode( Node_t * n )

{
    SCNSL_TRACE_DBG( 5, "<> addNode()." );

    if ( _nodes_number >= _NODES_NUMBER )
        throw std::logic_error( "Registering mode nodes than allowed." );

    _single_infos[ _nodes_number ].node = n;
    _ids[ n ] = _nodes_number ++;

    Channel_if_t::addNode( n );

    if ( _nodes_number >= _NODES_NUMBER )
    {
        SCNSL_TRACE_DBG( 3, "<> addNode(): initializing internal data." );

        // Initialization of transmission data.
        for( NodeList_t::iterator i = _nodes.begin();
             i != _nodes.end();
             ++i )
        {
            _initializeProperties( *i );
        }
    }
}


DelayedSharedChannel_t::errorcode_t DelayedSharedChannel_t::send( Node_t * n, const Packet_t & p )

{
    // 1- Scheduling the packet.
    // 2- Waiting the encoding delay.
    // 3- Check collisions due to send of pck, as internal_recv.

    // Phase 1:
    SCNSL_TRACE_LOG( 1, p, "<> send()." );
    SCNSL_TRACE_DBG( 1, p, ">> send()." );
    const Scnsl::Core::node_id_t ID = _ids[ n ];

    // - Pre-calculating times for source node:

    const Packet_t::size_t size( p.getSize() );
    const sc_core::sc_time encodingTime( size / n->getProperties( this ).bitrate, sc_core::SC_SEC );
    const sc_core::sc_time currentTime( sc_core::sc_time_stamp() );
    const sc_core::sc_time sendEnd( currentTime + encodingTime );

    // Storing this source sending times:
    _single_infos[ ID ].sendStart = currentTime;
    _single_infos[ ID ].sendEnd = sendEnd;

    packet_infos_t infos;
    // infos.corrupted = false; // default by constr
    // infos.collided = false;  // default by constr

    // - Pre-calculating times for each destination nodes:

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
    	if ( ID == i )
    		continue;

        const sc_core::sc_time delay( _transmissions[ ID ][ i ].channelDelay );
        const sc_core::sc_time interferenceStart( currentTime + delay );
        const sc_core::sc_time interferenceEnd( interferenceStart + encodingTime );

        // Storing times & packet:
        infos.sendStart = interferenceStart;
        infos.sendEnd = interferenceEnd;
        infos.encodingTime = encodingTime;
        infos.packet = p;
        infos.packet.setChannel( this );

        _transmissions[ ID ][ i ].packetInfos.push_back( infos );

#if (SCNSL_DBG >= 5)
        std::stringstream ss;
        ss<< "<> send: start = " << interferenceStart
          << " end = " << interferenceEnd << " channel delay = "
          << _transmissions[ ID ][ i ].channelDelay << ".";
        SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

        // - Registering callbacks:

        const event_id_t start_event_id = _eventsQueue->registerAction(
            interferenceStart,
            this,
            & DelayedSharedChannel_t::_startSingleSending,
            & _transmissions[ ID ][ i ].idsInfos
            );

        // Storing the IDs of the events:
        _transmissions[ ID ][ i ].startEventsIds.push_back( start_event_id );

        const event_id_t complete_event_id = _eventsQueue->registerAction(
        	interferenceEnd,
            this,
            & DelayedSharedChannel_t::_completeSingleSending,
            & _transmissions[ ID ][ i ].idsInfos
            );

        // Storing the IDs of the events:
        _transmissions[ ID ][ i ].completeEventsIds.push_back( complete_event_id );
    }

    // Phase 2:
    SCNSL_TRACE_DBG( 2, p, "send(): waiting encoding time." );

    wait( encodingTime );

    // Phase 3:
    SCNSL_TRACE_DBG( 2, p, "send(): updating collision infos." );

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID
             || ! _transmissions[ i ][ ID ].reachable
             || _transmissions[ i ][ ID ].packetInfos.empty() )
            continue;

        if ( INTERSECT_SENDING_TIME( _single_infos[ ID ].sendStart,
        		                     _single_infos[ ID ].sendEnd,
        		                     _transmissions[ i ][ ID ].packetInfos.front().sendStart,
        		                     _transmissions[ i ][ ID ].packetInfos.front().sendEnd ) )
        {
            // collision!
            _transmissions[ i ][ ID ].packetInfos.front().collided = true;
        }
    }

    SCNSL_TRACE_DBG( 1, p, "<< send()." );
    return 0;
}


bool DelayedSharedChannel_t::isMultipoint()
    const
{
    SCNSL_TRACE_DBG( 5, "isMultipoint()." );
    return true;
}


void DelayedSharedChannel_t::updateProperties( const Node_t * n )

{
    SCNSL_TRACE_DBG( 1, "<> updateProperites()." );

	const Scnsl::Core::node_id_t ID = _ids[n];
    const Node_t::node_properties_t & np = n->getProperties( this );

    // Updating transmissions:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // A node can't be in more positions at the same time.
    // Check if there are multiple movements of the node at the same time:
    if ( _single_infos[ ID ].movementTime == currentTime )
        throw std::logic_error( "Multiple movement of the node." );

    // Show the coordinates of the node.
#if (SCNSL_LOG >= 1)
    std::stringstream ss;
    ss<< "Position of Node" << ID << ": x = " << np.x << " y = " << np.y << " z = " << np.z << ".";
    SCNSL_TRACE_LOG( 5, ss.str().c_str() );
#endif

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        const Node_t::node_properties_t & n2p = _single_infos[ i ].node->getProperties( this );

        // Updating properties with n as sender.
        _updateSingleProperties( ID, i, np, n2p );

        // Updating properties with n as receiver.
        _updateSingleProperties( i, ID, n2p, np );
    }

    _single_infos[ ID ].movementTime = currentTime;

}

// ////////////////////////////////////////////////////////////////
// Initialization methods.
// ////////////////////////////////////////////////////////////////

void DelayedSharedChannel_t::_initializeProperties ( const Node_t * n )
{
    SCNSL_TRACE_DBG( 1, "<> _initializeProperties()." );

	const Scnsl::Core::node_id_t ID = _ids[n];
    const Node_t::node_properties_t & np = n->getProperties( this );

    // Show the coordinates of the node.
#if (SCNSL_LOG >= 1)
    std::stringstream ss;
    ss<< "Position of Node" << ID << ": x = " << np.x << " y = " << np.y << " z = " << np.z << ".";
    SCNSL_TRACE_LOG( 5, ss.str().c_str() );
#endif

    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        const Node_t::node_properties_t & n2p = _single_infos[ i ].node->getProperties( this );

        // Initializing properties with n as sender.
        _initializeSingleProperties( ID, i, np, n2p );

        // Initializing properties with n as receiver.
        _initializeSingleProperties( i, ID, n2p, np );
    }
}

// ////////////////////////////////////////////////////////////////
// Callbacks.
// ////////////////////////////////////////////////////////////////


void DelayedSharedChannel_t::_startSingleSending( transmission_ids_infos_t * ids )
{

    SCNSL_TRACE_DBG( 1, "<> _startSingleSending()." );

	const Scnsl::Core::node_id_t ID = ids->sourceId;
	const Scnsl::Core::node_id_t recv_id = ids->destinationId;

    if ( ID == recv_id || ! _transmissions[ ID ][ recv_id ].reachable )
    {
    	// The _startSingleSending() ends thus we remove the event ID from the list:
   	    _transmissions[ ID ][ recv_id ].startEventsIds.pop_front();
        return;
    }

    // Updating transmissions:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // Check if the movement of the node was performed after
    // the packet has reached the limit of the transmission range.
    const sc_core::sc_time startTime =  _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart -
    		                            _transmissions[ ID ][ recv_id ].channelDelay;

    if ( currentTime > ( _transmissions[ ID ][ recv_id ].maxDelay + startTime ) )
    {
    	// Since receiver already started, this packet is corrupted.
    	_transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = true;
    	// The _startSingleSending() ends thus we remove the event ID from the list:
    	_transmissions[ ID ][ recv_id ].startEventsIds.pop_front();
    	return;
    }

    // Checking collisions:
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID || i == recv_id ) continue;

        if ( ! _transmissions[ i ][ recv_id ].reachable
             || _transmissions[ i ][ recv_id ].packetInfos.empty() ) continue;

        if ( INTERSECT_SENDING_TIME( _transmissions[ i ][ recv_id ].packetInfos.front().sendStart,
        		                     _transmissions[ i ][ recv_id ].packetInfos.front().sendEnd,
        		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart,
        		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd ))
        {
            // collision!
        	if ( ! _transmissions[ i ][ recv_id ].packetInfos.front().collided )
                _transmissions[ i ][ recv_id ].packetInfos.front().collided = true;
        	if ( ! _transmissions[ ID ][ recv_id ].packetInfos.front().collided )
                _transmissions[ ID ][ recv_id ].packetInfos.front().collided = true;
        }
    }

    // Carrier:
    ++ _single_infos[ recv_id ].active_carriers;
    if ( _single_infos[ recv_id ].active_carriers == 1
         && _single_infos[ recv_id ].old_carrier == false )
    {
        _single_infos[ recv_id ].old_carrier = true;
        _single_infos[ recv_id ].node->setCarrier( this, true );
    }

    	_transmissions[ ID ][ recv_id ].packetInfos.back().send_started = true;
    	_transmissions[ ID ][ recv_id ].startEventsIds.pop_front();

}


void DelayedSharedChannel_t::_completeSingleSending( transmission_ids_infos_t * ids )
{
    SCNSL_TRACE_DBG( 1, "<> _completeSingleSending()." );

    // Updating transmissions:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

	const Scnsl::Core::node_id_t ID = ids->sourceId;
	const Scnsl::Core::node_id_t recv_id = ids->destinationId;

    if ( ID == recv_id || ! _transmissions[ ID ][ recv_id ].reachable )
    {
        _transmissions[ ID ][ recv_id ].packetInfos.pop_front();
    	// The _completeSingleSending() ends thus we remove the event ID from the list:
   	    _transmissions[ ID ][ recv_id ].completeEventsIds.pop_front();
        return;
    }

    // Check if the movement of the node was performed after
    // the packet has reached the limit of the transmission range.
    const sc_core::sc_time startTime =  _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart -
    		                            _transmissions[ ID ][ recv_id ].channelDelay;

    if ( currentTime > ( _transmissions[ ID ][ recv_id ].maxDelay + startTime +
        _transmissions[ ID ][ recv_id ].packetInfos.front().encodingTime) )
    {
        // The entire packet has passed the transmission maximum range
    	// thus we remove the packet:
        _transmissions[ ID ][ recv_id ].packetInfos.pop_front();
    	// The _completeSingleSending() ends thus we remove the event ID from the list:
    	_transmissions[ ID ][ recv_id ].completeEventsIds.pop_front();
    	return;
    }

    SCNSL_TRACE_DBG( 3, "<> _completeSingleSending(): forwarding." );

    // Checking collisions:
    for ( unsigned int i = 0; i < _NODES_NUMBER; ++i )
    {
        if ( i == ID || i == recv_id ) continue;

        if ( ! _transmissions[ i ][ recv_id ].reachable
             || _transmissions[ i ][ recv_id ].packetInfos.empty() ) continue;

        if ( INTERSECT_SENDING_TIME( _transmissions[ i ][ recv_id ].packetInfos.front().sendStart,
        		                     _transmissions[ i ][ recv_id ].packetInfos.front().sendEnd,
        		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart,
        		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd ))
        {
            // collision!
            _transmissions[ i ][ recv_id ].packetInfos.front().collided = true;
            _transmissions[ ID ][ recv_id ].packetInfos.front().collided = true;
        }
    }

    // Checking with rx in sending mode:
    if ( INTERSECT_SENDING_TIME( _single_infos[ recv_id ].sendStart,
    		                     _single_infos[ recv_id ].sendEnd,
    		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart,
    		                     _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd ) )
    {
        // collision!
    	 _transmissions[ ID ][ recv_id ].packetInfos.front().collided = true;
    }

    // Forwarding:
    if ( ! _transmissions[ ID ][ recv_id ].packetInfos.front().collided
         && ! _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted
         && _transmissions[ ID ][ recv_id ].same_encoding_speed )
    {
    	if ( _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd < currentTime )
    	{
#if (SCNSL_LOG >= 1)
    std::stringstream ss;
    ss<< "Warning: due to a node movement we receive the packet. The simulation may not be correct.";
    SCNSL_TRACE_LOG( 3, ss.str().c_str() );
#endif
    	}
        Packet_t & p = _transmissions[ ID ][ recv_id ].packetInfos.front().packet;
        p.setReceivedPower(_transmissions[ ID ][ recv_id ].power);
        _single_infos[ recv_id ].node->receive(p);

    }

    // Carrier:
    -- _single_infos[ recv_id ].active_carriers;
    if ( _single_infos[ recv_id ].active_carriers == 0 )
    {
        // Do we have a continuous packet?
        const bool continuous = ( _transmissions[ ID ][ recv_id ].packetInfos.size() > 1 )
        &&( ( ++_transmissions[ ID ][ recv_id ].packetInfos.begin())->sendStart ==
        	 _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd );

        if ( ! continuous )
        {
            _single_infos[ recv_id ].old_carrier = false;
            _single_infos[ recv_id ].node->setCarrier( this, false );
        }
    }

    // Cleanup:
    _transmissions[ ID ][ recv_id ].packetInfos.pop_front();
	_transmissions[ ID ][ recv_id ].completeEventsIds.pop_front();
}


// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////


void DelayedSharedChannel_t::_initializeSingleProperties(
	const Scnsl::Core::node_id_t ID,
	const Scnsl::Core::node_id_t recv_id,
    const Node_t::node_properties_t & sp,
    const Node_t::node_properties_t & rp )

{
    SCNSL_TRACE_DBG( 3, "<> _initializeSingleProperties()." );

    Scnsl::Utils::Environment_if_t* env = Scnsl::Utils::Environment_if_t::getInstance();
    if ( ID == recv_id ) return;

    _transmissions[ ID ][ recv_id ].idsInfos.sourceId = ID;
    _transmissions[ ID ][ recv_id ].idsInfos.destinationId = recv_id;

    // Calculating the time for a packet to reach the limit
    // of the transmission range from the source node:
    const double max_delay = env->getMaxDelay(sp, rp);
    _transmissions[ ID ][ recv_id ].maxDelay = sc_core::sc_time( max_delay, sc_core::SC_SEC );

    // They must encode/decode at the same bitrate:
    _transmissions[ ID ][ recv_id ].same_encoding_speed = (fabs( sp.bitrate - rp.bitrate) < _EPSILON );

    // They must be inside the transmission maximum range:
    const position_t dx = sp.x - rp.x;
    const position_t dy = sp.y - rp.y;
    const position_t dz = sp.z - rp.z;
    const position_t d2 = dx*dx + dy*dy + dz*dz;
    const double distance = sqrt( d2 );

	double delay_time = 0;

    // Check if the nodes have the same coordinates:
    if ( distance < _EPSILON )
    {
    	// The nodes have the same coordinates.
		_transmissions[ ID ][ recv_id ].reachable = true;
    }
    else
    {
    	// The nodes have different coordinates.
        const double power = env->getReceiverPower(sp, rp);
        const bool pow_reac = power  >= rp.receiving_threshold;
        _transmissions[ ID ][ recv_id ].reachable = pow_reac;
        _transmissions[ ID ][ recv_id ].power = power;

        // Calculating the channel delay:
        delay_time = distance / env->getPropagation();
    }

    // Updating the channel delay
    _transmissions[ ID ][ recv_id ].channelDelay = sc_core::sc_time( delay_time, sc_core::SC_SEC );
}


void DelayedSharedChannel_t::_updateSingleProperties(
	const Scnsl::Core::node_id_t ID,
	const Scnsl::Core::node_id_t recv_id,
    const Node_t::node_properties_t & sp,
    const Node_t::node_properties_t & rp )

{
    SCNSL_TRACE_DBG( 3, "<> _updateSingleProperties()." );

    Scnsl::Utils::Environment_if_t* env = Scnsl::Utils::Environment_if_t::getInstance();

    if ( ID == recv_id ) return;

    _transmissions[ ID ][ recv_id ].idsInfos.sourceId = ID;
    _transmissions[ ID ][ recv_id ].idsInfos.destinationId = recv_id;

    // Updating transmissions:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // Calculating the time for a packet to reach the limit
    // of the transmission range from the source node:
    const double max_delay = env->getMaxDelay(sp, rp);
    _transmissions[ ID ][ recv_id ].maxDelay = sc_core::sc_time( max_delay, sc_core::SC_SEC );

    // Before all the movements of the nodes at current time we need
    // to know all the reachability values.
    // Storing this value for the source node:
    if ( _single_infos[ ID ].movementTime != currentTime && _single_infos[ recv_id ].movementTime != currentTime )
        _transmissions[ ID ][ recv_id ].old_reachable = _transmissions[ ID ][ recv_id ].reachable;

    // They must encode/decode at the same bitrate:
    _transmissions[ ID ][ recv_id ].same_encoding_speed = (fabs( sp.bitrate - rp.bitrate) < _EPSILON );

    // They must be inside the transmission maximum range:
    const position_t dx = sp.x - rp.x;
    const position_t dy = sp.y - rp.y;
    const position_t dz = sp.z - rp.z;
    const position_t d2 = dx*dx + dy*dy + dz*dz;
    const double distance = sqrt( d2 );

	double delay_time = 0;

    // Check if the nodes have the same coordinates:
    if ( distance < _EPSILON )
    {
    	// The nodes have the same coordinates.
		_transmissions[ ID ][ recv_id ].reachable = true;
    }
    else
    {
    	// The nodes have different coordinates.
        const double power = env->getReceiverPower(sp, rp);
        const bool pow_reac = power >= rp.receiving_threshold;
        _transmissions[ ID ][ recv_id ].reachable = pow_reac;
        _transmissions[ ID ][ recv_id ].power = power;

        // Calculating the channel delay:
        delay_time = distance / env->getPropagation();
    }

    // Before all the movements of the nodes at current time we need
    // to know the current delay from ID to recv_id.
    // Storing this value:
    if ( _single_infos[ ID ].movementTime != currentTime && _single_infos[ recv_id ].movementTime != currentTime )
        _transmissions[ ID ][ recv_id ].oldChannelDelay = _transmissions[ ID ][ recv_id ].channelDelay;

    // Updating the channel delay:
    _transmissions[ ID ][ recv_id ].channelDelay = sc_core::sc_time( delay_time, sc_core::SC_SEC );

    // Registering callback

    // After updating the nodes properties we register a method
    // that updates the nodes-relative events:
    _eventsQueue->registerAction(
        currentTime,
        this,
        & DelayedSharedChannel_t::_updateEvents,
        & _transmissions[ ID ][ recv_id ].idsInfos
        );
}


void DelayedSharedChannel_t::_updateEvents(  transmission_ids_infos_t * ids )
{
    SCNSL_TRACE_DBG( 3, "<> _updateEvents()." );

	const Scnsl::Core::node_id_t ID = ids->sourceId;
	const Scnsl::Core::node_id_t recv_id = ids->destinationId;

    // 1 - Updating transmissions:
    const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

    // 2 - Update the _startSingleSending-relative events:

	// Check if there are elements in the startEventsIds queue:
	if ( ! _transmissions[ ID ][ recv_id ].startEventsIds.empty() )
	{
	    // Updating events:
	    for ( std::list< event_id_t >::iterator event_it = _transmissions[ ID ][ recv_id ].startEventsIds.begin();
	          event_it != _transmissions[ ID ][ recv_id ].startEventsIds.end(); ++event_it )
	    {
	        // Storing old event time:
	        const sc_core::sc_time oldTime = _eventsQueue->getActionTime( *event_it );

	        // Calculating new event time:
	        const sc_core::sc_time newTime = ( oldTime - _transmissions[ ID ][ recv_id ].oldChannelDelay ) +
											   _transmissions[ ID ][ recv_id ].channelDelay;

		    // Updating event:
		    _eventsQueue->updateAction( *event_it, newTime );

	        // Updating the packet-relative infos:
		    for ( std::list< packet_infos_t >::iterator packet_it = _transmissions[ ID ][ recv_id ].packetInfos.begin();
		          packet_it != _transmissions[ ID ][ recv_id ].packetInfos.end();
		          ++packet_it )
		    {
		    	if ( packet_it->sendStart == oldTime )
		    	{
		    		packet_it->sendStart = newTime;
		    		break;
		    	}
		    }
	    }
	}

    // 3 - Update the _completeSingleSending-relative events:

	// Check if there are elements in the completeEventsIds queue:
	if ( ! _transmissions[ ID ][ recv_id ].completeEventsIds.empty() )
	{
		// Updating events:
		for ( std::list< event_id_t >::iterator event_it = _transmissions[ ID ][ recv_id ].completeEventsIds.begin();
		      event_it != _transmissions[ ID ][ recv_id ].completeEventsIds.end(); ++event_it )
		{
		    // The event of the packet currently under encoding must not be updated.
		    if ( event_it == _transmissions[ ID ][ recv_id ].completeEventsIds.begin() && _transmissions[ ID ][ recv_id ].packetInfos.front().send_started )
		    	continue;

		    // Storing old event time:
		    const sc_core::sc_time oldTime = _eventsQueue->getActionTime( *event_it );

		    // Calculating new event time:
		    const sc_core::sc_time newTime = ( oldTime - _transmissions[ ID ][ recv_id ].oldChannelDelay ) +
		    		                           _transmissions[ ID ][ recv_id ].channelDelay;

		    // Updating event:
		    _eventsQueue->updateAction( *event_it, newTime );

		    // Updating the packet-relative infos:
			for ( std::list< packet_infos_t >::iterator packet_it = _transmissions[ ID ][ recv_id ].packetInfos.begin();
			      packet_it != _transmissions[ ID ][ recv_id ].packetInfos.end();
			      ++packet_it )
			{
			    if ( packet_it->sendEnd == oldTime )
			    {
			        packet_it->sendEnd = newTime;
					break;
			    }
			}
		}
	}

    // 4 - Checking packets corruption:

    // The only packet possibly affected, is the one
    // currently under encoding.
    // Checking this packet:
    if ( currentTime < _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd  // Not yet send completed
	    && currentTime > ( _transmissions[ ID ][ recv_id ].packetInfos.front().sendStart ) // Recv started
        &&  ! _transmissions[ ID ][ recv_id ].packetInfos.empty() ) // This check should be unuseful...
	{
	    if ( _transmissions[ ID ][ recv_id ].reachable && ! _transmissions[ ID ][ recv_id ].old_reachable )
	    {
	        // Was NOT reachable, but now it is.
            // Since internal_recv already started, this pck is corrupted.
		    _transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = true;

			// If required, updating sending infos:
			if ( _transmissions[ ID ][ recv_id ].packetInfos.front().send_started )
			{
		        const event_id_t event_id = _eventsQueue->registerAction(
		            currentTime,
		            this,
                    & DelayedSharedChannel_t::_startSingleSending,
		            & _transmissions[ ID ][ recv_id ].idsInfos
		            );

		        // Storing the IDs of the events:
		        _transmissions[ ID ][ recv_id ].startEventsIds.push_back( event_id );
			}
	    }
	    else if ( ! _transmissions[ ID ][ recv_id ].reachable && _transmissions[ ID ][ recv_id ].old_reachable )
	    {
		    // Was reachable, but now it is no more.
            // Since internal_recv already started, this pck is corrupted.
	    	_transmissions[ ID ][ recv_id ].packetInfos.front().corrupted = true;

	    	// Since it is no more reachable, we manage the carrier.
		    -- _single_infos[ recv_id ].active_carriers;
			if ( _single_infos[ recv_id ].active_carriers == 0 )
			{
			    // Do we have a continuous packet?
			    const bool continuous = ( _transmissions[ ID ][ recv_id ].packetInfos.size() > 1 )
			    &&( ( ++_transmissions[ ID ][ recv_id ].packetInfos.begin())->sendStart ==
			    _transmissions[ ID ][ recv_id ].packetInfos.front().sendEnd );

			    if ( ! continuous )
			    {
			        _single_infos[ recv_id ].old_carrier = false;
			        _single_infos[ recv_id ].node->setCarrier( this, false );
			    }
			}
        }
	}
}
