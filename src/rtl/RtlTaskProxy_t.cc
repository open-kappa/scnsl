// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// RTL taskproxy.

#include "scnsl/rtl/RtlTaskProxy_t.hh"
#include <sstream>

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using Scnsl::Rtl::RtlTaskProxy_t;



// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////


RtlTaskProxy_t::RtlTaskProxy_t( const sc_core::sc_module_name modulename,
                                Scnsl::Core::Task_if_t * t,
                                std::string bindIdentifier,
                                Scnsl::Core::Channel_if_t * ch )
     :
    // Parents:
    Scnsl::Core::TaskProxy_if_t( modulename, t, ch ),
    // Ports for task inputs.
    carrier( "carrier" ),
    inputPacketSize( "inputPacketSize" ),
    newInputPacket( "newInputPacket" ),
	inputPacketLabel( "inputPacketLabel" ),
    inputPacketPower( "inputPacketPower" ),
    inputPacket( nullptr ),
    // Ports for task outputs.
    outputPacketSize( "outputPacketSize" ),
    newOutputPacket( "newOutputPacket" ),
	outputPacketLabel( "outputPacketLabel" ),
    packetSendCompleted( "packetSendCompleted" ),
    outputPacket( nullptr ),
    // Internal fields:
    _new_carrier( false ),
    _receivedPacket(),
    _outgoingPacket(),
    _changeCarrier(),
    _forwardPacket(),
    _packet_send_completed( 0 ),
    _new_input_packet( 0 ),
    _delete_ports( nullptr )
{
    RtlTask_base_if_t * rtlTask = dynamic_cast< RtlTask_base_if_t * >( _task );

    if ( rtlTask == nullptr )
    {
        throw std::invalid_argument( "Required an RTL task." );
    }

    // Creating and connecting the packet ports.
    _delete_ports = rtlTask->createAndBoundPorts( inputPacket, outputPacket, this, bindIdentifier );


    // Setting just once, since it is always the same:
    _outgoingPacket.setSourceTaskProxy( this );
    // _outgoingPacket.setDestinationTaskProxy(  );
    _outgoingPacket.setSourceNode( _task->getNode() );
    _outgoingPacket.setDestinationNode( getDestinationNode() );
    _outgoingPacket.setChannel( _channel );


    // Creating processes.

    SC_THREAD( _readingProcess );
    sensitive << newOutputPacket;

    SC_METHOD( _forwardProcess );
    sensitive << _forwardPacket;
    dont_initialize();

    SC_METHOD( _changeCarrierProcess );
    sensitive << _changeCarrier;
    dont_initialize();
}

RtlTaskProxy_t::~RtlTaskProxy_t()
{
    (* _delete_ports)( inputPacket, outputPacket );
}

// ////////////////////////////////////////////////////////////////
// Reimplemented interface methods.
// ////////////////////////////////////////////////////////////////

void RtlTaskProxy_t::setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c )

{
    SCNSL_TRACE_LOG( 3, ch, c, "<> setCarrier()." );

    _new_carrier = c;
    _changeCarrier.notify();

    // To suppress compiler warnings:
    ch = nullptr;
}

RtlTaskProxy_t::errorcode_t RtlTaskProxy_t::receive( const Scnsl::Core::Packet_t & p )

{
    if (p.getDestTaskProxy() != nullptr && p.getDestTaskProxy() != this)
        return 0;
    SCNSL_TRACE_LOG( 3, p, "<> receive()." );

    _receivedPacket = p;
    _forwardPacket.notify();

    return 0;
}



// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void RtlTaskProxy_t::_readingProcess()
{
	for (;;)
	{
		wait();

    	static_cast< RtlTask_base_if_t * >( _task )->createInternalPacket(
    	    _outgoingPacket,
    	    outputPacket,
    	    outputPacketSize.read() );
		_outgoingPacket.setLabel( outputPacketLabel.read() );
        _outgoingPacket.setDestinationTask( _destinationTask );
        _outgoingPacket.setId( Scnsl::Core::Packet_t::get_new_packet_id() );

        SCNSL_TRACE_DBG( 1, _outgoingPacket, "<> _readingProcess()." );

    	send( _outgoingPacket );

    	packetSendCompleted.write( ++ _packet_send_completed );
	}
}

void RtlTaskProxy_t::_forwardProcess()
{
    static_cast< RtlTask_base_if_t * >( _task )->writeInternalPacket(
        _receivedPacket, inputPacket );

    SCNSL_TRACE_DBG( 1, _receivedPacket, "_forwardProcess().");

    inputPacketSize.write( _receivedPacket.getSize() );
	inputPacketLabel.write( _receivedPacket.getLabel() );
    inputPacketPower.write( _receivedPacket.getReceivedPower() );
    newInputPacket.write( ++ _new_input_packet );
}

void RtlTaskProxy_t::_changeCarrierProcess()
{
#if (SCNSL_DBG >= 3 )
    std::stringstream ss;
    ss << "<> _changeCarrierProcess(): " << _new_carrier << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif
    carrier.write( _new_carrier );
}
