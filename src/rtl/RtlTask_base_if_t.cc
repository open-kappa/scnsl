// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A base class for basic RTL tasks.

#include <string>
#include <sstream>

#include "scnsl/rtl/RtlTask_base_if_t.hh"


using Scnsl::Rtl::RtlTask_base_if_t;

// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

RtlTask_base_if_t::~RtlTask_base_if_t()
{
    // ntd
}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

RtlTask_base_if_t::size_t RtlTask_base_if_t::getProxiesNumber()
    const
{
    SCNSL_TRACE_DBG( 5, "<> getProxiesNumber()." );
    return _PROXIES_NUMBER;
}

RtlTask_base_if_t::size_t RtlTask_base_if_t::getBoundedProxiesNumber()
    const
{
    SCNSL_TRACE_DBG( 5, "<> getBoundedProxiesNumber()." );
    return _bounded_proxies;
}



// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////


RtlTask_base_if_t::RtlTask_base_if_t( const sc_core::sc_module_name modulename,
                                      const task_id_t id,
                                      Scnsl::Core::Node_t * n,
                                      const size_t proxies ):
    // Parent:
    Task_if_t( modulename, id, n ),
    // Ports for inputs:
    carrier("carrier", proxies),
    inputPacketSize("inputPacketSize", proxies),
    inputPacketLabel("inputPacketLabel", proxies),
    inputPacketPower("inputPacketPower", proxies),
    newInputPacket("newInputPacket", proxies),
    // Ports for outputs:
    outputPacketSize("outputPacketSize", proxies),
    newOutputPacket("newOutputPacket", proxies),
    outputPacketLabel("outputPacketLabel", proxies),
    packetSendCompleted("packetSendCompleted", proxies),
    // Fields:
    _PROXIES_NUMBER( proxies ),
    _bounded_proxies( 0 )
{
    if ( _PROXIES_NUMBER == 0 )
    {
        throw std::invalid_argument( "Task with 0 proxies associated." );
    }
}
