// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A basic task interface.

#include <sstream>
#include <string>

#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/TaskProxy_if_t.hh"
#include "scnsl/core/Channel_if_t.hh"
#include "scnsl/core/Node_t.hh"


using Scnsl::Core::Task_if_t;
using Scnsl::Core::Node_t;
using Scnsl::Core::TaskProxy_if_t;
using Scnsl::Core::Channel_if_t;
using Scnsl::Core::node_properties_t;

// ////////////////////////////////////////////////////////////////
// Public methods.
// ////////////////////////////////////////////////////////////////


Task_if_t::~Task_if_t()
{
    // Nothing to do.
}

Task_if_t::task_id_t Task_if_t::getId()
    const
{
#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss << "<> getId() " << _ID << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str());
#endif

    return _ID;
}

Scnsl::Core::Node_t * Task_if_t::getNode()
{
    SCNSL_TRACE_DBG( 5, "<> getNode().");

    return _node;
}

const Scnsl::Core::Node_t * Task_if_t::getNode()
    const
{
    SCNSL_TRACE_DBG( 5, "<> getNode().");

    return _node;
}

const Scnsl::Core::node_properties_t &Task_if_t::getNodeProperties(const std::string & id) const
{
    Proxies::const_iterator it = _proxies.find( id );
    if (it == _proxies.end()) throw "No such proxy";
    TaskProxy_if_t * proxy = it->second;
    const Channel_if_t * ch = proxy->getChannel();
    const Node_t * n = getNode();
    return n->getProperties( ch );
}

void Task_if_t::setNodeProperties(const std::string & id, const node_properties_t & properties, const bool noUpdate)
{
    Proxies::iterator it = _proxies.find( id );
    if (it == _proxies.end()) throw "No such proxy";
    TaskProxy_if_t * proxy = it->second;
    Channel_if_t * ch = const_cast <Channel_if_t*> (proxy->getChannel());
    Node_t * n = getNode();
    n->setProperties(properties, ch, noUpdate);
}


// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////

Task_if_t::Task_if_t( const sc_core::sc_module_name modulename,
                      const task_id_t id,
                      Node_t * n ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Members:
    _ID( id ),
    _node( n ),
    _bindId(),
    _bindIdMap(),
    _proxies()
{
    // Nothing to do.
}
