// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
///  Basic data types.



#include <scnsl/core/SocketMap.hh>
#include "scnsl/core/data_types.hh"

namespace tlm
{
#if SCNSL_SYSTEMC_VERSION_LT(2, 3, 2)
const unsigned int tlm_extension<Scnsl::Core::data_extension_t>::ID = tlm_extension_base::register_extension();
#else
const unsigned int tlm_extension<Scnsl::Core::data_extension_t>::ID = tlm_extension_base::register_extension(typeid(Scnsl::Core::data_extension_t));
#endif

tlm_extension<Scnsl::Core::data_extension_t>::~tlm_extension()
{
    // ntd
}

} // tlm


using namespace Scnsl::Core;

// /////////////////////////////////////////////////////////////////////////////
// node_properties_t
// /////////////////////////////////////////////////////////////////////////////

node_properties_t::node_properties_t()
    :id(0),
      sourceNode(),
      x(0),
      y(0),
      z(0),
      bitrate(0),
      transmission_power(0),
      receiving_threshold(0),
      lobes()
{
    // ntd
}

node_properties_t::node_properties_t(const node_properties_t & np):
    id(np.id),
    sourceNode(np.sourceNode),
    x(np.x),
    y(np.y),
    z(np.z),
    bitrate(np.bitrate),
    transmission_power(np.transmission_power),
    receiving_threshold(np.receiving_threshold),
    lobes(np.lobes)
{
    // ntd
}

node_properties_t &node_properties_t::operator=(const node_properties_t & np)
{
    if (this == &np) return *this;
    id = np.id;
    x = np.x;
    y = np.y;
    z = np.z;
    bitrate = np.bitrate;
    transmission_power = np.transmission_power;
    receiving_threshold = np.receiving_threshold;
    lobes = np.lobes;
    return *this;
}


// /////////////////////////////////////////////////////////////////////////////
// node_factory_infos_t
// /////////////////////////////////////////////////////////////////////////////

node_factory_infos_t::node_factory_infos_t():
    nodeStepX(0),
    nodeStepY(0),
    nodeStepZ(0),
    nodeNumberX(1),
    nodeNumberY(1),
    nodeNumberZ(1)
{}


// /////////////////////////////////////////////////////////////////////////////
// data_extension_t
// /////////////////////////////////////////////////////////////////////////////

data_extension_t::data_extension_t():
    tlm::tlm_extension< data_extension_t >(),
    label(0),
    power(0.0)
{
    // ntd
}

data_extension_t::~data_extension_t()
{
    // ntd
}

data_extension_t::data_extension_t(const data_extension_t & other):
    tlm::tlm_extension< data_extension_t >(other),
    label(other.label),
    power(other.power)
{
    // ntd
}

data_extension_t &data_extension_t::operator =(data_extension_t other)
{
    swap(other);
    return *this;
}

void data_extension_t::swap(data_extension_t & other)
{
    // parent has no members to swap!
    std::swap(label, other.label);
    std::swap(power, other.power);
}

tlm::tlm_extension_base *data_extension_t::clone() const
{
    return new data_extension_t(*this);
}

void data_extension_t::copy_from(const tlm::tlm_extension_base & ext)
{
    if (typeid(*this) != typeid(ext)) throw "data_extension_t::copy_from(): typeid error";
    *this = static_cast<const data_extension_t &>(ext);
}

// /////////////////////////////////////////////////////////////////////////////
// socket_properties_t
// /////////////////////////////////////////////////////////////////////////////

socket_properties_t::socket_properties_t():
    hostname("NULL"),
    source_ip(0),
    source_port(0),
    dest_ip(0),
    dest_port(0),
    socket_active(false)
{

}

socket_properties_t::socket_properties_t(const socket_properties_t & sp):
    hostname("NULL"),
    source_ip(sp.source_ip),
    source_port(sp.source_port),
    dest_ip(sp.dest_ip),
    dest_port(sp.dest_port),
    socket_active(sp.socket_active)

{
    
}

socket_properties_t & socket_properties_t::operator=(
    const socket_properties_t & sp)
{
    if (this == &sp) return *this;
    hostname = sp.hostname;
    source_ip = sp.source_ip;
    source_port = sp.source_port;
    dest_ip = sp.dest_ip;
    dest_port = sp.dest_port;
    socket_active = sp.socket_active;
    return *this;
}

bool socket_properties_t::operator==(const socket_properties_t & s) const
{
    std::cerr << "Comparing two socket" << std::endl;
    return source_ip == s.source_ip &&  // no check for name
        source_port == s.source_port && dest_ip == s.dest_ip
        && dest_port == s.dest_port;
}

bool socket_properties_t::operator!=(const socket_properties_t & rhs) const
{
    return !(rhs == *this);
}

bool socket_properties_t::operator<(const socket_properties_t & s) const
{
    // check done only on important value, hostname is not one of them
    if (source_ip == s.source_ip)
    {
        if (source_port == s.source_port)
        {
            if (dest_port == s.dest_port) return dest_ip < s.dest_ip;
            return dest_port < s.dest_port;
        }
        return source_port < s.source_port;
    }
    return source_ip < s.source_ip;
}

std::ostream & Scnsl::Core::operator<<(std::ostream & os, const socket_properties_t & t)
{
    os << "< " << Scnsl::Core::SocketMap::getIP(t.source_ip) << " "
       << t.source_port << " " << Scnsl::Core::SocketMap::getIP(t.dest_ip)
       << " " << t.dest_port << " >";
    return os;
}
