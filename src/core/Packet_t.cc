// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A generic packet.


#include <cstring>

#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/TaskProxy_if_t.hh"

using Scnsl::Core::Packet_t;
using Scnsl::Core::TaskProxy_if_t;
using Scnsl::Core::Task_if_t;
using Scnsl::Core::Node_t;
using Scnsl::Core::Channel_if_t;


// ////////////////////////////////////////////////////////////////
// Regular design pattern.
// ////////////////////////////////////////////////////////////////

Packet_t::Packet_t():
// Payload fields.
_protocolPacket(nullptr),
// Header fields.
_is_ack( false ),
_is_ack_required( false ),
_sequence_number( 0 ),
_label( 0 ),
_header_size( 0 ),
_sourceTaskproxy( nullptr ),
_destTaskproxy(nullptr ),
_destinationTask( nullptr ),
_sourceNode( nullptr ),
_destinationNode( nullptr ),
// Simulation fields.
_id( 0 ),
_channel( nullptr ),
_power(0.0),
// To be removed.
_is_valid( true ),
_sourceProperties(),
_command(NONE),
_type(DATA_PACKET)   //default is data packet
{
    // Nothing to do.
}

Packet_t::Packet_t( const Packet_t & p ):
// Payload fields.
_protocolPacket(nullptr),
// Header fields.
_is_ack( p._is_ack ),
_is_ack_required( p._is_ack_required ),
_sequence_number( p._sequence_number ),
_label( p._label ),
_header_size( p._header_size ),
_sourceTaskproxy( p._sourceTaskproxy ),
_destTaskproxy( p._destTaskproxy ),
_destinationTask( p._destinationTask ),
_sourceNode( p._sourceNode ),
_destinationNode( p._destinationNode ),
// Simulation fields.
_id( p._id ),
_channel( p._channel ),
_power(p._power),
// To be removed.
_is_valid( p._is_valid ),
_sourceProperties(p._sourceProperties),
_command(p._command),
_type(p._type)
{
    if (p._protocolPacket != nullptr)
        _protocolPacket = p._protocolPacket->clone();
}


Packet_t::~Packet_t()
{
    delete _protocolPacket;
}

Packet_t & Packet_t::operator = ( const Packet_t & p )

{
    // Packet fields.
    // Exception neutral.
    if (p._protocolPacket != nullptr)
    {
        auto tmp = p._protocolPacket->clone();
        delete _protocolPacket;
        _protocolPacket = tmp;
    }
    else
    {
        delete _protocolPacket;
        _protocolPacket = nullptr;
    }
    // Header fields.
    _is_ack = p._is_ack;
    _is_ack_required = p._is_ack_required;
    _sequence_number = p._sequence_number;
    _label = p._label;
    _header_size = p._header_size;
    _sourceTaskproxy = p._sourceTaskproxy;
    _destTaskproxy= p._destTaskproxy;
    _destinationTask = p._destinationTask;
    _sourceNode = p._sourceNode;
    _destinationNode = p._destinationNode;
    // Simulation fields.
    _id = p._id;
    _channel = p._channel;
    _power = p._power;
    // To be removed.
    _is_valid = p._is_valid;
    _sourceProperties = p._sourceProperties;
    _command = p._command;
    _type = p._type;
    return *this;
}


// ////////////////////////////////////////////////////////////////
// Payload related methods.
// ////////////////////////////////////////////////////////////////

Packet_t::size_t Packet_t::getPayloadSize()
    const
{
    if(_protocolPacket != nullptr)
        return _protocolPacket->getSize();
    return 0;
}

Packet_t::size_t Packet_t::getPayloadSizeInBytes()
    const
{
    return to_bytes( getPayloadSize() );
}

void Packet_t::setPayload(const Scnsl::Core::ProtocolPacket_if_t &protocolPacket)
{
    delete _protocolPacket;
    _protocolPacket = protocolPacket.clone();
}

Scnsl::Core::ProtocolPacket_if_t *Packet_t::getPayload() const
{
    assert(_protocolPacket != nullptr);
    return _protocolPacket;
}

Packet_t::byte_t* Packet_t::getInnerPayload( ) const
{
    assert (_protocolPacket != nullptr);
    byte_t* payload = _protocolPacket->getInnerBuffer();
    return payload;
}

Packet_t::size_t Packet_t::getInnerSize()
    const
{
    assert (_protocolPacket != nullptr);
    return _protocolPacket->getInnerSize();
}

Packet_t::size_t Packet_t::getInnerSizeInBytes()
    const
{
    assert (_protocolPacket != nullptr);
    return _protocolPacket->getInnerSizeInBytes();
}


// ////////////////////////////////////////////////////////////////
// Header methods.
// ////////////////////////////////////////////////////////////////

Packet_t::size_t Packet_t::getHeaderSize()
    const
{
    return _header_size;
}


Packet_t::size_t Packet_t::getHeaderSizeInBytes()
    const
{
    return to_bytes( _header_size );
}

void Packet_t::setHeaderSize( const size_t s )
{
    _header_size = s;
}

void Packet_t::setHeaderSizeInBytes( const size_t s )
{
    _header_size = to_bits( s );
}

bool Packet_t::isAck()
    const
{
    return _is_ack;
}


void Packet_t::setAck( const bool ack )
{
    _is_ack = ack;
}


bool Packet_t::isAckRequired()
    const
{
    return _is_ack_required;
}


void Packet_t::setAckRequired( const bool ack )
{
    _is_ack_required = ack;
}


Packet_t::counter_t Packet_t::getSequenceNumber()
    const
{
    return _sequence_number;
}


void Packet_t::setSequenceNumber( const counter_t seqno )
{
    _sequence_number = seqno;
}


Packet_t::label_t Packet_t::getLabel()
    const
{
    return _label;
}

void Packet_t::setLabel( const label_t l )
{
    _label = l;
}


void Packet_t::setSourceTaskProxy( const TaskProxy_if_t * tp )
{
    _sourceTaskproxy = tp;
}


const Scnsl::Core::TaskProxy_if_t * Packet_t::getSourceTaskProxy()
    const
{
    return _sourceTaskproxy;
}

void Packet_t::setDestTaskProxy( const TaskProxy_if_t * tp )
{
    _destTaskproxy = tp;
}


const Scnsl::Core::TaskProxy_if_t * Packet_t::getDestTaskProxy()
    const
{
    return _destTaskproxy;
}

void Packet_t::setDestinationTask( const Task_if_t * t )
{
    _destinationTask = t;
}

const Scnsl::Core::Task_if_t * Packet_t::getDestinationTask()
    const
{
    return _destinationTask;
}


void Packet_t::setSourceNode( const Node_t * n )
{
    _sourceNode = n;
}

const Scnsl::Core::Node_t * Packet_t::getSourceNode()
    const
{
    if ( _sourceNode != nullptr ) return _sourceNode;
    if ( _sourceTaskproxy != nullptr ) return _sourceTaskproxy->getTask()->getNode();
    return nullptr;
}

void Packet_t::setDestinationNode( const Node_t * n )
{
    _destinationNode = n;
}

const Scnsl::Core::Node_t * Packet_t::getDestinationNode()
    const
{
    if ( _destinationNode != nullptr ) return _destinationNode;
    if ( _destinationTask != nullptr ) return _destinationTask->getNode();
    return nullptr;
}


Scnsl::Core::PacketCommand Packet_t::getCommand() const
{
    return _command;
}


Scnsl::Core::PacketType Packet_t::getType() const
{
    return _type;
}


void Scnsl::Core::Packet_t::setCommand(Scnsl::Core::PacketCommand command)
{
    _command = command;
}


void Scnsl::Core::Packet_t::setType(const Scnsl::Core::PacketType type)
{
    _type = type;
}


Packet_t::size_t Packet_t::getSize()
    const
{
   return getPayloadSize() + getHeaderSize();
}

Packet_t::size_t Packet_t::getSizeInBytes()
    const
{
    return to_bytes(getSize());
}


void Packet_t::setSourceProperties(node_properties_t & sp)
{
     _sourceProperties = sp;
}


const Packet_t::node_properties_t & Packet_t::getSourceProperties()
     const
{
    return _sourceProperties;
}

Packet_t::power_t Packet_t::getReceivedPower() const
{
    return _power;
}

void Packet_t::setReceivedPower(const Packet_t::power_t power)
{
    _power = power;
}


// ////////////////////////////////////////////////////////////////
// Simulation methods.
// ////////////////////////////////////////////////////////////////


Packet_t::packet_id_t Packet_t::getId()
    const
{
    return _id;
}

void Packet_t::setId( const packet_id_t id )
{
    _id = id;
}

void Packet_t::setChannel( const Channel_if_t * ch )
{
    _channel = ch;
}

const Scnsl::Core::Channel_if_t * Packet_t::getChannel()
    const
{
    return _channel;
}



// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////



bool Packet_t::operator == ( const Packet_t & p )
    const
{
    if ( _protocolPacket == nullptr && p._protocolPacket != nullptr ) return false;
    if ( _protocolPacket != nullptr && p._protocolPacket == nullptr ) return false;
    // Header fields.
    if ( _is_ack != p._is_ack
         || _is_ack_required !=  p._is_ack_required
         || _sequence_number != p._sequence_number
         || _label != p._label
         || _header_size != p._header_size
         || _sourceTaskproxy != p._sourceTaskproxy
         || _destTaskproxy != p._destTaskproxy
         || _destinationTask != p._destinationTask
         || _sourceNode != p._sourceNode
         || _destinationNode != p._destinationNode
         || _command != p._command
         || _type != p._type )
        return false;
    return (*_protocolPacket) == (*p._protocolPacket) ;
}



// ////////////////////////////////////////////////////////////////
// To be removed methods.
// ////////////////////////////////////////////////////////////////


bool Packet_t::isValid()
    const
{
    return _is_valid;
}

void Packet_t::setValidity( const bool v )
{
    _is_valid = v;
}



// ////////////////////////////////////////////////////////////////
// Static support methods.
// ////////////////////////////////////////////////////////////////


Packet_t::packet_id_t Packet_t::get_new_packet_id()
{
    static packet_id_t id = 0;
    return id++;
}


Packet_t::size_t Packet_t::to_bytes( const size_t s )
{
    return (s / 8) + ( s % 8 ? 1 : 0);
}

Packet_t::size_t Packet_t::to_bits( const size_t s )
{
    return s * 8;
}
