// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A channel interface.


#include "scnsl/core/Node_t.hh"
#include "scnsl/core/Channel_if_t.hh"


using Scnsl::Core::Channel_if_t;
using Scnsl::Core::Node_t;

// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

Channel_if_t::~Channel_if_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void Channel_if_t::addNode( Node_t * n )

{
    SCNSL_TRACE_DBG( 5, "<> addNode()." );
    _nodes.push_back( n );
}

void Channel_if_t::updateProperties( const Node_t * /*n*/ )

{ }


// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

Channel_if_t::Channel_if_t( const sc_core::sc_module_name modulename ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Fields:
    _nodes()
{
    // Nohting to do.
}
