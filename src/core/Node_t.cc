// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A network node.


#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/Channel_if_t.hh"
#include "scnsl/core/Node_t.hh"

using Scnsl::Core::Node_t;
using Scnsl::Core::Packet_t;
using Scnsl::Core::Communicator_if_t;
using Scnsl::Core::TaskProxy_if_t;
using Scnsl::Core::Channel_if_t;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

Node_t::Node_t(const node_id_t id):
    // Parents:
    Communicator_if_t(),
    // Fields:
    _properties(),
    _status( true ),
    _id(id)
{
    // Nothing to do.
}

Node_t::~Node_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Communicator interface methods.
// ////////////////////////////////////////////////////////////////


Node_t::errorcode_t Node_t::send( const Packet_t & p )

{
    if (p.getPayload() != nullptr && p.getType() == COMMAND_PACKET) //command arrived here, error (cannot send a command packet)
    {
        std::cerr<< "ERROR :Command arrived to a node"<<std::endl;
        return -1;
    }
    if ( ! _status ) return 1;

    Scnsl::Core::node_properties_t sp = this->getProperties(p.getChannel());
    Packet_t * alias = const_cast<Packet_t*>(&p);
    alias->setSourceProperties(sp);

    return const_cast< Channel_if_t * >(p.getChannel())->send( this, p );
}


Node_t::errorcode_t Node_t::receive( const Packet_t & p )

{
    if ( ! _status ) return 1;
    return Communicator_if_t::receive( p );
}


// ////////////////////////////////////////////////////////////////
// Erroneous Communicator_if_t methods.
// ////////////////////////////////////////////////////////////////


void Node_t::bindTaskProxy( const TaskProxy_if_t * /*tp*/,
                            Communicator_if_t * /*c*/ )

{
    throw std::logic_error( "Invalid call of bind on a node." );
}


void Node_t::stackDown( Communicator_if_t * /*c*/ )

{
    throw std::logic_error( "invalid call to stackDown() on a node." );
}



// ////////////////////////////////////////////////////////////////
// Node specific methods.
// ////////////////////////////////////////////////////////////////

const Node_t::node_properties_t & Node_t::getProperties( const Channel_if_t * ch )
    const
{
    Properties_t::const_iterator i = _properties.find( ch );
    if ( i == _properties.end() ) throw std::domain_error( "Channel not binded." );
    return i->second;
}


void Node_t::setProperties( const node_properties_t & p , Channel_if_t * ch, const bool no_update )
{
    _properties[ ch ] = p;
    if ( ! no_update ) ch->updateProperties( this );
}


bool Node_t::getNodeStatus()
    const
{
    return _status;
}


void Node_t::setNodeStatus( const bool s )
{
    _status = s;
}

Node_t::node_id_t Node_t::getId()
    const
{
    return _id;
}
