#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
#include <cmath>
#include <limits>

#include "scnsl/core/RadiationPattern_if_t.hh"
#include "scnsl/core/Coordinate_t.hh"

using Scnsl::Core::Coordinate_t;

Coordinate_t::Coordinate_t(
        const bool isPolar,
        const double x_theta,
        const double y_phy,
        const double z_module):
    _isPolar(isPolar),
    _first(x_theta),
    _second(y_phy),
    _third(z_module),
    _x(),
    _y(),
    _z(),
    _t(),
    _p(),
    _m()
{
    if (_isPolar)
    {
        _t = _first;
        _p = _second;
        _m = _third;

        _x = _m * cos(_t * M_PI / 180);
        _y = _m * sin(_t * M_PI / 180) * cos(_p * M_PI / 180);
        _z = _m * sin(_t * M_PI / 180) * sin(_p * M_PI / 180);
    }
    else
    {
        _x = _first;
        _y = _second;
        _z = _third;

        _m = sqrt( _x * _x + _y * _y + _z * _z );
        _p = ( atan( _z / _y ) * 180) / M_PI;
        _t = ( atan( sqrt( _z * _z + _y * _y ) / _x ) * 180) / M_PI;

        if (_y < 0) { _p += 180; }
        if (_p < 0) { _p += 360; }

    }
}

Coordinate_t::~Coordinate_t()
{
    // Nothing to do.
}

Coordinate_t::Coordinate_t(const Scnsl::Core::Coordinate_t & other):
    _isPolar(other._isPolar),
    _first(other._first),
    _second(other._second),
    _third(other._third),
    _x(other._x),
    _y(other._y),
    _z(other._z),
    _t(other._t),
    _p(other._p),
    _m(other._m)
{
    // Nothing to do.
}

Coordinate_t &Coordinate_t::operator =(Coordinate_t other)
{
    swap(other);
    return *this;
}

void Coordinate_t::swap(Scnsl::Core::Coordinate_t & other)
{
    std::swap(_x, other._x);
    std::swap(_y, other._y);
    std::swap(_z, other._z);
    std::swap(_t, other._t);
    std::swap(_p, other._p);
    std::swap(_m, other._m);
}

double Coordinate_t::getX() const
{
    return _x;
}

double Coordinate_t::getY() const
{
    return _y;
}

double Coordinate_t::getZ() const
{
    return _z;
}

double Coordinate_t::getT() const
{
    return _t;
}

double Coordinate_t::getP() const
{
    return _p;
}

double Coordinate_t::getM() const
{
    return _m;
}

bool Coordinate_t::isPolar() const
{
    return _isPolar;
}

void Coordinate_t::print(const std::string name) const
{
    std::cout << "Coordinate for " << name << ": " << _x << ", " << _y << ", " << _z << std::endl;

}

