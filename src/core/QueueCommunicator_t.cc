// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A queue communicator.


#include "scnsl/core/QueueCommunicator_t.hh"
#include "scnsl/core/Packet_t.hh"

using Scnsl::Core::QueueCommunicator_t;
using Scnsl::Core::Packet_t;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

QueueCommunicator_t::QueueCommunicator_t( sc_core::sc_module_name modulename,
										  Queue_if_t * queueSend,
										  Queue_if_t * queueReceive,
										  bool dropPacketsWhenFull):
    // Parents:
	sc_core::sc_module( modulename ),
    Communicator_if_t(),
    // Fields:
    _queueSend( queueSend ),
    _queueReceive( queueReceive ),
	_pktSentDown(),
	_pktReceivedDown(),
	_pktReceivedUp(),
	_dropPacketsWhenFull(_dropPacketsWhenFull)
{
	SC_THREAD( sendProcess );
	SC_THREAD( receiveProcess );
}

QueueCommunicator_t::~QueueCommunicator_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Communicator interface methods.
// ////////////////////////////////////////////////////////////////

QueueCommunicator_t::errorcode_t QueueCommunicator_t::send( const Packet_t & p )

{

	if( _queueSend == nullptr )
		return Scnsl::Core::Communicator_if_t::send( p );

	// check the total capacity o the queue
	if( ! _queueSend->checkTot( const_cast< Packet_t & >(p) ) )
		return 1;

	// Drop packet when flag is set and queue full
	if (_dropPacketsWhenFull && 
		!_queueSend->check(const_cast< Packet_t & >(p)))
		return 0;

	while( ! _queueSend->check( const_cast< Packet_t & >(p) ) )
	{
		// wait if the queue is full
		wait( _pktSentDown );
	}

	if( _queueSend->enqueue( const_cast< Packet_t & >(p) ) )
	{
		// notify that a packet is recived from the upper layer
		_pktReceivedUp.notify();
		return 0;
	}
	return 1;
}


QueueCommunicator_t::errorcode_t QueueCommunicator_t::receive( const Packet_t & p )

{
	if( _queueReceive == nullptr )
		return Scnsl::Core::Communicator_if_t::receive( p );

    if( _queueReceive->enqueue( const_cast< Packet_t & >(p) ) )
	{
		// notify that a packet is recived from the lower layer
		_pktReceivedDown.notify();
		return 0;
	}
	return 1;
}


// ////////////////////////////////////////////////////////////////
// QueueCommunicator specific methods.
// ////////////////////////////////////////////////////////////////

void QueueCommunicator_t::sendProcess()
{
	Scnsl::Core::Packet_t p;
	if( _queueSend != nullptr )
	{
		for (;;)
		{
			// Sends a packet until the queue is not empty
			while( ! _queueSend->isEmpty() )
			{
				p = _queueSend->dequeue();
				_pktSentDown.notify();
				Scnsl::Core::Communicator_if_t::send( p );
			}

			// wait until a packet is received from the upper layer
			wait( _pktReceivedUp );
		}
	}
}


void QueueCommunicator_t::receiveProcess()
{
	Scnsl::Core::Packet_t p;
	if( _queueReceive != nullptr )
	{
		for (;;)
		{
			// Receives a packet until the queue is not empty
			while( ! _queueReceive->isEmpty() )
			{
				p = _queueReceive->dequeue();
				Scnsl::Core::Communicator_if_t::receive( p );
			}

			// wait until a packet is received from the lower layer
			wait( _pktReceivedDown );
		}
	}
}
