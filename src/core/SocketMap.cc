//
// Created by elia on 27/03/20.
//

#include "scnsl/core/SocketMap.hh"

using namespace Scnsl::Core;

std::map<const TaskProxy_if_t*,socket_properties_t> SocketMap::_taskBindings;
std::map<socket_properties_t,const TaskProxy_if_t*> SocketMap::_socketBindings;
std::map<unsigned , const Node_t*> SocketMap::_nodeIPs;
std::map<unsigned , std::string> SocketMap::_ips;
std::hash<std::string> SocketMap::_ip_hash;
std::map<std::string ,unsigned > SocketMap::_dns;
std::map<
    unsigned,std::vector<std::pair<unsigned short,const TaskProxy_if_t*>>
    > SocketMap::_multicast_groups;

void SocketMap::bindTask(const TaskProxy_if_t* tp, socket_properties_t sp)
{
    if (_socketBindings.find(sp) != _socketBindings.end())
        throw "Bind called with duplicate socket: ERROR ";
    _taskBindings[tp] = sp;
    _socketBindings[sp] = tp;
    std::cout<<"Binding "<<tp<<" TO "<<sp<<std::endl;
}

void SocketMap::bindNodes(const Node_t *n, unsigned ip)
{

    if (_nodeIPs.find(ip) != _nodeIPs.end())
    {
        std::cerr<<"IP already bound"<<std::endl;
        return;
        //ip already bound to a node, no more action
    }
    _nodeIPs[ip] = n;
}

void SocketMap::bindName(std::string host, unsigned ip)
{
    _dns[host] = ip;
}

socket_properties_t SocketMap::getSocket (const TaskProxy_if_t* tp)
{
    if (_taskBindings.find(tp) == _taskBindings.end())
        throw std::runtime_error("No socket bound to the taskproxy");
    return _taskBindings[tp];
}

const TaskProxy_if_t* SocketMap::getTp(const socket_properties_t s)
{
   if (_socketBindings.find(s) == _socketBindings.end())
       return nullptr;
   return _socketBindings[s];
}

const Node_t * SocketMap::getNode(const unsigned ip)
{
    if (_nodeIPs.find(ip) == _nodeIPs.end())
        return nullptr;
    return _nodeIPs[ip];
}

std::vector<unsigned >* SocketMap::getNetworkInterfaces(const Node_t* node)
{
    auto v = new  std::vector<unsigned >();
    for (auto i : _nodeIPs)
    {
        if (i.second == node)
            v->push_back(i.first);
    }
    return v;
}

unsigned SocketMap::dnsRequest( std::string host)
{
    if (_dns.find(host) != _dns.end())
        return _dns[host];
    return 0;
}

const std::string SocketMap::resolveIp(unsigned ip)
{
    std::string ret;
    for (auto i : _dns)
    {
        if (i.second == ip)
            ret = i.first;
    }
    return ret;
}

const std::string SocketMap::getIP(unsigned int ip)
{
    return _ips[ip];
}

unsigned SocketMap::getIP(std::string ip)
{
    unsigned ip_numb = _ip_hash(ip);
    if (_ips.find(ip_numb) == _ips.end())
        _ips[ip_numb] = ip;
    return  ip_numb;
}

void SocketMap::addMuticastTP(const TaskProxy_if_t* tp, unsigned ip,
                                unsigned short port)
{
    std::pair<unsigned short, const TaskProxy_if_t*> pair (port,tp);
    _multicast_groups[ip].push_back(pair);
}

bool SocketMap::isTpMulticast(const TaskProxy_if_t* tp, unsigned ip,
                                unsigned short port)
{
    std::pair<unsigned short, const TaskProxy_if_t*> pair(port, tp);
    //return true if the element is not the end of vector (i.e has been found)
    return std::find(_multicast_groups[ip].begin(),
                        _multicast_groups[ip].end(),
                        pair) != _multicast_groups[ip].end();
}

bool SocketMap::isIPMulticast(unsigned ip)
{
    return _multicast_groups.find(ip) != _multicast_groups.end();
}
