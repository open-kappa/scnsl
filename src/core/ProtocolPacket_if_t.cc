// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


/// @file
/// A generic protocol packet.

#include <cstring>
#include "scnsl/core/ProtocolPacket_if_t.hh"
#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/TaskProxy_if_t.hh"

using Scnsl::Core::Channel_if_t;
using Scnsl::Core::Node_t;
using Scnsl::Core::ProtocolPacket_if_t;
using Scnsl::Core::Task_if_t;
using Scnsl::Core::TaskProxy_if_t;

// ////////////////////////////////////////////////////////////////
// Constructors.
// ////////////////////////////////////////////////////////////////

ProtocolPacket_if_t::ProtocolPacket_if_t() :
_header_size(0)
{
    // Nothing to do.
}


ProtocolPacket_if_t::ProtocolPacket_if_t(size_t header_size):
_header_size(header_size)
{

}

ProtocolPacket_if_t::ProtocolPacket_if_t( const ProtocolPacket_if_t & p):
_header_size(p._header_size)
{

}


ProtocolPacket_if_t::~ProtocolPacket_if_t()
{

}


// ////////////////////////////////////////////////////////////////
// Implemented methods
// ////////////////////////////////////////////////////////////////


Scnsl::Core::size_t ProtocolPacket_if_t::getInnerSizeInBytes() const
{
    return to_bytes(getInnerSize());
}

Scnsl::Core::size_t ProtocolPacket_if_t::getPayloadSizeInBytes() const
{
    return to_bytes(getPayloadSize());
}



ProtocolPacket_if_t::size_t ProtocolPacket_if_t::getSize() const
{
    return _header_size + getPayloadSize();
}

ProtocolPacket_if_t::size_t ProtocolPacket_if_t::getSizeInBytes() const
{
    return to_bytes(getSize());
}

ProtocolPacket_if_t::size_t ProtocolPacket_if_t::getHeaderSize() const
{
    return _header_size;
}

ProtocolPacket_if_t::size_t ProtocolPacket_if_t::getHeaderSizeInBytes() const
{
    return to_bytes(_header_size);
}

void ProtocolPacket_if_t::setHeaderSize(const size_t s)
{
    _header_size = s;
}

void ProtocolPacket_if_t::setHeaderSizeInBytes(const size_t s)
{
    setHeaderSize(to_bits(s));
}


// //////////////
// STATIC METHOD
// //////////////


ProtocolPacket_if_t::size_t ProtocolPacket_if_t::to_bytes(const size_t s)
{
    return (s / 8) + (s % 8 ? 1 : 0);
}


ProtocolPacket_if_t::size_t ProtocolPacket_if_t::to_bits(const size_t s)
{
    return s * 8;
}
