// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

///@file
/// The basic implementation of the ProtocolPacket interface used to store a buffer


#include "scnsl/core/SimpleProtocolPacket_t.hh"

using namespace Scnsl::Core;


// ////////////////////////////////////////////////////////////////
// Constructors.
// ////////////////////////////////////////////////////////////////

SimpleProtocolPacket_t::SimpleProtocolPacket_t():
ProtocolPacket_if_t(),
_buffer(nullptr),
_buffer_size(0),
_buffer_capacity(0)
{
    // Nothing to do
}


SimpleProtocolPacket_t::SimpleProtocolPacket_t(const SimpleProtocolPacket_t &p):
ProtocolPacket_if_t(p),
_buffer(nullptr),
_buffer_size(p._buffer_size),
_buffer_capacity(p._buffer_capacity)
{
    if (_buffer_capacity == 0)
        return;
    _buffer = new byte_t[_buffer_capacity];
    memcpy(_buffer, p._buffer, _buffer_capacity);
}


SimpleProtocolPacket_t::~SimpleProtocolPacket_t()
{
    delete _buffer;
}


SimpleProtocolPacket_t &SimpleProtocolPacket_t::operator= (const SimpleProtocolPacket_t &p)
{
    // payload fields
    assert(p._buffer != nullptr); //security check, if used correctly should never fail
    if ( _buffer_capacity < to_bytes(p._buffer_size) )
    {
        byte_t * b = new byte_t[ to_bytes(p._buffer_size) ];
        delete [] _buffer;
        _buffer = b;
        _buffer_capacity = to_bytes(p._buffer_size);
    }

    memcpy( _buffer, p._buffer, to_bytes(p._buffer_size) );
    _buffer_size = p._buffer_size;
    _buffer_capacity = p._buffer_capacity;
    _header_size = p._header_size;
    return *this;
}

void SimpleProtocolPacket_t::setPayload(const byte_t *b, const size_t s)
{
    setPayloadInBytes(b, to_bytes(s));
    _buffer_size = s;
}

void SimpleProtocolPacket_t::setPayloadInBytes(const byte_t *b, const size_t s)
{
    if (_buffer_capacity < s)
    {
        byte_t * buf = new byte_t[s];
        delete[] _buffer;
        _buffer = buf;
        _buffer_capacity = s;
    }

    memcpy(_buffer, b, s);
    _buffer_size = to_bits(s);
}

// ////////////////////////////////////////////////////////////////
// Inherithed methods from interface.
// ////////////////////////////////////////////////////////////////


ProtocolPacket_if_t::size_t SimpleProtocolPacket_t::getPayloadSize() const
{
    return getInnerSize();
}

ProtocolPacket_if_t *SimpleProtocolPacket_t::getPayload() const
{
    throw "Error: SimpleProtocolPacket does not wrap any type of packet";
}

void SimpleProtocolPacket_t::setPayload(const ProtocolPacket_if_t * /*packet*/)
{
    throw "Error: SimpleProtocolPacket can wrap only buffer";
}

byte_t *SimpleProtocolPacket_t::getInnerBuffer() const
{
    return _buffer;
}

ProtocolPacket_if_t::size_t SimpleProtocolPacket_t::getInnerSize() const
{
    return _buffer_size;
}

SimpleProtocolPacket_t *SimpleProtocolPacket_t::clone() const
{
    auto copy = new SimpleProtocolPacket_t(*this);
    return copy;
}

bool SimpleProtocolPacket_t::operator==(const ProtocolPacket_if_t &p) const
{
    auto cast_packet = dynamic_cast <const SimpleProtocolPacket_t *> (&p);
    if (  cast_packet == nullptr) {
        return false;
    }
    if (_buffer_size != cast_packet->_buffer_size ||
        _header_size != cast_packet->_header_size)
    {
        return false;
    }
    return memcmp( _buffer, cast_packet->_buffer, to_bytes(_buffer_size) ) == 0 ;
}

