// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The communicator interface.

#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/Communicator_if_t.hh"


using Scnsl::Core::Communicator_if_t;
using Scnsl::Core::Channel_if_t;
using Scnsl::Core::Packet_t;
using Scnsl::Core::TaskProxy_if_t;


// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

Communicator_if_t::~Communicator_if_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void Communicator_if_t::setCarrier( const Channel_if_t * ch, const carrier_t c )

{
    ChannelBindings_t::iterator i = _channelBindings.find( const_cast< Channel_if_t * >( ch ));

    if ( i == _channelBindings.end() )
    {
        if ( _upper == nullptr )
        {
            // Error:
            throw std::logic_error( "Channel not registered here." );
        }

        _upper->setCarrier( ch, c );
        return;
    }

    CommunicatorList_t::iterator end = (*i).second.end();
    for ( CommunicatorList_t::iterator it = (*i).second.begin();
          it != end;
          ++ it )
    {
        (*it)->setCarrier( ch, c );
    }
}

Communicator_if_t::errorcode_t Communicator_if_t::send( const Packet_t & p )

{
    TaskProxyBindings_t::iterator i = _taskproxyBindings.find(
        const_cast< TaskProxy_if_t *>( p.getSourceTaskProxy() ));
    if ( i == _taskproxyBindings.end() )
    {
        if ( _lower == nullptr )
        {
            // Error:
            throw std::logic_error( "Taskproxy not registered." );
        }
        return _lower->send(p);
    }

    errorcode_t ret = 0;
    CommunicatorList_t::iterator end = (*i).second.end();
    for ( CommunicatorList_t::iterator it = (*i).second.begin();
          it != end;
          ++ it )
    {
        ret |= (*it)->send( p );
    }
    return ret;
}


Communicator_if_t::errorcode_t Communicator_if_t::receive( const Packet_t & p )

{
    ChannelBindings_t::iterator i = _channelBindings.find(
        const_cast< Channel_if_t * >( p.getChannel() ));

    if ( i == _channelBindings.end() )
    {
        if ( _upper == nullptr )
        {
            // Error:
            throw std::logic_error( "Channel not registered." );
        }
        return _upper->receive( p );
    }

    errorcode_t ret = 0;
    CommunicatorList_t::iterator end = (*i).second.end();
    for ( CommunicatorList_t::iterator it = (*i).second.begin();
          it != end;
          ++ it )
    {
        ret |= (*it)->receive( p );
    }

    return ret;
}


void Communicator_if_t::bindTaskProxy( const TaskProxy_if_t * tp,
                              Communicator_if_t * c)

{
    _taskproxyBindings[ const_cast< TaskProxy_if_t * >(tp) ].push_back( c );
}


void Communicator_if_t::bindChannel( const Channel_if_t * ch,
                              Communicator_if_t * c )

{
    _channelBindings[ const_cast< Channel_if_t * >(ch) ].push_back( c );
}


void Communicator_if_t::stackUp( Communicator_if_t * c )

{
    _upper = c;
}

void Communicator_if_t::stackDown( Communicator_if_t * c )

{
    _lower = c;
}


// ////////////////////////////////////////////////////////////////
// Regular deisgn pattern.
// ////////////////////////////////////////////////////////////////


Communicator_if_t::Communicator_if_t():
    _taskproxyBindings(),
    _channelBindings(),
    _upper( nullptr ),
    _lower( nullptr )
{
    // Nothing to do.
}

Communicator_if_t::Communicator_if_t( const Communicator_if_t & c ):
    _taskproxyBindings( c._taskproxyBindings ),
    _channelBindings( c._channelBindings ),
    _upper( c._upper ),
    _lower( c._lower )
{
    // Nothing to do.
}

Communicator_if_t & Communicator_if_t::operator = ( const Communicator_if_t & c )
{
    _taskproxyBindings = c._taskproxyBindings;
    _channelBindings = c._channelBindings;
    _upper = c._upper;
    _lower = c._lower;

    return *this;
}
