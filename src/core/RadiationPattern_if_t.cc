#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
//#define DEBUG_COUT

#include <cmath>
#include <limits>

#include "scnsl/core/RadiationPattern_if_t.hh"

using Scnsl::Core::RadiationPattern_if_t;

RadiationPattern_if_t::~RadiationPattern_if_t()
{
    // Nothing to do.
}

RadiationPattern_if_t::RadiationPattern_if_t(
        const Scnsl::Core::Coordinate_t &vTheta,
        const Scnsl::Core::Coordinate_t &vPhi,
        const bool active,
        const Scnsl::Core::Coordinate_t &vPol,
        const double antennaGain):
    _vTheta(vTheta),
    _vPhi(vPhi),
    _vN(_Xproduct(vTheta, vPhi)),
    _vPol(vPol),
    _antennaGain(antennaGain),
    _isActive(active),
    _ptn2xyz(),
    _xyz2ptn()
{
    _ptn2xyz[0][0] = vTheta.getX();
    _ptn2xyz[0][1] = vPhi.getX();
    _ptn2xyz[0][2] = _vN.getX();
    _ptn2xyz[1][0] = vTheta.getY();
    _ptn2xyz[1][1] = vPhi.getY();
    _ptn2xyz[1][2] = _vN.getY();
    _ptn2xyz[2][0] = vTheta.getZ();
    _ptn2xyz[2][1] = vPhi.getZ();
    _ptn2xyz[2][2] = _vN.getZ();

    double det = _ptn2xyz[0][0] * (_ptn2xyz[1][1] * _ptn2xyz[2][2] - _ptn2xyz[2][1] * _ptn2xyz[1][2]) -
                 _ptn2xyz[0][1] * (_ptn2xyz[1][0] * _ptn2xyz[2][2] - _ptn2xyz[1][2] * _ptn2xyz[2][0]) +
                 _ptn2xyz[0][2] * (_ptn2xyz[1][0] * _ptn2xyz[2][1] - _ptn2xyz[1][1] * _ptn2xyz[2][0]);
    double invdet = 1 / det;

    // inverse of matrix _ptn2xyz
    _xyz2ptn[0][0] = (_ptn2xyz[1][1] * _ptn2xyz[2][2] - _ptn2xyz[2][1] * _ptn2xyz[1][2]) * invdet;
    _xyz2ptn[0][1] = (_ptn2xyz[0][2] * _ptn2xyz[2][1] - _ptn2xyz[0][1] * _ptn2xyz[2][2]) * invdet;
    _xyz2ptn[0][2] = (_ptn2xyz[0][1] * _ptn2xyz[1][2] - _ptn2xyz[0][2] * _ptn2xyz[1][1]) * invdet;
    _xyz2ptn[1][0] = (_ptn2xyz[1][2] * _ptn2xyz[2][0] - _ptn2xyz[1][0] * _ptn2xyz[2][2]) * invdet;
    _xyz2ptn[1][1] = (_ptn2xyz[0][0] * _ptn2xyz[2][2] - _ptn2xyz[0][2] * _ptn2xyz[2][0]) * invdet;
    _xyz2ptn[1][2] = (_ptn2xyz[1][0] * _ptn2xyz[0][2] - _ptn2xyz[0][0] * _ptn2xyz[1][2]) * invdet;
    _xyz2ptn[2][0] = (_ptn2xyz[1][0] * _ptn2xyz[2][1] - _ptn2xyz[2][0] * _ptn2xyz[1][1]) * invdet;
    _xyz2ptn[2][1] = (_ptn2xyz[2][0] * _ptn2xyz[0][1] - _ptn2xyz[0][0] * _ptn2xyz[2][1]) * invdet;
    _xyz2ptn[2][2] = (_ptn2xyz[0][0] * _ptn2xyz[1][1] - _ptn2xyz[1][0] * _ptn2xyz[0][1]) * invdet;



}

RadiationPattern_if_t::RadiationPattern_if_t(const Scnsl::Core::RadiationPattern_if_t & other):
    _vTheta(other._vTheta),
    _vPhi(other._vPhi),
    _vN(other._vN),
    _vPol(other._vPol),
    _antennaGain(other._antennaGain),
    _isActive(other._isActive),
    _ptn2xyz(),
    _xyz2ptn()
{
    for (int i = 0; i < 3; i++ )
    {
        for (int j = 0; j < 3; j++ )
        {
            _ptn2xyz[i][j] = other._ptn2xyz[i][j];
            _xyz2ptn[i][j] = other._xyz2ptn[i][j];
        }
    }
}

void RadiationPattern_if_t::swap(Scnsl::Core::RadiationPattern_if_t & other)
{
    std::swap(_vTheta, other._vTheta);
    std::swap(_vPhi, other._vPhi);
    std::swap(_vPol, other._vPol);
    std::swap(_isActive, other._isActive);
    std::swap(_antennaGain, other._antennaGain);
    std::swap(_vN, other._vN);
    for (int i = 0; i < 3; i++ )
    {
        for (int j = 0; j < 3; j++ )
        {
            std::swap(_ptn2xyz[i][j], other._ptn2xyz[i][j]);
        }
    }
}

double RadiationPattern_if_t::_Sproduct(const Coordinate_t & vect0, const Coordinate_t & vect1) const
{
    double xyzProd = 0;
    double tpnProd = 0;

#ifdef DEBUG_COUT
    if ((!vect0.isPolar()) && (!vect1.isPolar()))
    {
        // ntd
    }
    else if ( (vect0.isPolar()) && (vect1.isPolar()) )
    {
        std::cout << "******* WARNING: scalar produt in polar coordinate system not yet implemented..."  << std::endl;
        // tpnProd = (vect0.t*vect1.t) + (vect0.p*vect1.p) + (vect0.n*vect1.n);
    }
    else
    {
        std::cout << "******* WARNING: vectors belong to differnt coordinate systems..."  << std::endl;
    }
#endif
    xyzProd = (vect0.getX() * vect1.getX()) + (vect0.getY() * vect1.getY()) + (vect0.getZ() * vect1.getZ());

    // one of them is zero in normal functioning

    return xyzProd + tpnProd;
}

RadiationPattern_if_t::Coordinate_t RadiationPattern_if_t::_Xproduct(const Coordinate_t & vect0, const Coordinate_t & vect1) const
{
    double xpx = 0;
    double xpy = 0;
    double xpz = 0;
    if ( (vect0.isPolar() == false) && (vect1.isPolar() == false) )
    {
        xpx = (vect0.getY() * vect1.getZ()) - (vect0.getZ() * vect1.getY());
        xpy = (vect0.getZ() * vect1.getX()) - (vect0.getX() * vect1.getZ());
        xpz = (vect0.getX() * vect1.getY()) - (vect0.getY() * vect1.getX());
    }
    else
    {
#ifdef DEBUG_COUT
        // not yet implemented in polar coordinate system
     //   normal._t = (vect0.t*vect1.t) - (vect0.t*vect1.t);
     //   normal._p = (vect0.p*vect1.p) - (vect0.p*vect1.p);
     //   normal._n = (vect0.n*vect1.n) - (vect0.n*vect1.n);
    std::cout << "******* WARNING: there is some vector in polar coordinate systems..."  << std::endl;
#endif
    }
    return Coordinate_t(false, xpx, xpy, xpz);
}


RadiationPattern_if_t::Coordinate_t RadiationPattern_if_t::_normalizeVector(Coordinate_t vect) const
{
    double d = (vect.getX() * vect.getX()) + (vect.getY() * vect.getY()) + (vect.getZ() * vect.getZ());
    double mod = sqrt(d);
    double nx = vect.getX() / mod;
    double ny = vect.getY() / mod;
    double nz = vect.getZ() / mod;
    return Coordinate_t(false, nx, ny, nz);
}

RadiationPattern_if_t::Coordinate_t RadiationPattern_if_t::_getVectorFromTo(const node_properties_t &from, const node_properties_t &to) const
{
    double vect_x = to.x - from.x;
    double vect_y = to.y - from.y;
    double vect_z = to.z - from.z;
    return Coordinate_t(false, vect_x, vect_y, vect_z);
}

double RadiationPattern_if_t::_getVect2VectAngle(const Coordinate_t & vect0, const Coordinate_t & vect1) const
{

    double scalar_product = _Sproduct(vect0, vect1);
    double mod0 = sqrt(  _Sproduct(vect0, vect0) );
    double mod1 = sqrt(  _Sproduct(vect1, vect1) );

    double param = scalar_product/(mod0*mod1);
      if(param > 1.0)
         param = 1.0;
      else if(param < -1.0)
         param = -1.0;
    return acos(param)*180/M_PI;

}


RadiationPattern_if_t::Coordinate_t RadiationPattern_if_t::_changeVectorBase(Coordinate_t thetaVersor, Coordinate_t phiVersor, Coordinate_t normalVersor, Coordinate_t vect) const
{

    // mtx1 is the change base matrix (nTheta,nPhi,normal)->(x,y,z)
    // therefore mtx2=mtx1^(-1) is the change base matrix (x,y,z)->(nTheta,nPhi,normal)

    double mtx1[3][3] = { { thetaVersor.getX() , phiVersor.getX() , normalVersor.getX() },
                          { thetaVersor.getY() , phiVersor.getY() , normalVersor.getY() },
                          { thetaVersor.getZ() , phiVersor.getZ() , normalVersor.getZ() } };

    double det = mtx1[0][0] * (mtx1[1][1] * mtx1[2][2] - mtx1[2][1] * mtx1[1][2]) -
                 mtx1[0][1] * (mtx1[1][0] * mtx1[2][2] - mtx1[1][2] * mtx1[2][0]) +
                 mtx1[0][2] * (mtx1[1][0] * mtx1[2][1] - mtx1[1][1] * mtx1[2][0]);

    double invdet = 1 / det;

    double mtx2[3][3]; // inverse of matrix m
    mtx2[0][0] = (mtx1[1][1] * mtx1[2][2] - mtx1[2][1] * mtx1[1][2]) * invdet;
    mtx2[0][1] = (mtx1[0][2] * mtx1[2][1] - mtx1[0][1] * mtx1[2][2]) * invdet;
    mtx2[0][2] = (mtx1[0][1] * mtx1[1][2] - mtx1[0][2] * mtx1[1][1]) * invdet;
    mtx2[1][0] = (mtx1[1][2] * mtx1[2][0] - mtx1[1][0] * mtx1[2][2]) * invdet;
    mtx2[1][1] = (mtx1[0][0] * mtx1[2][2] - mtx1[0][2] * mtx1[2][0]) * invdet;
    mtx2[1][2] = (mtx1[1][0] * mtx1[0][2] - mtx1[0][0] * mtx1[1][2]) * invdet;
    mtx2[2][0] = (mtx1[1][0] * mtx1[2][1] - mtx1[2][0] * mtx1[1][1]) * invdet;
    mtx2[2][1] = (mtx1[2][0] * mtx1[0][1] - mtx1[0][0] * mtx1[2][1]) * invdet;
    mtx2[2][2] = (mtx1[0][0] * mtx1[1][1] - mtx1[1][0] * mtx1[0][1]) * invdet;

    // Product between change base matrix and the given vector
    // New vector has t-p-n fields only
    double t = mtx2[0][0]*vect.getX() + mtx2[0][1]*vect.getY() + mtx2[0][2]*vect.getZ();
    double p = mtx2[1][0]*vect.getX() + mtx2[1][1]*vect.getY() + mtx2[1][2]*vect.getZ();
    double n = mtx2[2][0]*vect.getX() + mtx2[2][1]*vect.getY() + mtx2[2][2]*vect.getZ();

    // NB: the new coordinate system is still a non polar coordinate system.
    // t-p-n are its orthogonal reference vectors.
    // for a correct angle calculation theta scoud match with z and phy with x
    return Coordinate_t(false, t, p, n);

}


RadiationPattern_if_t::Coordinate_t RadiationPattern_if_t::_getVectorOntoPlane(Coordinate_t vect, Coordinate_t base0, Coordinate_t base1) const
{
    double scalar0 = _Sproduct(vect, base0);
    double scalar1 = _Sproduct(vect, base1);
    double mod0 = sqrt(  _Sproduct(base0, base0) );
    double mod1 = sqrt(  _Sproduct(base1, base1) );

    return Coordinate_t(false, scalar0/mod0, scalar1/mod1, 0);
}

bool RadiationPattern_if_t::_doubleEqual(const double a, const double b) const
{
    return fabs(a - b) < std::numeric_limits<double>::epsilon();
}

double RadiationPattern_if_t::_getDistance(
        const node_properties_t & sp,
        const node_properties_t & rp) const
{
    const position_t dx = rp.x - sp.x;
    const position_t dy = rp.y - sp.y;
    const position_t dz = rp.z - sp.z;
    const double d2 = (dx * dx) + (dy * dy) + (dz * dz);

    return sqrt(d2);
}

void RadiationPattern_if_t::_fixZero(double & value) const
{
    if (fabs(value) <= 100 * std::numeric_limits<double>::epsilon())
        value = 0;
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

bool RadiationPattern_if_t::isActive() const
{
    return _isActive;
}

void RadiationPattern_if_t::setActive(const bool val)
{
    _isActive = val;
}
