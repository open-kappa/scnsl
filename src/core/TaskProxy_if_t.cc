// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Task proxy interface.


#include "scnsl/core/Communicator_if_t.hh"
#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/TaskProxy_if_t.hh"


using Scnsl::Core::TaskProxy_if_t;
using Scnsl::Core::Packet_t;
using Scnsl::Core::Channel_if_t;
using Scnsl::Core::Communicator_if_t;
using Scnsl::Core::Task_if_t;

// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////


TaskProxy_if_t::~TaskProxy_if_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Communicator interface reimplemented methods.
// ////////////////////////////////////////////////////////////////


TaskProxy_if_t::errorcode_t TaskProxy_if_t::send( const Packet_t & p )

{
    if ( _task->getNode() != p.getSourceTaskProxy()->_task->getNode() )
    {
        throw std::logic_error( "Invalid call of send on taskproxy." );
    }

    return Communicator_if_t::send( p );
}

void TaskProxy_if_t::bindTaskProxy( const TaskProxy_if_t * tp,
                                    Communicator_if_t * c )

{
    if ( ! _taskproxyBindings.empty() )
    {
        throw std::logic_error(
            "It is illegal to call bind of taskproxy on a taskproxy more than once.");
    }

    Communicator_if_t::bindTaskProxy( tp, c );
}

void TaskProxy_if_t::bindChannel( const Channel_if_t * /*ch*/,
                                  Communicator_if_t * /*c*/ )

{
    throw std::logic_error("It is illegal to call bind of channel on a taskproxy.");
}

void TaskProxy_if_t::stackUp( Communicator_if_t * /*c*/ )

{
    throw std::logic_error( "Illegal call to stackUp() on a taskproxy." );
}


// ////////////////////////////////////////////////////////////////
// Taskproxy interface methods.
// ////////////////////////////////////////////////////////////////

const Scnsl::Core::Channel_if_t * TaskProxy_if_t::getChannel()
    const
{
    return _channel;
}

const Scnsl::Core::Task_if_t * TaskProxy_if_t::getTask()
    const
{
    return _task;
}

const Scnsl::Core::Task_if_t * TaskProxy_if_t::getDestinationTask()
    const
{
    return _destinationTask;
}

void TaskProxy_if_t::setDestinationTask( Scnsl::Core::Task_if_t * dst )
{
    _destinationTask = dst;
}


Scnsl::Core::Node_t * TaskProxy_if_t::getDestinationNode()
{
    return _destinationTask != nullptr ? _destinationTask->getNode() : nullptr;
}




// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////


TaskProxy_if_t::TaskProxy_if_t(
    const sc_core::sc_module_name modulename,
    Task_if_t * t,
    Channel_if_t * ch ):
    // Parents:
    sc_core::sc_module( modulename ),
    Communicator_if_t(),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Fields:
    _task( t ),
    _destinationTask( nullptr ),
    _channel( ch )
{
    // Nothing to do.
}
