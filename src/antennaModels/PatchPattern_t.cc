// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
//#define DEBUG_COUT

#include "scnsl/antennaModels/PatchPattern_t.hh"

using Scnsl::Core::RadiationPattern_if_t;
using namespace Scnsl::Core;
using namespace Scnsl::antennaModels;


PatchPattern_t::PatchPattern_t(const Coordinate_t & vTheta,
                               const bool active,
                               const double firstBeamwidth,
                               const double firstGain,
                               const Coordinate_t & vPhi,
                               const double secondBeamwidth,
                               const double secondGain,
                               const double secondLobes,
                               const double thirdBeamwidth,
                               const double thirdGain,
                               const double thirdLobes):
    Scnsl::Core::RadiationPattern_if_t(vTheta, vPhi, active, vTheta, 0),
    _firstGain(firstGain),
    _secondGain(secondGain),
    _thirdGain(thirdGain),
    _firstBeamwidth(firstBeamwidth),
    _secondBeamwidth(secondBeamwidth),
    _thirdBeamwidth(thirdBeamwidth),
    _secondLobes(secondLobes),
    _thirdLobes(thirdLobes),
    _nLobes(0)
{
    if ( (thirdBeamwidth < 0) && (secondBeamwidth < 0) )
    {
        _nLobes = 1;
#ifdef DEBUG_COUT
        std::cout << "Single lobe patch" << std::endl;
#endif
    } else if ( (thirdBeamwidth < 0) && (secondBeamwidth > 0) )
    {
        _nLobes = 2;
#ifdef DEBUG_COUT
        if ( firstBeamwidth > secondBeamwidth )
        {
            std::cout << "Warning: Bad angle of aperture description" << std::endl;
        }
#endif
    } else if ( (thirdBeamwidth > 0) && (secondBeamwidth > 0) )
    {
        _nLobes = 3;
#ifdef DEBUG_COUT
        if ( ( firstBeamwidth > secondBeamwidth ) || ( secondBeamwidth > thirdBeamwidth ) )
        {
            std::cout << "Warning: Bad angle of aperture description" << std::endl;
        }
#endif
    }
}

PatchPattern_t::~PatchPattern_t()
{
    // ntd
}

PatchPattern_t::PatchPattern_t(const PatchPattern_t & other):
    Scnsl::Core::RadiationPattern_if_t(other),
    _firstGain(other._firstGain),
    _secondGain(other._secondGain),
    _thirdGain(other._thirdGain),
    _firstBeamwidth(other._firstBeamwidth),
    _secondBeamwidth(other._secondBeamwidth),
    _thirdBeamwidth(other._thirdBeamwidth),
    _secondLobes(other._secondLobes),
    _thirdLobes(other._thirdLobes),
    _nLobes(other._nLobes)
{
    // ntd
}

PatchPattern_t &PatchPattern_t::operator =(PatchPattern_t other)
{
    swap(other);
    return *this;
}

void PatchPattern_t::swap(PatchPattern_t & other)
{
    Scnsl::Core::RadiationPattern_if_t::swap(other);
    std::swap(_firstGain, other._firstGain);
    std::swap(_secondGain, other._secondGain);
    std::swap(_thirdGain, other._thirdGain);
    std::swap(_firstBeamwidth, other._firstBeamwidth);
    std::swap(_secondBeamwidth, other._secondBeamwidth);
    std::swap(_thirdBeamwidth, other._thirdBeamwidth);
    std::swap(_secondLobes, other._secondLobes);
    std::swap(_thirdLobes, other._thirdLobes);
    std::swap(_nLobes, other._nLobes);
}

bool PatchPattern_t::isTheInterestedPattern(const node_properties_t & /*sp*/,
                                            const node_properties_t & /*rp*/) const
{
    return true;
}

double PatchPattern_t::getGain(const PatchPattern_t::node_properties_t & sp, const PatchPattern_t::node_properties_t & rp) const
{
    Coordinate_t radiation_vector = _getVectorFromTo(sp, rp);
    double theta = _getVect2VectAngle(radiation_vector, _vTheta);

#ifdef DEBUG_COUT
    std::cout << "Beacon: (" << rp.x << ", " << rp.y << ", " << rp.z
     << "), Antenna: (" << sp.x << ", " << sp.y << ", " << sp.z << ")."
     << " Radiation is: (" << radiation_vector._getx() << ", " << radiation_vector._gety() << ", " << radiation_vector._getz() << ")."
     << " Theta is " << theta << "°." << std::endl;
#endif

    if (_nLobes <= 1.0 && _nLobes >= 1.0)
    {
        if ( theta <= _firstBeamwidth )
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation matches the first aperture" << std::endl;
#endif
            theta = theta * (90/_firstBeamwidth);
            theta = theta * M_PI / 180;
            return _firstGain*cos(theta);
        }
        else
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation out of aperture" << std::endl;
#endif
            return 0;
        }

    }
    else if (_nLobes <= 2.0 && _nLobes >= 2.0)
    {

        if ( theta <= _firstBeamwidth )
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation matches the first aperture" << std::endl;
#endif
            theta = theta * (90/_firstBeamwidth);
            theta = theta * M_PI / 180;
            return _firstGain*cos(theta);
        }
        else if ( theta <= _secondBeamwidth )
        {
            if (_secondLobes <= 1.0 && _secondLobes >= 1.0)
            {
#ifdef DEBUG_COUT
                std::cout << "Radiation matches the second aperture" << std::endl;
#endif
                theta = theta - _firstBeamwidth;
                theta = theta * ( 180 / (_secondBeamwidth - _firstBeamwidth) );
                theta = theta * M_PI / 180;
                return _secondGain*sin(theta);
            }
            else
            {
                // find all the normalized versors for the antenna orthonormal base
                Coordinate_t nTheta = _normalizeVector(_vTheta);
                Coordinate_t nPhi = _normalizeVector(_vPhi);
                Coordinate_t vN = _Xproduct(_vTheta, _vPhi);
                Coordinate_t nN = _normalizeVector(vN);

                Coordinate_t antennaBaseRadiation = _changeVectorBase(nTheta, nPhi, nN, radiation_vector);

                double theta_mod = theta - _firstBeamwidth;
                theta_mod = theta_mod * ( 180 / (_secondBeamwidth - _firstBeamwidth) );
                double phi = antennaBaseRadiation.getP();
               // if ( phi < 0)  { phi += 360; }

                double phi_mod = phi * ( 180 * _secondLobes ) / 360;

#ifdef DEBUG_COUT
                std::cout << "\033[1;31m From antenna base radiation vecor (" << antennaBaseRadiation._getx() << "," << antennaBaseRadiation._gety() << "," << antennaBaseRadiation._getz() <<  "), theta is " << antennaBaseRadiation._gett() << ", theta mod is " << theta_mod << ", phi is " << phi << ", phi mod is " << phi_mod << ", modulo " << antennaBaseRadiation._getm() << "\033[0m" << std::endl;
#endif

                theta_mod = theta_mod * M_PI / 180;
                phi_mod = phi_mod * M_PI / 180;
                return _secondGain * sin( theta_mod ) * fabs( cos( phi_mod ) );
            }
        }
        else
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation out of aperture" << std::endl;
#endif
            return 0;
        }

    }
    else if (_nLobes <= 3.0 && _nLobes >= 3.0)
    {

        if ( theta <= _firstBeamwidth )
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation matches the first aperture" << std::endl;
#endif
            theta = theta * (90/_firstBeamwidth);
            theta = theta * M_PI / 180;
            return _firstGain*cos(theta);
        }
        else if ( theta <= _secondBeamwidth )
        {
            if (_secondLobes <= 1.0 && _secondLobes >= 1.0)
            {
#ifdef DEBUG_COUT
                std::cout << "Radiation matches the second aperture" << std::endl;
#endif
                theta = theta - _firstBeamwidth;
                theta = theta * ( 180 / (_secondBeamwidth - _firstBeamwidth) );
                theta = theta * M_PI / 180;
                return _secondGain*sin(theta);
            }
            else
            {
                // find all the normalized versors for the antenna orthonormal base
                Coordinate_t nTheta = _normalizeVector(_vTheta);
                Coordinate_t nPhi = _normalizeVector(_vPhi);
                Coordinate_t vN = _Xproduct(_vTheta, _vPhi);
                Coordinate_t nN = _normalizeVector(vN);

                Coordinate_t antennaBaseRadiation = _changeVectorBase(nTheta, nPhi, nN, radiation_vector);

                double theta_mod = theta - _firstBeamwidth;
                theta_mod = theta_mod * ( 180 / (_secondBeamwidth - _firstBeamwidth) );
                double phi = antennaBaseRadiation.getP();
               // if ( phi < 0)  { phi += 360; }

                double phi_mod = phi * ( 180 * _secondLobes ) / 360;

#ifdef DEBUG_COUT
                std::cout << "\033[1;31m From antenna base radiation vecor (" << antennaBaseRadiation._getx() << "," << antennaBaseRadiation._gety() << "," << antennaBaseRadiation._getz() <<  "), theta is " << antennaBaseRadiation._gett() << ", theta mod is " << theta_mod << ", phi is " << phi << ", phi mod is " << phi_mod << ", modulo " << antennaBaseRadiation._getm() << "\033[0m" << std::endl;
#endif

                theta_mod = theta_mod * M_PI / 180;
                phi_mod = phi_mod * M_PI / 180;
                return _secondGain * sin( theta_mod ) * fabs( cos( phi_mod ) );
            }
        }
        else if ( theta <= _thirdBeamwidth )
        {
            if (_thirdLobes <= 1.0 && _thirdLobes >= 1.0)
            {
#ifdef DEBUG_COUT
                        std::cout << "Radiation matches the third aperture" << std::endl;
#endif
                        theta = theta - _secondBeamwidth;
                        theta = theta * ( 180 / (_thirdBeamwidth - _secondBeamwidth) );
                        theta = theta * M_PI / 180;
                        return _thirdGain*sin(theta);
            }
            else
            {
                // find all the normalized versors for the antenna orthonormal base
                Coordinate_t nTheta = _normalizeVector(_vTheta);
                Coordinate_t nPhi = _normalizeVector(_vPhi);
                Coordinate_t vN = _Xproduct(_vTheta, _vPhi);
                Coordinate_t nN = _normalizeVector(vN);

                Coordinate_t antennaBaseRadiation = _changeVectorBase(nTheta, nPhi, nN, radiation_vector);

                double theta_mod = theta - _secondBeamwidth;
                theta_mod = theta_mod * ( 180 / (_thirdBeamwidth - _secondBeamwidth) );
                double phi = antennaBaseRadiation.getP();
               // if ( phi < 0)  { phi += 360; }

                double phi_mod = phi * ( 180 * _thirdLobes ) / 360;

#ifdef DEBUG_COUT
                std::cout << "\033[1;31m From antenna base radiation vecor (" << antennaBaseRadiation._getx() << "," << antennaBaseRadiation._gety() << "," << antennaBaseRadiation._getz() <<  "), theta is " << antennaBaseRadiation._gett() << ", theta mod is " << theta_mod << ", phi is " << phi << ", phi mod is " << phi_mod << ", modulo " << antennaBaseRadiation._getm() << "\033[0m" << std::endl;
#endif

                theta_mod = theta_mod * M_PI / 180;
                phi_mod = phi_mod * M_PI / 180;
                return _thirdGain * sin( theta_mod ) * fabs( cos( phi_mod ) );
            }
        }
        else
        {
#ifdef DEBUG_COUT
            std::cout << "Radiation out of aperture" << std::endl;
#endif
            return 0;
        }

    }
    else
    {
        return 0;
    }

}
