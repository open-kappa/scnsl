// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#if(defined _MSC_VER)
#define _USE_MATH_DEFINES
#endif
//#define DEBUG_COUT

#include "scnsl/antennaModels/ConePattern_t.hh"

using Scnsl::Core::RadiationPattern_if_t;
using namespace Scnsl::Core;
using namespace Scnsl::antennaModels;

ConePattern_t::ConePattern_t(const Coordinate_t vTheta,
                             const bool active,
                             const double coneGain,
                             const double beamwidth):
    Scnsl::Core::RadiationPattern_if_t(vTheta, vTheta, active, vTheta, coneGain),
    _beamwidth(beamwidth)
{
    //Nothing to do.
}

ConePattern_t::~ConePattern_t()
{
    //Nothing to do.
}

ConePattern_t::ConePattern_t(const ConePattern_t & other):
     RadiationPattern_if_t(other),
    _beamwidth(other._beamwidth)
{
    //Nothing to do.
}

ConePattern_t &ConePattern_t::operator =(ConePattern_t other)
{
    swap(other);
    return *this;
}

void ConePattern_t::swap(ConePattern_t & other)
{
    RadiationPattern_if_t::swap(other);
    std::swap(_beamwidth, other._beamwidth);
}

bool ConePattern_t::isTheInterestedPattern(
        const node_properties_t & sp, const node_properties_t & rp) const
{
    Coordinate_t radiation_vector = _getVectorFromTo(sp, rp);
    double theta = _getVect2VectAngle(radiation_vector, _vTheta);

    if (theta < _beamwidth )
    {
#ifdef DEBUG_COUT
    std::cout << "Radiation matches the antenna aperture (" << theta << " < " << _beamwidth << ")" << std::endl;
#endif
        return true;
    }
    return false;
}

double ConePattern_t::getGain(const ConePattern_t::node_properties_t & /*sp*/, const ConePattern_t::node_properties_t & /*rp*/) const
{
    return _antennaGain;
}
