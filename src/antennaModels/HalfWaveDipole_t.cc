// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
//#define DEBUG_COUT
#include <cmath>

#include "scnsl/antennaModels/HalfWaveDipole_t.hh"

using Scnsl::antennaModels::HalfWaveDipole_t;

HalfWaveDipole_t::HalfWaveDipole_t(
        const Coordinate_t vTheta,
        const bool active,
        const double antennaGain):
    Scnsl::Core::RadiationPattern_if_t(vTheta, vTheta, active, vTheta, antennaGain)
{
    // ntd
}

HalfWaveDipole_t::~HalfWaveDipole_t()
{
    // ntd
}

HalfWaveDipole_t::HalfWaveDipole_t( const HalfWaveDipole_t & other):
    Scnsl::Core::RadiationPattern_if_t(other)
{
    // ntd
}

HalfWaveDipole_t &HalfWaveDipole_t::operator =(HalfWaveDipole_t other)
{
    swap(other);
    return *this;
}

void HalfWaveDipole_t::swap(HalfWaveDipole_t & other)
{
    Scnsl::Core::RadiationPattern_if_t::swap(other);
}

bool HalfWaveDipole_t::isTheInterestedPattern(
        const node_properties_t & /*sp*/, const node_properties_t & /*rp*/) const
{
    return true;
}

double HalfWaveDipole_t::getGain(
        const node_properties_t & sp,
        const node_properties_t & rp) const
{

    Coordinate_t radiation_vector = _getVectorFromTo(sp, rp);
    double theta = _getVect2VectAngle(radiation_vector, _vTheta);
    theta = theta * M_PI / 180;

    double ret = cos((M_PI/2)*cos(theta))/sin(theta) * cos((M_PI/2)*cos(theta))/sin(theta);
    if (ret < 0.0) ret = -ret;

#ifdef DEBUG_COUT
    std::cout << "Beacon: (" << rp.x << ", " << rp.y << ", " << rp.z
     << ") ,Antenna: (" << sp.x << ", " << sp.y << ", " << sp.z << ")."
     << " Theta is " << theta << "°. Result is " << ret << std::endl;
#endif

    return ret;
}
