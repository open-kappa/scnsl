// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#if(defined _MSC_VER)
#define _USE_MATH_DEFINES
#endif
#include <cmath>

#include "scnsl/antennaModels/IsotropicAntenna_t.hh"

using Scnsl::antennaModels::IsotropicAntenna_t;

IsotropicAntenna_t::IsotropicAntenna_t(const Coordinate_t vTheta,
                                       const Coordinate_t vPhi,
                                       const Coordinate_t vPol,
                                       const bool active,
                                       const double antennaGain):
    Scnsl::Core::RadiationPattern_if_t(vTheta, vPhi, active, vPol, antennaGain)
{
    // ntd
}

IsotropicAntenna_t::~IsotropicAntenna_t()
{
    // ntd
}

IsotropicAntenna_t::IsotropicAntenna_t(const IsotropicAntenna_t & other):
    Scnsl::Core::RadiationPattern_if_t(other)
{
    // ntd
}

IsotropicAntenna_t &IsotropicAntenna_t::operator =(IsotropicAntenna_t other)
{
    swap(other);
    return *this;
}

void IsotropicAntenna_t::swap(IsotropicAntenna_t & other)
{
    Scnsl::Core::RadiationPattern_if_t::swap(other);
}

bool IsotropicAntenna_t::isTheInterestedPattern(
        const node_properties_t & /*sp*/, const node_properties_t & /*rp*/) const
{
    return true;
}

double IsotropicAntenna_t::getGain(
        const node_properties_t & /*sp*/,
        const node_properties_t & /*rp*/) const
{
    return 1;
}
