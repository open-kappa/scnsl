// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
//#define DEBUG_COUT

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "scnsl/antennaModels/CustomAntenna_t.hh"


using Scnsl::antennaModels::CustomAntenna_t;


CustomAntenna_t::CustomAntenna_t(
        const std::string name,
        const Coordinate_t vTheta,
        const Coordinate_t vPhi,
        const Coordinate_t vPol,
        const bool active,
        const std::string radiationFile):
        Scnsl::Core::RadiationPattern_if_t(vTheta, vPhi, active, vPol, 0),
        _name(name),
        _radFile(radiationFile),
        _mtxType(),
        _pnPlane(),
        _tpPlane(),
        _tnPlane(),
        _totTheta(0),
        _totPhi(0),
        _thetaValues(),
        _phiValues(),
        _patternMatrix()
{

    if (!(_getVect2VectAngle(vTheta,vPhi) <= 90.0 && _getVect2VectAngle(vTheta,vPhi) >= 90.0))
    {
        // for asymmetric patterns check if the given base is orthogonal.
        std::cout << "\033[1;31m Error: Given Coordinate system for antenna " << _name << " is not orthogonal... \033[0m" << std::endl;
    }
    else
    {

        std::ifstream in(radiationFile.c_str());
        std::cout << "Reading rad file for antenna " << _name << std::endl;
        if (!in.good()) {
            std::cout << "\033[1;31m Error (" << _name << "): Not existing rad file... \033[0m" << std::endl;
        } else {
            while (in.good()){
                std::string line;
                std::getline(in, line);
                std::getline(in, line);
                std::getline(in, line);
                std::getline(in, line);
                std::getline(in, line);
                std::cout << line << std::endl;
                std::size_t found = line.find("PatternType");
                if (found==std::string::npos){
                    std::cout << "\033[1;33m Warning (" << _name << "): Bad formed rad file... \033[0m" << std::endl;
                }
                _mtxType = line.substr (12,2);



                // ORTHOGONAL PLANES PATTERN
                if (_mtxType == "op") {
                    std::vector<std::string> tokens;
                    inputPlaneElement ipe;
                    std::vector<bool> tobe_scanned = {true, true, true};

                    for (int i = 0; i < 3; i++){
                        std::getline(in, line);

                        char * tok = strtok(const_cast<char *>(line.c_str()), " |,");
                        std::vector<std::string> tokensVector;
                        while(tok != nullptr)
                        {
                            tokensVector.push_back(tok);
                            tok = strtok(nullptr, " |,");
                        }
                        std::string plane = tokensVector[0].c_str();
                        auto sz = (tokensVector.size() - 1) / 4;
                        if ((plane == "pnplane") && tobe_scanned[0])
                        {
                            std::cout << "\033[1;36m -- PN PLANE (" << _name <<")\033[0m" << std::endl;
                            for (decltype(sz) j = 0; j < sz; j++){
                                ipe.Angle = atof(tokensVector[j*4+1].c_str());
                                ipe.Gh = atof(tokensVector[j*4+2].c_str());
                                ipe.Gv = atof(tokensVector[j*4+3].c_str());
                                ipe.Gt = atof(tokensVector[j*4+4].c_str());
                                _pnPlane.push_back(ipe);
                                std::cout << ipe.Angle << ipe.Gh << ipe.Gv << ipe.Gt << std::endl;
                            }
                            tobe_scanned[0] = false;

                        } else if ((plane == "tpplane") && tobe_scanned[1]) {

                            std::cout << "\033[1;36m -- TP PLANE (" << _name <<")\033[0m" << std::endl;
                            for (decltype(sz) j = 0; j < sz; j++){
                                ipe.Angle = atof(tokensVector[j*4+1].c_str());
                                ipe.Gh = atof(tokensVector[j*4+2].c_str());
                                ipe.Gv = atof(tokensVector[j*4+3].c_str());
                                ipe.Gt = atof(tokensVector[j*4+4].c_str());
                                _tpPlane.push_back(ipe);
                                std::cout << ipe.Angle << ipe.Gh << ipe.Gv << ipe.Gt << std::endl;
                            }
                            tobe_scanned[1] = false;

                        } else if ((plane == "tnplane") && tobe_scanned[2]) {

                            std::cout << "\033[1;36m -- TN PLANE (" << _name <<")\033[0m" << std::endl;
                            for (decltype(sz) j = 0; j < sz; j++){
                                ipe.Angle = atof(tokensVector[j*4+1].c_str());
                                ipe.Gh = atof(tokensVector[j*4+2].c_str());
                                ipe.Gv = atof(tokensVector[j*4+3].c_str());
                                ipe.Gt = atof(tokensVector[j*4+4].c_str());
                                _tnPlane.push_back(ipe);
                                std::cout << ipe.Angle << ipe.Gh << ipe.Gv << ipe.Gt << std::endl;
                            }
                            tobe_scanned[2] = false;

                        } else {
                        std::cout << "\033[1;33m Warning (" << _name << "): Replicated or bad formad plane... \033[0m" << std::endl;
                        break;
                        }
                    }
                }



                // COMPLETE PATTERN
                if (_mtxType == "cp") {
                    char * tok = strtok(const_cast<char *>(line.substr(15,100) .c_str()), " ");
                    std::vector<std::string> tokens;
                    while(tok != nullptr)
                    {
                        tokens.push_back(tok);
                        tok = strtok(nullptr, " ");
                    }
                    _totTheta = size_t(atoi(tokens[0].c_str()));
                    _totPhi = size_t(atoi(tokens[1].c_str()));

                    // Reading Theta and Phi values
                    std::getline(in, line);
                    tok = strtok(const_cast<char *>(line.c_str()), " ,");
                    tokens.clear();
                    while(tok != nullptr)
                    {
                        tokens.push_back(tok);
                        tok = strtok(nullptr, " ,");
                    }
                    std::cout << "\033[1;34m Theta values: \033[0m" << std::endl;
                    for (size_t t=0; t<_totTheta; t++)
                    {
                        _thetaValues.push_back(atof(tokens[t+size_t(1)].c_str()));
                        std::cout << atof(tokens[t+size_t(1)].c_str()) << std::endl;
                    }
                    std::getline(in, line);
                    tok = strtok(const_cast<char *>(line.c_str()), " ,");
                    tokens.clear();
                    while(tok != nullptr)
                    {
                        tokens.push_back(tok);
                        tok = strtok(nullptr, " ,");
                    }
                    std::cout << "\033[1;34m Phi values: \033[0m" << std::endl;
                    for (size_t p=0; p<_totPhi; p++)
                    {
                        _phiValues.push_back(atof(tokens[p+size_t(1)].c_str()));
                        std::cout << atof(tokens[p+size_t(1)].c_str()) << std::endl;
                    }


                    // Reading matrix values
                    inputGainElement ige;
                    std::vector<inputGainElement> elementsLine;
                    for (size_t t=0; t<_totTheta; t++)
                    {

                        std::cout << "\033[1;34m Gain values for Theta " << _thetaValues[t] << "°: \033[0m" << std::endl;
                        std::getline(in, line);
                        char * tokCStr = strtok(const_cast<char *>(line.c_str()), " |,");
                        std::vector<std::string> tokensVector;
                        while(tokCStr != nullptr)
                        {
                            tokensVector.push_back(tokCStr);
                            tokCStr = strtok(nullptr, " |,");
                        }
                        for (size_t p=0; p<_totPhi; p++) {
                            ige.Gh = atof(tokensVector[size_t(4)*p+size_t(2)].c_str());
                            ige.Gv = atof(tokensVector[size_t(4)*p+size_t(3)].c_str());
                            ige.Gt = atof(tokensVector[size_t(4)*p+size_t(4)].c_str());
                            elementsLine.push_back(ige);
                            std::cout << "Phi " << _phiValues[p] << "°:" << ige.Gh << " " << ige.Gv << " " << ige.Gt << std::endl;
                        }
                        _patternMatrix.push_back(elementsLine);
                        elementsLine.clear();
                    }
                }

                // INCOMPLETE PATTERN
                if (_mtxType == "ip") { }
                break;

            }
        }
    }
}

CustomAntenna_t::~CustomAntenna_t()
{
    // ntd
}

CustomAntenna_t::CustomAntenna_t(
        const CustomAntenna_t & other):
    Scnsl::Core::RadiationPattern_if_t(other),
    _name(other._name),
    _radFile(other._radFile),
    _mtxType(),
    _pnPlane(),
    _tpPlane(),
    _tnPlane(),
    _totTheta(0),
    _totPhi(0),
    _thetaValues(),
    _phiValues(),
    _patternMatrix()
{
    // ntd
}

CustomAntenna_t &CustomAntenna_t::operator =(
        CustomAntenna_t other)
{
    swap(other);
    return *this;
}

void CustomAntenna_t::swap(CustomAntenna_t & other)
{
    Scnsl::Core::RadiationPattern_if_t::swap(other);
    std::swap(_name, other._name);
    std::swap(_radFile, other._radFile);
}

bool CustomAntenna_t::isTheInterestedPattern(
        const node_properties_t & /*sp*/, const node_properties_t & /*rp*/) const
{
    return true;
}

double CustomAntenna_t::getGain(
        const node_properties_t & sp,
        const node_properties_t & rp) const
{

    // find the radiation vector in the orthonormal xyz base
    Coordinate_t radiation_vector = _getVectorFromTo(sp, rp);

    // find all the normalized versors for the antenna orthonormal base
    Coordinate_t nTheta = _normalizeVector(_vTheta);
    Coordinate_t nPhi = _normalizeVector(_vPhi);
    Coordinate_t vN = _Xproduct(_vTheta, _vPhi);
    Coordinate_t nN = _normalizeVector(vN);

    // changing the radiation vector coordinate system from x,y,z to theta,phi,normal
    Coordinate_t antennaBaseRadiation = _changeVectorBase(nTheta, nPhi, nN, radiation_vector);

    Coordinate_t dummyThetaVersor = Coordinate_t(false, 1, 0, 0);  // dummy theta versor in the antenna base system, mind that theta mathes with z
    double theta = _getVect2VectAngle(antennaBaseRadiation, dummyThetaVersor);

    std::cout << "\n This coordinate is " << antennaBaseRadiation.getX() << " " <<  antennaBaseRadiation.getY() << " " << antennaBaseRadiation.getZ() << std::endl;

#ifdef DEBUG_COUT
    std::cout << "Theta values are " << theta1 << " and " << theta << std::endl;
#endif


    // TODO: Check with Luca's fix
    // Projection on the Phi-normal plane is represented by corrdinates (Phi,normal)
    // in the antenna orthonormal system. Therefore Phi angle is atan(normal/phi)
    double phi;

    phi = antennaBaseRadiation.getP();

    if (phi >= 180) {
        theta = 180 + (180 - theta);
        phi = phi - 180;
    }
    if ( theta >= 360 - 100 * std::numeric_limits<double>::epsilon() ) {
        theta = 0;
    }
    if ( ( theta <= 100 * std::numeric_limits<double>::epsilon() ) ||  ( std::abs(theta - 180) <= 100 * std::numeric_limits<double>::epsilon() ) ) {
        phi = 0;
        std::cout << "AFFF!!!" << std::endl;
    }

    if ((phi<0) || (phi>=180) || (theta<0) || (theta>=360)) {
        std::cout << "*** Warning *** Out of bound radiation angle... " << std::endl;
    }

    std::cout << "Theta value is " << theta << " Phi value is " << phi << std::endl;

#ifdef DEBUG_COUT
    std::cout << "Theta value is " << theta << " Phi value is " << phi << std::endl;
#endif

    double analyticalGain = sin(theta * M_PI / 180);
    analyticalGain = pow(analyticalGain, 2);
    if (analyticalGain < 0.0) analyticalGain = -analyticalGain;

    double plf = 1;
    double absGain = 0;
    if (_mtxType == "op") {
        absGain = computeOrthogonalPlaneGain(theta, phi, plf,antennaBaseRadiation);
        std::cout << "\033[1;35m This op point gain is: " << absGain << " (ideal value: "<< analyticalGain <<")\033[0m" << std::endl;
    } else if (_mtxType == "cp") {
        absGain = computeCompletePatternGain(theta, phi, plf, antennaBaseRadiation);
        std::cout << "\033[1;35m This cp point gain is: " << absGain << " (ideal value: "<< analyticalGain <<")\033[0m" << std::endl;
    }


    //return analyticalGain;
    return plf*absGain;
}






double CustomAntenna_t::computeOrthogonalPlaneGain(const double theta, const double phi, const double /*plf*/, const Coordinate_t & inputRadiation) const
{
    // locating the radiation segments accordingly to phi-theta values and estimating the gain
    size_t tot_segments;
    double min_angle;
    double min_gain;
    double max_angle;
    double max_gain;
    int i_segment = -1;
    double relativeAngle;
    double relativeGain;

    // PN Gain
    tot_segments = _pnPlane.size();
    for (decltype(tot_segments) k = 0; k < tot_segments; k++ )
    {
        if (phi >= _pnPlane[k].Angle)
        {
            i_segment=int(k);
        }
    }
    min_angle = _pnPlane[size_t(i_segment)].Angle;
    min_gain = _pnPlane[size_t(i_segment)].Gt;
    if (size_t(i_segment) == tot_segments - 1){
        max_angle = 360;
        max_gain = _pnPlane[0].Gt;
    }
    else
    {
        max_angle = _pnPlane[size_t(i_segment)+1].Angle;
        max_gain = _pnPlane[size_t(i_segment)+1].Gt;
    }
    relativeAngle = phi - min_angle;
    relativeGain = (max_gain - min_gain)*relativeAngle/(max_angle - min_angle);
    double pn_dBGain = min_gain + relativeGain;


    // TP Gain
    tot_segments = _tpPlane.size();
    for (decltype(tot_segments) k = 0; k < tot_segments; k++ )
    {
        if (theta >= _tpPlane[k].Angle)
        {
            i_segment=int(k);
        }
    }
    min_angle = _tpPlane[size_t(i_segment)].Angle;
    min_gain = _tpPlane[size_t(i_segment)].Gt;
    if (size_t(i_segment) == tot_segments - 1)
    {
        max_angle = 360;
        max_gain = _tpPlane[0].Gt;
    }
    else
    {
        max_angle = _tpPlane[size_t(i_segment)+1].Angle;
        max_gain = _tpPlane[size_t(i_segment)+1].Gt;
    }
    relativeAngle = theta - min_angle;
    relativeGain = (max_gain - min_gain)*relativeAngle/(max_angle - min_angle);
    double tp_dBGain = min_gain + relativeGain;


    // TN Gain
    tot_segments = _tnPlane.size();
    for (decltype(tot_segments) k = 0; k < tot_segments; k++ )
    {
        if (theta >= _tnPlane[k].Angle)
        {
            i_segment = int(k);
        }
    }
    min_angle = _tnPlane[size_t(i_segment)].Angle;
    min_gain = _tnPlane[size_t(i_segment)].Gt;
    if (size_t(i_segment) == tot_segments - 1)
    {
        max_angle = 360;
        max_gain = _tnPlane[0].Gt;
    }
    else
    {
        max_angle = _tnPlane[size_t(i_segment)+1].Angle;
        max_gain = _tnPlane[size_t(i_segment)+1].Gt;
    }
    relativeAngle = theta - min_angle;
    relativeGain = (max_gain - min_gain)*relativeAngle/(max_angle - min_angle);
    double tn_dBGain = min_gain + relativeGain;

    // finding the angles to the projection planes
    // - find 3D angle with the normal.
    // - adjust values > 90° and get the complementary angle.
    Coordinate_t dummyThetaVersor = Coordinate_t(false, 1, 0, 0);
    double angle2pn = _getVect2VectAngle(inputRadiation, dummyThetaVersor);
    if (angle2pn > 90) { angle2pn = 180 - angle2pn; }
    angle2pn = 90 - angle2pn;

    Coordinate_t dummyNVersor = Coordinate_t(false, 0, 0, 1);
    double angle2tp = _getVect2VectAngle(inputRadiation, dummyNVersor);
    if (angle2tp > 90) { angle2tp = 180 - angle2tp; }
    angle2tp = 90 - angle2tp;

    Coordinate_t dummyPhiVersor = Coordinate_t(false, 0, 1, 0);
    double angle2tn = _getVect2VectAngle(inputRadiation, dummyPhiVersor);
    if (angle2tn > 90) { angle2tn = 180 - angle2tn; }
    angle2tn = 90 - angle2tn;

    // Estimate the Gain as function of the plane distance
    double tot_dBGain;

    if (_doubleEqual(angle2pn, 0.0))
    {
        tot_dBGain = pn_dBGain;
    }
    else if (_doubleEqual(angle2tp, 0.0))
    {
        tot_dBGain = tp_dBGain;
    }
    else if (_doubleEqual(angle2tn, 0.0))
    {
        tot_dBGain = tn_dBGain;
    }
    else
    {
        double totWeight = 1/angle2pn + 1/angle2tp + 1/angle2tn;
        tot_dBGain = (pn_dBGain/angle2pn + tp_dBGain/angle2tp + tn_dBGain/angle2tn)/totWeight;
    }

//    tot_dBGain = (pn_dBGain + tp_dBGain + tn_dBGain)/3;
    double absGain = std::pow(10 , tot_dBGain/10);
/*
    std::cout << "Rx: " << _name << "."
 //    << " PLF angle: " << polarizationAngle << " --> PLF=" << plf
 //    << " nTheta: (" << nTheta.x << ", " << nTheta.y << ", " << nTheta.z << ")."
 //    << " nPhi: (" << nPhi.x << ", " << nPhi.y << ", " << nPhi.z << ")."
 //    << " NORM: (" << nN.x << ", " << nN.y << ", " << nN.z << ")."
     << " RADIATION: (" << inputRadiation.t << ", " << inputRadiation.p << ", " << inputRadiation.n << ")."
     << " min-max for segment " << i_segment << ", gain is: "<< min_gain << "," << max_gain << ", angle is: "<< min_angle << "," << max_angle << ". "
//     << " Theta = " << theta << "°, Theta2 = " << theta2 << "°, Phi = " << phi << "°, Phi2 = " << phi2 << "°."
     << " Theta = " << theta << "°, Phi = " << phi << "°."
//     << " Calculated attenuation is " << dBGain << " dB (abs: " << absGain << ")"
//      with angle " << phi
//     << " PN segment " << pn_segment << " angle " << _pnPlane[pn_segment].Angle
//     << ", TP segment " << tp_segment << " Gt " << _tpPlane[tp_segment].Gt
     << " Angle to PN is " << angle2pn << " (Gain " << pn_dBGain << "), to TP is " << angle2tp << " (Gain " << tp_dBGain << "), to TN is " << angle2tn << " (Gain " << tn_dBGain << "). Tot: " << tot_dBGain
     << std::endl;
*/

    return absGain;
}

double CustomAntenna_t::computeCompletePatternGain(const double theta, const double phi, const double /*plf*/, const Coordinate_t & inputRadiation) const
{
    double tofind_theta = theta;
    double tofind_phi = phi;


    int theta_segment = 0;
    int phi_segment = 0;
    int lt_index = 0;
    int ht_index = 0;
    int lp_index = 0;
    int hp_index = 0;
    double lowerlower_dBGain = 0.0;
    double lowerhigher_dBGain = 0.0;
    double higherlower_dBGain = 0.0;
    double higherhigher_dBGain = 0.0;

    // finding boundary theta-phi value
    for (size_t i = 0; i < _totTheta; i++ )
    {
        if (tofind_theta >= _thetaValues[i])
        {
            theta_segment=int(i);
        }
    }
    for (size_t i = 0; i < _totPhi; i++ )
    {
        if (tofind_phi >= _phiValues[i])
        {
            phi_segment=int(i);
        }
    }

    if (size_t(theta_segment) == _thetaValues.size() - 1)
    {
        lt_index = theta_segment;
        ht_index = 0;
    }
    else
    {
        lt_index = theta_segment;
        ht_index = theta_segment+1;
    }
    if (size_t(phi_segment) == _phiValues.size() - 1)
    {
        lp_index = phi_segment;
        hp_index = 0;
    }
    else
    {
        lp_index = phi_segment;
        hp_index = phi_segment+1;
    }

    // converting boundary angles from spherical to cartesian coordinates

//    double this_t = cos(_thetaValues[size_t(lt_index)] * M_PI / 180);
//    double this_p = sin(_thetaValues[size_t(lt_index)] * M_PI / 180)*cos(_phiValues[size_t(lp_index)] * M_PI / 180);
//    double this_n = sin(_thetaValues[size_t(lt_index)] * M_PI / 180)*sin(_phiValues[size_t(lp_index)] * M_PI / 180);

    Coordinate_t lowerlowerCoord = Coordinate_t(true, _thetaValues[size_t(lt_index)], _phiValues[size_t(lp_index)], 1);

//    std::cout << "LL is: " << lowerlowerCoord.t << ", " << lowerlowerCoord.p << ", " << lowerlowerCoord.n << "."  << std::endl;

//    this_t = cos(_thetaValues[size_t(lt_index)] * M_PI / 180);
//    this_p = sin(_thetaValues[size_t(lt_index)] * M_PI / 180)*cos(_phiValues[size_t(hp_index)] * M_PI / 180);
//    this_n = sin(_thetaValues[size_t(lt_index)] * M_PI / 180)*sin(_phiValues[size_t(hp_index)] * M_PI / 180);

    Coordinate_t lowerhigherCoord = Coordinate_t(true, _thetaValues[size_t(lt_index)], _phiValues[size_t(hp_index)], 1);

//    std::cout << "LH is: " << lowerhigherCoord.t << ", " << lowerhigherCoord.p << ", " << lowerhigherCoord.n << "."  << std::endl;

//    this_t = cos(_thetaValues[size_t(ht_index)] * M_PI / 180);
//    this_p = sin(_thetaValues[size_t(ht_index)] * M_PI / 180)*cos(_phiValues[size_t(lp_index)] * M_PI / 180);
//    this_n = sin(_thetaValues[size_t(ht_index)] * M_PI / 180)*sin(_phiValues[size_t(lp_index)] * M_PI / 180);

    Coordinate_t higherlowerCoord = Coordinate_t(true, _thetaValues[size_t(ht_index)], _phiValues[size_t(lp_index)], 1);

//    std::cout << "HL is: " << higherlowerCoord.t << ", " << higherlowerCoord.p << ", " << higherlowerCoord.n << "."  << std::endl;

//    this_t = cos(_thetaValues[size_t(ht_index)] * M_PI / 180);
//    this_p = sin(_thetaValues[size_t(ht_index)] * M_PI / 180)*cos(_phiValues[size_t(hp_index)] * M_PI / 180);
//    this_n = sin(_thetaValues[size_t(ht_index)] * M_PI / 180)*sin(_phiValues[size_t(hp_index)] * M_PI / 180);

    Coordinate_t higherhigherCoord = Coordinate_t(true, _thetaValues[size_t(ht_index)], _phiValues[size_t(hp_index)], 1);

//    std::cout << "HH is: " << higherhigherCoord.t << ", " << higherhigherCoord.p << ", " << higherhigherCoord.n << "."  << std::endl;

    lowerlower_dBGain = _patternMatrix  [size_t(lt_index)][size_t(lp_index)].Gt;
    lowerhigher_dBGain = _patternMatrix [size_t(lt_index)][size_t(hp_index)].Gt;
    higherlower_dBGain = _patternMatrix [size_t(ht_index)][size_t(lp_index)].Gt;
    higherhigher_dBGain = _patternMatrix[size_t(ht_index)][size_t(hp_index)].Gt;

    // finding angles between boundaries and radiation and calculating weighted gain


    lowerlowerCoord.print("lowerlower");
    lowerhigherCoord.print("lowerhigher");
    higherlowerCoord.print("higherlower");
    higherhigherCoord.print("higherhigher");

    double angle2ll = _getVect2VectAngle(inputRadiation, lowerlowerCoord);
    double angle2lh = _getVect2VectAngle(inputRadiation, lowerhigherCoord);
    double angle2hl = _getVect2VectAngle(inputRadiation, higherlowerCoord);
    double angle2hh = _getVect2VectAngle(inputRadiation, higherhigherCoord);

    double tot_dBGain;

    if ((angle2ll <= 0.0 && angle2ll >= 0.0) || (angle2lh <= 0.0 && angle2lh >= 0.0) || (angle2hl <= 0.0 && angle2hl >= 0.0) || (angle2hh <= 0.0 && angle2hh >= 0.0))
    {
        tot_dBGain = lowerlower_dBGain;
    }
    else
    {
        double totWeight = 1/angle2ll + 1/angle2lh + 1/angle2hl + 1/angle2hh;
        tot_dBGain = (lowerlower_dBGain/angle2ll + lowerhigher_dBGain/angle2lh + higherlower_dBGain/angle2hl + higherhigher_dBGain/angle2hh)/totWeight;
    }

    double absGain = std::pow(10 , tot_dBGain/10);

#ifdef DEBUG_COUT
    std::cout << "Rx: " << _name << "."
              << "RADIATION: (" << inputRadiation._getx() << ", " << inputRadiation._gety() << ", " << inputRadiation._getz() << "). "
              << "Theta = " << theta << "°, Phi = " << phi << "°. "
              << "Theta values around radiation: (" << _thetaValues[size_t(lt_index)] << ", " << _thetaValues[size_t(ht_index)] << "), "
              << "Phi values around radiation: (" << _phiValues[size_t(lp_index)] << ", " << _phiValues[size_t(hp_index)] << "), "
              << "Separation angles: " << angle2ll << ", " << angle2lh << ", " << angle2hl << ", " << angle2hh << "."
              << std::endl;
#endif

    return absGain;
}
