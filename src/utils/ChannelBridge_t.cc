// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Encompasses a stack of communicators.



#include "scnsl/utils/ChannelBridge_t.hh"


using namespace Scnsl::Utils;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

ChannelBridge_t::ChannelBridge_t( ChannelWrapper_if_t * cw ):
    // Parents:
    Scnsl::Core::Node_t(0),
    // Fields:
    _channelWrapper( cw )
{

}

ChannelBridge_t::~ChannelBridge_t()
{

}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////


ChannelBridge_t::errorcode_t ChannelBridge_t::send( const Scnsl::Core::Packet_t & /*p*/ )

{
    return 0;
}


void ChannelBridge_t::setCarrier( const Scnsl::Core::Channel_if_t * /*ch*/, const carrier_t c )
{
	_channelWrapper->bridgeSetCarrier( this, c );
}


ChannelBridge_t::errorcode_t ChannelBridge_t::receive( const Scnsl::Core::Packet_t & p )

{
	return _channelWrapper->bridgeReceive( this, p );
}

void ChannelBridge_t::bindTaskProxy( const Scnsl::Core::TaskProxy_if_t * /*tp*/,
                                           Scnsl::Core::Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid bindTaskProxy() from channel bridge.");
}


void ChannelBridge_t::stackDown( Scnsl::Core::Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid stackDown() from channel bridge.");
}
