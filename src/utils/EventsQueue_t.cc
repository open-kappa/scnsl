// -*- SystemC -*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#include <algorithm>
#include <sstream>
#include "scnsl/utils/EventsQueue_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using namespace Scnsl::Utils;


// ////////////////////////////////////////////////////////////////
// Traits methods.
// ////////////////////////////////////////////////////////////////



EventsQueue_t::base_instance_data_t::~base_instance_data_t()
{
    // Nothing to do.
}



EventsQueue_t::queue_event_t::queue_event_t():
    time(),
    action( nullptr ),
    data( nullptr ),
    type(),
    id()
{
    // ntd
}

bool EventsQueue_t::queue_event_t::operator < ( const queue_event_t & e ) const
{
    // Since the priority is the reverse of time,
    // we have to perform the opposite check.
    //
    // The < is used and not <= to preserve relative enqueueing order
    // for events scheduled at the same time.
    return e.time < time;
}

bool EventsQueue_t::queue_event_t::operator == ( const queue_event_t & e )
    const
{
	return id == e.id;
}


// ////////////////////////////////////////////////////////////////
// node_instance_data_t
// ////////////////////////////////////////////////////////////////


EventsQueue_t::node_instance_data_t::node_instance_data_t():
    n( nullptr ),
    np(),
    ch( nullptr )
{
    // ntd
}

EventsQueue_t::node_instance_data_t::~node_instance_data_t()
{
    // ntd
}

void EventsQueue_t::node_instance_data_t::call()
{
    n->setProperties( np, ch, false );
}


// ////////////////////////////////////////////////////////////////
// Singleton design pattern.
// ////////////////////////////////////////////////////////////////

EventsQueue_t * EventsQueue_t::get_instance()
{
    static EventsQueue_t queue( "EventsQueue" );
    return & queue;
}

// ////////////////////////////////////////////////////////////////
// Events management functions.
// ////////////////////////////////////////////////////////////////


EventsQueue_t::event_id_t EventsQueue_t::registerAction( const sc_core::sc_time & time,
                                            sc_core::sc_event * event )
{
    SCNSL_TRACE_DBG( 3, "<> registerAction(): event." );

    // Storing the event-relative id:
    event_id_t id = _event_id;

    queue_event_t queue_event;
    queue_event.time = time;
    queue_event.action = & _event_action_performer;
    queue_event.data = reinterpret_cast< void * >( event );
    queue_event.type = EVENT_DATA;
    queue_event.id = id;

    ++_event_id;

    // Storing the event in the _queue:
    _queue.push_back( queue_event );
    // Sorting the _queue:
    _queue.sort(_compareTime);
    if ( time <= _queue.front().time ) _headInsertedEvent.notify();

    // Return the event-relative id:
    return id;
}


EventsQueue_t::event_id_t EventsQueue_t::registerNodeProperties( const sc_core::sc_time & time,
    		                                        Node_t * n,
    		                                        node_properties_t np,
    		                                        Channel_if_t * ch )
{
	const sc_core::sc_time currentTime = sc_core::sc_time_stamp();

#if (SCNSL_LOG >= 1)
	if ( time == currentTime )
	{
    std::stringstream ss;
    ss<< "Warning: a node movement was registered at current time. The simulation may not be correct.";
    SCNSL_TRACE_LOG( 3, ss.str().c_str() );
	}
#endif

	SCNSL_TRACE_DBG( 3, "<> registerNodeMovement(): instance callback." );

    // Storing the event-relative id:
    event_id_t id = _event_id;

	node_instance_data_t * nd = new node_instance_data_t();

	nd->n = n;
	nd->np = np;
	nd->ch = ch;

    queue_event_t queue_event;
    queue_event.time = time;
    queue_event.action = & _instance_callback_action_performer;
    queue_event.data = reinterpret_cast< void * >( nd );
    queue_event.type = INSTANCE_METHOD_DATA;
    queue_event.id = id;

	++_event_id;

    // Storing the event in the _queue:
	_queue.push_back( queue_event );
    // Sorting the _queue:
	_queue.sort(_compareTime);
	if ( time <= _queue.front().time ) _headInsertedEvent.notify();

    // Return the event-relative id:
	return id;
}


void EventsQueue_t::removeAction( event_id_t event_id )

{
	queue_event_t matcher;
	matcher.id = event_id;

	std::list< queue_event_t >::iterator it;

	// Find the id-relative event:
	it = find ( _queue.begin(), _queue.end(), matcher );
    if ( it == _queue.end() ) throw std::domain_error("Event ID not found.");

	// Delete the event:
	_queue.erase( it );
}


const sc_core::sc_time & EventsQueue_t::getActionTime( const event_id_t event_id )

{
	queue_event_t matcher;
	matcher.id = event_id;

	std::list< queue_event_t >::iterator it;

	// Find the id-relative event:
	it = find ( _queue.begin(), _queue.end(), matcher );
    if ( it == _queue.end() ) throw std::domain_error("Event ID not found.");


	// Return the simulation time into which fire the action:
	return it->time;
}


void EventsQueue_t::updateAction( event_id_t event_id, const sc_core::sc_time & time )

{
	queue_event_t matcher;
	matcher.id = event_id;

	std::list< queue_event_t >::iterator it;

	// Find the id-relative event:
	it = find ( _queue.begin(), _queue.end(), matcher );
    if ( it == _queue.end() ) throw std::domain_error("Event ID not found.");

	// Updating the simulation time into which fire the action:
	it->time = time;
    // Sorting the _queue:
	_queue.sort( _compareTime );
}


// ///////////////////////////////////////////////////
// Constructor and destructor.
// ///////////////////////////////////////////////////


EventsQueue_t::EventsQueue_t( const sc_core::sc_module_name modulename ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Fields:
    _headInsertedEvent(),
    _queue(),
    _event_id( 0 )
{
    SCNSL_TRACE_DBG( 5, "<> EventsQueue_t()." );
    SC_THREAD( _process );
}



EventsQueue_t::~EventsQueue_t()
{
    // Let's free enqueued events.
    while ( ! _queue.empty() )
    {
        switch ( _queue.front().type )
        {
            case EVENT_DATA:
                break;
            case METHOD_DATA:
                break;
            case INSTANCE_METHOD_DATA:
            {
                base_instance_data_t * dd = reinterpret_cast< base_instance_data_t * >( _queue.front().data );
                delete dd;
                break;
            }
            default: ;
        }
        _queue.pop_front();
    }
}


// ////////////////////////////////////////////////////////////////
// Support callback methods.
// ////////////////////////////////////////////////////////////////


void EventsQueue_t::_event_action_performer( void * d )
{
    sc_core::sc_event * e  = reinterpret_cast< sc_core::sc_event * >( d );
    e->notify();
}

void EventsQueue_t::_instance_callback_action_performer( void * d )
{
    base_instance_data_t * dd = reinterpret_cast< base_instance_data_t * >( d );
    dd->call();
    delete dd;
}



// ///////////////////////////////////////////////////
// Process.
// ///////////////////////////////////////////////////


void EventsQueue_t::_process()
{
    sc_core::sc_time time( sc_core::sc_time_stamp() );

	for (;;)
    {
        // Let's sleep...
        if ( _queue.empty() )
        {
            SCNSL_TRACE_DBG( 5, "<> _process(): waiting event." );
            wait( _headInsertedEvent );
        }

        time =  _queue.front().time - sc_core::sc_time_stamp();

        if ( time > sc_core::SC_ZERO_TIME )
        {
            SCNSL_TRACE_DBG( 3, "<> _process(): waiting event or timeout." );

#if (SCNSL_DBG >= 5 )
            std::stringstream ss;
            ss << "time to wait = " << time << ".";
            SCNSL_TRACE_DBG( 5, ss.str().c_str());
#endif

            wait( time, _headInsertedEvent );
        }

        SCNSL_TRACE_DBG( 3, "<> _process()." );

        time = sc_core::sc_time_stamp();

        // Let's try to dispatch events (if any).
        while ( ( ! _queue.empty() ) && time >=  _queue.front().time )
        {
            SCNSL_TRACE_DBG( 5, "<> _process(): dispatching." );

            const queue_event_t e( _queue.front() );
            _queue.pop_front();
            ( * ( e.action ) ) ( e.data );
        }
    }
}


// ////////////////////////////////////////////////////////////////
// Input method.
// ////////////////////////////////////////////////////////////////

bool EventsQueue_t::_compareTime( queue_event_t first_e, queue_event_t second_e)
{
	if ( second_e.time < first_e.time )
		return false;
	else if ( first_e.time < second_e.time )
		return true;

	return false;
}
