#if(defined _WIN32)
#define _USE_MATH_DEFINES
#endif
#include <cmath>
#include <limits>

#include "scnsl/utils/DirectionalEnvironment_t.hh"

using Scnsl::Utils::DirectionalEnvironment_t;
using namespace Scnsl::Core;

DirectionalEnvironment_t::DirectionalEnvironment_t(
        const double alpha, const double radio_freq, const Scnsl::Core::propagation_t propagation):
    _ALPHA_EXPONENT(alpha),
    _RADIO_FREQ(radio_freq),
    _PROPAGATION(propagation)
{
    // ntd
}

DirectionalEnvironment_t::~DirectionalEnvironment_t()
{
    // Nothing to do.
}

void DirectionalEnvironment_t::createInstance(const double alpha,
                                              const double radio_freq,
                                              const propagation_t propagation)
{
    static DirectionalEnvironment_t p(alpha, radio_freq, propagation);
    DirectionalEnvironment_t::_env = &p;
}

double DirectionalEnvironment_t::_getDistance(const node_properties_t & sp, 
                                              const node_properties_t & rp)
{
    const position_t dx = rp.x - sp.x;
    const position_t dy = rp.y - sp.y;
    const position_t dz = rp.z - sp.z;
    const double d2 = (dx * dx) + (dy * dy) + (dz * dz);

    return sqrt(d2);
}


bool DirectionalEnvironment_t::_doubleEqual(const double a, const double b)
{
    return fabs(a - b) < std::numeric_limits<double>::epsilon();
}


double DirectionalEnvironment_t::getReceiverPower(const node_properties_t & sp,
                                                  const node_properties_t & rp)
{
    double power = 0;
    double distance = 0;
    distance = _getDistance(sp, rp);

    // frequency in hertz
    double freq_hz = _RADIO_FREQ * 1000000;
    // speed of light
    const double c_0 = 299792458;

    // Isotropic source NODE and Isotropic receiver NODE
    if(sp.lobes.empty() && rp.lobes.empty())
    {
        if (!_isDoubleEquals(_RADIO_FREQ, 0.0))
        {
             power = sp.transmission_power * pow(c_0,2) / pow(4 * M_PI * distance * freq_hz, 2);
        }
        else
        {
            power = (sp.transmission_power / pow(distance, _ALPHA_EXPONENT));
        }
    }

    // Isotropic source NODE and Directional receiver NODE
    if(sp.lobes.empty() && !rp.lobes.empty())
    {
        for(Lobes::const_iterator it = rp.lobes.begin(); it != rp.lobes.end(); ++it)
        {
            if (!_isDoubleEquals(_RADIO_FREQ, 0.0))
            {
                RadiationPattern_if_t * lobe = * it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                const bool isIntereseted = lobe->isTheInterestedPattern(rp, sp);
                if (!isIntereseted) continue;

                power += sp.transmission_power * lobe->getGain(rp, sp) * pow(c_0, 2) / pow(4 * M_PI * distance * freq_hz, 2);
            }
            else
            {
                RadiationPattern_if_t * lobe = *it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                const bool isIntereseted = lobe->isTheInterestedPattern(rp, sp);
                if (!isIntereseted) continue;

                power += sp.transmission_power * lobe->getGain(rp, sp) / pow(distance, _ALPHA_EXPONENT);
            }
        }
    }

    // Directional source NODE and Isotropic receiver NODE
    if( !sp.lobes.empty() && rp.lobes.empty())
    {
        for(Lobes::const_iterator it = sp.lobes.begin(); it != sp.lobes.end(); ++it)
        {
            if (!_isDoubleEquals(_RADIO_FREQ, 0.0))
            {
                RadiationPattern_if_t * lobe = * it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                const bool isIntereseted = lobe->isTheInterestedPattern(rp, sp);
                if (!isIntereseted) continue;

                power += sp.transmission_power * lobe->getGain(sp, rp) * pow(c_0,2) / pow(4*M_PI*distance*freq_hz, 2);
            }
            else
            {
                RadiationPattern_if_t * lobe = *it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                const bool isIntereseted = lobe->isTheInterestedPattern(sp, rp);
                if (!isIntereseted) continue;

                power += sp.transmission_power * lobe->getGain(sp, rp) / pow(distance, _ALPHA_EXPONENT);
            }
        }
    }

    // Directional source NODE and Directional receiver NODE
    if (!sp.lobes.empty() && !rp.lobes.empty())
    {
        for(Lobes::const_iterator it = sp.lobes.begin(); it != sp.lobes.end(); ++it)
        {
            if (!_isDoubleEquals(_RADIO_FREQ, 0.0))
            {
                RadiationPattern_if_t * lobe = *it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                RadiationPattern_if_t * srcLobe = *it;
                // Skip inactive lobes
                if(!srcLobe->isActive()) continue;

                const bool isInteresetedSrc = srcLobe->isTheInterestedPattern(sp, rp);
                if (!isInteresetedSrc) continue;

                for(Lobes::const_iterator jt = rp.lobes.begin(); jt != rp.lobes.end(); ++jt)
                {
                    RadiationPattern_if_t * dstLobe = * jt;
                    if(!lobe->isActive()) continue;
                    const bool isIntereseted = lobe->isTheInterestedPattern(rp, sp);
                    if (!isIntereseted) continue;

                    power += sp.transmission_power * dstLobe->getGain(rp, sp) * srcLobe->getGain(sp, rp) * pow(c_0,2) / pow(4*M_PI*distance*freq_hz, 2);
             //       std::cout<< " Gain from radiation pattern is: " << srcLobe->getGain(sp, rp) << " (source pattern) " << dstLobe->getGain(rp, sp) << " (destination pattern)." << std::endl;
                }
            }
            else
            {
                RadiationPattern_if_t * lobe = *it;
                // Skip inactive lobes
                if(!lobe->isActive()) continue;
                RadiationPattern_if_t * srcLobe = *it;
                // Skip inactive lobes
                if(!srcLobe->isActive()) continue;

                const bool isInteresetedSrc = srcLobe->isTheInterestedPattern(sp, rp);
                if (!isInteresetedSrc) continue;

                for(Lobes::const_iterator jt = rp.lobes.begin(); jt != rp.lobes.end(); ++jt)
                {
                    RadiationPattern_if_t * dstLobe = *jt;
                    // Skip inactive lobes
                    if(!dstLobe->isActive()) continue;
                    const bool isInteresetedDst = dstLobe->isTheInterestedPattern(rp, sp);
                    if (!isInteresetedDst) continue;

                    power += sp.transmission_power * dstLobe->getGain(rp, sp) * srcLobe->getGain(sp, rp) / pow( distance, _ALPHA_EXPONENT);
                }
            }
        }
    }

    return power;
}

double DirectionalEnvironment_t::getMaxDelay( const node_properties_t & sp,
                                              const node_properties_t & rp)
{
    return pow(sp.transmission_power / rp.receiving_threshold,
               1 / _ALPHA_EXPONENT) / _PROPAGATION;

}

double DirectionalEnvironment_t::getBitErrorRate(const node_properties_t & /*sp*/,
                                                 const node_properties_t & /*rp*/)
{
    return 0.0;
}

Scnsl::Core::propagation_t DirectionalEnvironment_t::getPropagation()
{
    return _PROPAGATION;
}
