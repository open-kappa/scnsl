#include "scnsl/utils/DefaultEnvironment_t.hh"

using Scnsl::Utils::DefaultEnvironment_t;


DefaultEnvironment_t::DefaultEnvironment_t(
    const double alpha, const propagation_t propagation):
    _ALPHA_EXPONENT(alpha),
    _PROPAGATION(propagation)
{
    // Nothing to do.
}

DefaultEnvironment_t::~DefaultEnvironment_t()
{
    // Nothing to do.
}

void DefaultEnvironment_t::createInstance(const double alpha,const propagation_t propagation)
{
    static DefaultEnvironment_t p(alpha, propagation);

    DefaultEnvironment_t::_env = &p;

}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////


double DefaultEnvironment_t::getReceiverPower( const node_properties_t & sp,
    const node_properties_t & rp){

    const position_t dx = sp.x - rp.x;
    const position_t dy = sp.y - rp.y;
    const position_t dz = sp.z - rp.z;
    const position_t d2 = dx*dx + dy*dy + dz*dz;
    const double distance = sqrt( d2 );

    return (sp.transmission_power / pow( distance, _ALPHA_EXPONENT ));

}

double DefaultEnvironment_t::getMaxDelay( const node_properties_t & sp,
    const node_properties_t & rp)
{
    return pow(sp.transmission_power/rp.receiving_threshold, 1/_ALPHA_EXPONENT)/_PROPAGATION;

}

double DefaultEnvironment_t::getBitErrorRate(const node_properties_t & /*sp*/,
    const node_properties_t & /*rp*/)
{
    return 0.0;
}

Scnsl::Core::propagation_t DefaultEnvironment_t::getPropagation(){
    return _PROPAGATION;
}




