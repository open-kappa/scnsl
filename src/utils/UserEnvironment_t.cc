#include "scnsl/utils/UserEnvironment_t.hh"

using Scnsl::Utils::UserEnvironment_t;


UserEnvironment_t::UserEnvironment_t(receiverPowerFunction_t function, 
    const double alpha, const propagation_t propagation):
    _getReceiverPower(function),
    _ALPHA_EXPONENT(alpha),
    _PROPAGATION(propagation)
{
    // Nothing to do.
}

UserEnvironment_t::~UserEnvironment_t()
{
    // Nothing to do.
}

void UserEnvironment_t::createInstance(receiverPowerFunction_t function, 
    const double alpha,const propagation_t propagation)
{
    static UserEnvironment_t p(function, alpha, propagation);
    
    UserEnvironment_t::_env = &p;
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

double UserEnvironment_t::getReceiverPower( const node_properties_t & sp,
    const node_properties_t & rp)
{
    return _getReceiverPower(sp, rp);
}

double UserEnvironment_t::getMaxDelay( const node_properties_t & sp,
    const node_properties_t & rp)
{
    return pow(sp.transmission_power/rp.receiving_threshold, 1/_ALPHA_EXPONENT)/_PROPAGATION;
}

double UserEnvironment_t::getBitErrorRate(const node_properties_t & /*sp*/,
    const node_properties_t & /*rp*/)
{
    return 0.0;
}

Scnsl::Core::propagation_t UserEnvironment_t::getPropagation()
{
    return _PROPAGATION;
}
