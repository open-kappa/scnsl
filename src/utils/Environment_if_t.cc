// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The Environment interface.



#include "scnsl/utils/Environment_if_t.hh"
#include "scnsl/utils/DefaultEnvironment_t.hh"


using Scnsl::Utils::Environment_if_t;

Environment_if_t* Environment_if_t::_env = nullptr;

// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

Environment_if_t::~Environment_if_t()
{
    // Nothing to do.
}

Environment_if_t::Environment_if_t()
{
    // Nothing to do.
}

bool Environment_if_t::_isDoubleEquals(const double d1, const double d2)
{
    return d1 <= d2 && d1 >= d2;
}

Environment_if_t* Environment_if_t::getInstance()
{
    if(Environment_if_t::_env == nullptr)
    {
        Environment_if_t::_env = new DefaultEnvironment_t();
    }

    return Environment_if_t::_env;
}
