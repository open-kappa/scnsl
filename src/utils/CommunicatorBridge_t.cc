// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
///  Links a CommunicatorStack with a Communicator chain.



#include "scnsl/utils/CommunicatorBridge_t.hh"
#include "scnsl/utils/CommunicatorStack_t.hh"


using namespace Scnsl::Utils;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////


CommunicatorBridge_t::CommunicatorBridge_t( CommunicatorStack_t * cs ):
    // Parents:
    Scnsl::Core::Communicator_if_t(),
    // Fields:
    _communicatorStack( cs )
{
    // Nothing to do.
}

CommunicatorBridge_t::~CommunicatorBridge_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void CommunicatorBridge_t::setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c )

{
    _communicatorStack->bridgeSetCarrier( ch, c );
}

CommunicatorBridge_t::errorcode_t CommunicatorBridge_t::send( const Scnsl::Core::Packet_t & p )

{
    return _communicatorStack->bridgeSend( p );
}


CommunicatorBridge_t::errorcode_t CommunicatorBridge_t::receive( const Scnsl::Core::Packet_t & p )

{
    return _communicatorStack->bridgeReceive( p );
}


void CommunicatorBridge_t::bindTaskProxy( const Scnsl::Core::TaskProxy_if_t * /*tp*/,
                                          Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid bindTaskProxy() from communicator bridge.");
}

void CommunicatorBridge_t::bindChannel( const Scnsl::Core::Channel_if_t * /*ch*/,
                                        Scnsl::Core::Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid bindChannel() from communicator bridge.");
}


void CommunicatorBridge_t::stackUp( Scnsl::Core::Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid stackUp() from communicator bridge.");
}

void CommunicatorBridge_t::stackDown( Scnsl::Core::Communicator_if_t * /*c*/ )

{
    throw std::logic_error("Invalid stackDown() from communicator bridge.");
}
