// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Encompasses a stack of communicators.



#include "scnsl/utils/CommunicatorStack_t.hh"

#ifdef _MSC_VER
#pragma warning(disable:4355)
#endif

using namespace Scnsl::Utils;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

CommunicatorStack_t::CommunicatorStack_t( Scnsl::Core::Communicator_if_t * head,
                                          Scnsl::Core::Communicator_if_t * tail ):
    // Parents:
    Scnsl::Core::Communicator_if_t(),
    // Fields:
    _head( head ),
    _tail( tail ),
    _ownedCommunicators(),
    _bridge( this )
{
    // Binding the bridge with the communicators chain:
    _head->stackUp( & _bridge );
    _tail->stackDown( & _bridge );
}

CommunicatorStack_t::~CommunicatorStack_t()
{
    // Deleting owned communicators.
    for ( CommunicatorList_t::iterator i = _ownedCommunicators.begin();
          i != _ownedCommunicators.end();
          ++ i )
    {
        delete *i;
    }
}


// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////

void CommunicatorStack_t::acquireOwnership( Scnsl::Core::Communicator_if_t * c )
{
    _ownedCommunicators.push_back( c );
}

// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

void CommunicatorStack_t::setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c )

{
    _tail->setCarrier( ch, c );
}

CommunicatorStack_t::errorcode_t CommunicatorStack_t::send( const Scnsl::Core::Packet_t & p )

{
    return _head->send(p);
}

CommunicatorStack_t::errorcode_t CommunicatorStack_t::receive( const Scnsl::Core::Packet_t & p )

{
    return _tail->receive( p );
}


// ////////////////////////////////////////////////////////////////
// Methods for interconnection with the internal bridge.
// ////////////////////////////////////////////////////////////////


void CommunicatorStack_t::bridgeSetCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c )

{
    Scnsl::Core::Communicator_if_t::setCarrier( ch, c );
}

CommunicatorStack_t::errorcode_t CommunicatorStack_t::bridgeSend( const Scnsl::Core::Packet_t & p )

{
    return Scnsl::Core::Communicator_if_t::send( p );
}

CommunicatorStack_t::errorcode_t CommunicatorStack_t::bridgeReceive( const Scnsl::Core::Packet_t & p )

{
    return Scnsl::Core::Communicator_if_t::receive( p );
}
