// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Priority queue.


#include "scnsl/queueModels/Priority_t.hh"


using Scnsl::QueueModels::Priority_t;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

Priority_t::Priority_t( const size_t capacity,
						const size_t queueNumber,
						policy_type_t policy,
						const size_t * weights )
    :
    // Parent:
    Queue_if_t(),
    // Fields:
	_queueNumber( queueNumber ),
	_capacity( nullptr ),
	_capacityTot( capacity ),
	_lists( nullptr ),
	_pkt(),
	_policy( policy ),
	_weights( nullptr ),
	_totSize( 0 ),
	_totPackets( 0 ),
	_weightMin( 0 ),
	_count( nullptr ),
	_served( 0 ),
	_first( true )
{
	if ( capacity == 0 )
    {
        throw std::invalid_argument( "Invalid capacity" );
    }

	if ( _queueNumber == 0 )
    {
        throw std::invalid_argument( "Invalid queue number" );
    }

    _capacity = static_cast< size_t * >(
        operator new [] ( _queueNumber * sizeof( size_t ) ));

    _lists = static_cast< std::list< Packet_t > * >(
        operator new [] ( _queueNumber * sizeof( std::list< Packet_t > ) ));

    _count = static_cast< size_t * >(
        operator new [] ( _queueNumber * sizeof( size_t ) ));


	for(unsigned int i = 0; i < _queueNumber; i++)
	{
		_capacity[ i ] = capacity;
        new ( _lists + i ) std::list< Packet_t >();
	}

	if( weights != nullptr )
	{
		_weights = static_cast< size_t * >(
	        operator new [] ( _queueNumber * sizeof( size_t ) ));


		_weightMin = weights[ 0 ];

		for(unsigned int i = 0; i < _queueNumber; i++)
		{
			_weights[ i ] = weights[ i ];

			if( _weights[ i ] < _weightMin )
				_weightMin = _weights[ i ];
		}
	}

}


Priority_t::~Priority_t()
{
	for( unsigned int i = 0; i < _queueNumber; ++i )
	{
		_lists[ i ].std::list< Packet_t >::~list();
	}

    // Deallocating the memory.
    ::operator delete[] ( _capacity );
    ::operator delete[] ( _lists );
    ::operator delete[] ( _count );
	::operator delete[] ( _weights );
}


// ////////////////////////////////////////////////////////////////
// Traffic interface methods.
// ////////////////////////////////////////////////////////////////

bool Priority_t::enqueue( Packet_t & p )
{
	label_t pr = p.getLabel();

	if( pr >= _queueNumber )
		pr = _queueNumber - 1;

	// There is available space?
	for( unsigned int i = pr; i < _queueNumber; ++i )
	{
		if( _capacity[ i ] >= p.getSizeInBytes() )
		{
		    _lists[ i ].push_back( p );
		    _capacity[ i ] -= p.getSizeInBytes();
			_totPackets++;
			_totSize += p.getSizeInBytes();

		    return true;
		}
	}

    return false;
}


Priority_t::Packet_t & Priority_t::dequeue()
{
	if( _policy == Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t::STRONG_PRIORITY )
	for( unsigned int i = 0; i < _queueNumber; ++i )
	{
		if( !_lists[ i ].empty() )
		{
			_pkt = _lists[ i ].front();
			_lists[ i ].pop_front();
			_capacity[ i ] += _pkt.getSizeInBytes();

			_totPackets--;
			_totSize -= _pkt.getSizeInBytes();

			return _pkt;
		}
	}

	if( _policy == Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t::W_ROUND_ROBIN )
	{

		if( _first )
		{
			updateCount();
			_first = false;
		}

		for (;;)
		{
			if( !_lists[ _served ].empty() )
			{
				_pkt = _lists[ _served ].front();
				if( _count[ _served ] >= _pkt.getSizeInBytes() )
				{
					_lists[ _served ].pop_front();
					_capacity[ _served ] += _pkt.getSizeInBytes();
					_count[ _served ] -= _pkt.getSizeInBytes();

					_totPackets--;
					_totSize -= _pkt.getSizeInBytes();

					return _pkt;
				}
				else
				{
					_served = ( _served + 1 ) % _queueNumber;

					if( _served == 0 )
						updateCount();
				}
			}
			else
			{
				_served = ( _served + 1 ) % _queueNumber;

				if( _served == 0 )
					updateCount();
			}
		}
	}

	return _pkt;
}

void Priority_t::updateCount()
{
		double avgPacketSize = _totSize / _totPackets;

		for( unsigned int i = 0; i < _queueNumber; ++i )
		{
			_count[ i ] = static_cast< size_t >( _weights[ i ] * avgPacketSize / _weightMin );
		}
}


bool Priority_t::isEmpty()
{
	for( unsigned int i = 0; i < _queueNumber; ++i )
		if (!_lists[ i ].empty()) return false;

	return true;
}


bool Priority_t::check( const Packet_t & p )
{
	label_t pr = p.getLabel();
	bool c = false;

	for( unsigned int i = pr; i < _queueNumber && !c; ++i )
		c |= _capacity[ i ] >= p.getSizeInBytes();

	return c;
}


bool Priority_t::checkTot( const Packet_t & p )
{
	return _capacityTot >= p.getSizeInBytes();
}


Priority_t::size_t Priority_t::getCapacity()
	const
{
	size_t available = 0;
	for( unsigned int i = 0; i < _queueNumber; ++i )
		available += _capacity[ i ];

	return available;
}


Priority_t::size_t Priority_t::getCapacityTot()
	const
{
	return _capacityTot * _queueNumber;
}
