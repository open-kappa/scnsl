// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Fifo queue.


#include "scnsl/queueModels/Fifo_t.hh"


using Scnsl::QueueModels::Fifo_t;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

Fifo_t::Fifo_t( const size_t capacity )
    :
    // Fields:
	_capacity( capacity ),
	_capacityTot( capacity ),
	_list(),
	_pkt()
{
 	if ( _capacity == 0 )
    {
        throw std::invalid_argument( "Invalid capacity" );
    }
}


Fifo_t::~Fifo_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Traffic interface methods.
// ////////////////////////////////////////////////////////////////

bool Fifo_t::enqueue( Packet_t & p )
{
    // There is available space?
    if( _capacity >= p.getSizeInBytes() )
    {
        _list.push_back( p );
        _capacity -= p.getSizeInBytes();
        return true;
    }

    return false;
}


Fifo_t::Packet_t & Fifo_t::dequeue()
{
    // Checking for safety
    if( ! _list.empty() )
    {
        _pkt = _list.front();
        _list.pop_front();
        _capacity += _pkt.getSizeInBytes();
    }

    return _pkt;
}


bool Fifo_t::isEmpty()
{
	return _list.empty();
}


bool Fifo_t::check( const Packet_t & p )
{
	return _capacity >= p.getSizeInBytes();
}


bool Fifo_t::checkTot( const Packet_t & p )
{
	return _capacityTot >= p.getSizeInBytes();
}
