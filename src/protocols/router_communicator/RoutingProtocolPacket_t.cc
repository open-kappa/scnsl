// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

///@file
/// The basic implementation of the ProtocolPacket interface used to store a buffer


#include "scnsl/protocols/router_communicator/RoutingProtocolPacket_t.hh"

using namespace Scnsl::Protocols::RouterCommunicator;
using namespace Scnsl::Core;


// ////////////////////////////////////////////////////////////////
// Constructors.
// ////////////////////////////////////////////////////////////////

RoutingProtocolPacket_t::RoutingProtocolPacket_t():
ProtocolPacket_if_t(),
_payload(nullptr),
_dest(nullptr)
{
    // Nothing to do
}


RoutingProtocolPacket_t::RoutingProtocolPacket_t(const RoutingProtocolPacket_t &p):
ProtocolPacket_if_t(p),
_payload(nullptr),
_dest(nullptr)
{
    _payload = p.getPayload();
    _dest = p.getDestination();
}


RoutingProtocolPacket_t::~RoutingProtocolPacket_t()
{
    delete _payload;
}


RoutingProtocolPacket_t &RoutingProtocolPacket_t::operator= (const RoutingProtocolPacket_t &p)
{
    // payload fields
    _payload = p.getPayload();
    _dest = p.getDestination();
    _header_size = p._header_size;
    return *this;
}

void RoutingProtocolPacket_t::setDestination(const Node_t* dest)
{
    _dest = dest;
}

const Node_t* RoutingProtocolPacket_t::getDestination() const
{
    return _dest;
}

// ////////////////////////////////////////////////////////////////
// Inherithed methods from interface.
// ////////////////////////////////////////////////////////////////


ProtocolPacket_if_t::size_t RoutingProtocolPacket_t::getPayloadSize() const
{
    return getInnerSize();
}

ProtocolPacket_if_t *RoutingProtocolPacket_t::getPayload() const
{
    return _payload->clone();
}

void RoutingProtocolPacket_t::setPayload(const ProtocolPacket_if_t * packet) 
{
    _payload = packet;
}

byte_t *RoutingProtocolPacket_t::getInnerBuffer() const
{
    return _payload->getInnerBuffer();
}

ProtocolPacket_if_t::size_t RoutingProtocolPacket_t::getInnerSize() const
{
    return _payload->getInnerSize();
}

RoutingProtocolPacket_t *RoutingProtocolPacket_t::clone() const
{
    auto copy = new RoutingProtocolPacket_t(*this);
    return copy;
}

bool RoutingProtocolPacket_t::operator==(const ProtocolPacket_if_t &p) const
{
    auto cast_packet = dynamic_cast <const RoutingProtocolPacket_t *> (&p);
    if (  cast_packet == nullptr) {
        return false;
    }
    if (_payload != cast_packet->_payload ||
        _dest != cast_packet->_dest)
    {
        return false;
    }
    return true;
}

