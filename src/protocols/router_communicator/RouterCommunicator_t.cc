// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
///


#include "scnsl/protocols/router_communicator//RouterCommunicator_t.hh"
#include "scnsl/protocols/router_communicator/RoutingProtocolPacket_t.hh"

using namespace Scnsl::Protocols::RouterCommunicator;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////


RouterCommunicator_t::RouterCommunicator_t(
    const RoutingTable_t & routingTable, Scnsl::Core::Node_t * rn ):
    // Parents:
    Scnsl::Core::Communicator_if_t(),
    // Fields:
    _routerNode( rn ),
    _routingTable( routingTable ),
    _is_sending( false )
{
    // Nothing to do.
}



RouterCommunicator_t::~RouterCommunicator_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////



RouterCommunicator_t::errorcode_t RouterCommunicator_t::send( const Scnsl::Core::Packet_t & p)

{

    RoutingProtocolPacket_t* rp = dynamic_cast<RoutingProtocolPacket_t*> ( p.getPayload());
    if ( rp == nullptr)
    {
        //packet from sender=> wrap to routing packet
        rp = new RoutingProtocolPacket_t();
        rp->setPayload(p.getPayload());
        rp->setDestination(p.getDestinationNode());
    }

    if ( _is_sending ) return static_cast<errorcode_t>(-1);
    _is_sending = true;

	RoutingTable_t::iterator i = _routingTable.find(
            const_cast< Scnsl::Core::Node_t * >( rp->getDestination() ));
    if ( i == _routingTable.end() )
		throw std::logic_error( "Error: node not found in routingTable" );

	std::pair <Scnsl::Core::Channel_if_t * ,Scnsl::Core::Node_t * > values = i->second;

	Scnsl::Core::Channel_if_t * channel = values.first;
	Scnsl::Core::Node_t * destinationNode = values.second;

    if (( channel == nullptr) || ( destinationNode == nullptr ))
    {
    	throw std::logic_error( "Error: associated channel or destinationNode not found in the routing table" );
    }

    Scnsl::Core::Packet_t pRouter(p);
    pRouter.setChannel(channel);
    pRouter.setDestinationNode(destinationNode);
    pRouter.setPayload(*rp);

    errorcode_t ret = Communicator_if_t::send(pRouter);
    _is_sending = false;
    return ret;
}


RouterCommunicator_t::errorcode_t RouterCommunicator_t::receive( const Scnsl::Core::Packet_t & p)

{
	auto  pRouter = dynamic_cast<RoutingProtocolPacket_t*> (p.getPayload());
    if (pRouter == nullptr)
        return -1;
	if(pRouter->getDestination() == _routerNode)
	{
        //unwrap
        auto wrapped_packet = pRouter->getPayload();
        Scnsl::Core::Packet_t packet (p);
        packet.setPayload(*wrapped_packet);
		return Communicator_if_t::receive(packet);
	}
	else
	{
		return this->send(p);
	}

}
