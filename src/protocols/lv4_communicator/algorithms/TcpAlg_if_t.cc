#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_if_t.hh"

#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"

using Scnsl::Protocols::Network_Lv4::TcpAlg_if_t;

TcpAlg_if_t::TcpAlg_if_t(TcpConnection_if_t * conn)
{
    _conn = conn;
}

TcpAlg_if_t::~TcpAlg_if_t()
{}
