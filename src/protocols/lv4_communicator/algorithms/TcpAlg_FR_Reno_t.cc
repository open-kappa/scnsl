#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_FR_Reno_t.hh"

#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"

using namespace Scnsl::Protocols::Network_Lv4;

TcpAlg_FR_Reno_t::TcpAlg_FR_Reno_t(TcpConnection_if_t * conn): TcpAlg_if_t(conn)
{}

TcpAlg_FR_Reno_t::~TcpAlg_FR_Reno_t()
{}

void TcpAlg_FR_Reno_t::EFSM(
    bool timeout_occurred,
    bool is_last_ack_duplicate,
    size_t acknowledge_bytes)
{
    auto sstresh = _conn->getSstresh();
    auto cwnd = _conn->getCwnd();
    auto max_seg_size = _conn->getMaxSegSize();
    auto rcvwnd = _conn->getRcvwnd();

    if (!timeout_occurred)  // no timeout, proceed with algorithm
    {
        if (is_last_ack_duplicate)
        {
            // Increase cwnd by segment size for each duplicate ack received
            _conn->setCwnd(std::min(rcvwnd, cwnd + max_seg_size));
        }
        else
        {
            // When no more duplicate ack, adjust the cwnd and restart from
            // congestion avoidance
            _conn->setCwnd(sstresh);
            _conn->setCongestionState(CONGESTION_AVOIDANCE);
        }
    }
    else
    {
        // timeout occurred => restart from slow start and retransmit lost
        // segment
        _conn->toSlowStart();
    }
}

void TcpAlg_FR_Reno_t::initAlg()
{
    size_t sstresh;
    auto flsz = _conn->getFlightSize();
    auto max_seg_size = _conn->getMaxSegSize();

    // computing new values for variables
    sstresh = std::max(flsz / 2, 2 * max_seg_size);
    _conn->setSstresh(sstresh);
    _conn->setCwnd(sstresh + 3 * max_seg_size);
    _conn->setRetransmission(true);
    // disable limited mode (fast recovery activate with 3 duplicate ack
    _conn->setLimitedTransmissionEnabled(false);
}