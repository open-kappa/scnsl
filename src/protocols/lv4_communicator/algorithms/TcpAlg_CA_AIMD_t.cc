#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_CA_AIMD_t.hh"

#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"

using namespace Scnsl::Protocols::Network_Lv4;

TcpAlg_CA_AIMD_t::TcpAlg_CA_AIMD_t(TcpConnection_if_t * conn): TcpAlg_if_t(conn)
{}

TcpAlg_CA_AIMD_t::~TcpAlg_CA_AIMD_t()
{}

void TcpAlg_CA_AIMD_t::EFSM(bool timeout_occurred, bool is_last_ack_duplicate, 
                            size_t acknowledge_bytes)
{
    auto cwnd = _conn->getCwnd();
    auto max_seg_size = _conn->getMaxSegSize();
    auto rcvwnd = _conn->getRcvwnd();

    if (!timeout_occurred)  // no timeout, proceed with algorithm
    {
        if ((_conn->getDuplicateAck() == 1 || _conn->getDuplicateAck() == 2)
            && is_last_ack_duplicate)
        {
            // 1 or 2 duplicate ack in sequence => activate the limited mode to
            // allow more data to be send and to trigger more ack
            _conn->setLimitedTransmissionEnabled(true);
        }
        else
        {
            if (_conn->getDuplicateAck() == 3 && is_last_ack_duplicate)
            {
                // 3 duplicate ack in a row ==> fast recovery algorithm
                _conn->toFastRecovery();
            }
            else
            {
                // default behaviour, increase the cwnd by segment size when the
                // total bytes acknowledged surpass the cwnd value
                if (_conn->getAckSizeCounter() >= cwnd)
                {
                    cwnd = cwnd + max_seg_size;
                    _conn->setAckSizeCounter(0);
                }
                _conn->setCwnd(std::min(rcvwnd, cwnd));
            }
            // disable limited mode (even if it is already off)
            _conn->setLimitedTransmissionEnabled(false);
        }
    }
    else
    {
        // timeout occurred => restart from slow start and retransmit lost
        // segment
        _conn->toSlowStart();
    }
}

void TcpAlg_CA_AIMD_t::initAlg()
{
    // no operation
}