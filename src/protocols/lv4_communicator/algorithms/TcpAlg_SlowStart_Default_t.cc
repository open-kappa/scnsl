#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_SlowStart_Default_t.hh"

#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"

using namespace Scnsl::Protocols::Network_Lv4;

TcpAlg_SlowStart_Default_t::TcpAlg_SlowStart_Default_t(TcpConnection_if_t * conn):
TcpAlg_if_t(conn)
{}

TcpAlg_SlowStart_Default_t::~TcpAlg_SlowStart_Default_t()
{}

void TcpAlg_SlowStart_Default_t::EFSM(
    bool timeout_occurred,
    bool is_last_ack_duplicate,
    size_t acknowledge_bytes)
{
    auto sstresh = _conn->getSstresh();
    auto cwnd = _conn->getCwnd();

    if (!timeout_occurred)  // no timeout, proceed with algorithm
    {
        if ((_conn->getDuplicateAck() == 1 || _conn->getDuplicateAck() == 2)
            && is_last_ack_duplicate)
        {
            // 1 or 2 duplicate ack in sequence => activate the limited mode to
            // allow more data to be send and to trigger more ack
            // enable send over the cwnd (max 2 segments)
            _conn->setLimitedTransmissionEnabled(true);  
        }
        else
        {
            if (_conn->getDuplicateAck() == 3 && is_last_ack_duplicate)
            {
                // 3 duplicate ack in a row ==> fast recovery algorithm
                _conn->toFastRecovery();
            }
            else
            {
                // each ack increase cwnd by the number of bytes acknowledged,
                // max is ssthresh
                _conn->setCwnd(std::min(sstresh, cwnd + acknowledge_bytes));
                if (cwnd >= sstresh)  // change state if the treshold is reached
                    _conn->setCongestionState(CONGESTION_AVOIDANCE);
            }
            // disable limited mode (even if it is already off)
            _conn->setLimitedTransmissionEnabled(false);
        }
    }
    else
    {
        // timeout occurred => restart from slow start and retransmit lost
        // segment
        _conn->toSlowStart();
    }
}

void TcpAlg_SlowStart_Default_t::initAlg()
{
    auto flsz = _conn->getFlightSize();
    auto max_seg_size = _conn->getMaxSegSize();

    _conn->setSstresh(std::max(flsz / 2, 2 * max_seg_size));
    _conn->setFlightSize(0);
    _conn->setCwnd(max_seg_size);
    _conn->setDuplicateAck(0);
    _conn->setRetransmission(true);
    _conn->setLimitedTransmissionEnabled(false);
}
