#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlgFactory.hh"

using namespace Scnsl::Protocols::Network_Lv4;

TcpAlg_if_t * TcpAlgFactory::getAlgorithm(TcpAlgs alg, TcpConnection_if_t * conn)
{
    switch (alg)
    {
        case DEFAULT_SLOW_START: return new TcpAlg_SlowStart_Default_t(conn);
        case AIMD_CA: return new TcpAlg_CA_AIMD_t(conn);
        case RENO: return new TcpAlg_FR_Reno_t(conn);
        default: throw std::runtime_error("Unknown algorithm required");
    }
}