#include "scnsl/protocols/lv4_communicator/EventManager_t.hh"

using Scnsl::Protocols::Network_Lv4::EventManager_t;
using namespace Scnsl::Protocols::Network_Lv4;

EventManager_t::EventManager_t(): _default_event(), _events()
{

}

EventManager_t::~EventManager_t()
{
    
}

void EventManager_t::notify(TimeoutStruct_t ev)
{
    if (_events.empty() ||  // no events in the queue
        ev._timestamp < _events.top()._timestamp)  
    {
        _default_event.notify(ev._timestamp - sc_core::sc_time_stamp());
    }
    _events.push(ev);
}

TimeoutStruct_t EventManager_t::getTop()
{
    auto top = _events.top();
    _events.pop();
    return top;
}

bool EventManager_t::isTopFired()
{
    // true if the element at the top has timestamp of activation <= at current
    // timestamp
    return (!_events.empty())
        && _events.top()._timestamp <= sc_core::sc_time_stamp();
}

void EventManager_t::fireNext()
{
    // fire next event by calling a notify with argument the difference between
    // the event
    // timestamp and the current timestamp
    if (!_events.empty())
    {
        _default_event.notify(
            _events.top()._timestamp - sc_core::sc_time_stamp());
    }
}

sc_core::sc_event & EventManager_t::defaultEvent()
{
    return _default_event;
}

bool EventManager_t::empty()
{
    return _events.empty();
}