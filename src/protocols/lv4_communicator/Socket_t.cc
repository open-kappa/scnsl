#include "scnsl/protocols/lv4_communicator/Socket_t.hh"

#include "scnsl/core/data_types.hh"
#include "scnsl/protocols/lv4_communicator/Lv4ProtocolConstants.hh"

#include <sstream>
using Scnsl::Protocols::Network_Lv4::Socket_t;
using namespace Scnsl::Protocols::Network_Lv4;

Socket_t::Socket_t(socketFD_t fd, int type, sc_core::sc_event * select_wake):
_fd(fd),
_sock_type(type),
_source_ip(0),
_source_port_no(0),
_dest_ip(0),
_dest_port_no(0),
_reuseport(false),
_reuseaddr(false),
_taskproxy_key("-1"),
_recv_queue(new std::vector<byte_t>),
_connection_queue(nullptr),
_send_buffer_free_space(DEFAULT_WMEM),
_blocking(true),
_bound(false),
_type(NOT_SET),
_which_set(NONE_SET),
_select_wake(select_wake),
_income_msg(),
_connection_request(),
_closed(false),
_W_closed(false),
_R_closed(false),
_multicast_subscriptions()
{

}

Socket_t::Socket_t(socketFD_t fd, int type, Scnsl::Core::socket_properties_t sp,
                            sc_core::sc_event * select_wake):
_fd(fd),
_sock_type(type),
_dest_ip(sp.dest_ip),
_source_ip(sp.source_ip),
_source_port_no(sp.source_port),
_dest_port_no(sp.dest_port),
_reuseport(false),
_reuseaddr(false),
_taskproxy_key("-1"),
_recv_queue(new std::vector<byte_t>),
_connection_queue(nullptr),
_send_buffer_free_space(DEFAULT_WMEM),
_blocking(true),
_bound(false),
_type(NOT_SET),
_which_set(NONE_SET),
_select_wake(select_wake),
_income_msg(),
_connection_request(),
_closed(false),
_R_closed(false),
_W_closed(false),
_multicast_subscriptions()
{

}

Socket_t::~Socket_t()
{
    delete _recv_queue;
    delete _connection_queue;
}

void Socket_t::create_queue(unsigned length)
{
    _connection_queue = new std::vector<Socket_t *>;
    _connection_queue->reserve(length);
}

bool Socket_t::is_queue_empty()
{
    return _connection_queue->empty();
}

Socket_t * Socket_t::getConnection()
{
    auto s = _connection_queue->front();
    _connection_queue->erase(_connection_queue->begin());
    return s;
}

void Socket_t::pushConnectionRequest(Socket_t * s)
{
    _connection_queue->push_back(s);
    _connection_request.notify(sc_core::SC_ZERO_TIME);
    if (_which_set == READ_SET)  // the socket is in a read set of a select
    {
        _select_wake->notify(sc_core::SC_ZERO_TIME);
    }
}

void Socket_t::notifyConnectionRequest()
{
    _connection_request.notify(sc_core::SC_ZERO_TIME);
}

/////////
///  receive queue management
/////////

int Socket_t::moveDataToBuffer(byte_t * buffer, int length)
{
    unsigned long min_val = std::min( (unsigned long)length, _recv_queue->size());
    std::copy(_recv_queue->begin(), _recv_queue->begin() + min_val,buffer);
    _recv_queue->erase(_recv_queue->begin(), _recv_queue->begin() + min_val);
    return min_val;
}

void Socket_t::moveDataFromBuffer(byte_t * buffer, int length)
{
    _recv_queue->insert(_recv_queue->end(), buffer, buffer + length);
    _income_msg.notify(sc_core::SC_ZERO_TIME);
    if (_which_set == READ_SET)
    {
        _select_wake->notify(sc_core::SC_ZERO_TIME);
    }
}

messageInfo Socket_t::moveMessageToBuffer(byte_t* buffer, int length)
{

    //take minimum value between first message size and required length
    unsigned long min_val = std::min( (unsigned long)length, 
                                        _msg_info_queue.front().size);
    std::copy(_recv_queue->begin(), _recv_queue->begin() + min_val,buffer);
    //remove whole message from queue
    _recv_queue->erase(_recv_queue->begin(), 
                        _recv_queue->begin() + _msg_info_queue.front().size);
    messageInfo ret = _msg_info_queue.front(); //remove info on first element
    _msg_info_queue.pop();
    ret.size = min_val;
    return ret;

}

void Socket_t::moveMessageFromBuffer(byte_t* buffer, int length, socket_properties_t sp)
{
    //copy data inside buffer
    _recv_queue->insert(_recv_queue->end(), buffer, buffer + length);
    messageInfo new_message;
    new_message.size = length;
    new_message.src_ip = sp.dest_ip;
    new_message.src_port =  sp.dest_port;
    _msg_info_queue.push(new_message);
    _income_msg.notify(sc_core::SC_ZERO_TIME);
    if (_which_set == READ_SET)
    {
        _select_wake->notify(sc_core::SC_ZERO_TIME);
    }
}

bool Socket_t::isRecvQueueEmpty()
{
    return _recv_queue->empty();
}

///////////////////////
// Setters + Getters
///////////////////////

socketFD_t Socket_t::getFD() const
{
    return _fd;
}

void Socket_t::setSourceIp(const ip_addr_t & sourceIp)
{
    _source_ip = sourceIp;
}

const ip_addr_t & Socket_t::getSourceIp() const
{
    return _source_ip;
}

void Socket_t::setSourcePortNo(port_no_t sourcePortNo)
{
    _source_port_no = sourcePortNo;
}

port_no_t Socket_t::getSourcePortNo() const
{
    return _source_port_no;
}

void Socket_t::setDestIp(const ip_addr_t & destIp)
{
    _dest_ip = destIp;
}

const ip_addr_t & Socket_t::getDestIp() const
{
    return _dest_ip;
}

void Socket_t::setDestPortNo(port_no_t destPortNo)
{
    _dest_port_no = destPortNo;
}

port_no_t Socket_t::getDestPortNo() const
{
    return _dest_port_no;
}

void Socket_t::setType(open_type type)
{
    _type = type;
}

open_type Socket_t::getType() const
{
    return _type;
}

int Socket_t::getProtocol() const
{
    return _sock_type;
}


void Socket_t::setBlock(bool blocking)
{
    _blocking = blocking;
}

bool Socket_t::isBlocking() const
{
    return _blocking;
}

void Socket_t::setTaskproxyKey(const std::string & taskproxyKey)
{
    _taskproxy_key = taskproxyKey;
}

const std::string & Socket_t::getTaskproxyKey() const
{
    return _taskproxy_key;
}

void Socket_t::setBound(bool bound)
{
    _bound = bound;
}

bool Socket_t::isBound() const
{
    return _bound;
}

bool Socket_t::isClosed()
{
    return _closed;
}

void Socket_t::close()
{
    _closed = true;
}

bool Socket_t::isRClosed()
{
    return _R_closed;
}

void Socket_t::setRClosed()
{
    _R_closed = true;
}

bool Socket_t::isWClosed()
{
    return _W_closed;
}

void Socket_t::setWClosed()
{
    _W_closed = true;
}

bool Socket_t::hasSpaceToWrite() const
{
    return _send_buffer_free_space > NO_WRITE_TRESHOLD;
}

size_t Socket_t::getSpaceToWrite() const
{
    return _send_buffer_free_space;
}

void Socket_t::setWhichSet(unsigned short whichSet)
{
    _which_set = whichSet;
}

void Socket_t::setSndBufferSize(size_t dim)
{
    _send_buffer_free_space = dim;
    if (_which_set == WRITE_SET && _send_buffer_free_space > NO_WRITE_TRESHOLD)
    {
        // socket is in a write set of a select
        _select_wake->notify(sc_core::SC_ZERO_TIME);
    }
}

bool Socket_t::isReuseport() const
{
    return _reuseport;
}

void Socket_t::setReuseport(bool reuseport)
{
    _reuseport = reuseport;
}

bool Socket_t::isReuseaddr() const
{
    return _reuseaddr;
}

void Socket_t::setReuseaddr(bool reuseaddr)
{
    _reuseaddr = reuseaddr;
}

const sc_core::sc_event & Socket_t::getIncomeMsg() const
{
    return _income_msg;
}

const sc_core::sc_event & Socket_t::getConnectionRequest() const
{
    return _connection_request;
}

void Socket_t::addMulticastIP(unsigned ip)
{
    _multicast_subscriptions.insert(ip);
}

bool Socket_t::hasMulticastMembership(unsigned ip)
{
    return _multicast_subscriptions.find(ip) != _multicast_subscriptions.end();
}

/////////////////////////////////
// Comparison operator
////////////////////////////////
Socket_t & Socket_t::operator=(const Socket_t & sp)
{
    if (this == &sp) return *this;
    _source_ip = sp._source_ip;
    _source_port_no = sp._source_port_no;
    _dest_ip = sp._dest_ip;
    _dest_port_no = sp._dest_port_no;
    return *this;
}

bool Socket_t::operator==(const Socket_t & s) const
{
    return _source_ip == s._source_ip &&  // no check for id
        _source_port_no == s._source_port_no && 
        _dest_ip == s._dest_ip &&
        _dest_port_no == s._dest_port_no;
}

bool Socket_t::operator<(const Socket_t & s) const
{
    if (_source_ip == s._source_ip)
    {
        if (_source_port_no == s._source_port_no)
        {
            if (_dest_port_no == s._dest_port_no) 
                return _dest_ip < s._dest_ip;
            return _dest_port_no < s._dest_port_no;
        }
        return _source_port_no < s._source_port_no;
    }
    return _source_ip < s._source_ip;
}

bool Socket_t::operator!=(const Socket_t & rhs) const
{
    return !(rhs == *this);
}
