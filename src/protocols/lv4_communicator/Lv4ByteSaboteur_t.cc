#include "scnsl/protocols/lv4_communicator/Lv4ByteSaboteur_t.hh"

#include <sstream>

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;

Lv4ByteSaboteur_t::Lv4ByteSaboteur_t( sc_core::sc_module_name name, 
                                        const bool slow_packet,
                                        const bool delete_pckt,
                                        const double error_perc,
                                        const bool multiple_downs, 
                                        const std::queue<Lv4Saboteur_channel_down_infos> timeouts,
                                        unsigned min_delay,
                                        unsigned max_delay):
sc_core::sc_module(name),
Scnsl::Tracing::Traceable_base_t(name),
_slow_down_packet_value(-1),
_delayed_packets(),
_delete_packet_value(-1),
_receive_delayed_packet(),
_operation_number(slow_packet + delete_pckt),
_error_perc(error_perc),
_gen(std::chrono::system_clock::now().time_since_epoch().count()),
_dist(0.0, 1.0),
_timeouts(timeouts),
_multiple_downs(multiple_downs),
_channel_down(false),
_go_down(),
_avoid_double_delete(false),
_last_seqnumb(0),
_min_delay(min_delay),  // arbitrary choice, can change in future
_max_delay(max_delay)
{
    short int start = 0;
    if (slow_packet)
    {
        _slow_down_packet_value = start++;
        SC_THREAD(delayPacket);
    }
    if (delete_pckt)
    {
        _delete_packet_value = start;
    }
    if (_multiple_downs)
    {
        SC_THREAD(timeout)
        _go_down.notify(_timeouts.front().start_time);
    }
}

Lv4ByteSaboteur_t::~Lv4ByteSaboteur_t()
{

}

Scnsl::Core::errorcode_t Lv4ByteSaboteur_t::send(const Packet_t & p)
{
    if (_channel_down) return 1;
    return Communicator_if_t::send(p);
}

Scnsl::Core::errorcode_t Lv4ByteSaboteur_t::receive(const Packet_t & p)
{

    std::stringstream sstr;
    auto packet = dynamic_cast<LV4ProtocolPacket_t *>(p.getPayload());
    counter_t seqn;


    if (packet->getProtocolType() == TCP)
    {
        if ((packet->getInnerBuffer()[TCP_FLAGS_BYTE] & 0b10) ||
            (packet->getInnerBuffer()[TCP_FLAGS_BYTE] & 0b1))  // syn or fin flags set
        {
            SCNSL_TRACE_LOG(1, " IGNORING PACKET WITH FIN OR SYN FLAG");
            return Communicator_if_t::receive(p);
        }

            seqn = TcpByteConnection::vectToUInt(packet->getInnerBuffer(),
                                                    TCP_SEQ_NUMB_BYTE);
        sstr << "Received packet with seqnumber: " << seqn << " :";
    }
    else seqn = _last_seqnumb; // no update for UDP
    if (_channel_down)  // simulating channel loss-> all packet are discarded
    {
        sstr << " Dropping due to channel down error";
        SCNSL_TRACE_LOG(1, sstr.str().c_str());
        _last_seqnumb = std::max(_last_seqnumb, seqn);
        return 0;
    }

    // check if error occurs
    if (packet->getProtocolType() == TCP 
        &&_avoid_double_delete && seqn <= _last_seqnumb)  // no deleting retransmitted packets
    {
        sstr << " ignoring to avoid double loss";
        SCNSL_TRACE_LOG(1, sstr.str().c_str());
        return Communicator_if_t::receive(p);
    }

    if (_operation_number < 1)  // no errors
    {
        sstr << "Receiving";
        SCNSL_TRACE_LOG(1, sstr.str().c_str());
        _last_seqnumb = std::max(_last_seqnumb, seqn);
        return Communicator_if_t::receive(p);
    }

    // checking percentage of single packet delay or loss
    double prob = _dist(_gen);

    if (prob > _error_perc)
    {
        // random number > error percentage => no error
        sstr << "Passing";
        SCNSL_TRACE_LOG(1, sstr.str().c_str());
        _last_seqnumb = std::max(_last_seqnumb, seqn);
        return Communicator_if_t::receive(p);
    }

    // error occurs
    short int operation = rand() % _operation_number;

    if (operation == _slow_down_packet_value)
    {
        sstr << "ERROR, delaying packet";
        // keep packet for a random time period
        _delayed_packets.push(p);
        // wait between 400 - 800 ms (no too much, to avoid timeout)
        sc_core::sc_time retransmit_time(rand() % (_max_delay - _min_delay + 1) + _min_delay, sc_core::SC_MS);
        _receive_delayed_packet.notify(retransmit_time);
    }
    
    else
        sstr << "ERROR, deleting packet";
    SCNSL_TRACE_LOG(1, sstr.str().c_str());
    // if error is packet loss, just return without calling the receive.
    _last_seqnumb = std::max(_last_seqnumb, seqn);
    return 0;
}

void Lv4ByteSaboteur_t::avoid_delete_retransmitted_packets(bool val)
{
    _avoid_double_delete = val;
}

void Lv4ByteSaboteur_t::setMinDelay(double minDelay)
{
    _min_delay = minDelay;
}

void Lv4ByteSaboteur_t::setMaxDelay(double maxDelay)
{
    _max_delay = maxDelay;
}

//////////////////////
// PRIVATE FUNCTIONS
//////////////////////

void Lv4ByteSaboteur_t::timeout()
{
    while (1)
    {
        wait(_go_down.default_event());
        SCNSL_TRACE_LOG(2, " SABOTEUR: Channel going down");
        _channel_down = true;
        wait(_timeouts.front().down_time);
        SCNSL_TRACE_LOG(2, " Channel up");
        _channel_down = false;
        _timeouts.pop();
        if (_timeouts.empty()) break;
        _go_down.notify(_timeouts.front().start_time);
    }
}

void Lv4ByteSaboteur_t::delayPacket()
{
    SCNSL_TRACE_LOG(2, "DELAYER started");
    while (1)
    {
        wait(_receive_delayed_packet.default_event());
        SCNSL_TRACE_LOG(1, "DELAYER: ACTIVATING");
        if (!_delayed_packets.empty())
        {
            auto p = _delayed_packets.front();
            _delayed_packets.pop();
            Communicator_if_t::receive(p);
        }
    }
}
