#include "scnsl/protocols/lv4_communicator/NetworkAPI_Task_if_t.hh"

#include "scnsl/core/SocketMap.hh"

#include <sstream>


using namespace Scnsl::Protocols::Network_Lv4;

socketFD_t NetworkAPI_Task_if_t::_next_fd = 0;

using Scnsl::Core::SocketMap;

std::map<socketFD_t, NetworkAPI_Task_if_t *> * 
    NetworkAPI_Task_if_t::_fdMap = new std::map<socketFD_t, NetworkAPI_Task_if_t *>();

NetworkAPI_Task_if_t::NetworkAPI_Task_if_t(sc_core::sc_module_name name, const task_id_t id,
                                    Scnsl::Core::Node_t * n, const size_t proxies,
                                    size_t wmeme_size):
Scnsl::Tlm::TlmTask_if_t(name, id, n, proxies),
_id_socket_map(new std::map<const socketFD_t, Socket_t *>),
_bound_sockets(new std::map<source_pair_t, Socket_t *>()),
_listenmap(nullptr),
_tp_socket_map(new std::map<Scnsl::Core::TaskProxy_if_t *, Socket_t *>),
_select_awake(),
_errno(EAGAIN),
_deleteQueue(),
_start_time(),
_deleteEventQueue()
{
    SC_THREAD(main);  // lauching the main by executing it as a thread

    SC_THREAD(deleteSocket);  // timed delete of a closed socket
}

NetworkAPI_Task_if_t::~NetworkAPI_Task_if_t()
{
    // delete all the created objects
    delete _id_socket_map;
    delete _tp_socket_map;
    delete _listenmap;
}

////////////////////////////////////////////////
// NON-STATIC API
////////////////////////////////////////////////

socketFD_t NetworkAPI_Task_if_t::internal_socket(int domain, int type,int protocol)
{
    // create new socket if the flags are correct
    Socket_t * s = new Socket_t(_next_fd, type, &_select_awake);
    _next_fd++;
    (*_id_socket_map)[s->getFD()] = s;  // add the socket to the map
    (*_fdMap)[s->getFD()] = this;
    return s->getFD();  // return file descriptor
}

int NetworkAPI_Task_if_t::internal_fcntl(socketFD_t sfd, int opt, int val)
{
    if (_id_socket_map->find(sfd) == _id_socket_map->end())
    {
        std::cerr<<name()<<" no socket found"<<std::endl;
        return -1;
    }   
    if ((*_id_socket_map)[sfd]->getProtocol() == SOCK_DGRAM )
            return -1;
    if (opt == F_GETFL) return 0;
    // if option used are correct, change socket to nonblocking mode
    if (opt == F_SETFL && (val & O_NONBLOCK))
    {
        (*_id_socket_map)[sfd]->setBlock(false);
        return 0;
    }
    _errno = EAGAIN;
    return -1;
}

int NetworkAPI_Task_if_t::internal_bind(socketFD_t sfd, const sockaddr * socket_addr, int addrlen)
{
    if (addrlen < 1)
    {
        std::cerr << name() << " Wrong addr length" << std::endl;
        return -1;
    }

    if (_id_socket_map->find(sfd) == _id_socket_map->end())
    {
        std::cerr << name() << " File descriptor not existing" << std::endl;
        return -1;
    }

    //get the socket (if exists) from the map searching by fd
    auto s = (*_id_socket_map)[sfd];  
    if (s->isBound())  
    {
        //socket already bound or another socket is bound to same ip
        std::cerr << name() << " Socket already bound" << std::endl;
        return -1;
    }
    // bound the socket ip and port to the ones in the struct
    if (socket_addr == nullptr) 
        throw  std::invalid_argument("BIND: sockaddr is nullptr");


    source_pair_t sources(socket_addr->sin_addr.s_addr, socket_addr->sin_port);
    if (_bound_sockets->find(sources) != _bound_sockets->end() &&
        !(*_bound_sockets)[sources]->isReuseport())  
        // already exist a connected socket with same source pair AND cannot
        // reuse addr/port
        return -1;
    
    /**
     * Connect a socket to one or more TPs based on IP and port
    */
    Scnsl::Core::socket_properties_t sp;
    for (auto p = _proxies.begin(); p != _proxies.end(); p++)
    {
        

        sp = SocketMap::getSocket(p->second);
        source_pair_t sources(sp.source_ip, sp.source_port);

        if ( (socket_addr->sin_addr.s_addr == INADDR_ANY || 
              sp.source_ip == socket_addr->sin_addr.s_addr)
            && sp.source_port == socket_addr->sin_port)
        {
            // std::cerr<<name()<<" Binding tp "<<p->second<<"  with IP "<<sp.source_ip
            //         <<" port "<<sp.source_port<<" to socket "<<sfd<<std::endl;
            (*_tp_socket_map)[p->second] = s;
            (*_bound_sockets)[source_pair_t(sp.source_ip , sp.source_port)] = s;
        }
    }
    s->setSourceIp(socket_addr->sin_addr.s_addr);
    s->setSourcePortNo(socket_addr->sin_port);
    s->setBound(true);
    return 0;
}

int NetworkAPI_Task_if_t::internal_listen(socketFD_t sfd, int backlog)
{
    if ((*_id_socket_map)[sfd]->getProtocol() == SOCK_DGRAM ) //no UDP
            return -1;
    if (_id_socket_map->find(sfd) == _id_socket_map->end())
    {
        std::cerr << "NO FD" << std::endl;
        return -1;  // return error if the socket doesn't exits
    }

    auto s = (*_id_socket_map)[sfd];  // get the socket
    s->setType(PASSIVE_OPEN);  // passive open == server side
    s->create_queue(backlog);  // create queue for incoming connection with
                               // given length

    if (_listenmap == nullptr)
        _listenmap = new std::map<const socketFD_t, Socket_t *>;
    (*_listenmap)[sfd] = s;
    //remove socket from main map and move to internal_listen map.
    // the socket will be used to internal_accept connection and not to
    // read/write
    _id_socket_map->erase(sfd); 
    return 0;
}

socketFD_t NetworkAPI_Task_if_t::internal_accept(socketFD_t sfd, sockaddr * addr, 
                                                socklen_t * addrlLen)
{
    auto s = (*_listenmap)[sfd];

    if (s == nullptr) 
        throw std::runtime_error("No socket listening!");

    if (! s->isBound())
    {
        std::cerr<<"Listening on an unbound socket is not correct, returning error"
                <<std::endl;
        return -1;
    }

    if (s->getProtocol() == SOCK_DGRAM ) //no UDP
            return -1;

    if (s->is_queue_empty())
    {
        if (!s->isBlocking())  // nonblocking call -> exit
        {
            _errno = EWOULDBLOCK;
            return -1;
        }
        wait(s->getConnectionRequest());  // wait for incoming connection
    }

    auto new_socket = s->getConnection();
    //add socket to the map 
    (*_id_socket_map)[new_socket->getFD()] = new_socket; 
    // change the struct values to match the values of the new socket
    if (addr)
    {
        addr->sin_addr.s_addr = new_socket->getSourceIp();
        addr->sin_port = new_socket->getSourcePortNo();
    }

    //update map of taskproxies
    (*_tp_socket_map)[_proxies[new_socket->getTaskproxyKey()]] = new_socket; 
    (*_fdMap)[new_socket->getFD()] = this;
    new_socket->setBound(true);

    return new_socket->getFD();
}

int NetworkAPI_Task_if_t::internal_connect( socketFD_t sfd, const sockaddr * addr,int addrlen)
{
    std::cerr << name() << " connecting to " << addr->sin_addr.s_addr << " ("
              << SocketMap::getIP(addr->sin_addr.s_addr) << " "
              << addr->sin_port << ")" << std::endl;
    if (_id_socket_map->find(sfd) == _id_socket_map->end())
    {
        std::cerr << name() << " no socket" << std::endl;
        return -1;  // return error if the socket doesn't exits
    }
    if ((*_id_socket_map)[sfd]->getProtocol() == SOCK_DGRAM ) //no UDP
            return -1;

    auto s = (*_id_socket_map)[sfd];
    
    //a connect has already been called for the socket
    if (s->getType() == ACTIVE_OPEN)
    {
        std::cerr << name() << " Connect already called" << std::endl;
        return -1;  
    }

    std::string tp;  // Taskproxy to use

    Scnsl::Core::socket_properties_t sp;
    bool exit = false;
    for (auto p = _proxies.begin(); p != _proxies.end() && !exit; p++)
    {
        sp = SocketMap::getSocket(p->second);
        if (sp.dest_ip != addr->sin_addr.s_addr ||
            sp.dest_port != addr->sin_port)
            continue; //wrong destination

        std::pair<ip_addr_t,port_no_t> sources (sp.source_ip,sp.source_port);
        //found a feasible address to send

        if (_tp_socket_map->find(p->second) != _tp_socket_map->end() 
            && _tp_socket_map->at(p->second) != s)
            continue; //taskproxy bond to another socket->skip this one
        
        else if (s->isBound() && _tp_socket_map->find(p->second)==_tp_socket_map->end())
            continue; //socket bound but tp not bound -> wrong tp
        
        else if (s->isBound() && _tp_socket_map->at(p->second)==s)
        {
            //socket bound with this tp
            s->setDestPortNo(sp.dest_port);
            s->setDestIp(sp.dest_ip);
            s->setTaskproxyKey(p->first);
            exit = true;
        }
        else if (! s->isBound() && _tp_socket_map->find(p->second)==_tp_socket_map->end())
        {
            //socket not bound and tp not bound-> bind together
            (*_tp_socket_map)[p->second] = s;
            (*_bound_sockets)[sources] = s;
            s->setSourceIp(sp.source_ip);
            s->setSourcePortNo(sp.source_port);
            s->setBound(true);      
            s->setDestPortNo(sp.dest_port);
            s->setDestIp(sp.dest_ip);
            s->setTaskproxyKey(p->first);   
            exit = true;
        }
    
    }
    if (!exit)  // no proxy found
    {
        std::cerr <<name()<<" no proxy found" << std::endl;
        return -1;
    }

    if (!s->isBlocking())  
    {
        //if socket is non blocking, send the command now that the 
        //taskproxy is known
        byte_t command = Scnsl::Core::TCP_NOBLOCK;
        sendCommand((*_id_socket_map)[sfd]->getTaskproxyKey(), &command, 1);
    }
    // prepare command packet to start handshake
    s->setType(ACTIVE_OPEN);  // active open == client side
    byte_t command = Scnsl::Core::TCP_CONNECT;
    int l = 1;
    sendCommand(s->getTaskproxyKey(), &command, l);
    wait(s->getConnectionRequest());
    return 0;
}

int NetworkAPI_Task_if_t::internal_send (socketFD_t sfd, const void * v_buffer, int length, 
                            int flags)
{
    if (length < 1)  // wrong length
    {
        _errno = EAGAIN;
        return -1;
    }
    if (_id_socket_map->find(sfd) == _id_socket_map->end())  // no sfd found
    {
        _errno = EAGAIN;
        return -1;
    }
    if ((*_id_socket_map)[sfd]->getProtocol() == SOCK_DGRAM ) //no UDP
        return -1;

    auto s = (*_id_socket_map)[sfd];
    if (s->isClosed() || s->isWClosed())
    {
        _errno = EAGAIN;
        return -1;
    }

    if (! s->isBound())
    {
        //socket not bound, cannot send
        std::cerr<<"Error:Send from not connected socket"<<std::endl;
        return -1;
    }

    byte_t  buffer [length + 1];
    buffer[0]='T'; //add character to inform which protocol use in communicator
    memcpy(buffer+1,v_buffer,length);

    if (s->isBlocking())
    {
        send(s->getTaskproxyKey(), buffer, length+1);
        // return the number of byte written in the buffer (all since it is
        // a blocking call)
        return length;  
    }
    if ((long)s->getSpaceToWrite() == 0)  // no space to send
    {
        _errno = EWOULDBLOCK;
        std::cerr << "No space to write" << std::endl;
        return -1;
    }
    // cast to long to avoid unsigned underflow
    size_t remains = std::max( 0l, (long)s->getSpaceToWrite() - (long)length);  
    s->setSndBufferSize(remains);
    send(s->getTaskproxyKey(), buffer, length+1);
    // the number of byte written is the length of the buffer - the free space
    // (min is 0 if nothing has been write)
    return length < s->getSpaceToWrite() ? length : length - s->getSpaceToWrite();
}

int NetworkAPI_Task_if_t::internal_sendto (socketFD_t sfd, const void * v_buffer, int length,
                                            int flags, const sockaddr * dest_addr,
                                            socklen_t addrlen)
{
    if (length > MAX_UDP_SEGMENT)
        return -1;

    if (_id_socket_map->find(sfd) == _id_socket_map->end())
    {
        std::cerr<<name()<<" No socket"<<std::endl;
        return -1;
    }  
    if ((*_id_socket_map)[sfd]->getProtocol() == SOCK_STREAM) //no TCP
        return internal_send(sfd,v_buffer,length,flags); //for tcp call the send method
    
    auto s = (*_id_socket_map)[sfd];

    if (s->isClosed() || s->isWClosed())
        return -1;

    Scnsl::Core::socket_properties_t sp;
    //Search for a taskproxy free
    bool exit = false;

    //Avoid check if destiantion is already set
    if (!  (s->getDestIp() == dest_addr->sin_addr.s_addr && 
            s->getDestPortNo() == dest_addr->sin_port )) 
    {
        Scnsl::Core::socket_properties_t sp;
        bool exit = false;
        for (auto p = _proxies.begin(); p != _proxies.end() && !exit; p++)
        {
            sp = SocketMap::getSocket(p->second);
            if (sp.dest_ip != dest_addr->sin_addr.s_addr ||
                sp.dest_port != dest_addr->sin_port)
                {
                    std::cerr<<"Tp wrong dest"<<std::endl;
                    continue; //wrong destination
                }
            std::pair<ip_addr_t,port_no_t> sources (sp.source_ip,sp.source_port);
            //found a feasible address to send

            if (_tp_socket_map->find(p->second) != _tp_socket_map->end() 
                && _tp_socket_map->at(p->second) != s)
                {
                    std::cerr<<"taskproxy bond to another socket->skip this one"<<std::endl;
                    continue; //taskproxy bond to another socket->skip this one
                }
            
            else if (s->isBound() && _tp_socket_map->find(p->second)==_tp_socket_map->end())
                {
                    std::cerr<<"socket bound but tp not bound -> wrong tp"<<std::endl;
                    continue; //socket bound but tp not bound -> wrong tp
                } 
            
            else if (s->isBound() && _tp_socket_map->at(p->second)==s)
            {
                //socket bound with this tp
                s->setDestPortNo(sp.dest_port);
                s->setDestIp(sp.dest_ip);
                s->setTaskproxyKey(p->first);
                exit = true;
            }
            else if (! s->isBound() && _tp_socket_map->find(p->second)==_tp_socket_map->end())
            {
                //socket not bound and tp not bound-> bind together
                (*_tp_socket_map)[p->second] = s;
                (*_bound_sockets)[sources] = s;
                s->setSourceIp(sp.source_ip);
                s->setSourcePortNo(sp.source_port);
                s->setBound(true);      
                s->setDestPortNo(sp.dest_port);
                s->setDestIp(sp.dest_ip);
                s->setTaskproxyKey(p->first);   
                exit = true;
            }
        }
        if (!exit)  // no proxy found
        {
            std::cerr <<name()<<" no proxy found" << std::endl;
            return -1;
        }        
    }
    byte_t  buffer [length + 1];
    buffer[0]='U'; //add character to inform which protocol use in communicator
    memcpy(buffer+1,v_buffer,length);
    //ADD HERE EXTRA LETTER FOR SOURCE PROTOCOL TYPE
    send(s->getTaskproxyKey(), buffer, length+1);
    return length; //udp send all, return length    
}

int NetworkAPI_Task_if_t::internal_recv(socketFD_t sfd, void * v_buffer, int length,
                                        int flags)
{
    byte_t * buffer = (byte_t *)v_buffer;
    if (length < 1)  // wrong length
        return -1;

    if (_id_socket_map->find(sfd) == _id_socket_map->end())  // no sfd found
        return -1;

    auto s = (*_id_socket_map)[sfd];
    if (s->isRecvQueueEmpty())  
    {
        // can still receive from a closed socket if data were inserted
        // before closure
        if (s->isBlocking() && flags != MSG_DONTWAIT)
            wait(s->getIncomeMsg());
        else
        {
            _errno = EWOULDBLOCK;
            return -1;
        }
    }
    return s->moveDataToBuffer(buffer, length);  // retrieve data
}

int NetworkAPI_Task_if_t::internal_recvfrom(socketFD_t sfd, void * v_buffer, int length,
                                        int flags, sockaddr * src_addr, socklen_t * addrlen)
{
    if (length < 1)  // wrong length
        return -1;

    if (_id_socket_map->find(sfd) == _id_socket_map->end())  // no sfd found
        return -1;

    auto s = (*_id_socket_map)[sfd];
    if (! s->isBound())
        return -1; //recvfrom only for bound sockets
    if (s->isRecvQueueEmpty())  
    {
        // can still receive from a closed socket if data were inserted
        // before closure
        if (s->isBlocking())
            wait(s->getIncomeMsg());
        else
        {
            _errno = EWOULDBLOCK;
            return -1;
        }
    }
    
    // retrieve data
    messageInfo infos = s->moveMessageToBuffer((byte_t *)v_buffer, length);  
    if (src_addr != NULL)
    {

        src_addr->sin_addr.s_addr = infos.src_ip;
        src_addr->sin_port = infos.src_port;
    }
    return infos.size;
}

int NetworkAPI_Task_if_t::internal_select(int nfds, fd_set * readfds, fd_set * writefds,
                                        fd_set * exceptfds, struct timeval * timeout)
{
    bool exit = false;
    bool second_it = false;
    bool res_found = false;
    int elements = 0;
    bool read_not_null = (readfds != NULL);
    bool write_not_null = (writefds != NULL);
    bool except_not_null = (exceptfds != NULL);
    do{
        // check for readable sockets in read set
        for (int i = 0; i < nfds; i++)
        {
            if (read_not_null && readfds->count(i) == 1)  
            {
                // element with fd i in read set
                if (_listenmap != nullptr &&
                    _listenmap->find(i) != _listenmap->end())
                {
                    // fd in internal_listen map => socket waiting for
                    // connection
                    auto s = (*_listenmap)[i];
                    s->setWhichSet(READ_SET);
                    if (!s->is_queue_empty())  // connection pending in queue
                        res_found = true;
                    else if (second_it)
                        readfds->erase(i);
                    // on second iteration remove file from set, to avoid future
                    // update to wake the select if the fd is not more in the set
                    if (second_it) s->setWhichSet(NONE_SET);
                }
                else if (_id_socket_map->find(i) != _id_socket_map->end())
                {
                    // fd in map -> waiting for  messages
                    auto s = (*_id_socket_map)[i];
                    s->setWhichSet(READ_SET);
                    if (!s->isRecvQueueEmpty())  // data available for read
                        res_found = true;
                    else if (second_it)
                        readfds->erase(i);
                    // on second iteration remove file from set, to avoid future
                    // update to wake the select if the fd is not more in the set
                    if (second_it) s->setWhichSet(NONE_SET);
                }
            }
            else if (write_not_null && writefds->count(i) == 1)  
            {
                // element find in write set
                if (_id_socket_map->find(i) != _id_socket_map->end())
                {
                    auto s = (*_id_socket_map)[i];
                    s->setWhichSet(WRITE_SET);
                    if (s->hasSpaceToWrite())
                        res_found = true;
                    else if (second_it)
                        writefds->erase(i);
                    // on second iteration remove file from set, to avoid future
                    // update to wake the select if the fd is not more in the set
                    if (second_it) s->setWhichSet(NONE_SET);
                }
            }
            // no check for exception
        }
        if (!res_found && !second_it)  
        {
            // no result found on first iteration, wait for the time or
            // until an event occurs
            if (timeout == NULL)
                wait(_select_awake);  // wait for event
            else
            {
                sc_core::sc_time ts(timeout->tv_sec, sc_core::SC_SEC);
                sc_core::sc_time tus(timeout->tv_usec, sc_core::SC_US);
                sc_core::sc_time to_wait = ts + tus;
                sc_core::sc_time now = sc_core::sc_time_stamp();
                wait(to_wait, _select_awake);  // wait for timer or event
                now = sc_core::sc_time_stamp() - now;  // get time slept
                timeout->tv_sec = 0;
                // set time not slept in microseconds
                timeout->tv_usec = (to_wait - now).value() / 1000000;  
            }
            second_it = true;
        }
        else if (second_it)  // second iteration => exit in any case
            exit = true;
        else
            second_it = true;
    }
    while (!exit);
    // compute number of elements remaining
    if (except_not_null)
        exceptfds->clear();  // no fd in the exception list
    if (read_not_null)
        elements += readfds->size();
    if (write_not_null)
        elements += writefds->size();
    return elements;
}

int NetworkAPI_Task_if_t::internal_poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
    
    bool exit = false;
    bool second_it = false;
    bool res_found = false;
    bool read;
    bool write;
    int sfd;
    int res = 0;
    do
    {
        // check for readable sockets in read set
        for (int i = 0; i < nfds; i++)
        {
            read = fds[i].events & POLLIN;
            write = fds[i].events & POLLOUT;
            sfd = fds[i].fd;           

            if (sfd < 0){
                fds->revents = 0;
                continue;
            }
            if (read)  
            {
                // element with fd i in read set
                if (_listenmap != nullptr &&
                    _listenmap->find(sfd) != _listenmap->end())
                {

                    // fd in internal_listen map => socket waiting for
                    // connection
                    auto s = (*_listenmap)[sfd];
                    
                    s->setWhichSet(READ_SET);
                    if (!s->is_queue_empty())  // connection pending in queue
                        {
                            fds[i].revents =  POLLIN;
                            res_found = true;
                            res++;
                        }
                    // on second iteration remove file from set, to avoid future
                    // update to wake the select if the fd is not more in the set
                    if (second_it) s->setWhichSet(NONE_SET);
                }
                else if (_id_socket_map->find(sfd) != _id_socket_map->end())
                {
                    // fd in map -> waiting for  messages
                    auto s = (*_id_socket_map)[sfd];
                    s->setWhichSet(READ_SET);
                    if (!s->isRecvQueueEmpty())  // data available for read
                        {
                            fds[i].revents =  POLLIN;
                            res_found = true;
                            res++;
                        }
                    if (second_it) s->setWhichSet(NONE_SET);
                }

            }
            else if (write)  
            {
                // element find in write set
                if (_id_socket_map->find(sfd) != _id_socket_map->end())
                {
                    auto s = (*_id_socket_map)[sfd];
                    s->setWhichSet(WRITE_SET);
                    if (s->hasSpaceToWrite())
                    {
                        fds[i].revents = POLLOUT;
                        res_found = true;
                        res++;
                    }
                    else 
                    if (second_it) s->setWhichSet(NONE_SET);
                }
            }
            // no check for exception
        }
        if (!res_found && !second_it)  
        {
            // no result found on first iteration, wait for the time or
            // until an event occurs
            if (timeout < 0)
                wait(_select_awake);  // wait for event
            else
            {
                sc_core::sc_time ts(timeout, sc_core::SC_MS);
                wait(ts, _select_awake);  // wait for timer or event  
            }
            second_it = true;
        }
        else if (second_it)  // second iteration => exit in any case
            {
                exit = true;
            }
        else
            second_it = true;
    }
    while (!exit);
    return res;
}


int NetworkAPI_Task_if_t::internal_getsockopt(int sockfd, int level, int optname,
                                            void * optval, socklen_t * optlen)
{
    return 0;
}

int NetworkAPI_Task_if_t::internal_setsockopt( int sockfd, int level, int optname,
                                            const void * optval, socklen_t optlen)
{
    if (_id_socket_map->find(sockfd) == _id_socket_map->end())
        return -1;
    if (level == SOL_SOCKET)
    {
        if (optname == SO_REUSEADDR)
            (*_id_socket_map)[sockfd]->setReuseaddr(*(int *)(optval));
        else if (optname == SO_REUSEPORT)
            (*_id_socket_map)[sockfd]->setReuseport(*(int *)(optval));
    }
    if (level == IPPROTO_IP)
    {
        if (optname == IP_ADD_MEMBERSHIP)
        {
            (*_id_socket_map)[sockfd]->addMulticastIP(((ip_mreqn*)optval)->imr_multiaddr.s_addr);
        }
    }

    return 0;
}

int NetworkAPI_Task_if_t::internal_gethostname(char * name, size_t len)
{
    // get the ip address of the node
    auto ip = SocketMap::getNetworkInterfaces(_node);  
    std::string host = SocketMap::resolveIp(ip->front());
    memcpy(name, host.c_str(), len);
    return 0;
}

int NetworkAPI_Task_if_t::internal_getsockname(int sockfd, sockaddr * addr, socklen_t * addrlen)
{
    if (_id_socket_map->find(sockfd) == _id_socket_map->end()) 
        return -1;
    auto s = (*_id_socket_map)[sockfd];
    addr->sin_addr.s_addr = s->getSourceIp();
    addr->sin_port = s->getSourcePortNo();
    *addrlen = sizeof(addr);
    return 0;
}

int NetworkAPI_Task_if_t::internal_getpeername(int sockfd, sockaddr * addr, socklen_t * addrlen)
{
    if (_id_socket_map->find(sockfd) == _id_socket_map->end()) 
        return -1;
    auto s = (*_id_socket_map)[sockfd];
    addr->sin_addr.s_addr = s->getDestIp();
    addr->sin_port = s->getDestPortNo();
    addr->ss_family = AF_INET;
    addr->sa_family = AF_INET;
    *addrlen = sizeof(addr);
    return 0;
}

int NetworkAPI_Task_if_t::internal_getaddrinfo(const char * node, const char * service,
                                            const addrinfo * hints, addrinfo ** res)
{
    // THREE CASES:
    /// NODE IS NULL ==> RETURN IP ADDRESS OF THE NODE THE TASK IS RUNNING ON
    //  NODE IS AN IP ADDRESS => INSERT IN THE STRUCT AND RETURN
    //  NODE IS A HOSTNAME => FIND IN THE MAP
    // for now port is assumed to be a number, so it will be casted directly
    // the hints struct isn't used
    std::vector<unsigned> * ip;
    if (node == NULL)
        ip = SocketMap::getNetworkInterfaces(_node);
    else
    {
        ip = new std::vector<unsigned>();
        // need to know if node is a name or an ip
        // first search if it is a name
        std::string s(node);
        unsigned t_ip = SocketMap::dnsRequest(s);
        if (t_ip == 0)  // no name found, try ip
            if (SocketMap::getNode(SocketMap::getIP(s)) == nullptr)  // no ip
                    return -1;
            else t_ip = SocketMap::getIP(s);  // set ip found
        ip->push_back(t_ip);
    }
    addrinfo * prev = nullptr;
    std::stringstream sstr;
    for (auto i: *ip)
    {
        addrinfo * entry = new addrinfo();
        if (hints != NULL) // use the hints struct to initialize the new entry
            memcpy(entry, hints, sizeof(addrinfo));  
        else
            memset(entry, 0, sizeof(addrinfo));
        entry->ai_next = NULL;
        sstr.str("");
        if (node != NULL) 
        {
            sstr << node << ":" ;
            service != NULL ? sstr<< service :sstr<<"" ;
        }    
        else {
            sstr << i << ":" ;
            service!= NULL? sstr << service : sstr<<"";
        }   
        entry->ai_canonname = sstr.str().c_str();

        sockaddr * addr = new sockaddr();
        addr->sin_addr.s_addr = i;
        if (service) 
            addr->sin_port = std::atoi(service);
        entry->ai_addr = addr;
        entry->ai_family = AF_INET;
        entry->ai_addrlen = sizeof(&addr);
        if (prev != nullptr)
        {
            prev->ai_next = entry;
        }
        else
            *res = entry;
        prev = entry;
    }
    return 0;
}

int NetworkAPI_Task_if_t::internal_close(socketFD_t sfd)
{
    if (_id_socket_map->find(sfd) != _id_socket_map->end())
    {
        // socket used for communication (present in bind map)
        auto s = (*_id_socket_map)[sfd];
        // prepare command to start disconnection

        if (s->getProtocol()==SOCK_STREAM)
        {
            byte_t command = Scnsl::Core::TCP_DISCONNECT;
            int l = 1;
            // disconnect only if the socket was not closed previously by the other
            // side
            if (!s->isClosed()) sendCommand(s->getTaskproxyKey(), &command, l);
        }
        // schedule erase socket from memory
        socketDeleter deleteEvent;
        deleteEvent.fd = s->getFD();
        deleteEvent.socket = s;
        _deleteQueue.push(deleteEvent);
        _deleteEventQueue.notify(CONN_CLOSE_TIME);
        return 0;
    }
    if (_listenmap != nullptr && _listenmap->find(sfd) != _listenmap->end())
    {
        // listening socket
        auto s = (*_listenmap)[sfd];
        _listenmap->erase(sfd);
        delete s;
        return 0;
    }
    return -1;
}

int NetworkAPI_Task_if_t::internal_shutdown(int sockfd, int how)
{
    if (_id_socket_map->find(sockfd) == _id_socket_map->end()) 
        return -1;
    auto s = (*_id_socket_map)[sockfd];
    switch (how)
    {
        case SHUT_RD:  // read-close
        {
            s->setRClosed();
            break;
        }
        case SHUT_WR:  // write-close
        {
            s->setWClosed();
            break;
        }
        case SHUT_RDWR:  // read/write-close
        {
            s->setWClosed();
            s->setRClosed();
            break;
        }
        default: 
            return -1;
    }
    return 0;
}

///////////////////////////////////////////////////
// NON-API FUNCTIONS
//////////////////////////////////////////////////

int* NetworkAPI_Task_if_t::getInternalError()
{
    return &_errno;
}

void NetworkAPI_Task_if_t::waitForTime()
{
    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::nano> duration = now - _start_time;
    // calculate time spend by currApi outside scnsl functions and wait
    sc_core::sc_time nanoseconds(duration.count(), sc_core::SC_NS);
    wait(nanoseconds);
} 

void NetworkAPI_Task_if_t::initTime()
{
    auto now = std::chrono::high_resolution_clock::now();
    _start_time = now;
}

void NetworkAPI_Task_if_t::deleteSocket()
{
    while (1)
    {
        wait(_deleteEventQueue.default_event());
        socketDeleter s = _deleteQueue.front();
        _deleteQueue.pop();
        _id_socket_map->erase(s.fd);
        auto tp = _proxies[s.socket->getTaskproxyKey()];
        _tp_socket_map->erase(tp);
        _bound_sockets->erase(source_pair_t(s.socket->getSourceIp(),
                                                s.socket->getSourcePortNo()));
        delete s.socket;
    }
}

void NetworkAPI_Task_if_t::sendCommand(const std::string & tpKey, byte_t * buffer, 
                                    const size_t s, const label_t label)
{
    if (_bindIdMap.find(tpKey) == _bindIdMap.end())
    {
        throw std::runtime_error("This bind identifier has not been defined.");
    }
    const tlm_taskproxy_id_t tp = static_cast<tlm_taskproxy_id_t>(
        _bindIdMap.at(tpKey));

    _payload.set_command(Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::COMMAND_PAYLOAD));
    _payload.set_data_ptr(buffer);
    _payload.set_data_length(s);
    _payload.set_address(_ID);

    Scnsl::Core::data_extension_t ext;
    ext.label = label;
    _payload.set_extension(&ext);
    initiators[tp]->b_transport(_payload, _delay);
    _payload.set_extension(static_cast<Scnsl::Core::data_extension_t *>(nullptr));
}

void NetworkAPI_Task_if_t::b_transport(tlm::tlm_generic_payload & p, sc_core::sc_time & t)
{
    // get taskproxy id from packet address
    auto id = static_cast<Scnsl::Core::IdentifierNumber_t>(p.get_address());  
    std::string idKey;
    std::stringstream ss;
    ss << id;
    ss >> idKey;
    if (_proxies.find(idKey) == _proxies.end())
        throw std::runtime_error(
                "NetworkAPI: btransport couldn't find source taskproxy ");
    
    //get taskproxy from key
    Scnsl::Core::TaskProxy_if_t * tp = _proxies[idKey];  
    if (p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))  
    {
        // data packet
        if (_tp_socket_map->find(tp) == _tp_socket_map->end())  
        {
            std::stringstream ss ;
            ss<<name()<<" taskproxy "<<tp<< " not registered"<<std::endl;
            throw std::runtime_error(ss.str());  
        }
        
        // get socket relative to taskproxy
        Socket_t * ts = (*_tp_socket_map)[tp];  
        if (ts->isClosed())  // no more data in closed socket
            return;
        if(ts->getProtocol() == SOCK_STREAM)
            ts->moveDataFromBuffer(p.get_data_ptr(), p.get_data_length());
        else 
        {
            Scnsl::Core::socket_properties_t sp = SocketMap::getSocket(tp);
            if (SocketMap::isIPMulticast(sp.dest_ip) &&
                 ! ts->hasMulticastMembership(sp.dest_ip))
            {
                std::cerr<<"NOt receiving since socket not memeber of this multicast ip"<<std::endl;
                return; //message from multicast address which socket is not memeber,
                        // return wothout reading message
            }
            ts->moveMessageFromBuffer(p.get_data_ptr(), p.get_data_length(), sp);
        }
    }
    else  // command payload received
    {
        byte_t * buffer = p.get_data_ptr();
        // COMMAND FORMAT FOR TCP API: a single byte representing the value of
        // the command Available commands: connect ( a connection request to
        // server), disconnect (to server)
        switch (buffer[0])
        {
            case Scnsl::Core::TCP_CONNECT:
            {
                Scnsl::Core::socket_properties_t sp = SocketMap::getSocket(tp);
         
                // search if a socket was waiting on the dest ip and port
                bool found = false;
                Socket_t * s;
                if (_listenmap != nullptr)  
                {
                    // server side->find socket listening on the port/ip
                    for (auto i = _listenmap->begin(); i != _listenmap->end() && !found; i++)
                    {

                        s = i->second;
                        if ((s->getSourceIp() == sp.source_ip ||
                            s->getSourceIp() == INADDR_ANY) &&
                            s->getSourcePortNo() == sp.source_port)
                        {
                            // socket found
                            found = true;
                        }
                    }
                    if (!found)  // cannot access socket from tp map
                    {
                        std::cerr << name() << " No socket found listening on "
                                  << sp << std::endl;
                        return;  // no socket listening found
                    }
                    // create new socket to dialog with client
                    auto socket = new Socket_t(_next_fd, SOCK_STREAM, sp, &_select_awake);  
                    _next_fd++;
                    (*_id_socket_map)[socket->getFD()] = socket;
                    socket->setTaskproxyKey(idKey);
                    s->pushConnectionRequest(socket);
                }
                else
                {
                    
                    if (_tp_socket_map->find(tp) == _tp_socket_map->end())  
                    {
                        std::cerr << name() << " No socket found for " << sp
                                  << std::endl;
                        return;  // no socket listening found
                    }
                    else
                        s = (*_tp_socket_map)[tp];
                    s->notifyConnectionRequest();
                }
            }
            break;
            case Scnsl::Core::TCP_DISCONNECT:
            {
                Scnsl::Core::socket_properties_t sp = SocketMap::getSocket(tp);
                // search for socket
                Socket_t * s;
                if (_tp_socket_map->find(tp) == _tp_socket_map->end())  
                {
                    std::cerr << " No socket found listening on " << sp.dest_ip
                              << " " << sp.dest_port << std::endl;
                    return;  // no socket listening found
                }
                s = (*_tp_socket_map)[tp];
                s->close();
            }
            break;
            case Scnsl::Core::TCP_SND_SIZE:
            {
                if (!(_tp_socket_map->find(tp) == _tp_socket_map->end()))  
                {
                    Socket_t * s = (*_tp_socket_map)[tp];
                    size_t dim;
                    // retrieve the buffer dimension
                    memcpy(&dim, buffer + 1, sizeof(dim));  
                    s->setSndBufferSize(dim);
                }
            }
            break;
            default: return;
        }
    }
}


int NetworkAPI_Task_if_t::internal_getifaddrs (ifaddrs **__ifap)
{
    auto ip = SocketMap::getNetworkInterfaces(_node);
    ifaddrs *root, *curr, *next;
    bool first = 1;
    for (auto i:*ip)
    {
        
        next = new ifaddrs();
        if (first){
            root = next;
            first = 0;
        }
        else curr->ifa_next = next;
        next->ifa_name = "eth0";
        next->ifa_addr = new sockaddr();
        next->ifa_addr->sa_family = AF_INET;
        next->ifa_addr->sin_addr.s_addr=i;
        curr = next;
    }
    curr->ifa_next=NULL;
    return 0;
}

void NetworkAPI_Task_if_t::send(const std::string & tpKey, byte_t * buffer, 
                                const size_t s, const label_t label)
{
    TlmTask_if_t::send(tpKey,buffer,s,label);
}


