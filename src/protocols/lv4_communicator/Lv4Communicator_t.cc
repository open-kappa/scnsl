#include "scnsl/protocols/lv4_communicator/Lv4Communicator_t.hh"

#include <sstream>
using Scnsl::Core::SocketMap;
using Scnsl::Protocols::Network_Lv4::Lv4Communicator_t;

Lv4Communicator_t::Lv4Communicator_t(const sc_core::sc_module_name modulename, 
                                        bool log, TcpConns type):
sc_core::sc_module(modulename),
Scnsl::Core::Communicator_if_t(),
Scnsl::Tracing::Traceable_base_t(modulename),
_conn_type(type),
_rcvwnd(TCP_MAX_RECEIVE_BYTE),
_full_header_size(0),
_segment_size(TCP_DEFAULT_SEGMENT_SIZE),
_ssa(DEFAULT_SLOW_START),
_caa(AIMD_CA),
_fra(RENO),
_wmem_size(DEFAULT_WMEM),
_tcp_no_delay(false),
_rto(MIN_TIMEOUT_VALUE),
_fast_ack_count(0),
_send_ack_after_full_segment(true),
_id_socket_map(),
_connections(new std::map<unsigned, TcpConnection_if_t *>()),
_asynch_timeout_queue(),
_synch_timeout_queue(),
_status_log(log)
{
    SC_THREAD(asynchEventManager)

    SC_THREAD(sendEventManager)

    // debug print for plotting
    if (log)
        std::cerr << "####,Comm,id,direction,timestamp,source,dest,seq_numb,"
                     "ack_numb,flags,segment_size,total size" << std::endl;
}

Lv4Communicator_t::~Lv4Communicator_t()
{
    delete _connections;
}

void Lv4Communicator_t::setCarrier(const Channel_if_t * ch, const carrier_t c)
{

}

Lv4Communicator_t::errorcode_t Lv4Communicator_t::send(const Packet_t & p)
{
    SCNSL_TRACE_DBG(1, ">> send()");
    // get the relative connection, if present
    TcpConnection_if_t * conn;
    const TaskProxy_if_t * tp = p.getSourceTaskProxy(); 
    socket_properties_t s = SocketMap::getSocket(tp);

    /**
     * Get buffer to know which protocol used. First byte indicate protocol type:
        U = UDP, create a packet and send immediately
        T = TCP, proceed with standard TCP implementation.
        Caution with command packets. they work only for TCP
        In any case resize buffer after controls
    */
    // command arrived, try to decode or redirect to lower level
    if (p.getType() == Scnsl::Core::COMMAND_PACKET)
    {
        auto comm = p.getCommand();
        // possible command for the communicator: 1) CONNECT, 2) DISCONNECT
        // since the command is stored as a single byte, only the first element
        // of the buffer is checked
        if (comm == Scnsl::Core::TCP_CONNECT)
        {
            unsigned id;
            if (_id_socket_map.find(s) == _id_socket_map.end())  // connection not found
            {
                id = TcpConnection_if_t::getNextId();
                _id_socket_map[s] = id;
            }
            else
                id = _id_socket_map[s];
            if (_connections->find(id) == _connections->end() ||
                (*_connections)[id]->getConnectionState() == CLOSED)
            {
                // check if first condition is false (i.e the connection is
                // closed)
                if (_connections->find(id) != _connections->end())
                    id = TcpConnection_if_t::getNextId();
                // creating new connection if none found
                std::cerr << "Creating connection for socket " << s
                          << std::endl;
                auto new_conn = 
                    TcpConnectionFactory::getConnection(_conn_type, id, s, &_asynch_timeout_queue);

                new_conn->setWmemSize(_wmem_size);
                new_conn->setMaxSegSize(_segment_size);
                new_conn->setRcvwnd(_rcvwnd);
                new_conn->setFullHeaderSize(_full_header_size);
                new_conn->setSlowStart(_ssa);
                new_conn->setCongestionAvoidance(_caa);
                new_conn->setFastRecovery(_fra);
                new_conn->setTcpNoDelay(_tcp_no_delay);
                new_conn->setMinRetransmissionTimeout(_rto);
                new_conn->setSampleSendPacket(p, false);
                new_conn->setLog(_status_log);
                new_conn->setFastAckCount(_fast_ack_count);
                new_conn->setSendAckAfterFullSegment( _send_ack_after_full_segment);
                (*_connections)[new_conn->getId()] = new_conn;
                _id_socket_map[s] = new_conn->getId();
            }
            conn = (*_connections)[id];
            // if the connection does not have a sample packet ready, initialize it.
            if (!conn->isSamplePacketRdy()) conn->setSampleSendPacket(p, false);
            SCNSL_TRACE_DBG(1, "TCP COMMUNICATOR: starting connection");
            if (conn->openRequest())
            {
                // start 3 way handshake.
                std::stringstream ss;
                ss << "Sending syn from conn " << conn->getId();
                TimeoutStruct_t ts(DATA_SEND, conn->getId(), 0, 0,
                                    sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
                _synch_timeout_queue.notify(ts);
                SCNSL_TRACE_DBG(1, ss.str().c_str());
            }
            return 0;
        }
        else if (comm == Scnsl::Core::TCP_DISCONNECT)
        {
            unsigned id;
            if (_id_socket_map.find(s) == _id_socket_map.end())  // connection not found
            {
                id = TcpConnection_if_t::getNextId();
                _id_socket_map[s] = id;
            }
            else
                id = _id_socket_map[s];
            if (_connections->find(id) == _connections->end())
                return 0;  // disconnect on a closed/non-existent connection;
            conn = (*_connections)[id];
            SCNSL_TRACE_DBG(1, "TCP COMMUNICATOR: DISCONNECTION COMMAND");
            conn->closeRequest();  // if the connection need to send fin alone
            std::stringstream ss;
            ss << "Sending fin from conn " << conn->getId();
            SCNSL_TRACE_DBG(1, ss.str().c_str());
            TimeoutStruct_t ts(DATA_SEND, conn->getId(), 0, 0,
                                    sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
            _synch_timeout_queue.notify(ts);
            return 0;
        }
        else if (comm == Scnsl::Core::TCP_NOBLOCK)
        {
            unsigned id;
            if (_id_socket_map.find(s) == _id_socket_map.end())  // connection not found
            {
                id = TcpConnection_if_t::getNextId();
                _id_socket_map[s] = id;
            }
            else
                id = _id_socket_map[s];
            if (_connections->find(id) == _connections->end()) return 0;
            conn = (*_connections)[id];
            conn->setNonBlock(true);
            return 0;
        }
        else
            //command not recognized, redirect to lower communicators
            return Communicator_if_t::send(p);  
    }

    byte_t* buffer = p.getInnerPayload();
    size_t size = p.getPayloadSizeInBytes();   

    if (size < 2)
        return -1; //Data but less than one byte (the protocol to use)
                   // ==> empty packet, return

    if (buffer[0]=='U') 
    {
        
        return Communicator_if_t::send(
            UdpProtocol::createUDPSegment(p,s,_full_header_size,_status_log, name()));
    }
    
    if (buffer[0] != 'T' ||  p.getDestinationTask() == nullptr)
        return -1; //No UDP or multicast --> error

    // TCP PACKET MANAGING
    unsigned id;

    if (_id_socket_map.find(s) == _id_socket_map.end())  // connection not found
    {
        id = TcpConnection_if_t::getNextId();
        _id_socket_map[s] = id;
    }
    else
        id = _id_socket_map[s];
    // Add the packet to the connection buffer
    if (_connections->find(id) == _connections->end()) return 0;
    conn = (*_connections)[id];
    if (!conn->isSamplePacketRdy())  // if the connection does not have a
                                     // samplepacket ready, initialize it.
        conn->setSampleSendPacket(p, false);

    conn->addPacketToBuffer(p);
    
    if (conn->canSend())
    {
        TimeoutStruct_t ts(DATA_SEND, conn->getId(), 0, 0,
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        _synch_timeout_queue.notify(ts);
    }
    return 0;
}

Lv4Communicator_t::errorcode_t Lv4Communicator_t::receive(const Packet_t & p)
{
    SCNSL_TRACE_DBG(1, ">> receive()");
    socket_properties_t s;

    std::stringstream sstr;
    s = SocketMap::getSocket(p.getSourceTaskProxy());

    auto tempS = s.source_ip;
    auto tempUI = s.source_port;
    s.source_ip = s.dest_ip;
    s.source_port = s.dest_port;
    s.dest_ip = tempS;
    s.dest_port = tempUI;
    unsigned id;
    auto * packet = dynamic_cast<LV4ProtocolPacket_t*>(p.getPayload());
    
    if (packet == nullptr) 
        throw std::runtime_error("Receive: cannot cast packet to LV4Packet\n");

    if (packet->getProtocolType() == UDP)
        return Communicator_if_t::receive(UdpProtocol::removeUDPSegment(p,s,
                                                            _status_log,name()));

    if (packet->getProtocolType() != TCP)
        return -1; //NO UDP or TCP = error;
    //Normal TCP managing
    if (_id_socket_map.find(s) == _id_socket_map.end())  // connection not found
    {
        id = TcpConnection_if_t::getNextId();
        _id_socket_map[s] = id;
    }
    else
        id = _id_socket_map[s];

    if (_connections->find(id) == _connections->end() ||
        (*_connections)[id]->getConnectionState() == CLOSED)
    {
        if (TcpConnectionFactory::isSyn(_conn_type, p.getPayload()))
        {
            if (_connections->find(id) != _connections->end())
                id = TcpConnection_if_t::getNextId();
            // Connection not present and syn received, create a new one
            std::cerr<<name() << " Creating connection for socket " << s << std::endl;
            auto new_conn = TcpConnectionFactory::getConnection(_conn_type, id,
                                                    s, &_asynch_timeout_queue);
            new_conn->setWmemSize(_wmem_size);
            new_conn->setMaxSegSize(_segment_size);
            new_conn->setRcvwnd(_rcvwnd);
            new_conn->setFullHeaderSize(_full_header_size);
            new_conn->setSlowStart(_ssa);
            new_conn->setCongestionAvoidance(_caa);
            new_conn->setFastRecovery(_fra);
            new_conn->setTcpNoDelay(_tcp_no_delay);
            new_conn->setMinRetransmissionTimeout(_rto);
            new_conn->setSampleSendPacket(p, true);
            new_conn->setLog(_status_log);
            new_conn->setFastAckCount(_fast_ack_count);
            new_conn->setSendAckAfterFullSegment(_send_ack_after_full_segment);
            (*_connections)[new_conn->getId()] = new_conn;
            _id_socket_map[s] = new_conn->getId();
        }
        // no syn received, message discarded
        else
            return 0;
    }
    TcpConnection_if_t * conn;
    conn = (*_connections)[id];

    if (!conn->isSampleReceivePacketRdy())
        (*_connections)[id]->setSampleReceivePacket(p);

    sstr.str(std::string(""));

    // manage received packet
    conn->printTrace(p, name(), false);
    conn->managePacket(p);

    errorcode_t error = 0;

    // Receiving data to upper communicator
    Packet_t pckt;
    while (conn->hasPacketToread())  // while receive buffer has packet, send
                                     // them to upper communicators
    {
        pckt = conn->getNextReceivePacket();
        error = Communicator_if_t::receive(pckt) || error;
    }

    // ACK MANAGING
    if (conn->needSendAck())
    {
        TimeoutStruct_t ts( DATA_SEND, conn->getId(), conn->getNextPacketNumber(),
                                conn->getNextPacketNumber(), 
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        if (conn->needImmediateAck())
        {
            sstr << "<> receive(); Sending immediate ack "
                 << conn->getNextPacketNumber();
            SCNSL_TRACE_DBG(1, sstr.str().c_str());
            conn->addPriorityPacket(conn->sendAck());
            _synch_timeout_queue.notify(ts);
        }
        else
        {
            // change ack from immediate to timed
            ts._type = ACK_TIMEOUT;
            ts._timestamp = sc_core::sc_time_stamp() + MAX_ACK_TIMEOUT;
            sstr << ">> receive(); Scheduling ack at " << ts._timestamp;
            _asynch_timeout_queue.notify(ts);
            SCNSL_TRACE_DBG(1, sstr.str().c_str());
        }
    }
    sstr.str(std::string(""));
    if (conn->needSynAck())
    {
        sstr << "<> receive(); Scheduling syn + ack.";
        SCNSL_TRACE_DBG(1, sstr.str().c_str());

        conn->addPriorityPacket(conn->createPacketWithFlags(true, false, true));
        TimeoutStruct_t t(DATA_SEND, conn->getId(), 0, 0, 
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        _synch_timeout_queue.notify(t);
    }
    else if (conn->needFin())
    {
        std::stringstream ss;
        ss << "Sending fin from conn " << conn->getId();
        SCNSL_TRACE_DBG(1, ss.str().c_str());
        conn->addPriorityPacket(conn->createPacketWithFlags(false, true, true));
        TimeoutStruct_t t(DATA_SEND, conn->getId(), 0, 0, 
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        _synch_timeout_queue.notify(t);
    }

    sstr.str(std::string(""));
    if (conn->needRetransmission())  // retransmission is required
    {
        counter_t seqn = conn->getFirstRetransmitPacket();
        sstr << ">> receive(); retransmitting segment: " << seqn;
        SCNSL_TRACE_DBG(1, sstr.str().c_str());
        conn->addPriorityPacket(conn->getRetransmitPacket(seqn));
        TimeoutStruct_t t(IMMEDIATE_RETRANSMISSION, conn->getId(), 0, seqn, 
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        _synch_timeout_queue.notify(t);
    }

    if (conn->getConnectionState() == CLOSED)
    {
        SCNSL_TRACE_DBG(1, "Scheduling connection delete");
        TimeoutStruct_t ts(CONNECTION_DELETE, conn->getId(), 0, 0, 
                                CONN_CLOSE_TIME + sc_core::sc_time_stamp());
        _asynch_timeout_queue.notify(ts);
        return 0;
    }

    if (conn->canSend())
    {
        // Activating send thread if needed
        TimeoutStruct_t ts(DATA_SEND, conn->getId(), 0, 0,
                                sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
        _synch_timeout_queue.notify(ts);
    }

    SCNSL_TRACE_DBG(1, ">> receive(); done");
    return error;
}

void Lv4Communicator_t::registerTracer(Scnsl::Tracing::Tracer_t * t)
{
    Scnsl::Tracing::Traceable_base_t::registerTracer(t);
}

///////////////////////////
// SETTERS
//////////////////////////
void Lv4Communicator_t::setRcvwnd(size_t rcvwnd)
{
    _rcvwnd = rcvwnd;
}

void Lv4Communicator_t::setExtraHeaderSize( size_t ip_header_size, size_t mac_header_size)
{
    _full_header_size = ip_header_size + mac_header_size;
}

void Lv4Communicator_t::setSegmentSize(size_t segmentSize)
{
    _segment_size = segmentSize;
}

void Lv4Communicator_t::setSlowStartAlg(TcpAlgs ssa)
{
    _ssa = ssa;
}

void Lv4Communicator_t::setCongestionAvoidanceAlg(TcpAlgs caa)
{
    _caa = caa;
}

void Lv4Communicator_t::setFastRecoveryAlg(TcpAlgs fra)
{
    _fra = fra;
}

void Lv4Communicator_t::set_TCP_NO_DELAY(bool flag)
{
    _tcp_no_delay = flag;
}

void Lv4Communicator_t::setWmemSize(size_t wmemSize)
{
    _wmem_size = wmemSize;
}

void Lv4Communicator_t::setRto(sc_core::sc_time rto)
{
    _rto = rto;
}

void Lv4Communicator_t::setFastAckCount(unsigned short fastAckCount)
{
    _fast_ack_count = fastAckCount;
}

void Lv4Communicator_t::setSendAckAfterFullSegment(bool sendAckAfterFullSegment)
{
    _send_ack_after_full_segment = sendAckAfterFullSegment;
}

/////////////////////
/// PRIVATE THREADS
////////////////////

void Lv4Communicator_t::asynchEventManager()
{
    TcpConnection_if_t * conn;
    std::stringstream sstr;
    while (1)
    {
        wait(_asynch_timeout_queue.defaultEvent());

        // while the firs element in the queue has timestamp < current timestamp
        // i.e it has happened
        while (_asynch_timeout_queue.isTopFired())
        {
            TimeoutStruct_t s = _asynch_timeout_queue.getTop();
            if (_connections->find(s._connection_id) == _connections->end())
              // connection removed from the list (due to internal_close)
                continue;
            conn = (*_connections)[s._connection_id];
            // delete command, delete connection and go on
            if (s._type == CONNECTION_DELETE)
            {
                std::cerr << "Deleting Connection " << s._connection_id
                          << std::endl;
                _connections->erase(conn->getId());
                delete conn;
                continue;
            }
            // no delete, but connection is closed, skip
            if (conn->getConnectionState() == CLOSED) continue;

            sstr.str(std::string(""));
            sstr << " Connection: " << conn->getId();
            // need send ack, but check if it has already been send
            if (s._type == ACK_TIMEOUT &&  conn->getLastAckSend() < s._ack_number)  
            {
                sstr << " SENDING ACK " << s._sequence_number;
                SCNSL_TRACE_DBG(1, sstr.str().c_str());
                conn->addPriorityPacket(conn->sendAck());
                TimeoutStruct_t t(
                    ACK_TIMEOUT,
                    conn->getId(),
                    0,  // new timeout
                    s._sequence_number,
                    sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
                _synch_timeout_queue.notify(t);
            }
            else if ( s._type == RETRANSMISSION_TIMEOUT &&
                    !conn->isPacketAcknowledge(s._sequence_number))  
            {
                // retransmission timeout, and packet has not acknowledge yet
                sstr << " RETRANSMISSION TIMEOUT EVENT FOR ACK"
                     << s._sequence_number;
                SCNSL_TRACE_DBG(1, sstr.str().c_str());
                // activate fsm to tell a timeout has occurred. Only the first
                // parameter is used, hence all the other are set to basic values
                conn->tcp_EFSM(true, sc_core::SC_ZERO_TIME, false, 0);
                conn->addPriorityPacket(
                    conn->getRetransmitPacket(s._sequence_number));
                TimeoutStruct_t t(
                    RETRANSMISSION_TIMEOUT,
                    conn->getId(),
                    0,  // new timeout
                    s._sequence_number,
                    sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
                _synch_timeout_queue.notify(t);
                t._timestamp = conn->getRto() + sc_core::sc_time_stamp();
                _asynch_timeout_queue.notify(t);
            }
            else if (s._type == DATA_RECEIVE)
            {
                Packet_t pckt;
                // while receive buffer has packet, send them to upper communicators

                while (conn->hasPacketToread())            
                {
                    pckt = conn->getNextReceivePacket();
                    Communicator_if_t::receive(pckt);
                }
            }
        }
        _asynch_timeout_queue.fireNext();  // fire first element of the queue;
    }
}

void Lv4Communicator_t::sendEventManager()
{
    counter_t current_ack_number, next_ack_number;
    TcpConnection_if_t * conn;
    sc_core::sc_time RTO;
    std::stringstream sstr;
    while (1)
    {
        wait(_synch_timeout_queue.defaultEvent());

        // while the firs element in the queue has timestamp < current timestamp
        // i.e it has happened
        while (_synch_timeout_queue.isTopFired())
        {
            auto s = _synch_timeout_queue.getTop();
            if (_connections->find(s._connection_id) == _connections->end())  
            {
                conn = nullptr;
                continue;
            }

            conn = (*_connections)[s._connection_id];
            RTO = conn->getRto();

            // while there is at least one packet with high priority, send it
            while (conn->hasPriorityPacket())
            {
                auto p = conn->getPriorityPacket();
                conn->printTrace(p, name(), true);
                Communicator_if_t::send(p);
            }
            if (conn->canSend())
            {
                sstr.str(std::string("\t", conn->getId()));
                current_ack_number = conn->getNextSendPacketAckNumber();
                auto p = conn->preparePacket();
                conn->printTrace(p, name(), true);
                next_ack_number = conn->getNextSendPacketAckNumber();
                TimeoutStruct_t t(
                    RETRANSMISSION_TIMEOUT,
                    conn->getId(),
                    next_ack_number,
                    next_ack_number,
                    RTO + sc_core::sc_time_stamp());
                _asynch_timeout_queue.notify(t);
                sstr << "Conn" << conn->getId()
                     << " Activating timeout for packet " << current_ack_number
                     << " at " << sc_core::sc_time_stamp() << " + " << RTO
                     << " = " << t._timestamp << "\n";
                SCNSL_TRACE_DBG(1, sstr.str().c_str());
                Communicator_if_t::send(p);

                if (conn->needFin())  // if the connection need to send fin alone
                {
                    std::stringstream ss;
                    ss << "Sending fin from conn " << conn->getId();
                    SCNSL_TRACE_DBG(1, ss.str().c_str());
                    auto p = conn->createPacketWithFlags(false, true, true);
                    conn->printTrace(p, name(), true);
                    Communicator_if_t::send(p);
                }
            }
            if (conn->canSend())
            {
                // scheduling next send
                TimeoutStruct_t ts(DATA_SEND, conn->getId(), 0, 0,
                                        sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
                _synch_timeout_queue.notify(ts);
            }
        }
        _synch_timeout_queue.fireNext();
        sstr.str("");
    }
}
