#include "scnsl/protocols/lv4_communicator/TimeoutStruct_t.hh"

using Scnsl::Protocols::Network_Lv4::TimeoutStruct_comparator_t;
using Scnsl::Protocols::Network_Lv4::TimeoutStruct_t;

TimeoutStruct_t::TimeoutStruct_t(Scnsl::Protocols::Network_Lv4::EventType type,
                                    unsigned int connectionId,
                                    Scnsl::Core::counter_t ackNumber,
                                    Scnsl::Core::counter_t sequenceNumber,
                                    const sc_core::sc_time & timestamp):
_type(type),
_connection_id(connectionId),
_ack_number(ackNumber),
_sequence_number(sequenceNumber),
_timestamp(timestamp)
{

}

std::ostream & 
Scnsl::Protocols::Network_Lv4::operator<<(std::ostream & os, const TimeoutStruct_t & t)
{
    os << "_type: ";
    switch (t._type)
    {
        case ACK_TIMEOUT: os << " ACK SEND"; break;
        case DATA_SEND: os << " SENDING DATA"; break;
        case DATA_RECEIVE: os << " RECEIVING DATA"; break;
        case IMMEDIATE_RETRANSMISSION: os << " IMM. RETR."; break;
        case RETRANSMISSION_TIMEOUT: os << " RETR. TIMEOUT"; break;
        case CONNECTION_DELETE: os << " DELETING CONNECTION"; break;
        default: os << "ERROR";
    }
    os << " " << t._type << " _connection_id: " << t._connection_id
       << " _ack_number: " << t._ack_number
       << " _sequence_number: " << t._sequence_number
       << " _timestamp: " << t._timestamp;
    return os;
}

bool TimeoutStruct_comparator_t::operator()(const TimeoutStruct_t & t1, 
                                                const TimeoutStruct_t & t2)
{
    if (t1._timestamp == t2._timestamp)
        return true;  // same timestamp, return true to put in the last place of
                      // the queue
    return t1._timestamp > t2._timestamp;
}