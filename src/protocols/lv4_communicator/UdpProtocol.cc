#include "scnsl/protocols/lv4_communicator/UdpProtocol.hh"

#include <sstream>

using namespace Scnsl::Protocols::Network_Lv4::UdpProtocol;
using Scnsl::Core::SocketMap;

/**
 * Debug only function
 */
void printbuffer(byte_t * packet, unsigned int size)
{
    std::stringstream sstr;
    sstr << "Size " << size << "\n";
    for (int i = 0; i < size; i++)
    {
        if (i == TCP_BASIC_HEADER_LENGTH) sstr << "\n";
        sstr << (unsigned int)packet[i] << " ";
    }
    std::cerr << sstr.str().c_str() << std::endl;
}

Packet_t Scnsl::Protocols::Network_Lv4::UdpProtocol::createUDPSegment(
    const Packet_t p,
    socket_properties_t sp,
    size_t full_header_size,
    bool log,
    std::string comm_name)
{
    // Prepare packet
    size_t packet_dim = p.getPayloadSizeInBytes() - 1;
    byte_t packet[UDP_HEADER_LENGTH + packet_dim];
    bzero(packet, UDP_HEADER_LENGTH + packet_dim);

    // set ports

    packet[UDP_SOURCE_PORT_BYTE] = (sp.source_port >> 8) & 0xFF;
    packet[UDP_SOURCE_PORT_BYTE + 1] = sp.source_port & 0xFF;

    packet[UDP_DEST_PORT_BYTE] = (sp.dest_port >> 8) & 0xFF;
    packet[UDP_DEST_PORT_BYTE + 1] = sp.dest_port & 0xFF;

    ushort len = p.getPayloadSizeInBytes() + UDP_HEADER_LENGTH;

    packet[UDP_LENGTH_BYTE] = (len >> 8) & 0xFF;
    packet[UDP_LENGTH_BYTE + 1] = len & 0xFF;

    memcpy(packet + UDP_HEADER_LENGTH, p.getInnerPayload() + 1, packet_dim);

    SimpleProtocolPacket_t simple_packet;
    simple_packet.setPayloadInBytes(packet, packet_dim + UDP_HEADER_LENGTH);
    simple_packet.setHeaderSizeInBytes(0);

    LV4ProtocolPacket_t udpWrapper;
    udpWrapper.setProtocol(UDP);
    udpWrapper.setPayload(simple_packet.clone());

    Packet_t send_packet(p);
    send_packet.setId(Packet_t::get_new_packet_id());
    send_packet.setHeaderSizeInBytes(full_header_size);
    send_packet.setDestinationNode(SocketMap::getNode(sp.dest_ip));
    send_packet.setPayload(udpWrapper);

    // if packet is multicast, set destination tp to nullptr
    if (p.getDestinationTask() == nullptr)
        send_packet.setDestTaskProxy(nullptr);

    if (log)
    {
        std::stringstream sstream;
        sstream << "####, " << comm_name << ", UDP, >>>, ";
        sstream << sc_core::sc_time_stamp().to_seconds() << ", "
                << sp.source_port << ", " << sp.dest_port << ", "
                << send_packet.getPayloadSizeInBytes() - UDP_HEADER_LENGTH
                << ", " << send_packet.getSizeInBytes() << std::endl;
        std::cerr << sstream.str() << std::endl;
    }
    return send_packet;
}

Packet_t Scnsl::Protocols::Network_Lv4::UdpProtocol::removeUDPSegment(
    const Packet_t p,
    socket_properties_t sp,
    bool log,
    std::string comm_name)
{
    auto * packet = dynamic_cast<
        Scnsl::Protocols::Network_Lv4::LV4ProtocolPacket_t *>(p.getPayload());

    if (packet == nullptr)
        throw std::runtime_error("Receive: cannot cast packet to LV4Packet\n");
    if (log)
    {
        std::stringstream sstream;
        sstream << "####, " << comm_name << ", UDP, <<<, ";
        sstream << sc_core::sc_time_stamp().to_seconds() << ", " << sp.dest_port
                << ", " << sp.source_port << ", "
                << p.getPayloadSizeInBytes() - UDP_HEADER_LENGTH << ", "
                << p.getSizeInBytes() << std::endl;
        std::cerr << sstream.str() << std::endl;
    }

    SimpleProtocolPacket_t pk;
    byte_t * buffer = packet->getInnerBuffer();
    Packet_t sim(p);
    sim.setDestTaskProxy(SocketMap::getTp(sp));
    pk.setPayloadInBytes(
        buffer + UDP_HEADER_LENGTH,
        p.getPayloadSizeInBytes() - UDP_HEADER_LENGTH);
    sim.setPayload(pk);  // setting as packet the one inside the tcp;
    sim.setHeaderSizeInBytes(0);  // delete extra size due to headers
    return sim;
}
