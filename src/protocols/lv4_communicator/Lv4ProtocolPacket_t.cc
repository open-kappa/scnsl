// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

///@file
/// Implementation of the ProtocolPacket interface to indicate which LV4 protocol
///has been used to send a certain buffer
#include "scnsl/protocols/lv4_communicator/Lv4ProtocolPacket_t.hh"

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;


// ////////////////////////////////////////////////////////////////
// Constructors.
// ////////////////////////////////////////////////////////////////

LV4ProtocolPacket_t::LV4ProtocolPacket_t():
ProtocolPacket_if_t(),
_payload(nullptr),
_type(TCP)
{
    // Nothing to do
}


LV4ProtocolPacket_t::LV4ProtocolPacket_t(const LV4ProtocolPacket_t &p):
ProtocolPacket_if_t(p),
_payload(nullptr),
_type(TCP)
{
    _payload = p.getPayload();
    _type = p.getProtocolType();
}


LV4ProtocolPacket_t::~LV4ProtocolPacket_t()
{
    delete _payload;
}


LV4ProtocolPacket_t &LV4ProtocolPacket_t::operator= (const LV4ProtocolPacket_t &p)
{
    // payload fields
    _payload = p.getPayload();
    _type = p.getProtocolType();
    _header_size = p._header_size;
    return *this;
}

void LV4ProtocolPacket_t::setProtocol(protocol_type type)
{
    _type = type;
}

protocol_type LV4ProtocolPacket_t::getProtocolType() const
{
    return _type;
}

// ////////////////////////////////////////////////////////////////
// Inherithed methods from interface.
// ////////////////////////////////////////////////////////////////


ProtocolPacket_if_t::size_t LV4ProtocolPacket_t::getPayloadSize() const
{
    return getInnerSize();
}

ProtocolPacket_if_t *LV4ProtocolPacket_t::getPayload() const
{
    if (_payload == nullptr)
        return nullptr;
    return _payload->clone();
}

void LV4ProtocolPacket_t::setPayload(const ProtocolPacket_if_t * packet) 
{
    _payload = packet;
}

byte_t *LV4ProtocolPacket_t::getInnerBuffer() const
{
    return _payload->getInnerBuffer();
}

ProtocolPacket_if_t::size_t LV4ProtocolPacket_t::getInnerSize() const
{
    return _payload->getInnerSize();
}

LV4ProtocolPacket_t *LV4ProtocolPacket_t::clone() const
{
    auto copy = new LV4ProtocolPacket_t(*this);
    return copy;
}

bool LV4ProtocolPacket_t::operator==(const ProtocolPacket_if_t &p) const
{
    auto cast_packet = dynamic_cast <const LV4ProtocolPacket_t *> (&p);
    if (  cast_packet == nullptr) {
        return false;
    }
    if (_payload != cast_packet->_payload ||
        _type != cast_packet->_type)
    {
        return false;
    }
    return true;
}

