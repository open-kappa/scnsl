#include "scnsl/protocols/lv4_communicator/connections/TcpConnectionFactory.hh"

using namespace Scnsl::Protocols::Network_Lv4;

TcpConnection_if_t * TcpConnectionFactory::getConnection(
    TcpConns type,
    Scnsl::Core::counter_t connectionId,
    Scnsl::Core::socket_properties_t socket,
    EventManager_t * timeout_queue)
{
    switch (type)
    {
        case TCP_BYTE_CONNECTION:
            auto new_byte = new TcpByteConnection(
                connectionId,
                socket,
                timeout_queue);
            return new_byte;
    }
}

bool TcpConnectionFactory::isSyn(
    TcpConns type,
    Scnsl::Core::ProtocolPacket_if_t * p)
{
    switch (type)
    {
        case TCP_BYTE_CONNECTION: return TcpByteConnection::isPacketSyn(p);
    }
}
