#include "scnsl/protocols/lv4_communicator/connections/TcpByteConnection.hh"

#include <sstream>
using namespace Scnsl::Protocols::Network_Lv4;
using Scnsl::Core::SocketMap;

TcpByteConnection::TcpByteConnection( unsigned int connectionId, socket_properties_t s,
                                        EventManager_t * timeout_queue):
TcpConnection_if_t(connectionId, s, timeout_queue),
_packet_buffer(nullptr),
_packet_buffer_current_start(0),
_packet_buffer_current_end(0),
_waiting_packets(new std::map<counter_t, SimpleProtocolPacket_t *>()),
_transmission_time(new std::map<size_t, sc_core::sc_time>())
{

}

TcpByteConnection::~TcpByteConnection()
{
    delete[] _packet_buffer;
    delete _waiting_packets;
    delete _transmission_time;
}

void TcpByteConnection::addPacketToBuffer(const Packet_t & pckt)
{
    // Initialization of the ports using the connection between taskproxy and
    // socket;
    auto p = dynamic_cast<SimpleProtocolPacket_t *>(pckt.getPayload());

    if (p == nullptr)
    {
        throw std::invalid_argument(
                    "TcpByteConnection can only manage SimpleProtocolPacket");
    }

    size_t pcktSize = p->getPayloadSizeInBytes()-1;

    // wait for enough space if packet size is too big and the connection is
    // blocking
    while (_wmem_size - _wmem_current_size < pcktSize && !_noblocking)
        wait(_buffer_space);

    size_t dim;  // the number of byte that will be copyed from the packet

    if (_noblocking && pcktSize > (_wmem_size - _wmem_current_size))
        dim = pcktSize - (_wmem_size - _wmem_current_size);
    else
        dim = pcktSize;
    // not enough contiguous space => loop to the start
    if (_packet_buffer_current_start <= _packet_buffer_current_end
        && _wmem_size - _packet_buffer_current_end < dim)
    {
        size_t temp;
        memcpy(
            _packet_buffer + _packet_buffer_current_end,
            p->getInnerBuffer() + 1, //+1 to remove protocol type
            _wmem_size - _packet_buffer_current_end);
        temp = _wmem_size - _packet_buffer_current_end;
        _packet_buffer_current_end = 0;
        memcpy(_packet_buffer, p->getInnerBuffer() + temp +1, dim - temp);
        _packet_buffer_current_end += dim - temp;
    }
    else
    {
        memcpy(
            _packet_buffer + _packet_buffer_current_end,
            p->getInnerBuffer() + 1,
            dim);
        _packet_buffer_current_end = (_packet_buffer_current_end + pcktSize) %
                                     _wmem_size;
    }
    _wmem_current_size += dim;
}

TcpByteConnection::Packet_t TcpByteConnection::packetFromBuffer()
{
    /*
     * Create a packet from the given buffers
     * Three way to create a packet
     * 1) Limited transmission enabled-> must send what we have
     * 2) Nagle is off or there are no packets waiting for ack => fill a packet
     * with all the possible data 3) Nagle is active: create a full-sized packet
     * If none of these conditions are met, the buffer is left untouched
     *  until new data or ack arrives
     */
    size_t packet_dim;  // calculate the dimension of the packet
    if (_limited_transmission_enabled)  
    // limited transmission => must send something (min between what we have and 
    //  the segment size)
        packet_dim = std::min(_wmem_current_size, _max_seg_size);
    else if (_retrasmission_map->empty() || _tcp_no_delay)  
    {
        // no nagle or no waiting for ack -> immediate send
        if (_wmem_current_size >= _max_seg_size
            && _cwnd - _flightSize >= _max_seg_size)
            packet_dim = _max_seg_size;
        else if (
            _wmem_current_size < _max_seg_size
            && _cwnd - _flightSize >= _max_seg_size)
            packet_dim = _wmem_current_size;
        else if (
            _wmem_current_size >= _max_seg_size
            && _cwnd - _flightSize < _max_seg_size)
            packet_dim = _cwnd - _flightSize;
        else
            packet_dim = std::min(_wmem_current_size, _cwnd - _flightSize);
    }
    else
        packet_dim = _max_seg_size;

    // base tcp header with no options + size calculated.
    byte_t packet[TCP_BASIC_HEADER_LENGTH + packet_dim];  
    bzero(packet, TCP_BASIC_HEADER_LENGTH + packet_dim);
    uShortInVect(_source_port, packet, TCP_SOURCE_PORT_BYTE);
    uShortInVect(_dest_port, packet, TCP_DEST_PORT_BYTE);
    uIntInVect(_next_sequence_number, packet, TCP_SEQ_NUMB_BYTE);
    packet[TCP_LENGTH_BYTE] = 0b1000 << 4;  // 32 byte /4 byte
    packet[TCP_FLAGS_BYTE] = 0b1 << 3;  // set the push flag

    if (_packet_buffer_current_start + packet_dim < _wmem_size)  
    {
        // data are contiguous => straight copy
        memcpy(
            packet + TCP_BASIC_HEADER_LENGTH,
            _packet_buffer + _packet_buffer_current_start,
            packet_dim);
        _packet_buffer_current_start += packet_dim;// update start point of the buffer
    }
    else  // data are not contiguous=> loop back to start
    {
        size_t temp;
        // copy to the end of buffer
        memcpy(
            packet + TCP_BASIC_HEADER_LENGTH,
            _packet_buffer + _packet_buffer_current_start,
            _wmem_size - _packet_buffer_current_start);
        temp = _wmem_size - _packet_buffer_current_start; // update current packet size
        _packet_buffer_current_start = 0;
        // copy the remaining bytes from the start
        memcpy(
            packet + TCP_BASIC_HEADER_LENGTH + temp,
            _packet_buffer,
            packet_dim - temp);
        _packet_buffer_current_start = packet_dim - temp;// update new starting position
    }
    // updating size
    _wmem_current_size -= packet_dim;

    uIntInVect(_next_packet_waited, packet, TCP_ACK_NUMB_BYTE);  // set ack value
    packet[TCP_FLAGS_BYTE] = packet[TCP_FLAGS_BYTE] | 0x10;  // set ack bit to 1;
    _need_send_ack = false;  // the packet contains an ack, so the flag is reset
    _last_ack_send = _next_packet_waited;
    unsigned long ack_numb = _next_sequence_number + packet_dim;
    _flightSize += packet_dim;  // update number of bytes send
    _next_sequence_number += packet_dim;  // update seq number
    _buffer_space.notify(sc_core::SC_ZERO_TIME);
    (*_transmission_time)[ack_numb] = sc_core::sc_time_stamp();// save send time of the packet

    SimpleProtocolPacket_t simple_packet;
    simple_packet.setPayloadInBytes(packet, packet_dim + TCP_BASIC_HEADER_LENGTH);
    simple_packet.setHeaderSizeInBytes(0);

    LV4ProtocolPacket_t protocol;
    protocol.setProtocol(TCP);
    protocol.setPayload(simple_packet.clone());

    Packet_t new_sim_packet(_sample_send_packet);  // creating a new packet
    new_sim_packet.setPayload(protocol);
    new_sim_packet.setId(Packet_t::get_new_packet_id());
    new_sim_packet.setHeaderSizeInBytes(_full_header_size);
    new_sim_packet.setDestinationNode(SocketMap::getNode(_dest_ip));
    // save packet in retransmission map with relative ack number
    (*_retrasmission_map)[ack_numb] = new_sim_packet;  

    if (_wmem_current_size <= 0 && _close_request)  
    {
        // no more data and a fin req pending
        updateState(false, false, true, true);
        _needFin = true;
    }
    return new_sim_packet;
}

bool TcpByteConnection::canSend() const
{
    if (_wmem_current_size <= 0) return false;
    // check if can send some data at the current state
    if ((_connection_state == ESTAB || _connection_state == CLOSE_WAIT) &&
         ((_tcp_no_delay && _cwnd > _flightSize)
            || (_retrasmission_map->empty() && _cwnd > _flightSize)
            || (_wmem_current_size >= _max_seg_size
                && _cwnd >= _max_seg_size + _flightSize)
            || (_limited_transmission_enabled
                && _flightSize <= (_cwnd + _max_seg_size))))
        return true;
    return false;
}

void TcpByteConnection::managePacket(const Packet_t & p)
{
    // get the tcp packet
    LV4ProtocolPacket_t * protocol = 
                (dynamic_cast<LV4ProtocolPacket_t *>( p.getPayload()));

    if (protocol == nullptr)  throw std::bad_cast();

    SimpleProtocolPacket_t * packet = 
                (dynamic_cast<SimpleProtocolPacket_t *>( protocol->getPayload()));  

    if (packet == nullptr) throw std::bad_cast();

    byte_t * buffer = packet->getInnerBuffer();
    // get packet sequence and ack number
    counter_t seq_number = vectToUInt(buffer, TCP_SEQ_NUMB_BYTE);
    counter_t ack_number = vectToUInt(buffer, TCP_ACK_NUMB_BYTE);
    // check packet flags
    bool syn = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b10);
    bool ack = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b10000);
    bool fin = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b1);
    sc_core::sc_time rtt;
    size_t acknowledge_bytes = 0;
    socket_properties_t source_tp_socket = SocketMap::getSocket(
        p.getSourceTaskProxy());

    if (_log)
        std::cerr << " Connection " << _id << ": reading packet with seqnumb "
                  << seq_number << " waited: " << _next_packet_waited
                  << std::endl;

    if (seq_number < _next_packet_waited)
    {
        // sequence number is lower than expected => packet already processed,
        // discard
        _need_send_ack = false;
        return;
    }

    if (syn)
    {
        updateState(syn, ack, _fin_arrived, false);
        return;
    }
    if (ack)
    {
        if (_log)
            std::cerr << " Connection " << _id << ": reading packet ack number "
                      << ack_number << " last ack got: " << _last_ack_received
                      << std::endl;
        if (_retrasmission_map->find(ack_number)
            != _retrasmission_map->end())  // new data acknowledged
        {
            unsigned long temp;
            _last_ack_received = ack_number;
            // updating rtt
            if (!_retransmitting)
                rtt = sc_core::sc_time_stamp()
                    - (*_transmission_time)[ack_number];

            // DELETE ALL PACKET THAT HAVE ID < ACK VALUE
            while (_retrasmission_map->find(ack_number)
                   != _retrasmission_map->end())
            {
                temp = (*_retrasmission_map)[ack_number]
                           .getPayload()
                           ->getPayloadSizeInBytes()
                    - TCP_BASIC_HEADER_LENGTH;  // skip the tcp header
                _retrasmission_map->erase(ack_number);
                ack_number -= temp;
                acknowledge_bytes += temp;
                _ack_size_counter += acknowledge_bytes;
            }
            tcp_EFSM(false, rtt, false, acknowledge_bytes);  // update fsm
            _duplicate_ack = 0;
            if (_flightSize < acknowledge_bytes)
                _flightSize = 0;
            else
                _flightSize -= acknowledge_bytes;
            _retransmitting = false;
        }
        else if (
            _last_ack_received == ack_number && !fin
            && (_connection_state == ESTAB || _connection_state == FIN_WAIT_1
                || _connection_state == FIN_WAIT_2)
            && (packet->getPayloadSizeInBytes() - TCP_BASIC_HEADER_LENGTH == 0))
        {
            _duplicate_ack++;
            tcp_EFSM(false, sc_core::SC_ZERO_TIME, true, 0);  // update fsm values for
                                                              // duplicate ack
        }
    }
    if (packet->getPayloadSizeInBytes() - TCP_BASIC_HEADER_LENGTH > 0)  
    {
        // data inside the packet:
        // insert packet in waiting queue. here will be extract only when his
        // seq. number will mach the waited one
        _waiting_packets->insert(std::pair<counter_t, SimpleProtocolPacket_t *>(
            seq_number,
            packet->clone()));
        SimpleProtocolPacket_t pk;
        _need_send_ack = true;  // next packet need to send ack for data
        counter_t most_recent_seqn = 0;

        // while the waited packet is inside the map (i.e is arrived out of
        // order)
        while (_waiting_packets->find(_next_packet_waited)
               != _waiting_packets->end())
        {
            pk = *(_waiting_packets->find(_next_packet_waited)->second);
            buffer = pk.getInnerBuffer();

            // creating new packets, one for each packet stored inside tcp
            Packet_t sim(_sample_receive_packet); 
            socket_properties_t dtp;
            dtp.source_ip = source_tp_socket.dest_ip;
            dtp.source_port = source_tp_socket.dest_port;
            dtp.dest_ip = source_tp_socket.source_ip;
            dtp.dest_port = source_tp_socket.source_port;
            sim.setDestTaskProxy(SocketMap::getTp(dtp));
            // REMOVING TCP HEADER FROM BUFFER
            most_recent_seqn = vectToUInt(buffer, TCP_SEQ_NUMB_BYTE);// update last seq_numb
            pk.setPayloadInBytes(
                &buffer[TCP_BASIC_HEADER_LENGTH],
                pk.getPayloadSizeInBytes() - TCP_BASIC_HEADER_LENGTH);
            sim.setPayload(pk);  // setting as packet the one inside the tcp;
            sim.setHeaderSizeInBytes(0);  // delete extra size due to headers

            // set destination taskproxy by inverting the related socket

            _receive_buffer->push(sim);
            delete (*_waiting_packets)[_next_packet_waited];
            _waiting_packets->erase(_next_packet_waited);  // remove from waiting
            _next_packet_waited += pk.getPayloadSizeInBytes();  // next packet waited
            _received_packets++;
            if (!_ack_threshold_surpassed
                && _received_packets > _fast_ack_count)
            {
                // stop sending immediate ack
                _received_packets = 1;
                _ack_threshold_surpassed = true;
            }
        }
        // duplicate control, since the _send_ack_after_full_segment ca be true
        // or false. When true need to calculate if two full size segments has
        // been received,
        // when flase only if two segments has been received
        if (!_waiting_packets->empty() ||  // packet out of order
            !_ack_threshold_surpassed ||  // packets received under the threshold
            (_send_ack_after_full_segment &&
             _last_ack_send + _max_seg_size <= most_recent_seqn &&
             _ack_threshold_surpassed)||  // two full segments received and immediate threshold surpassed
            (!_send_ack_after_full_segment && _received_packets % 2 == 0 &&
            _ack_threshold_surpassed)  // two segments received and threshold surpassed
        )
            _immediate_ack = true;
        else
            _immediate_ack = false;
    }
    else if (fin && seq_number != _next_packet_waited)
    {
        _need_send_ack = true;
        _immediate_ack = true;
        _fin_arrived = true;
    }
    // fin bit set, all packets processed and sequence number match (no loss)
    _fin_arrived = fin || _fin_arrived;
    fin = _fin_arrived && _waiting_packets->empty()
        && seq_number == _next_packet_waited;
    updateState(syn, ack, fin, false);
}

TcpByteConnection::Packet_t TcpByteConnection::sendAck()
{
    Packet_t ack(_sample_send_packet);
    LV4ProtocolPacket_t protocol;
    SimpleProtocolPacket_t pckt;

    byte_t buffer[TCP_BASIC_HEADER_LENGTH];
    uShortInVect(_source_port, buffer, TCP_SOURCE_PORT_BYTE);
    uShortInVect(_dest_port, buffer, TCP_DEST_PORT_BYTE);
    uIntInVect(_next_sequence_number, buffer, TCP_SEQ_NUMB_BYTE);
    uIntInVect(_next_packet_waited, buffer, TCP_ACK_NUMB_BYTE);
    buffer[TCP_LENGTH_BYTE] = 0b1000 << 4;  // 32 byte /4 byte
    bzero(buffer + TCP_FLAGS_BYTE, TCP_BASIC_HEADER_LENGTH - TCP_FLAGS_BYTE);
    buffer[TCP_FLAGS_BYTE] = buffer[TCP_FLAGS_BYTE] | 0x10;

    pckt.setHeaderSizeInBytes(0);
    pckt.setPayloadInBytes(buffer, TCP_BASIC_HEADER_LENGTH);

    protocol.setProtocol(TCP);
    protocol.setPayload(pckt.clone());

    ack.setId(Packet_t::get_new_packet_id());
    ack.setHeaderSizeInBytes(_full_header_size);
    ack.setPayload(protocol);
    ack.setDestinationNode(SocketMap::getNode(_dest_ip));

    _need_send_ack = false;
    _immediate_ack = false;
    _last_ack_send = _next_packet_waited;
    return ack;
}

TcpConnection_if_t::Packet_t 
    TcpByteConnection::createPacketWithFlags( bool syn, bool fin, bool ack)
{
    Packet_t p(_sample_send_packet);
    LV4ProtocolPacket_t protocol;
    SimpleProtocolPacket_t pckt;

    byte_t buffer[TCP_BASIC_HEADER_LENGTH];
    uShortInVect(_source_port, buffer, TCP_SOURCE_PORT_BYTE);
    uShortInVect(_dest_port, buffer, TCP_DEST_PORT_BYTE);
    uIntInVect(_next_sequence_number, buffer, TCP_SEQ_NUMB_BYTE);
    uIntInVect(_next_packet_waited, buffer, TCP_ACK_NUMB_BYTE);
    buffer[TCP_LENGTH_BYTE] = 0b1000 << 4;  // 32 byte /4 byte
    bzero(buffer + TCP_FLAGS_BYTE, TCP_BASIC_HEADER_LENGTH - TCP_FLAGS_BYTE);
    byte_t flags = boolToByte(fin) | boolToByte(syn) << 1
        | boolToByte(ack) << 4;
    buffer[TCP_FLAGS_BYTE] = buffer[TCP_FLAGS_BYTE] | flags;

    pckt.setHeaderSizeInBytes(0);
    pckt.setPayloadInBytes(buffer, TCP_BASIC_HEADER_LENGTH);

    protocol.setProtocol(TCP);
    protocol.setPayload(pckt.clone());

    p.setId(Packet_t::get_new_packet_id());
    p.setDestinationNode(SocketMap::getNode(_dest_ip));
    p.setHeaderSizeInBytes(_full_header_size);
    p.setPayload(protocol);

    _needSynAck = false;
    _needFin = false;

    return p;
}

TcpByteConnection::counter_t TcpByteConnection::getFirstRetransmitPacket()
{
    counter_t packet_key = -1;
    SimpleProtocolPacket_t * p;
    bool found = false;
    // search for packet with seqnumb = last ack received
    for (auto i = _retrasmission_map->begin();
         i != _retrasmission_map->end() && !found;
         i++)
    {
        p = dynamic_cast<SimpleProtocolPacket_t *>(i->second.getPayload()->getPayload());
        if (p == nullptr) SCNSL_TRACE_DBG(5, "ERROR, BAD CAST");
        if (vectToUInt(p->getInnerBuffer(), TCP_SEQ_NUMB_BYTE)
            == _last_ack_received)  // seqnumber = last_ack ==> found packet
        {
            packet_key = i->first;
            found = true;
        }
    }
    return packet_key;
}

void TcpByteConnection::printTrace(const TcpConnection_if_t::Packet_t & p, 
                                    std::string comm_name, bool isSent)
{
    if (!_log)  // log function disabled, return
        return;
    byte_t * buffer = p.getInnerPayload();
    unsigned int seq_number = vectToUInt(buffer, TCP_SEQ_NUMB_BYTE);
    unsigned int ack_number = vectToUInt(buffer, TCP_ACK_NUMB_BYTE);
    bool syn = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b10);
    bool ack = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b10000);
    bool fin = byteToBool(buffer[TCP_FLAGS_BYTE] & 0b1);
    unsigned short sourceport = vectToUShort(buffer, TCP_SOURCE_PORT_BYTE);
    unsigned short destport = vectToUShort(buffer, TCP_DEST_PORT_BYTE);

    std::stringstream sstream;
    sstream << "####, " << comm_name << ", " << _id;
    isSent ? sstream << ", >>>, " : sstream << ", <<<, ";
    sstream << sc_core::sc_time_stamp().to_seconds() << ", " << sourceport
            << ", " << destport << ", " << seq_number << ", " << ack_number
            << ", ";
    if (syn) sstream << "SYN ";
    if (fin) sstream << "FIN ";
    if (ack) sstream << "ACK ";
    sstream << ", " << p.getPayloadSizeInBytes() - TCP_BASIC_HEADER_LENGTH
            << ", " << p.getSizeInBytes() << std::endl;
    std::cerr << sstream.str() << std::endl;
}

void TcpByteConnection::setWmemSize(size_t wmemSize)
{
    _wmem_size = wmemSize;
    if (!_packet_buffer) _packet_buffer = new byte_t[_wmem_size];
}

bool TcpByteConnection::isPacketSyn(ProtocolPacket_if_t * p)
{
    LV4ProtocolPacket_t * packet = dynamic_cast<LV4ProtocolPacket_t*>(p);
    if (packet == nullptr) return false;

    SimpleProtocolPacket_t* sPacket =
                             dynamic_cast<SimpleProtocolPacket_t*>(packet->getPayload());
    if (sPacket == nullptr) return false;
    else
        return sPacket->getInnerBuffer()[TCP_FLAGS_BYTE] & 0b10;
}

//////////////////////////////////
// PRIVATE UTILITIES FUNCTION
//////////////////////////////////

unsigned short TcpByteConnection::vectToUShort(byte_t * arr, int start_index)
{
    unsigned short value = (arr[start_index] << 8) + arr[start_index + 1];
    return value;
}

unsigned int TcpByteConnection::vectToUInt(byte_t * arr, int start_index)
{
    unsigned int value = (arr[start_index] << 24) + (arr[start_index + 1] << 16)
        + (arr[start_index + 2] << 8) + (arr[start_index + 3]);
    return value;
}

void TcpByteConnection::uShortInVect( unsigned short value, byte_t * arr, int start_index)
{
    arr[start_index] = (value >> 8) & 0xFF;
    arr[start_index + 1] = value & 0xFF;
}

void TcpByteConnection::uIntInVect(
    unsigned int value,
    byte_t * arr,
    int start_index)
{
    arr[start_index] = (value >> 24) & 0xFF;
    arr[start_index + 1] = (value >> 16) & 0xFF;
    arr[start_index + 2] = (value >> 8) & 0xFF;
    arr[start_index + 3] = value & 0xFF;
}

TcpByteConnection::byte_t TcpByteConnection::boolToByte(bool x)
{
    return x ? 0b1 : 0b0;
}

bool TcpByteConnection::byteToBool(byte_t x)
{
    return x != 0b0;
}

void TcpByteConnection::printbuffer(byte_t * packet, unsigned int size)
{
    std::stringstream sstr;
    sstr << "Size " << size << "\n";
    for (int i = 0; i < size; i++)
    {
        if (i == TCP_BASIC_HEADER_LENGTH) sstr << "\n";
        sstr << (unsigned int)packet[i] << " ";
    }
    std::cerr << sstr.str().c_str() << std::endl;
}
