#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"

#include <cstdlib>
#include <iomanip>

using namespace Scnsl::Protocols::Network_Lv4;

unsigned int TcpConnection_if_t::_next_id;

unsigned int TcpConnection_if_t::getNextId()
{
    return _next_id++;
}

TcpConnection_if_t::TcpConnection_if_t(counter_t connectionId, socket_properties_t s,
                                        EventManager_t * timeout_queue):
Scnsl::Tracing::Traceable_base_t(std::to_string(connectionId).c_str()),
_id(connectionId),
_source_port(s.source_port),
_dest_port(s.dest_port),
_source_ip(s.source_ip),
_dest_ip(s.dest_ip),
_connection_state(NEW),
_congestion_state(SLOW_START),
_slow_start(nullptr),
_congestion_avoidance(nullptr),
_fast_recovery(nullptr),
_max_seg_size(0),
_cwnd(0),
_flightSize(0),
_sstresh(0),
_rcvwnd(0),
_ack_size_counter(0),
_SRTT(0, sc_core::SC_SEC),
_RTO(MIN_TIMEOUT_VALUE),
_minRTO(MIN_TIMEOUT_VALUE),
_RTT_VAR(),
_rtt_first_calculation(true),
_retransmitting(false),
_need_send_ack(false),  // no need at the beginning
_immediate_ack(false),
_need_retransmission(false),
_needSynAck(false),
_needFin(false),
_fin_arrived(false),
_close_request(false),
_limited_transmission_enabled(false),
_tcp_no_delay(false),
_next_packet_waited(0),
_next_sequence_number(0),
_last_ack_received(0),
_last_ack_send(0),
_duplicate_ack(0),
_fast_ack_count(0),
_received_packets(0),
_ack_threshold_surpassed(false),
_send_ack_after_full_segment(true),
_full_header_size(0),
_wmem_current_size(0),  // no data inside
_wmem_size(0),
_sample_send_packet_rdy(false),
_sample_send_packet(),
_sample_receive_packet_rdy(false),
_sample_receive_packet(),
_immediate_queue(new std::queue<Packet_t>),
_noblocking(false),
_retrasmission_map(new std::map<size_t, Packet_t>()),
_receive_buffer(new std::queue<Packet_t>()),
_event_queue(timeout_queue),
_buffer_space()
{
    
}

TcpConnection_if_t::~TcpConnection_if_t()
{
    delete _retrasmission_map;
    delete _receive_buffer;
    delete _immediate_queue;
    delete _slow_start;
    delete _congestion_avoidance;
    delete _fast_recovery;
}

void TcpConnection_if_t::tcp_EFSM(
    bool timeout_occurred,
    sc_core::sc_time rtt,
    bool is_last_ack_duplicate,
    size_t acknowledge_bytes)
{
    if (!timeout_occurred)
    {
        // new_timeout calculation
        if (!_retransmitting)  // no calculation during retransmission
        {
            if (_rtt_first_calculation)  // first rto calculation
            {
                _SRTT = rtt;
                _RTT_VAR = rtt / 2;
                _rtt_first_calculation = false;
            }
            else
            {
                long long diff = (long long)_SRTT.value()
                    - (long long)rtt.value();
                _RTT_VAR = (1 - BETA) * _RTT_VAR
                    + BETA * sc_core::sc_time::from_value(std::abs(diff));
                _SRTT = (1 - ALPHA) * _SRTT + ALPHA * rtt;
            }
            _RTO = std::max(_SRTT + std::max(G, K * _RTT_VAR), _minRTO);
        }
    }
    else
        _RTO = 2 * _RTO;  // timeout

    switch (_congestion_state)
    {
        case SLOW_START:
            _slow_start->EFSM(
                timeout_occurred,
                is_last_ack_duplicate,
                acknowledge_bytes);
            break;
        case CONGESTION_AVOIDANCE:
            _congestion_avoidance->EFSM(
                timeout_occurred,
                is_last_ack_duplicate,
                acknowledge_bytes);
            break;
        case FAST_RECOVERY:
            _fast_recovery->EFSM(
                timeout_occurred,
                is_last_ack_duplicate,
                acknowledge_bytes);
            break;
        default:  // unnkown state? go to slow start and reset variables
            if (timeout_occurred)
            {
                _congestion_state = SLOW_START;
                _sstresh = _rcvwnd / 2;
                _need_retransmission = true;
                _limited_transmission_enabled = false;
            }
            else
            {
                _sstresh = std::max(_cwnd / 2, 2 * _max_seg_size);
                _cwnd = _max_seg_size;
                _duplicate_ack = 0;
                _congestion_state = SLOW_START;
                _limited_transmission_enabled = false;
            }
    }
    if (_log)
    {
        std::stringstream sstr;
        // print for plotting the cwnd
        sstr << "@@@@@@@@" << _id << ","
             << sc_core::sc_time_stamp().to_seconds() << "," << _cwnd;
        std::cerr << sstr.str().c_str() << std::endl;
    }
}

void TcpConnection_if_t::toSlowStart()
{
    _congestion_state = SLOW_START;
    _slow_start->initAlg();
}

void TcpConnection_if_t::toCongestionAvoidance()
{
    _congestion_state = CONGESTION_AVOIDANCE;
    _congestion_avoidance->initAlg();
}

void TcpConnection_if_t::toFastRecovery()
{
    _congestion_state = FAST_RECOVERY;
    _fast_recovery->initAlg();
}

void TcpConnection_if_t::updateState(bool syn, bool ack, bool fin, bool send)
{
    // state change depending on the flags send
    switch (_connection_state)
    {
        case NEW:
            if (syn && !ack && !fin && send)  // sending only syn => opening
                _connection_state = SYN_SENT;
            else if (syn && !ack && !fin && !send)  
            {
                // receiving only syn => sending synack
                _connection_state = SYN_RCVD;
                _needSynAck = true;
            }
            break;
        case SYN_RCVD:
            if (!syn && ack && !fin && !send)  // received ack of syn-ack
            {
                _connection_state = ESTAB;
                prepareCommandPacket(Scnsl::Core::TCP_CONNECT, nullptr, 0);
            }
            break;
        case SYN_SENT:
            if (syn && ack && !fin && !send)  // received syn + ack
            {
                _connection_state = ESTAB;
                prepareCommandPacket(Scnsl::Core::TCP_CONNECT, nullptr, 0);
                _need_send_ack = true;
                _immediate_ack = true;
            }
            break;
        case ESTAB:
            if (!syn && fin && send)  // sending fin =>closing
                _connection_state = FIN_WAIT_1;
            else if (!syn && fin && !send)  
            {
                // receiving fin => sending ack + fin
                _need_send_ack = true;
                _immediate_ack = true;
                _connection_state = CLOSING;
                _needFin = true;
            }
            break;
        case FIN_WAIT_1:
            if (!syn && fin && !send)  // received fin
            {
                _connection_state = CLOSING;
                _need_send_ack = true;
                _immediate_ack = true;
            }
            else if (!syn && ack && !fin && !send)
                _connection_state = FIN_WAIT_2;
            break;
        case FIN_WAIT_2:
            if (!syn && fin && !send)
            {
                _connection_state = CLOSED;
                _immediate_ack = true;
                _need_send_ack = true;
                prepareCommandPacket(Scnsl::Core::TCP_DISCONNECT, nullptr, 0);
            }
            break;
        case CLOSING:
            if (!syn && (ack || fin) && !send)
            {
                _connection_state = CLOSED;
                prepareCommandPacket(Scnsl::Core::TCP_DISCONNECT, nullptr, 0);
            }
            break;
        case CLOSED: break;
        default: throw std::runtime_error("ERROR, state not recognized");
    }
}

bool TcpConnection_if_t::openRequest()
{
    if (_connection_state == NEW)
    {
        updateState(true, false, false, true);
        addPriorityPacket(createPacketWithFlags(true, false, false));
        return true;
    }
    return false;
}

bool TcpConnection_if_t::closeRequest()
{
    _close_request = true;  // store internal_close request
    if (_wmem_current_size <= 0 && _connection_state == ESTAB)  
    {
        // if no more data to send close the connection
        std::cerr << "Close request accepted" << std::endl;
        updateState(false, false, true, true);
        addPriorityPacket(createPacketWithFlags(false, true, true));
        return true;
    }
    std::cerr << "Close req refused" << std::endl;
    return false;
}

TcpConnection_if_t::Packet_t TcpConnection_if_t::preparePacket()
{
    Packet_t p = packetFromBuffer();
    byte_t size[sizeof(size_t)];
    size_t free_space = _wmem_size - _wmem_current_size;
    memcpy(size, &free_space, sizeof(size_t));
    prepareCommandPacket(Scnsl::Core::TCP_SND_SIZE, size, sizeof(size_t));
    return p;
}

TcpConnection_if_t::Packet_t TcpConnection_if_t::getRetransmitPacket( const counter_t ack_number)
{
    if (_retrasmission_map->find(ack_number) == _retrasmission_map->end())
        throw std::runtime_error("WARNING, CANNOT FIND PACKET TO RETRANSMIT");
    Packet_t p = (*_retrasmission_map)[ack_number];
    _need_retransmission = false;
    _retransmitting = true;
    return p;
}

bool TcpConnection_if_t::hasPacketToread() const
{
    return !_receive_buffer->empty();
}

TcpConnection_if_t::Packet_t TcpConnection_if_t::getNextReceivePacket()
{
    Packet_t sim = _receive_buffer->front();
    _receive_buffer->pop();
    return sim;
}

bool TcpConnection_if_t::isPacketAcknowledge(const counter_t number) const
{
    if (_connection_state != CLOSED)
        return _retrasmission_map->find(number) == _retrasmission_map->end();
    return true;
}

//////////////////
// GETTERS + SETTERS
/////////////////

unsigned int TcpConnection_if_t::getId() const
{
    return _id;
}

void TcpConnection_if_t::setId(unsigned int id)
{
    _id = id;
}

const sc_core::sc_time & TcpConnection_if_t::getRto() const
{
    return _RTO;
}

bool TcpConnection_if_t::isSamplePacketRdy() const
{
    return _sample_send_packet_rdy;
}

void TcpConnection_if_t::setSampleSendPacket(const Packet_t & p, bool reverse)
{
    // copy only important parameters of the class. Assignment is not optimal
    // since it copy all the payload,
    // hence creating new  object that may be lost
    _sample_send_packet.setSequenceNumber(p.getSequenceNumber());
    _sample_send_packet.setLabel(p.getLabel());

    auto reversed_socket = Scnsl::Core::SocketMap::getSocket(
        p.getSourceTaskProxy());
    auto tempS = reversed_socket.source_ip;
    auto tempUI = reversed_socket.source_port;
    reversed_socket.source_ip = reversed_socket.dest_ip;
    reversed_socket.source_port = reversed_socket.dest_port;
    reversed_socket.dest_ip = tempS;
    reversed_socket.dest_port = tempUI;
    if (!reverse)
    {
        _sample_send_packet.setSourceTaskProxy(p.getSourceTaskProxy());
        _sample_send_packet.setDestinationTask(p.getDestinationTask());
        _sample_send_packet.setDestTaskProxy(Scnsl::Core::SocketMap::getTp(reversed_socket));
        _sample_send_packet.setDestinationNode(Scnsl::Core::SocketMap::getNode(reversed_socket.source_ip));
        _sample_send_packet.setSourceNode(p.getSourceNode());
        _sample_send_packet_rdy = true;
    }
    else
    {
        _sample_send_packet.setSourceTaskProxy(
            Scnsl::Core::SocketMap::getTp(reversed_socket));
        _sample_send_packet.setDestinationTask(p.getSourceTaskProxy()->getTask());
        _sample_send_packet.setDestTaskProxy(p.getSourceTaskProxy());
        _sample_send_packet.setDestinationNode(p.getSourceNode());
        _sample_send_packet.setSourceNode(Scnsl::Core::SocketMap::getNode(reversed_socket.dest_ip));
        // if reverse the packet used as example doesn't came from the taskproxy, 
        //some fields may not be correct 
        _sample_send_packet_rdy = false;  
    }
    _sample_send_packet.setId(p.getId());
    _sample_send_packet.setChannel(p.getChannel());
    _sample_send_packet.setReceivedPower(p.getReceivedPower());
    _sample_send_packet.setValidity(p.isValid());
    _sample_send_packet.setCommand(Core::NONE);
    _sample_send_packet.setType(Core::DATA_PACKET);
    _sample_send_packet.setValidity(p.isValid());
    Scnsl::Core::node_properties_t properties(p.getSourceProperties());
    _sample_send_packet.setSourceProperties(properties);
}

bool TcpConnection_if_t::isSampleReceivePacketRdy() const
{
    return _sample_receive_packet_rdy;
}

void TcpConnection_if_t::setSampleReceivePacket(const Packet_t & sampleReceivePacket)
{
    _sample_receive_packet.setSequenceNumber(sampleReceivePacket.getSequenceNumber());
    _sample_receive_packet.setLabel(sampleReceivePacket.getLabel());
    _sample_receive_packet.setSourceTaskProxy(sampleReceivePacket.getSourceTaskProxy());
    _sample_receive_packet.setDestTaskProxy(nullptr);
    _sample_receive_packet.setDestinationTask(sampleReceivePacket.getDestinationTask());
    _sample_receive_packet.setDestinationNode(sampleReceivePacket.getDestinationNode());
    _sample_receive_packet.setSourceNode(sampleReceivePacket.getSourceNode());
    _sample_receive_packet.setId(sampleReceivePacket.getId());
    _sample_receive_packet.setChannel(sampleReceivePacket.getChannel());
    _sample_receive_packet.setReceivedPower(sampleReceivePacket.getReceivedPower());
    _sample_receive_packet.setValidity(sampleReceivePacket.isValid());
    Scnsl::Core::node_properties_t properties(sampleReceivePacket.getSourceProperties());
    _sample_receive_packet.setSourceProperties(properties);
    _sample_receive_packet.setCommand(Core::NONE);
    _sample_receive_packet.setType(Core::DATA_PACKET);
    _sample_receive_packet_rdy = true;
}

Scnsl::Core::counter_t TcpConnection_if_t::getNextPacketNumber() const
{
    return _next_packet_waited;
}

Scnsl::Core::counter_t TcpConnection_if_t::getLastAckSend() const
{
    return _last_ack_send;
}

short TcpConnection_if_t::getDuplicateAck() const
{
    return _duplicate_ack;
}

void TcpConnection_if_t::setDuplicateAck(
    short duplicateAck)
{
    _duplicate_ack = duplicateAck;
}

bool TcpConnection_if_t::needSendAck() const
{
    return _need_send_ack;
}

bool TcpConnection_if_t::needImmediateAck() const
{
    return _immediate_ack;
}

bool TcpConnection_if_t::needRetransmission() const
{
    return _need_retransmission;
}

void TcpConnection_if_t::setRetransmission(bool needRetransmission)
{
    _need_retransmission = needRetransmission;
}

bool TcpConnection_if_t::needSynAck() const
{
    return _needSynAck;
}

bool TcpConnection_if_t::needFin() const
{
    return _needFin;
}

size_t TcpConnection_if_t::getMaxSegSize() const
{
    return _max_seg_size;
}

void TcpConnection_if_t::setMaxSegSize(size_t maxSegSize)
{
    _max_seg_size = maxSegSize;
    // congestion window initialization following standard
    if (_max_seg_size > MSS_INITIAL_BOUND_HIGH)
        _cwnd = 2 * _max_seg_size;
    else if (_max_seg_size > MSS_INITIAL_BOUND_MEDIUM)
        _cwnd = 3 * _max_seg_size;
    else
        _cwnd = 4 * _max_seg_size;
        
}

size_t TcpConnection_if_t::getCwnd() const
{
    return _cwnd;
}

void TcpConnection_if_t::setCwnd(size_t cwnd)
{
    _cwnd = cwnd;
}

size_t TcpConnection_if_t::getSstresh() const
{
    return _sstresh;
}

void TcpConnection_if_t::setSstresh(size_t sstresh)
{
    _sstresh = sstresh;
}

size_t TcpConnection_if_t::getRcvwnd() const
{
    return _rcvwnd;
}

void TcpConnection_if_t::setRcvwnd(size_t rcvwnd)
{
    _rcvwnd = rcvwnd;
    _sstresh = rcvwnd / 2;
}

void TcpConnection_if_t::setCongestionState(
    Fsm_states congestionState)
{
    _congestion_state = congestionState;
}

void TcpConnection_if_t::setSlowStart(TcpAlgs slowStart)
{
    _slow_start = TcpAlgFactory::getAlgorithm(slowStart, this);
}

void TcpConnection_if_t::setCongestionAvoidance(TcpAlgs congestionAvoidance)
{
    _congestion_avoidance = TcpAlgFactory::getAlgorithm(congestionAvoidance, this);
}

void TcpConnection_if_t::setFastRecovery(TcpAlgs fastRecovery)
{
    _fast_recovery = TcpAlgFactory::getAlgorithm(fastRecovery, this);
}

void TcpConnection_if_t::setFullHeaderSize(
    unsigned short ip_header,
    unsigned short mac_byte)
{
    _full_header_size = ip_header + mac_byte;
}

void TcpConnection_if_t::setWmemSize(size_t wmemSize)
{
    _wmem_size = wmemSize;
}

size_t TcpConnection_if_t::getAckSizeCounter() const
{
    return _ack_size_counter;
}

void TcpConnection_if_t::setAckSizeCounter(
    size_t ackSizeCounter)
{
    _ack_size_counter = ackSizeCounter;
}

size_t TcpConnection_if_t::getFlightSize() const
{
    return _flightSize;
}

void TcpConnection_if_t::setFlightSize(size_t flightSize)
{
    _flightSize = flightSize;
}

bool TcpConnection_if_t::hasPriorityPacket()
{
    return !_immediate_queue->empty();
}

TcpConnection_if_t::Packet_t TcpConnection_if_t::getPriorityPacket()
{
    auto p = _immediate_queue->front();
    _immediate_queue->pop();
    return p;
}

void TcpConnection_if_t::addPriorityPacket(Packet_t p)
{
    _immediate_queue->push(p);
}

void TcpConnection_if_t::setLimitedTransmissionEnabled(
    bool limitedTransmissionEnabled)
{
    _limited_transmission_enabled = limitedTransmissionEnabled;
}

void TcpConnection_if_t::setTcpNoDelay(bool tcpNoDelay)
{
    _tcp_no_delay = tcpNoDelay;
}

connection_states TcpConnection_if_t::getConnectionState() const
{
    return _connection_state;
}

TcpConnection_if_t::counter_t TcpConnection_if_t::getNextSendPacketAckNumber()
    const
{
    return _next_sequence_number;
}

void TcpConnection_if_t::setFastAckCount(unsigned short fastAckCount)
{
    _fast_ack_count = fastAckCount;
}

void TcpConnection_if_t::setSendAckAfterFullSegment(
    bool sendAckAfterFullSegment)
{
    _send_ack_after_full_segment = sendAckAfterFullSegment;
}

bool TcpConnection_if_t::isBlocking() const
{
    return _noblocking;
}

void TcpConnection_if_t::setNonBlock(bool blocking)
{
    _noblocking = blocking;
}

void TcpConnection_if_t::setLog(bool log)
{
    _log = log;
}

void TcpConnection_if_t::setMinRetransmissionTimeout(sc_core::sc_time rto)
{
    _minRTO = rto;
    _RTO = rto;
}

///////////////////////////
/// PRIVATE METHODS
///////////////////////////

void TcpConnection_if_t::prepareCommandPacket( Scnsl::Core::PacketCommand command,
                                                byte_t * buffer, size_t length)
{
    // send packet to upper level
    SimpleProtocolPacket_t p;
    if (buffer != nullptr)
    {
        p.setPayloadInBytes(buffer, length);
    }
    Packet_t packet(_sample_receive_packet);
    packet.setPayload(p);
    packet.setHeaderSize(0);
    packet.setType(Core::COMMAND_PACKET);
    packet.setCommand(command);
    socket_properties_t sp;
    sp.source_ip = _source_ip;
    sp.source_port = _source_port;
    sp.dest_ip = _dest_ip;
    sp.dest_port = _dest_port;
    packet.setDestTaskProxy(Scnsl::Core::SocketMap::getTp(sp));
    _receive_buffer->push(packet);
    TimeoutStruct_t ts(DATA_RECEIVE,_id,0,0,
                            sc_core::SC_ZERO_TIME + sc_core::sc_time_stamp());
    _event_queue->notify(ts);
}
