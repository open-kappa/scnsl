// -*- SystemC -*-

//
// Copyright (C) 2008-2019
// Davide Quaglia, Francesco Stefanni.
//
// This file is part of SCNSL.
//
// SCNSL is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCNSL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCNSL, in a file named LICENSE.txt. If not, see <http://www.gnu.org/licenses/>.
//

#include <sstream>
#include "scnsl/protocols/mac_802_15_4/MacEfsm_t.hh"
#include "scnsl/protocols/mac_802_15_4/MacTransitionFunctions_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// /////////////////////////////////////////////////////////////////////
// 802.15.4 Defines and consts.
// /////////////////////////////////////////////////////////////////////


// standard defines:
#define TSYMBOL 16    // Symbol time = 16 usec
#define UNIT_BACKOFF_PERIOD (20*TSYMBOL)
#define SENSING_PERIOD  (8*TSYMBOL)
#define CCA_PERIOD SENSING_PERIOD
// #define MIN_LIFS_PERIOD (40*TSYMBOL)
// #define MIN_SIFS_PERIOD (12*TSYMBOL)
#define MAC_MIN_BE 3
#define MAC_MAX_BE 53
#define MAC_MAX_CSMA_BACKOFFS 4
#define MAX_FRAME_RETRIES 3
#define PHY_SHR_DURATION (10*TSYMBOL)
#define PHY_SYMBOLS_PER_OCTET 2
#define TURNAROUND_TIME (12*TSYMBOL)
#define MAC_ACK_WAIT_DURATION (UNIT_BACKOFF_PERIOD + TURNAROUND_TIME + PHY_SHR_DURATION + 6 * PHY_SYMBOLS_PER_OCTET * TSYMBOL)



// Hardware specific constants:
/// Turn-on delay of RF interface, in TI CC243x, in usec.
#define RX_ON 450


using namespace Scnsl::Protocols::Mac_802_15_4;


// Unnamed namespace.
namespace {

  static const char * get_state_as_string( const MacEfsm_t::MAC_802_15_4_state_t state )
  {
      switch( state )
      {
          case MacEfsm_t::RADIO_OFF: return "RADIO_OFF";
          case MacEfsm_t::RX: return "RX";
          case MacEfsm_t::BACKOFF: return "BACKOFF";
          case MacEfsm_t::CCA: return "CCA";
          case MacEfsm_t::TX: return "TX";
          case MacEfsm_t::ACK_WAITING: return "ACK_WAITING";
          case MacEfsm_t::SEND_ACK: return "SEND_ACK";
          // case MacEfsm_t::TURN_FOR_ACK: return "TURN_FOR_ACK";
          // case MacEfsm_t::TURN_FOR_TX: return "TURN_FOR_TX";
          // case MacEfsm_t::TURN_FOR_RX: return "TURN_FOR_RX";
          // case MacEfsm_t::TURN_FOR_BACKOFF: return "TURN_FOR_BACKOFF";
          // case MacEfsm_t::TO_RX2: return "TO_RX2";
          default: return "ERROR";
      }
  }

} // end unnamed namespace




// /////////////////////////////////////////////////////////////////////
// MacEfsm implementation.
// /////////////////////////////////////////////////////////////////////


MacEfsm_t::MacEfsm_t( const sc_core::sc_module_name modulename,
                      MacTransitionFunctions_t * transitions,
                      MAC_802_15_4_state_t default_state,
                      bool always_on ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // State related members:
    _state( default_state ),
    _default_state( default_state ),
    _always_on( always_on ),
    // 802.15.4 MAC variables:
    _NB( 0 ),
    _BE( MAC_MIN_BE ),
    _NR( 0 ),
    // Implementation related members:
    _timeToWait( sc_core::SC_ZERO_TIME ),
    _previousTime( sc_core::SC_ZERO_TIME ),
    _transitions( transitions ),
    // Events for algorithmic management:
    _haveToSendAPacketEvent(),
    _alwaysOnEvent(),
    _newPacketArrivedEvent()
{
    _transitions->registerInternalEvents( _haveToSendAPacketEvent, _newPacketArrivedEvent );

    SC_THREAD( _nodeProcess );
}

MacEfsm_t::~MacEfsm_t()
{
    // Nothing to do.
}


bool MacEfsm_t::getAlwaysOn()
    const
{
#if (SCNSL_DBG >= 5 )
    std::stringstream ss;
    ss << "<> getAlwaysOn(): " << _always_on << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    return _always_on;
}

void MacEfsm_t::setAlwaysOn( const bool on )
{
#if (SCNSL_DBG >= 5 )
    std::stringstream ss;
    ss << "<> setAlwaysOn(): " << _always_on  << " --> " << on << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    _always_on = on;
    if ( _always_on )  _alwaysOnEvent.notify();
}


void MacEfsm_t::_nodeProcess()
{
    // \todo Check a real-life implementation to undestand what to do in case of failure.

	for (;;)
    {
        //        wait();

        // \todo Maybe a resetting mechanism could be useful...
        //       In case, check if extend the reset with a user-function fo flush
        //       implementation data.
        //         if ( reset )
        //         {
        //             // inits:
        //             _NB = 0;
        //             _BE = MAC_MIN_BE;
        //             _NR = 0;
        //             _state = _default_state;
        //             _resetTimer();
        //
        //             continue;
        //         }


        switch( _state )
        {

            case RADIO_OFF:
            {
                _transitions->setReadMode( false );
                if ( ! _always_on && ! _transitions->haveToSendAPacket() )
                {
                    wait( _haveToSendAPacketEvent | _alwaysOnEvent );
                }

                wait( RX_ON, sc_core::SC_US );

                if ( _transitions->haveToSendAPacket() )
                {
                    // Start transmitting a packet:
                    _transitions->prepareOutgoingDataPacket();
                    // RF on: prepare backoff
                    _BE = MAC_MIN_BE;
                    _NB = 0;
                    _timeToWait = sc_core::sc_time(_drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US);
                    _state = BACKOFF;

                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                else
                {
                    _state = RX;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                break;
            }

            case BACKOFF:
            {
                _transitions->setReadMode( true );
                if ( ! _transitions->newPacketArrived() )
                {
                    _previousTime = sc_core::sc_time_stamp();
                    wait( _timeToWait, _newPacketArrivedEvent );
                }

                if ( _transitions->newPacketArrived() )
                {
                    // A new packet has arrived.
                    _transitions->readPacket();
                    if ( _transitions->isAck() )
                    {
                        _transitions->disposeReadPacket();
                        _timeToWait -= (sc_core::sc_time_stamp() - _previousTime );
                    }
                    else
                    {
                        _transitions->manageReadDataPacket();
                        if ( _transitions->isAckRequiredForReceivedPacket() && ! _transitions->isReceivedPacketBroadcast() )
                        {
                            _transitions->setReadMode( false );
                            wait( TURNAROUND_TIME, sc_core::SC_US );
                            _state = SEND_ACK;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else
                        {
                            _transitions->disposeReadPacket();
                            _timeToWait -= (sc_core::sc_time_stamp() - _previousTime );
                        }
                    }
                }
                else
                {
                    // timer expired
                    _timeToWait = sc_core::sc_time( CCA_PERIOD, sc_core::SC_US );
                    _state = CCA;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }

                break;
            }

            case CCA:
            {
                _transitions->setReadMode( true );
                if ( ! _transitions->newPacketArrived() )
                {
                    _previousTime = sc_core::sc_time_stamp();
                    wait( _timeToWait, _newPacketArrivedEvent );
                }

                if ( _transitions->newPacketArrived() )
                {
                    // A new packet has arrived.
                    _transitions->readPacket();
                    if ( _transitions->isAck() )
                    {
                        _transitions->disposeReadPacket();
                        _timeToWait -= (sc_core::sc_time_stamp() - _previousTime );
                    }
                    else
                    {
                        _transitions->manageReadDataPacket();
                        if ( _transitions->isAckRequiredForReceivedPacket() && ! _transitions->isReceivedPacketBroadcast() )
                        {
                            _transitions->setReadMode( false );
                            wait( TURNAROUND_TIME, sc_core::SC_US );
                            _state = SEND_ACK;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else
                        {
                            _transitions->disposeReadPacket();
                            _timeToWait -= (sc_core::sc_time_stamp() - _previousTime );
                        }
                    }
                }
                else
                {
                    // timer expired
                    if ( _transitions->isChannelBusy() )
                    {
                        ++ _BE;
                        _BE = _BE < MAC_MAX_BE ? _BE : MAC_MAX_BE;
                        ++ _NB;

                        if ( _NB > MAC_MAX_CSMA_BACKOFFS )
                        {
                            _NR = 0;
                            _transitions->disposeSentPacket();
                            _transitions->failure( CHANNEL_ERROR );

                            if ( _transitions->haveToSendAPacket() )
                            {
                                // Start transmitting a packet:
                                _transitions->prepareOutgoingDataPacket();
                                // prepare backoff
                                _BE = MAC_MIN_BE;
                                _NB = 0;
                                _timeToWait = sc_core::sc_time(_drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US);
                                _state = BACKOFF;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                            else if ( _always_on )
                            {
                                // Switching to read mode:
                                _state = RX;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                            else
                            {
                                _state = RADIO_OFF;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                        }
                        else
                        {
                            _timeToWait = sc_core::sc_time(_drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US);
                            _state = BACKOFF;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                    }
                    else
                    {
                        _transitions->setReadMode( false );
                        wait( TURNAROUND_TIME, sc_core::SC_US );
                        _state = TX;
                        SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                    }
                }
                break;
            }

            case TX:
            {
                _transitions->sendPacket();
                // not needed: this is TLM, and when we come back from this
                // method call, the packet has already been sent
                // wait( _isSendCompletedEvent );

                if ( _transitions->isAckRequiredForSentPacket() && ! _transitions->isSentPacketBroadcast() )
                {
                    wait( TURNAROUND_TIME, sc_core::SC_US );
                    _timeToWait = sc_core::sc_time( MAC_ACK_WAIT_DURATION, sc_core::SC_US );
                    _state = ACK_WAITING;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                else
                {
                    _transitions->disposeSentPacket();

                    if ( _transitions->haveToSendAPacket() )
                    {
                        // Start transmitting a packet:
                        _transitions->prepareOutgoingDataPacket();
                        wait( TURNAROUND_TIME, sc_core::SC_US );

                        // prepare backoff
                        _BE = MAC_MIN_BE;
                        _NB = 0;
                        _timeToWait = sc_core::sc_time(_drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US);
                        _state = BACKOFF;
                        SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                    }
                    else if ( _always_on )
                    {
                        // Switching to read mode:
                        wait( TURNAROUND_TIME, sc_core::SC_US );
                        _state = RX;
                        SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                    }
                    else
                    {
                        _state = RADIO_OFF;
                        SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                    }
                }
                break;
            }

            case ACK_WAITING:
            {
                _transitions->setReadMode( true );
                if (! _transitions->newPacketArrived() )
                {
                    _previousTime = sc_core::sc_time_stamp();
                    wait( _timeToWait, _newPacketArrivedEvent );
                }

                if ( _transitions->newPacketArrived() )
                {
                    // A new packet has arrived.
                    _transitions->readPacket();
                    if ( _transitions->isAck() )
                    {
                        if ( _transitions->isExpectedAck() )
                        {
                            _NR = 0;
                            _transitions->disposeSentPacket();

                            if ( _transitions->haveToSendAPacket() )
                            {
                                // Start transmitting a packet:
                                // prepare backoff
                                _transitions->prepareOutgoingDataPacket();
                                _BE = MAC_MIN_BE;
                                _NB = 0;
                                _timeToWait = sc_core::sc_time( _drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US );
                                _state = BACKOFF;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                            else if ( _always_on )
                            {
                                // Switching to read mode:
                                _state = RX;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                            else
                            {
                                _state = RADIO_OFF;
                                SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                            }
                        }

                        _transitions->disposeReadPacket();
                    }
                    else
                    {
                        // new data packet received
                        _transitions->manageReadDataPacket();
                        if ( _transitions->isAckRequiredForReceivedPacket() && ! _transitions->isReceivedPacketBroadcast() )
                        {
                            _transitions->setReadMode( false );
                            wait( TURNAROUND_TIME, sc_core::SC_US );
                            _state = SEND_ACK;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else
                        {
                            _transitions->disposeReadPacket();
                        }
                    }
                }
                else
                {
                    if ( _NR < MAX_FRAME_RETRIES )
                    {
                        ++ _NR;
                        // prepare backoff: retransmission
                        _BE = MAC_MIN_BE;
                        _NB = 0;
                        _timeToWait = sc_core::sc_time( _drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US );
                        _state = BACKOFF;
                        SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                    }
                    else
                    {
                        _NR = 0;
                        _transitions->disposeSentPacket();
                        _transitions->failure( RETRIES_ERROR );

                        if ( _transitions->haveToSendAPacket() )
                        {
                            // Start transmitting a packet:
                            _transitions->prepareOutgoingDataPacket();
                            // prepare backoff
                            _BE = MAC_MIN_BE;
                            _NB = 0;
                            _timeToWait = sc_core::sc_time( _drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US );
                            _state = BACKOFF;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else if ( _always_on )
                        {
                            // Switching to read mode:
                            _state = RX;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else
                        {
                            _state = RADIO_OFF;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                    }
                }
                break;
            }

            case SEND_ACK:
            {
                _transitions->sendAck();
                // not needed: this is TLM, and when we come back from this
                // method call, the packet has already been sent
                //wait( _isSendCompletedEvent );

                _transitions->disposeReadPacket();
                if ( _transitions->haveToSendAPacket() )
                {
                    // Start transmitting a packet:
                    wait( TURNAROUND_TIME, sc_core::SC_US );
                    _transitions->prepareOutgoingDataPacket();
                    _BE = MAC_MIN_BE;
                    _NB = 0;
                    _timeToWait = sc_core::sc_time( _drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US );
                    _state = BACKOFF;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                else if ( _always_on )
                {
                    // Switching to read mode:
                    wait( TURNAROUND_TIME, sc_core::SC_US );
                    _state = RX;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                else
                {
                    _state = RADIO_OFF;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                break;
            }

            case RX:
            {
                _transitions->setReadMode( true );
                if ( ! _transitions->newPacketArrived() && ! _transitions->haveToSendAPacket() )
                {
                    wait( _newPacketArrivedEvent | _haveToSendAPacketEvent );
                }

                if ( _transitions->newPacketArrived() )
                {
                    _transitions->readPacket();
                    if ( _transitions->isAck() )
                    {
                        _transitions->disposeReadPacket();
                    }
                    else
                    {
                        // new data packet received
                        _transitions->manageReadDataPacket();
                        if ( _transitions->isAckRequiredForReceivedPacket() && ! _transitions->isReceivedPacketBroadcast() )
                        {
                            _transitions->setReadMode( false );
                            wait( TURNAROUND_TIME, sc_core::SC_US );
                            _state = SEND_ACK;
                            SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                        }
                        else
                        {
                            _transitions->disposeReadPacket();
                        }
                    }
                }
                else
                {
                    // Start transmitting a packet:
                    // prepare backoff
                    _transitions->prepareOutgoingDataPacket();
                    _BE = MAC_MIN_BE;
                    _NB = 0;
                    _timeToWait = sc_core::sc_time( _drawBackoff( _BE ) * UNIT_BACKOFF_PERIOD, sc_core::SC_US );
                    _state = BACKOFF;
                    SCNSL_TRACE_LOG( 3, (std::string("next = ") + get_state_as_string( _state )).c_str() );
                }
                break;
            }

            default:
            {
                _internalError();
            }
        }

        // To allow simulation progression.
        //        wait( 1, sc_core::SC_US );
    } // end of for ( ;; )
}




void MacEfsm_t::_internalError()
{
    SCNSL_TRACE_FATAL( 1, "802.15.4 internal error. Aborting." );

    std::cerr << "802.15.4 internal error. Aborting." << std::endl;
    exit( -1 );
}



unsigned int MacEfsm_t::_drawBackoff( unsigned int exp ) const

{
    // generate a random int number between 0 and 2^exp - 1
    unsigned int num = 1;

    for ( unsigned int i = 0; i < exp; i++) num = num << 1;

    unsigned int period = static_cast< unsigned int > ( ( num ) * ( rand() / ( RAND_MAX + 1.0 )) );

#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss << "<> _drawBackoff(): exp = " << exp << ", return = " << period << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    return period;
}
