// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A MAC 802.15.4 communicator.



#include "scnsl/protocols/mac_802_15_4/Mac802_15_4_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

#ifdef _MSC_VER
#pragma warning(disable:4355)
#endif

using namespace Scnsl::Protocols::Mac_802_15_4;


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////


Mac802_15_4_t::Mac802_15_4_t( const sc_core::sc_module_name modulename,
                              Node_t * node,
                              const bool ack_required,
                              const bool short_addresses ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Core::Communicator_if_t(),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Fields:
    _transitions( (static_cast<const char*>(modulename) + std::string("_transitions")).c_str(),
                  this,
                  node,
                  short_addresses ),
    _efsm( (static_cast<const char*>(modulename) + std::string("_efsm")).c_str(), &_transitions ),
    _ack_required( ack_required ),
    _receivedPacket()
{
    SC_THREAD( _readingRoutine );
}

Mac802_15_4_t::~Mac802_15_4_t()
{
    // Nohting to do.
}



// ////////////////////////////////////////////////////////////////
// Inherited methods from Communicator.
// ////////////////////////////////////////////////////////////////


void Mac802_15_4_t::setCarrier( const Channel_if_t * ch, const carrier_t c )

{
    SCNSL_TRACE_LOG( 1, ch, c );

    SCNSL_TRACE_DBG( 1, ch, c, ">> setCarrier()." );
    SCNSL_TRACE_DBG( 3, ch, c, "setCarrier(): calling transitions." );

    _transitions.setCarrier( c );

    // Propagation of carrier to upper levels not required.
    // The carrier is a lower level info hidden by the mac layer.
#if 0
    // Propagating to upper layers:
    SCNSL_TRACE_DBG( 3, ch, c, "setCarrier(): propagating to upper layers." );

    Communicator_if_t::setCarrier( ch, c );

    SCNSL_TRACE_DBG( 3, ch, c, "setCarrier() completed." );
#endif
    SCNSL_TRACE_DBG( 1, ch, c, "<< setCarrier()." );
}


Mac802_15_4_t::errorcode_t Mac802_15_4_t::send( const Packet_t & p )

{
    SCNSL_TRACE_LOG( 1, p );

    SCNSL_TRACE_DBG( 1, "<> send()." );
    SCNSL_TRACE_DBG( 3, p, "<> send()." );

    return _transitions.send( p, _ack_required );
}


Mac802_15_4_t::errorcode_t Mac802_15_4_t::receive( const Packet_t & p )

{
    SCNSL_TRACE_LOG( 1, p );

    SCNSL_TRACE_DBG( 1, ">> receive()." );
    SCNSL_TRACE_DBG( 3, p, "<> receive()." );

    _transitions.transport( p );

    SCNSL_TRACE_DBG( 1, "<< receive()." );
    return 0;
}

void Mac802_15_4_t::bindTaskProxy( const TaskProxy_if_t * /*tp*/,
                                   Communicator_if_t * /*c*/ )

{
    throw std::logic_error( "Invalid call to bindTaskProxy inside Mac802_15_4_t." );
}


void Mac802_15_4_t::bindChannel( const Channel_if_t * /*ch*/,
                                 Communicator_if_t * /*c*/ )

{
    throw std::logic_error( "Invalid call to bindChannel inside Mac802_15_4_t." );
}


// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////


void Mac802_15_4_t::transport( Packet_t & p )
{
    SCNSL_TRACE_DBG( 1, ">> transport()." );
    SCNSL_TRACE_DBG( 3, p, " transport()." );

    Communicator_if_t::send( p );

    SCNSL_TRACE_DBG( 1, "<< transport()." );
}


void Mac802_15_4_t::registerTracer( Scnsl::Tracing::Tracer_t * t )
{
    // Registering sub-modules:
    _transitions.registerTracer( t );
    _efsm.registerTracer( t );

    // Registering this module:
    Scnsl::Tracing::Traceable_base_t::registerTracer( t );
}


// ////////////////////////////////////////////////////////////////
// Routines.
// ////////////////////////////////////////////////////////////////


void Mac802_15_4_t::_readingRoutine()
{
	for (;;)
    {
        SCNSL_TRACE_DBG( 1, "<> _readingRoutine()." );

        _transitions.receive( _receivedPacket );

        SCNSL_TRACE_DBG( 3, _receivedPacket, "<> _readingRoutine(): received." );
        Communicator_if_t::receive( _receivedPacket );

        SCNSL_TRACE_DBG( 3, _receivedPacket, "<> _readingRoutine(): forwarding." );
    }
}
