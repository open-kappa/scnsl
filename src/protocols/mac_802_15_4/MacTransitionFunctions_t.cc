// -*- SystemC -*-

//
// Copyright (C) 2008-2019
// Davide Quaglia, Francesco Stefanni.
//
// This file is part of SCNSL.
//
// SCNSL is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCNSL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCNSL, in a file named LICENSE.txt. If not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>
#include <sstream>
#include "scnsl/protocols/mac_802_15_4/Mac802_15_4_t.hh"
#include "scnsl/protocols/mac_802_15_4/MacTransitionFunctions_t.hh"


using namespace Scnsl::Protocols::Mac_802_15_4;



// //////////////////////////////////////////////////////////////////////////////
// Queues management macros.
// //////////////////////////////////////////////////////////////////////////////


// The ack packet total size:
#define ACK_PACKET_SIZE 11


// //////////////////////////////////////////////////////////////////////////////
// Constructor and destructor.
// //////////////////////////////////////////////////////////////////////////////

MacTransitionFunctions_t::MacTransitionFunctions_t(
    const sc_core::sc_module_name modulename,
    Mac802_15_4_t * communicator,
    Node_t * node,
    const bool short_addresses ):
    // Parents:
    sc_core::sc_module( modulename ),
    Scnsl::Tracing::Traceable_base_t( modulename ),
    // Constants:
    _MAX_PAYLOAD( size_t(short_addresses? 116 : 104 )),
    _HEADER_SIZE( size_t(short_addresses? 17 : 29)),
    // Simulation variables:
    _sequence_number( 0 ),
    _last_sequence_number_transmitted( static_cast< unsigned int > ( -1 ) ),
    _error_code( 0 ),
    _channel_busy( false ),
    _dataOutput( communicator ),
    // Algorithmic support members:
    _in_set( false ),
    _out_set( false ),
    _new_packet_arrived( false ),
    _new_ack_arrived( false ),
    _packet_readable( false ),
    _new_packet_send( false ),
    _read_mode_on( false ),
    _recvSeqNumbers(),
    _inPacket(),
    _outPacket(),
    _ack(),
    _thisNode( node ),
    // Events:
    _incomingQueueNotEmpty(),
    _outgoingQueueNotFull(),
    _newPacketTransmission(),
    _ackWaitingCompleted(),
    _haveToSendAPacketEvent( nullptr ),
    _newPacketArrivedEvent( nullptr )
{
    _ack.setHeaderSizeInBytes( ACK_PACKET_SIZE );
    _ack.setAck( true );
    _ack.setAckRequired( false );
}

MacTransitionFunctions_t::~MacTransitionFunctions_t()
{
    // Nothing to do.
}


// //////////////////////////////////////////////////////////////////////////////
// Application-oriented interface functions.
// //////////////////////////////////////////////////////////////////////////////


MacTransitionFunctions_t::errorcode_t MacTransitionFunctions_t::send(
    const Packet_t & p, const bool ack_required )
{
    SCNSL_TRACE_DBG( 1, p, ">> send().");

    const unsigned int THIS_PACKET_SEQUENCE_NUMBER = _sequence_number;

    if ( _new_packet_send )
    {
        wait( _outgoingQueueNotFull );
    }

    // Encapsulating:
    if ( p.getSizeInBytes() > _MAX_PAYLOAD )
    {
        return 1;
    }

    _outPacket.setHeaderSizeInBytes( _HEADER_SIZE );
    _outPacket.setPayload( *p.getPayload() );
    _outPacket.setAck( false );
    _outPacket.setAckRequired( ack_required );
    _outPacket.setSequenceNumber( THIS_PACKET_SEQUENCE_NUMBER );
    _outPacket.setSourceTaskProxy( p.getSourceTaskProxy() );
    _outPacket.setDestinationTask( p.getDestinationTask() );
    _outPacket.setDestinationNode( p.getDestinationNode() );
    _outPacket.setChannel( p.getChannel() );
    _outPacket.setId( p.getId() );

    errorcode_t ret = 0;
    _new_packet_send = true;

    _haveToSendAPacketEvent->notify();

    if ( ack_required && _outPacket.getDestinationNode() != nullptr )
    {
        SCNSL_TRACE_DBG( 3, p, "send(): ack waiting.");

        // Waiting this packet transmission:
		for (;;)
        {
            wait( _newPacketTransmission );
            if ( _last_sequence_number_transmitted ==  THIS_PACKET_SEQUENCE_NUMBER )
            {
                break;
            }
        }

        ret = _error_code;

        if ( ret == 0 )
        {
            // Waiting the ack:
            wait( _ackWaitingCompleted );
            ret = _error_code;
        }
    }

    if (ret == 0) ++ _sequence_number;
    // Resetting _error_code var:
    _error_code = 0;

    SCNSL_TRACE_DBG( 1, p, "<< send().");
    return ret;
}


MacTransitionFunctions_t::errorcode_t MacTransitionFunctions_t::receive( Packet_t & p )
{
    SCNSL_TRACE_DBG( 1, p, ">> receive().");

    if ( ! _packet_readable )
    {
        wait( _incomingQueueNotEmpty );
    }

    p = _inPacket;
    _packet_readable = false;

    SCNSL_TRACE_DBG( 1, p, "<< receive().");

    return 0;
}




// //////////////////////////////////////////////////////////////////////////////
// Communication functions from lower layers.
// //////////////////////////////////////////////////////////////////////////////


void MacTransitionFunctions_t::setCarrier( const carrier_t c )
{
#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss << "<> setCarrier(): " << c << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    _channel_busy = c;
}


void MacTransitionFunctions_t::transport( const Packet_t & pck )
{
    SCNSL_TRACE_DBG( 1, pck, ">> transport()" );

    if ( ! _read_mode_on )
    {
        // not in read mode...let's ignore the packet
        SCNSL_TRACE_DBG( 1, pck, "<< transport(): _read_mode_on = false." );
        return;
    }

    // General checks:
    if ( pck.getDestinationNode() != _thisNode && pck.getDestinationNode() != nullptr )
    {
        // not for me
        SCNSL_TRACE_LOG( 3, pck, "<< transport(): invalid destination --> dropped." );
        return;
    }
    if ( _in_set )
    {
        // something strange...ok let's assume a sort of queue full...
        SCNSL_TRACE_LOG( 3, pck, "<< transport(): incoming queue full --> dropped." );
        return;
    }

    // @TODO insert the FCS check.

    const unsigned int BYTES = pck.getSizeInBytes();

    if ( pck.isAck() )
    {
        if ( BYTES != ACK_PACKET_SIZE )
        {
            // truncated:
            SCNSL_TRACE_LOG( 1, pck, "<< transport(): ack truncated --> dropped." );
            return;
        }

        _inPacket = pck;
        _new_ack_arrived = true;
        _newPacketArrivedEvent->notify();

        SCNSL_TRACE_DBG( 1, pck, "<< transport()" );
        return;
    }

    // A data packet:

    if ( BYTES < _HEADER_SIZE )
    {
        // truncated:
        SCNSL_TRACE_DBG( 1, pck, "<< transport(): truncated --> dropped." );
        return;
    }

    if ( BYTES - _HEADER_SIZE != pck.getPayloadSizeInBytes() )
    {
        // unexpected size!
        SCNSL_TRACE_DBG( 1, pck, "<< transport(): unexpected size --> dropped." );
        return;
    }

    _inPacket = pck;
    _new_packet_arrived = true;
    _newPacketArrivedEvent->notify();

    SCNSL_TRACE_DBG( 1, pck, "<< transport()" );
}



// ////////////////////////////////////////////////////////////////
// Configuration functions.
// ////////////////////////////////////////////////////////////////


void MacTransitionFunctions_t::registerInternalEvents( sc_core::sc_event & haveToSendAPacketEvent, sc_core::sc_event & newPacketArrivedEvent )
{
    SCNSL_TRACE_DBG( 5, "<> registerInternalEvents()." );

    _haveToSendAPacketEvent = & haveToSendAPacketEvent;
    _newPacketArrivedEvent = & newPacketArrivedEvent;
}


void MacTransitionFunctions_t::setReadMode( const bool read_mode_on )
{
#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss << "<> setReadMode(): " << _read_mode_on << " --> " << read_mode_on << ".";
    SCNSL_TRACE_DBG( 5, ss.str().c_str() );
#endif

    _read_mode_on = read_mode_on;
}




// ////////////////////////////////////////////////////////////////
// Receiving functions.
// ////////////////////////////////////////////////////////////////


void MacTransitionFunctions_t::readPacket()
{
    // Check if CORRECT packet FOR US is truly arrived.
    if ( ! _new_packet_arrived && ! _new_ack_arrived ) return;
    SCNSL_TRACE_DBG( 3, _inPacket, "<> readPacket()" );

    // Now we are sure it is for us and it is correct.
    _new_packet_arrived = false;
    _new_ack_arrived = false;
    _in_set = true;
}


void MacTransitionFunctions_t::manageReadDataPacket()
{
    if ( _in_set )
    {
        SCNSL_TRACE_DBG( 3, _inPacket, "<> manageReadDataPacket()" );

        SeqnoMap_t::iterator i = _recvSeqNumbers.find( _inPacket.getSourceNode() );
        if ( i != _recvSeqNumbers.end() && i->second == _inPacket.getSequenceNumber() )
        {
            // Dup pck.
            SCNSL_TRACE_LOG( 3, _inPacket, "<> manageReadDataPacket(): duplicated --> dropped." );
            return;
        }
        _recvSeqNumbers[ _inPacket.getSourceNode() ] = _inPacket.getSequenceNumber();

        _incomingQueueNotEmpty.notify();
        _packet_readable = true;

    }
}


void MacTransitionFunctions_t::disposeReadPacket()
{
    SCNSL_TRACE_DBG( 5, _inPacket, "<> disposeReadPacket()." );
    _in_set = false;
}



// ////////////////////////////////////////////////////////////////
// Sending functions.
// ////////////////////////////////////////////////////////////////


void MacTransitionFunctions_t::prepareOutgoingDataPacket()
{
    if ( ! _out_set )
    {
        SCNSL_TRACE_DBG( 3, _outPacket, "<> prepareOutgoingDataPacket()." );

        _out_set = true;
        _last_sequence_number_transmitted = _outPacket.getSequenceNumber();
        // resetting the errror_code:
        _error_code = 0;
    }
}


void MacTransitionFunctions_t::sendPacket()
{
    if ( ! _out_set )
    {
        SCNSL_TRACE_FATAL( 1, "<> sendPacket(): out not set." );
        exit( -2 );
    }

    SCNSL_TRACE_DBG( 3, _outPacket, "<> sendPacket()." );

    _newPacketTransmission.notify();

    _dataOutput->transport( _outPacket );
}


void MacTransitionFunctions_t::disposeSentPacket()
{
    if ( _out_set )
    {
        SCNSL_TRACE_DBG( 3, "<> disposeSentPacket()." );

        _out_set = false;
        _new_packet_send = false;
        _outgoingQueueNotFull.notify();
        wait( sc_core::SC_ZERO_TIME );
    }
}


void MacTransitionFunctions_t::sendAck()
{
    if ( ! _in_set )
    {
        SCNSL_TRACE_FATAL( 1, "<> sendAck()." );
        exit( -3 );
    }

    _ack.setSourceNode( _inPacket.getDestinationNode() );
    _ack.setDestinationNode( _inPacket.getSourceNode() );
    _ack.setSequenceNumber( _inPacket.getSequenceNumber() );
    _ack.setChannel( _inPacket.getChannel() );
    _ack.setId( Packet_t::get_new_packet_id() );

    // _newPacketTransmission.notify();
    // _incomingQueueNotEmpty.notify();
    // _packet_readable = true;

    SCNSL_TRACE_DBG( 1, _ack, "sendAck()" );

    _dataOutput->transport( _ack );
}



bool MacTransitionFunctions_t::haveToSendAPacket() const
{
    const bool ret = _new_packet_send;

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> haveToSendAPacket(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}


// ////////////////////////////////////////////////////////////////
// Check functions.
// ////////////////////////////////////////////////////////////////


bool MacTransitionFunctions_t::isReceivedPacketBroadcast() const
{
    const bool ret = _in_set && ( _inPacket.getDestinationNode() == nullptr );

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isReceivedPacketBroadcast(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}



bool MacTransitionFunctions_t::isAckRequiredForReceivedPacket() const
{
    const bool ret =  _in_set
    && _inPacket.isAckRequired();

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isAckRequiredForReceivedPacket(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    if ( ! ret )
    {
        _incomingQueueNotEmpty.notify();
        _packet_readable = true;
    }
    return ret;
}


bool MacTransitionFunctions_t::newPacketArrived() const
{
    const bool ret = _new_packet_arrived || _new_ack_arrived;

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> newPacketArrived(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}



bool MacTransitionFunctions_t::isAckRequiredForSentPacket() const
{
    const bool ret = _outPacket.isAckRequired();

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isAckRequiredForSentPacket(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}



bool MacTransitionFunctions_t::isSentPacketBroadcast() const
{
    const bool ret = (_outPacket.getDestinationNode() == nullptr);

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isSentPacketBroadcast(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}



bool MacTransitionFunctions_t::isAck() const
{
    const bool ret =  _in_set
    && _inPacket.isAck();

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isAck(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return ret;
}



bool MacTransitionFunctions_t::isExpectedAck() const
{
    const bool ret = _in_set
    && ( _outPacket.getSequenceNumber() == _inPacket.getSequenceNumber() );

#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isExpectedAck(): " << ret << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    if ( ret )
    {
        _error_code = 0;
        _ackWaitingCompleted.notify();
    }
    return ret;
}



bool MacTransitionFunctions_t::isChannelBusy() const
{
#if (SCNSL_DBG >= 3)
    std::stringstream ss;
    ss << "<> isChannelBusy(): " << _channel_busy << ".";
    SCNSL_TRACE_DBG( 3, ss.str().c_str() );
#endif

    return _channel_busy;
}



// ////////////////////////////////////////////////////////////////
// Error managing functions.
// ////////////////////////////////////////////////////////////////



void MacTransitionFunctions_t::failure( const Mac_802_15_4_error_t error_code )
{
    SCNSL_TRACE_DBG( 3, "<> failure()." );

    _error_code = error_code;
    _ackWaitingCompleted.notify();
    _newPacketTransmission.notify();
}
