// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// TLM task proxy.

#include <sstream>
#include "scnsl/core/SimpleProtocolPacket_t.hh"
#include "scnsl/tlm/TlmTask_if_t.hh"
#include "scnsl/tlm/TlmTaskProxy_t.hh"
#include "scnsl/core/SocketMap.hh"

using Scnsl::Tlm::TlmTaskProxy_t;
using namespace Scnsl::Setup;
using namespace Scnsl::Core;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

TlmTaskProxy_t::TlmTaskProxy_t(
    const sc_core::sc_module_name modulename,
    Scnsl::Core::Task_if_t * t,
    std::string bindIdentifier,
    Scnsl::Core::Channel_if_t * ch )
    :
    // Parents:
    Scnsl::Core::TaskProxy_if_t( modulename, t, ch ),
    tlm::tlm_fw_transport_if<>(),
	tlm::tlm_bw_transport_if<>(),
    // Sockets:
    initiator( "initiator" ),
    target( "target" ),
    // Fields:
    _payload(),
    _packet(),
    _delay( sc_core::SC_ZERO_TIME ),
    _carrier( false ),
    _id( 0 ),
	_taskId(bindIdentifier)
{
    // Checking the type binding:
    TlmTask_if_t * task = dynamic_cast< TlmTask_if_t * >( _task );
    if ( task == nullptr )
    {
        ;
    }

    // Setting this only once:
    _packet.setSourceTaskProxy( this );
    _packet.setChannel( _channel );

    // Binding:
    target.bind( *this );
	initiator.bind( *this );
    task->bindTaskProxy( this,_taskId );
}




TlmTaskProxy_t::~TlmTaskProxy_t()
{
    // Nothing to do.
}



bool TlmTaskProxy_t::get_direct_mem_ptr( tlm::tlm_generic_payload & /*p*/, tlm::tlm_dmi & /*dmi_data*/ )
{
	return false;
}


tlm::tlm_sync_enum TlmTaskProxy_t::nb_transport_fw(tlm::tlm_generic_payload & /*p*/, tlm::tlm_phase& /*phase*/, sc_core::sc_time & /*t*/)
{
	return tlm::TLM_COMPLETED;
}


unsigned int TlmTaskProxy_t::transport_dbg(tlm::tlm_generic_payload & /*p*/)
{
	return 0;
}


void TlmTaskProxy_t::invalidate_direct_mem_ptr(sc_dt::uint64 /*start_range*/, sc_dt::uint64 /*end_range*/)
{ }

tlm::tlm_sync_enum TlmTaskProxy_t::nb_transport_bw(tlm::tlm_generic_payload & /*p*/, tlm::tlm_phase& /*phase*/, sc_core::sc_time & /*t*/)
{
	return tlm::TLM_COMPLETED;
}


// ////////////////////////////////////////////////////////////////
// Communicator interface methods.
// ////////////////////////////////////////////////////////////////

void TlmTaskProxy_t::setCarrier( const Scnsl::Core::Channel_if_t * /*ch*/, const carrier_t c )
{
    _carrier = c;

    _payload.set_command(Scnsl::Tlm::tlm_command_t(CARRIER_COMMAND));
    _payload.set_data_ptr( reinterpret_cast< unsigned char * >(&_carrier) );
    _payload.set_data_length( sizeof( carrier_t ) );
    _payload.set_address( _id );

    initiator->b_transport( _payload, _delay );
}

TlmTaskProxy_t::errorcode_t TlmTaskProxy_t::receive( const Packet_t & p )
{
    //if destination taskproxy is set check if this is the correct one

    if (p.getDestTaskProxy() != nullptr && p.getDestTaskProxy() != this)
        return 0;
    
    if (p.getDestTaskProxy() == nullptr)
    {
        socket_properties_t sp;
        try{
            sp = Scnsl::Core::SocketMap::getSocket(p.getSourceTaskProxy());
            
            //check if this tp is in the same multicast group identified
            // by incoming packet destiation IP and port. If not return, 
            // otherwise receive the packet
            if ( ! Scnsl::Core::SocketMap::isTpMulticast(this,sp.dest_ip,sp.dest_port))
                return 0;
            }
            catch(std::runtime_error e)
            {
                //nothing to do, the message comes from a non lv4 application
            }
        //get source socket
        
    }
    int size = p.getPayloadSizeInBytes();
    if (p.getType() == COMMAND_PACKET)
    {
        size ++;
        _payload.set_command(Scnsl::Tlm::tlm_command_t(COMMAND_PAYLOAD));
    }
    else
        _payload.set_command(Scnsl::Tlm::tlm_command_t(PACKET_COMMAND));

    Packet_t::byte_t b[size];
    int start_index = 0;
    if (p.getType()==COMMAND_PACKET)
    {
        b[0]=p.getCommand();
        start_index++;
    }
    memcpy(b+start_index, p.getInnerPayload(),p.getInnerSizeInBytes());
    _payload.set_data_ptr( const_cast< unsigned char * >( b ) );
    _payload.set_data_length( p.getPayload()->getInnerSizeInBytes() );
    _payload.set_address( _id );

    data_extension_t ext;
    ext.label = p.getLabel();
    ext.power = p.getReceivedPower();
    _payload.set_extension( &ext );
    initiator->b_transport( _payload, _delay );
    _payload.set_extension( static_cast<Scnsl::Core::data_extension_t *> (nullptr) );
    return 0;
}


// ////////////////////////////////////////////////////////////////
// TLM interface methods.
// ////////////////////////////////////////////////////////////////


void TlmTaskProxy_t::b_transport(
    tlm::tlm_generic_payload & p, sc_core::sc_time & t )
{

    _packet.setDestinationTask( _destinationTask );
    _packet.setId( Packet_t::get_new_packet_id() );
    _packet.setSourceTaskProxy(this);
    Scnsl::Core::data_extension_t * ext;
    p.get_extension( ext );
    _packet.setLabel( ext->label );
    SimpleProtocolPacket_t packet ;
    if (p.get_command() == Scnsl::Tlm::COMMAND_PAYLOAD)
    {
        _packet.setType(COMMAND_PACKET);
        _packet.setCommand(static_cast<PacketCommand>(p.get_data_ptr()[0]));
    }
    else
    {
        _packet.setType(DATA_PACKET);
        packet.setPayloadInBytes( p.get_data_ptr(), p.get_data_length());
    }
    _packet.setPayload(packet);
    SCNSL_TRACE_LOG( 3, _packet, "<> b_transport()" );
    send( _packet );

    p.set_response_status( tlm::TLM_OK_RESPONSE );

    // To suppress compiler warnings:
    t += sc_core::SC_ZERO_TIME;
}



// ////////////////////////////////////////////////////////////////
// Support methods.
// ////////////////////////////////////////////////////////////////

void TlmTaskProxy_t::setId( const tlm_taskproxy_id_t id )
{
#if (SCNSL_DBG >= 5)
    std::stringstream ss;
    ss << "<> setId(): "<< _id << " --> " << id << ".";
    SCNSL_TRACE_LOG( 5, ss.str().c_str() );
#endif

    _id = id;
}
