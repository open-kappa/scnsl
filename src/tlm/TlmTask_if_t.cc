// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A TLM task.

#include <sstream>

#include "scnsl/tlm/TlmTaskProxy_t.hh"
#include "scnsl/tlm/TlmTask_if_t.hh"


using Scnsl::Tlm::TlmTask_if_t;
using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using Scnsl::Tlm::TlmTaskProxy_t;

// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

TlmTask_if_t::~TlmTask_if_t()
{
    // Calling destructors for all sockets.
    // NOTE: reverse order not truly required here,
    // while it is a must in the STL.
    for ( unsigned int i = _PROXIES_NUMBER; i != 0; )
    {
        --i;

        initiators[ i ].~InitiatorSocket_t();
        targets[ i ].~TargetSocket_t();
    }

    // Deallocating the memory.
    ::operator delete[] ( initiators );
    ::operator delete[] ( targets );

    // Ensuring that automatic destructor call in case of total simulation
    // timeout does not frees stack allocated extensions.
    // To produce the bug, comment this line and run test_802_15_4_tlm.
    // See also: TlmTask_if_t::send()
    _payload.set_extension(static_cast<Scnsl::Core::data_extension_t *>(nullptr));
}


// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

TlmTask_if_t::TlmTask_if_t( sc_core::sc_module_name modulename,
                            const task_id_t id,
                            Scnsl::Core::Node_t * n,
                            const size_t proxies )
    :
    // Parents:
    Scnsl::Core::Task_if_t( modulename, id, n ),
    tlm::tlm_fw_transport_if<>(),
    tlm::tlm_bw_transport_if<>(),
    // Sockets:
    initiators( nullptr ),
    targets( nullptr ),
    // Fields:
    _bounded_proxies( 0 ),
    _PROXIES_NUMBER( proxies ),
    _payload(),
    _delay( sc_core::SC_ZERO_TIME )
{
    if ( _PROXIES_NUMBER == 0 )
    {
        throw std::invalid_argument( "Invalid number of proxies." );
    }

    // Initializing in two steps, to be able to naming the sockets:

    initiators = static_cast< InitiatorSocket_t * >(
        operator new [] ( _PROXIES_NUMBER * sizeof( InitiatorSocket_t ) ));
    targets = static_cast< TargetSocket_t * >(
        operator new [] ( _PROXIES_NUMBER * sizeof( TargetSocket_t ) ));

    for ( unsigned int i = 0; i < _PROXIES_NUMBER; ++i )
    {
        std::string s;
        std::stringstream ss;
        ss << i;
        ss >> s;

        // Calling sockets constructors.
        new (initiators + i) InitiatorSocket_t( ("initiator" + s).c_str() );
        new (targets + i) TargetSocket_t( ("target" + s).c_str() );

        // Binding:
        targets[i].bind(*this);
        initiators[i].bind(*this);
    }
}


bool TlmTask_if_t::get_direct_mem_ptr( tlm::tlm_generic_payload & /*p*/, tlm::tlm_dmi & /*dmi_data*/ )
{
	return false;
}


tlm::tlm_sync_enum TlmTask_if_t::nb_transport_fw(tlm::tlm_generic_payload & /*p*/, tlm::tlm_phase& /*phase*/, sc_core::sc_time & /*t*/)
{
	return tlm::TLM_COMPLETED;
}


unsigned int TlmTask_if_t::transport_dbg(tlm::tlm_generic_payload & /*p*/)
{
	return 0;
}


void TlmTask_if_t::invalidate_direct_mem_ptr( sc_dt::uint64 /*start_range*/, sc_dt::uint64 /*end_range*/ )
{ }

tlm::tlm_sync_enum TlmTask_if_t::nb_transport_bw(tlm::tlm_generic_payload & /*p*/, tlm::tlm_phase& /*phase*/, sc_core::sc_time & /*t*/)
{
	return tlm::TLM_COMPLETED;
}

void TlmTask_if_t::bindTaskProxy( TlmTaskProxy_t * tp, std::string bindIdentifier )
{
    SCNSL_TRACE_DBG( 5, "<> bindTaskProxy().");

    if ( _bounded_proxies >= _PROXIES_NUMBER )
    {
        throw std::logic_error( "Already bounded all available proxies." );
    }

    IdentifierNumber_t newId = static_cast<IdentifierNumber_t>(_bounded_proxies);
    std::string newIdKey;
    std::stringstream ss;
    ss << newId;
    ss >> newIdKey;

    if (bindIdentifier != "") newIdKey = bindIdentifier;

    if(_bindIdMap.find(newIdKey) != _bindIdMap.end())
    {
        throw std::logic_error( "This bind Identifier is already used. Please check bsb bind Identifier attribute." );
    }
    _proxies[newIdKey] = tp;

	tp->initiator( this->targets[ _bounded_proxies ] );
	this->initiators[ _bounded_proxies ]( tp->target );
    tp->setId( static_cast<tlm_taskproxy_id_t> (_bounded_proxies) );

    ++ _bounded_proxies;
    _bindIdMap[newIdKey] = newId;
}

void TlmTask_if_t::send(const std::string & tpKey, byte_t * buffer, const size_t s, const label_t label )
{
    if(_bindIdMap.find(tpKey) == _bindIdMap.end())
    {
        throw std::logic_error( "This bind identifier has not been defined." );
    }

    const tlm_taskproxy_id_t tp = static_cast<tlm_taskproxy_id_t> (_bindIdMap.at(tpKey));
    send(tp, buffer, s, label);
}

void TlmTask_if_t::send(const tlm_taskproxy_id_t tp, byte_t * buffer, const size_t s, const label_t label )
{
    if(tp >= _PROXIES_NUMBER)
    {
        throw std::logic_error( "This proxy identifier has not been defined." );
    }

    SCNSL_TRACE_DBG( 5, "<> send().");
    SCNSL_TRACE_LOG( 3, "<> send().");

    _payload.set_command(Scnsl::Tlm::tlm_command_t(PACKET_COMMAND));
    _payload.set_data_ptr( buffer );
    _payload.set_data_length( s );
    _payload.set_address( _ID );

    Scnsl::Core::data_extension_t ext;
    ext.label = label;
    _payload.set_extension( &ext );
    initiators[ tp ]->b_transport( _payload, _delay );
    _payload.set_extension( static_cast<Scnsl::Core::data_extension_t *> (nullptr) );
}


// ////////////////////////////////////////////////////////////////
// Interface methods.
// ////////////////////////////////////////////////////////////////

TlmTask_if_t::size_t TlmTask_if_t::getProxiesNumber()
    const
{
    SCNSL_TRACE_DBG( 5, "<> getProxiesNumber().");
    return _PROXIES_NUMBER;
}

TlmTask_if_t::size_t TlmTask_if_t::getBoundedProxiesNumber()
    const
{
    SCNSL_TRACE_DBG( 5, "<> getBoundedProxiesNumber().");
    return _bounded_proxies;
}
