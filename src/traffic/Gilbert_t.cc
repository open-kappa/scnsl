// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Gilbert model traffic.


#include <time.h>
#include <sstream>
#include "scnsl/traffic/Gilbert_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using Scnsl::Traffic::Gilbert_t;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

Gilbert_t::Gilbert_t( sc_core::sc_module_name modulename,
					  const task_id_t id,
					  Scnsl::Core::Node_t * n,
					  const size_t proxies,
					  label_t label,
					  const size_t pktSize,
					  const sc_core::sc_time genTime,
					  double alpha,
					  double beta )
    :
    // Parents:
    Scnsl::Traffic::Traffic_if_t( modulename, id, n, proxies, label ),
    // Fields:
	_pktSize( pktSize ),
	_genTime( genTime ),
    _enable( false ),
	_activationEvent(),
	_alpha( alpha ),
	_beta( beta )
{
 	if ( _pktSize == 0 )
    {
        throw std::invalid_argument( "Invalid packet size" );
    }

    if ( _genTime == sc_core::SC_ZERO_TIME )
    {
        throw std::invalid_argument( "Invalid time generation" );
    }

 	if ( _alpha < 0 || _alpha > 1 || _beta < 0 || _beta > 1 )
    {
        throw std::invalid_argument( "Invalid probabilistic parameters" );
    }

	SC_THREAD( writingProcess );
}


Gilbert_t::~Gilbert_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Traffic interface methods.
// ////////////////////////////////////////////////////////////////

void Gilbert_t::enable()
{
	_enable = true;
    if (!sc_core::sc_is_running()) return;
	_activationEvent.notify();
}


void Gilbert_t::disable()
{
	_enable = false;
    if (!sc_core::sc_is_running()) return;
	_activationEvent.notify();
}

// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Gilbert_t::writingProcess()
{
    // Randomize the seed
    srand ( static_cast< unsigned int >( time( nullptr ) ) );

    // Starting from SEND state
    bool send_state = true;

	// Allocating memory for _data
	char * data = new char[ _pktSize ];
	data[ _pktSize -1 ] = '\0';
    const std::string tp = "0";

    double coin;
    double totalCounter = 0;
    double sendCounter = 0;

	for (;;)
    {
		while( _enable )
		{
		    totalCounter++;

		    // SEND state
		    if ( send_state )
		    {
		        // Flip a coin
		        coin = static_cast< double >( ( rand() % 100 + 1 ) ) / 100;

				if ( coin < ( 1 - _alpha ) )
		        {
		            // Stay in this state and send
		            sendCounter ++;

					// Packet generation
					for(unsigned int j = 0; j < _pktSize - 1; j++)
					{
						data[ j ] = static_cast< char >( rand()%25 + 65 );
					}

					#if ( SCNSL_LOG >= 1 )
				    std::stringstream ss;
				    ss << "data sent: " << data << " bytes: " << _pktSize;
				    SCNSL_TRACE_LOG( 1, ss.str().c_str() );
					#endif

		            TlmTask_if_t::send( tp, reinterpret_cast< byte_t * >( data ), _pktSize, _label );
		        }
		        else
		            // Changing state
		            send_state = false;
		    }
		    // NO_SEND state
		    else
		    {
		        // Flip a coin
		        coin = static_cast< double >( ( rand() % 100 + 1 ) ) / 100;

				if( coin >= ( 1 - _beta ) )
		        {
		            // Changing state and send
		            send_state = true;
		            sendCounter++;

					#if ( SCNSL_LOG >= 1 )
				    std::stringstream ss;
				    ss << "data sent: " << data << " bytes: " << _pktSize;
				    SCNSL_TRACE_LOG( 1, ss.str().c_str() );
					#endif

		            TlmTask_if_t::send( tp, reinterpret_cast< byte_t * >( data ), _pktSize, _label );
		        }
		    }
			if( ! _enable ) break;
			wait( _genTime, _activationEvent );
		}
		wait( _activationEvent );
    }
}
