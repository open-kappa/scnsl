// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// On-Off traffic.


#include <sstream>
#include "scnsl/traffic/OnOff_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using Scnsl::Traffic::OnOff_t;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

OnOff_t::OnOff_t( sc_core::sc_module_name modulename,
              		const task_id_t id,
              		Scnsl::Core::Node_t * n,
              		const size_t proxies,
					label_t label,
			  		const size_t pktSize,
			  		const sc_core::sc_time genTime,
					const sc_core::sc_time on,
					const sc_core::sc_time off )
    :
    // Parents:
    Scnsl::Traffic::Traffic_if_t( modulename, id, n, proxies, label ),
    // Fields:
	_pktSize( pktSize ),
	_genTime( genTime ),
    _enable( false ),
	_activationEvent(),
	_on( on ),
	_off( off )
{
 	if ( _pktSize <= 0 )
    {
        throw std::invalid_argument( "Invalid packet size" );
    }

    if ( _genTime == sc_core::SC_ZERO_TIME )
    {
        throw std::invalid_argument( "Invalid time generation" );
    }

	SC_THREAD( writingProcess );
}


OnOff_t::~OnOff_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Traffic interface methods.
// ////////////////////////////////////////////////////////////////

void OnOff_t::enable()
{
	_enable = true;
    if (!sc_core::sc_is_running()) return;
	_activationEvent.notify();
}


void OnOff_t::disable()
{
	_enable = false;
    if (!sc_core::sc_is_running()) return;
	_activationEvent.notify();
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void OnOff_t::writingProcess(){
	// Allocating memory for _data
	char * data = new char[ _pktSize ];
	data[ _pktSize -1 ] = '\0';

    const std::string tp = "0";
	sc_core::sc_time t;
	sc_core::sc_time realOff;


	for (;;)
	{

		t = sc_core::sc_time_stamp();

		while( _enable )
		{
			// Check if the transmission period has exceed the on time.
			if ( (sc_core::sc_time_stamp() - t) >= _on )
			{
				// Calculate the real off period.
				// If the transmission has exceed the off+on time then real off is reset to zero
				// else real off is equal to the remaining time to reach the on+off period.
				if( _off + _on <= ( sc_core::sc_time_stamp() - t))
					realOff = sc_core::SC_ZERO_TIME;
				else
					realOff = _off + _on - (sc_core::sc_time_stamp() - t );
				wait( realOff, _activationEvent );
				t = sc_core::sc_time_stamp();
			}

			// Packet generation
			for(unsigned int j = 0; j < _pktSize - 1; j++)
			{
				data[ j ] = static_cast< char >( rand()%25 + 65 );
			}

		    // Since we are tracing a complex message,
		    // we wrap the log with the macro, to avoid
		    // unuseful overhead.
	#if ( SCNSL_LOG >= 1 )
		    std::stringstream ss;
		    ss << "data sent: " << data << " bytes: " << _pktSize;
		    SCNSL_TRACE_LOG( 1, ss.str().c_str() );
	#endif


			TlmTask_if_t::send( tp, reinterpret_cast< byte_t *>( data ), _pktSize, _label );

			wait( _genTime, _activationEvent );
			if( ! _enable ) break;
		}
		wait( _activationEvent );
	}
}
