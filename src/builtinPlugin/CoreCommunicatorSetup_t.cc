// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Core communicator setup class.



#include "scnsl/builtinPlugin/CoreCommunicatorSetup_t.hh"


using namespace Scnsl::BuiltinPlugin;



CoreCommunicatorSetup_t::CoreCommunicatorSetup_t():
    // Parents:
    Scnsl::Setup::CommunicatorSetup_base_t(),
    // Fields:
    type( MAC_802_15_4 ),
    queueSend( FIFO ),
    queueReceive( FIFO ),
    capacitySend( 0 ),
    capacityReceive( 0 ),
    policySend( STRONG_PRIORITY ),
    policyReceive( STRONG_PRIORITY ),
    numberSend( 1 ),
    numberReceive( 1 ),
    weightSend( nullptr ),
    weightReceive( nullptr ),
    ack_required( true ),
    short_addresses( false ),
    node( nullptr ),
    routingTable(),
    bitErrorRateFunction( nullptr ),
    dropPacketsWhenFull(false)
{
    // Nothing to do.
}


CoreCommunicatorSetup_t::~CoreCommunicatorSetup_t()
{
    // Nohting to do.
}
