// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Core channel setup.



#include "scnsl/builtinPlugin/CoreChannelSetup_t.hh"


using namespace Scnsl::BuiltinPlugin;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

CoreChannelSetup_t::CoreChannelSetup_t():
    // Parents:
	Scnsl::Setup::ChannelSetup_base_t(),
	// Fields:
	channel_type( FULL_DUPLEX ),
	capacity( 0 ),
	capacity2( 0 ),
	delay( sc_core::SC_ZERO_TIME ),
	propagation( CUSTOM_SPEED ),
    custom_propagation( 0 ),
    nodes_number( 0 ),
    alpha( 0 ),
    radio_frequency( 0 )
{
    // Nothing to do.
}


CoreChannelSetup_t::~CoreChannelSetup_t()
{
    // Nothing to do.
}
