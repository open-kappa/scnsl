// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The extension to manage the builtin models.

#include <string>
#include <sstream>

#include "scnsl/builtinPlugin/CoreExtension_t.hh"
#include "scnsl/builtinPlugin/CoreChannelSetup_t.hh"
#include "scnsl/builtinPlugin/CoreTaskSetup_t.hh"
#include "scnsl/builtinPlugin/CoreTracingSetup_t.hh"
#include "scnsl/builtinPlugin/CoreTopologySetup_t.hh"
#include "scnsl/builtinPlugin/CoreCommunicatorSetup_t.hh"
#include "scnsl/channels/UnidirectionalChannel_t.hh"
#include "scnsl/channels/FullDuplexChannel_t.hh"
#include "scnsl/channels/HalfDuplexChannel_t.hh"
#include "scnsl/channels/SharedChannel_t.hh"
#include "scnsl/channels/DelayedSharedChannel_t.hh"
#include "scnsl/tlm/TlmTask_if_t.hh"
#include "scnsl/rtl/RtlTask_base_if_t.hh"
#include "scnsl/tlm/TlmTaskProxy_t.hh"
#include "scnsl/rtl/RtlTaskProxy_t.hh"
#include "scnsl/protocols/mac_802_15_4/Mac802_15_4_t.hh"
#include "scnsl/protocols/router_communicator/RouterCommunicator_t.hh"
#include "scnsl/tracing/Filter_if_t.hh"
#include "scnsl/tracing/Formatter_if_t.hh"
#include "scnsl/tracing/Tracer_t.hh"
#include "scnsl/tracing/BasicFilter_t.hh"
#include "scnsl/tracing/BasicFormatter_t.hh"
#include "scnsl/traffic/Cbr_t.hh"
#include "scnsl/traffic/Gilbert_t.hh"
#include "scnsl/traffic/OnOff_t.hh"
#include "scnsl/core/QueueCommunicator_t.hh"
#include "scnsl/queueModels/Fifo_t.hh"
#include "scnsl/queueModels/Priority_t.hh"
#include "scnsl/traffic/PitTask_t.hh"
#include "scnsl/protocols/saboteur/Saboteur_t.hh"
#include "scnsl/protocols/lv4_communicator/Lv4Communicator_t.hh"





using namespace Scnsl::BuiltinPlugin;



// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

CoreExtension_t::CoreExtension_t():
    // Parents:
    Scnsl::Setup::Extension_if_t( "core" )
{
    // Nothing to do.
}


CoreExtension_t::~CoreExtension_t()
{
    // Nothing to do.
}


// ////////////////////////////////////////////////////////////////
// Creation methods.
// ////////////////////////////////////////////////////////////////


Scnsl::Core::Channel_if_t * CoreExtension_t::createChannel(
    const ChannelSetup_base_t & s )

{
    const CoreChannelSetup_t * cs = dynamic_cast< const CoreChannelSetup_t * >( &s );
    if ( cs == nullptr ) throw std::domain_error( "Incorrect channel setup type." );
    Scnsl::Core::propagation_t propagation_speed = cs->custom_propagation;

    /// @brief SCNSL provides some default constants that simulate
    /// specific propagation speed for the data transmission over a channel.
    /// These constants are needed to define the channel delay.
    switch( cs->propagation )
    {
        /// @brief The propagation speed of a sound wave
        /// through the water which is 1480 m/s.
        case CoreChannelSetup_t::WATER_SOUND_SPEED:
        {
            propagation_speed = 1480;
            break;
        }
        /// @brief The propagation speed of a sound wave
        /// through the air which is 320 m/s.
        case CoreChannelSetup_t::AIR_SOUND_SPEED:
        {
            propagation_speed = 320;
            break;
        }
        /// @brief The speed of light which is
        /// 300000000 m/s.
        case CoreChannelSetup_t::EM_SPEED:
        {
            propagation_speed = 300000000;
            break;
        }
        /// @brief the speed of light through an
        /// optical fiber which is 200000000 m/s.
        case CoreChannelSetup_t::OPTICAL_FIBER_SPEED:
        {
            propagation_speed = 200000000;
            break;
        }
        /// @brief The speed of electricity through
        /// copper which is 200000000 m/s.
        case CoreChannelSetup_t::COPPER_SPEED:
        {
            propagation_speed = 200000000;
            break;
        }

        case CoreChannelSetup_t::CUSTOM_SPEED:
            break;

        default:
            // ERROR:
            throw std::domain_error( "Incorrect propagation speed." );
    }

    switch( cs->channel_type )
    {
        case CoreChannelSetup_t::UNIDIRECTIONAL:
            return new Scnsl::Channels::UnidirectionalChannel_t(
                cs->name.c_str(), cs->capacity, cs->delay );

        case CoreChannelSetup_t::FULL_DUPLEX:
            return new Scnsl::Channels::FullDuplexChannel_t(
                cs->name.c_str(), cs->capacity, cs->capacity2, cs->delay );

        case CoreChannelSetup_t::HALF_DUPLEX:
            return new Scnsl::Channels::HalfDuplexChannel_t(
                cs->name.c_str(), cs->capacity, cs->delay );

        case CoreChannelSetup_t::SHARED:
            return new Scnsl::Channels::SharedChannel_t(
                cs->name.c_str(), cs->nodes_number, cs->alpha, cs->delay );

        case CoreChannelSetup_t::DELAYED_SHARED:
            return new Scnsl::Channels::DelayedSharedChannel_t(
                cs->name.c_str(), cs->nodes_number, cs->alpha, propagation_speed );

        default:
            // ERROR:
            throw std::domain_error( "Incorrect channel type." );
    }
}



Scnsl::Core::Task_if_t * CoreExtension_t::createTask( const TaskSetup_base_t & s )

{

    const CoreTaskSetup_t * ts = dynamic_cast< const CoreTaskSetup_t * >( &s );
    if ( ts == nullptr ) throw std::domain_error( "Incorrect task setup type." );

    switch( ts->task_type )
    {
        case CoreTaskSetup_t::CBR:
            return new Scnsl::Traffic::Cbr_t(
                ts->name.c_str(), ts->id, ts->n, 1, ts->label, ts->pktSize, ts->genTime );

        case CoreTaskSetup_t::GILBERT:
            return new Scnsl::Traffic::Gilbert_t(
                ts->name.c_str(), ts->id, ts->n, 1, ts->label, ts->pktSize,
                ts->genTime, ts->alpha, ts->beta );

        case CoreTaskSetup_t::ON_OFF:
            return new Scnsl::Traffic::OnOff_t(
                ts->name.c_str(), ts->id, ts->n, 1, ts->label, ts->pktSize,
                ts->genTime, ts->on, ts->off );

        case CoreTaskSetup_t::PIT:
            return new Scnsl::Traffic::PitTask_t(
                ts->name.c_str(), ts->id, ts->n, 1 );


        default:
            // ERROR:
            throw std::domain_error( "Incorrect task type." );
    }

}



Scnsl::Core::Communicator_if_t * CoreExtension_t::createCommunicator(
    const CommunicatorSetup_base_t & s )

{
    const CoreCommunicatorSetup_t * cs = dynamic_cast< const CoreCommunicatorSetup_t * >( &s );
    if ( cs == nullptr ) throw std::domain_error( "Incorrect communicator setup type." );

    switch ( cs->type )
    {
        case CoreCommunicatorSetup_t::MAC_802_15_4:
            return new Scnsl::Protocols::Mac_802_15_4::Mac802_15_4_t( cs->name.c_str(),
                                                                      cs->node,
                                                                      cs->ack_required,
                                                                      cs->short_addresses );


        case CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR:
            return new Scnsl::Protocols::RouterCommunicator::RouterCommunicator_t(cs->routingTable, cs->node);

        case CoreCommunicatorSetup_t::QUEUE:
            return createQueue( s );

        case CoreCommunicatorSetup_t::SABOTEUR:
            return new Scnsl::Protocols::Saboteur::Saboteur_t( cs->name.c_str(), cs->bitErrorRateFunction );

        case CoreCommunicatorSetup_t::LV4_COMMUNICATOR:
            return new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t(cs->name.c_str());

    }

	throw std::domain_error("Incorrect communicator type for core package.");
    /*return nullptr;*/
}


Scnsl::Core::QueueCommunicator_t * CoreExtension_t::createQueue(
    const CommunicatorSetup_base_t & s)

{
    const CoreCommunicatorSetup_t * cs = dynamic_cast< const CoreCommunicatorSetup_t * >( &s );

    Scnsl::QueueModels::Queue_if_t * queueSend;
    Scnsl::QueueModels::Queue_if_t * queueReceive;

    switch ( cs->queueSend )
    {
        case CoreCommunicatorSetup_t::FIFO:
            queueSend = new Scnsl::QueueModels::Fifo_t( cs->capacitySend );
            break;

        case CoreCommunicatorSetup_t::PRIORITY:
            queueSend = new Scnsl::QueueModels::Priority_t( cs->capacitySend, cs->numberSend, cs->policySend, cs->weightSend );
            break;

        case CoreCommunicatorSetup_t::NONE:
            queueSend = nullptr;
            break;

        default:
            throw std::domain_error( "Incorrect queue type for core package." );
    }


    switch ( cs->queueReceive )
    {
        case CoreCommunicatorSetup_t::FIFO:
            queueReceive = new Scnsl::QueueModels::Fifo_t( cs->capacityReceive );
            break;

        case CoreCommunicatorSetup_t::PRIORITY:
            queueReceive = new Scnsl::QueueModels::Priority_t( cs->capacityReceive, cs->numberReceive, cs->policyReceive, cs->weightReceive );
            break;

        case CoreCommunicatorSetup_t::NONE:
            queueReceive = nullptr;
            break;

        default:
            throw std::domain_error( "Incorrect queue type for core package." );
    }

    return new Scnsl::Core::QueueCommunicator_t( cs->name.c_str(),
                                                 queueSend,
                                                 queueReceive,
                                                 cs->dropPacketsWhenFull );

}



Scnsl::Tracing::Tracer_t * CoreExtension_t::createTracer(
    const TracingSetup_base_t & s )

{
    Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

    Scnsl::Tracing::Filter_if_t * filter = scnsl->createFilter( s );
    Scnsl::Tracing::Formatter_if_t * formatter;
    try
    {
        formatter = scnsl->createFormatter( s );
    }
    catch ( ... )
    {
        delete filter;
        throw;
    }

    return new Scnsl::Tracing::Tracer_t( filter, formatter );
}


Scnsl::Tracing::Filter_if_t * CoreExtension_t::createFilter(
    const TracingSetup_base_t & s )

{
    if ( s.filterName == "basic" )
    {
        return new Scnsl::Tracing::BasicFilter_t(
            s.info,
            s.log,
            s.debug,
            s.warning,
            s.error,
            s.fatal );
    }
    else
    {
        throw std::domain_error( "Invalid filter name." );
    }
}


Scnsl::Tracing::Formatter_if_t * CoreExtension_t::createFormatter(
    const TracingSetup_base_t & s )

{
    const CoreTracingSetup_t * cs = dynamic_cast< const CoreTracingSetup_t * >( &s );
    if ( cs == nullptr ) throw std::domain_error( "Incorrect tracing setup type." );

    if ( cs->filterName == "basic" )
    {
        Scnsl::Tracing::BasicFormatter_t * f = new Scnsl::Tracing::BasicFormatter_t();
        f->printTraceType( cs->print_trace_type );
        f->printTraceTimestamp(cs->print_trace_timestamp);
        return f;
    }
    else
    {
        throw std::domain_error( "Invalid formatter name." );
    }
}


void CoreExtension_t::createTopology( const TopologySetup_base_t & s )

{

    const CoreTopologySetup_t * ts = dynamic_cast< const CoreTopologySetup_t * >( &s );
    if ( ts == nullptr ) throw std::domain_error( "Incorrect topology setup type." );

    // Singleton.
    Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

    switch ( ts->topology_type )
    {
        case CoreTopologySetup_t::BOTTLENECK:
        {
            // Nodes initialization
            Scnsl::Core::Node_t * n0 = sim->createNode();
            Scnsl::Core::Node_t * n1 = sim->createNode();
            Scnsl::Core::Node_t * n2 = sim->createNode();
            Scnsl::Core::Node_t * n3 = sim->createNode();
            Scnsl::Core::Node_t * n4 = sim->createNode();
            Scnsl::Core::Node_t * n5 = sim->createNode();

            // Channels initialization
            if( ts->channel_list.size() == 0 ) throw std::domain_error( "Channel list must not be null" );
            Scnsl::Core::Channel_if_t * ch0 = sim->createChannel( * ts->channel_list[ 0 ] );

            if( ch0->isMultipoint() )
            {
                // bind wireless
                sim->bind(n0, ch0, *ts->bind_list[0]);
                sim->bind(n1, ch0, *ts->bind_list[1]);
                sim->bind(n2, ch0, *ts->bind_list[2]);
                sim->bind(n3, ch0, *ts->bind_list[3]);
                sim->bind(n4, ch0, *ts->bind_list[4]);
                sim->bind(n5, ch0, *ts->bind_list[5]);
            }
            else
            {
                if( ts->channel_list.size() < 5 ) throw std::domain_error( "Channel list must contains almost 5 channels" );

                Scnsl::Core::Channel_if_t * ch1 = sim->createChannel( * ts->channel_list[ 1 ] );
                Scnsl::Core::Channel_if_t * ch2 = sim->createChannel( * ts->channel_list[ 2 ] );
                Scnsl::Core::Channel_if_t * ch3 = sim->createChannel( * ts->channel_list[ 3 ] );
                Scnsl::Core::Channel_if_t * ch4 = sim->createChannel( * ts->channel_list[ 4 ] );

                // Bind node - channel
                sim->bind(n0, ch0, *ts->bind_list[0]);
                sim->bind(n1, ch1, *ts->bind_list[1]);
                sim->bind(n2, ch2, *ts->bind_list[2]);
                sim->bind(n3, ch3, *ts->bind_list[3]);
                sim->bind(n4, ch0, *ts->bind_list[4]);
                sim->bind(n4, ch2, *ts->bind_list[4]);
                sim->bind(n4, ch4, *ts->bind_list[4]);
                sim->bind(n5, ch1, *ts->bind_list[5]);
                sim->bind(n5, ch3, *ts->bind_list[5]);
                sim->bind(n5, ch4, *ts->bind_list[5]);
            }

            return;
        }

        case CoreTopologySetup_t::MESH:
        {
            if ( (ts->node_factory_infos.nodeNumberX == 1 && ts->node_factory_infos.nodeNumberY == 1 && ts->node_factory_infos.nodeNumberZ == 1) ||
                 (ts->node_factory_infos.nodeNumberX == 0 || ts->node_factory_infos.nodeNumberY == 0 || ts->node_factory_infos.nodeNumberZ == 0) ||
                 ts->node_factory_infos.nodeNumberZ == 1)
                throw std::domain_error( "Incorrect topology parameter." );

            createMeshTopology(s);
            return;
        }

        default:
            throw std::domain_error( "Incorrect topology type for core package." );
    }
}

void CoreExtension_t::createMeshTopology( const TopologySetup_base_t & s )

{
    const CoreTopologySetup_t * ts = dynamic_cast< const CoreTopologySetup_t * >( &s );
    if ( ts == nullptr ) throw std::domain_error( "Incorrect topology setup type." );

    // Singleton.
    Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

    // Nodes initialization
    Scnsl::Core::Node_t ****n; // [ts->node_factory_infos.nodeNumberX][ts->node_factory_infos.nodeNumberY][ts->node_factory_infos.nodeNumberZ];
    n = new Scnsl::Core::Node_t *** [ ts->node_factory_infos.nodeNumberX ];
    for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX; ++x)
    {
        n[x] = new Scnsl::Core::Node_t ** [ ts->node_factory_infos.nodeNumberY ];
        for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY; ++y)
        {
            n[x][y] = new Scnsl::Core::Node_t * [ ts->node_factory_infos.nodeNumberZ ];
            for (unsigned int z = 0; z < ts->node_factory_infos.nodeNumberZ; ++z)
            {
                n[x][y][z] = sim->createNode();
            }
        }
    }

    unsigned int nodes_on_floor = ts->node_factory_infos.nodeNumberZ * ts->node_factory_infos.nodeNumberY;
    unsigned int nodes_on_row  = ts->node_factory_infos.nodeNumberZ;

    // Channel list must not be null
    if( ts->channel_list.size() == 0 ) throw std::domain_error( "Channel list must not be null." );

    Scnsl::Core::Channel_if_t * ch0 = sim->createChannel( * ts->channel_list[ 0 ] );

    if( ch0->isMultipoint() )
    {

        Scnsl::Core::distance_t dist_x = 0;
        Scnsl::Core::distance_t dist_y = 0;
        Scnsl::Core::distance_t dist_z = 0;

        // bind wireless
        for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX; ++x)
        {
            dist_y = 0;
            for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY; ++y)
            {
                dist_z = 0;
                for (unsigned int z = 0; z < ts->node_factory_infos.nodeNumberZ; ++z)
                {
                    Scnsl::Setup::BindSetup_base_t * bsb = ts->bind_list[ x * nodes_on_floor + y * nodes_on_row + z ];
                    bsb->node_binding.x = dist_x;
                    bsb->node_binding.y = dist_y;
                    bsb->node_binding.z = dist_z;
                    sim->bind(n[x][y][z], ch0, *bsb);

                    dist_z += ts->node_factory_infos.nodeStepZ;
                }
                dist_y += ts->node_factory_infos.nodeStepY;
            }
            dist_x += ts->node_factory_infos.nodeStepX;
        }
    }
    else
    {
        // number of Channels
        unsigned int number_of_channel =
        ts->node_factory_infos.nodeNumberZ * ( ts->node_factory_infos.nodeNumberX * ( 3 * ts->node_factory_infos.nodeNumberY - 1)
            - ts->node_factory_infos.nodeNumberY )
            - ts->node_factory_infos.nodeNumberX * ts->node_factory_infos.nodeNumberY;

        if( ts->channel_list.size() < number_of_channel )
        {
            std::string error_string = "Channel list must contains almost " + std::to_string(number_of_channel);
            error_string += " channels.";
            throw std::domain_error( error_string );
        }


        // Channels initialization
        Scnsl::Core::Channel_if_t **ch = new Scnsl::Core::Channel_if_t * [number_of_channel];
        ch[0]=ch0;
        for (unsigned int i = 1; i < number_of_channel; i++) ch[i] = sim->createChannel(*ts->channel_list[i]);


        long unsigned int index_bind_list = 0;

        for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX; ++x)
            for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY; ++y)
                for (unsigned int z = 0; z < ts->node_factory_infos.nodeNumberZ-1; ++z)
                {
                    sim->bind(n[x][y][z], ch[index_bind_list],
                                       *ts->bind_list[x * nodes_on_floor + y * nodes_on_row + z]);
                    sim->bind(n[x][y][z + 1], ch[index_bind_list],
                                       *ts->bind_list[x * nodes_on_floor + y * nodes_on_row + (z + 1)]);
                    index_bind_list++;
                }

        for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX; ++x)
            for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY-1; ++y)
                for (unsigned int z = 0; z < ts->node_factory_infos.nodeNumberZ; ++z)
                {
                    sim->bind(n[x][y][z], ch[index_bind_list],
                                       *ts->bind_list[x * nodes_on_floor + y * nodes_on_row + z]);
                    sim->bind(n[x][y + 1][z], ch[index_bind_list],
                                       *ts->bind_list[x * nodes_on_floor + (y + 1) * nodes_on_row + z]);
                    index_bind_list++;
                }

        for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX-1; ++x)
            for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY; ++y)
                for (unsigned int z = 0; z < ts->node_factory_infos.nodeNumberZ; ++z)
                {
                    sim->bind(n[x][y][z], ch[index_bind_list],
                                       *ts->bind_list[x * nodes_on_floor + y * nodes_on_row + z]);
                    sim->bind(n[x + 1][y][z], ch[index_bind_list],
                                       *ts->bind_list[(x + 1) * nodes_on_floor + y * nodes_on_row + z]);
                    index_bind_list++;
                }

        delete [] ch;
    }

    for (unsigned int x = 0; x < ts->node_factory_infos.nodeNumberX; ++x)
    {
        for (unsigned int y = 0; y < ts->node_factory_infos.nodeNumberY; ++y)
        {
            delete n[x][y];
        }
        delete n[x];
    }
    delete n;
}



Scnsl::Core::TaskProxy_if_t * CoreExtension_t::createTaskProxy(
    Scnsl::Core::Task_if_t * t,
	const Scnsl::Setup::BindSetup_base_t & bs,
    Scnsl::Core::Channel_if_t * ch )

{
    Scnsl::Tlm::TlmTask_if_t * tlmTask =
    dynamic_cast< Scnsl::Tlm::TlmTask_if_t * >( t );

    if ( tlmTask != nullptr )
    {
        std::string s;
        std::stringstream ss;

        ss << t->name() << "_proxy" << t->getBoundedProxiesNumber();
        ss >> s;

        return new Scnsl::Tlm::TlmTaskProxy_t( s.c_str(), t, bs.bindIdentifier, ch );
    }


    Scnsl::Rtl::RtlTask_base_if_t * rtlTask =
    dynamic_cast< Scnsl::Rtl::RtlTask_base_if_t * >( t );
    if ( rtlTask != nullptr )
    {
        std::string s;
        std::stringstream ss;

        ss << t->name() << "_proxy" << t->getBoundedProxiesNumber();
        ss >> s;

        return new Scnsl::Rtl::RtlTaskProxy_t( s.c_str(), t, bs.bindIdentifier, ch );
    }

    throw std::domain_error( "Incorrect task type for core package." );
}
