// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Core task setup.



#include "scnsl/builtinPlugin/CoreTaskSetup_t.hh"


using namespace Scnsl::BuiltinPlugin;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

CoreTaskSetup_t::CoreTaskSetup_t():
    // Parents:
	Scnsl::Setup::TaskSetup_base_t(),
	// Fields:
	label( 0 ),
	task_type( CBR ),
	pktSize( 0 ),
	genTime( sc_core::SC_ZERO_TIME ),
	on( sc_core::SC_ZERO_TIME ),
	off( sc_core::SC_ZERO_TIME ),
    alpha( 0.5 ),
    beta( 0.5 )
{
    // Nothing to do.
}


CoreTaskSetup_t::~CoreTaskSetup_t()
{
    // Nothing to do.
}
