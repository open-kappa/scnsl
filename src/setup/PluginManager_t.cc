// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A plugin manager.

#include <cstdlib>

#if (defined _WIN32)
#include <windows.h>
#elif (defined __linux__)
#include <dlfcn.h>
#else
#error "Unsupported compiler."
#endif

#include "scnsl/setup/Extension_if_t.hh"
#include "scnsl/setup/PluginManager_t.hh"

#ifdef _MSC_VER
#pragma warning(disable:4191)
#endif

using namespace Scnsl::Setup;

extern "C"
{
/** @brief The type of the loaded symbol. */
typedef void (* get_extensions_t )(void *);
}


namespace Scnsl { namespace Setup {

namespace /*anon*/ {

#if (defined _WIN32)
typedef HMODULE HandlerType;

void * _openLibrary(const std::string & lib)
{
    return static_cast<void *>(LoadLibrary(lib.c_str()));
}

get_extensions_t _getSymbol(void * const lib, const std::string & symb)
{
	return reinterpret_cast<get_extensions_t>(GetProcAddress(static_cast<HMODULE>(lib), symb.c_str()));
}

void _closeLibrary(void * lib)
{
    FreeLibrary(static_cast<HMODULE>(lib));
}

#elif (defined __linux__)
typedef void * HandlerType;

void * _openLibrary(const std::string & lib)
{
    return dlopen(lib.c_str(), RTLD_LAZY /*| RTLD_LOCAL*/);
}

get_extensions_t _getSymbol(void * const lib, const std::string & symb)
{
    void * value = dlsym(lib, symb.c_str());
    union avoid_cast_warning_t
    {
        void * v;
        get_extensions_t load;
    } u;

    u.v = value;

    return u.load;
}

void _closeLibrary(void * lib)
{
    dlclose(lib);
}

#else
#error "Unsupported compiler/os."
#endif

} // anon

} } // Scnsl::Setup


const char * const PluginManager_t::_SYMBOL = "get_extensions";


PluginManager_t::PluginManager_t():
    _handlers()
{
    // Nohting to do.
}


PluginManager_t::~PluginManager_t()
{
    // Reverse order.
    for ( HandlerList_t::reverse_iterator i = _handlers.rbegin();
        i != _handlers.rend();
        ++i )
    {
        _closeLibrary( *i );
    }
}


void PluginManager_t::load( const PluginName_t & name, ExtensionList_t & extensionList )

{
    void * handle = _openLibrary(name.c_str());

    if (handle == nullptr)
    {
        throw std::domain_error("Cannot open plugin: " + name);
    }

    // Cleaning up possible old error codes.
    get_extensions_t value = _getSymbol(handle, _SYMBOL);
    if (value == nullptr)
    {
        _closeLibrary(handle);
        throw std::logic_error(std::string("Cannot find plugin symbol: ") + _SYMBOL);
    }

    ExtensionList_t ext;
    (*value)( & ext );

    // Checking if an extension is already loaded:
    for (ExtensionList_t::iterator i = ext.begin();
        i != ext.end();
        ++ i )
    {
        ExtensionList_t::iterator f = extensionList.find( (*i).second->getId() );
        if (f != extensionList.end() )
        {
            _closeLibrary(handle);
            throw std::domain_error(std::string("Extension ID already in use: ") + f->first);
        }
    }

    // Finally, we can update loader status and extension list.
    // Exception neutrality reached. :)
    _handlers.push_back(handle);

    for ( ExtensionList_t::iterator i = ext.begin();
        i != ext.end();
        ++ i )
    {
        extensionList[ (*i).second->getId() ] = (*i).second;
    }
}
