// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// Communicator setup base class.



#include "scnsl/setup/CommunicatorSetup_base_t.hh"

using namespace Scnsl::Setup;



CommunicatorSetup_base_t::CommunicatorSetup_base_t():
    extensionId(),
    name()
{
    // Nothing to do.
}

CommunicatorSetup_base_t::~CommunicatorSetup_base_t()
{
    // Nothing to do.
}
