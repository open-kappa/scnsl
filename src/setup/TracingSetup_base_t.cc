// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The tracing setup base class.



#include "scnsl/setup/TracingSetup_base_t.hh"


using namespace Scnsl::Setup;


TracingSetup_base_t::TracingSetup_base_t():
    // Fields:
    extensionId(),
    filterExtensionId(),
    filterName(),
    formatterExtensionId(),
    formatterName(),
    info( 0 ),
    log( 0 ),
    debug( 0 ),
    warning( 0 ),
    error( 0 ),
    fatal( 0 )
{
    // Nothing to do.
}

TracingSetup_base_t::~TracingSetup_base_t()
{
    // Nothing to do.
}
