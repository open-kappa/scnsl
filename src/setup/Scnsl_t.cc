// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// The simulator class.

#include <exception>
#include <iostream>
#include <set>

#include "scnsl/core/TaskProxy_if_t.hh"

#include "scnsl/core/Channel_if_t.hh"
#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/Communicator_if_t.hh"
#include "scnsl/setup/Extension_if_t.hh"
#include "scnsl/core/Node_t.hh"

#include "scnsl/tracing/Tracer_t.hh"

#include "scnsl/builtinPlugin/CoreExtension_t.hh"

#include "scnsl/setup/Scnsl_t.hh"

#include "scnsl/core/SocketMap.hh"



using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;


// ////////////////////////////////////////////////////////////////
// Static members initialization.
// ////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////
// Singleton design pattern methods.
// ////////////////////////////////////////////////////////////////

Scnsl_t * Scnsl_t::get_instance()
{
    static Scnsl_t s;
    return &s;
}

// ////////////////////////////////////////////////////////////////
// Instantation methods.
// ////////////////////////////////////////////////////////////////

Node_t * Scnsl_t::createNode(node_id_t id)
{
	static node_id_t sid = 0;
	if (id == static_cast<node_id_t>(-1)) id = sid++;
    Node_t * ret = new Node_t(id);
    _nodes.push_back( ret );
    return ret;
}

Channel_if_t * Scnsl_t::createChannel( const ChannelSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    Channel_if_t * ret = i->second->createChannel( s );
    _channels.push_back( ret );
    return ret;
}

Task_if_t * Scnsl_t::createTask( const TaskSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    Task_if_t * ret = i->second->createTask( s );
    _tasks.push_back( ret );
    return ret;
}

Communicator_if_t * Scnsl_t::createCommunicator( const CommunicatorSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    Communicator_if_t * ret = i->second->createCommunicator( s );
    _communicators.push_back( ret );
    return ret;
}

void Scnsl_t::createTopology( const TopologySetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    i->second->createTopology( s );
}

Scnsl_t::Tracer_t * Scnsl_t::createTracer( const TracingSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    Tracer_t * ret = i->second->createTracer( s );
    _tracers.push_back( ret );
    return ret;
}

// ////////////////////////////////////////////////////////////////
// Binding methods.
// ////////////////////////////////////////////////////////////////

void Scnsl_t::bind( Node_t * n, Channel_if_t * ch, const BindSetup_base_t & s )
{
    
    static node_properties_id_t id = 0;
    BindSetup_base_t* ss = const_cast< BindSetup_base_t* >( &s );
    ss->node_binding.id = id++;
    ss->node_binding.sourceNode = n;
    n->setProperties( ss->node_binding, ch, true );
    ch->addNode( n );

    // ///////
    if (s.socket_binding.socket_active)
    {
        //bind the node with the source ip of the socket
        Scnsl::Core::SocketMap::bindNodes(n,s.socket_binding.source_ip); 
        //bind the name with the source ip of the socket
        Scnsl::Core::SocketMap::bindName(s.socket_binding.hostname,s.socket_binding.source_ip); 
    }
}

void Scnsl_t::bind( Task_if_t * t, Task_if_t * dst, Channel_if_t * ch,
                    const BindSetup_base_t & s, Communicator_if_t * c, bool bind_comm )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension." );

    // Task-TaskProxy bind is done directly inside taskproxy constructor.
    TaskProxy_if_t * tp = (*i).second->createTaskProxy( t, s,ch );
    _communicators.push_back( tp );
    tp->setDestinationTask( dst );

    // //////////////////////////////////
    if (s.socket_binding.socket_active) //request to save taskproxy->socket
    {
        Scnsl::Core::SocketMap::bindTask(tp,s.socket_binding);

    }
    // /////////////////////////////////

    Node_t * n = t->getNode();

    if ( c != nullptr )
    {
        // Binding with intermediate communicator.
        tp->stackDown( c );
        if (bind_comm)
        {
            c->stackDown(n);
            n->bindChannel( ch, c );
        }

        try
        {
            c->bindTaskProxy( tp, n );
        }
        catch( std::logic_error &)
        {
            // Communicator supports only stack down.
            if (bind_comm)
                c->stackDown( n );
        }

        try
        {
            c->bindChannel( ch, tp );
        }
        catch( std::logic_error &)
        {
            // Communicator supports only stack up.
            c->stackUp( tp );
        }
    }
    else
    {
        // Direct bind.
        tp->stackDown( n );
        n->bindChannel( ch, tp );
    }
}

void Scnsl_t::bind( std::vector<multicast_wrapper_t> elements, unsigned multicast_ip)
{
    std::set<Task_if_t*> connected_tasks;
    bool bind_comm ;

    for (auto element: elements)
    {
        bind_comm = true;
        ExtensionList_t::iterator i = _extensions.find( element.bsb->extensionId );
        if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension." );

        // Task-TaskProxy bind is done directly inside taskproxy constructor.
        TaskProxy_if_t * tp = (*i).second->createTaskProxy( element.task, *(element.bsb),
                                                            element.ch );
        _communicators.push_back( tp );
        //no dest, since multicast
        tp->setDestinationTask( nullptr );

        SocketMap::addMuticastTP(tp,multicast_ip,element.bsb->socket_binding.source_port);
        
        if (connected_tasks.count(element.task) == 0)
            connected_tasks.insert(element.task);
        else bind_comm = false; //no rebinding communicator to channel
        // //////////////////////////////////
        if (element.bsb->socket_binding.socket_active) //request to save taskproxy->socket
        {
            element.bsb->socket_binding.dest_ip=multicast_ip;
            std::cerr<<element.bsb->socket_binding.source_ip<<" "
                    <<element.bsb->socket_binding.source_port<<std::endl;
            Scnsl::Core::SocketMap::bindTask(tp,element.bsb->socket_binding);
        }
        // /////////////////////////////////

        Node_t * n = element.task->getNode();
        Communicator_if_t* c = element.comm;
        Channel_if_t* ch = element.ch;
        if ( c != nullptr )
        {
            // Binding with intermediate communicator.
            tp->stackDown( c );
            if (bind_comm)
            {
                c->stackDown(n);
                n->bindChannel( ch, c );
            }

            try
            {
                c->bindTaskProxy( tp, n );
            }
            catch( std::logic_error &)
            {
                // Communicator supports only stack down.
                if (bind_comm)
                    c->stackDown( n );
            }

            try
            {
                c->bindChannel( ch, tp );
            }
            catch( std::logic_error &)
            {
                // Communicator supports only stack up.
                c->stackUp( tp );
            }
        }
        else
        {
            // Direct bind.
            tp->stackDown( n );
            n->bindChannel( ch, tp );
        }
    }

}

// ////////////////////////////////////////////////////////////////
// Other functionalities.
// ////////////////////////////////////////////////////////////////

Scnsl_t::NodeList_t & Scnsl_t::getNodes()
{
    return _nodes;
}

Scnsl_t::ChannelList_t & Scnsl_t::getChannels()
{
    return _channels;
}

void Scnsl_t::loadExtension( const PluginName_t & id )

{
    _pluginManager.load( id, _extensions );
}

// ////////////////////////////////////////////////////////////////
// Internal support methods.
// ////////////////////////////////////////////////////////////////

Scnsl_t::Formatter_if_t * Scnsl_t::createFormatter( const TracingSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    return i->second->createFormatter( s );
}


Scnsl_t::Filter_if_t * Scnsl_t::createFilter( const TracingSetup_base_t & s )

{
    ExtensionList_t::iterator i = _extensions.find( s.extensionId );
    if ( i == _extensions.end() ) throw std::domain_error( "Unregistered extension.");

    return i->second->createFilter( s );
}



// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////

Scnsl_t::Scnsl_t():
    _nodes(),
    _channels(),
    _tasks(),
    _communicators(),
    _tracers(),
    _extensions(),
    _pluginManager()
{
	Extension_if_t * core = new CoreExtension_t();
	ExtensionId_t id = core->getId();
	_extensions[ id ] = core;
}

Scnsl_t::~Scnsl_t()
{
    for ( NodeList_t::iterator i = _nodes.begin();
          i != _nodes.end();
          ++ i )
    {
        delete *i;
    }

    for ( ChannelList_t::iterator i = _channels.begin();
          i != _channels.end();
          ++ i )
    {
        delete *i;
    }

    for ( TaskList_t::iterator i = _tasks.begin();
          i != _tasks.end();
          ++ i )
    {
        delete *i;
    }

    for ( CommunicatorList_t::iterator i = _communicators.begin();
          i != _communicators.end();
          ++ i )
    {
        delete *i;
    }

    for ( TracerList_t::iterator i = _tracers.begin();
          i != _tracers.end();
          ++ i )
    {
        delete *i;
    }

    for ( ExtensionList_t::iterator i = _extensions.begin();
          i != _extensions.end();
          ++ i )
    {
        delete (*i).second;
    }
}

////////////////////////////////////////
// MISC
///////////////////////////////////////
multicast_wrapper_t::multicast_wrapper_t():
    task(nullptr),
    comm(nullptr),
    ch(nullptr),
    bsb(nullptr)
{

}

multicast_wrapper_t::multicast_wrapper_t(const multicast_wrapper_t& mw):
    task(mw.task),
    comm(mw.comm),
    ch(mw.ch),
    bsb(mw.bsb)
{

}
