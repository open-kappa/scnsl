#include "scnsl/system_calls/TimedSyscalls.hh"
#include "scnsl/system_calls/SyscallsWait.hh"


using namespace Scnsl;
using namespace Scnsl::Syscalls;

int Syscalls::usleep(unsigned usec)
{
    auto currApi = synchTaskTime();
    sc_core::sc_time t(usec, sc_core::SC_US);
    sc_core::wait(t);
    currApi->initTime();
    return 0;
}

int Syscalls::nanosleep(const timespec *req, timespec *rem)
{

    auto currApi = synchTaskTime();
    sc_core::sc_time sec (req->tv_sec,sc_core::SC_SEC);
    sc_core::sc_time nsec (req->tv_nsec,sc_core::SC_NS);
    sc_core::wait(sec+nsec);
    rem->tv_sec=0;
    rem->tv_nsec=0;
    currApi->initTime();
    return 0;
}


int Syscalls::gettimeofday(timeval * tv, long int * tz)
{
    auto currApi = synchTaskTime();

    // use timestamp and convert it to second
    sc_core::sc_time t = sc_core::sc_time_stamp();
    tv->tv_sec = 0;
    tv->tv_usec = sc_core::sc_time_stamp().value() / 1000000;
    currApi->initTime();

    return 0;
}

time_t Syscalls::time(time_t * timer)
{

    auto currApi = synchTaskTime();

    sc_core::sc_time t = sc_core::sc_time_stamp();
    if (timer != NULL) 
        (*timer) = t.to_seconds();

    // set new start time
    currApi->initTime();

    return t.to_seconds();
}

int Syscalls::clock_gettime(clockid_t __clock_id, timespec * __tp)
{        

    auto currApi = synchTaskTime();

    __tp->tv_sec = 0;
    __tp->tv_nsec = sc_core::sc_time_stamp().value() / 1000;
    currApi->initTime();

    return 0;
}
