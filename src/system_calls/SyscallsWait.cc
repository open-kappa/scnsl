#include "scnsl/system_calls/SyscallsWait.hh"

using namespace Scnsl;

NetworkAPI_Task_if_t * Syscalls::synchTaskTime()
{
    auto curr_handle = sc_core::sc_get_current_process_handle();
    auto obj = curr_handle.get_parent_object();
    auto currApi = dynamic_cast<NetworkAPI_Task_if_t *>(obj);
    if (currApi == nullptr)
        throw std::bad_cast(); 
    currApi->waitForTime();
    return currApi;
}
