#include "scnsl/system_calls/NetworkSyscalls.hh"
#include "scnsl/system_calls/SyscallsWait.hh"
#include "scnsl/core/SocketMap.hh"

#include <sstream>

#include <sys/select.h>
using namespace Scnsl;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;

int* Syscalls::scnsl_internal_errno()
{
   auto currApi = synchTaskTime();
    auto ret = currApi->getInternalError();
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::scnsl_unsupported_ipv6(in6_addr * addr)
{
    auto currApi = synchTaskTime();
    // set new start time
    currApi->initTime();
    return 1;
}

socketFD_t Syscalls::socket(int domain, int type, int protocol)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_socket(domain, type, protocol);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::fcntl(socketFD_t sfd, int opt, int val)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_fcntl(sfd, opt, val);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::bind(socketFD_t sfd, const sockaddr * socket_addr, int addrlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_bind(sfd, socket_addr, addrlen);
    currApi->initTime();
    return ret;
}

int Syscalls::listen(socketFD_t sfd, int backlog)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_listen(sfd, backlog);
    currApi->initTime();
    return ret;
}

socketFD_t Syscalls::accept(socketFD_t sfd,sockaddr * addr,socklen_t * addrnen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_accept(sfd, addr, addrnen);
    currApi->initTime();
    return ret;
}
socketFD_t Syscalls::accept4(socketFD_t sfd, sockaddr * addr, socklen_t * addrnen,
                             int flags)
{
    //no difference, for the moment
    return accept(sfd,addr,addrnen);
}


int Syscalls::connect(socketFD_t sfd, const sockaddr * socket_addr, int addrlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_connect(sfd, socket_addr, addrlen);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::send(socketFD_t sfd, const void * buffer, int length, int flags)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_send(sfd, buffer, length, flags);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::sendto(socketFD_t sfd, const void * buffer, int length, int flags,
                        const struct sockaddr * dest_addr, socklen_t addrlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_sendto(sfd, buffer, length, flags, dest_addr,
                                        addrlen);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::recv(socketFD_t sfd, void * buffer, int length, int flags)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_recv(sfd, buffer, length, flags);
    // set new start time
    currApi->initTime();
    return ret;
}

ssize_t Syscalls::read(int fd, void *buf, size_t count)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_recv(fd, buf, count,0);
    currApi->initTime();
    return ret;

}


int Syscalls::recvfrom(socketFD_t sfd, void * buffer, int length, int flags,
                        struct sockaddr * src_addr, socklen_t * addrlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_recvfrom(sfd, buffer, length, flags, src_addr,
                                        addrlen);
    // set new start time
    currApi->initTime();
    return ret;
}

int Syscalls::select(int nfds, fd_set * readfds, fd_set * writefds, fd_set * exceptfds,
                        struct timeval * timeout)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_select(nfds, readfds, writefds, exceptfds,
                                        timeout);
    currApi->initTime();
    return ret;
}


int Syscalls::poll(struct pollfd *fds,nfds_t nfds, int timeout)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_poll(fds, nfds,  timeout);
    currApi->initTime();
    return ret;
}


int Syscalls::getsockopt(int sockfd, int level, int optname, void * optval,
                            socklen_t * optlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_getsockopt(sockfd, level, optname, optval,
                                            optlen);
    currApi->initTime();
    return ret;
}

int Syscalls::setsockopt(int sockfd,int level,int optname,const void * optval,
                            socklen_t optlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_setsockopt(sockfd, level, optname, optval,
                                            optlen);
    currApi->initTime();
    return ret;
}

int Syscalls::gethostname(char * name, size_t len)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_gethostname(name, len);
    currApi->initTime();
    return ret;
}

int Syscalls::close(socketFD_t sfd)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_close(sfd);
    currApi->initTime();
    return ret;
}

int Syscalls::shutdown(int sockfd, int how)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_shutdown(sockfd, how);
    currApi->initTime();
    return ret;
}

int Syscalls::getaddrinfo(const char * node,const char * service,

                            const addrinfo * hints,struct addrinfo ** res)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_getaddrinfo(node, service, hints, res);
    currApi->initTime();
    return ret;
}

void Syscalls::freeaddrinfo(struct addrinfo * ai)
{
    auto currApi = synchTaskTime();
    addrinfo * next = nullptr;
    do
    {
        next = ai->ai_next;
        delete ai->ai_addr;
        delete ai;
        ai = next;
    }
    while (ai != NULL);
    currApi->initTime();
}

const char * Syscalls::gai_strerror(int errorcode)
{
    auto currApi = synchTaskTime();    std::stringstream ss;
    switch (errorcode)
    {
        case EINTR:
            currApi->initTime();
             return "Operation interrupted";
        case EAGAIN:
            currApi->initTime();
            return "Try Again";
            break;
        case EINPROGRESS:
            currApi->initTime();
            return "Operation in progress";
        default:
            currApi->initTime();
             return "Something went wrong";
    }
}

int Syscalls::getnameinfo(const struct sockaddr * addr, socklen_t addrlen, char * host,
                            socklen_t hostlen, char * serv, socklen_t servlen, int flags)
{
    auto currApi = synchTaskTime();
    // convert the value from struct to addr from the string
    if (servlen > 0)
        memcpy(
            serv,
            SocketMap::getIP(addr->sin_addr.s_addr).c_str(),
            std::min(
                servlen,
                (unsigned)SocketMap::getIP(addr->sin_addr.s_addr).length()));
    if (hostlen > 0 && flags != NI_NUMERICHOST)
    {
        std::stringstream s;
        s << addr->sin_port;
        memcpy(
            host,
            s.str().c_str(),
            std::min(servlen, (unsigned)s.str().length()));
    }
    else
    {
        std::stringstream s;
        s << 42;  // put a default value as port
        memcpy(
            host,
            s.str().c_str(),
            std::min(servlen, (unsigned)s.str().length()));
    }
    currApi->initTime();

    return 0;
}

int Syscalls::getsockname(int sockfd, struct sockaddr * addr, socklen_t * addrlen)
{
    auto currApi = synchTaskTime();

    auto ret = currApi->internal_getsockname(sockfd, addr, addrlen);
    currApi->initTime();
    return ret;
}

int Syscalls::getpeername(int sockfd, struct sockaddr * addr, socklen_t * addrlen)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_getpeername(sockfd, addr, addrlen);
    currApi->initTime();
    return ret;
}

uint32_t Syscalls::htonl(uint32_t host)
{
    auto currApi = synchTaskTime();
    currApi->initTime();
    return host;
}

uint32_t Syscalls::ntohl(uint32_t net)
{
    auto currApi = synchTaskTime();
    currApi->initTime();
    return net;
}

uint16_t Syscalls::htons(uint16_t host)
{
    auto currApi = synchTaskTime();
    currApi->initTime();
    return host;
}

uint16_t Syscalls::ntohs(uint16_t net)
{
    auto currApi = synchTaskTime();
    currApi->initTime();
    return net;
}

char * Syscalls::inet_ntoa(in_addr in)
{
    auto currApi = synchTaskTime();

    auto buf = SocketMap::getIP(in.s_addr);
    char ret[buf.length() + 1];
    strcpy(ret, buf.c_str());
    currApi->initTime();
    return ret;
}

char *Syscalls::inet_ntop(int af, const void * src, char * dst, socklen_t size)
{
    if (dst == NULL)
        return dst;
    auto currApi = synchTaskTime();
    auto buf = SocketMap::getIP(((sockaddr*)src)->sin_addr.s_addr);
    memcpy(dst, buf.c_str(),std::min((unsigned)size,(unsigned)buf.length()));
    currApi->initTime();
    return dst;
}



int Syscalls::inet_pton(int af, const char * src, void * dst)
{
    auto currApi = synchTaskTime();
    if (af == AF_INET)
    {
        in_addr addr;
        addr.s_addr = SocketMap::getIP(src);
        memcpy(dst, &addr, sizeof(addr));
        currApi->initTime();
        return 1;
    }
    currApi->initTime();
    return -1;
}

unsigned int Syscalls::inet_addr(const char *cp)
{
    auto currApi = synchTaskTime();
    unsigned ret = SocketMap::getIP(cp);
    currApi->initTime();
    return ret;
}


ssize_t Syscalls::write(int fd, const void *buf, size_t count)
{
    auto currApi = synchTaskTime();
    auto ret = currApi->internal_send(fd, buf, count,0);
    currApi->initTime();
    return ret;
}

int Syscalls::socketpair(int domain, int type, int protocol, int sv[2])
{
    return -1;
}


int Syscalls::ioctl(int fd, unsigned long request, ...)
{
    return -1;
}

int Syscalls::getifaddrs (ifaddrs **__ifap)
{
    auto currApi = synchTaskTime();
    // Generate a ifaddr struct. Hoever only addr, and name are filled, all other 
    // attribute are left empty. 
    auto ret = currApi->internal_getifaddrs(__ifap);
    currApi->initTime();
    return ret;
}

void Syscalls::freeifaddrs ( ifaddrs *__ifa)
{
    // Delete only filled fields (addr and struct itself)
    auto currApi = synchTaskTime();
    ifaddrs* prev;
    while (__ifa != NULL)
    {
        delete __ifa->ifa_addr;
        prev = __ifa;
        __ifa= __ifa->ifa_next;
        delete prev;
    }
    currApi->initTime();
}
