# How adapt the Open62541 to the SCNSL simulator
In this file there are all the  instruction needed to allow the Open62541 library 
to the SCNSL simulation framework in posix architecture. Note however that the
publisher-subscriber implementation is not supported yet.

## CMake adaptation
In order  compiled simulations it is necessary that both Open62541 and SCNSL are
compiled in the same language and given that SCNSL is C++ only, it is necessary 
to compile the Open62541 in the same language. In order to so  it is necessary 
to edit the Open62541 main CMakeLists.txt as follows:
* search for the  
`` option(UA_FORCE_CPP "Force compilation with a C++ compiler"  OFF)`` 
line and replace ``OFF`` with ``ON`` to enable the C++ compilation.
* add the following lines : 
``SET(CMAKE_CXX_STANDARD "17")`` ,
``SET(CMAKE_CXX_EXTENSIONS "OFF")``

## Architecture changes
After having edited the CMake file, enter the ``arch/posix``directory. Replace 
files in here with the ones present in the ``posix`` directory where this Readme
is. Do not delete the CMake, since it is not necessary to replace it.

Optional: delete all other directories inside the ``arch`` folder. Remember then 
to edit the CMakeLists.txt present in the same folder to remove the 
``add_subdirectories`` command corresponding to deleted folders.  

## C++ compilation errrors for pubsub
To compile correctly the pubsub code it is necessary to replace some files in 
order to avoid compilation errors due C and C++ different behavior regarding 
``goto`` instruction. These files are:

* ``ua_client_highlevel.c`` and ``ua_client_subscriptions.c`` files in the
 ``client`` directory 
* ``ua_pubsub_writer.c`` in the ``pubsub`` directory (if working with pubsub 
    enabled)
