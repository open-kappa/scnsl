# How to adapt the libmodbus library to the SCNSL simulator

In order to allow the libmodbus library to work on SCNSL it is necessary to update
some of its files in order to compile SCNSL modules including it. Files which
needs to be edited are the ones present in the same directory this file is, and
have to be copied inside the ``src`` directory of a libmodbus installation.
 
## Files changes:

Here is a list of changed files with a summary of their modifications:
* **Makefile**: changed to compile the library using the C++ 17 standard. Also edited
to prevent the RTU components to be compiled, since not supported by SNCSL
* **Makefile.in**: same modificatons of the Makefile.
* **modbus.c**: removed the ``register`` keyworkd, forbidden in C++
* **modbus-private.h**: included the ``scnsl.h`` header
* **modbus-rtu.c**: removed headers in causing conflict with SCNSL
* **modbus-tcp.c**: updated header for to make compatible with SCNSL system calls