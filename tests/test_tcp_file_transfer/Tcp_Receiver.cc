#include "Tcp_Receiver.hh"

#include <fstream>
#include <sstream>


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Tcp_Receiver::Tcp_Receiver(const sc_core::sc_module_name modulename, 
                            const task_id_t id, Scnsl::Core::Node_t * n,
                            const size_t proxies, std::string output_file):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
    _output_file(output_file)
{

}

Tcp_Receiver::~Tcp_Receiver()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_Receiver::main()
{
    initTime();
    int sockfd, portno, n;
    struct sockaddr serv_addr;
    byte_t buffer[1024];
    std::string res;

    // std::ofstream file(_output_file);

    // if (!file.is_open()) 
    //     throw std::runtime_error("Error opening file");

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    portno = 2020;
    serv_addr.sin_port = portno;
    inet_pton(AF_INET, "192.168.0.2", &serv_addr.sin_addr);

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw std::runtime_error("ERROR connecting");
    int flen;

    recv(sockfd, buffer, sizeof(int), 0);

    memcpy(&flen, buffer, sizeof(int));

    std::cerr << "RECEIVER: File lenght: " << flen << std::endl;

    bzero(buffer, 1024);

    int total_byte = 0;

    std::string s;

    do
    {
        bzero(buffer, 1024);
        n = recv(sockfd, buffer, 1024, 0);
        if (n < 0)
            throw std::runtime_error("ERROR reading to socket");
        total_byte += n;
        std::cerr << "RECEIVER: got " << n << " bytes from sender, "
                  << total_byte << "/" << flen << std::endl;
        // file << std::string((char *)buffer, n);
    }
    while (total_byte < flen);
    std::cerr << "RECV: Closing" << std::endl;
    close(sockfd);
    // file.close();
}
