#include "Tcp_Receiver.hh"
#include "Tcp_Sender.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = scnsl->createNode();
        Scnsl::Core::Node_t * n1 = scnsl->createNode();

        // args = input file
        std::string filename;
        if (argc < 2)
            filename = "NONE";
        else
            filename = argv[1];

        sc_core::sc_time DELAY(90, sc_core::SC_US);
        unsigned int bitrate = 94200000;

        CoreChannelSetup_t csb;
        csb.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb.capacity = 100000000;
        csb.capacity2 = 100000000;
        csb.delay = sc_core::sc_time(DELAY);
        csb.extensionId = "core";
        csb.name = "channel_fullduplex";
        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(csb);

        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::size_t PROXIES = 1;

        Tcp_Sender ts("TaskServer", id0, n0, PROXIES, filename);
        Tcp_Receiver tc("TaskClient", id1, n1, PROXIES, "test_tcp_file_transfer_out.txt");

        // Creating the protocol Tcp:
        auto tcp0 = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp0", true);
        tcp0->setExtraHeaderSize(20, 14);
        tcp0->setSegmentSize(
            MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);

        auto tcp1 = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp1", true);
        tcp1->setExtraHeaderSize(20, 14);
        tcp1->setSegmentSize(
            MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        cts.print_trace_timestamp = true;

        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer1->addOutput(&std::cout);
        tracer1->trace(ch);
        tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp0));
        tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp1));
        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer2->addOutput(&std::cerr);
        // - Adding to trace:
        tracer2->trace(ch);
        tracer2->trace(Scnsl::Utils::EventsQueue_t::get_instance());
        tracer2->trace(dynamic_cast<Traceable_base_t *>(tcp0));
        tracer2->trace(dynamic_cast<Traceable_base_t *>(tcp1));

        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 0;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = bitrate;
        bsb0.node_binding.transmission_power = 1000;
        bsb0.node_binding.receiving_threshold = 1;

        bsb0.socket_binding.socket_active = true;
        bsb0.socket_binding.source_ip = SocketMap::getIP("192.168.0.2");
        bsb0.socket_binding.source_port = 2020;
        bsb0.socket_binding.dest_ip = SocketMap::getIP("192.168.0.1");
        bsb0.socket_binding.dest_port = 5050;

        scnsl->bind(n0, ch, bsb0);
        scnsl->bind(&ts, &tc, ch, bsb0, tcp0);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n0;
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 1;
        bsb1.node_binding.bitrate = bitrate;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        bsb1.socket_binding.socket_active = true;
        bsb1.socket_binding.source_ip = SocketMap::getIP("192.168.0.1");
        bsb1.socket_binding.source_port = 5050;
        bsb1.socket_binding.dest_ip = SocketMap::getIP("192.168.0.2");
        bsb1.socket_binding.dest_port = 2020;

        scnsl->bind(n1, ch, bsb1);
        scnsl->bind(&tc, &ts, ch, bsb1, tcp1);

        sc_core::sc_start(sc_core::sc_time(100, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
