// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask.hh"
#include "MyTask2.hh"
#include "MyTask4.hh"
#include "MyTask3.hh"
#include "MyTaskRouter.hh"

#include <map>

using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using Scnsl::Tracing::Traceable_base_t;


int sc_main( int argc, char * argv[] )
{
	try {

        Scnsl_t * sim = Scnsl_t::get_instance();


        // Nodes creation:
        Node_t * n1 = sim->createNode();
	Node_t * n2 = sim->createNode();

	Node_t * nodoRouter = sim->createNode();
	Node_t * nodoRouter2 = sim->createNode();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::FULL_DUPLEX;

        ccs.name = "SharedChannel";
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_MS );
        ccs.capacity = 1000;
        ccs.capacity2 = 1000;
        ccs.extensionId = "core";

        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );


	ccs.name = "SharedChannel2";
	Scnsl::Core::Channel_if_t * ch2 = sim->createChannel( ccs );


	ccs.name = "SharedChannel3";
	Scnsl::Core::Channel_if_t * ch3 = sim->createChannel( ccs );


        CoreCommunicatorSetup_t ccr1;
        ccr1.extensionId="core";
        ccr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr1.name = "macRouter_n1";
        ccr1.node=n1;

        CoreCommunicatorSetup_t ccr2;
        ccr2.extensionId="core";
        ccr2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr2.name = "macRouter_n2";
        ccr2.node=n2;

        CoreCommunicatorSetup_t ccoms1;
        ccoms1.extensionId="core";
        ccoms1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccoms1.name = "MacRouterRouting";
        ccoms1.node = nodoRouter;

        CoreCommunicatorSetup_t ccoms2;
        ccoms2.extensionId="core";
        ccoms2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccoms2.name = "MacRouterRouting2";
        ccoms2.node=nodoRouter2;


        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_n1;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_n1_second (ch,nodoRouter);
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_n2;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_n2_second (ch3,nodoRouter2);
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_gw;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t *> table_gw_second (ch,n1);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t *> table_gw_second_II (ch2,nodoRouter2);
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_gw2;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw2_second (ch2,nodoRouter);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw2_second_II (ch3,n2);

        table_n1[n2] = table_n1_second;
        table_n2[n1] = table_n2_second;

        table_gw[n1] = table_gw_second;
	table_gw[n2] = table_gw_second_II;

	table_gw2[n1] = table_gw2_second;
	table_gw2[n2] = table_gw2_second_II;

	ccoms1.routingTable = table_gw;
        ccoms2.routingTable = table_gw2;
        ccr1.routingTable = table_n1;
        ccr2.routingTable = table_n2;

        Communicator_if_t * mac1_router = sim->createCommunicator( ccr1 );
        Communicator_if_t * mac2_router = sim->createCommunicator( ccr2 );
        Communicator_if_t * macgwRouting1 = sim->createCommunicator( ccoms1 );
        Communicator_if_t * macgwRouting2 = sim->createCommunicator( ccoms2 );



        MyTask t1 ("T1",1,n1,1);
        MyTask3 t2 ("T2",4,n2,1);

        MyTaskRouter tr("ROUTER",0,nodoRouter,2);
        MyTaskRouter tr2("ROUTER2",2,nodoRouter2,2);


        /*
        //
	// TRACING
	//


	  // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer1->addOutput( & std::cout );
        // - Adding to trace:
        tracer1->trace( & t1 );
        tracer1->trace( & tr );
        tracer1->trace( & tr2 );
        tracer1->trace( & t2 );
        tracer1->trace( ch );
        tracer1->trace( ch2 );
        tracer1->trace( ch3 );
        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer2->addOutput( & std::cerr );
        // - Adding to trace:
        tracer2->trace( & t1 );
        tracer2->trace( & tr );
        tracer2->trace( & tr2 );
        tracer2->trace( & t2 );
        tracer2->trace( ch );
        tracer2->trace( ch2 );
        tracer2->trace( ch3 );
        tracer2->trace( Scnsl::Utils::EventsQueue_t::get_instance() );


	//
	//
	//

*/

        BindSetup_base_t bsbt1; // t1
        BindSetup_base_t bsbrouter; //router
        BindSetup_base_t bsbrouter2; //router2
        BindSetup_base_t bsbt2; // t2
        BindSetup_base_t bsbt3; // t3
        BindSetup_base_t bsbt4; // t4

        bsbt1.extensionId = "core";
        bsbt3.extensionId = "core";
        bsbrouter.extensionId = "core";
        bsbrouter2.extensionId = "core";
        bsbt2.extensionId = "core";
        bsbt4.extensionId = "core";


        bsbt1.destinationNode = nodoRouter;
        bsbt2.destinationNode = nullptr;
        bsbt3.destinationNode = nullptr;
        bsbt4.destinationNode = nullptr;


        bsbrouter.destinationNode = nullptr;
        bsbrouter2.destinationNode = nullptr;


        bsbt1.node_binding.x = 1;
        bsbt1.node_binding.y = 1;
        bsbt1.node_binding.z = 1;

        sim->bind(n1, ch, bsbt1);

        bsbrouter.node_binding.x = 4;
        bsbrouter.node_binding.y = 4;
        bsbrouter.node_binding.z = 4;

        sim->bind(nodoRouter, ch, bsbrouter);
        sim->bind(nodoRouter, ch2, bsbrouter);

        bsbrouter2.node_binding.x = 8;
        bsbrouter2.node_binding.y = 8;
        bsbrouter2.node_binding.z = 8;

        sim->bind(nodoRouter2, ch2, bsbrouter);
        sim->bind(nodoRouter2, ch3, bsbrouter);


        bsbt4.node_binding.x = 6;
        bsbt4.node_binding.y = 6;
        bsbt4.node_binding.z = 6;

        sim->bind(n2, ch3, bsbt4);

        sim->bind(&t1, &t2, ch, bsbt1, mac1_router);


        sim->bind(&tr, nullptr, ch, bsbrouter, macgwRouting1);
        sim->bind(&tr, nullptr, ch2, bsbrouter, macgwRouting1);

        sim->bind(&tr2, nullptr, ch2, bsbrouter2, macgwRouting2);
        sim->bind(&tr2, nullptr, ch3, bsbrouter2, macgwRouting2);

        sim->bind(&t2, nullptr, ch3, bsbt4, mac2_router);


        sc_core::sc_start( sc_core::sc_time( 5000, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}

    catch (exception & e)
    {
	cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
