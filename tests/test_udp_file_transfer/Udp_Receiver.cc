#include "Udp_Receiver.hh"

#include <fstream>
#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Udp_Receiver::Udp_Receiver(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies,
    std::string output_file):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
    _output_file(output_file)
{}

Udp_Receiver::~Udp_Receiver()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Receiver::main()
{
    initTime();
    int sockfd, portno, n;
    struct sockaddr recv_addr, src_addr;
    byte_t buffer[1024];
    std::string res;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    portno = 5050;
    recv_addr.sin_port = portno;
    inet_pton(AF_INET, "192.168.0.1", &recv_addr.sin_addr);

    if (bind(sockfd, (struct sockaddr *)&recv_addr, sizeof(recv_addr)) < 0)
        throw std::runtime_error("ERROR binding");

    int flen;
    uint addrlen;
    recvfrom(sockfd, buffer, sizeof(int), 0, &src_addr, &addrlen);

    memcpy(&flen, buffer, sizeof(int));

    std::cerr << "RECEIVER: File lenght: " << flen << "from "
              << src_addr.sin_addr.s_addr << " port " << src_addr.sin_port
              << std::endl;

    bzero(buffer, 1024);

    int total_byte = 0;

    std::string s;

    do
    {
        bzero(buffer, 1024);
        n = recvfrom(sockfd, buffer, 1024, 0, NULL, NULL);
        if (n < 0) throw std::runtime_error("ERROR reading to socket");
        total_byte += n;
        std::cerr << "RECEIVER: got " << n << " bytes from sender, "
                  << total_byte << "/" << flen << std::endl;
    }
    while (total_byte < flen);
    std::cerr << "RECV: Closing" << std::endl;
    close(sockfd);
}
