#include "Udp_Sender.hh"

#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Udp_Sender::Udp_Sender(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies,
    std::string input_file):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
    _input_file(input_file)
{
    if (input_file == "NONE")
        _file_valid = false;
    else
        _file_valid = true;
}

Udp_Sender::~Udp_Sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Sender::main()
{
    initTime();
    int sockfd, portno;
    socklen_t clilen;
    char buffer[1024];
    int length;
    int total_size = 0;
    std::ifstream in;
    struct sockaddr recv_addr;
    wait(1, sc_core::SC_MS);
    if (_file_valid)
    {
        in.open(_input_file, std::ifstream::binary);
        in.seekg(0, in.end);
        length = in.tellg();
        in.seekg(0, in.beg);
    }
    else
        length = 231605634;  // 200 Mb file

    int n;
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) throw std::runtime_error("ERROR opening socket");

    inet_pton(AF_INET, "192.168.0.1", &recv_addr.sin_addr);
    recv_addr.sin_port = 5050;

    int byte_read;

    std::cerr << "SENDER: File lenght: " << length << std::endl;

    byte_t l[sizeof(int)];
    memcpy(l, &length, sizeof(length));

    n = sendto(sockfd, l, sizeof(int), 0, &recv_addr, sizeof(recv_addr));
    if (n < 0) throw std::runtime_error("socket error");
    do
    {
        if (_file_valid)
        {
            in.read(buffer, 1024);
            byte_read = in.gcount();
        }
        else
        {
            int read_val = std::min(1024, length - total_size);
            for (int i = 0; i < read_val; i++)
                buffer[i] = static_cast<char>(rand() % 25 + 65);
            byte_read = read_val;
            total_size += read_val;
            std::cerr << "length: " << length << " total bytes: " << total_size
                      << std::endl;
        }
        if (byte_read)
        {
            n = Scnsl::Syscalls::sendto(
                sockfd,
                (byte_t *)(buffer),
                byte_read,
                0,
                &recv_addr,
                sizeof(recv_addr));
            if (n < 0) throw std::runtime_error("ERROR writing to socket");
        }
    }
    while (byte_read);

    if (n < 0) throw std::runtime_error("ERROR writing to socket");

    in.close();
    close(sockfd);
}
