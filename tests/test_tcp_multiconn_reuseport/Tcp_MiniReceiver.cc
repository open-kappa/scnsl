#include "Tcp_MiniReceiver.hh"

#include <fstream>
#include <sstream>

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Syscalls;
using Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t;
using Scnsl::Syscalls::send;

Tcp_Receiver::Tcp_Receiver(const sc_core::sc_module_name modulename, 
                            const task_id_t id, Scnsl::Core::Node_t * n,
                            const size_t proxies):
    // Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_Receiver::~Tcp_Receiver()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_Receiver::main()
{
    initTime();
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[10];

    sockaddr_in serv_addr, cli_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        throw std::runtime_error("NO socket");
    }

    int reuse = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char *)& reuse,
                    sizeof(reuse)) < 0)
        std::cerr << "internal_setsockopt(SO_REUSEADDR) failed" << std::endl;

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const char *)& reuse,
                    sizeof(reuse)) < 0)
        std::cerr << "internal_setsockopt(SO_REUSEPORT) failed" << std::endl;
    portno = 5050;
    inet_pton(AF_INET, "192.168.0.6", &serv_addr.sin_addr);
    serv_addr.sin_port = portno;

    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        throw "NO bind";
    }
    listen(sockfd, 1);
    clilen = sizeof(cli_addr);
    std::string file;
    int n;

    for (int i = 0; i < 10; i++)
    {
        newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
        //Uncomment to set NODELAY option
        // nt yes = 1;
        // int res = 
        //     setsockopt(newsockfd,IPPROTO_TCP,TCP_NODELAY,(char*)&yes,sizeof(yes));
        if (newsockfd < 0)
        {
            throw std::runtime_error("Error internal_accept");
        }

        n = recv(newsockfd, buffer, 10, 0);
        std::cerr << "received: " << buffer << std::endl;
        if (n < 0) throw std::runtime_error("socket error");
        close(newsockfd);
    }
    internal_close(sockfd);
}
