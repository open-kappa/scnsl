//
// Created by elia on 31/07/20.
//
/// @file
/// A simple TLM task.

#include "Tcp_MiniSender_reuseport.hh"

#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Syscalls;
using Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t;
using Scnsl::Syscalls::send;

Tcp_Sender::Tcp_Sender(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_Sender::~Tcp_Sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_Sender::main()
{
    initTime();
    int sockfd, portno, n;
    sockaddr_in serv_addr, my_addr;

    char buffer[10] = "PINGPONG!";
    int bufsize = 10;

    for (int i = 0; i < 10; i++)
    {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        int reuse = 1;
        if (sockfd < 0)
        {
            std::cout << "NO socket:" << errno << std::endl;
        }
        // REUSE PORT SET
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const char *)&reuse,
                        sizeof(reuse)) < 0)
            std::cerr << "internal_setsockopt(SO_REUSEPORT) failed: " << std::endl;

        portno = 2020;
        inet_pton(AF_INET, "192.168.0.5", &my_addr.sin_addr);
        my_addr.sin_port = portno;

        // BIND LOOP
        if (bind(sockfd, (struct sockaddr *)& my_addr, sizeof(my_addr)) < 0)
        {
            std::cerr << "NO bind: " << errno << std::endl;
        }

        portno = 5050;
        serv_addr.sin_port = portno;

        inet_pton(AF_INET, "192.168.0.6", &serv_addr.sin_addr);

        while (connect(sockfd, (struct sockaddr *)& serv_addr, sizeof(serv_addr)) < 0)
        {
            wait(sc_core::sc_time(1, sc_core::SC_MS));
        }

        std::cerr << "Send " << i << "/10" << std::endl;
        n = Scnsl::Syscalls::send(sockfd, buffer, bufsize, 0);

        wait(100, sc_core::SC_US);

        if (n < 0)
        {
            std::cerr << "\n ERROR in send: " << errno << std::endl;
        }
        close(sockfd);
        wait(100, sc_core::SC_US);
    }

    std::cout << "Ending" << std::endl;
}
