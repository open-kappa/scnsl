// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A TLM task.

#include <sstream>

#include "MyTask2.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTask2::MyTask2( sc_core::sc_module_name modulename,
                const task_id_t id,
                Scnsl::Core::Node_t * n,
                const size_t proxies )
    :
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies )
{
	SC_THREAD( writingProcess );
}


MyTask2::~MyTask2()  {}


void MyTask2::writingProcess()
{
	for ( ;; )
	{
		byte_t i = byte_t( rand()%25 + 97 );

		const std::string tp = "0";
		const size_t s = 1;

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << i;
		std::cout << ", packet size: " << s << " bytes" << std::endl;
		TlmTask_if_t::send( tp, &i, s);
		wait( 11000, sc_core::SC_US );
	}

}



void MyTask2::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ){

	t=t;
	char result;
	bool c;

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
	{
    	result = static_cast< char >( * p.get_data_ptr() );
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data received: " << result;
		std::cout << ", packet size: " << p.get_data_length() << " bytes" << std::endl;
	}

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND))
	{
		c = *p.get_data_ptr() != 0;
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", carrier: " << c << std::endl;
	}

}
