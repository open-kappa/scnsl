// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask.hh"
#include "MyTask2.hh"
#include "MyTask4.hh"
#include "MyTask3.hh"
#include "MyTaskRouter.hh"

#include <map>

using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using Scnsl::Tracing::Traceable_base_t;


int sc_main( int argc, char * argv[] )
{
	try {

        Scnsl_t * sim = Scnsl_t::get_instance();


        // Nodes creation:
        Node_t * n1 = sim->createNode();
	Node_t * n2 = sim->createNode();
	Node_t * n3 = sim->createNode();
	Node_t * n4 = sim->createNode();
	Node_t * nodoRouter = sim->createNode();

	// Channels:
        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;

        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs.nodes_number = 5;
        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );
        Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha);
        // n1
        CoreCommunicatorSetup_t cc1;
        cc1.extensionId = "core";
        cc1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc1.name = "mac802_15_4_n1";
        cc1.node = n1;

        cc1.ack_required = false;
        cc1.short_addresses = false;

        CoreCommunicatorSetup_t ccr1;
        ccr1.extensionId="core";
        ccr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr1.name = "macRouter_n1";
        ccr1.node=n1;


 	// n2
        CoreCommunicatorSetup_t cc2;
        cc2.extensionId = "core";
        cc2.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc2.name = "mac802_15_4_n2";
        cc2.node = n2;

        cc2.ack_required = false;
        cc2.short_addresses = false;

        CoreCommunicatorSetup_t ccr2;
        ccr2.extensionId="core";
        ccr2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr2.name = "macRouter_n2";
        ccr2.node=n2;

        // n3
        CoreCommunicatorSetup_t cc3;
        cc3.extensionId = "core";
        cc3.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc3.name = "mac802_15_4_n3";
        cc3.node = n3;

        cc3.ack_required = false;
        cc3.short_addresses = false;

        CoreCommunicatorSetup_t ccr3;
        ccr3.extensionId="core";
        ccr3.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr3.name = "macRouter_n3";
        ccr3.node=n3;

        // n4
        CoreCommunicatorSetup_t cc4;
        cc4.extensionId = "core";
        cc4.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc4.name = "mac802_15_4_n4";
        cc4.node = n4;

        cc4.ack_required = false;
        cc4.short_addresses = false;

        CoreCommunicatorSetup_t ccr4;
        ccr4.extensionId="core";
        ccr4.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr4.name = "macRouter_n4";
        ccr4.node=n4;

   	// gateway node :
        CoreCommunicatorSetup_t ccoms1;
        ccoms1.extensionId = "core";
        ccoms1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        ccoms1.name = "MacRouter802";
        ccoms1.node = nodoRouter;
        ccoms1.ack_required = false;
        ccoms1.short_addresses = false;

        CoreCommunicatorSetup_t ccoms2;
        ccoms2.extensionId="core";
        ccoms2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccoms2.name = "MacRouterRouting";
        ccoms2.node=nodoRouter;



        // all router tables :

        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_nodes;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t *> table_nodes_second (ch,nodoRouter);
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_gw;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second (ch,n1);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second_II (ch,n2);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second_III (ch,n3);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second_IV (ch,n4);

        table_nodes[n1] = table_nodes_second;
        table_nodes[n2] = table_nodes_second;
        table_nodes[n3] = table_nodes_second;
        table_nodes[n4] = table_nodes_second;


        table_gw[n1] = table_gw_second;
        table_gw[n2] = table_gw_second_II;
        table_gw[n3] = table_gw_second_III;
	table_gw[n4] = table_gw_second_IV;


	ccr1.routingTable = table_nodes;
        ccr2.routingTable = table_nodes;
        ccr3.routingTable = table_nodes;
        ccr4.routingTable = table_nodes;
        ccoms2.routingTable = table_gw;


        Communicator_if_t * mac1_802 = sim->createCommunicator( cc1 );
        Communicator_if_t * mac1_router = sim->createCommunicator( ccr1 );
        mac1_802->stackUp(mac1_router);
        mac1_router->stackDown(mac1_802);

        Communicator_if_t * mac2_802 = sim->createCommunicator( cc2 );
        Communicator_if_t * mac2_router = sim->createCommunicator( ccr2 );
        mac2_802->stackUp(mac2_router);
        mac2_router->stackDown(mac2_802);

        Communicator_if_t * mac3_802 = sim->createCommunicator( cc3 );
        Communicator_if_t * mac3_router = sim->createCommunicator( ccr3 );
        mac3_802->stackUp(mac3_router);
        mac3_router->stackDown(mac3_802);

        Communicator_if_t * mac4_802 = sim->createCommunicator( cc4 );
        Communicator_if_t * mac4_router = sim->createCommunicator( ccr4 );
        mac4_802->stackUp(mac4_router);
        mac4_router->stackDown(mac4_802);

        Communicator_if_t * macgw802 = sim->createCommunicator( ccoms1 );
        Communicator_if_t * macgwRouting = sim->createCommunicator( ccoms2 );
        macgw802->stackUp(macgwRouting);
        macgwRouting->stackDown(macgw802);

	Communicator_if_t * macStackN1 = new Scnsl::Utils::CommunicatorStack_t(mac1_router, mac1_802);
	Communicator_if_t * macStackN2 = new Scnsl::Utils::CommunicatorStack_t(mac2_router, mac2_802);
	Communicator_if_t * macStackN3 = new Scnsl::Utils::CommunicatorStack_t(mac3_router, mac3_802);
	Communicator_if_t * macStackN4 = new Scnsl::Utils::CommunicatorStack_t(mac4_router, mac4_802);
	Communicator_if_t * macStackGW1 = new Scnsl::Utils::CommunicatorStack_t(macgwRouting, macgw802);


	cout << "*** T1,T2 SENDER - T3,T4 RECEIVE *** \n";
	cout << "*** T1 -> T3,  T2 -> T4 *** \n";

        MyTask t1 ("T1",1,n1,1);
        MyTask2 t2 ("T2",2,n2,1);
        MyTask3 t3 ("T3",3,n3,1);
        MyTask4 t4 ("T4",4,n4,1);

        MyTaskRouter tr("ROUTER",0,nodoRouter,1);


        //
	// TRACING
	//
/*

	  // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer1->addOutput( & std::cout );
        // - Adding to trace:
        tracer1->trace( & t1 );
        tracer1->trace( & tr );
        tracer1->trace( & t2 );
        tracer1->trace( & t3 );
        tracer1->trace( ch );
        tracer1->trace( ch2 );
        tracer1->trace( ch3 );
        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer2->addOutput( & std::cerr );
        // - Adding to trace:
        tracer2->trace( & t1 );
        tracer2->trace( & tr );
        tracer2->trace( & t2 );
        tracer2->trace( & t3 );
        tracer2->trace( ch );
        tracer2->trace( ch2 );
        tracer2->trace( ch3 );
        tracer2->trace( Scnsl::Utils::EventsQueue_t::get_instance() );


	//
	//
	//
*/



        BindSetup_base_t bsbt1;
        BindSetup_base_t bsbrouter;
        BindSetup_base_t bsbt2;
        BindSetup_base_t bsbt3;
        BindSetup_base_t bsbt4;

        bsbt1.extensionId = "core";
        bsbt3.extensionId = "core";
        bsbrouter.extensionId = "core";
        bsbt2.extensionId = "core";
        bsbt4.extensionId = "core";

        // --------------------------------------

        bsbt1.destinationNode = nodoRouter;
        bsbt2.destinationNode = nodoRouter;
        bsbt3.destinationNode = nodoRouter;
        bsbt4.destinationNode = nodoRouter;


        bsbrouter.destinationNode = nullptr;

        // ---------------------------------------

        bsbt1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt1.node_binding.transmission_power = 100;
        bsbt1.node_binding.receiving_threshold = 10;
        bsbt1.node_binding.x = 1;
        bsbt1.node_binding.y = 1;
        bsbt1.node_binding.z = 1;

        sim->bind(n1, ch, bsbt1);

        bsbrouter.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbrouter.node_binding.transmission_power = 100;
        bsbrouter.node_binding.receiving_threshold = 10;
        bsbrouter.node_binding.x = 4;
        bsbrouter.node_binding.y = 4;
        bsbrouter.node_binding.z = 4;

        sim->bind(nodoRouter, ch, bsbrouter);

	bsbt2.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt2.node_binding.transmission_power = 100;
        bsbt2.node_binding.receiving_threshold = 10;
        bsbt2.node_binding.x = 5;
        bsbt2.node_binding.y = 5;
        bsbt2.node_binding.z = 5;

        sim->bind(n2, ch, bsbt2);

        bsbt3.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt3.node_binding.transmission_power = 100;
        bsbt3.node_binding.receiving_threshold = 10;
        bsbt3.node_binding.x = 7;
        bsbt3.node_binding.y = 7;
        bsbt3.node_binding.z = 7;

        sim->bind(n3, ch, bsbt3);

        bsbt4.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt4.node_binding.transmission_power = 100;
        bsbt4.node_binding.receiving_threshold = 10;
        bsbt4.node_binding.x = 9;
        bsbt4.node_binding.y = 9;
        bsbt4.node_binding.z = 9;

        sim->bind(n4, ch, bsbt4);


        sim->bind(&t1, &t3, ch, bsbt1, macStackN1);
        sim->bind(&t2, &t4, ch, bsbt2, macStackN2);

        sim->bind(&tr, nullptr, ch, bsbrouter, macStackGW1);

        sim->bind(&t3, nullptr, ch, bsbt3, macStackN3);
        sim->bind(&t4, nullptr, ch, bsbt4, macStackN4);

        sc_core::sc_start( sc_core::sc_time( 5000, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}

    catch (exception & e)
    {
	cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
