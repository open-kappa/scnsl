// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_MY_TASK_ROUTER_HH
#define SCNSL_MY_TASK_ROUTER_HH



/// @file
/// Source node.


#include <systemc>
#include <tlm.h>


// Including the library:
#include <scnsl.hh>


class MyTaskRouter :
    public Scnsl::Tlm::TlmTask_if_t
{

public:

    SC_HAS_PROCESS( MyTaskRouter );

    /// @brief Constructor.
    ///
    /// @param name This module name.
    /// @param id this module unique ID.
    /// @param n The node on which this task is placed.
    /// @param proxies The number of connected task proxies.
    /// @throw std::invalid_argument If proxies is zero.
    ///
	MyTaskRouter( sc_core::sc_module_name name,
            const task_id_t id,
            Scnsl::Core::Node_t * n,
            const size_t proxies );

    virtual ~MyTaskRouter();

	virtual void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

protected:

    /// @name Processes.
    //@{

    void sendPacket();

    //@}

private:

    MyTaskRouter( MyTaskRouter & );

    MyTaskRouter & operator = ( MyTaskRouter & );

};

#endif
