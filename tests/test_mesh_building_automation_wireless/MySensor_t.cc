// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include "MySensor_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////



struct Payload_t
{
   unsigned int id;
   double sender_times;
   int Temperature;
};

MySensor_t::MySensor_t( const sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies ):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    // Fields:
    _packetArrivedEvent(), taskid()
{

    SC_THREAD( _sender );


}


MySensor_t::~MySensor_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void MySensor_t::b_transport( tlm::tlm_generic_payload & , sc_core::sc_time &  )
{
}

// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////


void MySensor_t::_sender()  {
    const std::string tp = "0";
    Payload_t *p = static_cast<Payload_t *>(malloc(sizeof(Payload_t)*sizeof(p)));

    int randTemp = 0;
    for ( ;; ) {
        p->Temperature = (rand()%25 + 25 );
	randTemp = (rand()%50 + 50);
	wait(randTemp, sc_core::SC_MS);
        std::cout << "Sender name " << name() << " ,Temperature=" << p->Temperature << std::endl;;
        p->sender_times = sc_core::sc_time_stamp().to_double();
        TlmTask_if_t::send( tp, reinterpret_cast<byte_t *>(p), sizeof(Payload_t));
    }
}
