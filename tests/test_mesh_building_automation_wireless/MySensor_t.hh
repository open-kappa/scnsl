// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef MySensor_t_HH
#define MySensor_t_HH

#include <systemc>
#include <scnsl.hh>

class MySensor_t:
    public Scnsl::Tlm::TlmTask_if_t
{
public:

    SC_HAS_PROCESS( MySensor_t );
    MySensor_t( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies );

    /// @brief Virtual destructor.
    virtual
    ~MySensor_t();

    /// @name Inherited interface methods.
    //@{


    virtual
    void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

    //@}

    /// @brief Signals when a packet is arrived.
    sc_core::sc_event _packetArrivedEvent;
    unsigned int taskid;
private:

    /// @name Processes.
    //@{

    /// @brief Sender process.
    void _sender();



    /// @brief Disabled copy constructor.
    MySensor_t( const MySensor_t & );

    /// @brief Disabled assignment operator.
    MySensor_t & operator = ( MySensor_t & );
};



#endif
