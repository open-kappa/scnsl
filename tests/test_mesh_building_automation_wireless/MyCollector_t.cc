// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include "MyCollector_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////
struct Payload_t
{
  int id;			//4
  double sender_times;        //8
  int Temperature;
};

MyCollector_t::MyCollector_t( const sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies ):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    // Fields:
    _packetArrivedEvent()
{

}


MyCollector_t::~MyCollector_t()  {
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void MyCollector_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t )
{
	Payload_t *temp=nullptr;
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND)) {
    	    temp = reinterpret_cast<Payload_t *>( p.get_data_ptr() );
            double txtime=(temp->sender_times);
            double  rxtime=sc_core::sc_time_stamp().to_double();
            std::cout << "Collector name: " << name() <<
			 " RECEIVED data: " << temp->Temperature <<
			 ", size: " << p.get_data_length() <<
			 " delay "<< (rxtime-txtime)*1e-12 << std::endl;
	}
    	else {
           // ERROR.
           SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );

           // Just to avoid compiler warnings:
           t = sc_core::sc_time_stamp();
    }
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////


void MyCollector_t::_sender()
{
    const std::string tp = "0";
    const size_t s = 1;

    for ( ;; ) {
        byte_t i = static_cast<Scnsl::Core::byte_t>( rand()%25 + 65 );
	std::cout << "SEND data: " << i << " size: " << s << ".";
        wait( 20, sc_core::SC_MS ); TlmTask_if_t::send( tp, &i, s);
    }

}
