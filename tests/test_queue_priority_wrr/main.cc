// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask.hh"


using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;


int sc_main( int argc, char * argv[] )
{
	try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n1 = sim->createNode();
        Scnsl::Core::Node_t * n2 = sim->createNode();


		CoreCommunicatorSetup_t ccs;
		ccs.extensionId = "core";
		ccs.type = CoreCommunicatorSetup_t::QUEUE;
		ccs.queueSend = CoreCommunicatorSetup_t::PRIORITY;
		ccs.queueReceive = CoreCommunicatorSetup_t::NONE;
		ccs.capacitySend = 5;
		ccs.capacityReceive = 5;
		ccs.numberSend = 2;
		ccs.numberReceive = 2;
		ccs.policySend = CoreCommunicatorSetup_t::W_ROUND_ROBIN;
		ccs.policyReceive = CoreCommunicatorSetup_t::W_ROUND_ROBIN;
		unsigned long weight[ 2 ] = { 3, 1 };
		ccs.weightSend = weight;
		ccs.weightReceive = weight;
		ccs.name = "ccs_priority";

		Communicator_if_t * coda = sim->createCommunicator( ccs );



        CoreChannelSetup_t csb;

        csb.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb.capacity = 1000;
		csb.capacity2 = 1000;
        csb.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb.extensionId = "core";
        csb.name = "channel_fullduplex";

        Scnsl::Core::Channel_if_t * ch1 = sim->createChannel( csb );

        BindSetup_base_t bsb1;
        BindSetup_base_t bsb2;
        bsb1.extensionId = "core";
        bsb2.extensionId = "core";
        sim->bind(n1, ch1, bsb1);
        sim->bind(n2, ch1, bsb2);

        MyTask t1 ( "task1", 1, n1, 1 );
        MyTask t2 ( "task2", 2, n2, 1 );

        sim->bind(&t1, nullptr, ch1, bsb1, coda);
        sim->bind(&t2, nullptr, ch1, bsb2, nullptr);

        sc_core::sc_start( sc_core::sc_time( 100, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}
    catch (exception & e)
    {
		cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
