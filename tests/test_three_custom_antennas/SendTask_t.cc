// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A simple TLM task.

#if(defined _MSC_VER)
  #define _USE_MATH_DEFINES
#endif

#include <cmath>
#include <sstream>
#include "SendTask_t.hh"

#define RAD

using namespace Scnsl::antennaModels;
using namespace sc_core;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////
SendTask_t::SendTask_t(
                       const sc_core::sc_module_name modulename,
                       const task_id_t id,
                       Scnsl::Core::Node_t * n,
                       const size_t proxies,
                       const double angle_granularity,
                       const sc_core::sc_time movementPeriod,
                       const double distance,
                       const double height,
                       const int advConst,
                       const int randMax,
                       const int interchannelFreespace,
                       const sc_core::sc_time endtime,
					             const uint8_t beacon_num):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    _modulename(modulename),
    _angle_granularity(angle_granularity),
    _movementPeriod(movementPeriod),
    _radius(distance),
    _height(height),
    _advConst(advConst),
    _randMax(randMax),
    _interchannelFreespace(interchannelFreespace),
    _endtime(endtime),
  	_beacon_num(beacon_num)
{
    SC_THREAD( _sender );
}


SendTask_t::~SendTask_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////
void SendTask_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & /*t*/ )
{
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND) )
    {
        // ntd
    }
    else if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND) )
    {
        // ntd
    }
    else
    {
        // ERROR.
        SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );
    }
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////
void SendTask_t::_sender()
{
    const std::string tp0 = "0";
    const std::string tp1 = "1";
    const std::string tp2 = "2";
    Scnsl::Core::node_properties_t properties = getNodeProperties(tp0);

    // variables for the advertising timing
    double advInterval_base = 625*_advConst;
    double remainingIntervalTime;
    double freespace;
    if (_interchannelFreespace == -1)
    {
        // case -1 the freespace is equal to the patcket trasnsmission time.
        freespace = 368;
    } else
    {
        freespace = _interchannelFreespace;
    }


    // variables for the packet content
    uint8_t channel_oct = {0x00};
    uint8_t data2send = {0x00};
    // ibeacon packet: [preamble (1B), access address (4B), PDU (2+36B for apple devices), CRC (3B)]
    // ibeacon packet: [preamble (1B), access address (4B), PDU (2+36B for apple devices), CRC (3B)]
    //invio 46 byte
    uint8_t content[46] = {0xaa,
                           0x8e,0x89,0xbe,0xd6,
                           0x20,0x00,
                           0xff,0xee,0xdd,0xcc,0xbb,0xaa,0x99,0x88,0x77,0x66,0x55,0x44,0x33,0x22,0x11,0x00,
                           0x37,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                           0x11,0x22,0x33};
    // IMPORTANT: first field after UUID contatins the currect advertising channel

    // first wait for a random amount of time then start;
    int current_channel = 37;
    double r = rand() % (int(advInterval_base));
    wait( r, sc_core::SC_US );

    while ( sc_core::sc_time_stamp() < _endtime )
    {
        if ( current_channel == 37 )
        {
      			if(data2send == 127)
      				data2send = 0;
      			else
	            data2send++;

            std::cout<<  "\033[1;34m    **** " << sc_core::sc_time_stamp() << " : Triggering new advertising event  \033[0m" << std::endl;
            channel_oct = {0x37};
        }
        else if ( current_channel == 38 )
        {
            channel_oct = {0x38};
        }
        else if ( current_channel == 39 )
        {
            channel_oct = {0x39};
        }

        memcpy(content + 23, & channel_oct , 2);
        memcpy(content + 24, & data2send , 2); //incrementale
        memcpy(content + 25, & _beacon_num , 2); //nel main mettere un numero random per beacon

        std::stringstream dummy_sstr, ss_content;
        dummy_sstr << std::hex << data2send;
        std::string dummy_str = dummy_sstr.str();
        const char * dummy_char = dummy_str.c_str();
        char single_char = dummy_char[0];
        ss_content << std::hex << int(single_char);
        std::string str2send = ss_content.str();

        properties = getNodeProperties("2");
        std::cout<< _modulename << " @ " << sc_core::sc_time_stamp()
                 << ": sending " << sizeof(content) << " bytes on channel "
                 << current_channel << ", data = " << str2send << std::endl;

        // Printed double to the report is a composition of the channel (decimal part)
        // and the packet number (entire part) for the sake of a correct identification

        std::stringstream ss;
        ss << std::stoul(str2send, nullptr, 16) << "." << current_channel;
        std::string report_str = ss.str();

		    std::cout<< "SENDER "
                 << sc_core::sc_time_stamp()
                 << " " << report_str
                 << " " << properties.x
                 << " " << properties.y
                 << " " << properties.z
                 << std::endl;

        #define NO_SHOW_CONTENT
        #ifdef SHOW_CONTENT
            printf("content is: ");
            for (i = 0; i < sizeof content; i++)
                printf("%02x ", content[i]);
            printf("\n");
        #endif

        if ( current_channel == 37 )
        {
           TlmTask_if_t::send(tp0, content, sizeof(content));
           wait( freespace, sc_core::SC_US );
           current_channel = 38;
        }
        else if ( current_channel == 38 )
        {
           TlmTask_if_t::send(tp1, content, sizeof(content));
           wait( freespace, sc_core::SC_US );
           current_channel = 39;
        }
        else if ( current_channel == 39 )
        {
           TlmTask_if_t::send(tp2, content, sizeof(content));
           // random amount in us
           r = rand() % (_randMax * 1000);
           remainingIntervalTime = advInterval_base + r - (freespace * 5);
           wait( remainingIntervalTime, sc_core::SC_US );
           current_channel = 37;
        }
    }
}
