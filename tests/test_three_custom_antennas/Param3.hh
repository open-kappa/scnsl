
// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <vector>

//*** Antenna model and position
const std::string antennaModel = "QUARTERWAVE_DIPOLE";
const double distance = 2;
const double height = 0;

const int bitrate_val = 100000;
const int transmis_power = 100;
//*** Movement model
const double granularity = 15;				// beacon movement of 15 degrees
const double movementConstant = 120;  // every 2 minutes
//const double granularity = 1;           // Angular step in degrees of the Tx beacon
//const double movementConstant = 22;     // Period of movement in milliseconds (see below)
// for 10°/sec set movementConstant = 100;
// for 45°/sec set movementConstant = 22;
// for 90°/sec set movementConstant = 11;
const sc_core::sc_time movementPeriod (movementConstant, sc_core::SC_SEC);

//*** Receiver parameters
const int receiverBufferSize = 1;       // Size of the average buffer within the receiver.

//*** I2C parameters
const sc_core::sc_time pollingPeriod (1500, sc_core::SC_MS);

//*** BLE Advertising parameters
// advertising interval is roughly 625*advConst us.
// to advertise each 0.5s set advConst=800.
const int advConst = 800;               // Integer multiplicator to define the advertising interval.
const int randMax = 10;                 // Max random time to wait after an advertising event.
const int interchannelFreespace = -1;   // Time between consecutive transmissions within the same event.
const uint8_t beacon_num = 9;
const Scnsl::Core::size_t PROXIES = 3;

//*** BLE Scanning parameters
// Scanning windows is 625*scanWinConst us.
// Active portion of the scanning windows is 625*scanConst us.
// For continuous scanning set scanWinConst=scanConst.
const int scanConst = 2000;             // Integer multiplicator to define the active portion of the scanning window.
const int scanWinConst = 2000;          // Integer multiplicator to define the total scanning window interval.

//*** simulation endtime: define custom value...
// const sc_core::sc_time endtime (15000, sc_core::SC_MS);
//*** ... or use the following to simulate three beacon rotations around the head...

// const sc_core::sc_time endtime ((movementConstant*360*3)/granularity, sc_core::SC_MS);
const sc_core::sc_time endtime ((movementConstant*195)/granularity, sc_core::SC_SEC);



//#define CUSTOMANTENNAS
#define ISOTROPICANTENNAS

#ifdef CUSTOMANTENNAS
    // this coordinate represents the Theta versor, Phy versor is vertical (down) by default
    const double front_versor[3] = { 0 , 1 , 0 };
    const double left_versor[3] = { -1 , 0 , 0 };
    const double right_versor[3] = { 1 , 0 , 0 };
#endif


// Possible defines for attenuators:
// - NO_ATTENUATION
// - SHARP_ATTENUATION_180: 0to-8 dB transition at 0° and 180° (with straight antennas this will produce figure 14)
// - GRADUAL_ATTENUATION_180: same as above but gradual
// - SHARP_RIGHT_LEFT: additional -8 dB sharp attenuatiors on top of right antenna and at bottom of left antenna, front antemma still attenuated at 0-180°
// - GRADUAL_RIGHT_LEFT: same as above but gradual, inner attenuation is stronger than -8dB beacuse of the combined effect of head and the external attenuator.
#define NO_ATTENUATION


#ifdef NO_ATTENUATION
 const std::vector<std::vector<double> > front_attenuation { {0, 0}, {180, 0} };
 const std::vector<std::vector<double> > left_attenuation { {0, 0}, {180, 0} };
 const std::vector<std::vector<double> > right_attenuation { {0, 0}, {180, 0} };
#endif

#ifdef SHARP_ATTENUATION_180
 const std::vector<std::vector<double> > front_attenuation { {0, 0}, {0.01, -8}, {179.99, -8}, {180, 0} };
 const std::vector<std::vector<double> > left_attenuation { {0, 0}, {0.01, -8}, {179.99, -8}, {180, 0} };
 const std::vector<std::vector<double> > right_attenuation { {0, 0}, {0.01, -8}, {179.99, -8}, {180, 0} };
#endif

#ifdef GRADUAL_ATTENUATION_180
 const std::vector<std::vector<double> > front_attenuation { {0, 0}, {15, -8}, {140, -8}, {180, 0} };
 const std::vector<std::vector<double> > left_attenuation { {0, 0}, {15, -8}, {140, -8}, {180, 0} };
 const std::vector<std::vector<double> > right_attenuation { {0, 0}, {15, -8}, {140, -8}, {180, 0} };
#endif

#ifdef SHARP_RIGHT_LEFT
 const std::vector<std::vector<double> > front_attenuation { {0, 0}, {0.01, -8}, {179.99, -8}, {180, 0} };
 const std::vector<std::vector<double> > left_attenuation { {0, -8}, {179.99, -8}, {180, 0}, {270, 0}, {270.01, -8} };
 const std::vector<std::vector<double> > right_attenuation { {0, 0}, {0.01, -8}, {269.99, -8}, {270, 0} };
#endif

#ifdef GRADUAL_RIGHT_LEFT
 const std::vector<std::vector<double> > front_attenuation { {0, 0}, {15, -8}, {140, -8}, {180, 0} };
 const std::vector<std::vector<double> > left_attenuation { {0, -8}, {15, -12}, {30, -16}, {85, -16}, {95, -8}, {140, -8}, {180, 0}, {265, 0}, {275, -8}};
 const std::vector<std::vector<double> > right_attenuation { {0, 0}, {15, -8}, {95, -8}, {110, -11}, {140, -8}, {265, -8}, {275, 0} };
#endif

const double threshold = 1e-12;
