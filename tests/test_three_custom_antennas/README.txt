DESCRIPTION
***************************************************************************************
This test has 4 antennas as inputs: 
	* 3 custom antennas, placed on vertexes of an equlateral triangle
	* 1 isotropic antenna placed at the center of such a triangle
The central antenna transmits a power received by the other threee antennas with the same power. 
***************************************************************************************
Varying the distance will lead to a change of the received power.
#######################################################################################
REMINDER:
Before running the test, Copy the .rad file where the test binary is placed

