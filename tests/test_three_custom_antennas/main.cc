// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
//#include <systemc> già presente in SendTask_t.hh
#include <tlm.h>
//#include <scnsl.hh> già presente in SendTask_t.hh
#include <string>
#include <exception>
#include <cstdlib>
#include "ReceiveTask_t.hh"
#include "SendTask_t.hh"
#include "Param3.hh"
#include "scnsl/antennaModels/IsotropicAntenna_t.hh"

using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::antennaModels;

static void setChannelProperties(
        CoreChannelSetup_t & ch,
        ExtensionId_t extensionId,
        ModuleName_t name,
        CoreChannelSetup_t::channel_type_t type,
        CoreChannelSetup_t::counter_t nodes_number,
        CoreChannelSetup_t::delay_t delay,
        double alpha,
        double radio_frequency ){

    ch.extensionId	= extensionId;
    ch.channel_type = type;
    ch.name = name;
    ch.alpha = alpha;
    ch.nodes_number = nodes_number;
    ch.radio_frequency = radio_frequency;
    ch.delay = delay;
}

static void setBindProperties(
        BindSetup_base_t &bsb,
        ExtensionId_t extensionId,
        Scnsl::Core::Node_t *destination,
        double x,
        double y,
        double z,
        bitrate_t bitrate,
        power_t transmission_power,
        power_t receiving_threshold,
        RadiationPattern_if_t* pattern)
{
    bsb.extensionId = extensionId;
    bsb.destinationNode = destination;
    bsb.node_binding.x = x;
    bsb.node_binding.y = y;
    bsb.node_binding.z = z;
    bsb.node_binding.bitrate = bitrate;
    bsb.node_binding.transmission_power = transmission_power;
    //THRESHOLD
    bsb.node_binding.receiving_threshold =  receiving_threshold;
    bsb.node_binding.lobes.push_back(pattern);
}


int sc_main( int argc, char * argv[] )
{
    if (argc != 2)
    {
        std::cerr << "Expected one argument: radiation_pattern.rad file";
        exit(EXIT_FAILURE);
    }
    const std::string file_rad = argv[1];

    try
    {
        //////////////////////////////// INSTANZA DEL SIMULATORE //////////////////////
        // Singleton.
        Scnsl_t * sim = Scnsl_t::get_instance();

        ///////////////////////// CREAZIONE DEGLI OGGETTI ////////////////////////////
        // Nodes creation:
        Node_t * n0 = sim->createNode();
        Node_t * n_left = sim->createNode();
        Node_t * n_frontal = sim->createNode();
        Node_t * n_right = sim->createNode();

        // Coordinates creation:
        Coordinate_t Tcoord = Coordinate_t(false, 0,0,1);
        Coordinate_t Pcoord = Coordinate_t(false, 0,1,0);
        Coordinate_t Polar  = Coordinate_t(false, 0,0,1);

        /////////////////////////////  CHANNEL SETUP /////////////////////////////////
        // Channels creation:
        // channel, extensionId, name, type, nodes_number, delay, alpha, radio_frequency
        CoreChannelSetup_t ch37;
        setChannelProperties(ch37, "core", "SharedChannel37", CoreChannelSetup_t::SHARED, 4, sc_core::sc_time(2.0, sc_core::SC_US), 2, 25);
        Channel_if_t * blech_37 = sim->createChannel( ch37 );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ch37.alpha, ch37.radio_frequency);

        CoreChannelSetup_t ch38;
        setChannelProperties(ch38, "core", "SharedChannel38", CoreChannelSetup_t::SHARED, 4, sc_core::sc_time(2.0, sc_core::SC_US), 2, 25);
        Channel_if_t * blech_38 = sim->createChannel( ch38 );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ch38.alpha, ch38.radio_frequency);

        CoreChannelSetup_t ch39;
        setChannelProperties(ch39, "core", "SharedChannel39", CoreChannelSetup_t::SHARED, 4, sc_core::sc_time(2.0, sc_core::SC_US), 2, 25);
        Channel_if_t * blech_39 = sim->createChannel( ch39 );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ch39.alpha, ch39.radio_frequency);


        ///////////////// CREAZIONE DEI TASK //////////////////////////////////////
        SendTask_t t0( "Task0", 0, n0, 3, granularity, movementPeriod, distance, height, advConst, randMax, interchannelFreespace, endtime, beacon_num); //beacn numer è un numero random, scelgo 9
        ReceiveTask_t t_frontal( "TaskFrontal", 1, n_frontal, PROXIES, receiverBufferSize, scanConst, scanWinConst, endtime);
        ReceiveTask_t t_left( "TaskLeft", 2, n_left, PROXIES, receiverBufferSize, scanConst, scanWinConst, endtime);
        ReceiveTask_t t_right( "TaskRight", 3, n_right, PROXIES, receiverBufferSize, scanConst, scanWinConst, endtime);


        //////////////// BLE INFRASTRUCTURE ///////////////////////////////////
        // ANTENNA MODELS
        IsotropicAntenna_t adp_n0= IsotropicAntenna_t(Tcoord, Pcoord, Polar, true);	// Tx antenna (beacon 0)
        // CustomAntenna_t adp_n0= CustomAntenna_t(Tcoord, Pcoord, Polar, true);	// Tx antenna (beacon 0)

        // defining Rx front antenna
#ifdef ISOTROPICANTENNAS
        IsotropicAntenna_t adp_front= IsotropicAntenna_t(Tcoord, Pcoord, Polar, true);
        // CustomAntenna_t adp_front= CustomAntenna_t(Tcoord, Pcoord, Polar, true);
#endif

        // defining Rx left antenna
#ifdef ISOTROPICANTENNAS
        IsotropicAntenna_t adp_sx= IsotropicAntenna_t(Tcoord, Pcoord, Polar, true);
        // CustomAntenna_t adp_front= CustomAntenna_t(Tcoord, Pcoord, Polar, true);
#endif

        // defining Rx right antenna
#ifdef ISOTROPICANTENNAS
        IsotropicAntenna_t adp_dx= IsotropicAntenna_t(Tcoord, Pcoord, Polar, true);
        // CustomAntenna_t adp_front= CustomAntenna_t(Tcoord, Pcoord, Polar, true);
#endif


        /////////////////// BIND INPUT ///////////////////////////////////////
        // Tx for channel 37:
        // struttura: BindSetup_base_t, extensionId, destinationNode, x,y,z, bitrate, transmission_power, threshold, radiationPattern
        BindSetup_base_t bsbin_0_37;
        setBindProperties(bsbin_0_37, "core", nullptr, (sqrt(27)/3)*0.01,0,1, bitrate_val, transmis_power, threshold, &adp_n0);
        sim->bind(n0, blech_37, bsbin_0_37);
        sim->bind(&t0, nullptr, blech_37, bsbin_0_37, nullptr);

        // Tx binding for channel 38:
        BindSetup_base_t bsbin_0_38;
        setBindProperties(bsbin_0_38, "core", nullptr, (sqrt(27)/3)*0.01,0,1, bitrate_val, transmis_power, threshold, &adp_n0);
        sim->bind(n0, blech_38, bsbin_0_38);
        sim->bind(&t0, nullptr, blech_38, bsbin_0_38, nullptr);

        // Tx binding for channel 39:
        BindSetup_base_t bsbin_0_39;
        setBindProperties(bsbin_0_39, "core", nullptr, (sqrt(27)/3)*0.01,0,1, bitrate_val, transmis_power, threshold, &adp_n0);
        sim->bind(n0, blech_39, bsbin_0_39);
        sim->bind(&t0, nullptr, blech_39, bsbin_0_39, nullptr);


        /////////////////// BIND OUTPUT ///////////////////////////////////////
        // ------ Frontal binding
        // channel 37:
        BindSetup_base_t bsbout_frontal_37;
        setBindProperties(bsbout_frontal_37, "core", nullptr, sqrt(27)*0.01,0,0, bitrate_val, transmis_power, threshold, &adp_front);
        sim->bind(n_frontal, blech_37, bsbout_frontal_37);
        sim->bind(&t_frontal, nullptr, blech_37, bsbout_frontal_37, nullptr);

        // channel 38:
        BindSetup_base_t bsbout_frontal_38;
        setBindProperties(bsbout_frontal_38, "core", nullptr, sqrt(27)*0.01,0,0, bitrate_val, transmis_power, threshold, &adp_front);
        sim->bind(n_frontal, blech_38, bsbout_frontal_38);
        sim->bind(&t_frontal, nullptr, blech_38, bsbout_frontal_38, nullptr);

        // channel 39:
        BindSetup_base_t bsbout_frontal_39;
        setBindProperties(bsbout_frontal_39, "core", nullptr, sqrt(27)*0.01,0,0, bitrate_val, transmis_power, threshold, &adp_front);
        sim->bind(n_frontal, blech_39, bsbout_frontal_39);
        sim->bind(&t_frontal, nullptr, blech_39, bsbout_frontal_39, nullptr);

        // ------ Left binding
        // channel 37:
        BindSetup_base_t bsbout_left_37;
        setBindProperties(bsbout_left_37, "core", nullptr, 0,0.03,0, bitrate_val, transmis_power, threshold, &adp_sx);
        sim->bind(n_left, blech_37, bsbout_left_37);
        sim->bind(&t_left, nullptr, blech_37, bsbout_left_37, nullptr);

        // channel 38:
        BindSetup_base_t bsbout_left_38;
        setBindProperties(bsbout_left_38, "core", nullptr, 0,0.03,0, bitrate_val, transmis_power, threshold, &adp_sx);
        sim->bind(n_left, blech_38, bsbout_left_38);
        sim->bind(&t_left, nullptr, blech_38, bsbout_left_38, nullptr);

        // channel 39:
        BindSetup_base_t bsbout_left_39;
        setBindProperties(bsbout_left_39, "core", nullptr, 0,0.03,0, bitrate_val, transmis_power, threshold, &adp_sx);
        sim->bind(n_left, blech_39, bsbout_left_39);
        sim->bind(&t_left, nullptr, blech_39, bsbout_left_39, nullptr);


        // ------ Right binding
        // channel 37:
        BindSetup_base_t bsbout_right_37;
        setBindProperties(bsbout_right_37, "core", nullptr, 0,-0.03,0, bitrate_val, transmis_power, threshold, &adp_dx);
        sim->bind(n_right, blech_37, bsbout_right_37);
        sim->bind(&t_right, nullptr, blech_37, bsbout_right_37, nullptr);

        // channel 38:
        BindSetup_base_t bsbout_right_38;
        setBindProperties(bsbout_right_38, "core", nullptr, 0,-0.03,0, bitrate_val, transmis_power, threshold, &adp_dx);
        sim->bind(n_right, blech_38, bsbout_right_38);
        sim->bind(&t_right, nullptr, blech_38, bsbout_right_38, nullptr);

        // channel 39:
        BindSetup_base_t bsbout_right_39;
        setBindProperties(bsbout_right_39, "core", nullptr, 0,-0.03,0, bitrate_val, transmis_power, threshold, &adp_dx);
        sim->bind(n_right, blech_39, bsbout_right_39);
        sim->bind(&t_right, nullptr, blech_39, bsbout_right_39, nullptr);

        sc_core::sc_start();
    }
    catch (std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
