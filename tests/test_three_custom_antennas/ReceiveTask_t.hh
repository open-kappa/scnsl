// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef MYTASK_T_HH
#define MYTASK_T_HH

//#include <fstream>
#include <systemc>
#include <scnsl.hh>

/// @file
/// A simple TLM task.


/// @brief A simple TLM task.
///
/// Design patterns:
///
/// - Non copyable.
/// - Non assignable.
///
class ReceiveTask_t: public Scnsl::Tlm::TlmTask_if_t {
  public:
      typedef std::map<Scnsl::Tlm::tlm_taskproxy_id_t, std::string> ProxyMap;
      SC_HAS_PROCESS( ReceiveTask_t );

      /// @brief Constuctor.
      ///
      /// @param modulename This module name.
      /// @param id this task ID.
      /// @param n The relative host node.
      /// @param proxies The number of bounded proxies.
      ReceiveTask_t(
                    const sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies,
                    const int avgBufferSize,
                    const int scanConst,
                    const int scanWinConst,
                    const sc_core::sc_time endtime);

      // @brief Virtual destructor.
      virtual
      ~ReceiveTask_t();

      virtual
      void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );


  private:
      sc_core::sc_module_name _modulename;
      sc_core::sc_time _endtime;
      int _scanConst;
      int _scanWinConst;
      std::string _activeChannel;
      unsigned int _receiverID;
      const int _avgBufferSize;
      std::vector<std::vector<int>> _rawRssi;
      int _numberOfMeasurements;
      int _lastIndex;

      // packet content is [master address, WID, RSSI, crc, BEACON]
      // there is one packet for each beacon
      void _interleaver();
      void _pushPowerBuffer(double power, int beacon);
      /// @brief Disabled copy constructor.
      ReceiveTask_t( const ReceiveTask_t & );
      /// @brief Disabled assignment operator.
      ReceiveTask_t & operator = ( ReceiveTask_t & );
}; //fine classe ReceiveTask_t

#endif
