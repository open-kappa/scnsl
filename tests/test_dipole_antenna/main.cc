// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "ReceiveTask_t.hh"
#include "SendTask_t.hh"
#include "scnsl/antennaModels/QuarterWaveDipole_t.hh"

using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::antennaModels;


int sc_main( int /*argc*/, char * /*argv*/[] )
{
    try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = sim->createNode();
        Scnsl::Core::Node_t * n1 = sim->createNode();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;
        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
        ccs.nodes_number = 2;
        ccs.delay = sc_core::sc_time(10.0, sc_core::SC_MS);

        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ccs.alpha);

        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;

        const Scnsl::Core::size_t PROXIES = 1;

        SendTask_t t0( "Task0", id0, n0, PROXIES);
        ReceiveTask_t t1( "Task1", id1, n1, PROXIES );

        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 10;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = 100;
        bsb0.node_binding.transmission_power = 1500;
        bsb0.node_binding.receiving_threshold = 1;

        sim->bind(n0, ch, bsb0);
        sim->bind(&t0, &t1, ch, bsb0, nullptr);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n0;
        bsb1.node_binding.x = 0;
        bsb1.node_binding.y = 0;
        bsb1.node_binding.z = 0;
        bsb1.node_binding.bitrate = 100;
        bsb1.node_binding.transmission_power = 100;
        bsb1.node_binding.receiving_threshold = 1;

        Coordinate_t theta = Coordinate_t(false, 0, 0, 1);
        QuarterWaveDipole_t *adp= new QuarterWaveDipole_t(theta, true, 0);
        bsb1.node_binding.lobes.push_back(adp);
        sim->bind(n1, ch, bsb1);
        sim->bind(&t1, &t0, ch, bsb1, nullptr);

        sc_core::sc_start();
    }
    catch ( std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
