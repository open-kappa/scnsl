// Copyright (C) 2008
// by all contributors.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef MYTASK_CLIENT_HH
#define MYTASK_CLIENT_HH

#include <scnsl.hh>
#include <systemc>
#include <scnsl/system_calls/TimedSyscalls.hh>

class Tcp_client: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param is_sender True if is sender.
    /// @param test The kind of test.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    Tcp_client(
        const sc_core::sc_module_name modulename,
        const task_id_t id,
        Scnsl::Core::Node_t * n,
        const size_t proxies);

    /// @brief Virtual destructor.
    virtual ~Tcp_client();

private:
    /// @name Processes.
    // @}
    void main() override;
    //@{
};

#endif
