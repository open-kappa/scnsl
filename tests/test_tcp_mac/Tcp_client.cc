/// @file
/// A simple TLM task.

//#include <sstream>
#include "Tcp_client.hh"
// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using namespace Scnsl;

Tcp_client::Tcp_client(const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_client::~Tcp_client()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_client::main()
{
    initTime();
    int sockfd, portno, n;
    struct sockaddr serv_addr;
    byte_t buffer[256];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    timeval t;
    std::cerr << "time: " << gettimeofday(&t, NULL) << std::endl;

    portno = 2020;
    serv_addr.sin_port = portno;

    inet_pton(AF_INET, "192.168.0.5", &serv_addr.sin_addr);
    
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw("ERROR connecting");

    for (int i = 0; i < 256; i++)
    {
        buffer[i] = byte_t(rand() % 25 + 65);
    }

    std::cerr << " CLIENT: SENDING DATA" << std::endl;
    n = Syscalls::send(sockfd, buffer, 25, 0);

    if (n < 0)
        throw("ERROR writing to socket");
    bzero(buffer, 256);

    n = recv(sockfd, buffer, 255, 0);

    if (n < 0) throw("ERROR reading from socket");
    printf("%s\n", buffer);
    close(sockfd);
}
