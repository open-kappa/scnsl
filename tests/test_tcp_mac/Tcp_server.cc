/// @file
/// A simple TLM task.

#include "Tcp_server.hh"

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Tcp_server::Tcp_server(const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_server::~Tcp_server()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_server::main()
{
    initTime();
    int sockfd, newsockfd, portno;
    byte_t buffer[256];
    socklen_t clilen;
    struct sockaddr serv_addr, cli_addr;
    int n;
    // create a socket
    // socket(int domain, int type, int protocol)
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
        throw "ERROR opening socket";

    serv_addr.sin_addr.s_addr = INADDR_ANY;

    portno = 2020;
    serv_addr.sin_port = portno;

    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw("ERROR on binding");

    std::cerr << "\tS: CHECKPOINT 2" << std::endl;

    listen(sockfd, 5);

    std::cerr << "\tS: CHECKPOINT 3" << std::endl;

    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);

    std::cerr << "\tS: CHECKPOINT 4" << std::endl;

    if (newsockfd < 0)
        throw("ERROR on accept");

    std::cerr << "\tS: CHECKPOINT 5" << std::endl;

    bzero(buffer, 256);

    n = recv(newsockfd, buffer, 255, 0);

    std::cerr << "\tS: CHECKPOINT 6" << std::endl;

    if (n < 0)
        throw("ERROR reading from socket");
    printf("Here is the message: %s\n", buffer);

    Scnsl::Syscalls::send(newsockfd, (byte_t *)"Hello, world!\n", 14, 0);

    close(newsockfd);
    close(sockfd);
}
