#include "Tcp_client.hh"
#include "Tcp_server.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = scnsl->createNode();
        Scnsl::Core::Node_t * n1 = scnsl->createNode();

        CoreChannelSetup_t csb;
        csb.channel_type = CoreChannelSetup_t::SHARED;
        csb.extensionId = "core";
        csb.name = "SharedChannel";
        csb.alpha = 0.1;
        csb.delay = sc_core::sc_time(1.0, sc_core::SC_US);
        csb.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(csb);
        Scnsl::Utils::DefaultEnvironment_t::createInstance(csb.alpha);

        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::size_t PROXIES = 1;

        Tcp_server ts("TaskServer", id0, n0, PROXIES);
        Tcp_client tc("TaskClient", id1, n1, PROXIES);

        // Creating the protocol Tcp:

        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        // First instance:
        ccoms.name = "MacServer";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac0 = scnsl->createCommunicator(ccoms);
        // Second instance:
        ccoms.name = "MacClient";
        ccoms.node = n1;
        Scnsl::Core::Communicator_if_t * mac1 = scnsl->createCommunicator(ccoms);

        ccoms.type = CoreCommunicatorSetup_t::LV4_COMMUNICATOR;
        ccoms.name = "TcpServer";
        Scnsl::Core::Communicator_if_t * tcp0 = scnsl->createCommunicator(ccoms);
        ((Lv4Communicator_t *)tcp0)->setExtraHeaderSize(10);
        ((Lv4Communicator_t *)tcp0)->setSegmentSize(25);

        ccoms.name = "TcpClient";
        Scnsl::Core::Communicator_if_t * tcp1 = scnsl->createCommunicator(ccoms);
        ((Lv4Communicator_t *)tcp1)->setExtraHeaderSize(10);
        ((Lv4Communicator_t *)tcp1)->setSegmentSize(25);

        tcp0->stackDown(mac0);
        mac0->stackUp(tcp0);
        Communicator_if_t * stack0 = new Scnsl::Utils::CommunicatorStack_t(tcp0, mac0);

        tcp1->stackDown(mac1);
        mac1->stackUp(tcp1);
        Communicator_if_t * stack1 = new Scnsl::Utils::CommunicatorStack_t(tcp1, mac1);

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        cts.print_trace_timestamp = true;
        cts.info = 5;
        cts.debug = 5;
        cts.log = 5;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer2->addOutput(&std::cerr);
        // - Adding to trace:
        tracer2->trace(dynamic_cast<Traceable_base_t *>(tcp1));
        tracer2->trace(dynamic_cast<Traceable_base_t *>(tcp0));

        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 0;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = 94160000;
        bsb0.node_binding.transmission_power = 1000;
        bsb0.node_binding.receiving_threshold = 1;

        bsb0.socket_binding.socket_active = true;
        bsb0.socket_binding.source_ip = SocketMap::getIP("192.168.0.5");
        bsb0.socket_binding.source_port = 2020;
        bsb0.socket_binding.dest_ip = SocketMap::getIP("192.168.0.6");
        bsb0.socket_binding.dest_port = 5050;

        scnsl->bind(n0, ch, bsb0);
        scnsl->bind(&ts, &tc, ch, bsb0, stack0);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n0;
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 1;
        bsb1.node_binding.bitrate = 94160000;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        bsb1.socket_binding.socket_active = true;
        bsb1.socket_binding.source_ip = SocketMap::getIP("192.168.0.6");
        bsb1.socket_binding.source_port = 5050;
        bsb1.socket_binding.dest_ip = SocketMap::getIP("192.168.0.5");
        bsb1.socket_binding.dest_port = 2020;

        scnsl->bind(n1, ch, bsb1);
        scnsl->bind(&tc, &ts, ch, bsb1, stack1);

        std::cerr << "Starting simulation" << std::endl;
        sc_core::sc_start(sc_core::sc_time(30, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
