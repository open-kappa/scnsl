// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask_t.hh"


using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using Scnsl::Tracing::Traceable_base_t;

int sc_main( int argc, char * argv[] )
{
	try {

        // Which testcase? (from 1 to 3)
        unsigned int TESTCASE = 0;
        if ( argc == 2 )
        {
            std::stringstream ss;
            ss << argv[ 1 ];
            ss >> TESTCASE;
        }
        if ( TESTCASE > 3 || TESTCASE < 1 )
        {
            TESTCASE = 1;
        }

        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = scnsl->createNode();
        Scnsl::Core::Node_t * n1 = scnsl->createNode();


        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;
        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs.nodes_number = 2;

        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel( ccs );
        Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha);

        const bool IS_SENDER = true;
        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::size_t PROXIES = 1;

        MyTask_t t0( "Task0", IS_SENDER, TESTCASE, id0, n0, PROXIES );
        MyTask_t t1( "Task1", ! IS_SENDER, TESTCASE, id1, n1, PROXIES );

        // Creating the protocol 802.15.4:
        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        // First instance:
        ccoms.name = "Mac0";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac0 = scnsl->createCommunicator( ccoms );
        // Second instance:
        ccoms.name = "Mac1";
        ccoms.node = n1;
        Scnsl::Core::Communicator_if_t * mac1 = scnsl->createCommunicator( ccoms );

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        //cts.print_trace_type = true;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer( cts );
        // - Setting the output stream:
        tracer1->addOutput( & std::cout );
        // - Adding to trace:
        tracer1->trace( & t0 );
        tracer1->trace( & t1 );
        tracer1->trace( ch );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac0) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac1) );
        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer( cts );
        // - Setting the output stream:
        tracer2->addOutput( & std::cerr );
        // - Adding to trace:
        tracer2->trace( & t0 );
        tracer2->trace( & t1 );
        tracer2->trace( ch );
        tracer2->trace( Scnsl::Utils::EventsQueue_t::get_instance() );
        tracer2->trace( dynamic_cast<Traceable_base_t*>(mac0) );
        tracer2->trace( dynamic_cast<Traceable_base_t*>(mac1) );


        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 0;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb0.node_binding.transmission_power = 100;
        bsb0.node_binding.receiving_threshold = 10;

        scnsl->bind(n0, ch, bsb0);
        scnsl->bind(&t0, &t1, ch, bsb0, mac0);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = nullptr; // don't care: it receives only...
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 1;
        bsb1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        scnsl->bind(n1, ch, bsb1);
        scnsl->bind(&t1, &t0, ch, bsb1, mac1);


        sc_core::sc_start( sc_core::sc_time( 100, sc_core::SC_MS ) );
        sc_core::sc_stop();
	}
    catch ( std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
	}
	return 0;
}
