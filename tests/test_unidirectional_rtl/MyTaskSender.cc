// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A RTL task.

#include <sstream>
#include "MyTaskSender.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTaskSender::MyTaskSender( sc_core::sc_module_name modulename,
                            const task_id_t id,
                            Scnsl::Core::Node_t * n,
                            const size_t proxies )
    :
    // Parents:
    Scnsl::Rtl::RtlTask_if_t<8>( modulename, id, n, proxies )
{
	SC_THREAD( writingProcess );
	sensitive << packetSendCompleted[ 0 ];

	SC_THREAD( carrierProcess );
    sensitive << carrier[ 0 ];
}

MyTaskSender::~MyTaskSender()
{
    // ntd
}


void MyTaskSender::writingProcess()
{

	unsigned int i = 0;
	for ( ;; )
	{

		sc_dt::sc_uint<8> data = rand() % 25 + 65;

		byte_t b = byte_t( data );

		outputPacketSize[0].write(8);
		outputPacket[0].write( data );
		outputPacketLabel[0].write(0);
		newOutputPacket[0].write( ++ i );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << b;
		std::cout << ", packet size: " << data.length()/8 << " bytes" << std::endl;

		wait();
	}
}


void MyTaskSender::carrierProcess()
{
	for ( ;; )
	{
		wait();
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", carrier: " << carrier[0].read() << std::endl;
	}
}
