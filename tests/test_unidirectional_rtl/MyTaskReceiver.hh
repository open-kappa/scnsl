// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_MY_TASK_RECEIVER_HH
#define SCNSL_MY_TASK_RECEIVER_HH



/// @file
/// Source node.


#include <systemc>

// Allowing Scnsl::Rtl::LinkNode_t usage:
//#define SCNSL_TLM_USE_LINK_NODE_HH

// Including the library:
#include <scnsl.hh>



class MyTaskReceiver :
    public Scnsl::Rtl::RtlTask_if_t<8>
{

public:

    SC_HAS_PROCESS( MyTaskReceiver );

    /// @brief Constructor.
    ///
    /// @param name This module name.
    /// @param id this module unique ID.
    /// @param n The node on which this task is placed.
    /// @param proxies The number of connected task proxies.
    /// @throw std::invalid_argument If proxies is zero.
    ///
	MyTaskReceiver( sc_core::sc_module_name name,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies );

    virtual ~MyTaskReceiver();
protected:

    /// @name Processes.
    //@{

    void readingProcess();

    void carrierProcess();

    //@}

private:

    MyTaskReceiver( MyTaskReceiver & );

    MyTaskReceiver & operator = ( MyTaskReceiver & );

};

#endif
