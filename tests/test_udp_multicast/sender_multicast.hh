#ifndef MULTICAST_TASK_SEND_HH
#define MULTICAST_TASK_SEND_HH

#include <scnsl.hh>
#include <scnsl/system_calls/TimedSyscalls.hh>
#include <systemc>

class Udp_Mcast_sender:
    public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param is_sender True if is sender.
    /// @param test The kind of test.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    Udp_Mcast_sender(
        const sc_core::sc_module_name modulename,
        const task_id_t id,
        Scnsl::Core::Node_t * n,
        const size_t proxies);

    /// @brief Virtual destructor.
    virtual ~Udp_Mcast_sender();

private:
    /// @name Processes.
    // @}
    void main() override;
    //@{
};

#endif
