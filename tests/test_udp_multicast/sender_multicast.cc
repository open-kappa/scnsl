#include "sender_multicast.hh"
// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using namespace Scnsl;

Udp_Mcast_sender::Udp_Mcast_sender(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{}

Udp_Mcast_sender::~Udp_Mcast_sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Mcast_sender::main()
{
    initTime();
    char * group = "255.255.255.0";
    int port = 5050;
    const char * message = "Hello, World!";

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
    {
        throw std::runtime_error("error in socket");
    }

    // set up destination address
    //
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));

    inet_pton(AF_INET, group, &addr.sin_addr);
    addr.sin_port = htons(port);

    // now just sendto() our destination!
    //
    char ch = 0;
    int nbytes = sendto(
        fd,
        message,
        strlen(message),
        0,
        (struct sockaddr *)&addr,
        sizeof(addr));
    if (nbytes < 0)
    {
        throw std::runtime_error("error in sendto");
    }

    struct ip_mreq mreq;
    inet_pton(AF_INET, group, &mreq.imr_multiaddr.s_addr);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(
            fd,
            IPPROTO_IP,
            IP_ADD_MEMBERSHIP,
            (char *)&mreq,
            sizeof(mreq))
        < 0)
    {
        throw std::runtime_error("error in bind");
    }

    char msgbuf[50];
    socklen_t addrlen = sizeof(addr);
    nbytes = recvfrom(fd, msgbuf, 50, 0, (struct sockaddr *)&addr, &addrlen);
    if (nbytes < 0)
    {
        throw std::runtime_error("error in recvfrom");
    }
    msgbuf[nbytes] = '\0';
    puts(msgbuf);
    std::cerr << nbytes << std::endl;
}
