#include "receiver_multicast.hh"
// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using namespace Scnsl;

Udp_Mcast_recv::Udp_Mcast_recv(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
    MSGBUFSIZE(256)
{}

Udp_Mcast_recv::~Udp_Mcast_recv()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Mcast_recv::main()
{
    initTime();
    char * group = "255.255.255.0";
    int port = 5050;
    const char * message = "Hello, World!";

    /**
     *
     * MISSING SOME FUNCTIONS, JUST TO TEST IF IT WORKS
     *
     * */

    // create what looks like an ordinary UDP socket
    //
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
    {
        throw std::runtime_error("error in socket");
    }

    // allow multiple sockets to use the same PORT number
    //
    u_int yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) < 0)
    {
        throw std::runtime_error("error sockoption reuseaddr");
    }

    // set up destination address
    //
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);  // differs from sender
    addr.sin_port = htons(port);

    // bind to receive address
    //
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        throw std::runtime_error("error in bind");
    }

    // use setsockopt() to request that the kernel join a multicast group
    //

    // COMMENTING BECAUSE NOT YET IMPLEMENTED. IT SHOULD BE OK

    struct ip_mreq mreq;
    inet_pton(AF_INET, group, &mreq.imr_multiaddr.s_addr);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(
            fd,
            IPPROTO_IP,
            IP_ADD_MEMBERSHIP,
            (char *)&mreq,
            sizeof(mreq))
        < 0)
    {
        throw std::runtime_error("error in bind");
    }

    // now just enter a read-print loop
    //
    char msgbuf[MSGBUFSIZE];
    socklen_t addrlen = sizeof(addr);
    int nbytes = recvfrom(
        fd,
        msgbuf,
        MSGBUFSIZE,
        0,
        (struct sockaddr *)&addr,
        &addrlen);
    if (nbytes < 0)
    {
        throw std::runtime_error("error in recvfrom");
    }
    msgbuf[nbytes] = '\0';
    std::cout << name() << " ";
    puts(msgbuf);

    struct sockaddr_in newaddr;
    memset(&newaddr, 0, sizeof(addr));

    inet_pton(AF_INET, group, &newaddr.sin_addr);
    newaddr.sin_port = addr.sin_port;

    // now just sendto() our destination!
    //
    char ch = 0;
    const char * resp = name();
    nbytes = sendto(
        fd,
        resp,
        strlen(resp),
        0,
        (struct sockaddr *)&newaddr,
        sizeof(newaddr));
    if (nbytes < 0)
    {
        throw std::runtime_error("error in sendto");
    }
}
