#include "receiver_multicast.hh"
#include "sender_multicast.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = scnsl->createNode();
        Scnsl::Core::Node_t * n1 = scnsl->createNode();
        Scnsl::Core::Node_t * n2 = scnsl->createNode();

        CoreChannelSetup_t csb;
        csb.channel_type = CoreChannelSetup_t::SHARED;
        csb.extensionId = "core";
        csb.name = "SharedChannel";
        csb.alpha = 0.1;
        csb.delay = sc_core::sc_time(1.0, sc_core::SC_US);
        csb.nodes_number = 3;
        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(csb);
        Scnsl::Utils::DefaultEnvironment_t::createInstance(csb.alpha);

        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::size_t PROXIES = 1;

        Udp_Mcast_sender sender_t("Sender", id0, n0, PROXIES);
        Udp_Mcast_recv recv_1("Receiver_1", id1, n1, PROXIES);
        Udp_Mcast_recv recv_2("Receiver_2", 2, n2, PROXIES);

        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;

        ccoms.type = CoreCommunicatorSetup_t::LV4_COMMUNICATOR;
        ccoms.name = "UdpSender";
        Scnsl::Core::Communicator_if_t * sender = scnsl->createCommunicator(
            ccoms);
        ((Lv4Communicator_t *)sender)->setExtraHeaderSize(10);

        ccoms.name = "UdpRecv_1";
        Scnsl::Core::Communicator_if_t * receiver_1 = scnsl->createCommunicator(
            ccoms);
        ((Lv4Communicator_t *)receiver_1)->setExtraHeaderSize(10);

        ccoms.name = "UdpRecv_2";
        Scnsl::Core::Communicator_if_t * receiver_2 = scnsl->createCommunicator(
            ccoms);
        ((Lv4Communicator_t *)receiver_2)->setExtraHeaderSize(10);

        // // Adding tracing features:
        // CoreTracingSetup_t csender;
        // csender.extensionId = "core";
        // // - Setting the formatter:
        // csender.formatterExtensionId = "core";
        // csender.formatterName = "basic";
        // // Setting the filter:
        // csender.filterExtensionId = "core";
        // csender.filterName = "basic";
        // // Core formatter specific option:
        // // printing also the type of trace:
        // csender.print_trace_type = true;
        // csender.print_trace_timestamp = true;
        // csender.info = 5;
        // csender.debug = 5;
        // csender.log = 5;
        // csender.error = 5;
        // csender.warning = 5;
        // csender.fatal = 5;
        // // - Creating:
        // Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer(csender);
        // // - Setting the output stream:
        // tracer2->addOutput(&std::cerr);
        // // - Adding to trace:
        // tracer2->trace(dynamic_cast<Traceable_base_t *>(receiver_1));
        // tracer2->trace(dynamic_cast<Traceable_base_t *>(receiver_2));
        // tracer2->trace(dynamic_cast<Traceable_base_t *>(sender));

        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 0;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = 94160000;
        bsb0.node_binding.transmission_power = 1000;
        bsb0.node_binding.receiving_threshold = 1;

        bsb0.socket_binding.socket_active = true;
        bsb0.socket_binding.source_ip = SocketMap::getIP("192.168.0.5");
        bsb0.socket_binding.source_port = 2020;
        bsb0.socket_binding.dest_ip = SocketMap::getIP("255.255.255.0");
        bsb0.socket_binding.dest_port = 5050;

        scnsl->bind(n0, ch, bsb0);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n1;
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 1;
        bsb1.node_binding.bitrate = 94160000;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        bsb1.socket_binding.socket_active = true;
        bsb1.socket_binding.source_ip = SocketMap::getIP("192.168.0.6");
        bsb1.socket_binding.source_port = 5050;
        bsb1.socket_binding.dest_ip = SocketMap::getIP("255.255.255.0");
        bsb1.socket_binding.dest_port = 2020;

        scnsl->bind(n1, ch, bsb1);

        BindSetup_base_t bsb2;
        bsb2.extensionId = "core";
        bsb2.destinationNode = n1;
        bsb2.node_binding.x = 2;
        bsb2.node_binding.y = 2;
        bsb2.node_binding.z = 2;
        bsb2.node_binding.bitrate = 94160000;
        bsb2.node_binding.transmission_power = 1000;
        bsb2.node_binding.receiving_threshold = 1;

        bsb2.socket_binding.socket_active = true;
        bsb2.socket_binding.source_ip = SocketMap::getIP("192.168.0.7");
        bsb2.socket_binding.source_port = 5050;
        bsb2.socket_binding.dest_ip = SocketMap::getIP("255.255.255.0");
        bsb2.socket_binding.dest_port = 2020;

        scnsl->bind(n2, ch, bsb2);

        std::vector<Scnsl::Setup::multicast_wrapper_t> multicast_struct;

        multicast_wrapper_t sender_wrapper;
        sender_wrapper.task = &sender_t;
        sender_wrapper.comm = sender;
        sender_wrapper.ch = ch;
        sender_wrapper.bsb = &bsb0;

        multicast_wrapper_t recv_1_wrapper;
        recv_1_wrapper.task = &recv_1;
        recv_1_wrapper.comm = receiver_1;
        recv_1_wrapper.ch = ch;
        recv_1_wrapper.bsb = &bsb1;

        multicast_wrapper_t recv_2_wrapper;
        recv_2_wrapper.task = &recv_2;
        recv_2_wrapper.comm = receiver_2;
        recv_2_wrapper.ch = ch;
        recv_2_wrapper.bsb = &bsb2;

        multicast_struct.push_back(sender_wrapper);
        multicast_struct.push_back(recv_1_wrapper);
        multicast_struct.push_back(recv_2_wrapper);

        scnsl->bind(multicast_struct, SocketMap::getIP("255.255.255.0"));

        std::cerr << "Starting simulation" << std::endl;
        sc_core::sc_start(sc_core::sc_time(30, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
