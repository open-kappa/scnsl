// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include "MySensingRouter_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////



typedef struct P_t
{
   unsigned int src_id;			//4
   double sender_times;        //8
   int temperature;
} Payload_t;

static Payload_t arrivedPayload;

MySensingRouter_t::MySensingRouter_t( const sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies ):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    // Fields:
    _packetArrivedEvent(), taskid()
{
      taskid = id;
      SC_THREAD( _sensing );
      SC_THREAD( _routing );

}


MySensingRouter_t::~MySensingRouter_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void MySensingRouter_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t )
{
    Payload_t * temp;
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
    {
        temp = reinterpret_cast<Payload_t *>( p.get_data_ptr() );
        arrivedPayload.sender_times = temp->sender_times;
        arrivedPayload.src_id = temp->src_id;
        arrivedPayload.temperature = temp->temperature;
        std::cout << "Task name: "<<name()<<" RECEIVED data: " << temp->temperature << ", size: " << p.get_data_length()<<" delay "<< (sc_core::sc_time_stamp().to_double()-temp->sender_times)*1e-12<<std::endl;
        _packetArrivedEvent.notify();
     }
    else
        {
          // ERROR.
          SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );
          // Just to avoid compiler warnings:
          t = sc_core::sc_time_stamp();
        }
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////


void MySensingRouter_t::_sensing()
{
    const std::string tp = "0";
    Payload_t *p=static_cast<Payload_t *>(malloc(sizeof(Payload_t)*sizeof(p)));
    for ( ;; )
    {
        p->temperature= (rand()%25 + 25 );
        p->src_id=taskid;
        std::cout<<"Sender name "<<name()<< " ,Task id= "<<p->src_id<< " ,Temperature="<< p->temperature << std::endl;;
        p->sender_times= sc_core::sc_time_stamp().to_double();
        TlmTask_if_t::send( tp, reinterpret_cast<byte_t *>(p), sizeof(Payload_t));
        wait(10,sc_core::SC_MS);
    }

}

void MySensingRouter_t::_routing()
{
    const std::string tp = "0";
    for ( ;; )
    {
        wait(_packetArrivedEvent);
        std::cout<<"Task named "<<name()<< " ,Task id= "<<arrivedPayload.src_id<< " ,route Temperature="<< arrivedPayload.temperature<< std::endl;;
        TlmTask_if_t::send( tp, reinterpret_cast<byte_t *>(&arrivedPayload), sizeof(Payload_t));
    }

}
