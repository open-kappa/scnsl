#include "MyTask_Pit.hh"

#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

MyTask_Pit::MyTask_Pit(const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies,
                        std::string ip, int port):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies),
_ip(ip),
_port(port)
{
    
}

MyTask_Pit::~MyTask_Pit()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void MyTask_Pit::main()
{
    initTime();
    const size_t s = 10;
    int sockfd, newsockfd, portno, n;
    socklen_t clilen;
    sockaddr serv_addr, cli_addr;
    byte_t buffer[10];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
        throw std::runtime_error("ERROR opening socket");

    serv_addr.sin_port = _port;
    inet_pton(AF_INET, _ip.c_str(), &serv_addr.sin_addr);

    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw std::runtime_error("ERROR on binding");
    internal_listen(sockfd, 1);
    clilen = sizeof(cli_addr);
    newsockfd = internal_accept(sockfd, (struct sockaddr *)& cli_addr, &clilen);
    if (newsockfd < 0) 
        throw std::runtime_error("ERROR on internal_accept");
    for (;;)
    {
        n = internal_recv(newsockfd, buffer, s, 0);
        if (n < 0) 
            throw std::runtime_error("ERROR reading to socket");
        std::cout << "Time: " << sc_core::sc_time_stamp()
                  << ", Name: " << name() << ", data recvd: ";
        std::cout << std::string((char *)buffer, n) << std::endl;
    }
    internal_close(newsockfd);
    internal_close(sockfd);
}
