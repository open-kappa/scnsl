#ifndef MYTASK_PIT_HH
#define MYTASK_PIT_HH

#include <scnsl.hh>
#include <systemc>

class MyTask_Pit: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    MyTask_Pit(const sc_core::sc_module_name modulename, const task_id_t id, 
                Scnsl::Core::Node_t * n, const size_t proxies, std::string ip,
                int port);

    virtual ~MyTask_Pit();

private:

    void main() override;

    std::string _ip;
    int _port;
};

#endif
