
#include "MyTask_Pit.hh"
#include "MyTask_Send.hh"
#include "MyTask_Send_2.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * send_node_1 = scnsl->createNode();
        Scnsl::Core::Node_t * pit_node = scnsl->createNode();
        Scnsl::Core::Node_t * send_node_2 = scnsl->createNode();

        CoreChannelSetup_t csb;
        csb.extensionId = "core";
        csb.channel_type = CoreChannelSetup_t::SHARED;
        csb.name = "SharedChannel";
        csb.alpha = 0.1;
        csb.delay = sc_core::sc_time(sc_core::SC_ZERO_TIME);
        csb.nodes_number = 3;

        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(csb);
        Scnsl::Utils::DefaultEnvironment_t::createInstance(csb.alpha);

        //      Scnsl::Core::Channel_if_t * ch2 = scnsl->createChannel( csb );

        const bool IS_SENDER = true;
        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::task_id_t id2 = 2;
        const Scnsl::Core::task_id_t id3 = 3;

        MyTask_Sender send_1("Sender", id3, send_node_1, 1);
        MyTask_Sender_2 send_2("Sender_2", id0, send_node_2, 1);
        MyTask_Pit pit_1("Pit_1", id1, pit_node, 1, "192.168.0.2", 2020);
        MyTask_Pit pit_2("Pit_2", id2, pit_node, 1, "192.168.0.2", 2021);

        // Creating the protocol Tcp:

        auto tcp_sender_1 = new Lv4Communicator_t("SenderTcp");
        tcp_sender_1->setRcvwnd(500);  // max 500 bytes per send
        tcp_sender_1->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcp_sender_1->setSegmentSize(20);  // 25 bytes per segment

        auto tcp_pit = new Lv4Communicator_t("Pit");
        tcp_pit->setRcvwnd(500);  // max 500 bytes per send
        tcp_pit->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcp_pit->setSegmentSize(20);  // 25 bytes per segment

        auto tcp_sender_2 = new Lv4Communicator_t("sender2");
        tcp_sender_2->setRcvwnd(500);  // max 500 bytes per send
        tcp_sender_2->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcp_sender_2->setSegmentSize(20);  // 25 bytes per segment

        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        // First instance:

        ccoms.name = "Mac0";
        ccoms.node = send_node_1;
        Scnsl::Core::Communicator_if_t * mac_send_1 = scnsl->createCommunicator(ccoms);
        // Second instance:

        ccoms.name = "Mac1";
        ccoms.node = pit_node;
        Scnsl::Core::Communicator_if_t * mac_pit = scnsl->createCommunicator(ccoms);

        ccoms.name = "Mac2";
        ccoms.node = send_node_2;
        Scnsl::Core::Communicator_if_t * mac_send_2 = scnsl->createCommunicator(ccoms);

        tcp_sender_1->stackDown(mac_send_1);
        mac_send_1->stackUp(tcp_sender_1);
        Communicator_if_t *
            stack_send_1 = new Scnsl::Utils::CommunicatorStack_t(tcp_sender_1, mac_send_1);

        tcp_pit->stackDown(mac_pit);
        mac_pit->stackUp(tcp_pit);
        Communicator_if_t * stack_pit = new Scnsl::Utils::CommunicatorStack_t(tcp_pit, mac_pit);

        tcp_sender_2->stackDown(mac_send_2);
        mac_send_2->stackUp(tcp_sender_2);
        Communicator_if_t *
            stack_send_2 = new Scnsl::Utils::CommunicatorStack_t(tcp_sender_2, mac_send_2);

        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer1->addOutput(&std::cout);
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
       
        Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer2->addOutput(&std::cerr);
       
        // Binding:
        // send -> pit
        BindSetup_base_t sender_1_to_pit;
        sender_1_to_pit.extensionId = "core";
        sender_1_to_pit.destinationNode = pit_node;
        sender_1_to_pit.node_binding.x = 0;
        sender_1_to_pit.node_binding.y = 0.1;
        sender_1_to_pit.node_binding.z = 0;
        sender_1_to_pit.node_binding.bitrate = 10000000;  
        sender_1_to_pit.node_binding.transmission_power = 1000;
        sender_1_to_pit.node_binding.receiving_threshold = 1;

        sender_1_to_pit.socket_binding.socket_active = true;
        sender_1_to_pit.socket_binding.source_ip = SocketMap::getIP("192.168.0.1");
        sender_1_to_pit.socket_binding.source_port = 80;
        sender_1_to_pit.socket_binding.dest_ip = SocketMap::getIP("192.168.0.2");
        sender_1_to_pit.socket_binding.dest_port = 2020;

        scnsl->bind(send_node_1, ch, sender_1_to_pit);
        scnsl->bind(&send_1, &pit_1, ch, sender_1_to_pit, stack_send_1);

        // pit1 -> send1
        BindSetup_base_t pit_to_sender_1;
        pit_to_sender_1.extensionId = "core";
        pit_to_sender_1.destinationNode = send_node_1;
        pit_to_sender_1.node_binding.x = 0;
        pit_to_sender_1.node_binding.y = 0;
        pit_to_sender_1.node_binding.z = 0;
        pit_to_sender_1.node_binding.bitrate = 10000000;  
        pit_to_sender_1.node_binding.transmission_power = 1000;
        pit_to_sender_1.node_binding.receiving_threshold = 1;

        pit_to_sender_1.socket_binding.socket_active = true;
        pit_to_sender_1.socket_binding.source_ip = SocketMap::getIP( "192.168.0.2");
        pit_to_sender_1.socket_binding.source_port = 2020;
        pit_to_sender_1.socket_binding.dest_ip = SocketMap::getIP( "192.168.0.1");
        pit_to_sender_1.socket_binding.dest_port = 80;

        scnsl->bind(pit_node, ch, pit_to_sender_1);
        scnsl->bind(&pit_1, &send_1, ch, pit_to_sender_1, stack_pit);

        // send2 -> pit2
        BindSetup_base_t sender_2_to_pit;
        sender_2_to_pit.extensionId = "core";
        sender_2_to_pit.destinationNode = pit_node;
        sender_2_to_pit.node_binding.x = 0.1;
        sender_2_to_pit.node_binding.y = 0;
        sender_2_to_pit.node_binding.z = 0;
        sender_2_to_pit.node_binding.bitrate = 10000000;  // 100 megabit (to
                                                          // check ack timer)
        sender_2_to_pit.node_binding.transmission_power = 1000;
        sender_2_to_pit.node_binding.receiving_threshold = 1;

        sender_2_to_pit.socket_binding.socket_active = true;
        sender_2_to_pit.socket_binding.source_ip = SocketMap::getIP("192.168.0.4");
        sender_2_to_pit.socket_binding.source_port = 80;
        sender_2_to_pit.socket_binding.dest_ip = SocketMap::getIP("192.168.0.2");
        sender_2_to_pit.socket_binding.dest_port = 2021;

        scnsl->bind(send_node_2, ch, sender_2_to_pit); 
        scnsl->bind(&send_2, &pit_2, ch, sender_2_to_pit, stack_send_2);

        BindSetup_base_t pit_to_sender_2;
        pit_to_sender_2.extensionId = "core";
        pit_to_sender_2.destinationNode = send_node_2;
        pit_to_sender_2.node_binding.x = 0;
        pit_to_sender_2.node_binding.y = 0;
        pit_to_sender_2.node_binding.z = 0;
        pit_to_sender_2.node_binding.bitrate = 10000000;  
        pit_to_sender_2.node_binding.transmission_power = 1000;
        pit_to_sender_2.node_binding.receiving_threshold = 1;

        pit_to_sender_2.socket_binding.socket_active = true;
        pit_to_sender_2.socket_binding.source_ip = SocketMap::getIP(
            "192.168.0.2");
        pit_to_sender_2.socket_binding.source_port = 2021;
        pit_to_sender_2.socket_binding.dest_ip = SocketMap::getIP(
            "192.168.0.4");
        pit_to_sender_2.socket_binding.dest_port = 80;

        scnsl->bind(&pit_2, &send_2, ch, pit_to_sender_2, stack_pit); 

        sc_core::sc_start(sc_core::sc_time(60, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
