#ifndef MYTASK_SENDER_2_HH
#define MYTASK_SENDER_2_HH

#include <scnsl.hh>
#include <systemc>

class MyTask_Sender_2: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    
    MyTask_Sender_2(const sc_core::sc_module_name modulename, const task_id_t id,
                    Scnsl::Core::Node_t * n, const size_t proxies);

    virtual ~MyTask_Sender_2();

private:
    void main() override;
};

#endif
