#include "Tcp_Receiver_multi.hh"
#include "Tcp_Sender_multi.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Core;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();
        int nodes;
        std::string inputFile;
        if (argc < 3)
        {
            nodes = 6;
            inputFile = "NONE";
        }
        else
        {
            nodes = std::stoi(argv[2]);
            inputFile = argv[1];
        }

        Scnsl::Core::Channel_if_t * channel_array[nodes / 2];
        Scnsl::Core::Node_t * send_node_array[nodes / 2];
        Scnsl::Core::Node_t * recv_node_array[nodes / 2];
        Tcp_Sender * send_array[nodes / 2];
        Tcp_Receiver * recv_array[nodes / 2];
        Scnsl::Protocols::Network_Lv4::Lv4Communicator_t * tcp_senders[nodes / 2];
        Scnsl::Protocols::Network_Lv4::Lv4Communicator_t * tcp_receivers[nodes / 2];

        CoreChannelSetup_t csb;
        csb.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb.capacity = 100000000;
        csb.capacity2 = 100000000;
        csb.delay = sc_core::sc_time(390 / 2, sc_core::SC_US);
        csb.extensionId = "core";
        std::stringstream ss;
        const Scnsl::Core::size_t PROXIES = 1;

        for (int i = 0; i < nodes / 2; i++)
        {
            ss << "channel_fullduplex_" << i;
            csb.name = ss.str();
            channel_array[i] = scnsl->createChannel(csb);

            send_node_array[i] = scnsl->createNode();
            recv_node_array[i] = scnsl->createNode();

            const Scnsl::Core::task_id_t id0 = i * 2;
            const Scnsl::Core::task_id_t id1 = i * 2 + 1;

            ss.str("");
            ss << "TaskServer_" << i;
            send_array[i] = new Tcp_Sender(ss.str().c_str(), id0, 
                                            send_node_array[i], PROXIES,
                                            inputFile);
            ss.str("");
            ss << "test_tcp_multinode_" << i << "_byte_out.txt";

            std::stringstream ss2;
            ss2 << "TaskClient_" << i;
            recv_array[i] = new Tcp_Receiver(ss2.str().c_str(), id1,
                                                recv_node_array[i], PROXIES,
                                                ss.str().c_str());
            // Creating the protocol Tcp:

            tcp_senders[i] = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp0", true);
            tcp_senders[i]->setExtraHeaderSize(20, 14);
            tcp_senders[i]->setSegmentSize(
                MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);

            tcp_receivers[i] = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp1", true);
            tcp_receivers[i]->setExtraHeaderSize(20, 14);
            tcp_receivers[i]->setSegmentSize(
                MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);
            ss.str("");
        }
        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        cts.print_trace_timestamp = true;

        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer1->addOutput(&std::cout);
        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer2->addOutput(&std::cerr);

        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.node_binding.x = 0;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = 94200000;
        bsb0.node_binding.transmission_power = 1000;
        bsb0.node_binding.receiving_threshold = 1;
        bsb0.socket_binding.socket_active = true;

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 1;
        bsb1.node_binding.bitrate = 94200000;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        bsb1.socket_binding.socket_active = true;
        ss.str("");
        for (int i = 0; i < nodes / 2; i++)
        {
            bsb0.destinationNode = recv_node_array[i];
            bsb1.destinationNode = send_node_array[i];

            ss << "192.168.0." << i * 2;
            bsb0.socket_binding.source_ip = SocketMap::getIP(ss.str());
            bsb1.socket_binding.dest_ip = SocketMap::getIP(ss.str());

            ss.str("");
            ss << "192.168.0." << 2 * i + 1;
            bsb0.socket_binding.dest_ip = SocketMap::getIP(ss.str());
            bsb1.socket_binding.source_ip = SocketMap::getIP(ss.str());

            bsb0.socket_binding.source_port = 2020;
            bsb0.socket_binding.dest_port = 5050;

            bsb1.socket_binding.source_port = 5050;
            bsb1.socket_binding.dest_port = 2020;
            ss.str("");

            scnsl->bind(send_node_array[i], channel_array[i], bsb0);
            scnsl->bind(send_array[i], recv_array[i], channel_array[i], bsb0, tcp_senders[i]);
            scnsl->bind(recv_node_array[i], channel_array[i], bsb1);
            scnsl->bind(recv_array[i], send_array[i], channel_array[i], bsb1, tcp_receivers[i]);
        }  // Binding:

        sc_core::sc_start(sc_core::sc_time(100, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
