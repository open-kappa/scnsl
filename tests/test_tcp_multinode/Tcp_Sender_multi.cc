#include "Tcp_Sender_multi.hh"

#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>


// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Tcp_Sender::Tcp_Sender(const sc_core::sc_module_name modulename,
                        const task_id_t id, Scnsl::Core::Node_t * n,
                        const size_t proxies, std::string input_file):
    // Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
_input_file(input_file)
{
    if (input_file == "NONE")
        _file_valid = false;
    else _file_valid = true;
}

Tcp_Sender::~Tcp_Sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_Sender::main()
{
    initTime();
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[1024];
    int length;
    int total_size = 0;
    std::ifstream in; //declared but used only if a file has been specified
    if (_file_valid)
    {
        in.open(_input_file, std::ifstream::binary);
        in.seekg(0, in.end);
        length= in.tellg();
        in.seekg(0, in.beg);
    }
    else length = 231605634; //200 Mb file
    
    sockaddr serv_addr, cli_addr;

    int n;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw std::runtime_error("ERROR opening socket");
    portno = 2020;

    struct addrinfo hints;
    memset(&hints, 0, sizeof(addrinfo));

    struct addrinfo * result;
    std::stringstream s;
    s << portno;

    getaddrinfo(NULL, s.str().c_str(), &hints, &result);

    if (bind(sockfd, (struct sockaddr *)result->ai_addr, 
                sizeof(result->ai_addr)) < 0)
        throw std::runtime_error("ERROR on binding");

    listen(sockfd, 1);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
   
    if (newsockfd < 0) throw std::runtime_error("ERROR on accept");
   
    int byte_read;

    std::cerr << "SENDER: File lenght: " << length << std::endl;

    byte_t l[sizeof(int)];
    memcpy(l, &length, sizeof(length));

    n = Scnsl::Syscalls::send(newsockfd, l, sizeof(int), 0);
    if (n < 0) throw std::runtime_error("socket error");
    do
    {
        if (_file_valid)
        {
            in.read(buffer, 1024);
            byte_read = in.gcount();
        }
        else{
            int read_val = std::min(1024, length - total_size);
            for (int i = 0; i< read_val; i++)
            {
                buffer[i] = static_cast< char >( rand()%25 + 65 );
                byte_read = read_val;
                total_size += read_val;
            }
        }
        if (byte_read)
        {
            n = Scnsl::Syscalls::send(newsockfd, (byte_t *)(buffer), byte_read, 0);
            if (n < 0) 
            throw std::runtime_error("ERROR writing to socket");
        }
    }
    while (byte_read);

    if (n < 0) throw std::runtime_error("ERROR writing to socket");

    in.close();
    close(newsockfd);
    close(sockfd);
}
