// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include "MyTask_t.hh"

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

namespace /* anon */ {

typedef struct P_t
{
    unsigned int src_id;			//4
    double sender_times;        //8
    int temperature;
    const char * sender_name;

} Payload_t;

Payload_t arrivedPacket;

} // anon

MyTask_t:: MyTask_t( const sc_core::sc_module_name modulename,
                     const task_id_t id,
                     Scnsl::Core::Node_t * n,
                     const size_t proxies ):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    // Fields:
    _packetArrivedEvent(), taskid()
{
    taskid = id;
    SC_THREAD( _sensing );
    //   SC_THREAD( _routing );

}


MyTask_t::~ MyTask_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////
static double totaldelay=0, vv=1;

void MyTask_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time &  )
{

    Payload_t * temp;
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
    {
        temp = reinterpret_cast<Payload_t *>( p.get_data_ptr() );
        arrivedPacket.sender_times = temp->sender_times;
        arrivedPacket.src_id = temp->src_id;
        arrivedPacket.sender_name = temp->sender_name;
        arrivedPacket.temperature = temp->temperature;wait(rand()%5,sc_core::SC_MS);
        totaldelay=  totaldelay + (sc_core::sc_time_stamp().to_double()-temp->sender_times)*1e-12;
        if(name() !=temp->sender_name)
        {
            std::cout<< "Receiver name : "<<name()<< "  received "<<arrivedPacket.temperature<<std::endl;
            std::cout << "Delay between "<<name() <<"-&-"<<temp->sender_name<< " is "<< (sc_core::sc_time_stamp().to_double()-temp->sender_times)*1e-12<<std::endl;
        }
        //std::cout<<"Transmission POWER: S=11.8, R=11.8.       Average delay (end-to-end) = "<<totaldelay/vv <<std::endl;
        vv++;
        _packetArrivedEvent.notify();
    }

}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////


void  MyTask_t::_sensing()
{
    std::cout<<taskid<<std::endl;
    const std::string tp = "0";
    Payload_t *p=static_cast<Payload_t *>(malloc(sizeof(Payload_t)*sizeof(p)));
    //  if(taskid==0 || taskid==2)
    {
        for ( ;; )
        {

            //   std::cout<< "Task ID : "<<taskid<<" is sending"<<std::endl;
            p->temperature= (rand()%25 + 25 );
            p->src_id=taskid;
            p->sender_name= name();
            std::cout<<"Sender name "<<name()<< " ,Task id= "<<p->src_id<< " ,Temperature="<< p->temperature<< "    \t"<< std::endl;;
            p->sender_times= sc_core::sc_time_stamp().to_double();
            TlmTask_if_t::send( tp, reinterpret_cast<byte_t *>(p), sizeof(Payload_t));

            wait(rand()%1000,sc_core::SC_MS);

        }

    }

}


void MyTask_t::_routing()
{
    const std::string tp1 = "0";
    for ( ;; )
    {
        wait(_packetArrivedEvent);

        std::cout<<"Task named "<<name()<< " ,Task id= "<<arrivedPacket.src_id<< " ,route Temperature="<< arrivedPacket.temperature<< "    \t"<< "Task proxy " <<tp1<< std::endl;
        TlmTask_if_t::send( tp1, reinterpret_cast<byte_t *>(&arrivedPacket), sizeof(Payload_t));
    }
}
