// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>
#include <scnsl.hh>
#include "MyTask_t.hh"
#include <fstream>
#include <iostream>
using namespace Scnsl::BuiltinPlugin;
using Scnsl::Tracing::Traceable_base_t;

//
// C++ code generated by hif2sc
//

int sc_main( int /*argc*/, char * /*argv*/[] )
{
    const int Proxy_sensor( 1 );

    const int Proxy_router( 2 );

    const int Proxy_collector( 1 );

    Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();
    Scnsl::Core::Node_t * Node_0 = scnsl->createNode();
    Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t ccoms0;
    Scnsl::Core::Communicator_if_t * MAC_Node_0 = nullptr;
    Scnsl::Core::Node_t * Node_1 = scnsl->createNode();
    Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t ccoms1;
    Scnsl::Core::Communicator_if_t * MAC_Node_1 = nullptr;
    Scnsl::BuiltinPlugin::CoreTracingSetup_t cts;
    Scnsl::Setup::Scnsl_t::Tracer_t * tracer = nullptr;
    const int IDsensor( 0 );

    MyTask_t sensor( "sensor", IDsensor, Node_0, Proxy_sensor );
    const int IDrouter( 1 );

    MyTask_t router( "router", IDrouter, Node_1, Proxy_router );
    Scnsl::BuiltinPlugin::CoreChannelSetup_t ccs_ch;
    Scnsl::Core::Channel_if_t * ch = nullptr;
    Scnsl::Setup::BindSetup_base_t bsb_0;
    Scnsl::Setup::BindSetup_base_t bsb_1;
    Scnsl::Core::Node_t * Node_2 = scnsl->createNode();
    Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t ccoms2;
    Scnsl::Core::Communicator_if_t * MAC_Node_2 = nullptr;
    Scnsl::BuiltinPlugin::CoreChannelSetup_t ccs_CommunicationPath_Node_1_Node_2;
    Scnsl::Core::Channel_if_t * CommunicationPath_Node_1_Node_2 = nullptr;
    Scnsl::Setup::BindSetup_base_t bsb_2;
    Scnsl::Setup::BindSetup_base_t bsb_3;
    const int IDcollector( 2 );

    MyTask_t collector( "collector", IDcollector, Node_2, Proxy_collector );
    ccs_ch.nodes_number = 2;
    ccs_CommunicationPath_Node_1_Node_2.nodes_number = 2;
    ccoms0.extensionId = "core";
    ccoms0.ack_required = true;
    ccoms0.short_addresses = true;
    ccoms0.type = CoreCommunicatorSetup_t::MAC_802_15_4;
    ccoms0.name = "MAC_Node_0";
    ccoms0.node = Node_0;
    MAC_Node_0 = scnsl->createCommunicator( ccoms0 );
    ccoms1.extensionId = "core";
    ccoms1.ack_required = true;
    ccoms1.short_addresses = true;
    ccoms1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
    ccoms1.name = "MAC_Node_1";
    ccoms1.node = Node_1;
    MAC_Node_1 = scnsl->createCommunicator( ccoms1 );
    cts.extensionId = "core";
    cts.formatterExtensionId = "core";
    cts.formatterName = "basic";
    cts.print_trace_type = true;
    cts.info = 5;
    cts.debug = 0;
    cts.log = 0;
    cts.error = 5;
    cts.warning = 5;
    cts.fatal = 5;
    cts.filterExtensionId = "core";
    cts.filterName = "basic";
    tracer = scnsl->createTracer( cts );
    tracer->trace( (&sensor) );
    tracer->trace( (&router) );
    ccs_ch.extensionId = "core";
    ccs_ch.channel_type = CoreChannelSetup_t::SHARED;
    ccs_ch.name = "SharedChannel";
    ccs_ch.delay = sc_core::sc_time( 87654, sc_core::SC_US );
    ccs_ch.capacity = 25000;
    ch = scnsl->createChannel( ccs_ch );
    bsb_0.extensionId = "core";
    bsb_0.bindIdentifier = "1";
    bsb_0.destinationNode = Node_0;
    bsb_0.node_binding.x = 0;
    bsb_0.node_binding.y = 0;
    bsb_0.node_binding.z = 0;
    bsb_0.node_binding.bitrate = 250000;
    bsb_0.node_binding.transmission_power = 10;
    bsb_0.node_binding.receiving_threshold = 1;
    scnsl->bind(Node_1, ch, bsb_0);
    bsb_1.extensionId = "core";
    bsb_1.bindIdentifier = "0";
    bsb_1.destinationNode = Node_1;
    bsb_1.node_binding.x = 1;
    bsb_1.node_binding.y = 1;
    bsb_1.node_binding.z = 0;
    bsb_1.node_binding.bitrate = 250000;
    bsb_1.node_binding.transmission_power = 10;
    bsb_1.node_binding.receiving_threshold = 1;
    scnsl->bind(Node_0, ch, bsb_1);
    ccoms2.extensionId = "core";
    ccoms2.ack_required = true;
    ccoms2.short_addresses = true;
    ccoms2.type = CoreCommunicatorSetup_t::MAC_802_15_4;
    ccoms2.name = "MAC_Node_2";
    ccoms2.node = Node_2;
    MAC_Node_2 = scnsl->createCommunicator( ccoms2 );
    ccs_CommunicationPath_Node_1_Node_2.extensionId = "core";
    ccs_CommunicationPath_Node_1_Node_2.channel_type = CoreChannelSetup_t::HALF_DUPLEX;
    ccs_CommunicationPath_Node_1_Node_2.name = "HalfDuplex";
    ccs_CommunicationPath_Node_1_Node_2.delay = sc_core::sc_time( 2, sc_core::SC_US
     );
    ccs_CommunicationPath_Node_1_Node_2.capacity = 25000;
    CommunicationPath_Node_1_Node_2 = scnsl->createChannel( ccs_CommunicationPath_Node_1_Node_2
     );
    bsb_2.extensionId = "core";
    bsb_2.bindIdentifier = "0";
    bsb_2.destinationNode = Node_1;
    bsb_2.node_binding.x = 2;
    bsb_2.node_binding.y = 2;
    bsb_2.node_binding.z = 0;
    bsb_2.node_binding.bitrate = 250000;
    bsb_2.node_binding.transmission_power = 10;
    bsb_2.node_binding.receiving_threshold = 1;
    scnsl->bind(Node_2, CommunicationPath_Node_1_Node_2, bsb_2);
    bsb_3.extensionId = "core";
    bsb_3.bindIdentifier = "0";
    bsb_3.destinationNode = Node_2;
    bsb_3.node_binding.x = 3;
    bsb_3.node_binding.y = 3;
    bsb_3.node_binding.z = 0;
    bsb_3.node_binding.bitrate = 250000;
    bsb_3.node_binding.transmission_power = 10;
    bsb_3.node_binding.receiving_threshold = 1;
    scnsl->bind(Node_1, CommunicationPath_Node_1_Node_2, bsb_3);
    tracer->trace( (&collector) );
    scnsl->bind((&router), (&collector), CommunicationPath_Node_1_Node_2, bsb_3,
                         MAC_Node_1);
    scnsl->bind((&collector), (&router), CommunicationPath_Node_1_Node_2, bsb_2,
                         MAC_Node_2);
    scnsl->bind((&sensor), (&router), ch, bsb_1, MAC_Node_0);
    scnsl->bind((&router), (&sensor), ch, bsb_0, MAC_Node_1);
    sc_core::sc_start( sc_core::sc_time( 700000, sc_core::SC_MS ) );
    sc_core::sc_stop();

    return 0;
}
