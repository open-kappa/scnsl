// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef  MyTask_t_HH
#define  MyTask_t_HH

#include <systemc>
#include <scnsl.hh>

class  MyTask_t:
    public Scnsl::Tlm::TlmTask_if_t
{
public:

    SC_HAS_PROCESS(  MyTask_t );
     MyTask_t( const sc_core::sc_module_name modulename,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies );

    /// @brief Virtual destructor.
    virtual
    ~ MyTask_t();

    /// @name Inherited interface methods.
    //@{


    virtual
    void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

    //@}

    /// @brief Signals when a packet is arrived.
    sc_core::sc_event _packetArrivedEvent;
    unsigned int taskid;
private:

    /// @name Processes.
    //@{

    /// @brief Sender process.
    void _sensing();
      void _routing();


    /// @brief Disabled copy constructor.
     MyTask_t( const  MyTask_t & );

    /// @brief Disabled assignment operator.
     MyTask_t & operator = (  MyTask_t & );
};



#endif
