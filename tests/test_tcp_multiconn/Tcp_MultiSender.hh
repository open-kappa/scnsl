
#ifndef TCP_MULTISENDER_HH
#define TCP_MULTISENDER_HH

#include <scnsl.hh>
#include <systemc>

class Tcp_Sender: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    Tcp_Sender(const sc_core::sc_module_name modulename, const task_id_t id,
                Scnsl::Core::Node_t * n, const size_t proxies, int n_conn);

    virtual ~Tcp_Sender();

private:
    int _n_conn;
    void main() override;
};

#endif
