#include "Tcp_MultiSender.hh"

#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Syscalls;
using Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t;
using Scnsl::Syscalls::send;

Tcp_Sender::Tcp_Sender( const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies, int n_conn):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM),
_n_conn(n_conn)
{

}

Tcp_Sender::~Tcp_Sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_Sender::main()
{
    initTime();
    int sockfd, portno, n;
    sockaddr_in serv_addr, my_addr;

    char buffer[10] = "PINGPONG!";
    int bufsize = 10;

    for (int i = 0; i < _n_conn; i++)
    {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            std::cout << "NO socket:" << errno << std::endl;

        portno = 5050;
        serv_addr.sin_port = portno;
        inet_pton(AF_INET, "192.168.0.6", &serv_addr.sin_addr);

        if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
            throw std::runtime_error("NO connect");
        std::cerr << "Send " << i << "/10" << std::endl;
        n = Scnsl::Syscalls::send(sockfd, buffer, bufsize, 0);
        if (n < 0)
            std::cerr << "\n ERROR in send: " << errno << std::endl;
        close(sockfd);
    }
    std::cout << "Ending" << std::endl;
}
