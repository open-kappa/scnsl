#ifndef SCNSL_TCP_ADDON_TCP_MINI_RECEIVER_HH
#define SCNSL_TCP_ADDON_TCP_MINI_RECEIVER_HH

#include <scnsl.hh>
#include <systemc>

/// @file
/// A simple TLM task.

/// @brief A simple TLM task.
///
/// Design patterns:
///
/// - Non copyable.
/// - Non assignable.
///

class Tcp_Receiver: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param is_sender True if is sender.
    /// @param test The kind of test.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    Tcp_Receiver(const sc_core::sc_module_name modulename, const task_id_t id,
                    Scnsl::Core::Node_t * n, const size_t proxies, int n_conn);

    /// @brief Virtual destructor.
    virtual ~Tcp_Receiver();

private:
    int _n_conn;  // # of connections
    void main() override;
};
#endif  // SCNSL_TCP_ADDON_TCP_MINI_RECEIVER_HH
