// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <exception>
#include <iostream>
#include <fstream>
#include <map>

#include <systemc>
#include <tlm.h>
#include <scnsl.hh>

#include "MyTask_t.hh"

using namespace std;
using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace Scnsl::Tlm;

using Scnsl::Tracing::Traceable_base_t;

// Attention: use double type indeces in C++ containers only if
// the precision of the indices is fixed!
typedef std::map<double, double> inner_map;
typedef std::map<double, inner_map> outer_map;
static outer_map table;

// In this case we use the center frequency and the receiver power to
// calculate the bit error rate but you can use other receiver parameters.
static double getErrorBitRateSaboteur1(const node_properties_t & sp,
  const node_properties_t & rp)
{
    double center_frequency = 350.0; // MHz
    double power = Environment_if_t::getInstance()
        ->getReceiverPower(sp, rp); // W
    return table[center_frequency][static_cast<int>(power)];
}

static double getErrorBitRateSaboteur2(const node_properties_t & sp,
  const node_properties_t & rp)
{
    double center_frequency = 400.0; // MHz
    double power = Environment_if_t::getInstance()
        ->getReceiverPower(sp, rp); // W
    return table[center_frequency][static_cast<int>(power)];
}

static double getErrorBitRateSaboteur3(const node_properties_t & sp,
  const node_properties_t & rp)
{
    double center_frequency = 450.0; // MHz
    double power = Environment_if_t::getInstance()
        ->getReceiverPower(sp, rp); // W
    return table[center_frequency][static_cast<int>(power)];
}

int sc_main( int argc, char* argv[])
{
    double temp_a, temp_b, temp_c;

    if (argc != 3)
    {
        cout << "Use: ./test_bit_error_rate_function [TABLE_FILE] "
          << "[SIM_LENGTH(ns)]" << endl;
        return 1;
    }

    // Reading tables
    ifstream ifstr(argv[1]);
    while (ifstr >> temp_a >> temp_b >> temp_c)
      table[temp_a][temp_b] = temp_c;
    ifstr.close();

    try
    {
        // Singleton
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation
        Scnsl::Core::Node_t * n0 = scnsl->createNode();
        Scnsl::Core::Node_t * n1 = scnsl->createNode();
        Scnsl::Core::Node_t * n2 = scnsl->createNode();
        Scnsl::Core::Node_t * n3 = scnsl->createNode();
        Scnsl::Core::Node_t * n4 = scnsl->createNode();

        // Channel creation
        CoreChannelSetup_t ccs0;
        ccs0.extensionId = "core";
        ccs0.channel_type = CoreChannelSetup_t::SHARED;
        ccs0.name = "SharedChannel01";
        ccs0.alpha = 0.1;
        ccs0.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs0.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch01 = scnsl->createChannel( ccs0 );

        CoreChannelSetup_t ccs1;
        ccs1.extensionId = "core";
        ccs1.channel_type = CoreChannelSetup_t::SHARED;
        ccs1.name = "SharedChannel02";
        ccs1.alpha = 0.1;
        ccs1.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs1.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch02 = scnsl->createChannel( ccs1 );

        CoreChannelSetup_t ccs2;
        ccs2.extensionId = "core";
        ccs2.channel_type = CoreChannelSetup_t::SHARED;
        ccs2.name = "SharedChannel03";
        ccs2.alpha = 0.1;
        ccs2.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs2.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch03 = scnsl->createChannel( ccs2 );

        CoreChannelSetup_t ccs3;
        ccs3.extensionId = "core";
        ccs3.channel_type = CoreChannelSetup_t::SHARED;
        ccs3.name = "SharedChannel04";
        ccs3.alpha = 0.1;
        ccs3.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs3.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch04 = scnsl->createChannel( ccs3 );

        Scnsl::Utils::DefaultEnvironment_t::createInstance( ccs0.alpha );

        // Task creation
        const bool IS_SENDER = true;
        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::task_id_t id2 = 2;
        const Scnsl::Core::task_id_t id3 = 3;
        const Scnsl::Core::task_id_t id4 = 4;
        const Scnsl::Core::size_t PROXIES = 1;

        MyTask_t t0( "Task0", IS_SENDER, id0, n0, 4 );
        MyTask_t t1( "Task1", ! IS_SENDER, id1, n1, PROXIES );
        MyTask_t t2( "Task2", ! IS_SENDER, id2, n2, PROXIES );
        MyTask_t t3( "Task3", ! IS_SENDER, id3, n3, PROXIES );
        MyTask_t t4( "Task4", ! IS_SENDER, id4, n4, PROXIES );

        // Creating the protocol 802.15.4
        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        // First instance for n0
        ccoms.name = "Mac01";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac01 = scnsl->createCommunicator( ccoms );
        // Second instance for n0
        ccoms.name = "Mac02";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac02 = scnsl->createCommunicator( ccoms );
        // Third instance for n0
        ccoms.name = "Mac03";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac03 = scnsl->createCommunicator( ccoms );
        // Fourth instance for n0
        ccoms.name = "Mac04";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac04 = scnsl->createCommunicator( ccoms );
        // Instance for n1
        ccoms.name = "Mac1";
        ccoms.node = n1;
        Scnsl::Core::Communicator_if_t * mac1 = scnsl->createCommunicator( ccoms );
        // Instance for n2
        ccoms.name = "Mac2";
        ccoms.node = n2;
        Scnsl::Core::Communicator_if_t * mac2 = scnsl->createCommunicator( ccoms );
        // Instance for n3
        ccoms.name = "Mac3";
        ccoms.node = n3;
        Scnsl::Core::Communicator_if_t * mac3 = scnsl->createCommunicator( ccoms );
        // Instance for n4
        ccoms.name = "Mac4";
        ccoms.node = n4;
        Scnsl::Core::Communicator_if_t * mac4 = scnsl->createCommunicator( ccoms );

        // Creating saboteurs
        // Saboteur for n1
        CoreCommunicatorSetup_t ccoms1;
        ccoms1.extensionId = "core";
        ccoms1.bitErrorRateFunction = getErrorBitRateSaboteur1;
        ccoms1.type = CoreCommunicatorSetup_t::SABOTEUR;
        ccoms1.name = "Sab1";
        ccoms1.node = n1;
        Scnsl::Core::Communicator_if_t * sab1 = scnsl->createCommunicator( ccoms1 );

        // Saboteur for n2
        CoreCommunicatorSetup_t ccoms2;
        ccoms2.extensionId = "core";
        ccoms2.bitErrorRateFunction = getErrorBitRateSaboteur2;
        ccoms2.type = CoreCommunicatorSetup_t::SABOTEUR;
        ccoms2.name = "Sab2";
        ccoms2.node = n2;
        Scnsl::Core::Communicator_if_t * sab2 = scnsl->createCommunicator( ccoms2 );

        // Saboteur for n3
        CoreCommunicatorSetup_t ccoms3;
        ccoms3.extensionId = "core";
        ccoms3.bitErrorRateFunction = getErrorBitRateSaboteur3;
        ccoms3.type = CoreCommunicatorSetup_t::SABOTEUR;
        ccoms3.name = "Sab3";
        ccoms3.node = n3;
        Scnsl::Core::Communicator_if_t * sab3 = scnsl->createCommunicator( ccoms3 );

        // Saboteur for n4 (default bit error rate function)
        CoreCommunicatorSetup_t ccoms4;
        ccoms4.extensionId = "core";
        ccoms4.type = CoreCommunicatorSetup_t::SABOTEUR;
        ccoms4.name = "Sab4";
        ccoms4.node = n4;
        Scnsl::Core::Communicator_if_t * sab4 = scnsl->createCommunicator( ccoms4 );

        mac1->stackDown(sab1);
        sab1->stackUp(mac1);
        Communicator_if_t * macStack1 =
            new Scnsl::Utils::CommunicatorStack_t(mac1, sab1);

        mac2->stackDown(sab2);
        sab2->stackUp(mac2);
        Communicator_if_t * macStack2 =
            new Scnsl::Utils::CommunicatorStack_t(mac2, sab2);

        mac3->stackDown(sab3);
        sab3->stackUp(mac3);
        Communicator_if_t * macStack3 =
            new Scnsl::Utils::CommunicatorStack_t(mac3, sab3);

        mac4->stackDown(sab4);
        sab4->stackUp(mac4);
        Communicator_if_t * macStack4 =
            new Scnsl::Utils::CommunicatorStack_t(mac4, sab4);

        // Adding tracing features
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        cts.print_trace_type = true;
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // Creating
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer( cts );
        tracer1->addOutput( & std::cout );
        // tracer1->trace( & t0 );
        // tracer1->trace( & t1 );
        // tracer1->trace( & t2 );
        // tracer1->trace( & t3 );
        // tracer1->trace( & t4 );
        // tracer1->trace( dynamic_cast<Traceable_base_t*>(sab1) );
        // tracer1->trace( dynamic_cast<Traceable_base_t*>(sab2) );
        // tracer1->trace( dynamic_cast<Traceable_base_t*>(sab3) );
        // tracer1->trace( dynamic_cast<Traceable_base_t*>(sab4) );

        // Binding
        BindSetup_base_t bsb01;
        bsb01.extensionId = "core";
        bsb01.destinationNode = n1;
        bsb01.node_binding.x = 0;
        bsb01.node_binding.y = 0;
        bsb01.node_binding.z = 0;
        bsb01.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb01.node_binding.transmission_power = 78;
        bsb01.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb02;
        bsb02.extensionId = "core";
        bsb02.destinationNode = n2;
        bsb02.node_binding.x = 0;
        bsb02.node_binding.y = 0;
        bsb02.node_binding.z = 0;
        bsb02.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb02.node_binding.transmission_power = 68;
        bsb02.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb03;
        bsb03.extensionId = "core";
        bsb03.destinationNode = n3;
        bsb03.node_binding.x = 0;
        bsb03.node_binding.y = 0;
        bsb03.node_binding.z = 0;
        bsb03.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb03.node_binding.transmission_power = 62;
        bsb03.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb04;
        bsb04.extensionId = "core";
        bsb04.destinationNode = n4;
        bsb04.node_binding.x = 0;
        bsb04.node_binding.y = 0;
        bsb04.node_binding.z = 0;
        bsb04.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb04.node_binding.transmission_power = 55;
        bsb04.node_binding.receiving_threshold = 10;

        scnsl->bind(n0, ch01, bsb01);
        scnsl->bind(&t0, &t1, ch01, bsb01, mac01);

        scnsl->bind(n0, ch02, bsb02);
        scnsl->bind(&t0, &t2, ch02, bsb02, mac02);

        scnsl->bind(n0, ch03, bsb03);
        scnsl->bind(&t0, &t3, ch03, bsb03, mac03);

        scnsl->bind(n0, ch04, bsb04);
        scnsl->bind(&t0, &t4, ch04, bsb04, mac04);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = nullptr; // don't care: it receives only...
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 1;
        bsb1.node_binding.z = 0;
        bsb1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 1;

        BindSetup_base_t bsb2;
        bsb2.extensionId = "core";
        bsb2.destinationNode = nullptr; // don't care: it receives only...
        bsb2.node_binding.x = 1;
        bsb2.node_binding.y = 0;
        bsb2.node_binding.z = 1;
        bsb2.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb2.node_binding.transmission_power = 1000;
        bsb2.node_binding.receiving_threshold = 1;

        BindSetup_base_t bsb3;
        bsb3.extensionId = "core";
        bsb3.destinationNode = nullptr; // don't care: it receives only...
        bsb3.node_binding.x = 0;
        bsb3.node_binding.y = 1;
        bsb3.node_binding.z = 1;
        bsb3.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb3.node_binding.transmission_power = 1000;
        bsb3.node_binding.receiving_threshold = 1;

        BindSetup_base_t bsb4;
        bsb4.extensionId = "core";
        bsb4.destinationNode = nullptr; // don't care: it receives only...
        bsb4.node_binding.x = 0;
        bsb4.node_binding.y = 0;
        bsb4.node_binding.z = 1;
        bsb4.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb4.node_binding.transmission_power = 1000;
        bsb4.node_binding.receiving_threshold = 1;

        scnsl->bind(n1, ch01, bsb1);
        scnsl->bind(&t1, &t0, ch01, bsb1, macStack1);

        scnsl->bind(n2, ch02, bsb2);
        scnsl->bind(&t2, &t0, ch02, bsb2, macStack2);

        scnsl->bind(n3, ch03, bsb3);
        scnsl->bind(&t3, &t0, ch03, bsb3, macStack3);

        scnsl->bind(n4, ch04, bsb4);
        scnsl->bind(&t4, &t0, ch04, bsb4, macStack4);

        sc_core::sc_start( sc_core::sc_time( atoi(argv[2]), sc_core::SC_MS ) );
        sc_core::sc_stop();

    } catch ( std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
