// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef NODETASK_T_HH
#define NODETASK_T_HH

#include <systemc>
#include <scnsl.hh>
#include <fstream>

/// @file
/// A simple TLM task.


/// @brief A simple TLM task.
///
/// Design patterns:
///
/// - Non copyable.
/// - Non assignable.
///
class SendTask_t:
        public Scnsl::Tlm::TlmTask_if_t
{
public:

    SC_HAS_PROCESS( SendTask_t );

    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    SendTask_t(const sc_core::sc_module_name modulename,
               const task_id_t id,
               Scnsl::Core::Node_t * n,
               const size_t proxies,
               const double angle_granularity,
               const double distance,
               const double height,
               const std::string & file);

    /// @brief Virtual destructor.
    virtual
    ~SendTask_t();

    /// @name Inherited interface methods.
    //@{


    virtual
    void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

    //@}

private:

    const double _angle_granularity;
    const double _radius;
    const double _height;
    std::ofstream _file;

    void _fixZero(double & value) const;

    /// @brief Sender process.
    void _sender();

    /// @brief Disabled copy constructor.
    SendTask_t( const SendTask_t & );

    /// @brief Disabled assignment operator.
    SendTask_t & operator = ( SendTask_t & );
};



#endif
