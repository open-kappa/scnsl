// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <vector>
#include <map>

struct Data
{
    std::string senderTime;
    int data;
    double x;
    double y;
    double z;
    double angle;
    double Power;
    double dBPower;

    Data();
};

Data::Data():
    senderTime(),
    data(0),
    x(0),
    y(0),
    z(0),
    angle(0),
    Power(0),
    dBPower(0)
{
    // ntd
}


typedef std::map<int, Data> DataMap;
typedef std::vector<std::string> StringVector;

namespace /*anon*/ {


StringVector tokenize(const std::string & line)
{
    char * tok = strtok(const_cast<char *>(line.c_str()), ";");
    StringVector tokens;
    while(tok != nullptr)
    {
        tokens.push_back(tok);
        tok = strtok(nullptr, ";");
    }

    return tokens;
}

void parseSenderFile(const char * file, DataMap & map)
{
    std::ifstream in(file);
    while(in.good())
    {
        std::string line;
        std::getline(in, line);
        if (!in.good()) break;
        StringVector tokens = tokenize(line);
        assert(tokens.size() == 6);
        Data data;
        data.senderTime = tokens[0];
        data.data = atoi(tokens[1].c_str());
        data.x = atof(tokens[2].c_str());
        data.y = atof(tokens[3].c_str());
        data.z = atof(tokens[4].c_str());
        data.angle = atof(tokens[5].c_str());
        map[data.data] = data;
    }
}

void parseReceiverFile(const char * file, DataMap & map)
{
    std::ifstream in(file);
    while(in.good())
    {
        std::string line;
        std::getline(in, line);
        if (!in.good()) break;
        StringVector tokens = tokenize(line);
        assert(tokens.size() == 3);
        int data = atoi(tokens[1].c_str());
        double power = atof(tokens[2].c_str());
        double dbpower = atof(tokens[3].c_str());
        DataMap::iterator it = map.find(data);
        assert(it != map.end());
        Data & d = it->second;
        d.Power = power;
        d.dBPower = dbpower;
    }
}

void printFile(const char * file, DataMap map)
{
    std::ofstream out(file);
    out << "Data;Time;X;Y;Z;Angle;Power;Power(dBm)\n";
    for (DataMap::iterator it = map.begin(); it != map.end(); ++it)
    {
        Data & data = it->second;
        out << data.data << ";"
            << data.senderTime << ";"
            << data.x << ";"
            << data.y << ";"
            << data.z << ";"
            << data.angle << ";"
            << data.Power << ";"
            << data.dBPower << std::endl;
    }
}

} //anon

int main(int argc, char * argv[])
{
    if (argc != 4)
    {
        std::cerr << "Wrong number of parameters. Usage: <sender file> <receiver file> <out file>" << std::endl;
        return 1;
    }

    DataMap map;
    parseSenderFile(argv[1], map);
    parseReceiverFile(argv[2], map);
    printFile(argv[3], map);

    return 0;
}
