// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
#include <math.h>

struct InputElement
{
    double Phi;
    double Theta;
    double Gh;
    double Gv;
    double Gt;

    InputElement();
};

InputElement::InputElement():
    Phi(),
    Theta(),
    Gh(),
    Gv(),
    Gt()

{
    // ntd
}



typedef std::vector<std::string> StringVector;
typedef std::vector<InputElement> RadiationPoint;

namespace /*anon*/ {


InputElement getElement(const std::string & line)
{
    char * tok = strtok(const_cast<char *>(line.c_str()), ";");
    StringVector tokens;
    while(tok != nullptr)
    {
        tokens.push_back(tok);
        tok = strtok(nullptr, ";");
    }
    InputElement el;
    double x;
    x = atof(tokens[0].c_str());
    if (x>=0) {
        el.Phi = x;
    } else {
        el.Phi = x+360;
    }
    x = atof(tokens[1].c_str());
    if (x>=0) {
        el.Theta = x;
    } else {
        el.Theta = x+360;
    }
    el.Gh = atof(tokens[2].c_str());
    el.Gv = atof(tokens[3].c_str());
    el.Gt = atof(tokens[4].c_str());

    return el;
}

}

static bool lowerPhi (InputElement i,InputElement j)
{
    return (i.Phi<j.Phi);
}

static bool lowerTheta (InputElement i,InputElement j)
{
    return (i.Theta<j.Theta);
}

int main(int /*argc*/, char * argv[])
{
    std::vector<InputElement> AllInputs;
    std::vector<double> ThetaValues;
    std::vector<double> PhiValues;
    size_t iter = 0;
    double angle;
    std::string instring = argv[1];
    std::string filename = instring.substr(0, instring.find(".", 0));
    std::string outfilename = filename + ".rad";

    std::ifstream in(argv[1]);
    while(in.good())
    {
        std::string line;
        std::getline(in, line);
        if (!in.good()) break;
        if (iter == 0)
        {
            std::getline(in, line);
            if (!in.good()) break;
            iter++;
        }
        InputElement el = getElement(line);
        AllInputs.push_back(el);
        std::cout << iter << " : " << el.Phi << " "  << el.Theta << " "  << el.Gh << " "  << el.Gv << " "  << el.Gt << " " << std::endl;
        iter++;

        angle = el.Phi;
        if ( std::find(PhiValues.begin(), PhiValues.end(), angle) == PhiValues.end() ) {
           PhiValues.push_back(angle);
        }
        angle = el.Theta;
        if (std::find(ThetaValues.begin(), ThetaValues.end(), angle) == ThetaValues.end())
        {
            ThetaValues.push_back(angle);
        }
    }

    iter = iter - 1;
    std::cout << "Total lines : " << iter << std::endl;
    size_t tot_input = iter;
    std::cout << "Phi values (" << PhiValues.size() << ") are : ";

    std::vector<double>::const_iterator it;

    std::sort (PhiValues.begin(), PhiValues.end());
    for( it=PhiValues.begin() ; it < PhiValues.end(); ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << "\n " << std::endl;

    std::sort (ThetaValues.begin(), ThetaValues.end());
    std::cout << "Theta values (" << ThetaValues.size() << ") are : ";
    for( it=ThetaValues.begin() ; it < ThetaValues.end(); ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << "\n " << std::endl;


    InputElement selected;
    bool is_per_planes = true;
    for (size_t i = 1; i <= iter; ++i)
    {
        selected = AllInputs[i];
        const auto r1 = remainder(selected.Phi, 90.0);
        const auto r2 = remainder(selected.Theta, 90.0);
        if ((r1 < 0.0 || r1 > 0.0) && (r2 < 0.0 || r2 > 0.0))
        {
            is_per_planes = false;
            iter = i;
        }
    }

    std::ofstream myfile;
    myfile.open (outfilename.c_str());
    myfile << "// File automatically generated from input file " << argv[1] << "\n";
    myfile << "// \n";
    if(is_per_planes){
        std::cout << "Given inputs are an orthogonal planes representation of the radiation pattern..." << std::endl;
        myfile << "// Given inputs are an orthogonal planes representation of the radiation pattern...\n\n";
        myfile << "PatternType op\n";
        std::vector<InputElement> PNplane (AllInputs.size()); // Phi-Normal plane (this is the horizontal plane if Theta is vertical).
        std::vector<InputElement> TPplane (AllInputs.size()); // Theta-Phi plane (for any Phi = 0°).
        std::vector<InputElement> TNplane (AllInputs.size()); // Theta-Normal plane (for any Phi = 90°).

        auto cpit = std::copy_if(AllInputs.begin(), AllInputs.end(), PNplane.begin(),
                                 [](InputElement ie)
        {
                const auto r = remainder(ie.Theta-90,180.0);
                return (r <= 0.0 && r >= 0.0);
        });
        PNplane.resize(size_t(std::distance(PNplane.begin(),cpit)));
        // riporto i valori sulla circonferenza orizzontale per Theta = 270 prima di riordinare
        for (size_t i = 0; i < PNplane.size(); ++i)
        {
            if (PNplane[i].Theta >= 270.0 && PNplane[i].Theta <= 270.0)
            {
                PNplane[i].Phi = PNplane[i].Phi + 180;
                PNplane[i].Theta = 90;
            }
        }
        std::sort (PNplane.begin(), PNplane.end(), lowerPhi);
        std::cout << "\033[1;34m Input for the PN plane are:\033[0m" << std::endl;
        myfile << "pnplane";
        for (size_t i = 0; i<PNplane.size(); ++i)
        {
            selected = PNplane[i];
            std::cout << i << " : " << selected.Phi << " "  << selected.Theta << " "  << selected.Gh << " "  << selected.Gv << " "  << selected.Gt << " " << std::endl;
            myfile << " | " << selected.Phi << ", " << selected.Gh << ", " << selected.Gv << ", " << selected.Gt;
        }
        myfile << "\n";


        cpit = std::copy_if (AllInputs.begin(), AllInputs.end(), TPplane.begin(), [](InputElement ie){return (ie.Phi == 0);} );
        TPplane.resize(std::distance(TPplane.begin(),cpit));
        std::sort (TPplane.begin(), TPplane.end(), lowerTheta);
        std::cout << "\033[1;34m Input for the TP plane are:\033[0m" << std::endl;
        myfile << "tpplane";
        for (size_t i = 0; i<TPplane.size(); ++i)
        {
            selected = TPplane[i];
            std::cout << i << " : " << selected.Phi << " "  << selected.Theta << " "  << selected.Gh << " "  << selected.Gv << " "  << selected.Gt << " " << std::endl;
            myfile << " | " << selected.Theta << ", " << selected.Gh << ", " << selected.Gv << ", " << selected.Gt;
        }
        myfile << "\n";


        cpit = std::copy_if (AllInputs.begin(), AllInputs.end(), TNplane.begin(), [](InputElement ie){return (ie.Phi == 90);} );
        TNplane.resize(size_t(std::distance(TNplane.begin(),cpit)));
        std::sort (TNplane.begin(), TNplane.end(), lowerTheta);
        std::cout << "\033[1;34m Input for the TN plane are:\033[0m" << std::endl;
        myfile << "tnplane";
        for (int i=0; i<TNplane.size(); i++)
        {
            selected = TNplane[i];
            std::cout << i << " : " << selected.Phi << " "  << selected.Theta << " "  << selected.Gh << " "  << selected.Gv << " "  << selected.Gt << " " << std::endl;
            myfile << " | " << selected.Theta << ", " << selected.Gh << ", " << selected.Gv << ", " << selected.Gt;
        }
        myfile << "\n";

    }
    else
    {
        if (tot_input == ThetaValues.size()*PhiValues.size())
        {
            std::cout << "Given inputs are a complete matrix representation of radiation pattern..." << std::endl;
            myfile << "// Given inputs are a complete matrix representation of the radiation pattern...\n\n";
            myfile << "PatternType cp " << ThetaValues.size() << " " << PhiValues.size() << "\n";
            myfile << "Theta ";
            for (it=ThetaValues.begin() ; it < ThetaValues.end(); ++it)
            {
                if (it != ThetaValues.begin()) myfile << ", ";
                myfile << *it ;
            }
            myfile << "\n";
            myfile << "Phi ";
            for (it=PhiValues.begin() ; it < PhiValues.end(); it++)
            {
                if (it!=PhiValues.begin()) myfile << ", ";
                myfile << *it ;
            }
            myfile << "\n";

            std::vector<InputElement> matrixLine (PhiValues.size());

            double to_find = ThetaValues[0];

            for (size_t l = 0; l < ThetaValues.size(); ++l)
            {
                auto cpit = std::copy_if(AllInputs.begin(), AllInputs.end(), matrixLine.begin(),
                                         [=](InputElement ie)
                {
                    return ie.Theta <= ThetaValues[l] && ie.Theta >= ThetaValues[l];
                });
                std::sort (matrixLine.begin(), matrixLine.end(), lowerPhi);
                myfile << ThetaValues[l];
                for (size_t i=0; i < matrixLine.size(); i++)
                {
                    selected = matrixLine[i];
                    myfile << " | " << selected.Phi << ", "<< selected.Gh << ", " << selected.Gv << ", " << selected.Gt;
                }
                myfile << "\n";
            }
        }
        else
        {
            // incomplete matrix
            std::cout << "Given inputs are a incomplete matrix representation of radiation pattern..." << std::endl;
        }
 //       selected = AllInputs[iter];
 //       std::cout << "E.g. => bad positioned radiation point at line " << iter+1 << " : " << selected.Phi << " " << selected.Theta << " " << selected.Gh << " " << selected.Gv << " " << selected.Gt << std::endl;
    }

    myfile.close();

    return 0;
}
