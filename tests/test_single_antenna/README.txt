After building go to executable dir and copy the main script:

cp ../../tests/test_single_antenna/printPolar.sh .

and the example csv file for custom antennas:


cp ../../tests/test_single_antenna/custom_dipole.csv .    	// A complete dipole

cp ../../tests/test_single_antenna/custom_dipole_att.csv . 	// A dipole with an 8dB attenuated semi-sphere



Then run the script:

source printPolar.sh [ param | noparam ]



... and follow the instructions to set up your own antenna ...




