// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A simple TLM task.

#if(defined _MSC_VER)
#define _USE_MATH_DEFINES
#endif
#include <cmath>
#include <sstream>
#include "SendTask_t.hh"

#define CSV

using namespace Scnsl::antennaModels;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

SendTask_t::SendTask_t(const sc_core::sc_module_name modulename,
                       const task_id_t id,
                       Scnsl::Core::Node_t * n,
                       const size_t proxies,
                       const double angle_granularity,
                       const double distance,
                       const double height,
                       const std::string & file ):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    _angle_granularity(angle_granularity),
    _radius(distance),
    _height(height),
    _file(file.c_str())
{
    SC_THREAD( _sender );
}


SendTask_t::~SendTask_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void SendTask_t::b_transport( tlm::tlm_generic_payload & p,
                              sc_core::sc_time & /*t*/ )
{
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND) )
    {
        // ntd
    }
    else if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND) )
    {
        // ntd
    }
    else
    {
        // ERROR.
        SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );
    }
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void SendTask_t::_fixZero(double & value) const
{
    if (fabs(value) <= 100 * std::numeric_limits<double>::epsilon())
        value = 0;
}

void SendTask_t::_sender()
{
    const std::string tp = "0";
    double angle = 0;
    int step = 0;
    Scnsl::Core::node_properties_t properties = getNodeProperties(tp);
#ifndef CSV
    std::cout << "***SESSION DATA***"
              << " ANGLE GRANULARITY: " << _angle_granularity
              << " RADIUS: "<< _radius
              << " HEIGHT: "<< _height
              << std::endl;
#endif
    byte_t payload[31];
    int * data = reinterpret_cast<int*>(payload);
    *data = 0;

    while ( true )
    {

        if (angle >= 360)
        {
            angle=0;
            step++;
            std::cout << "**************************************" << std::endl;
            std::cout << "**  Beginning Step n° " << step << std::endl;
            std::cout << "**************************************" << std::endl;

        }
        if (step==3){
            break;
        }

        if ( step == 0 ) {
            // evaluating xy plane

            properties.x = _radius * cos(angle * M_PI/180);
            properties.y = _radius * sin(angle * M_PI/180);
            properties.z = 0;
            _fixZero(properties.x);
            _fixZero(properties.y);
            _fixZero(properties.z);
            setNodeProperties(tp, properties);

        }

        if ( step == 1 ) {
            // evaluating xz plane

            properties.x = _radius * cos(angle * M_PI/180);
            properties.y = 0;
            properties.z = _radius * sin(angle * M_PI/180);
            _fixZero(properties.x);
            _fixZero(properties.y);
            _fixZero(properties.z);
            setNodeProperties(tp, properties);

        }

        if ( step == 2 ) {
            // evaluating yz plane

            properties.x = 0;
            properties.y = _radius * cos(angle * M_PI/180);
            properties.z = _radius * sin(angle * M_PI/180);
            _fixZero(properties.x);
            _fixZero(properties.y);
            _fixZero(properties.z);
            setNodeProperties(tp, properties);

        }

#ifndef CSV
        _file<< "SENDER "
                 << sc_core::sc_time_stamp()
                 << " " << *data
                 << " " << properties.x
                 << " " << properties.y
                 << " " << properties.z
                 << " " << angle
                 << std::endl;
#else
        _file<< sc_core::sc_time_stamp()
                 << ";" << *data
                 << ";" << properties.x
                 << ";" << properties.y
                 << ";" << properties.z
                 << ";" << angle
                 << std::endl;
#endif

        TlmTask_if_t::send(tp, payload, sizeof(payload));
        wait( 350, sc_core::SC_MS );
        ++*data;


        angle += _angle_granularity;

    }
}
