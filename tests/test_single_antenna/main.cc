// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <string>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "ReceiveTask_t.hh"
#include "SendTask_t.hh"

using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::antennaModels;

#define ENABLE_INPUT_PARAMETER


int sc_main( int argc, char * argv[] )
{

#ifdef ENABLE_INPUT_PARAMETER
    const double xaut = atof(argv[1]);
    const double yaut = atof(argv[2]);
    const double zaut = atof(argv[3]);

    const double distance = atof(argv[4]);
    const double height = atof(argv[5]);
    const std::string antennaModel = argv[6];
# else

    // set here your simulation parameter.

    const double xaut = 0;
    const double yaut = 0;
    const double zaut = 0;

    const double distance = 10;
    const double height = 0;

    // it may be one of the following:
    // CUSTOM , PATCH , PATCH2 , PATCH3 , QUARTERWAVE_DIPOLE , HALFWAVE_DIPOLE , FULLWAVE_DIPOLE
    const std::string antennaModel = "CUSTOM";
#endif

    const double granularity = 2;
    const std::string file = "prova";

    const double threshold = 1e-12;
    try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = sim->createNode();
        Scnsl::Core::Node_t * n1 = sim->createNode();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;
        ccs.name = "SharedChannel";
        ccs.alpha = 2;
        ccs.nodes_number = 2;

        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ccs.alpha);
        const Scnsl::Core::size_t PROXIES = 1;

        SendTask_t t0( "Task0", 0, n0, PROXIES, granularity,
                       distance, height, file + "_sender.txt");
        ReceiveTask_t t1( "TaskFrontal", 1, n1, PROXIES, file + "_receiver.txt");

        // Sender Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = nullptr;
        bsb0.node_binding.x = distance;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = height;
        bsb0.node_binding.bitrate = 1000;
        bsb0.node_binding.transmission_power = 0.010;
        //THRESHOLD
        bsb0.node_binding.receiving_threshold =  threshold;

        sim->bind(n0, ch, bsb0);
        sim->bind(&t0, &t1, ch, bsb0, nullptr);

        //Receiver Binding:
        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n0;
        bsb1.node_binding.x = xaut;
        bsb1.node_binding.y = yaut;
        bsb1.node_binding.z = zaut;
        bsb1.node_binding.bitrate = 1000;
        bsb1.node_binding.transmission_power = 0.010;
        bsb1.node_binding.receiving_threshold = threshold;
        using Coordinate_t = Scnsl::Core::Coordinate_t;
        if (antennaModel == "CUSTOM")
        {
#ifdef ENABLE_INPUT_PARAMETER
            CustomAntenna_t *adp= new CustomAntenna_t("Custom", Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), Coordinate_t(false, atof(argv[10]), atof(argv[11]), atof(argv[12])), Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, "radiation_pattern.rad");
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "PATCH")
        {
#ifndef ENABLE_INPUT_PARAMETER
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 110);
#else
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]), atof(argv[11]) );
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "PATCH2")
        {
#ifndef ENABLE_INPUT_PARAMETER
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 70, 1.0, Coordinate_t(false, 0, 1, 0), 100, 0.5, 4);
#else
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]), atof(argv[11]), Coordinate_t(false, atof(argv[12]), atof(argv[13]), atof(argv[14])), atof(argv[15]), atof(argv[16]), atof(argv[17]));
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "PATCH3")
        {
#ifndef ENABLE_INPUT_PARAMETER
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 50, 1.0, Coordinate_t(false, 0, 1, 0), 100, 0.5, 4, 110, 0.7, 5);
#else
            PatchPattern_t *adp= new PatchPattern_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]), atof(argv[11]), Coordinate_t(false, atof(argv[12]), atof(argv[13]), atof(argv[14])), atof(argv[15]), atof(argv[16]), atof(argv[17]), atof(argv[18]), atof(argv[19]), atof(argv[20]));
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "QUARTERWAVE_DIPOLE")
        {
#ifndef ENABLE_INPUT_PARAMETER
            QuarterWaveDipole_t *adp= new QuarterWaveDipole_t(Coordinate_t(false, 0, 1, 0), true, 0);
#else
            QuarterWaveDipole_t *adp= new QuarterWaveDipole_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]));
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "HALFWAVE_DIPOLE")
        {
#ifndef ENABLE_INPUT_PARAMETER
            HalfWaveDipole_t *adp= new HalfWaveDipole_t(Coordinate_t(false, 0, 1, 0), true, 0);
#else
            HalfWaveDipole_t *adp= new HalfWaveDipole_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]));
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "FULLWAVE_DIPOLE")
        {
#ifndef ENABLE_INPUT_PARAMETER
            FullWaveDipole_t *adp= new FullWaveDipole_t(Coordinate_t(false, 0, 1, 0), true);
#else
            FullWaveDipole_t *adp= new FullWaveDipole_t(Coordinate_t(false, atof(argv[7]), atof(argv[8]), atof(argv[9])), true, atof(argv[10]));
#endif
            bsb1.node_binding.lobes.push_back(adp);
        }
        else if (antennaModel == "CONE")
        {
            ConePattern_t *adp= new ConePattern_t(Coordinate_t(false, 1, 0, 0), true, 1, 80);
            bsb1.node_binding.lobes.push_back(adp);
        }
        // else if (antennaMOdel == "ISOTROPIC") {/*ntd*/}
        sim->bind(n1, ch, bsb1);
        sim->bind(&t1, &t0, ch, bsb1, nullptr);

        sc_core::sc_start();
    }
    catch ( std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
