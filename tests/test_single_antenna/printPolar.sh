# The following script prints the three orthogonal planes for a given antenna.
#
# - Copy this file in teh executable files directory:
#
#	cp ../../tests/test_single_antenna/printPolar.sh .
#
# - Modify you antenna positioning within the main file, than compile and run me with option noparam!! 
# - Run me with option param to start the setup procedure


echo

if [ $# -le 0 -o $# -gt 2 ]
then
    echo "Usage: polar_plot.sh [param|noparam]"
	return
fi

simulation=$1


getBigger() {
   awk -v n1="$1" -v n2="$2" 'BEGIN {printf (n1>n2?n1:n2) "\n"}'
}



rm -f prova* polar* *plane.csv *.rad




if [ "$simulation" == "param" ]
then 

	echo ""
	echo "** Setting global positions..."
	echo ""

	echo -n "Set x y x position for antenna under test (AUT) ( x y z ) > "
	read aut

	aut=($aut)
	xaut=${aut[0]}
	yaut=${aut[1]}
	zaut=${aut[2]}

	echo -n "Set monitor antenna distance (m) > "
	read distance
	echo -n "Set monitor antenna height (m) > "
	read height

	echo ""
	echo "** Setting antenna proprieties..."
	echo ""

	echo -n "Select the antenna model [ QUARTERWAVE_DIPOLE HALFWAVE_DIPOLE FULLWAVE_DIPOLE PATCH CUSTOM ] > "
	read aModel


	if [ "$aModel" == "PATCH" ]
	then 

		echo -n "Set the number of secondary lobes [ 0 1 2 ] > "
		read nLobes

		echo -n "Set x y z for theta (axes for the main lobe) > "
		read vTheta

		vTheta=($vTheta)
		xTheta=${vTheta[0]}
		yTheta=${vTheta[1]}
		zTheta=${vTheta[2]}

		echo -n "Set main lobe aperture (°) > "
		read mainAperture

		echo -n "Set main lobe gain (absolute) > "
	 	read mainGain

	    if [ $nLobes = "0" ] 
	    then
	    	aModel=$aModel
			./test_single_antenna "$xaut" "$yaut" "$zaut" "$distance" "$height" "$aModel" "$xTheta" "$yTheta" "$zTheta" "$mainAperture" "$mainGain"
		elif [ $nLobes = "1" ] 
	    then
			aModel="PATCH2"

			echo -n "Set x y z for phi (reference starting direction for secondary lobes) > "
			read vPhi

			vPhi=($vPhi)
			xPhi=${vPhi[0]}
			yPhi=${vPhi[1]}
			zPhi=${vPhi[2]}	

			echo -n "Set second level lobe aperture (° wrt theta) > "
			read secondAperture

			echo -n "Set second level lobe gain (absolute) > "
			read secondGain

			echo -n "Set second level lobe number > "
			read secondLobes

			./test_single_antenna "$xaut" "$yaut" "$zaut" "$distance" "$height" "$aModel" "$xTheta" "$yTheta" "$zTheta" "$mainAperture" "$mainGain" "$xPhi" "$yPhi" "$zPhi" "$secondAperture" "$secondGain" "$secondLobes"
		elif [ $nLobes = "2" ] 
	    then
			aModel="PATCH3"

			echo -n "Set x y z for phi (reference starting direction for secondary lobes) > "
			read vPhi

			vPhi=($vPhi)
			xPhi=${vPhi[0]}
			yPhi=${vPhi[1]}
			zPhi=${vPhi[2]}	

			echo -n "Set second level lobe aperture (° wrt theta) > "
			read secondAperture

			echo -n "Set second level lobe gain (absolute) > "
			read secondGain

			echo -n "Set second level lobe number > "
			read secondLobes

			echo -n "Set third level lobe aperture (° wrt theta) > "
			read thirdAperture

			echo -n "Set third level lobe gain (absolute) > "
			read thirdGain

			echo -n "Set third level lobe number > "
			read thirdLobes

			./test_single_antenna "$xaut" "$yaut" "$zaut" "$distance" "$height" "$aModel" "$xTheta" "$yTheta" "$zTheta" "$mainAperture" "$mainGain" "$xPhi" "$yPhi" "$zPhi" "$secondAperture" "$secondGain" "$secondLobes" "$thirdAperture" "$thirdGain" "$thirdLobes"
		fi

	elif [ $aModel = "CUSTOM" ]
	then
		echo "Available csv files:"
		ls *.csv
		echo -n "Select csv file > "
		read csv

		if [ -f $csv ] 
		then
			./parse_custom_antenna $csv
			mv *.rad radiation_pattern.rad

			echo -n "Set x y z for theta (your axes) > "
			read vTheta

			vTheta=($vTheta)
			xTheta=${vTheta[0]}
			yTheta=${vTheta[1]}
			zTheta=${vTheta[2]}

			echo -n "Set x y z for phi (reference rotation) > "
			read vPhi

			vPhi=($vPhi)
			xPhi=${vPhi[0]}
			yPhi=${vPhi[1]}
			zPhi=${vPhi[2]}	

			./test_single_antenna "$xaut" "$yaut" "$zaut" "$distance" "$height" "$aModel" "$xTheta" "$yTheta" "$zTheta" "$xPhi" "$yPhi" "$zPhi"

		else
			echo "ERROR: input csv file does not exist!"
		fi

	else

		echo -n "Set x y z for theta (dipole axes) > "
		read vTheta

		vTheta=($vTheta)
		xTheta=${vTheta[0]}
		yTheta=${vTheta[1]}
		zTheta=${vTheta[2]}

		echo -n "Set antenna gain (absolute) > "
	 	read mainGain

		./test_single_antenna "$xaut" "$yaut" "$zaut" "$distance" "$height" "$aModel" "$xTheta" "$yTheta" "$zTheta" "$mainGain"

	fi

elif [ "$simulation" == "noparam" ]
then 

	./test_single_antenna

else
	echo "ERROR: Not accepted parameter"
	echo "Usage: polar_plot.sh [param|noparam]"
	return
fi


./test_single_antenna_parser "prova_sender.txt" "prova_receiver.txt" "prova.csv"


sed -n '1,1p' prova.csv > xy_plane.csv
sed -n '1,1p' prova.csv > xz_plane.csv
sed -n '1,1p' prova.csv > yz_plane.csv
sed -n '2,181p' prova.csv >> xy_plane.csv
sed -n '182,361p' prova.csv >> xz_plane.csv
sed -n '362,541p' prova.csv >> yz_plane.csv



for plane in "xy" "xz" "yz"; do 

input="${plane}_plane.csv"
title="polar_${plane}"
# mode may be mW or dB
mode="mW"


if [ $mode = "mW" ]
then

	echo "Plotting power received by the antennas in mW..."
	output="${title}_mW.eps"
	plot="plot '$input' using 6:((\$7)*1000) title ''"

elif [ $mode = "dB" ]
then

	echo "Plotting power received by the antennas in dB..."
	output="${title}_dBm.eps"
	plot="plot '$input' using 6:8 title ''"
else
	echo "Error: \"$mode\" is not an accepted argument"
	exit
fi

massimo=0
if [ $plane = "xy" ] 
then
   	hlabel="X"
   	vlabel="Y"

	sed 1d xy_plane.csv | while read line           
	do  
		this=`echo $line | cut -d ";" -f 7`
		massimo=`getBigger $this $massimo`
		echo $massimo > max.txt

	done

	elif [ $plane = "xz" ]
	then
	   hlabel="X"
	   vlabel="Z"

	sed 1d xz_plane.csv | while read line           
	do  
		this=`echo $line | cut -d ";" -f 7`
		massimo=`getBigger $this $massimo`
		echo $massimo > max.txt

	done

	elif [ $plane = "yz" ]
	then
	   hlabel="Y"
	   vlabel="Z"

	sed 1d yz_plane.csv | while read line           
	do  
		this=`echo $line | cut -d ";" -f 7`
		massimo=`getBigger $this $massimo`
		echo $massimo > max.txt

	done 
fi


massimo=$(<max.txt)
massimo=`awk '{printf "%.10f\n", $1}' max.txt`



if [ $mode = "mW" ]
then
    massimo=`echo "$massimo*1000" | bc`
fi

divisor=0
for i in 0.1 0.2 0.5 1 2 5 10 20 50 100 200 500 1000 2000 5000 10000 20000 50000 100000 200000 50000; do
    prova=`echo "$massimo*$i" | bc`
    prova2=`echo ${prova/\.*}`
    if [ -z "$prova2" ]
    then
    	divisor=$i
    fi

done


#echo "divisor is " $divisor
#echo "massimo is " $massimo


if [ $divisor = "0" ]
then
# this may happens when massimo is 0
    divisor=50000
fi

limitp=`echo "1.0 / $divisor" | bc -l`
limitn=-1*$limitp
label=`echo "$limitp*1.25" | bc`
#echo "limitp is " $limitp
#echo $label
mytick=`echo "$limitp*0.2" | bc`






#Generate plot
echo "
set terminal postscript color eps enhanced \"Times-Roman\" 20
unset border
set polar
set angles degrees

set style line 10 lt 1 lc 0 lw 0.2
set grid polar 30

set xrange [$limitn:$limitp]
set yrange [$limitn:$limitp]

unset xtics
unset ytics

set rtic font ', 1'
set rtic($mytick,2*$mytick,3*$mytick,4*$mytick,5*$mytick)

unset raxis

r=5*$mytick

set for [i=1:5] label at first r*0.02, first r*((i/5.0) + 0.05) sprintf('%.5f', '$mytick'*i)
unset raxis


set label '$hlabel' at $label,0
set label '$vlabel' at 0,$label

set size square
set key lmargin

set style line 11 lt 1 lw 2 pt 2 ps 2

set datafile sep ';'
set style data linespoints
set output '$output'
set title '$plane plane received power' offset 0,2
$plot" | gnuplot > $output

echo "Plot saved in $output"

done

evince polar* &

