/// @file
/// A simple TLM task.

#include "Tcp_server_static.hh"

#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Syscalls;
using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl;

Tcp_server::Tcp_server(const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_server::~Tcp_server()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_server::main()
{
    initTime();
    int sockfd, newsockfd, portno;
    byte_t buffer[256];
    socklen_t clilen;
    struct sockaddr serv_addr, cli_addr;
    int n;
    // create a socket
    // socket(int domain, int type, int protocol)
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) throw "ERROR opening socket";

    inet_pton(AF_INET, "192.168.0.5", &serv_addr.sin_addr);

    portno = 2020;
    // convert short integer value for port must be converted into network byte
    // order
    serv_addr.sin_port = portno;

    // bind(int fd, struct sockaddr *local_addr, socklen_t addr_length)
    // bind() passes file descriptor, the address structure,
    // and the length of the address structure
    // This bind() call will bind  the socket to the current IP address on port
    // portno
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw std::runtime_error("ERROR on binding");

    // This listen() call tells the socket to internal_listen to the incoming
    // connections. The internal_listen() function places all incoming
    // connection into a backlog queue until internal_accept() call accepts the
    // connection. Here, we set the maximum size for the backlog queue to 5.
    listen(sockfd, 5);

    // The internal_accept() call actually accepts an incoming connection
    clilen = sizeof(cli_addr);
    // This internal_accept() function will write the connecting client's
    // address info into the the address structure and the size of that
    // structure is clilen. The internal_accept() returns a new socket file
    // descriptor for the accepted connection. So, the original socket file
    // descriptor can continue to be used for accepting new connections while
    // the new socker file descriptor is used for communicating with the
    // connected client.
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);

    if (newsockfd < 0) 
        throw std::runtime_error("ERROR on internal_accept");

    bzero(buffer, 256);

    n = recv(newsockfd, buffer, 255, 0);

    if (n < 0)
        throw std::runtime_error("ERROR reading from socket");
    printf("Here is the message: %s\n", buffer);

    // This send() function sends the 13 bytes of the string to the new socket
    Syscalls::send(newsockfd, (byte_t *)"Hello, world!\n", 14, 0);

    Syscalls::close(newsockfd);
    Syscalls::close(sockfd);
}
