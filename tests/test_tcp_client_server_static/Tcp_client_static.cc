#include "Tcp_client_static.hh"

#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;

using namespace Scnsl::Syscalls;


Tcp_client::Tcp_client(const sc_core::sc_module_name modulename, const task_id_t id,
                        Scnsl::Core::Node_t * n, const size_t proxies):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

Tcp_client::~Tcp_client()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Tcp_client::main()
{
    using Scnsl::Syscalls::send;

    initTime();
    int sockfd, portno, n;
    struct sockaddr serv_addr;
    byte_t buffer[256];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    portno = 2020;
    serv_addr.sin_port = portno;
    inet_pton(AF_INET, "192.168.0.5", &serv_addr.sin_addr);

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw std::runtime_error("ERROR connecting");

    for (int i = 0; i < 256; i++)
    {
        buffer[i] = byte_t(rand() % 25 + 65);
    }

    std::cerr << " CLIENT: SENDING DATA" << std::endl;
    n = send(sockfd, buffer, 256, 0);

    if (n < 0)
        throw std::runtime_error("ERROR writing to socket");
    bzero(buffer, 256);

    n = recv(sockfd, buffer, 255, 0);

    if (n < 0) 
        throw std::runtime_error("ERROR reading from socket");
    printf("%s\n", buffer);
    close(sockfd);
}
