
#ifndef MYTASK_CLIENT_HH
#define MYTASK_CLIENT_HH

#include <scnsl.hh>
#include <systemc>

/// @file
/// A simple TLM task.

class Tcp_client: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param is_sender True if is sender.
    /// @param test The kind of test.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    Tcp_client(const sc_core::sc_module_name modulename, const task_id_t id,
                Scnsl::Core::Node_t * n, const size_t proxies);

    /// @brief Virtual destructor.
    virtual ~Tcp_client();

private:
    /// @name Processes.
    // @}
    void main() override;
    //@{
};

#endif
