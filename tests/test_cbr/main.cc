// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTaskSender.hh"
#include "MyTaskReceiver.hh"


using namespace Scnsl;
//using namespace Scnsl::Channels;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using namespace Scnsl::Traffic;



int sc_main( int argc, char * argv[] )
{
	try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n1 = sim->createNode();
        Scnsl::Core::Node_t * n2 = sim->createNode();


        CoreChannelSetup_t csb;

        csb.channel_type = CoreChannelSetup_t::UNIDIRECTIONAL;
        csb.capacity = 1000;
        csb.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb.extensionId = "core";
        csb.name = "channel_unidirectional";

        Scnsl::Core::Channel_if_t * ch1 = sim->createChannel( csb );


		CoreTaskSetup_t corets;

		corets.task_type = CoreTaskSetup_t::CBR;
		corets.name = "task1";
		corets.id = 1;
		corets.n = n1;
		corets.pktSize = 8;
		corets.genTime = sc_core::sc_time(5, sc_core::SC_MS);
		corets.extensionId = "core";

        BindSetup_base_t bsb1;
        BindSetup_base_t bsb2;
        bsb1.extensionId = "core";
        bsb2.extensionId = "core";
        sim->bind(n1, ch1, bsb1);
        sim->bind(n2, ch1, bsb2);

		// Manual creation
		//sc_core::sc_time genT = sc_core::sc_time(5, sc_core::SC_MS);
        //MyTaskSender t1( "task1", 1, n1, 1, 8, genT );

		Scnsl::Core::Task_if_t * t1 = sim->createTask( corets );
		MyTaskReceiver t2( "task2", 2, n2, 1 );

        sim->bind(t1, nullptr, ch1, bsb1, nullptr);
        sim->bind(&t2, nullptr, ch1, bsb2, nullptr);

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // - Setting to trace all:
        cts.info = 5;
        cts.debug = 5;
        cts.log = 5;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // Core formatter specific option:
        //  printing also the type of trace:
        cts.print_trace_type = true;
        // - Creating:
        Scnsl_t::Tracer_t * tracer = sim->createTracer( cts );
        // - Setting the output stream:
        tracer->addOutput( & std::cout );
        // - Adding to trace:
        tracer->trace( t1 );
        tracer->trace( & t2 );

		static_cast< Scnsl::Traffic::Cbr_t * >( t1 )->enable();

        sc_core::sc_start( sc_core::sc_time( 500, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}
    catch (exception & e)
    {
		cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}

	return 0;
}
