// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A TLM task.

#include <sstream>

//#include "TlmTaskProxy_t.hh"
#include "MyTaskSender.hh"


//using Scnsl::Tlm::TlmTask_if_t;


// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTaskSender::MyTaskSender( sc_core::sc_module_name modulename,
                            const task_id_t id,
                            Scnsl::Core::Node_t * n,
                            const size_t proxies,
							label_t label,
							const size_t pktSize,
			  				const sc_core::sc_time genTime )
    :
    // Parents:
    Scnsl::Traffic::Cbr_t( modulename, id, n, proxies, label, pktSize, genTime )
{
	SC_THREAD( writingProcess );
}

MyTaskSender::~MyTaskSender()  {}

void MyTaskSender::writingProcess()
{

	Cbr_t::enable();
	wait(100, sc_core::SC_MS);
	Cbr_t::disable();
	wait(200, sc_core::SC_MS);
	Cbr_t::enable();

}
