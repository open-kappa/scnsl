// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A TLM task.

#include <sstream>

#include "MyTaskReceiver.hh"


//using namespace Scnsl::Tlm;


// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTaskReceiver::MyTaskReceiver( sc_core::sc_module_name modulename,
                                const task_id_t id,
                                Scnsl::Core::Node_t * n,
                                const size_t proxies )
    :
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies )
{
}


MyTaskReceiver::~MyTaskReceiver()  {}


void MyTaskReceiver::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t )
{
	t=t;
	char * result;
	bool c;
    size_t s;

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
	{
    	//result = static_cast< char >( * p.get_data_ptr() );
        s = p.get_data_length();

		result = static_cast< char *>( malloc ( s ));
        memcpy(result, p.get_data_ptr(), s);

        // Since we are tracing a complex message,
        // we wrap the log with the macro, to avoid
        // unuseful overhead.
#if ( SCNSL_LOG >= 1 )
        std::stringstream ss;
        ss << "data received: " << result << " bytes: " << s;
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif

	}

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND))
	{
		c = * p.get_data_ptr() != 0;

        // Since we are tracing a complex message,
        // we wrap the log with the macro, to avoid
        // unuseful overhead.
#if ( SCNSL_LOG >= 1 )
        std::stringstream ss;
        ss << "carrier: " << c;
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif
	}
}
