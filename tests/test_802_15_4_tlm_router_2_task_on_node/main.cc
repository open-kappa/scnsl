// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask.hh"
#include "MyTask2.hh"
#include "MyTask3.hh"
#include "MyTaskRouter.hh"

#include <map>

using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using Scnsl::Tracing::Traceable_base_t;


int sc_main( int argc, char * argv[] )
{
	try {

        Scnsl_t * sim = Scnsl_t::get_instance();


        // Nodes creation
        Node_t * n1 = sim->createNode();
        Node_t * n2 = sim->createNode();
        Node_t * n3 = sim->createNode();
        Node_t * nodoRouter = sim->createNode();

	// Channels
        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;

        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs.nodes_number = 4;
        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );

        Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha);
	// n1
        CoreCommunicatorSetup_t cc1;
        cc1.extensionId = "core";
        cc1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc1.name = "mac802_15_4_n1";
        cc1.node = n1;

        cc1.ack_required = false;
        cc1.short_addresses = false;

        CoreCommunicatorSetup_t ccr1;
        ccr1.extensionId="core";
        ccr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr1.name = "macRouter_n1";
        ccr1.node=n1;

        // n2
        CoreCommunicatorSetup_t cc2;
        cc2.extensionId = "core";
        cc2.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc2.name = "mac802_15_4_n2";
        cc2.node = n2;

        cc2.ack_required = false;
        cc2.short_addresses = false;

        CoreCommunicatorSetup_t ccr2;
        ccr2.extensionId="core";
        ccr2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr2.name = "macRouter_n2";
        ccr2.node=n2;

         // n3
        CoreCommunicatorSetup_t cc3;
        cc3.extensionId = "core";
        cc3.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc3.name = "mac802_15_4_n3";
        cc3.node = n3;

        cc3.ack_required = false;
        cc3.short_addresses = false;

        CoreCommunicatorSetup_t ccr3;
        ccr3.extensionId="core";
        ccr3.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr3.name = "macRouter_n3";
        ccr3.node=n3;

        // gateway
        CoreCommunicatorSetup_t ccoms1;
        ccoms1.extensionId = "core";
        ccoms1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        ccoms1.name = "MacRouter1802";
        ccoms1.node = nodoRouter;
        ccoms1.ack_required = false;
        ccoms1.short_addresses = false;

        CoreCommunicatorSetup_t ccomsr1;
        ccomsr1.extensionId="core";
        ccomsr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccomsr1.name = "MacRouter1Routing";
        ccomsr1.node = nodoRouter;


        // gateway table:
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_nodes;
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > table_gw;
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_second (ch,nodoRouter);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second (ch,n1);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second_II (ch,n2);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > table_gw_second_III (ch,n3);

        table_nodes[n1] = table_second;
        table_nodes[n2] = table_second;
        table_nodes[n3] = table_second;

        ccr1.routingTable = table_nodes;
        ccr2.routingTable = table_nodes;
        ccr3.routingTable = table_nodes;

        table_gw[n1]=table_gw_second; table_gw[n2]=table_gw_second_II;
        table_gw[n3]=table_gw_second_III;
        ccomsr1.routingTable = table_gw;


         // n1
        Communicator_if_t * mac1_802 = sim->createCommunicator( cc1 );
        Communicator_if_t * mac1_router = sim->createCommunicator( ccr1 );
        mac1_802->stackUp(mac1_router);
        mac1_router->stackDown(mac1_802);

        // n2
        Communicator_if_t * mac2_802 = sim->createCommunicator( cc2 );
        Communicator_if_t * mac2_router = sim->createCommunicator( ccr2 );
        mac2_802->stackUp(mac2_router);
        mac2_router->stackDown(mac2_802);

        // n3
        Communicator_if_t * mac3_802 = sim->createCommunicator( cc3 );
        Communicator_if_t * mac3_router = sim->createCommunicator( ccr3 );
        mac3_802->stackUp(mac3_router);
        mac3_router->stackDown(mac3_802);

        // gateway
        Communicator_if_t * macgw1802 = sim->createCommunicator( ccoms1 );
        Communicator_if_t * macgw1Routing = sim->createCommunicator( ccomsr1 );
        macgw1802->stackUp(macgw1Routing);
        macgw1Routing->stackDown(macgw1802);

        Communicator_if_t * macStackN1 = new Scnsl::Utils::CommunicatorStack_t(mac1_router, mac1_802);
        Communicator_if_t * macStackN2 = new Scnsl::Utils::CommunicatorStack_t(mac2_router, mac2_802);
        Communicator_if_t * macStackN3 = new Scnsl::Utils::CommunicatorStack_t(mac3_router, mac3_802);
        Communicator_if_t * macStackgw1 = new Scnsl::Utils::CommunicatorStack_t(macgw1Routing, macgw1802);

        MyTask t1 ("T1",1,n1,1);
        MyTask t2 ( "T2", 2, n1,1);
        MyTask3 t3 ("T3",3,n2,1);
        MyTask3 t4 ("T4",4,n3,1);
        MyTaskRouter tr("ROUTER",0,nodoRouter,1);


        BindSetup_base_t bsbt1;
        BindSetup_base_t bsbrouter;
        BindSetup_base_t bsbt2;
        BindSetup_base_t bsbt3;
        BindSetup_base_t bsbt4;

        bsbt1.extensionId = "core";
        bsbt3.extensionId = "core";
        bsbrouter.extensionId = "core";
        bsbt2.extensionId = "core";
        bsbt4.extensionId = "core";

        // --------------------------------------

        bsbt1.destinationNode = nodoRouter;
        bsbt2.destinationNode = nullptr;
        bsbt3.destinationNode = nullptr;
        bsbt4.destinationNode = nullptr;
        bsbrouter.destinationNode = nullptr;

        // ---------------------------------------

        bsbt1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt1.node_binding.transmission_power = 100;
        bsbt1.node_binding.receiving_threshold = 10;
        bsbt1.node_binding.x = 1;
        bsbt1.node_binding.y = 1;
        bsbt1.node_binding.z = 1;

        sim->bind(n1, ch, bsbt1);

        bsbrouter.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbrouter.node_binding.transmission_power = 100;
        bsbrouter.node_binding.receiving_threshold = 10;
        bsbrouter.node_binding.x = 4;
        bsbrouter.node_binding.y = 4;
        bsbrouter.node_binding.z = 4;

        sim->bind(nodoRouter, ch, bsbrouter);


	bsbt2.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt2.node_binding.transmission_power = 100;
        bsbt2.node_binding.receiving_threshold = 10;
        bsbt2.node_binding.x = 5;
        bsbt2.node_binding.y = 5;
        bsbt2.node_binding.z = 5;

        sim->bind(n2, ch, bsbt2);


        bsbt4.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt4.node_binding.transmission_power = 100;
        bsbt4.node_binding.receiving_threshold = 10;
        bsbt4.node_binding.x = 6;
        bsbt4.node_binding.y = 6;
        bsbt4.node_binding.z = 6;

        sim->bind(n3, ch, bsbt4);


        sim->bind(&t1, &t3, ch, bsbt1, macStackN1);
        sim->bind(&t2, &t4, ch, bsbt1, macStackN1);

        sim->bind(&tr, nullptr, ch, bsbrouter, macStackgw1);

        sim->bind(&t3, nullptr, ch, bsbt3, macStackN2);
        sim->bind(&t4, nullptr, ch, bsbt4, macStackN3);


        sc_core::sc_start( sc_core::sc_time( 5000, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}

    catch (exception & e)
    {
	cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
