**************************************************
*  test_directional_more_lobes                   *
**************************************************
In this simple test there are 2 nodes: it is possible to test the situation with
both nodes sending packets or with only one source node.
It is possible to test some different situations:

     -  both nodes are omnidirectional (but still using the Directional Environment)
     -  only the sender is directional while the receiver remains omnidirectional
     -  both the tasks are directional and each of them has ten directional lobes.
        The aim of the example is to show the iteration along the lobes vector of both
        sides (sender and receiver) in order to find a combination that allows the 
        communication to take place.
     -  both the tasks are directional and each of them has ten directional lobes.
        In addition, also the Task1 is sending packets here. 
