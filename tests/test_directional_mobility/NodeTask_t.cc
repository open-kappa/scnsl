// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A simple TLM task.


#include <sstream>
#include "NodeTask_t.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

using namespace Scnsl::antennaModels;

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

NodeTask_t::NodeTask_t( const sc_core::sc_module_name modulename,
                        const bool is_sender,
                        const task_id_t id,
                        Scnsl::Core::Node_t * n,
                        const size_t proxies,
                        Scnsl::Core::Channel_if_t * ch):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    // Fields:
    _packetArrivedEvent(),
    _channel(ch),
    _node(n)
{
    SC_THREAD( _circularMotion );
    if ( is_sender )
    {
        SC_THREAD( _sender );
    }
}


NodeTask_t::~NodeTask_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void NodeTask_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t )
{
    char result;
    bool c;

    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
    {
        result = * reinterpret_cast< char * >( p.get_data_ptr() );

#if (SCNSL_LOG >= 1)
        std::stringstream ss;
        ss << "RECEIVE data: " << result << ", size: " << p.get_data_length() << ".";
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif

        _packetArrivedEvent.notify();
    }
    else if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND) )
    {
        c = * reinterpret_cast< bool * >( p.get_data_ptr() );
#if (SCNSL_LOG >= 1)
        std::stringstream ss;
        ss << "carrier: " << c << ".";
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif

    }
    else
    {
        // ERROR.
        SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );

        // Just to avoid compiler warnings:
        t = sc_core::sc_time_stamp();
    }
}


// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////


void NodeTask_t::_sender()
{
    const std::string tp = "0";
    const size_t s = 1;

    while ( true )
    {
        byte_t i = static_cast<Scnsl::Core::byte_t>( rand()%25 + 65 );

#if ( SCNSL_LOG >= 1 )
        std::stringstream ss;
        ss << "SEND data: " << i << " size: " << s << ".";
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif

        TlmTask_if_t::send( tp, &i, s);
    }
}


void NodeTask_t::_circularMotion()
{
    using Coordinate_t = Scnsl::Core::Coordinate_t;
    Scnsl::Core::node_properties_t np=_node->getProperties(_channel);
    ConePattern_t *cp1 = new ConePattern_t(Coordinate_t(false, 1, 0, 0), true, 1, 30);
    ConePattern_t *cp2 = new ConePattern_t(Coordinate_t(false, 0, 1, 0), true, 1, 30);
    ConePattern_t *cp3 = new ConePattern_t(Coordinate_t(false, -1, 0, 0), true, 1, 30);
    ConePattern_t *cp4 = new ConePattern_t(Coordinate_t(false, 0, -1, 0), true, 1, 30);
    np.lobes.push_back(cp1);
    np.lobes.push_back(cp2);
    np.lobes.push_back(cp3);
    np.lobes.push_back(cp4);
    _node->setProperties(np, _channel, false);
    bool forward=true;

    while(true)
    {
        Scnsl::Core::node_properties_t np0=_node->getProperties(_channel);

        if(np0.x<500.1 && forward){
            np0.x+=.5;
            np0.y=fabs(sqrt((1-(np0.x*np0.x)/(500*500))*500*500));
            if(np0.x>500)forward=false;
        }
        else if(np0.x>(-500.1) && !forward){
            np0.x-=.5;
            np0.y=-fabs(sqrt((1-(np0.x*np0.x)/(500*500))*500*500));
            if(np0.x<(-500))forward=true;
        }
        _node->setProperties(np0, _channel, false);
        wait( 3, sc_core::SC_MS );

    }
}
