// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef NODETASK_T_HH
#define NODETASK_T_HH

#include <cmath>
#include <systemc>
#include <scnsl.hh>

/// @file
/// A simple TLM task.


/// @brief A simple TLM task.
///
/// Design patterns:
///
/// - Non copyable.
/// - Non assignable.
///
class NodeTask_t:
    public Scnsl::Tlm::TlmTask_if_t
{
public:

    SC_HAS_PROCESS( NodeTask_t );

    /// @brief Constuctor.
    ///
    /// @param modulename This module name.
    /// @param is_sender True if is sender.
    /// @param id this task ID.
    /// @param n The relative host node.
    /// @param proxies The number of bounded proxies.
    ///
    NodeTask_t( const sc_core::sc_module_name modulename,
              const bool is_sender,
              const task_id_t id,
              Scnsl::Core::Node_t * n,
              const size_t proxies,
              Scnsl::Core::Channel_if_t * ch);

    /// @brief Virtual destructor.
    virtual
    ~NodeTask_t();

    /// @name Inherited interface methods.
    //@{


    virtual
    void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

    //@}

    /// @brief Signals when a packet is arrived.
    sc_core::sc_event _packetArrivedEvent;

private:

    Scnsl::Core::Channel_if_t * _channel;
    Scnsl::Core::Node_t * _node;
    /// @name Processes.
    //@{

    /// @brief Sender process.
    void _sender();

    void _circularMotion();

    //@}

    /// @brief Disabled copy constructor.
    NodeTask_t( const NodeTask_t & );

    /// @brief Disabled assignment operator.
    NodeTask_t & operator = ( NodeTask_t & );
};



#endif
