// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "TaskRouter.hh"
#include "TaskRouter2.hh"




using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using namespace Scnsl::Traffic;


int sc_main( int argc, char * argv[] )
{
	try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

		CoreTopologySetup_t cts;
		cts.topology_type = CoreTopologySetup_t::BOTTLENECK;
		cts.extensionId = "core";


		// setup channel 1
		CoreChannelSetup_t csb1;
        csb1.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb1.capacity = 1000;
		csb1.capacity2 = 1000;
        csb1.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb1.extensionId = "core";
        csb1.name = "channel_1_fullduplex";
		cts.channel_list.push_back( & csb1 );

		// setup channel 2
		CoreChannelSetup_t csb2;
        csb2.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb2.capacity = 1000;
		csb2.capacity2 = 1000;
        csb2.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb2.extensionId = "core";
        csb2.name = "channel_2_fullduplex";
		cts.channel_list.push_back( & csb2 );

		// setup channel 3
		CoreChannelSetup_t csb3;
        csb3.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb3.capacity = 1000;
		csb3.capacity2 = 1000;
        csb3.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb3.extensionId = "core";
        csb3.name = "channel_3_fullduplex";
		cts.channel_list.push_back( & csb3 );

		// setup channel 4
		CoreChannelSetup_t csb4;
        csb4.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb4.capacity = 1000;
		csb4.capacity2 = 1000;
        csb4.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb4.extensionId = "core";
        csb4.name = "channel_4_fullduplex";
		cts.channel_list.push_back( & csb4 );

		// setup channel 5
		CoreChannelSetup_t csb5;
        csb5.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb5.capacity = 1000;
		csb5.capacity2 = 1000;
        csb5.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb5.extensionId = "core";
        csb5.name = "channel_5_fullduplex";
		cts.channel_list.push_back( & csb5 );

		// bind 1
		BindSetup_base_t bsb1;
		bsb1.extensionId = "core";
		cts.bind_list.push_back( & bsb1 );

		// bind 2
		BindSetup_base_t bsb2;
		bsb2.extensionId = "core";
		cts.bind_list.push_back( & bsb2 );

		// bind 3
		BindSetup_base_t bsb3;
		bsb3.extensionId = "core";
		cts.bind_list.push_back( & bsb3 );

		// bind 4
		BindSetup_base_t bsb4;
		bsb4.extensionId = "core";
		cts.bind_list.push_back( & bsb4 );

		// bind 5
		BindSetup_base_t bsb5;
		bsb5.extensionId = "core";
		cts.bind_list.push_back( & bsb5 );

		// bind 6
		BindSetup_base_t bsb6;
		bsb6.extensionId = "core";
		cts.bind_list.push_back( & bsb6 );


		sim->createTopology( cts );

		std::vector< Node_t * > nodes = sim->getNodes();
		std::vector< Channel_if_t * > channel_list = sim->getChannels();

		// task1 CBR
		CoreTaskSetup_t ct1;
		ct1.task_type = CoreTaskSetup_t::CBR;
		ct1.name = "task1_cbr";
		ct1.id = 1;
		ct1.n = nodes[ 0 ];
		ct1.pktSize = 8;
		ct1.label = 0;
		ct1.genTime = sc_core::sc_time(5, sc_core::SC_MS);
		ct1.extensionId = "core";
        Task_if_t * t1 = sim->createTask( ct1 );


		// task3 ON_OFF
		CoreTaskSetup_t ct3;
		ct3.task_type = CoreTaskSetup_t::ON_OFF;
		ct3.name = "task3_on_off";
		ct3.id = 3;
		ct3.n = nodes[ 2 ];
		ct3.pktSize = 8;
		ct3.label = 1;
		ct3.genTime = sc_core::sc_time(5, sc_core::SC_MS);
		ct3.on = sc_core::sc_time(50, sc_core::SC_MS);
		ct3.off = sc_core::sc_time(30, sc_core::SC_MS);
		ct3.extensionId = "core";
        Task_if_t * t3 = sim->createTask( ct3 );


		// task2 PIT
		CoreTaskSetup_t ct2;
		ct2.task_type = CoreTaskSetup_t::PIT;
		ct2.name = "task2_pit";
		ct2.id = 2;
		ct2.n = nodes[ 1 ];
		ct2.extensionId = "core";
		Task_if_t * t2 = sim->createTask( ct2 );

		// task4 PIT
		CoreTaskSetup_t ct4;
		ct4.task_type = CoreTaskSetup_t::PIT;
		ct4.name = "task4_pit";
		ct4.id = 4;
		ct4.n = nodes[ 3 ];
		ct4.extensionId = "core";
		Task_if_t * t4 = sim->createTask( ct4 );

		// task5 router
		TaskRouter t5( "Task5_router", 5, nodes[ 4 ], 3 );
		// task6 router
		TaskRouter2 t6( "Task6_router", 6, nodes[ 5 ], 3 );


		CoreCommunicatorSetup_t ccs;
		ccs.extensionId = "core";
		ccs.type = CoreCommunicatorSetup_t::QUEUE;
		ccs.queueSend = CoreCommunicatorSetup_t::NONE;
		ccs.queueReceive = CoreCommunicatorSetup_t::PRIORITY;
		ccs.capacityReceive = 16;
		ccs.numberReceive = 2;
		ccs.policyReceive = CoreCommunicatorSetup_t::STRONG_PRIORITY;
		ccs.name = "ccs_priority1";
		Communicator_if_t * coda1 = sim->createCommunicator( ccs );

		ccs.name = "ccs_priority2";
		Communicator_if_t * coda2 = sim->createCommunicator( ccs );


		// bind task
        sim->bind(t1, nullptr, channel_list[0], bsb1, nullptr);
        sim->bind(t3, nullptr, channel_list[2], bsb3, nullptr);

        sim->bind(&t5, nullptr, channel_list[0], bsb5, coda1);
        sim->bind(&t5, nullptr, channel_list[2], bsb5, coda1);
        sim->bind(&t5, nullptr, channel_list[4], bsb5, coda1);

        sim->bind(&t6, nullptr, channel_list[1], bsb6, coda2);
        sim->bind(&t6, nullptr, channel_list[3], bsb6, coda2);
        sim->bind(&t6, nullptr, channel_list[4], bsb6, coda2);

        sim->bind(t2, nullptr, channel_list[1], bsb2, nullptr);
        sim->bind(t4, nullptr, channel_list[3], bsb4, nullptr);



		// Adding tracing features:
        CoreTracingSetup_t ctrs;
        ctrs.extensionId = "core";
        // - Setting the formatter:
        ctrs.formatterExtensionId = "core";
        ctrs.formatterName = "basic";
        // Setting the filter:
        ctrs.filterExtensionId = "core";
        ctrs.filterName = "basic";
        // - Setting to trace all:
        ctrs.info = 5;
        ctrs.debug = 5;
        ctrs.log = 5;
        ctrs.error = 5;
        ctrs.warning = 5;
        ctrs.fatal = 5;
        // Core formatter specific option:
        //  printing also the type of trace:
        ctrs.print_trace_type = true;
        // - Creating:
        Scnsl_t::Tracer_t * tracer = sim->createTracer( ctrs );
        // - Setting the output stream:
        tracer->addOutput( & std::cout );
        // - Adding to trace:
        tracer->trace( t1 );
        tracer->trace( t3 );
		tracer->trace( & t5 );
		tracer->trace( & t6 );
		tracer->trace( t2 );
		tracer->trace( t4 );







		static_cast< Scnsl::Traffic::Cbr_t * >( t1 )->enable();
		static_cast< Scnsl::Traffic::OnOff_t * >( t3 )->enable();

        sc_core::sc_start( sc_core::sc_time( 500, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}
    catch (exception & e)
    {
		cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
