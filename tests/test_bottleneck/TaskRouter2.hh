// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_TASK_ROUTER2_HH
#define SCNSL_TASK_ROUTER2_HH



/// @file
/// Router node.


#include <systemc>
#include <tlm.h>


// Including the library:
#include <scnsl.hh>



class TaskRouter2 :
    public Scnsl::Tlm::TlmTask_if_t
{

public:

    SC_HAS_PROCESS( TaskRouter2 );

    /// @brief Constructor.
    ///
    /// @param name This module name.
    /// @param id this module unique ID.
    /// @param n The node on which this task is placed.
    /// @param proxies The number of connected task proxies.
    /// @throw std::invalid_argument If proxies is zero.
    ///
	TaskRouter2( sc_core::sc_module_name name,
            	const task_id_t id,
            	Scnsl::Core::Node_t * n,
            	const size_t proxies );

    virtual ~TaskRouter2();

	virtual void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t );

protected:

    /// @name Processes.
    //@{

    void writingProcess();

    //@}

private:

    TaskRouter2( TaskRouter2 & );

    TaskRouter2 & operator = ( TaskRouter2 & );

};

#endif
