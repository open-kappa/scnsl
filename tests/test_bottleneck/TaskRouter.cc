// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A router task.

#include <sstream>

#include "TaskRouter.hh"



// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

TaskRouter::TaskRouter( sc_core::sc_module_name modulename,
                		const task_id_t id,
                		Scnsl::Core::Node_t * n,
                		const size_t proxies )
    :
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies )
{
	SC_THREAD( writingProcess );
}

TaskRouter::~TaskRouter()  {}

void TaskRouter::writingProcess()
{

	/*for ( ;; )
	{
		byte_t i = byte_t( rand() % 25 + 65 );
		const std::string tp = "0";
		const size_t s = 1;
		label_t label = label_t( rand() % 2 );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << i;
		std::cout << ", packet size: " << s << " bytes, label: " << label << std::endl;

		TlmTask_if_t::send( tp, &i, s, label );
	}*/
}



void TaskRouter::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ){

	t=t;
	byte_t * result;
	bool c;
	Scnsl::Core::data_extension_t * ext;

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND))
	{
		result = static_cast< byte_t *>( malloc ( p.get_data_length() ));
        memcpy(result, p.get_data_ptr(), p.get_data_length());

		p.get_extension( ext );
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data received: " << result;
		std::cout << ", packet size: " << p.get_data_length() << " bytes, label: " << ext->label << std::endl;

		const std::string tp = "2";
		TlmTask_if_t::send( tp, result, p.get_data_length(), ext->label );

	}

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND))
	{
		c = *p.get_data_ptr() != 0;
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", carrier: " << c << std::endl;
	}

}
