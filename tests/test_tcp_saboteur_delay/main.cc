#include "Tcp_Receiver.hh"
#include "Tcp_Sender.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    // Singleton.
    Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

    // Nodes creation:
    Scnsl::Core::Node_t * n0 = scnsl->createNode();
    Scnsl::Core::Node_t * n1 = scnsl->createNode();

    // args = input file
    std::string filename;
    if (argc < 2)
        filename = "NONE";
    else
        filename = argv[1];

    sc_core::sc_time DELAY(390, sc_core::SC_US);
    unsigned int bitrate = 94200000;

    CoreChannelSetup_t ccs;
    ccs.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
    ccs.capacity = 100000000;
    ccs.capacity2 = 100000000;
    ccs.delay = DELAY;
    ccs.extensionId = "core";
    ccs.name = "channel_fullduplex";
    Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(ccs);

    const bool IS_SENDER = true;
    const Scnsl::Core::task_id_t id0 = 0;
    const Scnsl::Core::task_id_t id1 = 1;
    const Scnsl::Core::size_t PROXIES = 1;

    Tcp_Sender sender("Sender", id0, n0, 1, filename);
    Tcp_Receiver p1("TaskClient", id1, n1, 1, 
                    "test_tcp_saboteur_delay_out.txt");

    // Creating the protocol Tcp:

    auto tcp0 = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp0");
    tcp0->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
    tcp0->setSegmentSize(
        MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);

    auto tcp1 = new Scnsl::Protocols::Network_Lv4::Lv4Communicator_t("Tcp1");
    tcp1->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
    tcp1->setSegmentSize(
        MAX_ETH_SEGMENT - TCP_BASIC_HEADER_LENGTH - IP_HEADER_MIN_SIZE);

    std::queue<Lv4Saboteur_channel_down_infos> timeouts;
    // saboteur with 0.5% error
    auto sab1 = new Scnsl::Protocols::Network_Lv4::Lv4ByteSaboteur_t("Sab_1", true, false,
                                                                0.005, false, timeouts);
    tcp1->stackDown(sab1);
    sab1->stackUp(tcp1);
    Communicator_if_t * stack1 = new Scnsl::Utils::CommunicatorStack_t(tcp1, sab1);

    // Adding tracing features:
    CoreTracingSetup_t cts;
    cts.extensionId = "core";
    // - Setting the formatter:
    cts.formatterExtensionId = "core";
    cts.formatterName = "basic";
    // Setting the filter:
    cts.filterExtensionId = "core";
    cts.filterName = "basic";
    // Core formatter specific option:
    // printing also the type of trace:
    cts.print_trace_type = true;
    cts.print_trace_timestamp = true;

    // - Setting to trace only user-like infos:
    cts.info = 5;
    cts.debug = 5;
    cts.log = 5;
    cts.error = 5;
    cts.warning = 5;
    cts.fatal = 5;
    // - Creating:
    Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer(cts);
    // - Setting the output stream:
    tracer1->addOutput(&std::cout);
    // - Adding to trace:
    tracer1->trace(&sender);
    tracer1->trace(&p1);
    tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp0));
    tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp1));
    tracer1->trace(dynamic_cast<Traceable_base_t *>(sab1));
    // - Setting to trace backend-like infos:
    cts.info = 5;
    cts.debug = 5;
    cts.log = 5;
    cts.error = 5;
    cts.warning = 5;
    cts.fatal = 5;

    // Binding:
    BindSetup_base_t bsb0;
    bsb0.extensionId = "core";
    bsb0.destinationNode = n1;
    bsb0.node_binding.x = 0;
    bsb0.node_binding.y = 0;
    bsb0.node_binding.z = 0;
    bsb0.node_binding.bitrate = bitrate;
    bsb0.node_binding.transmission_power = 100;
    bsb0.node_binding.receiving_threshold = 1;

    bsb0.socket_binding.socket_active = true;
    bsb0.socket_binding.source_ip = SocketMap::getIP("192.168.0.5");
    bsb0.socket_binding.source_port = 2020;
    bsb0.socket_binding.dest_ip = SocketMap::getIP("192.168.0.6");
    bsb0.socket_binding.dest_port = 5050;

    scnsl->bind(n0, ch, bsb0);
    scnsl->bind(&sender, &p1, ch, bsb0, tcp0);

    BindSetup_base_t bsb1;
    bsb1.extensionId = "core";
    bsb1.destinationNode = n0;
    bsb1.node_binding.x = 1;
    bsb1.node_binding.y = 1;
    bsb1.node_binding.z = 1;
    bsb1.node_binding.bitrate = bitrate;
    bsb1.node_binding.transmission_power = 1000;
    bsb1.node_binding.receiving_threshold = 1;

    bsb1.socket_binding.socket_active = true;
    bsb1.socket_binding.source_ip = SocketMap::getIP("192.168.0.6");
    ;
    bsb1.socket_binding.source_port = 5050;
    bsb1.socket_binding.dest_ip = SocketMap::getIP("192.168.0.5");
    bsb1.socket_binding.dest_port = 2020;

    scnsl->bind(n1, ch, bsb1);
    scnsl->bind(&p1, &sender, ch, bsb1, stack1);

    sc_core::sc_start(sc_core::sc_time(4000, sc_core::SC_SEC));
    sc_core::sc_stop();

    return 0;
}
