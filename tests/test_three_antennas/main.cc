// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <string>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "ReceiveTask_t.hh"
#include "SendTask_t.hh"

using namespace Scnsl::Setup;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::antennaModels;


int sc_main( int argc, char * argv[] )
{
    if(argc != 6)
    {
        std::cerr << "Required 5 parameters: <angle granularity> <distance> <height> <antenna model> <file>." << std::endl;
        return 1;
    }

    const double granularity = atof(argv[1]);
    const double distance = atof(argv[2]);
    const double height = atof(argv[3]);
    const std::string antennaModel = argv[4];
    const std::string file = argv[5];

    const double threshold = 1e-12;
    try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = sim->createNode();
        Scnsl::Core::Node_t * n_frontal = sim->createNode();
        Scnsl::Core::Node_t * n_left = sim->createNode();
        Scnsl::Core::Node_t * n_right = sim->createNode();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;
        ccs.name = "SharedChannel";
        ccs.alpha = 2;
        ccs.nodes_number = 4;

        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );
        Scnsl::Utils::DirectionalEnvironment_t::createInstance(ccs.alpha);
        const Scnsl::Core::size_t PROXIES = 1;

        SendTask_t t0( "Task0", 0, n0, PROXIES, granularity,
                       distance, height, file + "_sender.txt");
        ReceiveTask_t t_frontal( "TaskFrontal", 1, n_frontal, PROXIES, file + "_frontal.txt");
        ReceiveTask_t t_left( "TaskLeft", 2, n_left, PROXIES, file + "_left.txt");
        ReceiveTask_t t_right( "TaskRight", 3, n_right, PROXIES, file + "_right.txt");

        // Sender Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = nullptr;
        bsb0.node_binding.x = distance;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = height;
        bsb0.node_binding.bitrate = 1000;
        bsb0.node_binding.transmission_power = 0.010;
        //THRESHOLD
        bsb0.node_binding.receiving_threshold =  threshold;

        sim->bind(n0, ch, bsb0);
        sim->bind(&t0, nullptr, ch, bsb0, nullptr);

        //Receiver Binding:
        BindSetup_base_t bsb_frontal;
        bsb_frontal.extensionId = "core";
        bsb_frontal.destinationNode = n0;
        bsb_frontal.node_binding.x = 0.05;
        bsb_frontal.node_binding.y = 0;
        bsb_frontal.node_binding.z = 0;
        bsb_frontal.node_binding.bitrate = 1000;
        bsb_frontal.node_binding.transmission_power = 0.010;
        bsb_frontal.node_binding.receiving_threshold = threshold;
        using Coordinate_t = Scnsl::Core::Coordinate_t;
        if (antennaModel == "PATCH")
        {
            PatchPattern_t *adp_front= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 70, 1.0);
            bsb_frontal.node_binding.lobes.push_back(adp_front);
        }
        else if (antennaModel == "QUARTERWAVE_DIPOLE")
        {
            QuarterWaveDipole_t *adp_front= new QuarterWaveDipole_t(Coordinate_t(false, 0, 1, 0), true, 0);
            bsb_frontal.node_binding.lobes.push_back(adp_front);
        }
        else if (antennaModel == "HALFWAVE_DIPOLE")
        {
            HalfWaveDipole_t *adp_front= new HalfWaveDipole_t(Coordinate_t(false, 0, 1, 0), true, 0);
            bsb_frontal.node_binding.lobes.push_back(adp_front);
        }
        else if (antennaModel == "FULLWAVE_DIPOLE")
        {
            FullWaveDipole_t *adp_front= new FullWaveDipole_t(Coordinate_t(false, 0, 1, 0), true);
            bsb_frontal.node_binding.lobes.push_back(adp_front);
        }
        else if (antennaModel == "CONE")
        {
            ConePattern_t *adp_front= new ConePattern_t(Coordinate_t(false, 1, 0, 0), true, 1, 80);
            bsb_frontal.node_binding.lobes.push_back(adp_front);
        }
        // else if (antennaMOdel == "ISOTROPIC") {/*ntd*/}
        sim->bind(n_frontal, ch, bsb_frontal);
        sim->bind(&t_frontal, nullptr, ch, bsb_frontal, nullptr);

        BindSetup_base_t bsb_sx;
        bsb_sx.extensionId = "core";
        bsb_sx.destinationNode = n0;
        bsb_sx.node_binding.x = 0;
        bsb_sx.node_binding.y = 0.06;
        bsb_sx.node_binding.z = 0;
        bsb_sx.node_binding.bitrate = 1000;
        bsb_sx.node_binding.transmission_power = 0.010;
        bsb_sx.node_binding.receiving_threshold = threshold;
        using Coordinate_t = Scnsl::Core::Coordinate_t;
        if (antennaModel == "PATCH")
        {
            PatchPattern_t *adp_sx= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 70, 1.0);
            bsb_sx.node_binding.lobes.push_back(adp_sx);
        }
        else if (antennaModel == "QUARTERWAVE_DIPOLE")
        {
            QuarterWaveDipole_t *adp_sx= new QuarterWaveDipole_t(Coordinate_t(false, -1, 0, 0), true, 0);
            bsb_sx.node_binding.lobes.push_back(adp_sx);
        }
        else if (antennaModel == "HALFWAVE_DIPOLE")
        {
            HalfWaveDipole_t *adp_sx= new HalfWaveDipole_t(Coordinate_t(false, -1, 0, 0), true, 0);
            bsb_sx.node_binding.lobes.push_back(adp_sx);
        }
        else if (antennaModel == "FULLWAVE_DIPOLE")
        {
            FullWaveDipole_t *adp_sx= new FullWaveDipole_t(Coordinate_t(false, -1, 0, 0), true, 0);
            bsb_sx.node_binding.lobes.push_back(adp_sx);
        }
        else if (antennaModel == "CONE")
        {
            ConePattern_t *adp_sx= new ConePattern_t(Coordinate_t(false, 0, 1, 0), true, 1, 80);
            bsb_sx.node_binding.lobes.push_back(adp_sx);
        }
        // else if (antennaMOdel == "ISOTROPIC") {/*ntd*/}
        sim->bind(n_left, ch, bsb_sx);
        sim->bind(&t_left, nullptr, ch, bsb_sx, nullptr);

        BindSetup_base_t bsb_dx;
        bsb_dx.extensionId = "core";
        bsb_dx.destinationNode = n0;
        bsb_dx.node_binding.x = 0;
        bsb_dx.node_binding.y = -0.06;
        bsb_dx.node_binding.z = 0;
        bsb_dx.node_binding.bitrate = 1000;
        bsb_dx.node_binding.transmission_power = 0.010;
        bsb_dx.node_binding.receiving_threshold = threshold;
        using Coordinate_t = Scnsl::Core::Coordinate_t;
        if (antennaModel == "PATCH")
        {
            PatchPattern_t *adp_dx= new PatchPattern_t(Coordinate_t(false, 1, 0, 0), true, 70, 1.0);
            bsb_dx.node_binding.lobes.push_back(adp_dx);
        }
        else if (antennaModel == "QUARTERWAVE_DIPOLE")
        {
            QuarterWaveDipole_t *adp_dx= new QuarterWaveDipole_t(Coordinate_t(false, 1, 0, 0), true, 0);
            bsb_dx.node_binding.lobes.push_back(adp_dx);
        }
        else if (antennaModel == "HALFWAVE_DIPOLE")
        {
            HalfWaveDipole_t *adp_dx= new HalfWaveDipole_t(Coordinate_t(false, 1, 0, 0), true, 0);
            bsb_dx.node_binding.lobes.push_back(adp_dx);
        }
        else if (antennaModel == "FULLWAVE_DIPOLE")
        {
            FullWaveDipole_t *adp_dx= new FullWaveDipole_t(Coordinate_t(false, 1, 0, 0), true, 0);
            bsb_dx.node_binding.lobes.push_back(adp_dx);
        }
        else if (antennaModel == "CONE")
        {
            ConePattern_t *adp_dx= new ConePattern_t(Coordinate_t(false, 0, -1, 0), true, 1, 80);
            bsb_dx.node_binding.lobes.push_back(adp_dx);
        }
        // else if (antennaMOdel == "ISOTROPIC") {/*ntd*/}
        sim->bind(n_right, ch, bsb_dx);
        sim->bind(&t_right, nullptr, ch, bsb_dx, nullptr);

        sc_core::sc_start();
    }
    catch ( std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
