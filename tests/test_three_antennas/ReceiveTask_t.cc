// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.



/// @file
/// A simple TLM task.


#include <sstream>
#include "ReceiveTask_t.hh"

#define CSV

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

ReceiveTask_t::ReceiveTask_t( const sc_core::sc_module_name modulename,
                                          const task_id_t id,
                                          Scnsl::Core::Node_t * n,
                                          const size_t proxies,
                              const std::string & file):
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies ),
    _file(file.c_str())
{
    //Nothing to do.
}


ReceiveTask_t::~ReceiveTask_t()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Inherited interface methods.
// ////////////////////////////////////////////////////////////////


void ReceiveTask_t::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & /*t*/ )
{
    if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::PACKET_COMMAND) )
    {
        int * result = reinterpret_cast< int * >( p.get_data_ptr() );
        Scnsl::Core::data_extension_t * ext = nullptr;
        p.get_extension( ext );
#ifndef CSV
        _file << "RECEIVER "
                  << sc_core::sc_time_stamp()
                  << " DATA: " << *result
                  << " SENSOR: " << this->name()
                  << " RECEIVED POWER: " << ext->power
                  << std::endl;
#else
        _file << sc_core::sc_time_stamp()
                  << ";" << *result
                  << ";" << ext->power
                  << std::endl;
#endif
    }
    else if( p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND) )
    {
        // ntd
    }
    else
    {
        // ERROR.
        SCNSL_TRACE_ERROR( 1, "Invalid PACKET_COMMAND." );
    }
}
