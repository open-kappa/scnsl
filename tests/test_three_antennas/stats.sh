#!/bin/bash 

#Angle increment step.
step=10

PATTERNS="QUARTERWAVE_DIPOLE HALF_QUARTERWAVE_DIPOLE HALFWAVE_DIPOLE HALF_HALFWAVE_DIPOLE FULLWAVE_DIPOLE HALF_FULLWAVE_DIPOLE QUARTERWAVE_WITH_OBSTACLE HALFWAVE_WITH_OBSTACLE FULLWAVE_WITH_OBSTACLE ISOTROPIC PATCH HALF_PATCH"

for pattern in $PATTERNS; do
	folder="simulation_results"/$pattern"_results"
	mkdir -p $folder

	for distance in 0.5 1.5 5 10 15 20 50 100
	 do
	 for height in -1.5 0 1.5
		do
		  ./test_three_antennas $step $distance $height $pattern $folder/$pattern\_d$distance\_h$height
		  ./test_three_antennas_parser $folder/$pattern\_d$distance\_h$height\_sender.txt \
		  $folder/$pattern\_d$distance\_h$height\_frontal.txt \
		  $folder/$pattern\_d$distance\_h$height\_left.txt \
		  $folder/$pattern\_d$distance\_h$height\_right.txt \
		  $folder/$pattern\_d$distance\_h$height\_total.csv
		   echo "DISTANCE " $distance " HEIGHT" $height " PATTERN "$pattern
		done
	 done
done
echo "End of computation."
