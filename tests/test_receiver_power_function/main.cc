// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <exception>
#include <iostream>
#include <fstream>
#include <map>
#include <utility>

#include <systemc>
#include <tlm.h>
#include <scnsl.hh>

#include "MyTask_t.hh"

using namespace std;
using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace Scnsl::Tlm;

using Scnsl::Tracing::Traceable_base_t;

// Attention: use double type indeces in C++ containers only if
// the precision of the indices is fixed!
typedef std::pair<double, double> t_inner_power;
typedef std::map<t_inner_power, double> t_outer_power;
typedef std::map<unsigned int, t_inner_power> t_inner_node;
typedef std::map<unsigned int, t_inner_node> t_outer_node;

namespace /*anon*/ {

t_outer_node nodes;
t_outer_power powers;

} // anon

static double receiverPowerFunction(const node_properties_t & sp,
  const node_properties_t & rp)
{
    const position_t dx = sp.x - rp.x;
    const position_t dy = sp.y - rp.y;
    const position_t dz = sp.z - rp.z;
    const position_t d2 = dx*dx + dy*dy + dz*dz;
    const distance_t distance = sqrt( d2 );
    const power_t receiverPower = (sp.transmission_power / distance)
           + powers[nodes[sp.sourceNode->getId()][sp.id]];

    return receiverPower;
}

int sc_main( int argc, char* argv[])
{
    unsigned int node_id, bind_id;
    double temp_a, temp_b, temp_c;

    if (argc != 4)
    {
        cout << "Use: ./test_bit_error_rate_function [NODE_TABLE] "
          << "[POWER_TABLE] [SIM_LENGTH(ns)]" << endl;
        return 1;
    }

    // Reading tables
    ifstream ifstr_nodes(argv[1]);
    while (ifstr_nodes >> node_id >> bind_id >> temp_a >> temp_b)
        nodes[node_id][bind_id] = make_pair(temp_a, temp_b);
    ifstr_nodes.close();

    ifstream ifstr_powers(argv[2]);
    while (ifstr_powers >> temp_a >> temp_b >> temp_c)
        powers[make_pair(temp_a,temp_b)] = temp_c;
    ifstr_powers.close();

    try {
        // Singleton
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation
        Scnsl::Core::Node_t * n0 = scnsl->createNode(0);
        Scnsl::Core::Node_t * n1 = scnsl->createNode(1);
        Scnsl::Core::Node_t * n2 = scnsl->createNode(2);
        Scnsl::Core::Node_t * n3 = scnsl->createNode(3);
        Scnsl::Core::Node_t * n4 = scnsl->createNode(4);
        Scnsl::Core::Node_t * n5 = scnsl->createNode(5);
        Scnsl::Core::Node_t * n6 = scnsl->createNode(6);

        // Channel creation
        CoreChannelSetup_t ccs01;
        ccs01.extensionId = "core";
        ccs01.channel_type = CoreChannelSetup_t::SHARED;
        ccs01.name = "SharedChannel01";
        ccs01.alpha = 0.1;
        ccs01.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs01.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch01 = scnsl->createChannel( ccs01 );

        CoreChannelSetup_t ccs23;
        ccs23.extensionId = "core";
        ccs23.channel_type = CoreChannelSetup_t::SHARED;
        ccs23.name = "SharedChannel23";
        ccs23.alpha = 0.1;
        ccs23.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs23.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch23 = scnsl->createChannel( ccs23 );

        CoreChannelSetup_t ccs45;
        ccs45.extensionId = "core";
        ccs45.channel_type = CoreChannelSetup_t::SHARED;
        ccs45.name = "SharedChannel45";
        ccs45.alpha = 0.1;
        ccs45.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs45.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch45 = scnsl->createChannel( ccs45 );

        CoreChannelSetup_t ccs46;
        ccs46.extensionId = "core";
        ccs46.channel_type = CoreChannelSetup_t::SHARED;
        ccs46.name = "SharedChannel46";
        ccs46.alpha = 0.1;
        ccs46.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs46.nodes_number = 2;
        Scnsl::Core::Channel_if_t * ch46 = scnsl->createChannel( ccs46 );

        // User environment
        Scnsl::Utils::UserEnvironment_t::createInstance(
           receiverPowerFunction, ccs01.alpha );

        // Task creation
        const bool IS_SENDER = true;
        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::task_id_t id2 = 2;
        const Scnsl::Core::task_id_t id3 = 3;
        const Scnsl::Core::task_id_t id4 = 4;
        const Scnsl::Core::task_id_t id5 = 5;
        const Scnsl::Core::task_id_t id6 = 6;

        MyTask_t t0( "Task0", IS_SENDER, id0, n0, 1 );
        MyTask_t t1( "Task1", ! IS_SENDER, id1, n1, 1 );
        MyTask_t t2( "Task2", IS_SENDER, id2, n2, 1 );
        MyTask_t t3( "Task3", ! IS_SENDER, id3, n3, 1 );
        MyTask_t t4( "Task4", IS_SENDER, id4, n4, 2 );
        MyTask_t t5( "Task5", ! IS_SENDER, id5, n5, 1 );
        MyTask_t t6( "Task6", ! IS_SENDER, id6, n6, 1 );

        // Creating the protocol 802.15.4
        CoreCommunicatorSetup_t ccoms;
        ccoms.extensionId = "core";
        ccoms.ack_required = true;
        ccoms.short_addresses = true;
        ccoms.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        // Instance for n0
        ccoms.name = "Mac0";
        ccoms.node = n0;
        Scnsl::Core::Communicator_if_t * mac0 = scnsl->createCommunicator( ccoms );
        // Instance for n1
        ccoms.name = "Mac1";
        ccoms.node = n1;
        Scnsl::Core::Communicator_if_t * mac1 = scnsl->createCommunicator( ccoms );
        // Instance for n2
        ccoms.name = "Mac2";
        ccoms.node = n2;
        Scnsl::Core::Communicator_if_t * mac2 = scnsl->createCommunicator( ccoms );
        // Instance for n3
        ccoms.name = "Mac3";
        ccoms.node = n3;
        Scnsl::Core::Communicator_if_t * mac3 = scnsl->createCommunicator( ccoms );
        // Instance for n4
        ccoms.name = "Mac4";
        ccoms.node = n4;
        Scnsl::Core::Communicator_if_t * mac4 = scnsl->createCommunicator( ccoms );
        // Instance for n5
        ccoms.name = "Mac5";
        ccoms.node = n5;
        Scnsl::Core::Communicator_if_t * mac5 = scnsl->createCommunicator( ccoms );
        // Instance for n6
        ccoms.name = "Mac6";
        ccoms.node = n6;
        Scnsl::Core::Communicator_if_t * mac6 = scnsl->createCommunicator( ccoms );

        // Adding tracing features
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        cts.print_trace_type = true;
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // Creating
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer( cts );
        tracer1->addOutput( & std::cout );
        /*tracer1->trace( & t0 );
        tracer1->trace( & t1 );
        tracer1->trace( & t2 );
        tracer1->trace( & t3 );
        tracer1->trace( & t4 );
        tracer1->trace( & t5 );
        tracer1->trace( & t6 ); */
       /*tracer1->trace( dynamic_cast<Traceable_base_t*>(mac0) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac1) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac2) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac3) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac4) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac5) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac6) );*/
       /* tracer1->trace( ch01 );
        tracer1->trace( ch23 );
        tracer1->trace( ch45 );
        tracer1->trace( ch46 );
       */
        // Binding
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n1;
        bsb0.node_binding.x = 1;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb0.node_binding.transmission_power = 100;
        bsb0.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb2;
        bsb2.extensionId = "core";
        bsb2.destinationNode = n3;
        bsb2.node_binding.x = 3;
        bsb2.node_binding.y = 0;
        bsb2.node_binding.z = 0;
        bsb2.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb2.node_binding.transmission_power = 100;
        bsb2.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb45;
        bsb45.extensionId = "core";
        bsb45.destinationNode = n5;
        bsb45.node_binding.x = 5;
        bsb45.node_binding.y = 0;
        bsb45.node_binding.z = 0;
        bsb45.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb45.node_binding.transmission_power = 100;
        bsb45.node_binding.receiving_threshold = 10;

        BindSetup_base_t bsb46;
        bsb46.extensionId = "core";
        bsb46.destinationNode = n6;
        bsb46.node_binding.x = 5;
        bsb46.node_binding.y = 0;
        bsb46.node_binding.z = 0;
        bsb46.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb46.node_binding.transmission_power = 100;
        bsb46.node_binding.receiving_threshold = 10;

        scnsl->bind(n0, ch01, bsb0);
        scnsl->bind(&t0, &t1, ch01, bsb0, mac0);

        scnsl->bind(n2, ch23, bsb2);
        scnsl->bind(&t2, &t3, ch23, bsb2, mac2);

        scnsl->bind(n4, ch45, bsb45);
        scnsl->bind(&t4, &t5, ch45, bsb45, mac4);

        scnsl->bind(n4, ch46, bsb46);
        scnsl->bind(&t4, &t6, ch46, bsb46, mac4);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = nullptr; // don't care: it receives only...
        bsb1.node_binding.x = 1;
        bsb1.node_binding.y = 5;
        bsb1.node_binding.z = 0;
        bsb1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb1.node_binding.transmission_power = 1000;
        bsb1.node_binding.receiving_threshold = 15;

        BindSetup_base_t bsb3;
        bsb3.extensionId = "core";
        bsb3.destinationNode = nullptr; // don't care: it receives only...
        bsb3.node_binding.x = 3;
        bsb3.node_binding.y = 5;
        bsb3.node_binding.z = 0;
        bsb3.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb3.node_binding.transmission_power = 1000;
        bsb3.node_binding.receiving_threshold = 15;

        BindSetup_base_t bsb5;
        bsb5.extensionId = "core";
        bsb5.destinationNode = nullptr; // don't care: it receives only...
        bsb5.node_binding.x = 5;
        bsb5.node_binding.y = 5;
        bsb5.node_binding.z = 0;
        bsb5.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb5.node_binding.transmission_power = 1000;
        bsb5.node_binding.receiving_threshold = 15;

        BindSetup_base_t bsb6;
        bsb6.extensionId = "core";
        bsb6.destinationNode = nullptr; // don't care: it receives only...
        bsb6.node_binding.x = 5;
        bsb6.node_binding.y = 10;
        bsb6.node_binding.z = 0;
        bsb6.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsb6.node_binding.transmission_power = 1000;
        bsb6.node_binding.receiving_threshold = 15;

        scnsl->bind(n1, ch01, bsb1);
        scnsl->bind(&t1, &t0, ch01, bsb1, mac1);

        scnsl->bind(n3, ch23, bsb3);
        scnsl->bind(&t3, &t2, ch23, bsb3, mac3);

        scnsl->bind(n5, ch45, bsb5);
        scnsl->bind(&t5, &t4, ch45, bsb5, mac5);

        scnsl->bind(n6, ch46, bsb6);
        scnsl->bind(&t6, &t4, ch46, bsb6, mac6);

        sc_core::sc_start( sc_core::sc_time( atoi(argv[3]), sc_core::SC_MS ) );
        sc_core::sc_stop();

    } catch ( std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
