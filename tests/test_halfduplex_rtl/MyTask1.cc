// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A RTL task.

#include <sstream>

#include "MyTask1.hh"

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTask1::MyTask1( sc_core::sc_module_name modulename,
                  const task_id_t id,
                  Scnsl::Core::Node_t * n,
                  const size_t proxies,
                  const int scenario )
    :
    // Parents:
    Scnsl::Rtl::RtlTask_if_t<8>( modulename, id, n, proxies )
{

	switch ( scenario )
	{
		case 1: 	SC_THREAD( writingProcess1 );
            sensitive << packetSendCompleted[ 0 ];
            break;
		case 2: 	SC_THREAD( writingProcess2 );
            sensitive << packetSendCompleted[ 0 ];
            break;
		case 3: 	SC_THREAD( writingProcess3 );
            sensitive << packetSendCompleted[ 0 ];
            break;
		case 4: 	SC_THREAD( writingProcess4 );
            sensitive << packetSendCompleted[ 0 ];
            break;
		default:	SC_THREAD( writingProcess1 );
            sensitive << packetSendCompleted[ 0 ];
	}

	SC_THREAD( readingProcess );
	sensitive << newInputPacket[0];

	SC_THREAD( carrierProcess );
	sensitive << carrier[ 0 ];
}

MyTask1::~MyTask1()
{
    // ntd
}


void MyTask1::writingProcess1()
{

	unsigned int i = 0;
	for ( ;; )
	{

		sc_dt::sc_uint<8> data = rand() % 25 + 65;
		byte_t b = byte_t( data );

		outputPacketSize[0].write(8);
		outputPacket[0].write( data );
		newOutputPacket[0].write( ++ i );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << b;
		std::cout << ", packet size: " << data.length()/8 << " bytes" << std::endl;

		wait();
	}
}

void MyTask1::writingProcess2()
{

	unsigned int i = 0;
	for ( ;; )
	{

		sc_dt::sc_uint<8> data = rand() % 25 + 65;
		byte_t b = byte_t( data );

		outputPacketSize[0].write(8);
		outputPacket[0].write( data );
		newOutputPacket[0].write( ++ i );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << b;
		std::cout << ", packet size: " << data.length()/8 << " bytes" << std::endl;

		wait();
	}
}

void MyTask1::writingProcess3()
{

	unsigned int i = 0;
	for ( ;; )
	{

		sc_dt::sc_uint<8> data = rand() % 25 + 65;
		byte_t b = byte_t( data );

		outputPacketSize[0].write(8);
		outputPacket[0].write( data );
		newOutputPacket[0].write( ++ i );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << b;
		std::cout << ", packet size: " << data.length()/8 << " bytes" << std::endl;

		wait();
		wait( 28, sc_core::SC_MS );
	}
}

void MyTask1::writingProcess4()
{

	unsigned int i = 0;
	for ( ;; )
	{

		sc_dt::sc_uint<8> data = rand() % 25 + 65;
		byte_t b = byte_t( data );

		outputPacketSize[0].write(8);
		outputPacket[0].write( data );
		newOutputPacket[0].write( ++ i );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data sended: " << b;
		std::cout << ", packet size: " << data.length()/8 << " bytes" << std::endl;

		wait();
		wait( 20, sc_core::SC_MS );
		wait( 8, sc_core::SC_MS );
	}
}


void MyTask1::readingProcess()
{
	for (;;)
 	{
		wait();

		sc_dt::sc_uint<8> data = inputPacket[0].read();
		byte_t b = byte_t( data );

		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", data received: " << b;
		std::cout << ", packet size: " << inputPacketSize[0].read()/8 << " bytes" << std::endl;
	}
}


void MyTask1::carrierProcess()
{
	for ( ;; )
	{
		wait();
		std::cout << "Time: " << sc_core::sc_time_stamp() << ", Name: " << name() << ", carrier: " << carrier[0].read() << std::endl;
	}
}
