// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_MY_TASK1_HH
#define SCNSL_MY_TASK1_HH



/// @file
/// Source node.


#include <systemc>


// Including the library:
#include <scnsl.hh>



class MyTask1 :
    public Scnsl::Rtl::RtlTask_if_t<8>
{

public:

    SC_HAS_PROCESS( MyTask1 );

    /// @brief Constructor.
    ///
    /// @param name This module name.
    /// @param id this module unique ID.
    /// @param n The node on which this task is placed.
    /// @param proxies The number of connected task proxies.
	/// @param scenario The number that represents the type of communication between tasks, as described in README.txt.
    /// @throw std::invalid_argument If proxies is zero.
    ///
	MyTask1( sc_core::sc_module_name name,
             const task_id_t id,
             Scnsl::Core::Node_t * n,
             const size_t proxies,
			 const int scenario );

    virtual ~MyTask1();
protected:

    /// @name Processes.
    //@{

    void writingProcess1();
    void writingProcess2();
    void writingProcess3();
    void writingProcess4();

    void readingProcess();

    void carrierProcess();

    //@}

private:

    MyTask1( MyTask1 & );

    MyTask1 & operator = ( MyTask1 & );

};

#endif
