#include "Udp_Simple_Receiver.hh"

#include <fstream>
#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Udp_Receiver::Udp_Receiver(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    const size_t proxies):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{}

Udp_Receiver::~Udp_Receiver()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Receiver::main()
{
    initTime();
    int sockfd;
    char buffer[1024];
    char * hello = "Hello from server";
    struct sockaddr_in servaddr, cliaddr;

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;  // IPv4
    inet_pton(AF_INET, "192.168.0.1", &servaddr.sin_addr);
    servaddr.sin_port = htons(5050);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    uint len, n;

    len = sizeof(cliaddr);  // len is value/resuslt

    for (int i = 0; i < 1 << 30; i++)
    {}

    n = recvfrom(
        sockfd,
        (char *)buffer,
        1024,
        0,
        (struct sockaddr *)&cliaddr,
        &len);
    buffer[n] = '\0';
    printf("Client : %s\n", buffer);
    n = recvfrom(
        sockfd,
        (char *)buffer,
        1024,
        0,
        (struct sockaddr *)&cliaddr,
        &len);
    buffer[n] = '\0';
    printf("Client : %s\n", buffer);

    // sendto(sockfd, (const char *)hello, strlen(hello),
    // 	MSG_CONFIRM, (const struct sockaddr *) &cliaddr,
    // 		len);
}
