#include "Udp_Simple_Sender.hh"

#include <chrono>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

Udp_Sender::Udp_Sender(
    const sc_core::sc_module_name modulename,
    const task_id_t id,
    Scnsl::Core::Node_t * n,
    size_t proxies):
    // Parents:
    NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{}

Udp_Sender::~Udp_Sender()
{
    // Nothing to do.
}
// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void Udp_Sender::main()
{
    initTime();
    int sockfd1, sockfd2;
    char buffer[1024];
    char * str1 = "PING";
    char * str2 = "PONG";
    struct sockaddr_in servaddr1, servaddr2;

    // Creating socket file descriptor
    if ((sockfd1 = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket  1 creation failed");
        exit(EXIT_FAILURE);
    }
    if ((sockfd2 = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket  2 creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr1, 0, sizeof(servaddr1));
    memset(&servaddr2, 0, sizeof(servaddr1));

    // Filling server information
    inet_pton(AF_INET, "192.168.0.1", &servaddr1.sin_addr);
    servaddr1.sin_port = htons(5050);
    inet_pton(AF_INET, "192.168.0.1", &servaddr2.sin_addr);
    servaddr2.sin_port = htons(5050);

    uint n, len;

    sendto(
        sockfd1,
        (const char *)str1,
        strlen(str1),
        0,
        (struct sockaddr *)&servaddr1,
        sizeof(servaddr1));
    sendto(
        sockfd2,
        (const char *)str2,
        strlen(str2),
        0,
        (struct sockaddr *)&servaddr2,
        sizeof(servaddr2));

    // n = recvfrom(sockfd1, (char *)buffer, MAXLINE,
    // 			MSG_WAITALL, (struct sockaddr *) &servaddr1,
    // 			&len);
    // buffer[n] = '\0';
    // printf("Server 1: %s\n", buffer);

    // n = recvfrom(sockfd2, (char *)buffer, MAXLINE,
    // MSG_WAITALL, (struct sockaddr *) &servaddr2,
    // &len);
    close(sockfd1);
    close(sockfd2);
}
