// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask.hh"
#include "MyTask2.hh"
#include "MyTask4.hh"
#include "MyTask3.hh"
#include "MyTaskRouter.hh"

#include <map>

using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Utils;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;
using Scnsl::Tracing::Traceable_base_t;


int sc_main( int argc, char * argv[] )
{
	try {


        Scnsl_t * sim = Scnsl_t::get_instance();


        // Nodes creation:
        Node_t * n1 = sim->createNode();
	Node_t * n2 = sim->createNode();
	Node_t * nodoRouter = sim->createNode();
	Node_t * nodoRouter2 = sim->createNode();

	// channels :
        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::SHARED;

        ccs.name = "SharedChannel";
        ccs.alpha = 0.1;
        ccs.delay = sc_core::sc_time( 1.0, sc_core::SC_US );
        ccs.nodes_number = 4;
        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );

    Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha);

	// n1
        CoreCommunicatorSetup_t cc1;
        cc1.extensionId = "core";
        cc1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc1.name = "mac802_15_4_n1";
        cc1.node = n1;

        cc1.ack_required = false;
        cc1.short_addresses = false;

        CoreCommunicatorSetup_t ccr1;
        ccr1.extensionId="core";
        ccr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr1.name = "macRouter_n1";
        ccr1.node=n1;

        // n2
        CoreCommunicatorSetup_t cc2;
        cc2.extensionId = "core";
        cc2.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        cc2.name = "mac802_15_4_n2";
        cc2.node = n2;

        cc2.ack_required = false;
        cc2.short_addresses = false;

        CoreCommunicatorSetup_t ccr2;
        ccr2.extensionId="core";
        ccr2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccr2.name = "macRouter_n2";
        ccr2.node=n2;

        // gateway 1
        CoreCommunicatorSetup_t ccoms1;
        ccoms1.extensionId = "core";
        ccoms1.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        ccoms1.name = "MacRouter1802";
        ccoms1.node = nodoRouter;
        ccoms1.ack_required = false;
        ccoms1.short_addresses = false;

        CoreCommunicatorSetup_t ccomsr1;
        ccomsr1.extensionId="core";
        ccomsr1.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccomsr1.name = "MacRouter1Routing";
        ccomsr1.node = nodoRouter;

        // gateway 2
        CoreCommunicatorSetup_t ccoms2;
        ccoms2.extensionId = "core";
        ccoms2.type = CoreCommunicatorSetup_t::MAC_802_15_4;
        ccoms2.name = "MacRouter2802";
        ccoms2.node = nodoRouter2;
        ccoms2.ack_required = false;
        ccoms2.short_addresses = false;

        CoreCommunicatorSetup_t ccomsr2;
        ccomsr2.extensionId="core";
        ccomsr2.type = CoreCommunicatorSetup_t::ROUTER_COMMUNICATOR;
        ccomsr2.name = "MacRouter2Routing";
        ccomsr2.node=nodoRouter2;


        // tables :
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > tablen1;
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > tablen2;
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > tablegw;
        std::map< Scnsl::Core::Node_t *, std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > > tablegw2;


        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablen1_second (ch,nodoRouter);

        tablen1[n2] = tablen1_second;

        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablen2_second (ch,nodoRouter2);

        tablen2[n1] = tablen2_second;

        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablegw_second (ch,n1);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablegw_second_II (ch,nodoRouter2);

        tablegw[n1] = tablegw_second;
	tablegw[n2] = tablegw_second_II;


        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablegw2_second (ch,nodoRouter);
        std::pair< Scnsl::Core::Channel_if_t * , Scnsl::Core::Node_t * > tablegw2_second_II (ch,n2);

	tablegw2[n1] = tablegw2_second;
	tablegw2[n2] = tablegw2_second_II;

	ccr1.routingTable = tablen1;
        ccr2.routingTable = tablen2;
        ccomsr1.routingTable = tablegw;
        ccomsr2.routingTable = tablegw2;

        // n1
        Communicator_if_t * mac1_802 = sim->createCommunicator( cc1 );
        Communicator_if_t * mac1_router = sim->createCommunicator( ccr1 );
        mac1_802->stackUp(mac1_router);
        mac1_router->stackDown(mac1_802);

        // n2
        Communicator_if_t * mac2_802 = sim->createCommunicator( cc2 );
        Communicator_if_t * mac2_router = sim->createCommunicator( ccr2 );
        mac2_802->stackUp(mac2_router);
        mac2_router->stackDown(mac2_802);

        // gw1
        Communicator_if_t * macgw1802 = sim->createCommunicator( ccoms1 );
        Communicator_if_t * macgw1Routing = sim->createCommunicator( ccomsr1 );
        macgw1802->stackUp(macgw1Routing);
        macgw1Routing->stackDown(macgw1802);

        // gw2
        Communicator_if_t * macgw2802 = sim->createCommunicator( ccoms2 );
        Communicator_if_t * macgw2Routing = sim->createCommunicator( ccomsr2 );
        macgw2802->stackUp(macgw2Routing);
        macgw2Routing->stackDown(macgw2802);


        Communicator_if_t * macStackN1 = new Scnsl::Utils::CommunicatorStack_t(mac1_router, mac1_802);
        Communicator_if_t * macStackN2 = new Scnsl::Utils::CommunicatorStack_t(mac2_router, mac2_802);
        Communicator_if_t * macStackgw1 = new Scnsl::Utils::CommunicatorStack_t(macgw1Routing, macgw1802);
        Communicator_if_t * macStackgw2 = new Scnsl::Utils::CommunicatorStack_t(macgw2Routing, macgw2802);




        MyTask t1 ("T1",1,n1,1);
        MyTask3 t2 ("T2",4,n2,1);
        MyTaskRouter tr("ROUTER",0,nodoRouter,1);
        MyTaskRouter tr2("ROUTER2",2,nodoRouter2,1);



        //
	// TRACING
	//


	  // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;

        cts.print_trace_timestamp = false;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 5;
        cts.log = 5;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer1->addOutput( & std::cout );
        // - Adding to trace:
        tracer1->trace( & t1 );
        tracer1->trace( & tr );
        tracer1->trace( & tr2 );
        tracer1->trace( & t2 );
        tracer1->trace( ch );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac1_802) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(macgw1802) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(macgw2802) );
        tracer1->trace( dynamic_cast<Traceable_base_t*>(mac2_802) );


	//
	//
	//




        BindSetup_base_t bsbt1;
        BindSetup_base_t bsbrouter;
        BindSetup_base_t bsbrouter2;
        BindSetup_base_t bsbt2;
        BindSetup_base_t bsbt3;
        BindSetup_base_t bsbt4;

        bsbt1.extensionId = "core";
        bsbt3.extensionId = "core";
        bsbrouter.extensionId = "core";
        bsbrouter2.extensionId = "core";
        bsbt2.extensionId = "core";
        bsbt4.extensionId = "core";

        // --------------------------------------

        bsbt1.destinationNode = nullptr;
        bsbt2.destinationNode = nullptr;
        bsbt3.destinationNode = nullptr;
        bsbt4.destinationNode = nullptr;
        bsbrouter.destinationNode = nullptr;
        bsbrouter2.destinationNode = nullptr;

        // ---------------------------------------

        bsbt1.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt1.node_binding.transmission_power = 100;
        bsbt1.node_binding.receiving_threshold = 10;
        bsbt1.node_binding.x = 1;
        bsbt1.node_binding.y = 1;
        bsbt1.node_binding.z = 1;

        sim->bind(n1, ch, bsbt1);

        // gateway
        bsbrouter.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbrouter.node_binding.transmission_power = 100;
        bsbrouter.node_binding.receiving_threshold = 10;
        bsbrouter.node_binding.x = 4;
        bsbrouter.node_binding.y = 4;
        bsbrouter.node_binding.z = 4;

        sim->bind(nodoRouter, ch, bsbrouter);

        // gateway 2
        bsbrouter2.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbrouter2.node_binding.transmission_power = 100;
        bsbrouter2.node_binding.receiving_threshold = 10;
        bsbrouter2.node_binding.x = 8;
        bsbrouter2.node_binding.y = 8;
        bsbrouter2.node_binding.z = 8;

        sim->bind(nodoRouter2, ch, bsbrouter2);


        bsbt4.node_binding.bitrate = Scnsl::Protocols::Mac_802_15_4::BITRATE;
        bsbt4.node_binding.transmission_power = 100;
        bsbt4.node_binding.receiving_threshold = 10;
        bsbt4.node_binding.x = 6;
        bsbt4.node_binding.y = 6;
        bsbt4.node_binding.z = 6;

        sim->bind(n2, ch, bsbt4);


        sim->bind(&t1, &t2, ch, bsbt1, macStackN1);
        sim->bind(&tr, nullptr, ch, bsbrouter, macStackgw1);
        sim->bind(&tr2, nullptr, ch, bsbrouter2, macStackgw2);
        sim->bind(&t2, nullptr, ch, bsbt4, macStackN2);


        sc_core::sc_start( sc_core::sc_time( 5000, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}

    catch (exception & e)
    {
	cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}
	return 0;
}
