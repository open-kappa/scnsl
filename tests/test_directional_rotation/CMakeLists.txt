set(TEST_NAME "test_directional_rotation")

mycmake_add_executable(${TEST_NAME}
    EXCLUDE_FROM_ALL
    main.cc
    MyTask_t.cc
    NodeTask_t.cc
    )

add_dependencies(${SCNSL_TESTS} ${TEST_NAME})
mycmake_target_link_libraries(${TEST_NAME}
    PUBLIC
        ${SCNSL_EXE_DEPENDENCIES}
    )

mycmake_install(${TEST_NAME} OPTIONAL)

mycmake_ctest_add_test(
    NAME ${TEST_NAME}
    COMMAND ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}/${TEST_NAME}
    ENVIRONMENT "${MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR}=\$${MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR}${MYCMAKE_OS_ENV_PATH_SEPARATOR}${CMAKE_INSTALL_PREFIX}/${MYCMAKE_OS_SHARED_LIBRARY_INSTALL_DIR}"
    )

# EOF
