#ifndef MYTASK_SENDER_HH
#define MYTASK_SENDER_HH

#include <scnsl.hh>
#include <systemc>

class MyTask_Sender: public Scnsl::Protocols::Network_Lv4::NetworkAPI_Task_if_t
{
public:
    
    MyTask_Sender(const sc_core::sc_module_name modulename, const task_id_t id, 
                    Scnsl::Core::Node_t * n, const size_t proxies);

    virtual ~MyTask_Sender();

private:
    void main() override;
};

#endif
