#include "MyTask_Sender.hh"

#include <sstream>

// ////////////////////////////////////////////////////////////////
// Constructor and destructor.
// ////////////////////////////////////////////////////////////////

using namespace Scnsl::Protocols::Network_Lv4;
using namespace Scnsl::Syscalls;
using Scnsl::Syscalls::send;

MyTask_Sender::MyTask_Sender(const sc_core::sc_module_name modulename, 
                                const task_id_t id, Scnsl::Core::Node_t * n,
                                const size_t proxies):
// Parents:
NetworkAPI_Task_if_t(modulename, id, n, proxies, DEFAULT_WMEM)
{

}

MyTask_Sender::~MyTask_Sender()
{
    // Nothing to do.
}

// ////////////////////////////////////////////////////////////////
// Processes.
// ////////////////////////////////////////////////////////////////

void MyTask_Sender::main()
{
    initTime();
    const size_t s = 10;
    byte_t buffer[10];
    int sockfd_1, sockfd_2, portno, n;
    struct sockaddr pit_addr_1, pit_addr_2;

    sockfd_1 = socket(AF_INET, SOCK_STREAM, 0);
    sockfd_2 = socket(AF_INET, SOCK_STREAM, 0);

    portno = 2020;

    pit_addr_1.sin_port = portno;
    inet_pton(AF_INET, "192.168.0.2", &pit_addr_1.sin_addr);

    pit_addr_2.sin_port = portno;
    inet_pton(AF_INET, "192.168.0.3", &pit_addr_2.sin_addr);

    if (connect(sockfd_1, (struct sockaddr *)&pit_addr_1, sizeof(pit_addr_1))  < 0)
    {
        throw std::runtime_error("ERROR connecting pit 1");
    }
    if (connect(sockfd_2, (struct sockaddr *)&pit_addr_2, sizeof(pit_addr_2)) < 0)
    {
        throw std::runtime_error("ERROR connecting pit 2");
    }

    for (;;)
    {
        for (int j = 0; j < s; j++)
            buffer[j] = byte_t(rand() % 25 + 65);

        std::cout << "Time: " << sc_core::sc_time_stamp()
                  << ", Name: " << name() << ", data sended: ";
        for (int j = 0; j < s; j++)
            std::cout << buffer[j] << " ";
        std::cout << ", packet size: " << s << " bytes" << std::endl;

        n = Scnsl::Syscalls::send(sockfd_1, buffer, s, 0);

        if (n < 0) throw std::runtime_error("ERROR writing to socket");
        wait(std::rand() % 10 + 1, sc_core::SC_US);

        n = Scnsl::Syscalls::send(sockfd_2, buffer, s, 0);
        if (n < 0)
            throw std::runtime_error("ERROR writing to socket");
        wait(std::rand() % 10 + 10, sc_core::SC_MS);
    }

    close(sockfd_1);
    close(sockfd_2);
}
