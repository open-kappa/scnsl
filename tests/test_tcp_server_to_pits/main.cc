
#include "MyTask_Pit.hh"
#include "MyTask_Sender.hh"

#include <exception>
#include <scnsl.hh>
#include <sstream>
#include <systemc>
#include <tlm.h>

using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Protocols::Network_Lv4;
using Scnsl::Tracing::Traceable_base_t;

int sc_main(int argc, char * argv[])
{
    try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * scnsl = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * senderNode = scnsl->createNode();
        Scnsl::Core::Node_t * pit_1_node = scnsl->createNode();
        Scnsl::Core::Node_t * pit_2_node = scnsl->createNode();

        CoreChannelSetup_t csb;
        csb.channel_type = CoreChannelSetup_t::FULL_DUPLEX;
        csb.capacity = 100000000;  // 100 megabit
        csb.capacity2 = 100000000;
        csb.delay = sc_core::sc_time(1, sc_core::SC_MS);
        csb.extensionId = "core";
        csb.name = "channel_fullduplex";
        Scnsl::Core::Channel_if_t * ch = scnsl->createChannel(csb);
        Scnsl::Core::Channel_if_t * ch2 = scnsl->createChannel(csb);

        const bool IS_SENDER = true;
        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::task_id_t id2 = 2;

        MyTask_Sender sender("Sender", id0, senderNode, 2);
        MyTask_Pit p1("Pit_1", id1, pit_1_node, 1, "192.168.0.2");
        MyTask_Pit p2("Pit_2", id2, pit_2_node, 1, "192.168.0.3");

        // Creating the protocol Tcp:

        auto tcpServer = new Lv4Communicator_t("SenderTcp");
        tcpServer->setRcvwnd(500);  // max 500 bytes per send
        tcpServer->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcpServer->setSegmentSize(25);  // 25 bytes per segment

        auto tcp1 = new Lv4Communicator_t("Pit1");
        tcp1->setRcvwnd(500);  // max 500 bytes per send
        tcp1->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcp1->setSegmentSize(25);  // 25 bytes per segment

        auto tcp2 = new Lv4Communicator_t("Pit2");
        tcp2->setRcvwnd(500);  // max 500 bytes per send
        tcp2->setExtraHeaderSize(IP_HEADER_MIN_SIZE, 14);
        tcp2->setSegmentSize(25);  // 25 bytes per segment

        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;
        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = scnsl->createTracer(cts);
        // - Setting the output stream:
        tracer1->addOutput(&std::cout);
        // - Adding to trace:
        tracer1->trace(&p2);
        tracer1->trace(&p1);
        tracer1->trace(&sender);
        tracer1->trace(ch);
        tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp1));
        tracer1->trace(dynamic_cast<Traceable_base_t *>(tcp2));
        tracer1->trace(dynamic_cast<Traceable_base_t *>(tcpServer));

        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        //        // - Creating:
        // Binding:

        // SERVER -> TASK1
        BindSetup_base_t sender_to_pit1;
        sender_to_pit1.extensionId = "core";
        sender_to_pit1.destinationNode = pit_1_node;
        sender_to_pit1.node_binding.x = 0;
        sender_to_pit1.node_binding.y = 0;
        sender_to_pit1.node_binding.z = 0;
        sender_to_pit1.node_binding.bitrate = 100000000;  // 100 megabit (to
                                                          // check ack timer)
        sender_to_pit1.node_binding.transmission_power = 100;
        sender_to_pit1.node_binding.receiving_threshold = 1;

        sender_to_pit1.socket_binding.socket_active = true;
        sender_to_pit1.socket_binding.source_ip = SocketMap::getIP("192.168.0.1");
        sender_to_pit1.socket_binding.source_port = 80;
        sender_to_pit1.socket_binding.dest_ip = SocketMap::getIP("192.168.0.2");
        sender_to_pit1.socket_binding.dest_port = 2020;

        scnsl->bind(senderNode, ch, sender_to_pit1);
        scnsl->bind(&sender, &p1, ch, sender_to_pit1, tcpServer);

        // TASK1 -> SERVER
        BindSetup_base_t pit1_to_sender;
        pit1_to_sender.extensionId = "core";
        pit1_to_sender.destinationNode = senderNode;
        pit1_to_sender.node_binding.x = 1;
        pit1_to_sender.node_binding.y = 0;
        pit1_to_sender.node_binding.z = 0;
        pit1_to_sender.node_binding.bitrate = 1000000;  // 1 megabit (to check
                                                        // ack timer)
        pit1_to_sender.node_binding.transmission_power = 100;
        pit1_to_sender.node_binding.receiving_threshold = 1;

        pit1_to_sender.socket_binding.socket_active = true;
        pit1_to_sender.socket_binding.socket_active = true;
        pit1_to_sender.socket_binding.source_ip = SocketMap::getIP("192.168.0.2");
        pit1_to_sender.socket_binding.source_port = 2020;
        pit1_to_sender.socket_binding.dest_ip = SocketMap::getIP("192.168.0.1");
        pit1_to_sender.socket_binding.dest_port = 80;

        scnsl->bind(pit_1_node, ch, pit1_to_sender);
        scnsl->bind(&p1, &sender, ch, pit1_to_sender, tcp1);

        // SERVER -> TASK2
        BindSetup_base_t sender_to_pit2;
        sender_to_pit2.extensionId = "core";
        sender_to_pit2.destinationNode = pit_2_node;
        sender_to_pit2.node_binding.x = 0;
        sender_to_pit2.node_binding.y = 0;
        sender_to_pit2.node_binding.z = 0;
        sender_to_pit2.node_binding.bitrate = 100000000;  
        sender_to_pit2.node_binding.transmission_power = 100;
        sender_to_pit2.node_binding.receiving_threshold = 1;

        sender_to_pit2.socket_binding.socket_active = true;
        sender_to_pit2.socket_binding.source_ip = SocketMap::getIP( "192.168.0.1");
        sender_to_pit2.socket_binding.source_port = 81;
        sender_to_pit2.socket_binding.dest_ip = SocketMap::getIP("192.168.0.3");
        sender_to_pit2.socket_binding.dest_port = 2020;

        scnsl->bind(senderNode, ch2, sender_to_pit2);
        scnsl->bind(&sender, &p2, ch2, sender_to_pit2, tcpServer);

        BindSetup_base_t pit2_to_sender;
        pit2_to_sender.extensionId = "core";
        pit2_to_sender.destinationNode = senderNode;
        pit2_to_sender.node_binding.x = 0;
        pit2_to_sender.node_binding.y = 1;
        pit2_to_sender.node_binding.z = 0;
        pit2_to_sender.node_binding.bitrate = 1000000; 
        pit2_to_sender.node_binding.transmission_power = 100;
        pit2_to_sender.node_binding.receiving_threshold = 1;

        pit2_to_sender.socket_binding.socket_active = true;
        pit2_to_sender.socket_binding.source_ip = SocketMap::getIP("192.168.0.3");
        pit2_to_sender.socket_binding.source_port = 2020;
        pit2_to_sender.socket_binding.dest_ip = SocketMap::getIP("192.168.0.1");
        pit2_to_sender.socket_binding.dest_port = 81;

        scnsl->bind(pit_2_node, ch2, pit2_to_sender);
        scnsl->bind(&p2, &sender, ch2, pit2_to_sender, tcp2);

        sc_core::sc_start(sc_core::sc_time(60, sc_core::SC_SEC));
        sc_core::sc_stop();
    }
    catch (std::exception & e)
    {
        std::cerr << "Exception in sc_main(): " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
