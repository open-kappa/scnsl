// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTaskSender.hh"


using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace std;
using namespace Scnsl::Tlm;


int sc_main( int argc, char * argv[] )
{
	try
    {
        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n1 = sim->createNode();
        Scnsl::Core::Node_t * n2 = sim->createNode();


        CoreChannelSetup_t csb;

        csb.channel_type = CoreChannelSetup_t::UNIDIRECTIONAL;
        csb.capacity = 1000;
        csb.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
        csb.extensionId = "core";
        csb.name = "channel_unidirectional";

        Scnsl::Core::Channel_if_t * ch1 = sim->createChannel( csb );

		CoreTaskSetup_t corets;

		corets.task_type = CoreTaskSetup_t::PIT;
		corets.name = "task2";
		corets.id = 2;
		corets.n = n2;
		corets.extensionId = "core";

        BindSetup_base_t bsb1;
        BindSetup_base_t bsb2;
        bsb1.extensionId = "core";
        bsb2.extensionId = "core";
        sim->bind(n1, ch1, bsb1);
        sim->bind(n2, ch1, bsb2);


        MyTaskSender t1( "task1", 1, n1, 1 );
		Task_if_t * t2 = sim->createTask( corets );

        sim->bind(&t1, nullptr, ch1, bsb1, nullptr);
        sim->bind(t2, nullptr, ch1, bsb2, nullptr);

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // - Setting to trace all:
        cts.info = 5;
        cts.debug = 5;
        cts.log = 5;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // Core formatter specific option:
        //  printing also the type of trace:
        cts.print_trace_type = true;
        // - Creating:
        Scnsl_t::Tracer_t * tracer = sim->createTracer( cts );
        // - Setting the output stream:
        tracer->addOutput( & std::cout );
        // - Adding to trace:
        tracer->trace( & t1 );
        tracer->trace( t2 );

        sc_core::sc_start( sc_core::sc_time( 100, sc_core::SC_MS ) );
        sc_core::sc_stop();

	}
    catch (exception & e)
    {
		cout << e.what() << endl;

        cout << argc << endl;
        cout << argv[0] << endl;
	}

	return 0;
}
