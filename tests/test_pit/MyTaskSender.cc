// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

/// @file
/// A TLM task.

#include <sstream>

//#include "TlmTaskProxy_t.hh"
#include "MyTaskSender.hh"


//using Scnsl::Tlm::TlmTask_if_t;

#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

// ////////////////////////////////////////////////////////////////
// Constructor.
// ////////////////////////////////////////////////////////////////

MyTaskSender::MyTaskSender( sc_core::sc_module_name modulename,
                            const task_id_t id,
                            Scnsl::Core::Node_t * n,
                            const size_t proxies )
    :
    // Parents:
    Scnsl::Tlm::TlmTask_if_t( modulename, id, n, proxies )
{
	SC_THREAD( writingProcess );
}


MyTaskSender::~MyTaskSender() {}


void MyTaskSender::writingProcess()
{

	for ( ;; )
	{
		byte_t i = byte_t( rand()%25 + 65 );
	    const std::string tp = "0";
		const size_t s = 1;

        // Since we are tracing a complex message,
        // we wrap the log with the macro, to avoid
        // unuseful overhead.
#if ( SCNSL_LOG >= 1 )
        std::stringstream ss;
        ss << "data sent: " << i << " bytes: " << s;
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif
		TlmTask_if_t::send( tp, &i, s, 0 );
	}
}


void MyTaskSender::b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ){

	t=t;
	bool c;

	if(p.get_command() == Scnsl::Tlm::tlm_command_t(Scnsl::Tlm::CARRIER_COMMAND))
	{
		c = *p.get_data_ptr() != 0;

        // Since we are tracing a complex message,
        // we wrap the log with the macro, to avoid
        // unuseful overhead.
#if ( SCNSL_LOG >= 1 )
        std::stringstream ss;
        ss << "carrier: " << c;
        SCNSL_TRACE_LOG( 1, ss.str().c_str() );
#endif
	}

}
