// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <sstream>
#include <systemc>
#include <tlm.h>
#include <exception>

#include <scnsl.hh>
#include "MyTask_t.hh"



using namespace Scnsl::Setup;
using namespace Scnsl::BuiltinPlugin;


int sc_main( int argc, char * argv[] )
{
	try {

        // Singleton.
        Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

        // Nodes creation:
        Scnsl::Core::Node_t * n0 = sim->createNode();
        Scnsl::Core::Node_t * n1 = sim->createNode();
        Scnsl::Core::Node_t * n2 = sim->createNode();
        Scnsl::Core::Node_t * n3 = sim->createNode();

        CoreChannelSetup_t ccs;

        ccs.extensionId = "core";
        ccs.channel_type = CoreChannelSetup_t::DELAYED_SHARED;
        ccs.name = "DelayedSharedChannel";
        ccs.alpha = 1;
        ccs.propagation = CoreChannelSetup_t::WATER_SOUND_SPEED;
        ccs.nodes_number = 4;
         Scnsl::Utils::DefaultEnvironment_t::createInstance(ccs.alpha,1480);
        Scnsl::Core::Channel_if_t * ch = sim->createChannel( ccs );

        const Scnsl::Core::task_id_t id0 = 0;
        const Scnsl::Core::task_id_t id1 = 1;
        const Scnsl::Core::task_id_t id2 = 2;
        const Scnsl::Core::task_id_t id3 = 3;

        const Scnsl::Core::size_t PROXIES = 1;

        MyTask_t t0( "Task0", 1, id0, n0, PROXIES );
        MyTask_t t1( "Task1", 2, id1, n1, PROXIES );
        MyTask_t t2( "Task2", 3, id2, n2, PROXIES );
        MyTask_t t3( "Task3", 3, id3, n3, PROXIES );

        // Adding tracing features:
        CoreTracingSetup_t cts;
        cts.extensionId = "core";
        // - Setting the formatter:
        cts.formatterExtensionId = "core";
        cts.formatterName = "basic";
        // Setting the filter:
        cts.filterExtensionId = "core";
        cts.filterName = "basic";
        // Core formatter specific option:
        // printing also the type of trace:
        cts.print_trace_type = true;

        // - Setting to trace only user-like infos:
        cts.info = 5;
        cts.debug = 0;
        cts.log = 5;
        cts.error = 0;
        cts.warning = 0;
        cts.fatal = 0;
        // - Creating:
        Scnsl_t::Tracer_t * tracer1 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer1->addOutput( & std::cout );
        // - Adding to trace:
        tracer1->trace( & t0 );
        tracer1->trace( & t1 );
        tracer1->trace( & t2 );
        tracer1->trace( & t3 );
        tracer1->trace( ch );

        // - Setting to trace backend-like infos:
        cts.info = 0;
        cts.debug = 5;
        cts.log = 0;
        cts.error = 5;
        cts.warning = 5;
        cts.fatal = 5;
        // - Creating:
        Scnsl_t::Tracer_t * tracer2 = sim->createTracer( cts );
        // - Setting the output stream:
        tracer2->addOutput( & std::cerr );
        // - Adding to trace:
        tracer2->trace( & t0 );
        tracer2->trace( & t1 );
        tracer2->trace( & t2 );
        tracer2->trace( & t3 );
        tracer2->trace( ch );
        tracer2->trace( Scnsl::Utils::EventsQueue_t::get_instance() );

        // Binding:
        BindSetup_base_t bsb0;
        bsb0.extensionId = "core";
        bsb0.destinationNode = n2;
        bsb0.node_binding.x = 1000;
        bsb0.node_binding.y = 0;
        bsb0.node_binding.z = 0;
        bsb0.node_binding.bitrate = 100;
        bsb0.node_binding.transmission_power = 1500;
        bsb0.node_binding.receiving_threshold = 1;

        sim->bind(n0, ch, bsb0);
        sim->bind(&t0, &t2, ch, bsb0, nullptr);

        BindSetup_base_t bsb1;
        bsb1.extensionId = "core";
        bsb1.destinationNode = n3;
        bsb1.node_binding.x = 2000;
        bsb1.node_binding.y = 0;
        bsb1.node_binding.z = 0;
        bsb1.node_binding.bitrate = 100;
        bsb1.node_binding.transmission_power = 1500;
        bsb1.node_binding.receiving_threshold = 1;

        sim->bind(n1, ch, bsb1);
        sim->bind(&t1, &t3, ch, bsb1, nullptr);

        BindSetup_base_t bsb2;
        bsb2.extensionId = "core";
        bsb2.destinationNode = nullptr;
        bsb2.node_binding.x = 0;
        bsb2.node_binding.y = 0;
        bsb2.node_binding.z = 0;
        bsb2.node_binding.bitrate = 100;
        bsb2.node_binding.transmission_power = 100;
        bsb2.node_binding.receiving_threshold = 1;

        sim->bind(n2, ch, bsb2);
        sim->bind(&t2, &t0, ch, bsb2, nullptr);

        BindSetup_base_t bsb3;
        bsb3.extensionId = "core";
        bsb3.destinationNode = nullptr;
        bsb3.node_binding.x = 3000;
        bsb3.node_binding.y = 0;
        bsb3.node_binding.z = 0;
        bsb3.node_binding.bitrate = 100;
        bsb3.node_binding.transmission_power = 100;
        bsb3.node_binding.receiving_threshold = 1;

        sim->bind(n3, ch, bsb3);
        sim->bind(&t3, &t1, ch, bsb3, nullptr);

        sc_core::sc_start( sc_core::sc_time( 3, sc_core::SC_SEC ) );
        sc_core::sc_stop();
	}
    catch ( std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << argc << std::endl;
        std::cerr << argv[ 0 ] << std::endl;
        return 1;
	}
	return 0;
}
