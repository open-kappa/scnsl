// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#include <scnsl.hh>


using namespace Scnsl;
using namespace Scnsl::Core;
using namespace Scnsl::BuiltinPlugin;
using namespace Scnsl::Setup;
using namespace Scnsl::Tlm;

int sc_main( int /*argc*/, char * /*argv*/[] )
{
    // ///////////////////////////////////////////////////////////
    // Scnsl:
    // ///////////////////////////////////////////////////////////

    Scnsl::Setup::Scnsl_t * sim = Scnsl::Setup::Scnsl_t::get_instance();

    // ///////////////////////////////////////////////////////////
    // Nodes:
    // ///////////////////////////////////////////////////////////

    Scnsl::Core::Node_t * n1 = sim->createNode();
    Scnsl::Core::Node_t * n2 = sim->createNode();

    // ///////////////////////////////////////////////////////////
    // Channel:
    // ///////////////////////////////////////////////////////////

    CoreChannelSetup_t csb;

    csb.extensionId = "core";
    csb.channel_type = CoreChannelSetup_t::UNIDIRECTIONAL;
    csb.capacity = 10000;
    csb.delay = sc_core::sc_time(10.0, sc_core::SC_MS);
    csb.name = "channel_unidirectional";

    Scnsl::Core::Channel_if_t * ch = sim->createChannel( csb );

    // ///////////////////////////////////////////////////////////
    // Node bindings:
    // ///////////////////////////////////////////////////////////

    BindSetup_base_t bsb1;
    bsb1.extensionId = "core";

    sim->bind(n1, ch, bsb1);

    BindSetup_base_t bsb2;
    bsb2.extensionId = "core";

    sim->bind(n2, ch, bsb2);

    // ///////////////////////////////////////////////////////////
    // Tasks:
    // ///////////////////////////////////////////////////////////

    CoreTaskSetup_t cts1;

    cts1.extensionId = "core";
    cts1.task_type = CoreTaskSetup_t::CBR;
    cts1.id = 1;
    cts1.n = n1;
    cts1.pktSize = 2;
    cts1.genTime = sc_core::sc_time(5, sc_core::SC_MS);
    cts1.name = "task1";

    Scnsl::Core::Task_if_t * t1 = sim->createTask( cts1 );
    static_cast< Scnsl::Traffic::Traffic_if_t * >( t1 )->enable();


    CoreTaskSetup_t cts2;
    cts2.extensionId = "core";
    cts2.task_type = CoreTaskSetup_t::PIT;
    cts2.id = 2;
    cts2.n = n2;
    cts2.name = "task2";

    Scnsl::Core::Task_if_t * t2 = sim->createTask( cts2 );

    // ///////////////////////////////////////////////////////////
    // Task bindings:
    // ///////////////////////////////////////////////////////////

    sim->bind(t1, nullptr, ch, bsb1, nullptr);
    sim->bind(t2, nullptr, ch, bsb2, nullptr);

    // ///////////////////////////////////////////////////////////
    // Tracing:
    // ///////////////////////////////////////////////////////////

    CoreTracingSetup_t cts;

    cts.extensionId = "core";
    cts.formatterExtensionId = "core";
    cts.filterExtensionId = "core";
    cts.formatterName = "basic";
    cts.filterName = "basic";

    cts.info = 5;
    cts.debug = 5;
    cts.log = 5;
    cts.error = 5;
    cts.warning = 5;
    cts.fatal = 5;

    Scnsl_t::Tracer_t * tracer = sim->createTracer( cts );

    tracer->addOutput( & std::cout );

    tracer->trace( t1 );
    tracer->trace( t2 );

    // ///////////////////////////////////////////////////////////
    // Executing:
    // ///////////////////////////////////////////////////////////

    sc_core::sc_start( sc_core::sc_time( 100, sc_core::SC_SEC ) );
    sc_core::sc_stop();

    return 0;
}
