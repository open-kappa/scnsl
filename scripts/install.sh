#!/usr/bin/env bash

# Check if script is running inside Ubuntu
if [ $(lsb_release -i -s) != "Ubuntu" ]; then
    >&2 echo "This script is made for Ubuntu!"
    exit 1
fi

# Check if SystemC is already downloaded
download_sysc=$false
if ! [ -e third-parties/systemc/CMakeLists.txt ]; then
    echo "No SystemC  CMake found in third parties. Should this script download and install it?"
    select yn in "y" "n"; do
        case $yn in 
            y ) download_sysc=$true; break;;
            n ) echo "Please download SystemC 2.3.2 and copy the folder inside third-parties/systemC, than run this script again";
                exit 0
        esac
    done
fi

# Check if SystemC is already installed (check for lib)
install_sysc=$true
if [ -e /usr/local/lib/libsystemc.so.2.3.2 ]; then
    echo "WARNING, SystemC 2.3.2 is already installed on this machine."
    echo "Do you wish to override current systemC installation?"
    select yn in "y" "n"; do
        case $yn in 
            y ) break;;
            n ) install_sysc=$false; break;;
        esac
    done
fi

# Install requirements
sudo apt install -y build-essential git wget doxygen

# Download and install SystemC 2.3.2 (last working version with SCNSL) if needed
if [ $download_sysc -eq $true ]; then
    cd third-parties/
    rm -r systemc
    wget -O systemc.tar.gz https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.2.tar.gz
    tar xvf systemc.tar.gz
    rm -r systemc
    mv systemc-2.3.2 systemc
    rm systemc.tar.gz
    cd ..
fi

#Compile and install Scnsl + SystemC
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=. ..
make -j$(grep ^processor /proc/cpuinfo | wc -l)
make install
make scnsl-tests -j$(grep ^processor /proc/cpuinfo | wc -l)
if [ $install_sysc -eq $true ]; then
    #copy everything 
    sudo cp -av lib/libs* /usr/local/lib/; 
    sudo cp -r include/* /usr/local/include/
else
    #copy only scnsl
    sudo cp -av lib/libscnsl* /usr/local/lib/; 
    sudo cp -r include/scnsl* /usr/local/include/
fi
echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib/" >> ~/.bashrc
echo "############################"
echo "Installation finished!"
echo "############################"
