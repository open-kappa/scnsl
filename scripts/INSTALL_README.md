# How to use the install.sh script

1. Open the terminal and run the script from the sources root dir.
    Change script's permission if needed.
2. The script will check if the third-parties/systemc folder is empty. If yes it
    will ask the user if automatically download SystemC or not. If something is
    already present in the directory, this passage will be skipped.
3. The script will check if SystemC-2.3.2 library is already installed. If yes it
    will ask weather overwrite it or not.
4. The script will download all required tools needed to compile SCNSL
5. The scirpt will compile SystemC + SCNSL via CMake and Makefile using all available
    resources.
    WARNING: due using parallel compiation, deadlocks may occur which stop compilation.
    In this case it is necessary to halt the scirpt and run it again.
6. Based on previous decisions, the scirpt will either install libraries and headers
    only of SCNSL or SCNSL and SystemC in usr/local.
