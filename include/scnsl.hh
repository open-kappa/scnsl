// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_HH
#define SCNSL_HH

/// @brief The namespace which groups all SCNSL code.
namespace Scnsl {}


// ////////////////////////////////////////////////////////////////
// Core package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Core {}
 }

#include "scnsl/scnslConfig.hh"
#include "scnsl/core/data_types.hh"
#include "scnsl/core/Node_t.hh"
#include "scnsl/core/Packet_t.hh"
#include "scnsl/core/ProtocolPacket_if_t.hh"
#include "scnsl/core/SimpleProtocolPacket_t.hh"
#include "scnsl/core/PacketType.hh"
#include "scnsl/core/PacketCommand.hh"
#include "scnsl/core/TaskProxy_if_t.hh"
#include "scnsl/core/Communicator_if_t.hh"
#include "scnsl/core/Channel_if_t.hh"
#include "scnsl/core/Task_if_t.hh"
#include "scnsl/core/QueueCommunicator_t.hh"
#include "scnsl/core/RadiationPattern_if_t.hh"
#include "scnsl/core/Coordinate_t.hh"
#include "scnsl/core/SocketMap.hh"

// ////////////////////////////////////////////////////////////////
// RTL package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Rtl {}
}


#include "scnsl/rtl/rtl_data_types.hh"
#include "scnsl/rtl/RtlTask_base_if_t.hh"
#include "scnsl/rtl/RtlTask_if_t.hh"
#include "scnsl/rtl/RtlTask_if_t.i.hh"
#include "scnsl/rtl/RtlTaskProxy_t.hh"


// ////////////////////////////////////////////////////////////////
// TLM package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Tlm {}
}


#include "scnsl/tlm/tlm_data_types.hh"
#include "scnsl/tlm/TlmTask_if_t.hh"
#include "scnsl/tlm/TlmTaskProxy_t.hh"


// ////////////////////////////////////////////////////////////////
// Utils package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Utils {}
}


#include "scnsl/utils/CommunicatorBridge_t.hh"
#include "scnsl/utils/CommunicatorStack_t.hh"
#include "scnsl/utils/ChannelBridge_t.hh"
#include "scnsl/utils/ChannelWrapper_if_t.hh"
#include "scnsl/utils/Environment_if_t.hh"
#include "scnsl/utils/DefaultEnvironment_t.hh"
#include "scnsl/utils/DirectionalEnvironment_t.hh"
#include "scnsl/utils/UserEnvironment_t.hh"




// ////////////////////////////////////////////////////////////////
// Setup package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Setup {}
}


#include "scnsl/setup/BindSetup_base_t.hh"
#include "scnsl/setup/ChannelSetup_base_t.hh"
#include "scnsl/setup/CommunicatorSetup_base_t.hh"
#include "scnsl/setup/TracingSetup_base_t.hh"
#include "scnsl/setup/Extension_if_t.hh"
#include "scnsl/setup/PluginManager_t.hh"
#include "scnsl/setup/Scnsl_t.hh"
#include "scnsl/setup/setup_data_types.hh"
#include "scnsl/setup/TaskSetup_base_t.hh"
#include "scnsl/setup/TopologySetup_base_t.hh"


// ////////////////////////////////////////////////////////////////
// Tracing package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all SCNSL tracing features.
  namespace Tracing {}
}


#include "scnsl/tracing/tracing_data_types.hh"
#include "scnsl/tracing/tracing_macros.hh"
#include "scnsl/tracing/Traceable_base_t.hh"
#include "scnsl/tracing/Tracer_t.hh"
#include "scnsl/tracing/Formatter_if_t.hh"
#include "scnsl/tracing/Filter_if_t.hh"
#include "scnsl/tracing/BasicFormatter_t.hh"
#include "scnsl/tracing/BasicFilter_t.hh"


// ////////////////////////////////////////////////////////////////
// BuiltinPlugin package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace BuiltinPlugin {}
}


#include "scnsl/builtinPlugin/CoreTopologySetup_t.hh"
#include "scnsl/builtinPlugin/CoreChannelSetup_t.hh"
#include "scnsl/builtinPlugin/CoreTaskSetup_t.hh"
#include "scnsl/builtinPlugin/CoreExtension_t.hh"
#include "scnsl/builtinPlugin/CoreCommunicatorSetup_t.hh"
#include "scnsl/builtinPlugin/CoreTracingSetup_t.hh"



// ////////////////////////////////////////////////////////////////
// Channels package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Channels {}
}

#include "scnsl/channels/HalfDuplexChannel_t.hh"
#include "scnsl/channels/FullDuplexChannel_t.hh"
#include "scnsl/channels/UnidirectionalChannel_t.hh"
#include "scnsl/channels/SharedChannel_t.hh"




// ////////////////////////////////////////////////////////////////
// Protocols package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups builtin protocols.
  namespace Protocols {}
}
//
// MAC 802.15.4
//
namespace Scnsl { namespace Protocols {
    /// @brief Provides an implementation of MAC 802.15.4 as EFSM.
    namespace Mac_802_15_4 {}
  } }
//
#include "scnsl/protocols/mac_802_15_4/Mac802_15_4_t.hh"
#include "scnsl/protocols/mac_802_15_4/MacEfsm_t.hh"
#include "scnsl/protocols/mac_802_15_4/MacTransitionFunctions_t.hh"
//

//
// RouterCommunicator
//
namespace Scnsl { namespace Protocols {
    /// @brief Provides an implementation of a router.
    namespace RouterCommunicator {}
  } }
//
#include "scnsl/protocols/router_communicator/RouterCommunicator_t.hh"
#include "scnsl/protocols/router_communicator/RoutingProtocolPacket_t.hh"
//

//
// TCP
//
namespace Scnsl{ namespace Protocols {
        ///@brief Implements tcp protocol
        namespace Network_Lv4 {}
}}

//
#include "scnsl/protocols/lv4_communicator/Lv4ProtocolPacket_t.hh"
#include "scnsl/protocols/lv4_communicator/connections/TcpByteConnection.hh"
#include "scnsl/protocols/lv4_communicator/connections/TcpConnection_if_t.hh"
#include "scnsl/protocols/lv4_communicator/connections/TcpConnectionFactory.hh"
#include "scnsl/protocols/lv4_communicator/Lv4Communicator_t.hh"
#include "scnsl/protocols/lv4_communicator/TimeoutStruct_t.hh"
#include "scnsl/protocols/lv4_communicator/Lv4ByteSaboteur_t.hh"
#include "scnsl/protocols/lv4_communicator/NetworkAPI_Task_if_t.hh"
#include "scnsl/protocols/lv4_communicator/Socket_t.hh"
#include "scnsl/protocols/lv4_communicator/EventManager_t.hh"
#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_if_t.hh"
#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_SlowStart_Default_t.hh"
#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_CA_AIMD_t.hh"
#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlg_FR_Reno_t.hh"
#include "scnsl/protocols/lv4_communicator/algorithms/TcpAlgFactory.hh"
#include "scnsl/protocols/lv4_communicator/UdpProtocol.hh"
/////////////////////////////////////////////////////////////////
// Traffic package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace Traffic {}
}

#include "scnsl/traffic/Traffic_if_t.hh"
#include "scnsl/traffic/Cbr_t.hh"
#include "scnsl/traffic/Gilbert_t.hh"
#include "scnsl/traffic/OnOff_t.hh"
#include "scnsl/traffic/PitTask_t.hh"


// ////////////////////////////////////////////////////////////////
// Queue models package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all basic SCNSL types and interfaces.
  namespace QueueModels {}
}

#include "scnsl/queueModels/Queue_if_t.hh"
#include "scnsl/queueModels/Fifo_t.hh"
#include "scnsl/queueModels/Priority_t.hh"

// ////////////////////////////////////////////////////////////////
// Antenna models package includes.
// ////////////////////////////////////////////////////////////////

namespace Scnsl {
  /// @brief Namespace which groups all different antenna radiation patterns
  namespace antennaModels {}
}
#include "scnsl/antennaModels/QuarterWaveDipole_t.hh"
#include "scnsl/antennaModels/HalfWaveDipole_t.hh"
#include "scnsl/antennaModels/FullWaveDipole_t.hh"
#include "scnsl/antennaModels/ConePattern_t.hh"
#include "scnsl/antennaModels/PatchPattern_t.hh"
#include "scnsl/antennaModels/IsotropicAntenna_t.hh"
#include "scnsl/antennaModels/CustomAntenna_t.hh"

// ////////////////////////////////////////////////////////////////
// Systemc Calls re-implemented to be SystemC-compatible.
// ////////////////////////////////////////////////////////////////
namespace Scnsl{
    ///@brief Namespace which groups all systemc calls implemented to be compatible with SystemC an Scnsl
    namespace Syscalls {}
}
// #include "scnsl/system_calls/TimedSyscalls.h"
// #include "scnsl/system_calls/NetworkSyscalls.hh"
#include "scnsl/system_calls/SyscallsWait.hh"


#endif //SCNSL_HH

