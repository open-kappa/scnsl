// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_BINDSETUP_BASE_T_HH
#define SCNSL_SETUP_BINDSETUP_BASE_T_HH



/// @file
/// Bind setup class.

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "setup_data_types.hh"

namespace Scnsl { namespace Core {

    class Node_t;

  } }

namespace Scnsl { namespace Setup {


    /// @brief The bind setup class.
    /// Internal fields are public, since this class is intended to be used
    /// as a sort of struct.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT BindSetup_base_t
    {
    public:

        /// @brief Constructor.
        BindSetup_base_t();

        /// @brief Destructor.
        virtual
        ~BindSetup_base_t();

        /// @brief The ID of the extension.
        Scnsl::Setup::ExtensionId_t extensionId;

        /// @brief The properties of node-channel binding.
        Scnsl::Core::node_properties_t node_binding;

        /// @brief The socket properties of task-to-task binding.
        Scnsl::Core::socket_properties_t socket_binding;


        /// @brief The destination node.
        /// Broadcast is nullptr.
        Scnsl::Core::Node_t * destinationNode;

		/// @brief The Identifier Number of the task.
        std::string bindIdentifier;
    private:

        /// @brief Disabled copy constructor.
        BindSetup_base_t( const  BindSetup_base_t & );

        /// @brief Disabled assignemnt operator.
        BindSetup_base_t & operator = ( const  BindSetup_base_t & );
    };



} }



#endif
