// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_SCNSL_T_HH
#define SCNSL_SETUP_SCNSL_T_HH

/// @file
/// The simulator class.

#include <exception>
#include <vector>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "setup_data_types.hh"
#include "ChannelSetup_base_t.hh"
#include "TaskSetup_base_t.hh"
#include "CommunicatorSetup_base_t.hh"
#include "TracingSetup_base_t.hh"
#include "TopologySetup_base_t.hh"
#include "BindSetup_base_t.hh"

#include "PluginManager_t.hh"

namespace Scnsl { namespace Core {

    class Node_t;
    class Channel_if_t;
    class Task_if_t;
    class Communicator_if_t;

  } }

namespace Scnsl { namespace Tracing {

    class Tracer_t;
    class Formatter_if_t;
    class Filter_if_t;

  } }

namespace Scnsl { namespace Setup {

    class Extension_if_t;


  ///@brief Information needed during the bind of multiple tasks together
  struct SCNSL_EXPORT multicast_wrapper_t
  {
    Scnsl::Core::Task_if_t* task;
    Scnsl::Core::Communicator_if_t* comm;
    Scnsl::Core::Channel_if_t* ch;
    Scnsl::Setup::BindSetup_base_t* bsb;
    /// @brief Empty struct constructor
    multicast_wrapper_t();

    /// @brief Copy struct constructor
    multicast_wrapper_t(const multicast_wrapper_t& mw);
  };

  } }


namespace Scnsl { namespace Setup {



    /// @brief The simulator class.
    /// Used to simplify components instantation.
    ///
    /// Design patterns:
    /// - Singleton.
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT Scnsl_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The node type.
        typedef Scnsl::Core::Node_t Node_t;

        /// @brief The node id type.
        typedef Scnsl::Core::node_id_t node_id_t;

        /// @brief The node properties id type.
        typedef Scnsl::Core::node_properties_id_t node_properties_id_t;

        /// @brief The channel type.
        typedef Scnsl::Core::Channel_if_t Channel_if_t;

        /// @brief The task type.
        typedef Scnsl::Core::Task_if_t Task_if_t;

        /// @brief The communicator type.
        typedef Scnsl::Core::Communicator_if_t Communicator_if_t;

        /// @brief The tracer type.
        typedef Scnsl::Tracing::Tracer_t Tracer_t;

        /// @brief The formatter type.
        typedef Scnsl::Tracing::Formatter_if_t Formatter_if_t;

        /// @brief The filter type.
        typedef Scnsl::Tracing::Filter_if_t Filter_if_t;

        /// @brief The extension id type.
        typedef Scnsl::Setup::ExtensionId_t Extension_id_t;

        /// @brief The plugin name type.
        typedef Scnsl::Setup::PluginName_t PluginName_t;

        /// @brief A list of registered nodes.
        typedef std::vector< Node_t * > NodeList_t;

        /// @brief A list of registered channels.
        typedef std::vector< Channel_if_t * > ChannelList_t;

        /// @brief A list of registered tasks.
        typedef std::vector< Task_if_t * > TaskList_t;

        /// @brief A list of registered communicators.
        /// It contains: taskproxies and other communicators.
        typedef std::vector< Communicator_if_t * > CommunicatorList_t;

        /// @brief Tracer list.
        typedef std::vector< Tracer_t * > TracerList_t;

        /// @brief Associates extensions ids to extensions implementations.
        typedef Scnsl::Setup::ExtensionList_t ExtensionList_t;


        /// @brief A size type.
        typedef Scnsl::Core::size_t size_t;

        //@}

        /// @name Singleton design pattern methods.
        //@{

        /// @brief Gets an instance of the simulator.
        static
        Scnsl_t * get_instance();

        //@}


        /// @name Instantation methods.
        //@{

        /// @brief Creates a node with the specified id.
        ///
        /// @param id the node id.
        ///
        Node_t * createNode(node_id_t id = static_cast<node_id_t>(-1));

        /// @brief Creates a channel.
        ///
        /// @param s The channel parameters.
        ///
        Channel_if_t * createChannel( const ChannelSetup_base_t & s );

        /// @brief Creates a task.
        ///
        /// @param s The task parameters.
        ///
        Task_if_t * createTask( const TaskSetup_base_t & s );

        /// @brief Creates a communicator.
        ///
        /// @param s The communicator parameters.
        ///
        Communicator_if_t * createCommunicator( const CommunicatorSetup_base_t & s );

        /// @brief Creates a predefined topology.
        ///
        /// @param s The topology parameters.
        ///
        void createTopology( const TopologySetup_base_t & s );


        /// @brief Creates a tracer.
        ///
        /// @param s The tracer parameters.
        ///
        Tracer_t * createTracer( const TracingSetup_base_t & s );

        //@}

        /// @name Binding methods.
        //@{

        /// @brief Binds a node with a channel.
        ///
        /// @param n The node.
        /// @param ch The channel.
        /// @param s The node-channel setup.
        ///
        void bind( Node_t * n, Channel_if_t * ch, const BindSetup_base_t & s );

        /// @brief Binds a task and a channel using a communicator.
        ///        In case of direct bind parameter c shall be nullptr.
        ///
        /// @param t The task.
        /// @param dst The destination task. Can be nullptr for broadcast.
        /// @param ch The channel.
        /// @param s The binding setup.
        /// @param c The communicator. Optional.
        /// @param reuseComm True if the communicator must be bound to the channel and the node
        /// (set to false to bind two task multiple time using the same communicator). The default value is true.
        /// @throw std::domain_error Unregistered extension.
        /// @throw std::logic_error Bind error occurred.
        ///
        void bind( Task_if_t * t, Task_if_t * dst, Channel_if_t * ch,
                   const BindSetup_base_t & s, Communicator_if_t * c = nullptr, bool reuseComm = true );

        /// @brief Binds multiple task which communicates via a given multicast address
        /// @param elemenst a list of multicast_wrapper_t containing all information to
        /// bind a task.
        /// @param multicast_ip the multicast ip used to bind all elements
        /// @param multicast_port the port task will used to receive multicast messages 
        /// @throw std::domain_error Unregistered extension.
        /// @throw std::logic_error Bind error occurred.
        ///
        void bind( std::vector<multicast_wrapper_t> elements, unsigned multicast_ip);

        //@}

        /// @name Other functionalities.
        //@{

        /// @brief Gets the list of all created nodes.
        NodeList_t & getNodes();

        /// @brief Gets the list of all created channels.
        ChannelList_t & getChannels();

        /// @brief Loads an extension.
        ///
        /// @param id The extension ID.
        ///
        void loadExtension( const PluginName_t & id );

        /// @brief Sets an event.
        // void setEvent( /* params*/ )
        //;

        //@}



        /// @name Internal support methods.
        //@{

        /// @brief Creates a formatter.
        ///
        /// @param s The formatter parameters.
        ///
        Formatter_if_t * createFormatter( const TracingSetup_base_t & s );


        /// @brief Creates a filter.
        ///
        /// @param s The filter parameters.
        ///
        Filter_if_t * createFilter( const TracingSetup_base_t & s );

        //@}

    protected:

        /// @brief Default consturctor.
        /// It also sets new exception termintate() and
        /// unexpected() handlers.
        Scnsl_t();

        /// @brief Destructor.
        ~Scnsl_t();

        /// @brief A list of registered nodes.
        NodeList_t _nodes;

        /// @brief A list of registered channels.
        ChannelList_t _channels;

        /// @brief A list of registered tasks.
        TaskList_t _tasks;

        /// @brief A list of registered communicators.
        /// It contains: taskproxies and other communicators.
        CommunicatorList_t _communicators;

        /// @brief The list of tracers.
        TracerList_t _tracers;

        /// @brief Associates extensions ids to extensions implementations.
        ExtensionList_t _extensions;

        /// @brief The plugin manager.
        PluginManager_t _pluginManager;

    private:

        /// @brief Disabled copy constructor.
        Scnsl_t( const Scnsl_t & );

        /// @brief Disabled assignement oeprator.
        Scnsl_t & operator = ( const Scnsl_t & );

    };

  } }



#endif
