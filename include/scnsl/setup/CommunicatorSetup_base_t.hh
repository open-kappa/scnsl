// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_COMMUNICATORSETUP_BASE_T_HH
#define SCNSL_SETUP_COMMUNICATORSETUP_BASE_T_HH



/// @file
/// Communicator setup base class.

#include "../scnslConfig.hh"
#include "setup_data_types.hh"

namespace Scnsl { namespace Setup {


    /// @brief The communicator setup base class.
    /// Since copying or assigning this class could create slicing,
    /// it is non copiable and non assignable.
    ///
    /// This class is intended to be used as a base class, and not manipulated
    /// as an interface.
    /// Thus, the virtual destructor is added just to allow RTTI.
    /// It is not impure, since communicator derived classes could not need
    /// additional parameter w.r.t. the Communicator_if_t class constructor.
    ///
    /// Internal fields are public, since this class is intended to be used
    /// as a sort of struct.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT CommunicatorSetup_base_t
    {
    public:

        /// @brief Constructor.
        CommunicatorSetup_base_t();

        /// @brief Virtual destructor.
        virtual
        ~CommunicatorSetup_base_t();

        /// @brief The ID of the extension.
        Scnsl::Setup::ExtensionId_t extensionId;

        /// @brief The communicator module name.
        Scnsl::Setup::ModuleName_t name;

    private:

        /// @brief Disabled copy constructor.
        CommunicatorSetup_base_t( const  CommunicatorSetup_base_t & );

        /// @brief Disabled assignemnt operator.
        CommunicatorSetup_base_t & operator = ( const  CommunicatorSetup_base_t & );
    };



} }



#endif
