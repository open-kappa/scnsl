// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_PLUGINMANAGER_T_HH
#define SCNSL_SETUP_PLUGINMANAGER_T_HH



/// @file
/// A plugin manager.

#include <stdexcept>
#include <list>

#include "../scnslConfig.hh"
#include "setup_data_types.hh"

namespace Scnsl { namespace Setup {


  /// @brief A dynamic library loader for extensions.
  /// Any plugin library shall implement a C method,
  /// void get_extensions( void * ), which takes a pointer to
  /// an ExtensionList_t, and adds to it all the plugin implemented
  /// extensions.
  /// These extensions will be deleted by SCNSL.
  ///
  /// Since it seems to make no sense to copy or clone this class,
  /// such a class is non copiable and non assignable.
  ///
  /// Design patterns:
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT PluginManager_t
  {
  public:

      /// @brief Traits.
      //@{

      /// @brief The plugin name type.
      typedef Scnsl::Setup::PluginName_t PluginName_t;

      /// @brief The dynamic loaded library handler type.
      typedef void * lib_handler_t;

      /// @brief The list of loaded libraries handlers.
      typedef std::list< lib_handler_t > HandlerList_t;

      /// @brief Associates extensions ids to extensions implementations.
      typedef Scnsl::Setup::ExtensionList_t ExtensionList_t;


      //@}

      /// @brief Constructor.
      PluginManager_t();


      /// @brief Destructor.
      /// Closes also all opened libraries.
      ~PluginManager_t();


      /// @brief Loads an exeternal set of extensions.
      /// The library is  opened using the RTLD_LOCAL and RTLD_LAZY flags.
      /// If a loaded extension already exists, an exception is thrown.
      ///
      /// @param name The plugin name.
      /// @param extensionList The extension list where to put the extensions.
      /// @throw std::domain_error An error occurred.
      ///
      void load( const PluginName_t & name, ExtensionList_t & extensionList );

  protected:

      /// @brief The list of opened handlers.
      HandlerList_t _handlers;

      /// @brief The method symbol to be checked.
      static
      const char * const _SYMBOL;

  private:

      /// @brief Disabled copy constructor.
      PluginManager_t( const PluginManager_t & );

      /// @brief Disabled assignement operator.
      PluginManager_t operator = ( const PluginManager_t & );

  };


} }



#endif
