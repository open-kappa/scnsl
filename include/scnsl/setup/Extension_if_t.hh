// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_EXTENSION_IF_T_HH
#define SCNSL_SETUP_EXTENSION_IF_T_HH



/// @file
/// The extension interface.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "setup_data_types.hh"
#include "Scnsl_t.hh"

namespace Scnsl { namespace Core {

    class Channel_if_t;
    class Task_if_t;
    class TaskProxy_if_t;
    class Communicator_if_t;

  } }


namespace Scnsl { namespace Tracing {

    class Tracer_t;
    class Formatter_if_t;
    class Filter_if_t;

  } }


namespace Scnsl { namespace Setup {

    class ChannelSetup_base_t;
    class TaskSetup_base_t;
    class CommunicatorSetup_base_t;
    class TracingSetup_base_t;
    class TopologySetup_base_t;

  } }

namespace Scnsl { namespace Setup {

    /// @brief The extension interface.
    ///
    /// Design patterns:
    /// - Interface.
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT Extension_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The extension ID type.
        typedef Scnsl::Setup::ExtensionId_t ExtensionId_t;

        /// @brief The node list type.
        typedef Scnsl_t::NodeList_t NodeList_t;

        /// @brief The channel list type.
        typedef Scnsl_t::ChannelList_t ChannelList_t;

        //@}

        virtual
        ~Extension_if_t();

        /// @brief Support methods.
        //@{

        /// @brief Gets this extension ID.
        const ExtensionId_t & getId()
            const;

        //@}

        /// @brief Creation methods.
        //@{

        /// @brief Creates a channel.
        ///
        /// @param s The channel parameters.
        /// @return The created channel.
        ///
        virtual
        Scnsl::Core::Channel_if_t * createChannel( const ChannelSetup_base_t & s ) = 0;

        /// @brief Creates a task.
        ///
        /// @param s The task parameters.
        /// @return The created task.
        ///
        virtual
        Scnsl::Core::Task_if_t * createTask( const TaskSetup_base_t & s ) = 0;

        /// @brief Creates a communicator.
        ///
        /// @param s The communicator parameters.
        /// @return The created communicator.
        ///
        virtual
        Scnsl::Core::Communicator_if_t * createCommunicator( const CommunicatorSetup_base_t & s ) = 0;


        /// @brief Creates a tracer.
        ///
        /// @param s The tracer parameters.
        /// @return The creater tracer.
        ///
        virtual
        Scnsl::Tracing::Tracer_t * createTracer( const TracingSetup_base_t & s ) = 0;

        /// @brief Creates a formatter.
        ///
        /// @param s The formatter parameters.
        /// @return The created formatter.
        ///
        virtual
        Scnsl::Tracing::Formatter_if_t * createFormatter( const TracingSetup_base_t & s ) = 0;

        /// @brief Creates a filter.
        ///
        /// @param s The filter parameters.
        /// @return The created filter.
        ///
        virtual
        Scnsl::Tracing::Filter_if_t * createFilter( const TracingSetup_base_t & s ) = 0;


        /// @brief Creates a predefined topology.
        ///
        /// @param s The topology parameters.
        ///
        virtual
        void createTopology( const TopologySetup_base_t & s ) = 0;

        /// @brief Creates a taskproxy.
        ///
        /// @param t The associated task.
        /// @param s The setupconfiguration.
        /// @param ch The associated channel.
        /// @return The created taskproxy.
        ///
        virtual
        Scnsl::Core::TaskProxy_if_t * createTaskProxy(
            Scnsl::Core::Task_if_t * t,
			const Scnsl::Setup::BindSetup_base_t & s,
            Scnsl::Core::Channel_if_t * ch ) = 0;

        //@}


    protected:

        /// @brief Constructor.
        explicit
        Extension_if_t( const ExtensionId_t & id );

        /// @brief This extension ID.
        const ExtensionId_t _ID;

    private:

        /// @brief Disabled copy construrctor.
        Extension_if_t( const Extension_if_t & );

        /// @brief Disabled assignment operator.
        Extension_if_t & operator = ( const Extension_if_t & );
    };


  } }



#endif
