// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_SETUP_DATA_TYPES_HH
#define SCNSL_SETUP_SETUP_DATA_TYPES_HH



/// @file
/// Setup general types.

#include <string>
#include <map>
#include <systemc>

#include "../scnslConfig.hh"

namespace Scnsl { namespace Setup {

    class Extension_if_t;

  } }

namespace Scnsl { namespace Setup {

    /// @name Setup general types.
    //@{

    /// @brief The extension id type.
    typedef std::string ExtensionId_t;

    /// @brief The extension id type.
    typedef std::string PluginName_t;

    /// @brief The module name.
    typedef std::string ModuleName_t;

    /// @brief Associates extensions ids to extensions implementations.
    typedef std::map< ExtensionId_t, Extension_if_t * > ExtensionList_t;

    //@}

  } }



#endif
