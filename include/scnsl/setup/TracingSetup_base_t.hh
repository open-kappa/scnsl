// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_SETUP_TRACINGSETUP_BASE_T_HH
#define SCNSL_SETUP_TRACINGSETUP_BASE_T_HH


/// @file
/// The tracing setup base class.

#include "../scnslConfig.hh"
#include "setup_data_types.hh"
#include "../tracing/tracing_data_types.hh"

namespace Scnsl { namespace Setup {

    /// @brief The tracing setup base class.
    /// Since copying or assigning this class could create slicing,
    /// it is non copiable and non assignable.
    ///
    /// This class is intended to be used as a base class, and not manipulated
    /// as an interface.
    /// Thus, the virtual destructor is added just to allow RTTI.
    /// It is not impure, since tracing derived classes could not need
    /// additional parameter w.r.t. the builtin plugin.
    ///
    /// Internal fields are public, since this class is intended to be used
    /// as a sort of struct.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT TracingSetup_base_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The level type.
        typedef Scnsl::Tracing::level_t level_t;

        //@}

        /// @brief Constructor.
        TracingSetup_base_t();

        /// @brief Virtual destructor.
        virtual
        ~TracingSetup_base_t();

        /// @brief The ID of the extension.
        Scnsl::Setup::ExtensionId_t extensionId;

        /// @brief The ID of the filter extension.
        Scnsl::Setup::ExtensionId_t filterExtensionId;

        /// @brief The name of filter.
        Scnsl::Setup::ModuleName_t filterName;

        /// @brief The ID of the extension.
        Scnsl::Setup::ExtensionId_t formatterExtensionId;

        /// @brief The name of formatter.
        Scnsl::Setup::ModuleName_t formatterName;

        /// @brief Infos trace level [0, 5].
        level_t info;

        /// @brief Logs trace level [0, 5].
        level_t log;

        /// @brief Debugs trace level [0, 5].
        level_t debug;

        /// @brief Warnings trace level [0, 5].
        level_t warning;

        /// @brief Errors trace level [0, 5].
        level_t error;

        /// @brief Fatals trace level [0, 5].
        level_t fatal;

    private:

        /// @brief Disabled copy constructor.
        TracingSetup_base_t( const  TracingSetup_base_t & );

        /// @brief Disabled assignemnt operator.
        TracingSetup_base_t & operator = ( const  TracingSetup_base_t & );
    };


  } }


#endif
