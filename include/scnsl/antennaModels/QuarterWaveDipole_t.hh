// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_ANTENNAMODELS_QUARTERWAVEDIPOLE_T_HH
#define SCNSL_ANTENNAMODELS_QUARTERWAVEDIPOLE_T_HH

#include "../core/RadiationPattern_if_t.hh"

namespace Scnsl { namespace antennaModels {

/// @brief Model of an antenna having pattern of kind "Quarter Wave Dipole".
/// Implemented formula: gain * sin() ^ 2.
class SCNSL_EXPORT QuarterWaveDipole_t:
        public Scnsl::Core::RadiationPattern_if_t
{
public:

    /// @brief Constructor for ideal dipole (not attenuated).
    /// @param vTheta versor (dipole orientation).
    /// @param active is true if the antenna is active at startup.
    /// @param antennaGain in dB.
    QuarterWaveDipole_t(const Coordinate_t vTheta,
                        const bool active,
                        const double antennaGain = 0.0);

    virtual ~QuarterWaveDipole_t();
    QuarterWaveDipole_t(const QuarterWaveDipole_t & other);
    QuarterWaveDipole_t & operator =(QuarterWaveDipole_t other);
    void swap(QuarterWaveDipole_t & other);

    virtual
    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const override;

    virtual
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const override;


};

} } // Scnsl::antennaModels

#endif // QUARTERWAVEDIPOLE_T_HH
