// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_ANTENNAMODELS_ISOTROPIC_T_HH
#define SCNSL_ANTENNAMODELS_ISOTROPIC_T_HH

#include "../core/RadiationPattern_if_t.hh"

namespace Scnsl { namespace antennaModels {

/// @brief Model of an antenna having pattern of kind "Quarter Wave Dipole".
/// Implemented formula: gain * sin() ^ 2.
class SCNSL_EXPORT IsotropicAntenna_t:
        public Scnsl::Core::RadiationPattern_if_t
{
public:

    /// @brief Constructor.
    /// @param vTheta versor expressed in (xyz)
    /// @param vPhi versor expressed in (xyz)
    /// @param active if true the pattern is working
    /// @param vPol polarization versor expressed in (ptn)
    /// @param antennaGain expressed in dB affects gain in any direction (default 0).
    IsotropicAntenna_t(const Coordinate_t vTheta,
                       const Coordinate_t vPhi,
                       const Coordinate_t vPol,
                       const bool active,
                       const double antennaGain = 1.0);

    virtual ~IsotropicAntenna_t();
    IsotropicAntenna_t(const IsotropicAntenna_t & other);
    IsotropicAntenna_t & operator =(IsotropicAntenna_t other);
    void swap(IsotropicAntenna_t & other);

    virtual
    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const override;

    virtual
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const override;



};

} } // Scnsl::antennaModels

#endif // ISOTROPIC_T_HH
