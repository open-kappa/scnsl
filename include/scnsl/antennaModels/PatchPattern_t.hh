// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_ANTENNA_MODELS_PATCH_T_HH
#define SCNSL_ANTENNA_MODELS_PATCH_T_HH

#include <systemc>

#include "../core/data_types.hh"
#include "../core/RadiationPattern_if_t.hh"

// TODO: CHECK
#include "../core/Coordinate_t.hh"
using namespace Scnsl::Core;

namespace Scnsl { namespace antennaModels {

class SCNSL_EXPORT PatchPattern_t:
        public Scnsl::Core::RadiationPattern_if_t

{
public:
    typedef Scnsl::Core::position_t position_t;
    typedef Scnsl::Core::node_properties_t node_properties_t;

    /// @brief Constructor for patch antenna pattern (Phi versor not yet implemented).
    /// @param vTheta versor (cone axis).
    /// @param active is true if the antenna is active at startup.
    /// @param firstBeamwidth is the smaller cone aperture, angular distance between vTheta and the first cone edge.
    /// @param firstGain is the gain in dB within the smaller cone aperture.
    /// @param vPhi reference versor for secondary lobe orientation.
    /// @param secondBeamwidth is the second cone aperture, angular distance between vTheta and the second cone edge.
    /// @param secondGain is the gain in dB within the second cone aperture.
    /// @param secondLobes is the number of lobes in the second level, if 1 the side loge rounds all around vTheta axis.
    /// @param thirdBeamwidth is the third cone aperture, angular distance between vTheta and the third cone edge.
    /// @param thirdGain is the gain in dB towards other direction.
    /// @param thirdLobes is the number of lobes in the third level, if 1 the side loge rounds all around vTheta axis.
    PatchPattern_t(const Coordinate_t & vTheta,
                   const bool active,
                   const double firstBeamwidth,
                   const double firstGain = 1.0,
                   const Coordinate_t & vPhi = Coordinate_t(false, 0, 0, 0),
                   const double secondBeamwidth = -1,
                   const double secondGain = 0.5,
                   const double secondLobes = 1,
                   const double thirdBeamwidth = -1,
                   const double thirdGain = 0.01,
                   const double thirdLobes = 1);

    virtual
    ~PatchPattern_t();

    PatchPattern_t(const PatchPattern_t & other);
    PatchPattern_t & operator =(PatchPattern_t other);
    void swap(PatchPattern_t & other);
    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const override;

    virtual
    /// @brief compute and return the effective gain value used for the communication
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const override;

private:
    double _firstGain;
    double _secondGain;
    double _thirdGain;
    double _firstBeamwidth;
    double _secondBeamwidth;
    double _thirdBeamwidth;
    double _secondLobes;
    double _thirdLobes;
    double _nLobes;
};

}}//Scnsl::antennaModels


#endif // PATCH_T_HH
