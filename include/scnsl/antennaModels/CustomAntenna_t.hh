// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_ANTENNAMODELS_CUSTOM_T_HH
#define SCNSL_ANTENNAMODELS_CUSTOM_T_HH

#include "../core/RadiationPattern_if_t.hh"

struct inputPlaneElement
{
    double Angle;
    double Gh;
    double Gv;
    double Gt;
};

struct inputGainElement
{
    double Gh;
    double Gv;
    double Gt;
};


namespace Scnsl { namespace antennaModels {

/// @brief Model of a custom antenna.
class SCNSL_EXPORT CustomAntenna_t:
        public Scnsl::Core::RadiationPattern_if_t
{
public:

    /// @brief Constructor.
    /// @param name of the antenna.
    /// @param vTheta versor.
    /// @param vPhi versor.
    /// @param vPol the polarization versor expressed in (theta,phy) angles.
    /// @param active is true if the antenna is active at startup.
    /// @param radiationFile is the parsed .rad file associate to this antenna.
    CustomAntenna_t(const std::string name,
                    const Coordinate_t vTheta,
                    const Coordinate_t vPhi,
                    const Coordinate_t vPol,
                    const bool active,
                    const std::string radiationFile);

    virtual ~CustomAntenna_t();
    CustomAntenna_t(const CustomAntenna_t & other);
    CustomAntenna_t & operator =(CustomAntenna_t other);
    void swap(CustomAntenna_t & other);

    virtual
    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const override;

    virtual
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const override;

private:

    std::string _name;
    std::string _radFile;

    // ------ derived from the rad file
    std::string _mtxType;
    // specific for per-plane pattern
    std::vector<inputPlaneElement> _pnPlane;
    std::vector<inputPlaneElement> _tpPlane;
    std::vector<inputPlaneElement> _tnPlane;
    // specific for matrix-based
    std::size_t _totTheta;
    std::size_t _totPhi;
    std::vector<double> _thetaValues;
    std::vector<double> _phiValues;
    std::vector<std::vector<inputGainElement> > _patternMatrix;

    double computeOrthogonalPlaneGain(const double theta,
                                      const double phi,
                                      const double plf,
                                      const Coordinate_t & inputRadiation) const;

    double computeCompletePatternGain(const double theta,
                                      const double phi,
                                      const double plf,
                                      const Coordinate_t & inputRadiation) const;

};

} } // Scnsl::antennaModels

#endif // SCNSL_ANTENNAMODELS_CUSTOM_T_HH
