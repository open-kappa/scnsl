// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_ANTENNA_MODELSCONEPATTERN_T_HH
#define SCNSL_ANTENNA_MODELSCONEPATTERN_T_HH

#include <systemc>

#include "../core/data_types.hh"
#include "../core/RadiationPattern_if_t.hh"

namespace Scnsl { namespace antennaModels {

class SCNSL_EXPORT ConePattern_t:
        public Scnsl::Core::RadiationPattern_if_t
{
public:
    typedef Scnsl::Core::position_t position_t;
    typedef Scnsl::Core::node_properties_t node_properties_t;

    /// @brief Constructor for cone pattern.
    /// @param vTheta versor (cone axis).
    /// @param active is true if the antenna is active at startup.
    /// @param coneGain is the gain in dB within the cone aperture (-inf towards all the other direction).
    /// @param beamwidth is the cone aperture, angular distance between vTheta and the cone edge.
    ConePattern_t(const Coordinate_t vTheta,
                  const bool active,
                  const double coneGain = 0.0,
                  const double beamwidth = 90);
    virtual
    ~ConePattern_t();

    ConePattern_t(const ConePattern_t & other);
    ConePattern_t & operator =(ConePattern_t other);
    void swap(ConePattern_t & other);

    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const override;

    virtual
    /// @brief compute and return the effective gain value used for the communication
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const override;

private:
    double _beamwidth;

};
}}//Scnsl::antennaModels

#endif // CONEPATTERN_T_HH
