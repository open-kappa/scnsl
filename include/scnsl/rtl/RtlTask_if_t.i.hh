// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_RTL_RTLTASK_IF_T_I_HH
#define SCNSL_RTL_RTLTASK_IF_T_I_HH



/// @file
/// Basic rtl task interface.

#include <sstream>
#include "RtlTask_if_t.hh"
#include "RtlTaskProxy_t.hh"


namespace Scnsl { namespace Rtl {


// ////////////////////////////////////////////////////////////////
// Destructor.
// ////////////////////////////////////////////////////////////////

template< const int PACKET_SIZE >
RtlTask_if_t< PACKET_SIZE >::~RtlTask_if_t()
{
    // ntd
}


// ////////////////////////////////////////////////////////////////
// Implemented support methods for taskporxies.
// ////////////////////////////////////////////////////////////////

template< const int PACKET_SIZE >
typename RtlTask_if_t< PACKET_SIZE >::delete_ports_t
RtlTask_if_t< PACKET_SIZE >::createAndBoundPorts(
        sc_core::sc_port_base * & inPacket,
        sc_core::sc_port_base * & outPacket,
        RtlTaskProxy_t * tp,
        std::string bindIdentifier )

{
    SCNSL_TRACE_DBG( 5, "<> createAndBoundPorts()." );

    if ( _bounded_proxies >= _PROXIES_NUMBER )
    {
        throw std::logic_error( "Bounding too much proxies." );
    }

    // Creating proxy ports:
    // creaed here since their type depends on PACKET_SIZE
    sc_core::sc_out< rtl_packet_t > * in = new sc_core::sc_out< rtl_packet_t >( "inputPacket" );
    sc_core::sc_in< rtl_packet_t > * out = new sc_core::sc_in< rtl_packet_t >( "outputPacket" );

    inPacket = in;
    outPacket = out;

    Scnsl::Core::IdentifierNumber_t newId = Scnsl::Core::IdentifierNumber_t(_bounded_proxies);
    std::string newIdKey;
    std::stringstream ss;
    ss << newId;
    ss >> newIdKey;

    if (bindIdentifier != "") newIdKey = bindIdentifier;

    if(_bindIdMap.find(newIdKey) != _bindIdMap.end())
    {
        throw std::logic_error( "This bind Identifier is already used. Please check bsb bind Identifier attribute." );
    }
    _proxies[newIdKey] = tp;

    sc_core::sc_vector_base::size_type index =
            static_cast<sc_core::sc_vector_base::size_type>(newId);
    // Bounding proxy ports:
    (*in)( _inputPacketSignal[ index ] );
    (*out)( _outputPacketSignal[ index ] );

    // Bounding task ports:
    this->inputPacket[ index ]( _inputPacketSignal[ index ] );
    this->outputPacket[ index ]( _outputPacketSignal[ index ] );

    carrier[ index ]( _carrierSignal[ index ] );
    tp->carrier( _carrierSignal[ index ] );
    inputPacketSize[ index ]( _inputPacketSizeSignal[ index ] );
    tp->inputPacketSize( _inputPacketSizeSignal[ index ] );
    inputPacketLabel[ index ]( _inputPacketLabelSignal[ index ] );
    tp->inputPacketLabel( _inputPacketLabelSignal[ index ] );
    inputPacketPower[ index ]( _inputPacketPowerSignal[ index ] );
    tp->inputPacketPower( _inputPacketPowerSignal[ index ] );
    newInputPacket[ index ]( _newInputPacketSignal[ index ] );
    tp->newInputPacket( _newInputPacketSignal[ index ] );

    packetSendCompleted[ index ]( _packetSendCompletedSignal[ index ] );
    tp->packetSendCompleted( _packetSendCompletedSignal[ index ] );
    outputPacketSize[ index ]( _outputPacketSizeSignal[ index ] );
    tp->outputPacketSize( _outputPacketSizeSignal[ index ] );
    outputPacketLabel[ index ]( _outputPacketLabelSignal[ index ] );
    tp->outputPacketLabel( _outputPacketLabelSignal[ index ] );
    newOutputPacket[ index ]( _newOutputPacketSignal[ index ] );
    tp->newOutputPacket( _newOutputPacketSignal[ index ] );


    // Updating bounded proxies:
    ++ _bounded_proxies;
    _bindIdMap[newIdKey] = newId;

    return delete_ports;
}

template< const int PACKET_SIZE >
void RtlTask_if_t< PACKET_SIZE >::createInternalPacket(
        Packet_t & p,
        sc_core::sc_port_base * outPacket,
        const counter_t outPacketSize )
{
    SCNSL_TRACE_DBG( 5, "<> createInternalPacket()." );

    // This dynamic cast should never fail.
    // sc_core::sc_in< rtl_packet_t > * pp =
    // dynamic_cast< sc_core::sc_in< rtl_packet_t > * >( outputPacket );
    // Thus, it is replaced with a static cast:
    sc_core::sc_in< rtl_packet_t > * pp =
            static_cast< sc_core::sc_in< rtl_packet_t > * >( outPacket );

    rtl_packet_to_buffer( pp->read(), p, outPacketSize );
}

template< const int PACKET_SIZE >
void RtlTask_if_t< PACKET_SIZE >::writeInternalPacket(
        Packet_t & p,
        sc_core::sc_port_base * inPacket )
{
    SCNSL_TRACE_DBG( 5, "<> writeInternalPacket()." );

    // This dynamic cast should never fail.
    // sc_core::sc_in< rtl_packet_t > * pp =
    // dynamic_cast< sc_core::sc_in< rtl_packet_t > * >( outputPacket );
    // Thus, it is replaced with a static cast:
    sc_core::sc_out< rtl_packet_t > * pp =
            static_cast< sc_core::sc_out< rtl_packet_t > * >( inPacket );

    rtl_packet_t rtlp;
    const auto packet =  dynamic_cast<Scnsl::Core::SimpleProtocolPacket_t*>(p.getPayload());
    assert(packet != nullptr);
    byte_t * b = packet->getInnerBuffer();
    buffer_to_rtl_packet( b, rtlp, p.getSizeInBytes() );

    pp->write( rtlp );
}


// ////////////////////////////////////////////////////////////////
// General support methods.
// ////////////////////////////////////////////////////////////////


template< const int PACKET_SIZE >
void RtlTask_if_t< PACKET_SIZE >::rtl_packet_to_buffer( const rtl_packet_t & p,
                                                        Packet_t & pck,
                                                        const size_t s )
{
    const size_t ss = (s%8) + (s/8);
    byte_t * buffer = new byte_t[ ss ];
    for ( unsigned int i = 0; i < ss; ++i )
    {
        buffer[ i ] = byte_t(p.range( int(PACKET_SIZE - i * 8 -1),
                                      int(PACKET_SIZE - (i + 1) * 8 )).to_int());
    }

    auto packet = new Scnsl::Core::SimpleProtocolPacket_t();
    packet->setPayload( buffer, s );
    pck.setPayload(*packet);
    delete [] buffer;
}

template< const int PACKET_SIZE >
void RtlTask_if_t< PACKET_SIZE >::buffer_to_rtl_packet( const byte_t * buffer,
                                                        rtl_packet_t & p,
                                                        const size_t s )
{
    for ( unsigned int i = 0; i < s; ++i )
    {
        p.range( int(PACKET_SIZE - i * 8 -1),
                 int(PACKET_SIZE - (i + 1) * 8) ) =
                static_cast<unsigned int>(buffer[ i ]);
    }
}


// ////////////////////////////////////////////////////////////////
// Protected methods.
// ////////////////////////////////////////////////////////////////

template< const int PACKET_SIZE >
RtlTask_if_t< PACKET_SIZE >::RtlTask_if_t(
        const sc_core::sc_module_name module_name,
        const task_id_t id,
        Node_t * n,
        const size_t proxies )
    :
      // Parent:
      RtlTask_base_if_t( module_name, id, n, proxies ),
      // Ports:
      inputPacket("inputPacket", proxies),
      outputPacket("outputPacket", proxies),
      // Internal fields:
      _inputPacketSignal("_inputPacketSignal", proxies),
      _outputPacketSignal("_outputPacketSignal", proxies),
      _carrierSignal("_carrierSignal", proxies),
      _inputPacketSizeSignal("_inputPacketSizeSignal", proxies),
      _inputPacketLabelSignal("_inputPacketLabelSignal", proxies),
      _inputPacketPowerSignal("_inputPacketPowerSignal", proxies),
      _newInputPacketSignal("_newInputPacketSignal", proxies),
      _packetSendCompletedSignal("_packetSendCompletedSignal", proxies),
      _outputPacketSizeSignal("_outputPacketSizeSignal", proxies),
      _outputPacketLabelSignal("_outputPacketLabelSignal", proxies),
      _newOutputPacketSignal("_newOutputPacketSignal", proxies)
{
    // ntd
}

template< const int PACKET_SIZE >
void RtlTask_if_t< PACKET_SIZE >::delete_ports(
        sc_core::sc_port_base * p1, sc_core::sc_port_base * p2 )
{
    sc_core::sc_in< rtl_packet_t > * in =
            static_cast<sc_core::sc_in< rtl_packet_t > *>(p1);

    sc_core::sc_out< rtl_packet_t > * out =
            static_cast<sc_core::sc_out< rtl_packet_t > *>(p2);

    delete in;
    delete out;
}

} }



#endif
