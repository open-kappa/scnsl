// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_RTL_RTLTASK_IF_T_HH
#define SCNSL_RTL_RTLTASK_IF_T_HH



/// @file
/// Basic rtl task interface.

#include "../scnslConfig.hh"
#include "../core/Packet_t.hh"
#include "../core/Node_t.hh"
#include "rtl_data_types.hh"
#include "RtlTask_base_if_t.hh"

namespace Scnsl { namespace Rtl {

    /// @brief A basic RTL task.
    /// SystemC standard forces to allocate ports within a sc_module.
    /// Thus, we have to create arrays of ports, in order to allow
    /// multiple taskproxies bindings.
    ///
    /// Design patterns:
    /// - Interface.
    /// - Non copiable.
    /// - Non assignable.
    /// - Non Template Base Class.
    ///
    /// @tparam PACKET_SIZE The maximum rtl packet size in bits.
    ///        The type is int since the OSCII SystemC standard assumes this type.
    ///
    template< const int PACKET_SIZE >
    class RtlTask_if_t :
        public RtlTask_base_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The RTL packet type exchanged with the ports.
        typedef typename Scnsl::Rtl::rtl_packet_type_helper_t< PACKET_SIZE >::rtl_packet_t rtl_packet_t;

        /// @brief The byte type.
        typedef Scnsl::Core::byte_t byte_t;

        /// @brief The internal packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

        /// @brief The node type.
        typedef Scnsl::Core::Node_t Node_t;

		/// @brief The label type.
		typedef Scnsl::Core::label_t label_t;

        /// @brief The power type.
        typedef Scnsl::Core::power_t power_t;

        //@}


        /// @brief Ports.
        //@{

        /// @brief The input packet.
        sc_core::sc_vector< sc_core::sc_in< rtl_packet_t > > inputPacket;

        /// @brief The output packet port.
        sc_core::sc_vector< sc_core::sc_out< rtl_packet_t > > outputPacket;

        //@}

        /// @brief Impure virtual destructor.
        virtual
        ~RtlTask_if_t() = 0;


        /// @brief Implemented support methods for taskporxies.
        //@{

        virtual
        delete_ports_t createAndBoundPorts(sc_core::sc_port_base * & inputPacket,
                                 sc_core::sc_port_base * & outputPacket,
                                 RtlTaskProxy_t * tp,
                                 std::string bindIdentifier);

        virtual
        void createInternalPacket( Packet_t & p,
                                   sc_core::sc_port_base * outputPacket,
                                   const counter_t outputPacketSize );

        /// @brief Method used to delete ports created for the taskproxy.
        ///
        /// @param p1 The input port to be deleted.
        /// @param p2 The output port to be deleted.
        ///
        ///
        static
        void delete_ports( sc_core::sc_port_base * p1, sc_core::sc_port_base * p2 );

        virtual
        void writeInternalPacket( Packet_t & p,
                                  sc_core::sc_port_base * inputPacket );

        //@}


        /// @brief General support methods.
        //@{

        /// @brief Converts a rtl packet to a buffer.
        ///
        /// @param p The packet.
        /// @param pck The packet into which fill the buffer.
        /// @param s The buffer size.
        ///
        static
        void rtl_packet_to_buffer( const rtl_packet_t & p,
                                   Packet_t & pck,
                                   const size_t s );

        /// @brief Converts a buffer into a rtl packet.
        ///
        /// @param buffer The buffer.
        /// @param p The packet.
        /// @param s The bufffer size.
        ///
        static
        void buffer_to_rtl_packet( const byte_t * buffer,
                                   rtl_packet_t & p,
                                   const size_t s );

        //@}

    protected:

        /// @brief Constructor.
        ///
        /// @param name This module name.
        /// @param id This module unique ID.
        /// @param n The node to which the task belongs.
        /// @param proxies The number of total taskproxies associate with this task.
        /// @throw std::invalid_argument If proxiesx == 0.
        ///
        RtlTask_if_t( const sc_core::sc_module_name name,
                      const task_id_t id,
                      Node_t * n,
                      const size_t proxies );

        /// @brief Signals for inputPacket bindings.
        sc_core::sc_vector< sc_core::sc_signal< rtl_packet_t > > _inputPacketSignal;

        /// @brief Signals for outputPacket bindings.
        sc_core::sc_vector< sc_core::sc_signal< rtl_packet_t > > _outputPacketSignal;

		/// @brief Signals for carrier bindings.
        sc_core::sc_vector< sc_core::sc_signal< carrier_t > > _carrierSignal;

		/// @brief Signals for size bindings.
        sc_core::sc_vector< sc_core::sc_signal< size_t > > _inputPacketSizeSignal;

		/// @brief Signals for label bindings.
        sc_core::sc_vector< sc_core::sc_signal< label_t > > _inputPacketLabelSignal;

        /// @brief Signals for label bindings.
        sc_core::sc_vector< sc_core::sc_signal< power_t > > _inputPacketPowerSignal;

		/// @brief Signals for carrier bindings.
        sc_core::sc_vector< sc_core::sc_signal< counter_t > > _newInputPacketSignal;

		/// @brief Signals for carrier bindings.
        sc_core::sc_vector< sc_core::sc_signal< counter_t > > _packetSendCompletedSignal;

		/// @brief Signals for carrier bindings.
        sc_core::sc_vector< sc_core::sc_signal< size_t > > _outputPacketSizeSignal;

		/// @brief Signals for label bindings.
        sc_core::sc_vector< sc_core::sc_signal< label_t > > _outputPacketLabelSignal;

		/// @brief Signals for carrier bindings.
        sc_core::sc_vector< sc_core::sc_signal< counter_t > > _newOutputPacketSignal;


    private:

        /// @brief Disabled copy constructor.
        RtlTask_if_t( const RtlTask_if_t & );

        /// @brief Disabled assignement operator.
        RtlTask_if_t & operator = ( const RtlTask_if_t & );

    };

  } }


#include "RtlTask_if_t.i.hh"


#endif
