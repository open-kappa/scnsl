// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_RTLTASKPROXY_T_HH
#define SCNSL_CORE_RTLTASKPROXY_T_HH



/// @file
/// RTL taskproxy.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Packet_t.hh"
#include "../core/TaskProxy_if_t.hh"
#include "../setup/setup_data_types.hh"
#include "../core/data_types.hh"
#include "RtlTask_base_if_t.hh"
#include "rtl_data_types.hh"


namespace Scnsl { namespace Core {

  class Task_if_t;

} }

namespace Scnsl { namespace Setup {

  class BindSetup_base_t;

} }

namespace Scnsl { namespace Rtl {

  /// @brief An RTL task proxy.
  ///
  /// Design patterns:
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT RtlTaskProxy_t :
        public Scnsl::Core::TaskProxy_if_t
  {
  public:

      /// @name Traits.
      //@{

      /// @brief The size type.
      typedef Scnsl::Core::size_t size_t;

      /// @brief The counter type.
      typedef Scnsl::Core::counter_t counter_t;

      /// @brief The carrier flag type.
      typedef Scnsl::Core::carrier_t carrier_t;

      /// @brief The byte type.
      typedef Scnsl::Core::byte_t byte_t;

      /// @brief The label type.
      typedef Scnsl::Core::label_t label_t;

      /// @brief The power type.
      typedef Scnsl::Core::power_t power_t;

      //@}

      SC_HAS_PROCESS( RtlTaskProxy_t );

      /// @name Ports for task inputs.
      //@{

      /// @brief The sensing of the channel.
      sc_core::sc_out< carrier_t > carrier;

      /// @brief Signals the size of the input packet, in bits.
      sc_core::sc_out< size_t > inputPacketSize;

      /// @brief A flag to warn the node that there is a new packet incoming.
      sc_core::sc_out< counter_t > newInputPacket;

      /// @brief Signals the label of the input packet.
      sc_core::sc_out< label_t > inputPacketLabel;

      /// @brief Signals the power of the input packet.
      sc_core::sc_out< power_t > inputPacketPower;

      /// @brief The input packet.
      sc_core::sc_port_base * inputPacket;

      //@}


      /// @name Ports for task outputs.
      //@{

      /// @brief Signals the size of the output packet, in bits.
      sc_core::sc_in< size_t > outputPacketSize;

      /// @brief A flag to warn that there is a new packet outgoing.
      sc_core::sc_in< counter_t > newOutputPacket;

	  /// @brief Signals the label of the output packet.
      sc_core::sc_in< label_t > outputPacketLabel;

      /// @brief A flag used to signal the transmission completion.
      sc_core::sc_out< counter_t > packetSendCompleted;

      /// @brief The output packet port.
      sc_core::sc_port_base * outputPacket;

      //@}





      /// @brief Constructor.
      ///
      /// @param name This module name.
      /// @param t The connected task.
      /// @param bindIdentifier The id for the task proxy.
      /// @param ch The associated channel.
      /// @throw std::invalid_argument The task is not child of RtlTask_if_t.
      /// @throw std::logic_error The task has already bounded all available proxies.
      ///
      RtlTaskProxy_t( const sc_core::sc_module_name name,
                      Scnsl::Core::Task_if_t * t,
                      std::string bindIdentifier,
                      Scnsl::Core::Channel_if_t * ch );

      /// @brief Virtual destructor.
      virtual
      ~RtlTaskProxy_t();

      /// @name Reimplemented interface methods.
      //@{

      virtual
      void setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c ) override;

      virtual
      errorcode_t receive( const Scnsl::Core::Packet_t & p ) override;

      //@}

  protected:

      /// @brief Variable used to store the new carrier value.
      carrier_t _new_carrier;

      /// @brief The packet to be forwarded to the task.
      Scnsl::Core::Packet_t _receivedPacket;

      /// @brief The packet sent by the task.
      Scnsl::Core::Packet_t _outgoingPacket;

      /// @brief Event to signal to change the carrier.
      sc_core::sc_event _changeCarrier;

      /// @brief Event to signal to forward a the packet to the task.
      sc_core::sc_event _forwardPacket;

      /// @brief The counter for signaling the send completion.
      counter_t _packet_send_completed;

      /// @brief The counter for signaling a new input packet.
      counter_t _new_input_packet;

      /// @brief The pointer to function to delete the ports.
      RtlTask_base_if_t::delete_ports_t _delete_ports;

  private:

      /// @name Processes.
      //@{

      /// @brief Process to transmit task packets.
      void _readingProcess();

      /// @brief Process to forward packets to the task.
      void _forwardProcess();

      void _changeCarrierProcess();

      //@}


      /// @brief Disabled copy constructor.
      RtlTaskProxy_t( const RtlTaskProxy_t & );

      /// @brief Disabled assignement operator.
      RtlTaskProxy_t & operator = ( const RtlTaskProxy_t & );

  };

} }



#endif
