// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_RTL_RTLTASK_BASE_IF_T_HH
#define SCNSL_RTL_RTLTASK_BASE_IF_T_HH



/// @file
/// A base class for basic RTL tasks.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/Task_if_t.hh"
#include "../setup/setup_data_types.hh"

namespace Scnsl { namespace Core {

  class Packet_t;

} }

namespace Scnsl { namespace Rtl {

	class RtlTaskProxy_t;

} }

namespace Scnsl { namespace Rtl {

    /// @brief A base class for basic RTL tasks.
    /// SystemC standard forces to allocate ports within a sc_module.
    /// Thus, we have to create arrays of ports, in order to allow
    /// multiple proxies bindings.
    ///
    /// Design patterns:
    /// - Interface.
    /// - Non copiable.
    /// - Non assignable.
    /// - Non Template Base Class.
    ///
    class SCNSL_EXPORT RtlTask_base_if_t :
        public Scnsl::Core::Task_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The size type.
        typedef Scnsl::Core::size_t size_t;

        /// @brief The counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief The carrier flag type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief Convenience function pointer type.
        typedef void (* delete_ports_t)( sc_core::sc_port_base *, sc_core::sc_port_base *);

		/// @brief The label type.
		typedef Scnsl::Core::label_t label_t;

        /// @brief The label type.
        typedef Scnsl::Core::power_t power_t;

        //@}

        /// @name Ports for inputs.
        //@{

        /// @brief The sensing of the channel.
        sc_core::sc_vector< sc_core::sc_in< carrier_t > > carrier;

        /// @brief Signals the size of the input packet, in bits.
        sc_core::sc_vector<sc_core::sc_in< size_t > > inputPacketSize;

        /// @brief Signals the label of the input packet.
        sc_core::sc_vector<sc_core::sc_in< label_t > > inputPacketLabel;

        /// @brief Signals the power of the input packet.
        sc_core::sc_vector<sc_core::sc_in< power_t > > inputPacketPower;

        /// @brief A flag to warn the node that there is a new packet incoming.
        sc_core::sc_vector<sc_core::sc_in< counter_t > > newInputPacket;


        //@}


        /// @name Ports for outputs.
        //@{

        /// @brief Signals the size of the output packet, in bits.
        sc_core::sc_vector< sc_core::sc_out< size_t > > outputPacketSize;

        /// @brief A flag to warn that there is a new packet outgoing.
        sc_core::sc_vector< sc_core::sc_out< counter_t > > newOutputPacket;

        /// @brief Signals the label of the output packet.
        sc_core::sc_vector< sc_core::sc_out< label_t > > outputPacketLabel;

        /// @brief A flag used to signal the transmission completion.
        sc_core::sc_vector< sc_core::sc_in< counter_t > > packetSendCompleted;

        //@}



        /// @brief Impure virtual destructor.
        virtual
        ~RtlTask_base_if_t() = 0;

        /// @name Interface methods.
        //@{

        /// @brief Gets the number of associated proxies.
        virtual
        size_t getProxiesNumber()
            const override;

        /// @brief Gets the number of already bounded proxies.
        virtual
        size_t getBoundedProxiesNumber()
            const override;

        ///@}

        /// @brief Support methods for taskporxies.
        //@{

        /// @brief Creates and bounds packet ports of a taskproxy.
        ///
        /// @param inputPacket The port for task input packets.
        /// @param outputPacket The port for task output packets.
        /// @param tp The taskproxy to bound.
	/// @param bindIdentifier The bind identifier.
        /// @return A pointer to function, to be called to delete the created ports.
        /// @throw std::logic_error Bounding too much taskproxies.
        ///
        virtual
        delete_ports_t createAndBoundPorts( sc_core::sc_port_base * & inputPacket,
                                            sc_core::sc_port_base * & outputPacket,
                                            RtlTaskProxy_t * tp,
                                            std::string bindIdentifier
					  ) = 0;


        /// @brief Compiles the fields of a packet.
        ///
        /// @param p The packet to be compiled.
        /// @param outputPacket The port from which to read the packet.
        /// @param outputPacketSize The transmitted packet size in bits.
		///
        virtual
        void createInternalPacket( Scnsl::Core::Packet_t & p,
                                   sc_core::sc_port_base * outputPacket,
                                   const counter_t outputPacketSize ) = 0;

        /// @brief Writes a packet on the output port.
        ///
        /// @param p the packet to be written.
        /// @param inputPacket The port where to write the packet.
        ///
        virtual
        void writeInternalPacket( Scnsl::Core::Packet_t & p,
                                  sc_core::sc_port_base * inputPacket ) = 0;

        //@}

    protected:

        /// @brief Constructor.
        ///
        /// @param name This module name.
        /// @param id This module unique ID.
        /// @param n The node on which the task is placed..
        /// @param proxies The number of total proxies associate with this task.
        /// @throw std::invalid_argument If proxies == 0.
        ///
        RtlTask_base_if_t( const sc_core::sc_module_name name,
                           const task_id_t id,
                           Scnsl::Core::Node_t * n,
                           const size_t proxies );


        /// @brief The total number of proxies associated with this task.
        const size_t _PROXIES_NUMBER;

        /// @brief The currently bounded proxies.
        size_t _bounded_proxies;

    private:

        /// @brief Disabled copy constructor.
        RtlTask_base_if_t( const RtlTask_base_if_t & );

        /// @brief Sisabled assignemnt operator.
        RtlTask_base_if_t & operator = ( const RtlTask_base_if_t & );
    };

  } }



#endif
