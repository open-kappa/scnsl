// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_RTL_RTL_DATA_TYPES_T_HH
#define SCNSL_RTL_RTL_DATA_TYPES_T_HH

/// @file
/// RTL related base types.

#include <systemc>
#include "../scnslConfig.hh"

namespace Scnsl { namespace Rtl {


  /// @name RTL-related types.
  //@{

  /// @brief A typedef template helper struct.
  template< int i >
  struct rtl_packet_type_helper_t
  {
      /// @brief The RTL packet type exchanged with the ports.
      typedef sc_dt::sc_bv< i > rtl_packet_t;
  };

  //@}


} }


#endif
