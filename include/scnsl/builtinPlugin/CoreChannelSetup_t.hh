// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_CORECHANNELSETUP_T_HH
#define SCNSL_BUILTIN_PLUGIN_CORECHANNELSETUP_T_HH



/// @file
/// Core channel setup.

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../setup/ChannelSetup_base_t.hh"


namespace Scnsl { namespace BuiltinPlugin {

    /// @brief Core channel setup.
    ///
    class SCNSL_EXPORT CoreChannelSetup_t:
        public Scnsl::Setup::ChannelSetup_base_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The capacity type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief The capacity type.
        typedef Scnsl::Core::propagation_t propagation_t;

        /// @brief The delay type.
        typedef Scnsl::Core::delay_t delay_t;

        /// @brief The counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief The type of channel.
        enum channel_type_t
        {
            UNIDIRECTIONAL,
            FULL_DUPLEX,
            HALF_DUPLEX,
            SHARED,
            DELAYED_SHARED
        };

        /// @brief The propagation speed.
        enum propagation_speed_t
        {
            /// For custom speeds:
            CUSTOM_SPEED,
			WATER_SOUND_SPEED,
            AIR_SOUND_SPEED,
            EM_SPEED,
            OPTICAL_FIBER_SPEED,
            COPPER_SPEED
        };

        //@}

        /// @brief Constructor.
        CoreChannelSetup_t();

        /// @brief Destructor.
        virtual
        ~CoreChannelSetup_t();

        /// @brief The type of channel to be created.
        channel_type_t channel_type;

        /// @brief The capacity of the channel to be created.
        bitrate_t capacity;

        /// @brief The capacity of the channel in the opposite direction.
        /// Used only for full duplex channel.
        bitrate_t capacity2;

        /// @brief The internal delay of the channel.
        delay_t delay;

        /// @brief The propagation speed.
        /// It is defined in meters per second.
        propagation_speed_t propagation;

        /// @brief  Eventual custom propagation speed.
        /// Valid, with default CUSTOM_SPEED value for propagation field.
        propagation_t custom_propagation;

        /// @brief The total number of nodes that will be binded.
        /// Used by shared and delayed_shared models.
        counter_t nodes_number;

        /// @brief The degradation exponent for signal transmission.
        double alpha;

        /// @brief To be set non-zero if antenna to antenna commuication is being considered.
        /// In this case degradation formula will follow the Friis transmission equation.
        /// radio_frequency is expressed in MHz
        double radio_frequency;

	private:
		CoreChannelSetup_t(const CoreChannelSetup_t&);
		CoreChannelSetup_t & operator =(const CoreChannelSetup_t&);
    };

  } }



#endif
