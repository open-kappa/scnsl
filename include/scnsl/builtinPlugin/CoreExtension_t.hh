// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_COREEXTENSION_T_HH
#define SCNSL_BUILTIN_PLUGIN_COREEXTENSION_T_HH



/// @file
/// The extension to manage the builtin models.

#include "../scnslConfig.hh"
#include "../setup/Extension_if_t.hh"

namespace Scnsl { namespace Core {

        class QueueCommunicator_t;

} }

namespace Scnsl { namespace BuiltinPlugin {

        /// @brief The extension to manage the builtin models.
        ///
        /// Design patterns:
        /// - Non copiable.
        /// - Non assignable.
        ///
        class SCNSL_EXPORT CoreExtension_t:
        public Scnsl::Setup::Extension_if_t
        {
        public:

            /// @brief The ChannelSetup_base type.
            typedef Scnsl::Setup::ChannelSetup_base_t ChannelSetup_base_t;

            /// @brief The TaskSetup_base type.
            typedef Scnsl::Setup::TaskSetup_base_t TaskSetup_base_t;

            /// @brief The CommunicatorSetup_base type
            typedef Scnsl::Setup::CommunicatorSetup_base_t CommunicatorSetup_base_t;

            /// @brief The TracingSetup_base type
            typedef Scnsl::Setup::TracingSetup_base_t TracingSetup_base_t;

            /// @brief The TopologySetup_base type
            typedef Scnsl::Setup::TopologySetup_base_t TopologySetup_base_t;

            /// @brief The propagation type
            typedef Scnsl::Core::propagation_t propagation_t;

            /// @brief Constructor.
            CoreExtension_t();

            /// @brief Virtual destructor.
            virtual
            ~CoreExtension_t();


            /// @brief Creation methods.
            //@{

            virtual
            Scnsl::Core::Channel_if_t * createChannel( const ChannelSetup_base_t & s ) override;

            virtual
            Scnsl::Core::Task_if_t * createTask( const TaskSetup_base_t & s ) override;

            virtual
            Scnsl::Core::Communicator_if_t * createCommunicator( const CommunicatorSetup_base_t & s ) override;

            virtual
            Scnsl::Tracing::Tracer_t * createTracer( const TracingSetup_base_t & s ) override;

            virtual
            Scnsl::Tracing::Formatter_if_t * createFormatter( const TracingSetup_base_t & s ) override;

            virtual
            Scnsl::Tracing::Filter_if_t * createFilter( const TracingSetup_base_t & s ) override;

            virtual
            void createTopology( const TopologySetup_base_t & s ) override;

            virtual
            Scnsl::Core::TaskProxy_if_t * createTaskProxy(
                Scnsl::Core::Task_if_t * t,
                const Scnsl::Setup::BindSetup_base_t & s,
                Scnsl::Core::Channel_if_t * ch ) override;

            //@}


        protected:

            Scnsl::Core::QueueCommunicator_t * createQueue( const CommunicatorSetup_base_t & s );

        private:

            /// @brief Disabled copy constructor.
            CoreExtension_t( const CoreExtension_t & );

            /// @brief Disabled assignment operator.
            CoreExtension_t & operator = ( const CoreExtension_t & );

            /// @brief Mesh topology creation.
            void createMeshTopology( const TopologySetup_base_t &  );

        };

} }



#endif
