// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_CORETASKSETUP_T_HH
#define SCNSL_BUILTIN_PLUGIN_CORETASKSETUP_T_HH



/// @file
/// Core task setup.

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../setup/TaskSetup_base_t.hh"


namespace Scnsl { namespace BuiltinPlugin {

    /// @brief Core task setup.
    ///
    class SCNSL_EXPORT CoreTaskSetup_t:
        public Scnsl::Setup::TaskSetup_base_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The size type.
        typedef Scnsl::Core::size_t size_t;

        /// @brief The label type.
        typedef Scnsl::Core::label_t label_t;


        /// @brief The type of task.
        enum task_type_t
        {
            CBR,
            GILBERT,
            ON_OFF,
			PIT
        };

        //@}

        /// @brief Constructor.
        CoreTaskSetup_t();

        /// @brief Destructor.
        virtual
        ~CoreTaskSetup_t();

		/// @brief Tha packet label
        label_t label;

        /// @brief The type of task to be created.
        task_type_t task_type;

		/// @brief The packet size.
        size_t pktSize;

        /// @brief Time between a packet from another, in ms.
        sc_core::sc_time genTime;

	    /// @brief The transmission period.
        sc_core::sc_time on;

	    /// @brief The pause period.
        sc_core::sc_time off;

	  /// @brief The probability to go in no-send state.
      double alpha;

	  /// @brief The probability to go in send state.
      double beta;

	private:
		CoreTaskSetup_t(const CoreTaskSetup_t &);
		CoreTaskSetup_t operator =(const CoreTaskSetup_t&);
    };

  } }



#endif
