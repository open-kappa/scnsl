// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_CORETRACINGSETUP_T_HH
#define SCNSL_BUILTIN_PLUGIN_CORETRACINGSETUP_T_HH



/// @file
/// Core tracing setup.

#include "../scnslConfig.hh"
#include "../setup/TracingSetup_base_t.hh"

namespace Scnsl { namespace BuiltinPlugin {

    /// @brief Core tracing setup.
    class SCNSL_EXPORT CoreTracingSetup_t:
        public Scnsl::Setup::TracingSetup_base_t
    {
    public:

        /// @brief Constructor.
        CoreTracingSetup_t();

        /// @brief Ddestructor.
        virtual
        ~CoreTracingSetup_t();


        bool print_trace_type;

        bool print_trace_timestamp;

    private:

        /// @brief Disabled copy constructor.
        CoreTracingSetup_t( const CoreTracingSetup_t & );

        /// @brief Disabled assignment operator.
        CoreTracingSetup_t & operator = ( const CoreTracingSetup_t & );

    };

} }



#endif
