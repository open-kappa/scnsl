// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_CORECOMMUNICATORSETUP_T_HH
#define SCNSL_BUILTIN_PLUGIN_CORECOMMUNICATORSETUP_T_HH



/// @file
/// Core communicator setup class.

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Node_t.hh"
#include "../protocols/saboteur/Saboteur_t.hh"
#include "../setup/CommunicatorSetup_base_t.hh"

namespace Scnsl { namespace Core {

    class Node_t;
    class Channel_if_t;

  } }

namespace Scnsl { namespace BuiltinPlugin {

    /// @brief Core communicator setup class.
    class SCNSL_EXPORT CoreCommunicatorSetup_t:
        public Scnsl::Setup::CommunicatorSetup_base_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The size type.
        typedef Scnsl::Core::size_t size_t;

        /// @brief The routing table type.
        typedef Scnsl::Core::RoutingTable_t RoutingTable_t;

        /// @brief The node properties.
        typedef Scnsl::Core::Node_t::node_properties_t node_properties_t;

        /// @brief The bit error rate function for saboteur.
        typedef Scnsl::Protocols::Saboteur::Saboteur_t::bitErrorRateFunction_t
            bitErrorRateFunction_t;

        /// @brief the type of communicator to be created.
        enum type_t
        {
            MAC_802_15_4 = 0,
            ROUTER_COMMUNICATOR = 1,
            QUEUE = 2,
            SABOTEUR = 3,
            LV4_COMMUNICATOR = 4
        };

        /// @brief the type of queue to be created.
        enum queue_type_t
        {
            NONE = 0,
            FIFO = 1,
            PRIORITY = 2
        };

        /// @brief the type of algorithm for the queue
        enum policy_type_t
        {
            STRONG_PRIORITY = 0,
            W_ROUND_ROBIN = 1
        };


        //@}


        /// @brief Constructor.
        CoreCommunicatorSetup_t();


        /// @brief Destructor.
        virtual
        ~CoreCommunicatorSetup_t();


        /// @brief The type of communicator to be created.
        type_t type;


        /// @name Queue-related fields.
        //@{

        /// @brief The type of sender queue to be created.
        queue_type_t queueSend;

        /// @brief The type of receiver queue to be created.
        queue_type_t queueReceive;

        /// @brief The total capacity of the sender queue.
        size_t capacitySend;

        /// @brief The total capacity of the receiver queue.
        size_t capacityReceive;

        /// @brief The policy of the sender queue.
        policy_type_t policySend;

        /// @brief The policy of the receiver queue.
        policy_type_t policyReceive;

        /// @brief The number of sender queues to be created.
        size_t numberSend;

        /// @brief The number of receiver queues to be created.
        size_t numberReceive;

        /// @brief The weight for each sender queue.
        size_t * weightSend;

        /// @brief The weight for each receiver queue.
        size_t * weightReceive;

        /// @brief True if the queue should drop incoming packets when full
        bool dropPacketsWhenFull;

        //@}

        /// @name Protocol-related fields.
        //@{

        /// @brief True if the protocol shall be confirmed.
        bool ack_required;

        /// @brief True if the adreesing mode uses short adresses (8 bits).
        /// False if it uses long addresses (64 bits).
        bool short_addresses;

        /// @brief The host node.
        Scnsl::Core::Node_t * node;

        /// @brief The routing table.
        RoutingTable_t routingTable;

        //@}

        /// @name Saboteur-related fields.
        //@{

        /// @brief The bit error rate function pointer.
        bitErrorRateFunction_t bitErrorRateFunction;

        //@}

    private:

        /// @brief Disabled copy constructor.
        CoreCommunicatorSetup_t( const CoreCommunicatorSetup_t & cs );

        /// @brief Disabled assignment operator.
        CoreCommunicatorSetup_t & operator = ( const CoreCommunicatorSetup_t & cs );

    };

} }



#endif
