// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_BUILTIN_PLUGIN_CORETOPOLOGYSETUP_T_HH
#define SCNSL_BUILTIN_PLUGIN_CORETOPOLOGYSETUP_T_HH



/// @file
/// Core topology setup.

#include "../scnslConfig.hh"
#include "../setup/TopologySetup_base_t.hh"
#include "../setup/ChannelSetup_base_t.hh"
#include "../setup/BindSetup_base_t.hh"
#include "../setup/Scnsl_t.hh"


namespace Scnsl { namespace BuiltinPlugin {

        /// @brief Core topology setup.
        ///
        class SCNSL_EXPORT CoreTopologySetup_t:
        public Scnsl::Setup::TopologySetup_base_t
        {
        public:

            /// @brief Traits.
            //@{

            /// @brief The type of topology
            /// - Case BOTTLENECK: network settings are the following
            ///      N1 ---      --- N2
            ///           |      |
            ///           |      |
            ///           N5 --- N6
            ///           |      |
            ///           |      |
            ///      N3 ---      --- N4
            /// The order of nodes is represented in the schema above.
            /// Channel_list represents the list of channel to be created.
            /// 1st channel is the one between N1 and N2,
            /// 2nd channel is the one between N6 and N2,
            /// 3rd channel is the one between N3 and N5,
            /// 4th channel is the one between N6 and N4,
            /// 5th channel is the one between N5 and N6.
            /// To create a wireless network, Channel_list must contains
            /// in 1st position a multipoint channel.
            /// Over the nodes N5 and N6 there is a router.
            ///
            /// - Case MESH: this topology type includes these network topologies:
            ///   LINE, PARALLELEPIPED and MESH.
            ///
            /// An example of wired LINE topology:
            /// N1 --- N2 --- N3 --- N4
            /// The nodes' order is represented in the schema above.
            /// To have a topology like this use the parameter node_number and use
            /// only one of its coordinates
            /// (e.g. set the z coordiante to 4 and set the others to 1).
            /// After this step, you have to put n-1 not multipoint channels in
            /// channel_list, where n is the coordinate' value not set to 1.
            /// The channels' order is a sequential order:
            /// 1st channel is the one between N1 and N2
            /// 2nd channel is the one between N2 and N3
            /// and so on.
            ///
            /// An example of wired PARALLELEPIPED topology:
            ///      z
            ///     --->
            ///    |  N1 --- N2 ---  N3  --- N4
            ///   y|   |      |       |       |
            ///       N5 --- N6 ---  N7  --- N8
            ///        |      |       |       |
            ///       N9 --- N10 --- N11 --- N12
            ///
            /// The nodes' order is represented in the schema above.
            /// To have a topology like this use the parameter node_number and
            /// set only one of its coordinates to 1.
            /// After this step, you have to put [(n - 1) * m] + [n * (m - 1)]
            /// not multipoint channels in channel_list,
            /// where n and m are the coordinates' values not set to 1.
            /// The channels' order is:
            /// first the z-1 channels on the coordinate z for y times
            /// (i.e. in the example above:
            ///     1st channel is the one between N1 and N2,
            ///     2nd channel is the one between N2 and N3,
            ///     ....
            ///     8nd channel is the one between N10 and N11,
            ///     9nd channel is the one between N11 and N12 )
            /// then, the z channels on the coordinate y for y-1 times
            /// (i.e. in the example above:
            ///     10nd channel is the one between N1 and N5,
            ///     11nd channel is the one between N2 and N6,
            ///     ....
            ///     16nd channel is the one between N7 and N11,
            ///     17nd channel is the one between N8 and N12 )
            ///
            /// An example of wired MESH topology:
            /// this topology is like that above but every node is connected with
            /// all its neighbors in each directions (x,y,z).
            /// To have a topology like this use the parameter node_number and set
            /// all its coordinates' value with a number greater than 1.
            /// The nodes' order is like above but repeated x-1 times.
            /// An example with z=4, y=3 and x=2 :
            ///             x = 0                               x = 1
            ///      z                                    z
            ///     --->                                 --->
            ///    |  N1 --- N2 ---  N3  --- N4      |  N13 --- N14 --- N15 --- N16
            ///   y|   |      |       |       |     y|   |       |       |       |
            ///       N5 --- N6 ---  N7  --- N8         N17 --- N18 --- N19 --- N20
            ///        |      |       |       |          |       |       |       |
            ///       N9 --- N10 --- N11 --- N12        N21 --- N22 --- N23 --- N24
            ///
            /// After this step, you have to put [n * ( k * ( 3 * m - 1) - m ) - k * m]
            /// not multipoint channels in channel_list.
            /// The number k, m and n are the coordinates' values x,y and z.
            /// The channels' order is:
            /// first the z-1 channels on coordinate z for y*x times,
            /// the y-1 channels on coordinate y for x*z times and
            /// then the x-1 channels on coordinate x for z*y times.
            /// (now we have three dimensions!).
            /// (i.e. in the example above:
            ///     1st channel is the one between N1 and N2,
            ///     ....
            ///     9nd channel is the one between N11 and N12,
            ///    10nd channel is the one between N13 and N14,
            ///     ...
            ///    18nd channel is the one between N23 and N24,
            ///    19nd channel is the one between N1 and N5,
            ///     ...
            ///    26nd channel is the one between N8 and N12,
            ///    27nd channel is the one between N13 and N17,
            ///     ...
            ///    34nd channel is the one between N20 and N24.
            ///    35nd channel is the one between N1 and N13.
            ///     ...
            ///    46nd channel is the one between N12 and N24.
            ///
            /// (each node is connected with all its neighbors!
            ///  Node N13 is near N1!).
            ///
            /// If you want a wireless network you have to use the parameters
            /// node_number and distance.
            /// Use the node_number like explained above while use the parameter
            /// distance's properties to set
            /// the step in meters to outline the nodes' positions in the area.
            /// If you work with a wireless network you will need to put only a
            /// channel in channel_list.
            /// The channel has to be a multiPoint channel!

            enum topology_type_t
            {
                //STAR,
                //RING,
                BOTTLENECK,
                MESH
            };


            /// @brief Vector of CoreTaskSetup type.
            typedef std::vector< Scnsl::Setup::ChannelSetup_base_t * > ChannelSetupList_t;

            /// @brief Vector of CoreTaskSetup type.
            typedef std::vector< Scnsl::Setup::BindSetup_base_t * > BindSetupList_t;


            //@}


            /// @brief Constructor.
            CoreTopologySetup_t();

            /// @brief Destructor.
            virtual
            ~CoreTopologySetup_t();


            /// @brief The type of topology to be created.
            topology_type_t topology_type;

            /// @brief Distances between nodes and number of nodes for each
            /// coordinate used in mesh topology.
            Scnsl::Core::node_factory_infos_t node_factory_infos;

            /// @brief The channels to be created in the specified topology.
            ChannelSetupList_t channel_list;

            /// @brief The tasks to be created in the specified topology.
            BindSetupList_t bind_list;

		private:
			CoreTopologySetup_t(const CoreTopologySetup_t &);
			CoreTopologySetup_t & operator =(const CoreTopologySetup_t&);
        };

} }



#endif
