// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_QUEUEMODELS_QUEUE_IF_T_HH
#define SCNSL_QUEUEMODELS_QUEUE_IF_T_HH



/// @file
/// The queue interface.

#include <stdexcept>
#include <list>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"

namespace Scnsl { namespace Core {

    class Packet_t;

  } }

namespace Scnsl { namespace QueueModels {


    /// @brief The queue interface.
    ///
    /// Design patterns:
    /// - Interface.
    /// - Regular.
    ///
    class SCNSL_EXPORT Queue_if_t
    {
    public:

		/// @name Traits.
        //@{

		/// @brief The packet type.
		typedef Scnsl::Core::Packet_t Packet_t;

		//@}

        /// @brief Impure virtual destructor.
        virtual
        ~Queue_if_t();

        /// @name Interface methods.
        //@{


		/// @brief Enqueue a packet
        ///
        /// @param p The packet to be enqueued.
        virtual
		bool enqueue( Packet_t & p ) = 0;


        /// @brief Dequeue a packet
		///
		/// @return The reference to the packet.
        virtual
		Packet_t & dequeue() = 0;


		/// @brief Checks if the queue is empty
        ///
        /// @return True if the queue is empty, false otherwise.
        virtual bool isEmpty() = 0;


		/// @brief Checks if the packet can be enqueued
		///
		/// @param p The packet
		/// @return True if the packet can be enqueued, false otherwise.
        virtual
		bool check( const Packet_t & p ) = 0;

		/// @brief Checks if the size of the packet is less or equal to the total capacity of the queue
		///
		/// @param p The packet
		/// @return True if the packet can be enqueued, false otherwise.
        virtual
		bool checkTot( const Packet_t & p ) = 0;


        //@}

    protected:

        /// @brief Default constructor.
        Queue_if_t();

        /// @brief Copy constructor.
        Queue_if_t( const Queue_if_t & c );

        /// @brief Assignment operator.
        Queue_if_t & operator = ( const Queue_if_t & c );

    };

  } }



#endif
