// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_QUEUEMODELS_FIFO_T_HH
#define SCNSL_QUEUEMODELS_FIFO_T_HH



/// @file
/// The Fifo queue.

#include <list>
#include "../scnslConfig.hh"
#include "Queue_if_t.hh"
#include "../core/Packet_t.hh"



namespace Scnsl { namespace QueueModels {

  /// @brief The Fifo queue type.
  ///
  /// Design patterns:
  /// - Non-copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT Fifo_t :
        public Scnsl::QueueModels::Queue_if_t
  {
  public:

      /// @name Traits.
      //@{

	  /// @brief The size type.
	  typedef Scnsl::Core::size_t size_t;

	  /// @brief The packet type.
	  typedef Scnsl::Core::Packet_t Packet_t;

      //@}

      /// @brief Constructor.
      ///
      /// @param capacity The capacity of the queue.
      /// @throw std::invalid_argument
      ///
      Fifo_t( const size_t capacity );


      /// @brief Destructor.
      virtual
      ~Fifo_t();


      /// @name Queue interface methods.
      //@{

	  virtual
      bool enqueue( Packet_t & p ) override;

      virtual
      Packet_t & dequeue() override;

      virtual bool isEmpty() override;

	  virtual
      bool check( const Packet_t & p ) override;

	  virtual
      bool checkTot( const Packet_t & p ) override;

      //@}



  protected:

      /// @brief The available capacity of the queue in bytes.
      size_t _capacity;

      /// @brief The total capacity of the queue in bytes.
      size_t _capacityTot;

	  /// @brief The list of packets.
	  std::list< Packet_t > _list;

	  /// @brief The head of the list.
	  Packet_t _pkt;

  private:

      /// @brief Disabled copy constructor.
      Fifo_t ( const Fifo_t & );

      /// @brief Disabled assigmenemt operator.
      Fifo_t & operator = ( const Fifo_t & );
  };

} }



#endif
