// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_QUEUEMODELS_PRIORITY_T_HH
#define SCNSL_QUEUEMODELS_PRIORITY_T_HH



/// @file
/// The Priority queue.

#include <list>
#include "../scnslConfig.hh"
#include "Queue_if_t.hh"
#include "../core/Packet_t.hh"
#include "../builtinPlugin/CoreCommunicatorSetup_t.hh"



namespace Scnsl { namespace QueueModels {

  /// @brief The Priority queue type.
  ///
  /// Design patterns:
  /// - Non-copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT Priority_t :
        public Scnsl::QueueModels::Queue_if_t
  {
  public:

      /// @name Traits.
      //@{

	  /// @brief The size type.
	  typedef Scnsl::Core::size_t size_t;

	  /// @brief The packet type.
	  typedef Scnsl::Core::Packet_t Packet_t;

	  /// @brief The label type.
	  typedef Scnsl::Core::label_t label_t;

	  /// @brief The policy type.
	  typedef Scnsl::BuiltinPlugin::CoreCommunicatorSetup_t::policy_type_t policy_type_t;

	  /// @brief The counter type.
	  typedef Scnsl::Core::counter_t counter_t;



      //@}

      /// @brief Constructor.
      ///
      /// @param capacity The capacity of each queue.
      /// @param queueNumber The number of queues.
	  /// @param policy The algorithm that manages the queues.
      /// @param weights The weights for internal queues.
      /// @throw std::invalid_argument
      ///
      Priority_t( const size_t capacity,
				  const size_t queueNumber,
				  policy_type_t policy,
				  const size_t * weights );


      /// @brief Destructor.
      virtual
      ~Priority_t();


      /// @name Queue interface methods.
      //@{

	  virtual
      bool enqueue( Packet_t & p ) override;

      virtual
      Packet_t & dequeue() override;

      virtual bool isEmpty() override;

	  virtual
      bool check( const Packet_t & p ) override;

	  virtual
      bool checkTot( const Packet_t & p ) override;

      size_t getCapacity()
          const;

      size_t getCapacityTot()
          const;

      //@}



  protected:

	  /// @brief The number of queues to be created.
	  size_t _queueNumber;

	  /// @brief The available capacity of the queue in bytes.
      size_t * _capacity;

      /// @brief The total capacity of the queue in bytes.
      size_t _capacityTot;

	  /// @brief The list of packets.
	  std::list< Packet_t > * _lists;

	  /// @brief The head of the list.
	  Packet_t _pkt;

	  /// @brief The algorithm that manages the queues.
	  policy_type_t _policy;

	  /// @brief The weight for each queue.
	  size_t * _weights;

	  /// @brief The sum of packets size in the queue;
	  size_t _totSize;

	  /// @brief The total number of packets in the queue.
	  counter_t _totPackets;

	  /// @brief The minimum weight.
	  size_t _weightMin;

	  /// @brief The maximum size, in bytes, that can be dequeued from each queue.
	  size_t * _count;

	  /// @brief The queue that must be served.
	  counter_t _served;

      /// @brief First
	  bool _first;

	  /// @brief Updates count variable.
	  void updateCount();

  private:

      /// @brief Disabled copy constructor.
      Priority_t ( const Priority_t & );

      /// @brief Disabled assigmenemt operator.
      Priority_t & operator = ( const Priority_t & );
  };

} }



#endif
