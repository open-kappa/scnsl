#ifndef SCNSL_SYSCALLS_HH
#define SCNSL_SYSCALLS_HH

#include "NetworkSyscalls.hh"
#include "TimedSyscalls.hh"

///@file File used to override normal system calls with SCNSL ones

using Scnsl::Syscalls::clock_gettime;
using Scnsl::Syscalls::gettimeofday;
using Scnsl::Syscalls::time;
using Scnsl::Syscalls::usleep;

using Scnsl::Syscalls::OPTVAL_TYPE;
using Scnsl::Syscalls::sockaddr;
using Scnsl::Syscalls::sockaddr_in;
using Scnsl::Syscalls::sockaddr_storage;
#define fd_set Scnsl::Syscalls::fd_set

using Scnsl::Syscalls::accept;
using Scnsl::Syscalls::addrinfo;
using Scnsl::Syscalls::bind;
using Scnsl::Syscalls::close;
using Scnsl::Syscalls::connect;
using Scnsl::Syscalls::fcntl;
using Scnsl::Syscalls::freeaddrinfo;
using Scnsl::Syscalls::gai_strerror;
using Scnsl::Syscalls::getaddrinfo;
using Scnsl::Syscalls::gethostname;
using Scnsl::Syscalls::getnameinfo;
using Scnsl::Syscalls::getsockname;
using Scnsl::Syscalls::getsockopt;
using Scnsl::Syscalls::htonl;
using Scnsl::Syscalls::inet_pton;
using Scnsl::Syscalls::listen;
using Scnsl::Syscalls::ntohl;
using Scnsl::Syscalls::recv;
using Scnsl::Syscalls::recvfrom;
using Scnsl::Syscalls::select;
using Scnsl::Syscalls::send;
using Scnsl::Syscalls::sendto;
using Scnsl::Syscalls::setsockopt;
using Scnsl::Syscalls::shutdown;
using Scnsl::Syscalls::socket;

#endif  // SCNSL_SYSCALLS_HH
