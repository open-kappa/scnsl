#ifndef SCNSL_TIME_SYSCALLS_HH
#define SCNSL_TIME_SYSCALLS_HH

#include "../scnslConfig.hh"
#include <ctime>

///@file Here are defined all the socket system call users have access to
namespace Scnsl { namespace Syscalls {

///@name Timed-based System Calls available in the simulator
//@{
typedef long time_t;
///@brief Suspend execution for microsecond intervals
SCNSL_EXPORT int usleep(unsigned usec);

///@brief Suspend execution for a given intervals
SCNSL_EXPORT int nanosleep(const timespec *req, timespec *rem);

///@brief Get the current time of day and timezone information,
/// putting it into *TV and *TZ.  If TZ is NULL, *TZ is not filled.
///@return 0 on success, -1 on errors.
///@note: This form of timezone information is obsolete.
SCNSL_EXPORT int gettimeofday(timeval * tv, long int * tz);

///@brief Return the current time and put it in *TIMER if TIMER is not NULL.
SCNSL_EXPORT time_t time(time_t * timer);

///@brief Get current value of (systemC) clock and store it in TP.  */
SCNSL_EXPORT int clock_gettime(clockid_t __clock_id, timespec * __tp);

}}  // namespace Scnsl::Syscalls
#endif  // SCNSL_TIME_SYSCALLS_HH
