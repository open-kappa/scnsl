#ifndef SCNSL_NETWORKSYSCALLS_HH
#define SCNSL_NETWORKSYSCALLS_HH

#include "../scnslConfig.hh"
#include <queue>
#include <unordered_set>
#include <ctime>
#include <cstdio>
#include <cstdint>

///@file Here are defined all the socket system call users have access to
namespace Scnsl { namespace Core {

typedef unsigned int task_id_t;
typedef unsigned char byte_t;

}}  // namespace Scnsl::Core

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

typedef unsigned int socketFD_t;

}}}

using namespace Scnsl::Protocols::Network_Lv4;

namespace Scnsl { namespace Syscalls {

///@brief Fake constants for systemc calls
enum syscall_parameters //: int
{
    INADDR_ANY = 0,
    AF_INET,
    AF_INET6,
    AF_UNIX,
    AF_UNSPEC,
    F_SETFL,
    F_GETFL,
    O_NONBLOCK,
    NI_NUMERICHOST,
    SHUT_RD,
    SHUT_WR,
    SHUT_RDWR,
    AI_PASSIVE,
    SOL_SOCKET,
    SO_ERROR,
    SO_REUSEADDR,
    SO_REUSEPORT,
    IPPROTO_IPV6,
    IPV6_V6ONLY,
    IPPROTO_TCP,
    IPPROTO_UDP,
    IPPROTO_IP,
    TCP_NODELAY,
    IP_ADD_MEMBERSHIP,
    IP_TOS,
    IPTOS_LOWDELAY,
    PF_INET,
    MSG_DONTWAIT,
    SOCK_CLOEXEC
};


///@brief struct to mimic the real used in socket programming
struct in_addr
{
    unsigned s_addr;  // address in numeric format
};

struct in6_addr
{
    unsigned char s6_addr[16]; /* IPv6 address */
};

struct sockaddr_in6
{
    syscall_parameters sin_family;  // e.g. AF_INET
    in6_addr sin6_addr;  // ip address
    unsigned int sin6_port;  // port number
    unsigned int sin6_scope_id;
};

struct sockaddr
{
    syscall_parameters sin_family;  // e.g. AF_INET
    syscall_parameters sa_family;  // e.g. AF_INET
    syscall_parameters ss_family;  // e.g. AF_INET
    in_addr sin_addr;  // ip address
    unsigned int sin_port;  // port number
};

struct sockaddr_in
{
    syscall_parameters sin_family;  // e.g. AF_INET
    syscall_parameters sa_family;  // e.g. AF_INET
    syscall_parameters ss_family;  // e.g. AF_INET
    in_addr sin_addr;  // ip address
    unsigned int sin_port;  // port number
};

struct sockaddr_storage
{
    syscall_parameters sin_family;  // e.g. AF_INET
    syscall_parameters sa_family;  // e.g. AF_INET
    syscall_parameters ss_family;  // e.g. AF_INET
    in_addr sin_addr;  // ip address
    unsigned int sin_port;  // port number
};

struct ip_mreqn
{
    struct in_addr imr_multiaddr; /* IP multicast group
                                    address */
    struct in_addr imr_address;   /* IP address of local
                                    interface */
    int imr_ifindex;   /* interface index */
};

struct ip_mreq //duplicate for compatibility
{
    struct in_addr imr_multiaddr; /* IP multicast group
                                    address */
    struct in_addr imr_interface;   /* IP address of local
                                    interface */
};

typedef Scnsl::Core::byte_t byte_t;
typedef unsigned socklen_t;
typedef Scnsl::Core::task_id_t task_id_t;
typedef std::unordered_set<socketFD_t> fd_set;
typedef int OPTVAL_TYPE;

///@brief store data for internal_getaddrinfo function
struct addrinfo
{
    int ai_flags;
    int ai_family;
    int ai_socktype;
    int ai_protocol;
    socklen_t ai_addrlen;
    struct sockaddr * ai_addr;
    const char * ai_canonname;
    struct addrinfo * ai_next;
};
//poll events definition from poll.h
#define POLLIN		0x0001
#define POLLPRI		0x0002
#define POLLOUT		0x0004
#define POLLERR		0x0008
#define POLLHUP		0x0010
#define POLLNVAL	0x0020

typedef unsigned int nfds_t;
/// @brief struct for poll() function
struct pollfd
{
    int   fd;         /* file descriptor */
    short events;     /* requested events */
    short revents;    /* returned events */
};


/* The `getifaddrs' function generates a linked list of these structures.
   Each element of the list describes one network interface.  */
struct ifaddrs
{
  struct ifaddrs *ifa_next;        /* Pointer to the next structure.  */

  char *ifa_name;                /* Name of this network interface.  */
  unsigned int ifa_flags;        /* Flags as from SIOCGIFFLAGS ioctl.  */

  struct sockaddr *ifa_addr;        /* Network address of this interface.  */
  struct sockaddr *ifa_netmask; /* Netmask of this interface.  */
  union
  {
    /* At most one of the following two is valid.  If the IFF_BROADCAST
       bit is set in `ifa_flags', then `ifa_broadaddr' is valid.  If the
       IFF_POINTOPOINT bit is set, then `ifa_dstaddr' is valid.
       It is never the case that both these bits are set at once.  */
    struct sockaddr *ifu_broadaddr; /* Broadcast address of this interface. */
    struct sockaddr *ifu_dstaddr; /* Point-to-point destination address.  */
  } ifa_ifu;
  void *ifa_data;                /* Address-specific data (may be unused).  */
};

#  define ifa_broadaddr        ifa_ifu.ifu_broadaddr
#  define ifa_dstaddr        ifa_ifu.ifu_dstaddr


// Macros used for file descriptor set (for select)
#define FD_ZERO(fdsp) (*fdsp).clear()
#define FD_SET(d, set) (*set).insert(d)
#define FD_CLR(d, set) (*set).erase(d)
#define FD_ISSET(d, set) (*set).count(d)

#define	FD_SETSIZE 1024

#define EINTR 4 /* Interrupted system call */
#define EAGAIN 11 /*Try again*/
#define EWOULDBLOCK EAGAIN /* Operation would block*/
#define EINPROGRESS 115 /* Operation now in progress */

///@brief Return last call error value
///@return last function error value
SCNSL_EXPORT int* scnsl_internal_errno();

// Error values for errno variable
#define errno (*Scnsl::Syscalls::scnsl_internal_errno())

///@name Socket system calls
//@{
///@brief return false when IN6_IS_ADDR_UNSPECIFIED is called
SCNSL_EXPORT int scnsl_unsupported_ipv6(in6_addr * addr);

#define IN6_IS_ADDR_UNSPECIFIED(s) (Scnsl::Syscalls::scnsl_unsupported_ipv6(s))

///@brief Create a socket and return a file descriptor to it
SCNSL_EXPORT socketFD_t socket(int domain, int type, int protocol);

///@brief Allow socket to pasockaddr_storagess from synchronous to asynchronous
SCNSL_EXPORT int fcntl(socketFD_t sfd, int opt, int val = 0);

///@brief Bind a socket to a specific ip address/port
SCNSL_EXPORT int bind(socketFD_t sfd, const sockaddr * socket_addr, int addrlen);

///@brief Create a receive queue for pending connection of the given length
///@param sfd the socket file descriptor
///@param backlog the length of the queue
SCNSL_EXPORT int listen(socketFD_t sfd, int backlog);

///@brief Wait until a connection request is received
SCNSL_EXPORT socketFD_t
accept(socketFD_t sfd, sockaddr * addr, socklen_t * addrnen);

///@brief Accept, but with another name and flags
SCNSL_EXPORT socketFD_t
accept4(socketFD_t sfd, sockaddr * addr, socklen_t * addrnen, int flags);

///@brief Establish a connection to a specific socket
SCNSL_EXPORT int connect(socketFD_t sfd, const sockaddr * socket_addr, int addrlen);

///@brief Send a buffer using the specified socket
///@param sfd the socket used to send data
///@param buffer the buffer to send
///@param length the number of byte to send
///@param flags the flags used
SCNSL_EXPORT int send(
    socketFD_t sfd,
    const void * buffer,
    int length,
    int flags);

///@brief Send a buffer to a specific destination
///@warning FUNCTION NOT AVAILABLE FOR THIS CLASS
///@param sfd the socket used to send data
///@param buffer the buffer to send
///@param length the number of byte to send
///@param flags the flags used
///@param dest_addr the struct with the destination address
///@param addrlen the size of the address structure
SCNSL_EXPORT int sendto(
    socketFD_t sfd,
    const void * buffer,
    int length,
    int flags,
    const sockaddr * dest_addr,
    socklen_t addrlen);

///@brief Receive a buffer from the specified socket
///@param sfd The socket where the data are read
///@param buffer the buffer where the data are stored
///@param length the maximum number of byte read from the receive buffer
///@param flags the flags used
SCNSL_EXPORT int recv(socketFD_t sfd, void * buffer, int length, int flags);

///@brief Receive a buffer from the specified socket
///@warning FUNCTION NOT AVAILABLE FOR THIS CLASS
///@param sfd The socket where the data are read
///@param buffer the buffer where the data are stored
///@param length the maximum number of byte read from the receive buffer
///@param flags the flags used
///@param src_addr the struct with the source address
///@param addrlen the size of the address structure
SCNSL_EXPORT int recvfrom(
    socketFD_t sfd,
    void * buffer,
    int length,
    int flags,
    sockaddr * src_addr,
    socklen_t * addrlen);

///@brief Block the execution until at least one of the file descriptor in the
///set is ready
///@param nfds the maximum file descriptor value in all sets +1
///@param readfds the set containing all the read fd to watch
///@param write the set containing all the write fd to watch
///@param exceptfds the set containing all the exceptions fd to watch
///@param timeout the maximum timeout to block the program
SCNSL_EXPORT int select(
    int nfds,
    fd_set * readfds,
    fd_set * writefds,
    fd_set * exceptfds,
    timeval * timeout);

///@brief Block the execution until at least one of the file descriptor in the
///set is ready
///@param nfds the maximum file descriptor value in all sets +1
///@param readfds the set containing all the read fd to watch
///@param write the set containing all the write fd to watch
///@param exceptfds the set containing all the exceptions fd to watch
///@param timeout the maximum timeout to block the program
SCNSL_EXPORT int poll(
    pollfd *fds,
    nfds_t nfds,
    int timeout);

///@brief return an option from a socket. (only error option for now)
///@param sockfd the file descriptor
///@param level the level the option must be retrieved (only socket for now)
///@param optlen the name of the option
///@param optval pointer where the return value is stored
///@param optlen size of the element pointed by optval
SCNSL_EXPORT int getsockopt(
    int sockfd,
    int level,
    int optname,
    void * optval,
    socklen_t * optlen);

///@brief Set an option from a socket. (only error option for now)
///@param sockfd the file descriptor
///@param level the level the option must be retrieved (only socket for now)
///@param optlen the name of the option
///@param optval pointer where the return value is stored
///@param optlen size of the element pointed by optval
SCNSL_EXPORT int setsockopt(
    int sockfd,
    int level,
    int optname,
    const void * optval,
    socklen_t optlen);

///@brief return the host name
///@param name the buffer to store the name
///@param len the buffer length. If the name exceed, it is truncated
SCNSL_EXPORT int gethostname(char * name, size_t len);

///@brief Close a socket
///@param sfd The socket to internal_close
SCNSL_EXPORT int close(socketFD_t sfd);

///@brief Close only part of a fullduplex connection
///@param sockfd the socket file descriptor
///@param how which part of the connection must be stopped
SCNSL_EXPORT int shutdown(int sockfd, int how);

///@brief Return a addrinfo struct containing info based on the node (ip) and
///service (port) specified
/// To simplify, only one element is returned, and the hints struct, used to
/// filter the results, is left unused
///@note Thi method works only with ip ad port number-like string. Service name
///or host name won't work
///@param node the string with the ip (can be NULL)
///@param service the string with the port
///@param hints struct used to "filter" the results. Not used here
///@param res list of result (only one since multiple ips are not managed)
SCNSL_EXPORT int getaddrinfo(
    const char * node,
    const char * service,
    const addrinfo * hints,
    addrinfo ** res);

///@brief free the space used by an addrinfo struct
///@param ai the list of struct to free
SCNSL_EXPORT void freeaddrinfo(addrinfo * ai);

///@brief Translate an error code into a string
SCNSL_EXPORT const char * gai_strerror(int errorcode);

///@brief Converts a socket address to a corresponding host and service. For
///ease of use hostname is the ip address,
/// and server name is the port
///@param addr pointer to a sockaddr structure
///@param addrlen the size of addr
///@param host pointer to a buffer in which the host name will be placed
///@param hostlen the size of the host buffer
///@param serv pointer to a buffer in which the service name will be placed
///@param host pointer to a buffer in which the host name will be placed
SCNSL_EXPORT int getnameinfo(
    const sockaddr * addr,
    socklen_t addrlen,
    char * host,
    socklen_t hostlen,
    char * serv,
    socklen_t servlen,
    int flags);

///@brief return the address a socket is bound
///@param sockfd the socket fd
///@param addr the struct to store the result
///@param addrlen the size of the struct
SCNSL_EXPORT int getsockname(int sockfd, sockaddr * addr, socklen_t * addrlen);

///@brief returns the address of the peer connected to the socket sockfd
///@param sockfd the socket fd
///@param addr the struct to store the result
///@param addrlen the size of the struct
SCNSL_EXPORT int getpeername(int sockfd, sockaddr * addr, socklen_t * addrlen);

///@brief Dummy function, useless for tcp
SCNSL_EXPORT uint32_t htonl(uint32_t host);

///@brief Dummy function, useless for tcp
SCNSL_EXPORT uint32_t ntohl(uint32_t net);

///@brief Dummy function, useless for tcp
SCNSL_EXPORT uint16_t htons(uint16_t host);

///@brief Dummy function, useless for tcp
SCNSL_EXPORT uint16_t ntohs(uint16_t net);

///@brief Convert numeric IP address in readable format
SCNSL_EXPORT char *inet_ntoa(in_addr in);

///@brief Convert numeric IP address in readable format
SCNSL_EXPORT char *inet_ntop(int af, const void * src, char * dst, socklen_t size);

///@brief Convert ip address to binary form
SCNSL_EXPORT int inet_pton(int af, const char * src, void * dst);

///@brief Convert ip address to binary form
SCNSL_EXPORT unsigned int inet_addr(const char *cp);

///@brief read data from a socket
///@return number of bytes read
SCNSL_EXPORT ssize_t read(int fd, void *buf, size_t count);

///@brief read data from a socket
///@return number of bytes read
SCNSL_EXPORT ssize_t write(int fd, const void *buf, size_t count);

///@brief unused function, implemented only for compatibility purpose
SCNSL_EXPORT int socketpair(int domain, int type, int protocol, int sv[2]);

///@brief unused function, implemented only for compatibility purpose
SCNSL_EXPORT int ioctl(int fd, unsigned long request, ...);

///@brief Create a linked list of `struct ifaddrs' structures, one for each
/// network interface on the host machine. For now it returns only one element,
/// as multiple netwrok interfaces are not allowed  
///@return  0 on success, -1 on error.
SCNSL_EXPORT int getifaddrs (ifaddrs **__ifap);

///@brief Reclaim the storage allocated by a previous `getifaddrs' call.
SCNSL_EXPORT void freeifaddrs (ifaddrs *__ifa) ;
//@}

}}  // namespace Scnsl::Syscalls


#endif  // SCNSL_TCPSYSCALLS_HH
