#ifndef SYSCALLS_WAIT
#define SYSCALLS_WAIT
#include <chrono>
#include <systemc>
#include "scnsl/protocols/lv4_communicator/NetworkAPI_Task_if_t.hh"
#include "../scnslConfig.hh"


///@file Special function(s) to call from re-implemented system calls
namespace Scnsl { namespace Syscalls {

///@brief Stop the task currently in execution to recover time spent oustside
///SCNSL control
///@return a pointer to the current exectued task
SCNSL_EXPORT NetworkAPI_Task_if_t * synchTaskTime();

}}
#endif // !SYSCALLS_WAIT
