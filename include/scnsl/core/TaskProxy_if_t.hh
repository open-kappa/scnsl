// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_TASKPROXY_IF_T_HH
#define SCNSL_CORE_TASKPROXY_IF_T_HH



/// @file
/// Task proxy interface.

#include <stdexcept>
#include <systemc>

#include "../scnslConfig.hh"
#include "Communicator_if_t.hh"
#include "../tracing/Traceable_base_t.hh"

namespace Scnsl { namespace Core {

  class Task_if_t;
  class Channel_if_t;
  class Node_t;

} }

namespace Scnsl { namespace Core {

  /// @brief The task proxy.
  /// This class is used to hide the task implementation details from the
  /// rest of the simulator. E.g. the rest of the simulator is unaware
  /// if the task is described at RTL or TLM.
  /// moreover this class is the joining point of tasks to channels.
  ///
  /// Design patterns:
  /// - Interface.
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT TaskProxy_if_t :
        public sc_core::sc_module,
        public Communicator_if_t,
        public Scnsl::Tracing::Traceable_base_t
  {
  public:

      /// @brief Impure virtual destructor.
      virtual
      ~TaskProxy_if_t() = 0;

      /// @name Communicator reimplemented methods.
      //@{

      /// @brief reimplemented since it is legal only if
      /// the two tasks are on the same node.
      /// If the test passes, the receive() method is called.
      virtual
      errorcode_t send( const Packet_t & p ) override;

      /// @brief Reimplemented since it is legal to call only once.
      virtual
      void bindTaskProxy( const TaskProxy_if_t * tp,
                          Communicator_if_t * c ) override;

      /// @brief Reimplemented since it is illegal to call.
      virtual
      void bindChannel( const Channel_if_t * ch,
                        Communicator_if_t * c ) override;


      /// @brief Reimplemented since it is illegal to call.
      virtual
      void stackUp( Communicator_if_t * c ) override;

      //@}

      /// @name Taskproxy interface methods.
      //@{

      /// @brief Gets the associated channel.
      const Channel_if_t * getChannel()
          const;

      /// @brief Gets the associated task.
      const Task_if_t * getTask()
          const;

      /// @brief Gets the destination task.
      const Task_if_t * getDestinationTask()
          const;

      /// @brief Sets the destination task.
      void setDestinationTask( Task_if_t * dst );

      /// @brief Gets the destination node.
      Node_t * getDestinationNode();

      //@}

  protected:

      /// @brief Constructor.
      ///
      /// @param name This module name.
      /// @param t The relative task.
      /// @param ch The relative channel.
      ///
      TaskProxy_if_t(
          sc_core::sc_module_name name,
          Task_if_t * t,
          Channel_if_t * ch );

      /// @brief The associated task.
      Task_if_t * _task;

      /// @brief The destination task.
      Task_if_t * _destinationTask;

      /// @brief The associated channel.
      Channel_if_t * _channel;

  private:

      /// @brief Disabled copy constructor.
      TaskProxy_if_t( const TaskProxy_if_t & );

      /// @brief Disabled assignment operator.
      TaskProxy_if_t & operator = ( const TaskProxy_if_t & );

  };

} }

#endif
