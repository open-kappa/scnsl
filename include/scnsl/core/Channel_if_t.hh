// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_CHANNEL_IF_T_HH
#define SCNSL_CORE_CHANNEL_IF_T_HH



/// @file
/// A channel inteface.

#include <stdexcept>
#include <list>
#include <systemc>

#include "../scnslConfig.hh"
#include "data_types.hh"
#include "../tracing/Traceable_base_t.hh"

namespace Scnsl { namespace Core {

  class Node_t;
  class Packet_t;

} }

namespace Scnsl { namespace Core {

  /// @brief The base channel inteface.
  ///
  /// Design pattenrs:
  /// - Non copiable.
  /// - Non assignable.
  /// - Inteface.
  ///
  class SCNSL_EXPORT Channel_if_t:
        public sc_core::sc_module,
        public Scnsl::Tracing::Traceable_base_t
  {
  public:

      /// @name Traits.
      //@{

      /// @brief The errorcode type.
      typedef Scnsl::Core::errorcode_t errorcode_t;

      /// @brief The list of binded nodes.
      typedef std::list< Node_t * > NodeList_t;

      //@}


      /// @brief Impure virtual destructor.
      virtual
      ~Channel_if_t() = 0;

      /// @name Interface methods.
      //@{

      /// @brief Adds a node to this channel.
      /// Default implementation simply stores the node in the list.
      ///
      /// @param n The node to bind.
      /// @throw std::logic_error It is not possible to register the node.
      ///
      virtual
      void addNode( Node_t * n );

      /// @brief Sends a packet throw this channel.
      /// Pure virtual.
      ///
      /// @param n The sending node.
      /// @param p The packet to send.
      /// @return Zero on success.
      /// @throw std::invalid_argument The sender node is invalid.
      ///
      virtual
      errorcode_t send( Node_t * n, const Packet_t & p ) = 0;


      /// @brief Tests if this channel can be binded with two nodes,
      /// like a link, or if it allows multiple bindings.
      virtual
      bool isMultipoint()
          const = 0;

      /// @brief Updates node properties.
      /// Default implementation does nothing.
      ///
      /// @param n The node.
      ///
      virtual void updateProperties( const Node_t * n );

      //@}

  protected:

      /// @brief Constructor.
      ///
      /// @param name This module name.
      ///
      explicit
      Channel_if_t( const sc_core::sc_module_name name );

      /// @brief The list of binded nodes.
      NodeList_t _nodes;

  private:

      /// @brief Disabled copy constructor.
      Channel_if_t( const Channel_if_t & );

      /// @brief Disabled assignment operator.
      Channel_if_t & operator = ( const Channel_if_t & );

  };

} }



#endif
