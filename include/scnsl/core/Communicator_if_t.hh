// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_COMMUNICATOR_IF_T_HH
#define SCNSL_CORE_COMMUNICATOR_IF_T_HH



/// @file
/// The communicator interface.

#include <stdexcept>
#include <map>
#include <list>
#include "../scnslConfig.hh"
#include "data_types.hh"

namespace Scnsl { namespace Core {

    class Packet_t;
    class Channel_if_t;
    class TaskProxy_if_t;

  } }

namespace Scnsl { namespace Core {

    /// @brief The communicator interface.
    /// It is allowed to create chains of communicators,
    /// as long as the head is made by a taskproxy and
    /// the tail is made by a node.
    ///
    /// It is also legal to connect directly two task proxies.
    ///
    /// There are two possible stacking strategies:
    /// - Specify taskproxy by taskproxy and channel by channel
    ///   to which communicator forward the packets (via bind() methods).
    /// - Specify a direct upper or lower communicator layer for all packets
    ///   ( via the stack*() methods).
    ///
    /// Design patterns:
    /// - Interface.
    /// - Regular.
    ///
    class SCNSL_EXPORT Communicator_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The carrier type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief the error code type.
        typedef Scnsl::Core::errorcode_t errorcode_t;

        /// @brief The communicator list type.
        typedef std::list< Communicator_if_t * > CommunicatorList_t;

        /// @brief The data structure to store the bindings for task.
        typedef std::map< TaskProxy_if_t *, CommunicatorList_t >
        TaskProxyBindings_t;

        /// @brief The data structure to store the bindings for channels.
        typedef std::map< Channel_if_t *, CommunicatorList_t >
        ChannelBindings_t;

        //@}

        /// @brief Impure virtual destructor.
        virtual
        ~Communicator_if_t() = 0;

        /// @name Interface methods.
        /// They are all provided with a default implementation.
        //@{

        /// @brief Sets the carrier value for a task.
        ///
        /// @param ch the channel to which the carrier belongs.
        /// @param c The new carrier value.
        /// @throw std::logic_error If it is invalid to call such
        ///        a method on this communicator.
        virtual
        void setCarrier( const Channel_if_t * ch, const carrier_t c );

        /// @brief Sends a packet.
        /// @return Zero on success.
        /// @throw std::logic_error If it is invalid to call such
        ///        a method on this communicator.
        virtual
        errorcode_t send( const Packet_t & p );

        /// @brief Receives a packet.
        /// @return Zero on success.
        /// @throw std::logic_error If it is invalid to call such
        ///        a method on this communicator.
        virtual
        errorcode_t receive( const Packet_t & p );

        /// @brief Binds the chain, for a taskproxy.
        /// By default, all packets sent by the taskproxy are forwarded to
        /// the associated communicator.
        ///
        /// @param tp The taskproxy.
        /// @param c The communicator.
        ///
        virtual
        void bindTaskProxy( const TaskProxy_if_t * tp,
                            Communicator_if_t * c );

        /// @brief Binds the chain, for a channel.
        /// By default, all packets received by the node from that channel,
        /// are forwarded to the associated communicator.
        ///
        /// @param ch The channel.
        /// @param c The communicator.
        ///
        virtual
        void bindChannel( const Channel_if_t * ch,
                          Communicator_if_t * c );


        /// @brief Stacks as upper layer the given communicator.
        ///
        /// @param c The communicator.
        ///
        virtual
        void stackUp( Communicator_if_t * c );

        /// @brief Stacks as lower layer the given communicator.
        ///
        /// @param c The communicator.
        ///
        virtual
        void stackDown( Communicator_if_t * c );

        //@}

    protected:

        /// @brief Default constructor.
        Communicator_if_t();

        /// @brief Copy constructor.
        Communicator_if_t( const Communicator_if_t & c );

        /// @brief Assignment operator.
        Communicator_if_t & operator = ( const Communicator_if_t & c );

        /// @brief The channel bindings.
        TaskProxyBindings_t _taskproxyBindings;

        /// @brief The channel bindings.
        ChannelBindings_t _channelBindings;

        /// @brief The upper stack level.
        Communicator_if_t * _upper;

        /// @brief The lower stack level.
        Communicator_if_t * _lower;
    };

  } }



#endif
