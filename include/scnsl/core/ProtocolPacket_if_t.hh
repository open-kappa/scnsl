// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_CORE_PROTOCOL_PACKET_T_HH
#define SCNSL_CORE_PROTOCOL_PACKET_T_HH

/// @file
/// A generic protocol packet.

#include "../scnslConfig.hh"
#include "PacketCommand.hh"
#include "PacketType.hh"
#include "data_types.hh"

#include <iostream>
#include <stdexcept>

namespace Scnsl { namespace Core {

/// @brief A generic protocol packet. All the protocol-exclusive
/// headers fields must be implemented into other children classes.
/// This class implements all the common methods a protocol packet should have,
/// as the data field necessary.
class SCNSL_EXPORT ProtocolPacket_if_t
{
public:
    /// @name Traits.
    //@{

    /// @brief The byte type.
    typedef Scnsl::Core::byte_t byte_t;

    /// @brief The size type.
    typedef Scnsl::Core::size_t size_t;

    /// @brief The size type.
    typedef Scnsl::Core::counter_t counter_t;

    /// @brief The packet label type.
    typedef Scnsl::Core::label_t label_t;

    /// @brief The node properties.
    typedef Scnsl::Core::node_properties_t node_properties_t;

    //@}

    /// @brief Default constructor.
    ProtocolPacket_if_t();

    /// @brief Command constructor.
    ProtocolPacket_if_t(size_t header_size);

    /// @brief Copy constructor.
    ProtocolPacket_if_t(const ProtocolPacket_if_t & p);

    /// @brief Destructor.
    virtual ~ProtocolPacket_if_t() = 0;

    ///@name Interface methods
    //@{
    /// @brief Gets the buffer size inside the packet in bits.
    ///
    /// @return The size.
    ///
    virtual size_t getInnerSize() const = 0;

    /// @brief Gets the payload size in bits.
    ///
    /// @return The size.
    ///
    virtual size_t getPayloadSize() const = 0;

    ///@brief Get the payload, as a ProtocolPacket pointer.
    /// Note that some classes can throw error if they don't encapsulate any
    /// other packet.
    virtual ProtocolPacket_if_t * getPayload() const = 0;

    ///@brief Set the  payload.
    /// Note that some classes can throw error if they don't encapsulate any
    /// other packet.
    virtual void setPayload(const ProtocolPacket_if_t * packet) = 0;

    ///@brief return the inner buffer
    virtual byte_t * getInnerBuffer() const = 0;

    ///@brief Clone the packet into a new one
    virtual ProtocolPacket_if_t * clone() const = 0;

    ///@brief Equality operator
    virtual bool operator==(const ProtocolPacket_if_t & p) const = 0;

    //@}

    ///@name Specific packet methods
    //@{

    /// @brief Gets the buffer size inside the packet in bytes.
    ///
    /// @return The size.
    ///
    size_t getInnerSizeInBytes() const;

    /// @brief Gets the payload size in bytes.
    ///
    /// @return The size.
    ///
    size_t getPayloadSizeInBytes() const;

    /// @brief Gets the total packet size in bits.
    ///
    /// @return The size.
    ///
    size_t getSize() const;

    /// @brief Gets the total packet size in bytes.
    ///
    /// @return The size.
    ///
    size_t getSizeInBytes() const;

    /// @brief Gets the header size.
    ///
    /// @return The size.
    ///
    size_t getHeaderSize() const;

    ///@brief Gets the header size in bytes.
    ///
    /// @return The size.
    ///
    size_t getHeaderSizeInBytes() const;

    ///@brief Set the header size in bits
    void setHeaderSize(const size_t s);

    ///@brief Set the header size in bytes
    void setHeaderSizeInBytes(const size_t s);

    //@}

    /// @name support methods.
    //@{

    /// @brief Gets the required bytes to encode the passed bits.
    static size_t to_bytes(const size_t s);

    /// @brief Gets the required bits to encode the passed bytes.
    static size_t to_bits(const size_t s);

    //@}
protected:
    ///@brief the header size in bits
    size_t _header_size;
};
}}  // namespace Scnsl::Core
#endif
