#ifndef SCNSL_RADIATIONPATTERN_IF_T_HH
#define SCNSL_RADIATIONPATTERN_IF_T_HH

#include <systemc>

#include "data_types.hh"
#include "Coordinate_t.hh"


namespace Scnsl { namespace Core {

class SCNSL_EXPORT RadiationPattern_if_t
{
public:

    typedef Scnsl::Core::node_properties_t node_properties_t;
    typedef Scnsl::Core::position_t position_t;
    typedef Scnsl::Core::Coordinate_t Coordinate_t;

    virtual
    ~RadiationPattern_if_t() = 0;

    /// @brief set the bool value used to turn on/off the radiation
    void setActive(const bool val);

    /// @brief return true if the radiation pattern is active
    bool isActive() const;

    /// @brief return true if this is the pattern effectively used in the communication
    ///        between the two nodes
    virtual
    bool isTheInterestedPattern(const node_properties_t & sp,
                                const node_properties_t & rp) const = 0;


    /// @brief Compute and return the effective gain value used for the communication
    virtual
    double getGain(const node_properties_t & sp,
                   const node_properties_t & rp) const =0;

protected:
    RadiationPattern_if_t(const Coordinate_t & vTheta,
                          const Coordinate_t & vPhi,
                          const bool active,
                          const Coordinate_t & vPol,
                          const double antennaGain);



    RadiationPattern_if_t(const RadiationPattern_if_t & other);
    void swap(RadiationPattern_if_t & other);

    /// @brief Returns the cross product between two vectors
    double _Sproduct(const Coordinate_t & vect0, const Coordinate_t & vect1) const;

    /// @brief Returns the scalar product between two vectors
    Coordinate_t _Xproduct(const Coordinate_t & vect0, const Coordinate_t & vect1) const;

    /// @brief Normalizes a vector with respect to the reference carthesian coordinate system
    Coordinate_t _normalizeVector(Coordinate_t vect) const;

    /// @brief Returns a vector in cartesian system connecting node "from" with node "to"
    Coordinate_t _getVectorFromTo(const node_properties_t & from, const node_properties_t & to) const;

    /// @brief Returns the angle between two vectors if these belongs to the same coordinate system (print a warning otherwise)
    double _getVect2VectAngle(const Coordinate_t & vect0, const Coordinate_t & vect1) const;

    /// @brief Converts vect from carthesian coordinate to the local R3 antenna coordnate system (ACS)
    Coordinate_t _changeVectorBase(Coordinate_t thetaVersor, Coordinate_t phiVersor, Coordinate_t normalVersor, Coordinate_t vect) const;

    /// @brief Returns the projection of vect onto the plane defined by base0 and base1
    /// base0-base1 must be orthogonal!
    Coordinate_t _getVectorOntoPlane(Coordinate_t vect, Coordinate_t base0, Coordinate_t base1) const;

    /// @brief Returns true if the difference between two double numbers
    /// is less than std::numeric_limits<double>::epsilon()
    bool _doubleEqual(const double a, const double b) const;

    /// @brief Computes distance between the positions of two nodes
    double _getDistance(const node_properties_t & sp,
                        const node_properties_t & rp) const;

    void _fixZero(double & value) const;

    /// @brief the Theta reference versor
    Coordinate_t _vTheta;
    /// @brief the Phi reference versor
    Coordinate_t _vPhi;
    /// @brief the Theta x Phi normal reference versor
    Coordinate_t _vN;
    /// @brief the Polarization versor with respect to the orthogonal reference versor system
    Coordinate_t _vPol;
    /// @brief the antenna gain in dB
    double _antennaGain;

    /// @brief True if the Radiation Pattern of the antennna is working
    bool _isActive;

    /// @brief change base matrix from Phi-Theta-Normal to x-y-z
    double _ptn2xyz[3][3];
    /// @brief change base matrix from x-y-z to Phi-Theta-Normal
    double _xyz2ptn[3][3];

private:

    /// @brief Disabled Assignment operator.
    RadiationPattern_if_t & operator = ( const RadiationPattern_if_t & e );
};

} }
#endif


