// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_QUEUECOMMUNICATOR_T_HH
#define SCNSL_CORE_QUEUECOMMUNICATOR_T_HH



/// @file
/// A queue communicator.

#include <stdexcept>
#include <map>

#include "../scnslConfig.hh"
#include "data_types.hh"
#include "Communicator_if_t.hh"
#include "../queueModels/Queue_if_t.hh"


namespace Scnsl { namespace Core {

    /// @brief A type of communicator for wrap the queues.
    ///
    /// Design patterns:
    /// - Non-copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT QueueCommunicator_t:
		public sc_core::sc_module,
        public Communicator_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The queue type
        typedef Scnsl::QueueModels::Queue_if_t Queue_if_t;

        //@}


        SC_HAS_PROCESS( QueueCommunicator_t );

        /// @brief Default constructor.
        ///
        /// @param modulename This module name.
        /// @param queueSend The queue for the send.
        /// @param queueReceive The queue for the receive.
        ///
        QueueCommunicator_t( sc_core::sc_module_name modulename,
                             Queue_if_t * queueSend,
                             Queue_if_t * queueReceive ,
                             bool dropPackestWhenFull=false);

        /// @brief Destructor.
        virtual
        ~QueueCommunicator_t();

        /// @brief Communicator interface methods.
        //@{

        virtual
        errorcode_t send( const Packet_t & p ) override;

        virtual
        errorcode_t receive( const Packet_t & p ) override;

        //@}




    protected:

        /// @brief The queue for the send.
        Queue_if_t * _queueSend;

        /// @brief The queue for the receive.
        Queue_if_t * _queueReceive;

        /// @brief The event for notify that a packet ahas been sent.
        sc_core::sc_event _pktSentDown;

        /// @brief The event for notify that a packet arrived from the lower layer.
        sc_core::sc_event _pktReceivedDown;

        /// @brief The event for notify that a packet arrived from the upper layer.
        sc_core::sc_event _pktReceivedUp;

        /// @brief True if pakcets are dropped when queue is full
        bool _dropPacketsWhenFull;

        /// @brief Process that sends packets.
        void sendProcess();

        /// @brief Process that receives packets.
        void receiveProcess();

    private:

        /// @brief Disabled copy constructor.
        QueueCommunicator_t( const QueueCommunicator_t & n );

        /// @brief Disabled assignemnet operator.
        QueueCommunicator_t & operator = ( const QueueCommunicator_t & n );


    };

  } }



#endif
