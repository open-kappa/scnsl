#ifndef SCNSL_COORDINATE_T_HH
#define SCNSL_COORDINATE_T_HH

#include <systemc>

namespace Scnsl { namespace Core {
class Coordinate_t;
}}

#include "data_types.hh"

namespace Scnsl { namespace Core {

class SCNSL_EXPORT Coordinate_t
{
public:
    /// @brief Coordinate constructor.
    /// @param isPolar 1 if a polar coordinate is being defined, 0 if a cartesian coordinate is being defined.
    /// @param x_theta x or theta component.
    /// @param y_phy y or phi component.
    /// @param z_module z component or module.
    Coordinate_t(const bool isPolar,
                 const double x_theta,
                 const double y_phy,
                 const double z_module);


    virtual ~Coordinate_t();

    Coordinate_t(const Coordinate_t & other);
    Coordinate_t & operator =(Coordinate_t other);
    void swap(Coordinate_t & other);

    double getX() const;
    double getY() const;
    double getZ() const;
    double getT() const;
    double getP() const;
    double getM() const;
    bool isPolar() const;
    void print(const std::string name) const;

private:

    bool _isPolar;
    double _first;
    double _second;
    double _third;

    /// @brief x carthesian coordinate
    double _x;
    /// @brief y carthesian coordinate
    double _y;
    /// @brief z carthesian coordinate
    double _z;

    /// @brief theta polar coordinate
    double _t;
    /// @brief phi polar coordinate
    double _p;
    /// @brief module of the polar coordinate
    double _m;

};

} }
#endif


