// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_PACKET_T_HH
#define SCNSL_CORE_PACKET_T_HH



/// @file
/// A generic packet.

#include <stdexcept>
#include <iostream>

#include "../scnslConfig.hh"
#include "data_types.hh"
#include "ProtocolPacket_if_t.hh"

namespace Scnsl { namespace Core {

class Task_if_t;
class TaskProxy_if_t;
class Channel_if_t;
class Node_t;

} }

namespace Scnsl { namespace Core {

/// @brief A generic packet.
///
/// By default, all the sizes are considered in bits.
/// The passed buffer is copyed, and not aliased.
///
/// By default a new created packet is valid, has a new id,
/// has the default printing method.
///
/// Design patterns:
/// - Regular.
///
class SCNSL_EXPORT Packet_t
{
public:

    /// @name Traits.
    //@{

    /// @brief The byte type.
    typedef Scnsl::Core::byte_t byte_t;

    /// @brief The size type.
    typedef Scnsl::Core::size_t size_t;

    /// @brief The size type.
    typedef Scnsl::Core::counter_t counter_t;

    /// @brief The packet id type.
    typedef Scnsl::Core::packet_id_t packet_id_t;

    /// @brief The packet label type.
    typedef Scnsl::Core::label_t label_t;

    /// @brief The node properties.
    typedef Scnsl::Core::node_properties_t node_properties_t;

    typedef Scnsl::Core::power_t power_t;

    //@}

    /// @brief Default constructor.
    Packet_t();

    /// @brief Copy constructor.
    /// @throw std::bad_alloc If no enough memory is available.
    Packet_t( const Packet_t & p );

    /// @brief Destructor.
    ~Packet_t();

    /// @brief Assignment operator.
    /// @throw std::bad_alloc If no enough memory is available.
    Packet_t & operator = ( const Packet_t & p );

    /// @brief Gets the payload size in bits.
    ///
    /// @return The size.
    ///
    size_t getPayloadSize()
        const;

    /// @brief Gets the payload size in bytes.
    ///
    /// @return The size.
    ///
    size_t getPayloadSizeInBytes()
        const;

    /// @brief Encapsulates a protocol packet.
    ///The packet is copyed
    /// @param p The packet to encapsulate.
    ///
    void setPayload(const ProtocolPacket_if_t & protocolPacket);

    /// @brief Gets the payload, as a pointer to protocolPacket .
    ///
    ProtocolPacket_if_t *getPayload() const;

    /// @brief Gets the inner byte array.
    ///
    byte_t * getInnerPayload( )
        const;

    /// @brief Gets the inner size in bits.
    ///
    size_t getInnerSize()
        const;

    /// @brief Gets the inner size in bytes.
    ///
    size_t getInnerSizeInBytes()
        const;

    //@}

    /// @name Header methods.
    //@{

    /// @brief Gets the header size in bits.
    ///
    /// @return The size.
    ///
    size_t getHeaderSize()
        const;

    /// @brief Gets the header size in bytes.
    ///
    /// @return The size.
    ///
    size_t getHeaderSizeInBytes()
        const;

    /// @brief Sets the header size in bits.
    ///
    /// @param s The size.
    ///
    void setHeaderSize( const size_t s );

    /// @brief Sets the header size in bytes.
    ///
    /// @param s The size.
    ///
    void setHeaderSizeInBytes( const size_t s );


    /// @brief Gets the ack flag.
    ///
    /// @return True if this packet is an ack.
    ///
    bool isAck()
        const;

    /// @brief Sets the ack flag.
    ///
    /// @param ack True if this packet is an ack.
    ///
    void setAck( const bool ack );

    /// @brief Gets if this packet requires an ack as reply.
    ///
    /// @return True if an ack is required.
    ///
    bool isAckRequired()
        const;

    /// @brief Sets if this packet requires an ack as reply.
    ///
    /// @param ack True if an ack is required.
    ///
    void setAckRequired( const bool ack );

    /// @brief Gets this packet sequence number.
    ///
    /// @return The sequence number.
    ///
    counter_t getSequenceNumber()
        const;

    /// @brief Sets this packet sequence number.
    ///
    /// @param seqno The sequence number.
    ///
    void setSequenceNumber( const counter_t seqno );

    /// @brief Gets this packet label.
    ///
    /// @return The label.
    ///
    label_t getLabel()
        const;

    /// @brief Sets this packet label.
    ///
    /// @param l The label.
    ///
    void setLabel( const label_t l );


    /// @brief Sets the packet taskproxy sender.
    void setSourceTaskProxy( const TaskProxy_if_t * tp );

    /// @brief Gets the packet taskproxy sender.
    const TaskProxy_if_t * getSourceTaskProxy()
        const;

    /// @brief Sets the packet taskproxy destination.
    void setDestTaskProxy( const TaskProxy_if_t * tp );

    /// @brief Gets the packet taskproxy destination.
    const TaskProxy_if_t * getDestTaskProxy()
    const;

    /// @brief Sets the packet task destination.
    void setDestinationTask( const Task_if_t * t );

    /// @brief Gets the packet task destination.
    const Task_if_t * getDestinationTask()
        const;


    /// @brief Sets the packet source node.
    void setSourceNode( const Node_t * n );

    /// @brief Gets the packet source node.
    const Node_t * getSourceNode()
        const;

    /// @brief Sets the packet destination node.
    /// nullptr means broadcast.
    void setDestinationNode( const Node_t * n );

    /// @brief Gets the packet destination node.
    const Node_t * getDestinationNode()
        const;

    /// @brief Gets the command the packet is carrying.
    ///
    /// @return The command.
    ///
    PacketCommand getCommand() const;

    /// @brief Gets the packet type.
    ///
    /// @return The packet type.
    ///
    PacketType  getType() const;

    ///@brief Set the packet command
    void setCommand(const PacketCommand command);

    ///@brief Set the packet type
    void setType( const PacketType type);
    //@}


    /// @name General methods.
    //@{

    /// @brief Gets the total packet size in bits.
    ///
    /// @return The size.
    ///
    size_t getSize()
        const;

    /// @brief Gets the total packet size in bytes.
    ///
    /// @return The size.
    ///
    size_t getSizeInBytes()
        const;

    //@}


    /// @name Simulation methods.
    //@{

    /// @brief Gets the packet id.
    packet_id_t getId()
        const;

    /// @brief Sets the packet id.
    void setId( const packet_id_t id );

    /// @brief Sets the packet propagation channel.
    void setChannel( const Channel_if_t * ch );

    /// @brief Gets the packet propagation channel.
    const Channel_if_t * getChannel()
        const;

    void setSourceProperties(node_properties_t & sp);


    const node_properties_t & getSourceProperties()
        const;

    /// @brief Get the sensed power when this packet was received.
    power_t getReceivedPower()
        const;

    /// @brief Set the sensed power when this packet was received.
    void setReceivedPower(const power_t power);

    //@}


    /// @name Support methods.
    //@{

    /// @brief Equality operator.
    /// We do not consider mixed payload types,
    /// nor simulation fields,
    /// nor fields that will be removed in the future.
    bool operator == ( const Packet_t & p )
        const;

    //@}


    /// @name To be removed methods.
    //@{

    /// @brief Gets the packet validity.
    bool isValid()
        const;

    /// @brief Sets the packet validity.
    void setValidity( const bool v );

    //@}

    /// @brief Static support methods.
    //@{

    /// @brief Gets a new packet id.
    static
    packet_id_t get_new_packet_id();


    /// @brief Gets the required bytes to encode the passed bits.
    static
    size_t to_bytes( const size_t s );

    /// @brief Gets the required bits to encode the passed bytes.
    static
    size_t to_bits( const size_t s );

    //@}

protected:

    /// @name Payload fields.
    //@{

    ///@brief The protocol packet encapsulated inside
    ProtocolPacket_if_t* _protocolPacket;

    //@}


    /// @brief Header fields.
    //@{

    /// @brief True if the packet is an ack.
    bool _is_ack;

    /// @brief True if this packet requires an ack as reply.
    bool _is_ack_required;

    /// @brief This packet sequence number.
    counter_t _sequence_number;

    /// @brief Label of the packet.
    label_t _label;

    /// @brief This header size in bits.
    size_t _header_size;

    /// @brief The sender taskproxy.
    const TaskProxy_if_t * _sourceTaskproxy;

    /// @brief The destination taskproxy, used to receive a message only for a specific task.
    const TaskProxy_if_t * _destTaskproxy;


    /// @brief The destination task.
    const Task_if_t * _destinationTask;

    /// @brief The source node.
    const Node_t * _sourceNode;

    /// @brief The destination node.
    const Node_t * _destinationNode;

    //@}

    /// @name Simulation fields.
    //@{

    /// @brief The packet id.
    packet_id_t _id;

    /// @brief The transmission channel.
    const Channel_if_t * _channel;

    /// @brief Sensed power when this packet was received.
    power_t _power;

    //@}

    /// @name To be removed.
    //@{

    /// @brief The validity flag.
    bool _is_valid;

    /// @brief Source node properties.
    node_properties_t  _sourceProperties;

    ///@brief the command the packet is carrying
    PacketCommand _command;

    ///@brief the packet type
    PacketType _type;

    //@}
};

} }


#endif
