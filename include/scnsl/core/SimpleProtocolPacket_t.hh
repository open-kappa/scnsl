// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_SIMPLE_PROTOCOL_PACKET_HH
#define SCNSL_SIMPLE_PROTOCOL_PACKET_HH

///@file The basic implementation of the ProtocolPacket interface used to store
///a buffer

#include "ProtocolPacket_if_t.hh"

namespace Scnsl { namespace Core {

/// @brief a simple implementation of the Protocol Packet for communication
/// between two taskproxy. Core information are stored inside. No header is
/// present, only a buffer for data This packet must be encapsulated.
class SCNSL_EXPORT SimpleProtocolPacket_t:
    public Scnsl::Core::ProtocolPacket_if_t
{
public:
    ///@brief Default constructor
    SimpleProtocolPacket_t();

    ///@brief copy constructor
    SimpleProtocolPacket_t(const SimpleProtocolPacket_t & p);

    ///@brief Default destructor
    ~SimpleProtocolPacket_t() override;

    ///@name Inherited method from the base class
    //@{

    size_t getPayloadSize() const override;

    void setPayload(const ProtocolPacket_if_t * packet) override;

    ProtocolPacket_if_t * getPayload() const override;

    byte_t * getInnerBuffer() const override;

    size_t getInnerSize() const override;

    SimpleProtocolPacket_t * clone() const override;

    bool operator==(const ProtocolPacket_if_t & p) const override;

    //@}

    /// @brief Assigment operator.
    SimpleProtocolPacket_t & operator=(const SimpleProtocolPacket_t & p);

    /// @brief Sets the buffer as payload.
    /// The buffer is copied and not aliased.
    ///
    /// @param b The buffer.
    /// @param s The size in bits.
    /// @throw std::bad_alloc If no enough memory is available.
    ///
    void setPayload(const byte_t * b, const size_t s);

    /// @brief Sets the buffer as payload.
    /// The buffer is copied and not aliased.
    ///
    /// @param b The buffer.
    /// @param s The size in bytes.
    /// @throw std::bad_alloc If no enough memory is available.
    ///
    void setPayloadInBytes(const byte_t * b, const size_t s);

private:
    /// @name Payload fields.
    //@{

    /// @brief The internal buffer.
    byte_t * _buffer;

    /// @brief The buffer size in bits.
    size_t _buffer_size;

    /// @brief The buffer capacity, in bytes.
    size_t _buffer_capacity;

    //@}
};
}}  // namespace Scnsl::Core
#endif
