// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_NODE_T_HH
#define SCNSL_CORE_NODE_T_HH



/// @file
/// A network node.

#include <stdexcept>
#include <map>

#include "../scnslConfig.hh"
#include "data_types.hh"
#include "Communicator_if_t.hh"

namespace Scnsl { namespace Core {

  class Channel_if_t;
  class Task_if_t;

} }


namespace Scnsl { namespace Core {

  /// @brief A netowrk generic node.
  ///
  /// Design patterns:
  /// - Non-copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT Node_t:
        public Communicator_if_t
  {
  public:

      /// @name Traits.
      //@{

      /// @brief The node properties.
      typedef Scnsl::Core::node_properties_t node_properties_t;

      /// @brief The node properties map.
      typedef std::map< const Channel_if_t *, node_properties_t > Properties_t;

      /// @brief The node id type.
      typedef Scnsl::Core::node_id_t node_id_t;

      //@}

      /// @brief Constructor.
      ///
      /// @param id the node id.
      ///
      Node_t(const node_id_t id);

      /// @brief Destructor.
      virtual
      ~Node_t();

      /// @brief Communicator interface methods.
      //@{

      // Since carrier is a status info, it is always propagated,
      // even in case othe node is down.
      // virtual
      // void setCarrier( const Channel_if_t * ch, const carrier_t c )
      //     ;

      virtual
      errorcode_t send( const Packet_t & p ) override;

      virtual
      errorcode_t receive( const Packet_t & p ) override;

      //@}

      /// @brief Erroneous Communicator_if_t methods.
      //@{

      virtual
      void bindTaskProxy( const TaskProxy_if_t * tp,
                 Communicator_if_t * c ) override;

      virtual
      void stackDown( Communicator_if_t * c ) override;

      //@}

      /// @name Node specific methods.
      //@{

      /// @brief Gets the node properties relatives to the channel.
      ///
      /// @param ch the channel.
      /// @return The node properties.
      ///
      const node_properties_t & getProperties( const Channel_if_t * ch )
          const ;

      /// @brief Sets a node properties.
      /// The no_update flags inhibits the update of related channel infos.
      /// This flag should be true only during the setup of the simulation.
      ///
      /// @param p The new properties.
      /// @param ch The channel to which these properties are referred.
      /// @param no_update Inhibits the update of related channel infos.
      ///
      void setProperties( const node_properties_t & p, Channel_if_t * ch, const bool no_update = false );

      /// @brief Gets node status.
      ///
      /// @return True if the node is active.
      ///
      bool getNodeStatus()
          const;

      /// @brief Sets the node status.
      ///
      /// @param s The new status.
      ///
      void setNodeStatus( const bool s );

      /// @brief Gets node id.
      ///
      /// @return the node id.
      ///
      node_id_t getId()
          const;

      //@}

  protected:

      /// @brief Node properties.
      Properties_t _properties;

      /// @brief The node status.
      /// By default, a node is active (status == true).
      bool _status;

      /// @brief Node id.
      const node_id_t _id;

  private:

      /// @brief Disabled copy constructor.
      Node_t( const Node_t & n );

      /// @brief Disabled assignemnet operator.
      Node_t & operator = ( const Node_t & n );


  };

} }



#endif
