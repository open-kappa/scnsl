
#ifndef SCNSL_TCP_ADDON_SOCKETMAP_HH
#define SCNSL_TCP_ADDON_SOCKETMAP_HH

#include "TaskProxy_if_t.hh"
#include "data_types.hh"

#include <map>

namespace Scnsl { namespace Core {

///@brief Class to store all information about network (ip, sockets) of the
///simualtor

class SCNSL_EXPORT SocketMap
{
public:
    ///@brief bind a taskpoxy with a specific socket
    ///@param tp the taskproxy
    ///@param sp the socket
    static void bindTask(const TaskProxy_if_t * tp, socket_properties_t sp);

    ///@brief bind a node with a specific ip address
    ///@param tp the node
    ///@param ip the ip address
    static void bindNodes(const Node_t * n, unsigned ip);

    ///@brief bind a hostname with a specific ip address
    ///@param host the hostname
    ///@param ip the ip address
    static void bindName(std::string host, unsigned ip);

    ///@brief return a socket from a specific taskproxy
    ///@param tp the taskproxy
    ///@return the socket bound to the taskproxy
    static socket_properties_t getSocket(const TaskProxy_if_t * tp);

    ///@brief return a taskpoxy from a specific socket
    ///@param s the socket
    ///@return the taskproxy bounded to the socket
    static const TaskProxy_if_t * getTp(const socket_properties_t s);

    ///@brief return a node from a specific ip
    ///@param ip the ip address
    ///@return the node bounded to the ip
    static const Node_t * getNode(const unsigned ip);

    ///@brief return a vector of ip address a node has
    ///@param node the node
    ///@return all the ips bound to the node
    static std::vector<unsigned> * getNetworkInterfaces(const Node_t * node);

    ///@brief return an ip address from a specific name
    ///@param host the host name
    static unsigned dnsRequest(std::string host);

    ///@brief return a host name from an ip
    ///@param ip the ip address
    static const std::string resolveIp(unsigned ip);

    ///@brief return the string format of an ip
    ///@param ip the ip address
    static const std::string getIP(unsigned ip);

    ///@brief return the value from an ip number
    ///@param ip the ip address
    static unsigned getIP(std::string ip);

    ///@brief add a taskproxy to a multicast group with same IP and port
    ///@param tp the Taskproxy to add
    ///@param ip the multicast IP
    ///@param port the port to receive mulitcast data
    static void addMuticastTP(const TaskProxy_if_t* tp, unsigned ip, 
                                unsigned short port);
    ///@brief return true if the given taskproxy is in a multicast group with the
    /// given port
    static bool isTpMulticast(const TaskProxy_if_t* tp, unsigned ip, 
                                unsigned short port);

    ///@brief return true if the ip is a mutlicast one
    static bool isIPMulticast(unsigned ip);                 
private:
    ///@brief a map containing the binding between taskproxys and sockets
    static std::map<const TaskProxy_if_t *, socket_properties_t> _taskBindings;
   
    ///@brief a map containing the binding between sockets and taskproxys
    static std::map<socket_properties_t, const TaskProxy_if_t *>_socketBindings;
   
    ///@brief a map containing the binding between nodes and its ip
    static std::map<unsigned, const Node_t *> _nodeIPs;
   
    ///@brief a map to bind a hostname with a secific ip (similar to a DNS)
    static std::map<std::string, unsigned> _dns;
   
    ///@brief a map containing the binding between an ip numerical value and its
    ///corresponding string format
    static std::map<unsigned, std::string> _ips;
   
    ///@brief next value to assign to an ip;
    static std::hash<std::string> _ip_hash;

    ///@brief List of taskProxies connected to the same multicast IP with realitve port
    static std::map<
        unsigned, std::vector<std::pair<unsigned short, const TaskProxy_if_t*>>
        > _multicast_groups;
};
}}  // namespace Scnsl::Core

#endif
