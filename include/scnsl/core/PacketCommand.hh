// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_PACKET_COMMAND_HH
#define SCNSL_PACKET_COMMAND_HH

#include "data_types.hh"

///@file Commands list that a ProtocolPacket can carry to communicators
namespace Scnsl { namespace Core {

    ///@brief All command available, add here custom commando for communicators
    enum PacketCommand: byte_t
    {
        NONE = 0b0, //no command
        TCP_CONNECT = 0b1,  //Request to start a tcp connection
        TCP_DISCONNECT = 0b10, //Request to release a tcp connection
        TCP_SND_SIZE= 0b11, //Update on the send buffer size
        TCP_NOBLOCK = 0b100,
    };
}}

#endif
