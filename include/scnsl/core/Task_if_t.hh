// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CORE_TASK_IF_T_HH
#define SCNSL_CORE_TASK_IF_T_HH



/// @file
/// A basic task interface.

#include <systemc>
#include <stdexcept>
#include <map>
#include <set>

#include "../scnslConfig.hh"
#include "data_types.hh"
#include "../tracing/Traceable_base_t.hh"

namespace Scnsl { namespace Core {

  class TaskProxy_if_t;
  class Node_t;

} }

namespace Scnsl { namespace Core {

  /// @brief The parent of all tasks.
  ///
  /// Design patterns:
  /// - Interface.
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT Task_if_t :
        public sc_core::sc_module,
        public Scnsl::Tracing::Traceable_base_t
  {
  public:

      /// @name Traits.
      //@{

      /// @brief A task ID.
      typedef Scnsl::Core::task_id_t task_id_t;

	  /// @brief The map of bound ids.
      typedef std::map<std::string, IdentifierNumber_t> BindIdMap;

      /// @brief Map from id to proxy
      typedef std::map<std::string, TaskProxy_if_t *> Proxies;
      //@}

      /// @brief Impure virtual destructor.
      virtual ~Task_if_t();

      /// @name Interface methods.
      //@{

      /// @brief Gets this task ID.
      task_id_t getId()
          const;

      /// @brief Gets the task node.
      Node_t * getNode();

      /// @brief Gets the task node.
      const Node_t * getNode()
          const;

      const node_properties_t & getNodeProperties(const std::string & id) const;

      void setNodeProperties(const std::string & id, const node_properties_t & properties, const bool noUpdate = false);

	  /// @brief Gets the number of associated proxies.
      virtual
	  size_t getProxiesNumber()
           const = 0;

	  /// @brief Gets the number of already bounded proxies.
      virtual
	  size_t getBoundedProxiesNumber()
           const = 0;

      //@}

  protected:

      /// @brief Constructor.
      ///
      /// @param name This module name.
      /// @param id this module unique ID.
      /// @param n The node on which this task is placed.
      ///
      Task_if_t( const sc_core::sc_module_name name,
                 const task_id_t id,
                 Node_t * n );

      /// @brief This task ID.
      const task_id_t _ID;

      /// @brief The node to which this task belongs.
      Node_t * _node;

      /// @brief Sanity check of the provided ID.
      std::set<IdentifierNumber_t> _bindId;

      /// @brief Map for binding ID.
      BindIdMap _bindIdMap;

      /// @brief Map for proxies.
      Proxies _proxies;

  private:

      /// @brief Disabled copy constructor.
      Task_if_t( const Task_if_t & t );

      /// @brief Disabled assignment operator..
      Task_if_t & operator = ( const Task_if_t & t );

  };


} }



#endif
