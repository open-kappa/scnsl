// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TLM_TLM_DATA_TYPES_HH
#define SCNSL_TLM_TLM_DATA_TYPES_HH



/// @file
/// TLM base types.

#include <systemc>
#include <tlm.h>

#include "../scnslConfig.hh"

namespace Scnsl { namespace Tlm {

  /// @name Generic TLM types.
  //@{

  /// @brief Initiator socket type.
  typedef tlm::tlm_initiator_socket<  >   InitiatorSocket_t;

  /// @brief Target socket type.
  typedef tlm::tlm_target_socket<  > TargetSocket_t;

  /// @brief The type used for TLM commands.
  typedef tlm::tlm_command tlm_command_t;

  /// @brief The TLM taskproxy ID type.
  typedef sc_dt::uint64 tlm_taskproxy_id_t;

  //@}

  /// @brief Constants.
  enum Constants
  {
      /// @brief A packet command.
      PACKET_COMMAND = tlm::TLM_READ_COMMAND,

      /// @brief A carrier command.
      CARRIER_COMMAND = tlm::TLM_WRITE_COMMAND,

      /// @brief A Packet that store a command payload
      COMMAND_PAYLOAD = tlm::TLM_IGNORE_COMMAND
  };

} }



#endif
