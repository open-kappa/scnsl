// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TLM_TLMTASK_IF_T_HH
#define SCNSL_TLM_TLMTASK_IF_T_HH



/// @file
/// A TLM task.

#include <iostream>
#include <stdexcept>
#include <tlm.h>

#include "../scnslConfig.hh"

#include "../core/data_types.hh"
#include "../core/Task_if_t.hh"

#include "tlm_data_types.hh"
#include "../setup/setup_data_types.hh"

namespace Scnsl { namespace Tlm {

  class TlmTaskProxy_t;

} }

namespace Scnsl { namespace Tlm {


  /// @brief A TLM task interface.
  /// TLM tasks are both initiators and targets.
  /// Used a loosely timed blocking protocol.
  ///
  /// Generic payload fields used:
  /// - data_ptr & data_length: exchanged data.
  /// - commnad: the packet or carrier action.
  /// - address: the taskproxy id.
  /// - response_status: TLM_OK_RESPONSE.
  ///
  /// Design patterns:
  /// - Interface.
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT TlmTask_if_t:
        public Scnsl::Core::Task_if_t,
        public tlm::tlm_fw_transport_if<>,
		public tlm::tlm_bw_transport_if<>
  {
  public:

      /// @name Traits.
      //@{

      /// @brief The byte type.
      typedef Scnsl::Core::byte_t byte_t;

      /// @brief The size type.
      typedef Scnsl::Core::size_t size_t;

      /// @brief Initiator socket type.
      typedef Scnsl::Tlm::InitiatorSocket_t InitiatorSocket_t;

      /// @brief Target socket type.
      typedef Scnsl::Tlm::TargetSocket_t TargetSocket_t;

      /// @brief The taskproxy ID type.
      typedef Scnsl::Tlm::tlm_taskproxy_id_t tlm_taskproxy_id_t;

	  /// @brief The label type.
	  typedef Scnsl::Core::label_t label_t;

      //@}

      /// @brief Initiator sockets array.
      InitiatorSocket_t * initiators;

      /// @brief Target sockets array.
      TargetSocket_t * targets;

      /// @brief Impure virtual destructor.
      virtual
      ~TlmTask_if_t() = 0;

	  // BEGIN - Forward interface implementation
      virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload & p, tlm::tlm_dmi & dmi_data) override;

      virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload & p, tlm::tlm_phase& phase, sc_core::sc_time & t) override;

      virtual unsigned int transport_dbg(tlm::tlm_generic_payload & p) override;
	  // END - Forward interface implementation

	  // BEGIN - Backward interface implementation
      virtual void invalidate_direct_mem_ptr(sc_dt::uint64 start_range, sc_dt::uint64 end_range) override;

      virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload & p, tlm::tlm_phase& phase, sc_core::sc_time & t) override;

	  // END - Backward interface implementation

      /// @name Interface methods.
      //@{

      /// @brief The transport interface method to be implemented.
      /// Reminder for children classes.
      ///
      /// @param p The payload received.
      /// @param t The delay. Ignored.
      ///
      virtual
      void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ) override = 0;

      /// @brief Binds a new taskproxy, setting also its ID.
      ///
      /// @param tp The taskproxy.
      /// @param bindIdentifier The identifier assigned to the task proxy.
      /// @throw std::logic_error All available tasks are altrady binded.
      ///
      void bindTaskProxy( TlmTaskProxy_t * tp, std::string bindIdentifier );


	  /// @brief Gets the number of associated proxies.
      virtual
      size_t getProxiesNumber()
          const override;

      /// @brief Gets the number of already bounded proxies.
      virtual
      size_t getBoundedProxiesNumber()
          const override;

      //@}

  protected:

      /// @brief Constructor.
      ///
      /// @param name This module name.
      /// @param id this module unique ID.
      /// @param n The node on which this task is placed.
      /// @param proxies The number of connected task proxies.
      /// @throw std::invalid_argument If proxies is zero.
      ///
      TlmTask_if_t( sc_core::sc_module_name name,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies );


      /// @name Convenience methods.
      //@{

      /// @brief Sends a packet.
      ///
      /// @param tpKey The task proxy to be used.
      /// @param buffer The buffer with the data.
      /// @param s The size of the buffer in bytes.
	  /// @param label The label that represents the packet priority. Optional, default value 0.
      ///
      void send(const std::string & tpKey, byte_t * buffer, const size_t s, const label_t label = 0 );

      /// @brief Sends a packet.
      ///
      /// @param tp The task proxy to be used.
      /// @param buffer The buffer with the data.
      /// @param s The size of the buffer in bytes.
	  /// @param label The label that represents the packet priority. Optional, default value 0.
      ///
      void send(const tlm_taskproxy_id_t tp, byte_t * buffer, const size_t s, const label_t label = 0 );

      //@}

      /// @brief The number of already binded proxies.
      size_t _bounded_proxies;

      /// @brief The number of connected proxies.
      const size_t _PROXIES_NUMBER;

      /// @brief The sent payload.
      tlm::tlm_generic_payload _payload;

      /// @brief The sent delay time.
      sc_core::sc_time _delay;

  private:

      /// @brief Disabled copy constructor.
      TlmTask_if_t( const TlmTask_if_t & );

      /// @brief Disabled assignement operator.
      TlmTask_if_t & operator = ( const TlmTask_if_t & );

  };

} }



#endif
