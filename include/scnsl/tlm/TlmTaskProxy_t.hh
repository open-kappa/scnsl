// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TLM_TLMTASKPROXY_T_HH
#define SCNSL_TLM_TLMTASKPROXY_T_HH



/// @file
/// TLM task proxy.

#include <tlm.h>

#include "../scnslConfig.hh"

#include "../core/Packet_t.hh"

#include "tlm_data_types.hh"
#include "../core/TaskProxy_if_t.hh"
#include "../core/data_types.hh"

namespace Scnsl { namespace Tlm {

  /// @brief TLM task proxy.
  /// A TLM proxy is both an initiator and a target.
  /// Adopted a loosely timed blocking protocol.
  ///
  /// Generic payload fields used:
  /// - data_ptr & data_length: exchanged data.
  /// - command: the packet or carrier action.
  /// - address: the taskproxy id.
  /// - response_status: TLM_OK_RESPONSE.
  ///
  /// Design patterns:
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT TlmTaskProxy_t :
        public Scnsl::Core::TaskProxy_if_t,
        public tlm::tlm_fw_transport_if<>,
		public tlm::tlm_bw_transport_if<>
  {
  public:

      /// @name Traits.
      //@{

      /// @brief Initiator socket type.
      typedef Scnsl::Tlm::InitiatorSocket_t InitiatorSocket_t;

      /// @brief Target socket type.
      typedef Scnsl::Tlm::TargetSocket_t TargetSocket_t;

      /// @brief The internal packet format.
      typedef Scnsl::Core::Packet_t Packet_t;

      /// @brief The taskproxy ID type.
      typedef Scnsl::Tlm::tlm_taskproxy_id_t tlm_taskproxy_id_t;

      //@}

      /// @brief The initiator socket.
      InitiatorSocket_t initiator;

      /// @brief The target socket.
      TargetSocket_t target;

      /// @brief Constructor.
      ///
      /// @param name This module name.
      /// @param t The relative task.
      /// @param bindIdentifier The identifier of the task proxy.
      /// @param ch The relative channel.
      /// @throw std::invalid_argument The task is not child of TlmTask_if_t.
      /// @throw std::logic_error The task has already bounded all available proxies.
      ///
      TlmTaskProxy_t(
          const sc_core::sc_module_name name,
          Scnsl::Core::Task_if_t * t,
          std::string bindIdentifier,
          Scnsl::Core::Channel_if_t * ch );

      /// @brief Destructor.
      virtual
      ~TlmTaskProxy_t();

	  // BEGIN - Forward interface implementation
      virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload & p, tlm::tlm_dmi & dmi_data) override;

      virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload & p, tlm::tlm_phase& phase, sc_core::sc_time & t) override;

      virtual unsigned int transport_dbg(tlm::tlm_generic_payload & p) override;
	  // END - Forward interface implementation


	  // BEGIN - Backward interface implementation
      virtual void invalidate_direct_mem_ptr(sc_dt::uint64 start_range, sc_dt::uint64 end_range) override;

      virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload & p, tlm::tlm_phase& phase, sc_core::sc_time & t) override;

	  // END - Backward interface implementation



      /// @name Communicator interface methods.
      //@{

      virtual
      void setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c ) override;

      virtual
      errorcode_t receive( const Packet_t & p ) override;

      //@}

      /// @name TLM interface methods.
      //@{

      /// @brief The transport interface method to be implemented.
      /// Reminder for children classes.
      ///
      /// @param p The payload received.
      /// @param t The delay. Ignored.
      ///
      virtual
      void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ) override;

      //@}

      /// @name Support methods.
      //@{

      void setId( const tlm_taskproxy_id_t id );

      //@}

  protected:

      /// @brief The payload sent to the node.
      tlm::tlm_generic_payload _payload;

      /// @brief The internal conversion packet.
      Packet_t _packet;

      /// @brief Fake delay for b_transport.
      sc_core::sc_time _delay;

      /// @brief The new carrier value.
      carrier_t _carrier;

      /// @brief This taskproxy ID.
      tlm_taskproxy_id_t _id;

	 /// @brief This taskbind ID.
      std::string _taskId;
  private:

      /// @brief Disabled copy constructor.
      TlmTaskProxy_t ( const TlmTaskProxy_t & );

      /// @brief Disabled assigmenemt operator.
      TlmTaskProxy_t & operator = ( const TlmTaskProxy_t & );
  };

} }



#endif
