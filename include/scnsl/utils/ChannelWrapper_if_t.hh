// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_UTILS_CHANNELWRAPPER_IF_T_HH
#define SCNSL_UTILS_CHANNELWRAPPER_IF_T_HH



/// @file
/// Encompasses a stack of communicators.

#include "../scnslConfig.hh"
#include "../core/Channel_if_t.hh"


namespace Scnsl { namespace Utils {

	class ChannelBridge_t;

  } }

namespace Scnsl { namespace Utils {


    /// @brief A wrapper of channels.
    /// They are encompassed, to create more complex behaviors.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT ChannelWrapper_if_t:
        public Scnsl::Core::Channel_if_t
    {
    public:

        /// @brief Impure virtual destructor.
        virtual
        ~ChannelWrapper_if_t();

        /// @name Interface methods.
        //@{


      	virtual
        void addNode( Scnsl::Core::Node_t * n ) override = 0;

      	virtual
        errorcode_t send( Scnsl::Core::Node_t * n, const Scnsl::Core::Packet_t & p ) override = 0;

      	virtual
      	bool isMultipoint()
            const override = 0;

	    //@}

 		/// @name Methods for interconnection with the internal bridge.
        //@{

        /// @brief The setCarrier() method called by the bridge.
        virtual
		void bridgeSetCarrier( const Scnsl::Utils::ChannelBridge_t * cb, const Scnsl::Core::carrier_t c ) = 0;

        /// @brief The receive() method called by the bridge.
        virtual
		errorcode_t bridgeReceive( const Scnsl::Utils::ChannelBridge_t * cb, const Scnsl::Core::Packet_t & p ) = 0;

        //@}

    protected:

        /// @brief Constructor.
        ///
        /// @param modulename This module name
        ///
		explicit
        ChannelWrapper_if_t( const sc_core::sc_module_name modulename );


    private:

        /// @brief Disabled copy constructor.
        ChannelWrapper_if_t( const ChannelWrapper_if_t & );

        /// @brief Disabled assignement operator.
        ChannelWrapper_if_t & operator = ( const ChannelWrapper_if_t & );
    };


  } }



#endif
