// -*- SystemC -*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_EVENTS_QUEUE_T_HH
#define SCNSL_EVENTS_QUEUE_T_HH

/// @file
/// A event dispatcher queue.

#include <list>
#include <systemc>

#include "../scnslConfig.hh"
#include "../tracing/Traceable_base_t.hh"
#include "../core/data_types.hh"
#include "../core/Node_t.hh"
#include "../core/Channel_if_t.hh"

namespace Scnsl { namespace Utils {

  /// @brief This class is a priority queue based on absolute time.
  /// It is useful to schedule events such as a node movement.
  /// When the scheduled time elapse, the associated action is performed.
  ///
  /// Design patterns:
  /// - Non copyable.
  /// - Non assignable.
  /// - Singleton.
  ///
  class SCNSL_EXPORT EventsQueue_t :
        public sc_core::sc_module,
        public Scnsl::Tracing::Traceable_base_t
  {
  public:

      /// @name Traits.
      //@{

      /// @brief The node properties type.
      typedef Scnsl::Core::node_properties_t node_properties_t;

      /// @brief The node type.
      typedef Scnsl::Core::Node_t Node_t;

      /// @brief The channel type.
      typedef Scnsl::Core::Channel_if_t Channel_if_t;

      /// @brief The event ID type.
      typedef Scnsl::Core::event_id_t event_id_t;

      struct base_instance_data_t
      {
          virtual
          ~base_instance_data_t();

          virtual
          void call() = 0;
      };

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4371)
#endif
      template< class T, class D >
      struct instance_data_t
      {
          T * obj;
          void (T::* callback)( D * );
          D * data;

          instance_data_t():
              obj( nullptr ),
              callback( nullptr ),
              data( nullptr )
          {}

          virtual
          ~instance_data_t(){}

          virtual
          void call()
          {
              (obj->*callback)( data );
          }

      private:

          /// @brief Disabled.
          instance_data_t( const instance_data_t & );

          /// @brief Disabled.
          instance_data_t & operator = ( const instance_data_t & );
      };
#ifdef _MSC_VER
#pragma warning(pop)
#endif

      struct node_instance_data_t
      {
    	  Node_t * n;
    	  node_properties_t np;
    	  Channel_if_t * ch;

          node_instance_data_t();

          virtual
          ~node_instance_data_t();

          virtual
          void call();

      private:

          /// @brief Disabled.
          node_instance_data_t( const node_instance_data_t & ) = delete;

          /// @brief Disabled.
          node_instance_data_t & operator = ( const node_instance_data_t & ) = delete;
      };


      /// @brief This <tt>enum</tt> defines constants about the kind of struct.
      enum data_type_t
      {
          EVENT_DATA           = 0,
          METHOD_DATA          = 1,
          INSTANCE_METHOD_DATA = 2
      };

      /// @brief This struct is used to store an event to be dispatched.
      /// This struct also implements a comparison operator in order to deal with data structures.
      ///
      struct queue_event_t
      {
          sc_core::sc_time time;          ///< The time into which fire the event.
          void ( * action )( void * );    ///< The function to call as event firing.
          void * data;                    ///< The data to be passed to the function called.
          data_type_t type;               ///< The type of the pointed data.
          event_id_t id;                  ///< The ID of the event.

          queue_event_t();

          /// @brief Comparison operator.
          bool operator < ( const queue_event_t & e )
              const;

          /// @brief Equality operator.
          bool operator == ( const queue_event_t & e )
              const;
      };

      //@}


      SC_HAS_PROCESS( EventsQueue_t );


      /// @name Singleton design pattern.
      //@{

      /// @brief Gets this class instance.
      /// The name of the instance will be "EventsQueue".
      static
      EventsQueue_t * get_instance();

      //@}


      /// @name Events management functions.
      /// These methods do not hold pointers ownerships, but such pointers must
      /// be valid till event completion.
      /// The passed time must be an absolute simulation time.
      /// Objects passed as references are copied (and hence a copy constructor
      /// is required ) and managed by the EventsQueue_t.
      /// The callback implementations shall not block the caller.
      //@{


      /// @brief Registers an event made by an sc_core::event to be fired.
      ///
      /// @param time The simulation time into which fire the action.
      /// @param event The event to fire.
      ///
      event_id_t registerAction( const sc_core::sc_time & time, sc_core::sc_event * event );


      /// @brief Registers a function with a parameter to be called.
      ///
      /// @param time The simulation time into which fire the action.
      /// @param callback The function to call.
      /// @param data The data to pass to the function.
      ///
      template< typename T >
      event_id_t registerAction( const sc_core::sc_time & time,
                                 void callback( T * ),
                                 T * data );


      /// @brief Registers an instance method to be called.
      ///
      /// @param time The simulation time into which fire the action.
      /// @param obj The object.
      /// @param callback The function to call.
      /// @param data The data to pass to the function.
      ///
      template< typename T, typename D >
      event_id_t registerAction( const sc_core::sc_time & time,
                                 T * const obj,
                                 void (T::* callback)( D * ),
                                 D * data );


      /// @brief Register a node movement to be performed.
      ///
      /// @param time The simulation time into which fire the action.
      /// @param n The node.
      /// @param np The node properties.
      /// @param ch The channel.
      ///
      event_id_t registerNodeProperties( const sc_core::sc_time & time,
    		                             Node_t * n,
    		                             node_properties_t np,
    		                             Channel_if_t * ch );


      /// @brief Remove the ID-relative action.
      ///
      /// @param event_id The ID of the event to remove.
      /// @throw std::domain_error No event found in the queue.
      ///
      void removeAction( event_id_t event_id );


      /// @brief Return the event-relative time.
      ///
      /// @param event_id The ID of the event.
      /// @throw std::domain_error No event found in the queue.
      ///
      const sc_core::sc_time & getActionTime( const event_id_t event_id );


      /// @brief Update the event-relative time.
      ///
      /// @param event_id The ID of the event.
      /// @param time The time into witch fire the action.
      /// @throw std::domain_error No event found in the queue.
      ///
      void updateAction( event_id_t event_id, const sc_core::sc_time & time );

      //@}

  protected:

      /// @brief Constructor.
      ///
      /// @param modulename This module name.
      ///
      explicit
      EventsQueue_t( sc_core::sc_module_name modulename );


      /// @brief Destructor.
      ~EventsQueue_t();


      /// @name Support callback methods.
      //@{

      /// @brief Notifies an event.
      ///
      /// @param d A data of type sc_core::sc_event.
      ///
      static
      void _event_action_performer( void * d );

      /// @brief Calls an instance method.
      ///
      /// @param d A data of type instance_data_t.
      ///
      static
      void _instance_callback_action_performer( void * d );

      //@}


      /// @name Protected fields.
      //@{

      /// This event is fired when the head of the queue is changed.
      sc_core::sc_event _headInsertedEvent;

      /// This is the events list.
      std::list< queue_event_t > _queue;

      //@}

  private:

      /// @name Processes.
      //@{

      /// @brief This process dispatch events and performs required waits.
      void _process();

      //@}

      /// @name Support sorting method.
      //@{

      /// @brief This method performed a comparison between events.
      static
      bool _compareTime( queue_event_t first_e, queue_event_t second_e);

      //@}

      /// @brief Disabled copy constructor.
      EventsQueue_t( const EventsQueue_t & eventsQueue );



      /// @brief Disabled assignement operator.
      EventsQueue_t & operator = ( const EventsQueue_t & eventsQueue );


      /// @brief The ID of the event.
      event_id_t _event_id;

  };


  } }


namespace Scnsl { namespace Utils {

    //
    // Implementation of template methods.
    //

    template< typename T >
    EventsQueue_t::event_id_t EventsQueue_t::registerAction( const sc_core::sc_time & time,
                                                void callback( T * ),
                                                T * data )
    {
        SCNSL_TRACE_DBG( 3, "<> registerAction(): callback." );

        // Storing the event-relative id:
        event_id_t id = _event_id;

        queue_event_t queue_event;
        queue_event.time = time;
        queue_event.action = reinterpret_cast< void (*)( void * ) >( & callback );
        queue_event.data = reinterpret_cast< void * >( data );
        queue_event.type = METHOD_DATA;
        queue_event.id = id;

        ++_event_id;

        // Storing the event in the _queue:
        _queue.push_back( queue_event );
        // Sorting the _queue:
        _queue.sort(_compareTime);
        if ( time <= _queue.front().time ) _headInsertedEvent.notify();

        // Return the event-relative id:
        return id;
    }


    template< typename T, typename D >
    EventsQueue_t::event_id_t  EventsQueue_t::registerAction( const sc_core::sc_time & time,
                                                 T * const obj,
                                                 void (T::* callback)( D * ),
                                                 D * data )
    {
        SCNSL_TRACE_DBG( 3, "<> registerAction(): instance callback." );

        // Storing the event-relative id:
        event_id_t id = _event_id;

        instance_data_t< T, D > * d = new instance_data_t< T, D >();

        d->obj = obj;
        d->callback = callback;
        d->data = data;

        queue_event_t queue_event;
        queue_event.time = time;
        queue_event.action = & _instance_callback_action_performer;
        queue_event.data = reinterpret_cast< void * >( d );
        queue_event.type = INSTANCE_METHOD_DATA;
        queue_event.id = id;

        ++_event_id;

        // Storing the event in the _queue:
        _queue.push_back( queue_event );
        // Sorting the _queue:
        _queue.sort(_compareTime);
        if ( time <= _queue.front().time ) _headInsertedEvent.notify();

        // Return the event-relative id:
        return id;
    }

  } } // End of template method implementation namespace

#endif
