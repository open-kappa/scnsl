// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_UTILS_DIRECTIONALENVIRONMENT_T_HH
#define SCNSL_UTILS_DIRECTIONALENVIRONMENT_T_HH

/// @file
/// Handles the physical communication between wireless nodes and allows
/// the option of using nodes based on directional radiation patterns.

#include "../core/Packet_t.hh"
#include "../core/Node_t.hh"
#include "Environment_if_t.hh"

namespace Scnsl { namespace Utils {

/// @brief This class handles the physical communication between
/// wireless nodes and it has been built to operate with nodes having
/// directional radiation patterns also if these are made of many lobes,
/// that could be in turn directional or omnidirectional. It is possible
/// to adopt standard omnidirectional nodes, having no directional lobes,
/// together with nodes having multi-directional patterns made of many
/// lobes, all in the same scenario. The class handles every possible
/// hybrid case, and it can also correctly manage the scenario with only
/// standard omnidirectional nodes, producing in this case the same
/// behaviour of the class DefaultEnvironment_t.
///
class SCNSL_EXPORT DirectionalEnvironment_t:
        public Environment_if_t
{
public:
    /// @brief The node properties.
    typedef Scnsl::Core::node_properties_t node_properties_t;
    typedef Scnsl::Core::propagation_t propagation_t;
    typedef Scnsl::Core::position_t position_t;

    DirectionalEnvironment_t(const double alpha = 1,
                             const double radio_freq = 0,
                             const propagation_t propagation = 1);
    ~DirectionalEnvironment_t();
    static void createInstance(const double alpha = 1,
                               const double radio_freq = 0,
                               const propagation_t propagation = 1);


    /// @brief Gets Bit error rate.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getBitErrorRate(const node_properties_t & sp,
                                   const node_properties_t & rp) override;

    /// @brief Gets recive power.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getReceiverPower( const node_properties_t & sp,
                                     const node_properties_t & rp) override;

    /// @brief Gets maximum delay.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getMaxDelay( const node_properties_t & sp,
                                const node_properties_t & rp) override;

    /// @brief Gets propagation value.
    ///
    ///
    virtual Scnsl::Core::propagation_t getPropagation() override;

protected:
    /// @brief The attenuation exponent.
    const double _ALPHA_EXPONENT;
    const double _RADIO_FREQ;
    const propagation_t _PROPAGATION;

private:
    DirectionalEnvironment_t(const DirectionalEnvironment_t&);
    DirectionalEnvironment_t & operator =(const DirectionalEnvironment_t&);

    /// @brief Returns true if the difference between two double numbers
    /// is less than std::numeric_limits<double>::epsilon()
    bool _doubleEqual(const double a, const double b);

    /// @brief Computes distance between the positions of two nodes
    double _getDistance(const node_properties_t & sp,
                        const node_properties_t & rp);
};

} }

#endif
