// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_UTILS_COMMUNICATORSTACK_T_HH
#define SCNSL_UTILS_COMMUNICATORSTACK_T_HH



/// @file
/// Encompasses a stack of communicators.

#include "../scnslConfig.hh"
#include "../core/Communicator_if_t.hh"
#include "CommunicatorBridge_t.hh"

namespace Scnsl { namespace Utils {

    /// @brief A stack of communicators.
    /// They are encompassed, to simplify instantation.
    ///
    /// This class does not holds the ownership of the encompassed communicators.
    /// If this is a wanted behaviour, register a communicator for deletion,
    /// calling the acquireOwnership() method.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT CommunicatorStack_t:
        public Scnsl::Core::Communicator_if_t
    {
    public:

        /// @brief Constructor.
        ///
        /// @param head The highest leve communicator.
        /// @param tail The lowest level communicator.
        ///
        CommunicatorStack_t( Scnsl::Core::Communicator_if_t * head,
                             Scnsl::Core::Communicator_if_t * tail );

        /// @brief Virtual destructor.
        virtual
        ~CommunicatorStack_t();

        /// @brief Support methods.
        //@{

        /// @brief Acquires the ownership of the passed communicator.
        ///
        /// @param c The communicator.
        ///
        void acquireOwnership( Scnsl::Core::Communicator_if_t * c );

        //@}

        /// @name Interface methods.
        //@{

        virtual
        void setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c ) override;

        virtual
        errorcode_t send( const Scnsl::Core::Packet_t & p ) override;

        virtual
        errorcode_t receive( const Scnsl::Core::Packet_t & p ) override;

/* TODO CHECK THIS
        virtual
        void bindTaskProxy( const Scnsl::Core::TaskProxy_if_t * tp,
                            Scnsl::Core::Communicator_if_t * c );

        virtual
        void bindChannel( const Scnsl::Core::Channel_if_t * ch,
                          Scnsl::Core::Communicator_if_t * c );

        virtual
        void stackUp( Scnsl::Core::Communicator_if_t * c );

        virtual
        void stackDown( Scnsl::Core::Communicator_if_t * c );
*/
        //@}


        /// @name Methods for interconnection with the internal bridge.
        //@{

        /// @brief The setCarrier() method called by the bridge.
        void bridgeSetCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c );

        /// @brief The send() method called by the bridge.
        errorcode_t bridgeSend( const Scnsl::Core::Packet_t & p );

        /// @brief The receive() method called by the bridge.
        errorcode_t bridgeReceive( const Scnsl::Core::Packet_t & p );

        //@}

    protected:

        /// @brief The topmost communicator of the chain.
        Scnsl::Core::Communicator_if_t * _head;

        /// @brief The bottom communicator of the chain.
        Scnsl::Core::Communicator_if_t * _tail;

        /// @brief List of communicators owned.
        CommunicatorList_t _ownedCommunicators;

        /// @brief Internal communicator bridge.
        CommunicatorBridge_t _bridge;

    private:

        /// @brief Disabled copy constructor.
        CommunicatorStack_t( const CommunicatorStack_t & );

        /// @brief Disabled assignement operator.
        CommunicatorStack_t & operator = ( const CommunicatorStack_t & );
    };


  } }



#endif
