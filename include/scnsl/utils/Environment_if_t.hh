// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_UTILS_ENVIRONMENT_IF_T_HH
#define SCNSL_UTILS_ENVIRONMENT_IF_T_HH

#include "../scnslConfig.hh"
#include "../core/Packet_t.hh"
#include "../core/Node_t.hh"

namespace Scnsl { namespace Utils {


class SCNSL_EXPORT Environment_if_t
{
public:

    /// @brief The node properties.
    typedef Scnsl::Core::node_properties_t node_properties_t;

    virtual
    ~Environment_if_t();

    static Environment_if_t* getInstance( );

    /// @brief Gets Bit error rate.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getBitErrorRate(const node_properties_t & sp,
                                   const node_properties_t & rp) = 0;

    /// @brief Gets recive power.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getReceiverPower( const node_properties_t & sp,
                                     const node_properties_t & rp) = 0;

    /// @brief Gets maximum delay.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getMaxDelay( const node_properties_t & sp,
                                const node_properties_t & rp) = 0;

    /// @brief Gets propagation value.
    ///
    ///
    virtual Scnsl::Core::propagation_t getPropagation() = 0;

protected:

    Environment_if_t();

    /// @brief Copy constructor.
    Environment_if_t( const Environment_if_t & e );

    /// @brief Assignment operator.
    Environment_if_t & operator = ( const Environment_if_t & e );

    static bool _isDoubleEquals(const double d1, const double d2);

    static Environment_if_t * _env;
};



} }

#endif
