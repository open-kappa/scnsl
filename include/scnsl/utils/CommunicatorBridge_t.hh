// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_UTILS_COMMUNICATORBRIDGE_T_HH
#define SCNSL_UTILS_COMMUNICATORBRIDGE_T_HH



/// @file
/// Links a CommunicatorStack with a Communicator chain.

#include "../scnslConfig.hh"
#include "../core/Communicator_if_t.hh"

namespace Scnsl { namespace Utils {

    class CommunicatorStack_t;

  } }

namespace Scnsl { namespace Utils {

    /// @brief Links a CommunicatorStack with a Communicator chain.
    /// This class is intended as an internal class, not directly available to users.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT CommunicatorBridge_t:
        public Scnsl::Core::Communicator_if_t
    {
    public:

        /// @brief Constructor.
        ///
        /// @param cs The associated communicator stack.
        ///
        CommunicatorBridge_t( CommunicatorStack_t * cs );

        /// @brief Virtual destructor.
        virtual
        ~CommunicatorBridge_t();

        /// @name Interface methods.
        /// stackUp() and stackDown() are not reimplemented.
        //@{

        virtual
        void setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c ) override;

        virtual
        errorcode_t send( const Scnsl::Core::Packet_t & p ) override;

        virtual
        errorcode_t receive( const Scnsl::Core::Packet_t & p ) override;

        /// Throws.
        virtual
        void bindTaskProxy( const Scnsl::Core::TaskProxy_if_t * tp,
                            Communicator_if_t * c ) override;

        /// Throws.
        virtual
        void bindChannel( const Scnsl::Core::Channel_if_t * ch,
                          Scnsl::Core::Communicator_if_t * c ) override;

        /// Throws.
        virtual
        void stackUp( Scnsl::Core::Communicator_if_t * c ) override;

        /// Throws.
        virtual
        void stackDown( Scnsl::Core::Communicator_if_t * c ) override;


        //@}

    protected:

        /// @brief The associated communicator stack.
        CommunicatorStack_t * _communicatorStack;

    private:

        /// @brief Disabled copy constructor.
        CommunicatorBridge_t( const CommunicatorBridge_t & );

        /// @brief Disabled assignement operator.
        CommunicatorBridge_t & operator = ( const CommunicatorBridge_t & );

    };

  } }

#endif
