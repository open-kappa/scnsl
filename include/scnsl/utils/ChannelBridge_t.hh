// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_UTILS_CHANNELBRIDGE_T_HH
#define SCNSL_UTILS_CHANNELBRIDGE_T_HH



/// @file
/// Links a CommunicatorStack with a Communicator chain.

#include "../scnslConfig.hh"
#include "../core/Node_t.hh"
#include "../core/Packet_t.hh"
#include "ChannelWrapper_if_t.hh"

namespace Scnsl { namespace Channels {

    class FullDuplexChannel_t;
    class HalfDuplexChannel_t;

  } }

namespace Scnsl { namespace Utils {

    /// @brief Links a CommunicatorStack with a Communicator chain.
    /// This class is intended as an internal class, not directly available to users.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT ChannelBridge_t:
        public Scnsl::Core::Node_t
    {
    public:

        /// @brief Constructor.
        ///
        /// @param cw The associated communicator stack.
        ///
        ChannelBridge_t( ChannelWrapper_if_t * cw );

        /// @brief Virtual destructor.
        virtual
        ~ChannelBridge_t();

        /// @name Interface methods.

        //@{

        virtual
        errorcode_t send( const Scnsl::Core::Packet_t & p ) override;

        /// Throws.
        virtual
        void bindTaskProxy( const Scnsl::Core::TaskProxy_if_t * tp,
                            Scnsl::Core::Communicator_if_t * c ) override;

        /// Throws.
        virtual
        void stackDown( Scnsl::Core::Communicator_if_t * c ) override;

		/// Throws.
		virtual
        void setCarrier( const Scnsl::Core::Channel_if_t * ch, const carrier_t c ) override;

      	virtual
        errorcode_t receive( const Scnsl::Core::Packet_t & p ) override;

        //@}

    protected:

        /// @brief The associated communicator stack.
        ChannelWrapper_if_t * _channelWrapper;

    private:

        /// @brief Disabled copy constructor.
        ChannelBridge_t( const ChannelBridge_t & );

        /// @brief Disabled assignement operator.
        ChannelBridge_t & operator = ( const ChannelBridge_t & );

    };

  } }

#endif
