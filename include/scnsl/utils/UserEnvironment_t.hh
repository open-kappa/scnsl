// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_UTILS_USERENVIRONMENT_T_HH
#define SCNSL_UTILS_USERENVIRONMENT_T_HH

#include "../scnslConfig.hh"
#include "../core/Packet_t.hh"
#include "../core/Node_t.hh"
#include "Environment_if_t.hh"

namespace Scnsl { namespace Utils {


class SCNSL_EXPORT UserEnvironment_t:
    public Environment_if_t
{
public:

    typedef Scnsl::Core::node_properties_t node_properties_t;
    typedef Scnsl::Core::propagation_t propagation_t;
    typedef Scnsl::Core::position_t position_t;
    typedef double (*receiverPowerFunction_t) (
      const node_properties_t & sp,
      const node_properties_t & rp);

    UserEnvironment_t(receiverPowerFunction_t function, const double alpha = 1,
      const propagation_t propagation = 1);

    ~UserEnvironment_t();

    static void createInstance(receiverPowerFunction_t function,
      const double alpha = 1, const propagation_t propagation = 1);

    /// @brief Gets Bit error rate.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getBitErrorRate(const node_properties_t & sp,
                                   const node_properties_t & rp) override;

    /// @brief Gets receiver power.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getReceiverPower( const node_properties_t & sp,
                                     const node_properties_t & rp) override;

    /// @brief Gets maximum delay.
    ///
    /// @param sp source node properties.
    /// @param rp destination node properties.
    ///
    virtual double getMaxDelay( const node_properties_t & sp,
                                const node_properties_t & rp) override;

    /// @brief Gets propagation value.
    ///
    ///
    virtual Scnsl::Core::propagation_t getPropagation() override;

protected:
    // @brief The user getReceiverPower function.
    receiverPowerFunction_t _getReceiverPower;

    /// @brief The attenuation exponent.
    const double _ALPHA_EXPONENT;
    const propagation_t _PROPAGATION;

private:
	UserEnvironment_t(const UserEnvironment_t &);
	UserEnvironment_t & operator =(const UserEnvironment_t &);
};

} }

#endif
