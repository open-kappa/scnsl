// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRAFFIC_PIT_TASK_T_HH
#define SCNSL_TRAFFIC_PIT_TASK_T_HH



/// @file
/// Pit task.


#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../tlm/TlmTask_if_t.hh"

namespace Scnsl { namespace Traffic {

  /// @brief The Pit task.
  ///
  /// Design patterns:
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT PitTask_t :
        public Scnsl::Tlm::TlmTask_if_t
  {
  public:

      /// @brief Constructor.
      ///
      /// @param modulename This module name.
      /// @param id this module unique ID.
      /// @param n The node on which this task is placed.
      /// @param proxies The number of connected task proxies.
      /// @throw std::invalid_argument
      ///
      PitTask_t( sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies );


      /// @brief Destructor.
      virtual
      ~PitTask_t();


      virtual void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ) override;


  private:

      /// @brief Disabled copy constructor.
      PitTask_t ( const PitTask_t & );

      /// @brief Disabled assigmenemt operator.
      PitTask_t & operator = ( const PitTask_t & );
  };

} }



#endif
