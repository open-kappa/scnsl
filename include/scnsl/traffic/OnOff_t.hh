// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRAFFIC_ONOFF_T_HH
#define SCNSL_TRAFFIC_ONOFF_T_HH



/// @file
/// On-Off traffic.

#include "../scnslConfig.hh"
#include "Traffic_if_t.hh"

namespace Scnsl { namespace Traffic {

  /// @brief The On-Off traffic
  ///
  /// Design patterns:
  /// - Non copiable.
  /// - Non assignable.
  ///
  class SCNSL_EXPORT OnOff_t :
        public Scnsl::Traffic::Traffic_if_t
  {
  public:


	  SC_HAS_PROCESS( OnOff_t );

      /// @brief Constructor.
      ///
      /// @param modulename This module name.
      /// @param id this module unique ID.
      /// @param n The node on which this task is placed.
      /// @param proxies The number of connected task proxies.
	  /// @param label The label of the generated packets.
      /// @param pktSize The packet size in bytes.
      /// @param genTime Time between a packet from another, in ms.
	  /// @param on The transmission period, in ms.
	  /// @param off The pause period, in ms.
      /// @throw std::invalid_argument
      ///
      OnOff_t( sc_core::sc_module_name modulename,
                    const task_id_t id,
                    Scnsl::Core::Node_t * n,
                    const size_t proxies,
					label_t label,
					const size_t pktSize,
					const sc_core::sc_time genTime,
					const sc_core::sc_time on,
					const sc_core::sc_time off
					 );


      /// @brief Destructor.
      virtual
      ~OnOff_t();


      /// @name Traffic interface methods.
      //@{

	  virtual
      void enable() override;

      virtual
      void disable() override;

      //@}


  protected:

	  /// @brief The process that sends packets.
	  void writingProcess();

      /// @brief The packets size in bytes.
      const size_t _pktSize;

      /// @brief Time between a packet from another, in ms.
      const sc_core::sc_time _genTime;

	  /// @brief True if the traffic is enabled, false otherwise.
      bool _enable;

	  /// @brief The event for notify that the traffic is enabled/disabled.
	  sc_core::sc_event _activationEvent;

	  /// @brief The transmission period.
	  const sc_core::sc_time _on;

	  /// @brief The pause period.
	  const sc_core::sc_time _off;

  private:

      /// @brief Disabled copy constructor.
      OnOff_t ( const OnOff_t & );

      /// @brief Disabled assigmenemt operator.
      OnOff_t & operator = ( const OnOff_t & );
  };

} }



#endif
