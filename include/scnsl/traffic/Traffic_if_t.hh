// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRAFFIC_TRAFFIC_IF_T_HH
#define SCNSL_TRAFFIC_TRAFFIC_IF_T_HH



/// @file
/// A Traffic task.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../tlm/TlmTask_if_t.hh"


namespace Scnsl { namespace Traffic {


    /// @brief A Traffic task interface.
    ///
    ///
    /// Design patterns:
    /// - Interface.
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT Traffic_if_t:
        public Scnsl::Tlm::TlmTask_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The size type.
        typedef Scnsl::Core::size_t size_t;


        //@}


        /// @brief Impure virtual destructor.
        virtual
        ~Traffic_if_t() = 0;

        /// @name Interface methods.
        //@{

        /// @brief Enables traffic generation.
        ///
        virtual
        void enable() = 0;

        /// @brief Disables traffic generation.
        ///
        virtual
        void disable() = 0;


        /// @brief Sets the packet label.
        ///
        /// @param label The label.
        void setLabel( label_t label );

        virtual void b_transport( tlm::tlm_generic_payload & p, sc_core::sc_time & t ) override;

        //@}

    protected:

        /// @brief Constructor.
        ///
        /// @param name This module name.
        /// @param id this module unique ID.
        /// @param n The node on which this task is placed.
        /// @param proxies The number of connected task proxies.
        /// @param label The label of the generated packets. Optional, default value is 0.
        /// @throw std::invalid_argument
        ///
        Traffic_if_t( sc_core::sc_module_name name,
                      const task_id_t id,
                      Scnsl::Core::Node_t * n,
                      const size_t proxies,
                      label_t label = 0 );


        /// @brief The packet label.
        label_t _label;


    private:

        /// @brief Disabled copy constructor.
        Traffic_if_t( const Traffic_if_t & );

        /// @brief Disabled assignement operator.
        Traffic_if_t & operator = ( const Traffic_if_t & );

    };

  } }



#endif
