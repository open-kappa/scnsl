// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CHANNELS_FULLDUPLEXCHANNEL_T_HH
#define SCNSL_CHANNELS_FULLDUPLEXCHANNEL_T_HH



/// @file
/// A full duplex channel.

#include "../scnslConfig.hh"
#include "../utils/ChannelWrapper_if_t.hh"
#include "../utils/ChannelBridge_t.hh"
#include "UnidirectionalChannel_t.hh"



namespace Scnsl { namespace Channels {


    /// @brief A full duplex channel.
    /// It is implemented as a wrapper to two unidirectional channels.
    ///
    /// Design patterns:
    /// - Adapter.
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT FullDuplexChannel_t:
        public Scnsl::Utils::ChannelWrapper_if_t

    {
    public:

        /// @name Traits.
        //@{

        /// @brief The delay type.
        typedef Scnsl::Core::delay_t delay_t;

        /// @brief The capacity or bitrate type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief A counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief A carrier type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief The Node type.
        typedef Scnsl::Core::Node_t Node_t;

        /// @brief The internal Packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

        /// @brief The internal ChannelBridge type.
        typedef Scnsl::Utils::ChannelBridge_t ChannelBridge_t;

        //@}

        /// @brief Constructor.
        ///
        /// @param modulename This module name.
        /// @param capacity1 The first direction channel capacity in bit/sec.
        /// @param capacity2 The second direction channel capacity in bit/sec.
        /// @param delay This channel delay in sec.
        /// @throw std::domain_error Invalid arguments.
        ///
        FullDuplexChannel_t( const sc_core::sc_module_name modulename,
                             const bitrate_t capacity1,
                             const bitrate_t capacity2,
                             const delay_t delay );

        /// @brief Destructor.
        virtual
        ~FullDuplexChannel_t();


        /// @name Inherited channel interface methods.
        //@{

        /// @brief Overridden.
        /// Its calling is allowd only twice.
        /// It wraps the calls to internal unidirectional channels.
        /// The first invocation registers the sender node for the first direction.
        /// The second invocation registers the sender node for the other direction.
        virtual
        void addNode( Node_t * n ) override;

        virtual
        errorcode_t send( Node_t * n, const Packet_t & p ) override;

        virtual
        bool isMultipoint()
            const override;

        virtual
        void bridgeSetCarrier( const Scnsl::Utils::ChannelBridge_t * cb, const carrier_t c ) override;

        virtual
        errorcode_t bridgeReceive( const Scnsl::Utils::ChannelBridge_t * cb, const Packet_t & p ) override;

        //@}

    protected:

        /// @brief The first direction channel.
        UnidirectionalChannel_t _channel1;

        /// @brief The second direction channel.
        UnidirectionalChannel_t _channel2;

        /// @brief The maximum number of possible binded nodes.
        static const counter_t _MAXIMUM_BOUNDED_NODES;

        /// @brief Internal communicator bridge.
        ChannelBridge_t _bridge1;

        /// @brief Internal communicator bridge.
        ChannelBridge_t _bridge2;

        bool _flagCarrier1;

        bool _flagCarrier2;


    private:

        /// @brief Disabled copy constructor.
        FullDuplexChannel_t ( const FullDuplexChannel_t & );

        /// @brief Disabled assignment operator.
        FullDuplexChannel_t & operator = ( const FullDuplexChannel_t & );

    };

  } }



#endif
