// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CHANNELS_HALFDUPLEXCHANNEL_T_HH
#define SCNSL_CHANNELS_HALFDUPLEXCHANNEL_T_HH



/// @file
/// An half duplex channel.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Packet_t.hh"
#include "../core/Channel_if_t.hh"
#include "../core/Node_t.hh"



namespace Scnsl { namespace Channels {

    /// @brief An half duplex channel.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT HalfDuplexChannel_t:
        public Scnsl::Core::Channel_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The delay type.
        typedef Scnsl::Core::delay_t delay_t;

        /// @brief The capacity or bitrate type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief A counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief A carrier type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief The Node type.
        typedef Scnsl::Core::Node_t Node_t;

      	/// @brief The internal Packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

        /// @brief A struct for storing transmission informations.
        struct transmission_infos_t
        {
            /// @brief Stores the starting waiting time
            /// for the propagation delay.
            sc_core::sc_time time;
            /// @brief The packet to be sent.
            Scnsl::Core::Packet_t packet;

            transmission_infos_t();
        };

        /// @brief The fifo to store packets while waiting the delay.
        typedef std::list< transmission_infos_t > Queue_t;

        //@}

        SC_HAS_PROCESS( HalfDuplexChannel_t );


        /// @brief Constructor.
        ///
        /// @param modulename This module name.
        /// @param capacity The channel capacity in bit/sec.
        /// @param delay This channel delay in sec.
        /// @throw std::domain_error Invalid arguemnts.
        ///
        HalfDuplexChannel_t( const sc_core::sc_module_name modulename,
                             const bitrate_t capacity,
                             const delay_t delay );


        /// @brief Destructor.
        virtual
        ~HalfDuplexChannel_t();

        /// @name Inherited channel interface methods.
        //@{

        /// @brief Overridden, since its calling is allowd only twice.
        /// The first invocation registers the first sender node.
        /// The second invocation registers the second sender node.
        virtual
        void addNode( Node_t * n ) override;

        virtual
        errorcode_t send( Node_t * n, const Packet_t & p ) override;

        virtual
        bool isMultipoint()
            const override;

        //@}


    protected:

        /// @name Support methods.
        //@{

        /// @brief Updates collision infos.
        ///
        /// @param destination_node_id the destination node index.
        ///
        void checkCollisions( const counter_t destination_node_id );

        /// @brief Sets collided packets to invalid.
        ///
        /// @param destination_node_id the destination node index.
        ///
        void setCollisions( const counter_t destination_node_id );

        //@}


        /// @brief The maximum number of possible binded nodes.
        static const counter_t _MAXIMUM_BOUNDED_NODES;

        /// @brief This channel delay.
        const delay_t _DELAY;

        /// @brief This channel capacity.
        const bitrate_t _CAPACITY;

        /// @brief The queues in the two directions.
        Queue_t _queues[ 2 ];

        /// @brief A flag to assure that only a packet at time is sent.
        bool _locks[ 2 ];

        /// @brief A temp transmission infos.
        transmission_infos_t _infos[ 2 ];

        /// @brief A new packet into the queue.
        sc_core::sc_event _newPacket[ 2 ];

        /// @brief Stores the number of ongoing packets.
        counter_t _ongoing_packets_number[ 2 ];

        /// @brief The transmission completion time.
        sc_core::sc_time _completionTime[ 2 ];


    private:

        /// @brief Disabled copy constructor.
        HalfDuplexChannel_t( const HalfDuplexChannel_t & );

        /// @brief Disabled assignment operator.
        HalfDuplexChannel_t & operator = ( const HalfDuplexChannel_t & );

        /// @name Processes.
        //@{

        /// @brief First direction delay process.
        void _delayProcess1();

        /// @brief Second direction delay process.
        void _delayProcess2();

        //@}

    };

  } }



#endif
