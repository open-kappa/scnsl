// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CHANNELS_DELAYEDSHAREDCHANNEL_T_HH
#define SCNSL_CHANNELS_DELAYEDSHAREDCHANNEL_T_HH



/// @file
/// A shared model with variable delay.

#include <stdexcept>
#include <map>
#include <list>
#include <vector>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Packet_t.hh"
#include "../core/Channel_if_t.hh"
#include "../core/Node_t.hh"
#include "../utils/EventsQueue_t.hh"


namespace Scnsl { namespace Channels {


    /// @brief A shared model with variable delay.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT DelayedSharedChannel_t:
        public Scnsl::Core::Channel_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The propagation speed type.
        typedef Scnsl::Core::propagation_t propagation_t;

        /// @brief The capacity or bitrate type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief A counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief A carrier type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief A position type.
        typedef Scnsl::Core::position_t position_t;

        /// @brief The Node type.
        typedef Scnsl::Core::Node_t Node_t;

      	/// @brief The internal Packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

      	/// @brief The event ID type.
        typedef Scnsl::Core::event_id_t event_id_t;

        /// @brief Packet-relative infos for each node.
        struct packet_infos_t
        {
            /// @brief The packet.
            Packet_t packet;

        	/// @brief The start of sending.
            sc_core::sc_time sendStart;

        	/// @brief The end of sending.
            sc_core::sc_time sendEnd;

        	/// @brief The encoding time of the packet.
            sc_core::sc_time encodingTime;

        	/// @brief Tracks if the packet has collided.
            bool collided;

        	/// @brief Tracks corruption.
            bool corrupted;

        	/// @brief Tracks if the _startSingleSending() method has been already called.
            bool send_started;

            packet_infos_t();
        };

        /// @brief The single packet infos queue.
        typedef std::list< packet_infos_t > PacketInfosQueue_t;

        /// @brief The queue for the events IDs.
        typedef std::list< event_id_t > EventsIdsQueue_t;

        /// @brief The source id and the destination id of the transmission nodes.
        struct transmission_ids_infos_t
        {
        	/// @brief The id of the source node.
			Scnsl::Core::node_id_t sourceId;

        	/// @brief The id of the destination node.
			Scnsl::Core::node_id_t destinationId;

            transmission_ids_infos_t();
        };

        /// @brief Stores transmission infos for each node.
        /// There are n * n instances.
        struct transmission_infos_t
        {
            /// @brief Stores packet-relative infos.
            PacketInfosQueue_t packetInfos;

            /// @brief Stores IDs for _startSingleSending-relative events.
            EventsIdsQueue_t startEventsIds;

            /// @brief Stores IDs for _completeSingleSending-relative events.
            EventsIdsQueue_t completeEventsIds;

            /// @brief Stores the transmission ids.
            transmission_ids_infos_t idsInfos;

            /// @brief Checks reachability.
            bool reachable;

            /// @brief Stores old reachability.
            bool old_reachable;

            /// @brief Checks encoding speed.
            bool same_encoding_speed;

            /// @brief The channel delay
            sc_core::sc_time channelDelay;

            /// @brief The old channel delay
            sc_core::sc_time oldChannelDelay;

            /// @brief The time for a packet to reach
            /// the limit of the transmission range.
            sc_core::sc_time maxDelay;

            /// @brief The received power.
            double power;

            transmission_infos_t();
        };

        /// @brief Infos for all nodes.
        /// There are n instances.
        struct single_infos_t
        {
            /// @brief The associated node.
            Node_t * node;

        	/// @brief The start of sending.
            sc_core::sc_time sendStart;

        	/// @brief The end of sending.
            sc_core::sc_time sendEnd;

            /// @brief The number of currently active carriers.
            counter_t active_carriers;

            /// @brief The last carrier value.
            carrier_t old_carrier;

            /// @brief Checks if it's the first node movement.
            sc_core::sc_time movementTime;

            single_infos_t();

        private:

            single_infos_t( const single_infos_t & );

            single_infos_t & operator = ( const single_infos_t & );
        };


        /// @brief Map from nodes to ids.
		typedef std::map< const Node_t *, Scnsl::Core::node_id_t > IdMap_t;

        //@}

        SC_HAS_PROCESS( DelayedSharedChannel_t );


        /// @brief Constructor.
        ///
        /// @param modulename This module name.
        /// @param nodes_number The total number of nodes.
        /// @param alpha The signal degradation exponent.
        /// @param propagation This channel propagation speed constant.
        /// @throw std::domain_error Invalid arguments.
        ///
        DelayedSharedChannel_t( const sc_core::sc_module_name modulename,
                                const counter_t nodes_number,
                                const double alpha,
                                const propagation_t propagation );


        /// @brief Destructor.
        virtual
        ~DelayedSharedChannel_t();


        /// @name Inherited channel interface methods.
        //@{

        /// @brief Overwritten, since its calling is allowed only n-times.
        virtual
        void addNode( Node_t * n ) override;

        virtual
        errorcode_t send( Node_t * n, const Packet_t & p ) override;

        virtual
        bool isMultipoint()
            const override;

        virtual
        void updateProperties( const Node_t * n ) override;

        //@}

    protected:

        /// @name Callbacks.
        //@{

        /// @brief Starts to send a packet between a couple of nodes.
        ///
        /// @param ids The sender and the receiver IDs.
        ///
        void _startSingleSending( transmission_ids_infos_t * ids );

        /// @brief Completes to send a packet between a couple of nodes.
        ///
        /// @param ids The sender and the receiver IDs.
        ///
        void _completeSingleSending( transmission_ids_infos_t * ids );

        //@}

        /// @name Initialization methods.
        //@{

        /// @brief Updates the properties of the node when _addNode() method is called.
        ///
        /// @param n The node.
        ///
        void _initializeProperties ( const Node_t * n );

        //@}

        /// @name Support methods.
        //@{

        /// @brief Updates the properties between a couple of nodes when _addNode() method is called.
        ///
        /// @param ID The sender ID.
        /// @param recv_id The receiver ID.
        /// @param sp The sender node properties.
        /// @param rp The receiver node properties.
        /// @throw std::domain_error Invalid arguments.
        ///
        void _initializeSingleProperties(
			const Scnsl::Core::node_id_t ID,
			const Scnsl::Core::node_id_t recv_id,
            const Node_t::node_properties_t & sp,
            const Node_t::node_properties_t & rp );

        /// @brief Updates properties between a couple of nodes.
        ///
        /// @param ID The sender ID.
        /// @param recv_id The receiver ID.
        /// @param sp The sender node properties.
        /// @param rp The receiver node properties.
        /// @throw std::domain_error Invalid arguments.
        ///
        void _updateSingleProperties(
			const Scnsl::Core::node_id_t ID,
			const Scnsl::Core::node_id_t recv_id,
            const Node_t::node_properties_t & sp,
            const Node_t::node_properties_t & rp );

        /// @brief Updates the node-relative events after a node movement.
        ///
        /// @param ids The sender and the receiver IDs.
        ///
        void _updateEvents(  transmission_ids_infos_t * ids );

        //@}

        /// @brief The transmission data structure.
        transmission_infos_t ** _transmissions;

        /// @brief Infos about ongoing transmissions, with single instance.
        single_infos_t * _single_infos;

        /// @brief The nodes number.
        const counter_t _NODES_NUMBER;

        /// @brief The propagation speed.
        const propagation_t _PROPAGATION;

        /// @brief The attenuation exponent.
        const double _ALPHA_EXPONENT;

        /// @brief Infinitesimal value.
        const double _EPSILON;

        /// @brief The currently registered nodes.
        counter_t _nodes_number;

        /// @brief Maps nodes to ID's.
        IdMap_t _ids;

        /// @brief A reference to the instance of the events queue.
        Scnsl::Utils::EventsQueue_t * _eventsQueue;

    private:

        /// @brief Disabled copy constructor.
        DelayedSharedChannel_t( const DelayedSharedChannel_t & );

        /// @brief Disabled assignment operator.
        DelayedSharedChannel_t & operator = ( const DelayedSharedChannel_t & );

    };

  } }



#endif
