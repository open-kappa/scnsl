// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CHANNELS_UNIDIRECTIONALCHANNEL_T_HH
#define SCNSL_CHANNELS_UNIDIRECTIONALCHANNEL_T_HH



/// @file
/// An unidirectiona channel.

#include <stdexcept>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Packet_t.hh"
#include "../core/Channel_if_t.hh"
#include "../core/Node_t.hh"


namespace Scnsl { namespace Channels {


    /// @brief An unidirectional channel.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT UnidirectionalChannel_t:
        public Scnsl::Core::Channel_if_t
    {
    public :

        /// @name Traits.
        //@{

        /// @brief The delay type.
        typedef Scnsl::Core::delay_t delay_t;

        /// @brief The capacity or bitrate type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief A counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief The Node type.
        typedef Scnsl::Core::Node_t Node_t;

        /// @brief The internal Packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

        /// @brief A struct for storing transmission informations.
        struct transmission_infos_t
        {
            /// @brief Stores the starting waiting time
            /// for the propagation delay.
            sc_core::sc_time time;
            /// @brief The packet to be sent.
            Scnsl::Core::Packet_t packet;

            transmission_infos_t();
        };

        /// @brief The fifo to store packets while waiting the delay.
        typedef std::list< transmission_infos_t > Queue_t;

        //@}


        SC_HAS_PROCESS( UnidirectionalChannel_t );

        /// @brief Constructor.
        ///
        /// @param modulename This module name.
        /// @param capacity This channel capacity in bit/sec.
        /// @param delay This channel delay in sec.
        /// @throw std::domain_error Invalid arguments.
        ///
        UnidirectionalChannel_t( const sc_core::sc_module_name modulename,
                                 const bitrate_t capacity,
                                 const delay_t delay );

        /// @brief Destructor.
        virtual
        ~UnidirectionalChannel_t();

        /// @name Inherited channel interface methods.
        //@{

        /// @brief Overridden, since its calling is allowd only twice.
        /// The first invocation registers the sender node.
        /// The second invocation registers the receiver node.
        virtual
        void addNode( Node_t * n ) override;

        virtual
        Scnsl::Core::errorcode_t send( Node_t * n, const Packet_t & p ) override;

        virtual
        bool isMultipoint()
            const override;

        //@}

    protected:

        /// @brief Checks if the channel is empty.
        bool isEmpty()
            const;

        /// @brief The maximum number of possible binded nodes.
        static const counter_t _MAXIMUM_BOUNDED_NODES;

        /// @brief This channel delay.
        const delay_t _DELAY;

        /// @brief This channel capacity.
        const bitrate_t _CAPACITY;

        /// @brief A flag to assure that only a packet at time is sent.
        bool _lock;

        /// @brief The fifo to store packets.
        Queue_t _queue;

        /// @brief A temp transmission infos.
        transmission_infos_t _info;

        /// @brief A new packet into the queue.
        sc_core::sc_event _newPacket;

    private:

        /// @brief Disabled copy constructor.
        UnidirectionalChannel_t( const UnidirectionalChannel_t & );

        /// @brief Disabled assignment operator.
        UnidirectionalChannel_t & operator = ( const UnidirectionalChannel_t &);

        /// @name Processes.
        //@{

        /// @brief The delay process.
        void _delayProcess();

        //@}
    };

  } }


#endif
