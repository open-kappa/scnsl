// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_CHANNELS_SHAREDCHANNEL_T_HH
#define SCNSL_CHANNELS_SHAREDCHANNEL_T_HH



/// @file
/// A shared model with constant delay.

#include <stdexcept>
#include <map>
#include <list>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "../core/Packet_t.hh"
#include "../core/Channel_if_t.hh"
#include "../core/Node_t.hh"
#include "../utils/EventsQueue_t.hh"


namespace Scnsl { namespace Channels {


    /// @brief A shared model with constant delay.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    ///
    class SCNSL_EXPORT SharedChannel_t:
        public Scnsl::Core::Channel_if_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The delay type.
        typedef Scnsl::Core::delay_t delay_t;

        /// @brief The capacity or bitrate type.
        typedef Scnsl::Core::bitrate_t bitrate_t;

        /// @brief A counter type.
        typedef Scnsl::Core::counter_t counter_t;

        /// @brief A carrier type.
        typedef Scnsl::Core::carrier_t carrier_t;

        /// @brief A position type.
        typedef Scnsl::Core::position_t position_t;

        /// @brief The Node type.
        typedef Scnsl::Core::Node_t Node_t;

      	/// @brief The internal Packet type.
        typedef Scnsl::Core::Packet_t Packet_t;

        /// @brief Packet-relative infos for each node.
        struct packet_infos_t
        {
            /// @brief Tracks if the packet has collided.
            bool collided;

            /// @brief Tracks corruption.
            bool corrupted;

            /// @brief Time into which the packet is marked as corrupted.
            sc_core::sc_time corruptionTime;

            /// @brief The encoding time of the packet.
            sc_core::sc_time encodingTime;

            packet_infos_t();

        };

        /// @brief The single packet infos queue.
        typedef std::list< packet_infos_t > PacketInfosQueue_t;

        /// @brief Stores transmission infos for each node.
        /// There are n * n instances.
        struct transmission_infos_t
        {
            /// @brief Stores packet-relative infos.
            PacketInfosQueue_t packetInfos;

            /// @brief Checks reachability.
            bool reachable;
            /// @brief Sensed power.
            double power;

            /// @brief Checks encoding speed.
            bool same_encoding_speed;

            transmission_infos_t();
        };

        /// @brief Packet-relative infos, one for all nodes.
        struct single_packet_infos_t
        {
            /// @brief The packet.
            Packet_t packet;

            /// @brief The start of sending.
            sc_core::sc_time sendStart;
            /// @brief The end of sending.
            sc_core::sc_time sendEnd;

            /// @brief Tracks if the startSending() method has been already called.
            bool send_started;

            single_packet_infos_t();
        };

        /// @brief The single packet infos queue.
        typedef std::list< single_packet_infos_t > SinglePacketInfosQueue_t;

        /// @brief Infos for all nodes.
        /// There are n instances.
        struct single_infos_t
        {
            /// @brief The associated node.
            Node_t * node;
            /// @brief Tracks single packet infos.
            SinglePacketInfosQueue_t packetInfos;

            /// @brief Starting of transmission from this node.
            sc_core::sc_time sendStart;
            /// @brief Ending of transmission from this node.
            sc_core::sc_time sendEnd;

            /// @brief The number of currently active carriers.
            counter_t active_carriers;
            /// @brief The last carrier value.
            carrier_t old_carrier;

            single_infos_t();

        private:

            single_infos_t( const single_infos_t & );

            single_infos_t & operator = ( const single_infos_t & );
        };

        /// @brief Map from nodes to ids.
        typedef std::map< const Node_t *, Scnsl::Core::node_id_t > IdMap_t;

        //@}

        SC_HAS_PROCESS( SharedChannel_t );


        /// @brief Constructor.
        ///
        /// @param modulename This module name.
        /// @param nodes_number The total number of nodes.
        /// @param alpha The signal degradation exponent.
        /// @param delay This channel delay in sec.
        /// @throw std::domain_error Invalid arguments.
        ///
        SharedChannel_t( const sc_core::sc_module_name modulename,
                         const counter_t nodes_number,
                         const double alpha,
                         const delay_t delay );


        /// @brief Destructor.
        virtual
        ~SharedChannel_t();


        /// @name Inherited channel interface methods.
        //@{

        /// @brief Overwritten, since its calling is allowed only n-times.
        virtual
        void addNode( Node_t * n ) override;

        virtual
        errorcode_t send( Node_t * n, const Packet_t & p ) override;

        virtual
        bool isMultipoint()
            const override;

        virtual
        void updateProperties( const Node_t * n ) override;

        //@}

    protected:

        /// @name Callbacks.
        //@{

        /// @brief Starts the sending of a packet.
        void _startSending( const Node_t * n );

        /// @brief Completes the sending of a packet.
        void _completeSending( const Node_t * n );

        //@}

        /// @name Support methods.
        //@{

        /// @brief Starts to send a packet between a couple of nodes.
        ///
        /// @param ID The sender ID.
        /// @param recv_id The receiver ID.
        ///
		void _startSingleSending(const Scnsl::Core::node_id_t ID, const Scnsl::Core::node_id_t recv_id);

        /// @brief Completes to send a packet between a couple of nodes.
        ///
        /// @param ID The sender ID.
        /// @param recv_id The receiver ID.
        ///
		void _completeSingleSending(const Scnsl::Core::node_id_t ID, const Scnsl::Core::node_id_t recv_id);

        /// @brief Updates reachability properties between a couple of nodes.
        ///
        /// @param ID The sender ID.
        /// @param recv_id The receiver ID.
        /// @param sp The sender node properties.
        /// @param rp The receiver node properties.
        /// @throw std::domain_error Invalid arguments.
        ///
        void _updateSingleProperties(
			const Scnsl::Core::node_id_t ID,
			const Scnsl::Core::node_id_t recv_id,
            const Node_t::node_properties_t & sp,
            const Node_t::node_properties_t & rp );

        //@}


        /// @brief The transmission data structure.
        transmission_infos_t ** _transmissions;

        /// @brief Infos about ongoing transmissions, with single instance.
        single_infos_t * _single_infos;

        /// @brief The nodes number.
        const counter_t _NODES_NUMBER;

        /// @brief The propagation delay.
        const delay_t _DELAY;

        /// @brief The attenuation exponent.
        const double _ALPHA_EXPONENT;

        /// @brief Infinitesimal value.
        const double _EPSILON;

        /// @brief The currently registered nodes.
        counter_t _nodes_number;

        /// @brief Maps nodes to ID's.
        IdMap_t _ids;

        /// @brief A reference to the instance of the events queue.
        Scnsl::Utils::EventsQueue_t * _eventsQueue;

    private:

        /// @brief Disabled copy constructor.
        SharedChannel_t( const SharedChannel_t & );

        /// @brief Disabled assignment operator.
        SharedChannel_t & operator = ( const SharedChannel_t & );

    };

  } }



#endif
