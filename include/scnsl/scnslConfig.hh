// -*- SystemC -*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_SCNSL_CONFIG_HH
#define SCNSL_SCNSL_CONFIG_HH

#include "scnslExport.h"

#if (defined _MSC_VER)

// Non-dll interface parent classes (e.g. SystemC classes)
#pragma warning(disable:4275)
// Stl data structures unexported:
#pragma warning(disable:4251)
// Cast between different pointer to member representations: e.g. SC_THREAD() macro.
#pragma warning(disable:4407)
// New behaviour: member arrays are defaoult-initialized
#pragma warning(disable:4351)
// Compiler cannot force left-to-right operators operands evaluation order
#pragma warning(disable:4866)

#endif

#if (defined _MSC_VER)
#pragma warning(push)
#pragma warning(disable:4127)
#pragma warning(disable:4512)
#endif

// #include <stdint.h>
// #include <systemc>


#if (defined _MSC_VER)
#pragma warning(pop)
#endif




#endif
