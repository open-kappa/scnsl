// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_TRACEABLE_BASE_T_HH
#define SCNSL_TRACING_TRACEABLE_BASE_T_HH



/// @file
/// An object which is traceable.

#include <string>
#include <list>
#include <systemc>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"
#include "tracing_data_types.hh"

// This is not required: included just to make macros directly
//  available to children implementations.
#include "tracing_macros.hh"


namespace Scnsl { namespace Core {

    class Channel_if_t;
    class Packet_t;

  } }

namespace Scnsl { namespace Tracing {

    class Tracer_t;

  } }

namespace Scnsl { namespace Tracing {

    /// @brief An object which is traceable.
    ///
    /// Design patterns:
    /// - Base class.
    /// - Regular.
    ///
    class SCNSL_EXPORT Traceable_base_t
    {
    public:

        /// @name Traits.
        //@{

        /// @brief The type for tracer list.
        typedef std::list< Tracer_t * > TracersList_t;

        //@}

        /// @name Interface methods.
        //@{

        /// @brief Registers a tracer.
        /// Multiple tracers can be registered.
        /// Children classes can overwrite this method to
        /// allow tracing of children submodules.
        ///
        /// @param t The tracer.
        ///
        virtual
        void registerTracer( Tracer_t * t );

        //@}

    protected:

        /// @brief Constructor.
        ///
        /// @param name This traceable name.
        ///
        explicit
        Traceable_base_t( const char * name );

        /// @brief Copy constructor.
        Traceable_base_t( const Traceable_base_t & t );

        /// @brief Destructor.
        /// This destructor could be ok if non-virtual,
        /// but it is required due to a GCC bug.
        virtual
        ~Traceable_base_t();


        /// @brief Assignment operator.
        Traceable_base_t & operator = ( const Traceable_base_t & t );

        /// @name Internal tracing methods.
        /// This methods are declared const as hacking: this allow to trace
        /// even inside children objects const methods.
        //@{

        /// @brief Traces an info.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceInfo( const sc_core::sc_time & ts,
                        const char * const filename,
                        const unsigned int fileline,
                        const level_t level,
                        const char * const message )
            const;

        /// @brief Traces an info.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceInfo( const sc_core::sc_time & ts,
                        const char * const filename,
                        const unsigned int fileline,
                        const level_t level,
                        const Scnsl::Core::Packet_t & packet,
                        const char * const message = nullptr )
            const;

        /// @brief Traces an info.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceInfo( const sc_core::sc_time & ts,
                        const char * const filename,
                        const unsigned int fileline,
                        const level_t level,
                        const Scnsl::Core::Channel_if_t * channel,
                        const Scnsl::Core::carrier_t & carrier,
                        const char * const message = nullptr )
            const;

        /// @brief Traces an log.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceLog( const sc_core::sc_time & ts,
                       const char * const filename,
                       const unsigned int fileline,
                       const level_t level,
                       const char * const message )
            const;

        /// @brief Traces an log.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceLog( const sc_core::sc_time & ts,
                       const char * const filename,
                       const unsigned int fileline,
                       const level_t level,
                       const Scnsl::Core::Packet_t & packet,
                       const char * const message = nullptr )
            const;

        /// @brief Traces an log.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceLog( const sc_core::sc_time & ts,
                       const char * const filename,
                       const unsigned int fileline,
                       const level_t level,
                       const Scnsl::Core::Channel_if_t * channel,
                       const Scnsl::Core::carrier_t & carrier,
                       const char * const message = nullptr )
            const;

        /// @brief Traces an debug.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceDebug( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const char * const message )
            const;

        /// @brief Traces an debug.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceDebug( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Packet_t & packet,
                         const char * const message = nullptr )
            const;

        /// @brief Traces an debug.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceDebug( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Channel_if_t * channel,
                         const Scnsl::Core::carrier_t & carrier,
                         const char * const message = nullptr )
            const;

        /// @brief Traces an warning.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceWarning( const sc_core::sc_time & ts,
                           const char * const filename,
                           const unsigned int fileline,
                           const level_t level,
                           const char * const message )
            const;

        /// @brief Traces an warning.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceWarning( const sc_core::sc_time & ts,
                           const char * const filename,
                           const unsigned int fileline,
                           const level_t level,
                           const Scnsl::Core::Packet_t & packet,
                           const char * const message = nullptr )
            const;

        /// @brief Traces an warning.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceWarning( const sc_core::sc_time & ts,
                           const char * const filename,
                           const unsigned int fileline,
                           const level_t level,
                           const Scnsl::Core::Channel_if_t * channel,
                           const Scnsl::Core::carrier_t & carrier,
                           const char * const message = nullptr )
            const;

        /// @brief Traces an error.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceError( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const char * const message )
            const;

        /// @brief Traces an error.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceError( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Packet_t & packet,
                         const char * const message = nullptr )
            const;

        /// @brief Traces an error.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceError( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Channel_if_t * channel,
                         const Scnsl::Core::carrier_t & carrier,
                         const char * const message = nullptr )
            const;

        /// @brief Traces an fatal.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param message The message to trace.
        ///
        void traceFatal( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const char * const message )
            const;

        /// @brief Traces an fatal.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param packet The packet to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceFatal( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Packet_t & packet,
                         const char * const message = nullptr )
            const;

        /// @brief Traces an fatal.
        ///
        /// @param ts A timestamp.
        /// @param filename The source file name.
        /// @param fileline The source file line.
        /// @param level The message level.
        /// @param channel The channel relative to the carrier.
        /// @param carrier The carrier to trace.
        /// @param message Optional. An extra message to trace.
        ///
        void traceFatal( const sc_core::sc_time & ts,
                         const char * const filename,
                         const unsigned int fileline,
                         const level_t level,
                         const Scnsl::Core::Channel_if_t * channel,
                         const Scnsl::Core::carrier_t & carrier,
                         const char * const message = nullptr )
            const;

        //@}


        /// @brief The internal tracers list.
        mutable TracersList_t _tracers;

        /// @brief A struct used to optimize and simplify data tracing.
        mutable trace_data_t _data;

    };
  } }



#endif
