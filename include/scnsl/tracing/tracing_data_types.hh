// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_TRACING_DATA_TYPES_HH
#define SCNSL_TRACING_TRACING_DATA_TYPES_HH



/// @file
/// Tracing specific base types.

#include <string>
#include <systemc>

#include "../scnslConfig.hh"
#include "../core/data_types.hh"


namespace Scnsl { namespace Core {

    class Packet_t;
    class Channel_if_t;

  } }

namespace Scnsl { namespace Tracing {

    /// @brief The output level type.
    typedef unsigned int level_t;

    /// @brief A struct used to optimize and simplify data tracing.
    struct SCNSL_EXPORT trace_data_t
    {
        std::string name;
        sc_core::sc_time timestamp;
        const char * filename;
        unsigned int fileline;
        level_t level;
        const char * message;
        const Scnsl::Core::Packet_t * packet;
        Scnsl::Core::carrier_t carrier;
        const Scnsl::Core::Channel_if_t * channel;

        trace_data_t();

        trace_data_t( const trace_data_t & d );

        trace_data_t & operator = ( const trace_data_t & d );

    };
} }



#endif
