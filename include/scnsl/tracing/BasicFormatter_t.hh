// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_BASICFORMATTER_T_HH
#define SCNSL_TRACING_BASICFORMATTER_T_HH



/// @file
/// A basic formatter.

#include "../scnslConfig.hh"
#include "Formatter_if_t.hh"

namespace Scnsl { namespace Tracing {

    /// @brief A basic formatter.
    ///
    /// Design patterns:
    /// - Regular.
    ///
    class SCNSL_EXPORT BasicFormatter_t:
        public Formatter_if_t
    {
    public:

        /// @brief Constructor.
        BasicFormatter_t();

        /// @brief Copy constructor.
        BasicFormatter_t( const BasicFormatter_t & f );

        /// @brief Destructor.
        virtual
        ~BasicFormatter_t();

        /// @brief Assignment operator.
        BasicFormatter_t & operator = ( const BasicFormatter_t & f );

        /// @name Interface methods.
        //@{

        /// @brief Sets if the trace type shall be printed.
        ///
        /// @param b True if trace type shall be printed.
        ///
        void printTraceType( const bool b );

        /// @brief Sets if the trace timestamp shall be printed.
        ///
        /// @param b True if trace timestamp shall be printed.
        ///
        void printTraceTimestamp( const bool b );


        //@}

    protected:


        /// @name Inherited interface methods.
        //@{

        virtual
        void _traceInfo( std::ostream & os, const trace_data_t & data ) override;

        virtual
        void _traceLog( std::ostream & os, const trace_data_t & data ) override;

        virtual
        void _traceDebug( std::ostream & os, const trace_data_t & data ) override;

        virtual
        void _traceWarning( std::ostream & os, const trace_data_t & data ) override;

        virtual
        void _traceError( std::ostream & os, const trace_data_t & data ) override;

        virtual
        void _traceFatal( std::ostream & os, const trace_data_t & data ) override;

        //@}

        /// @name Internal support methods.
        //@{

        /// @brief Traces the data.
        ///
        /// @param os The output stream.
        /// @param data The data to trace.
        /// @param kind The kind of trace.
        /// @param print_sourcefile_infos True if filename and filenumber infos shall be printed.
        ///
        void _basicTrace( std::ostream & os,
                          const trace_data_t & data,
                          const char * const kind,
                          const bool print_sourcefile_infos );

        //@}


        /// @brief Flag to print trace type.
        bool _print_trace_type;

        /// @brief Flag to print timestamp.
        bool _print_trace_timestamp;


    };

  } }



#endif
