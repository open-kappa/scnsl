// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_BASICFILTER_T_HH
#define SCNSL_TRACING_BASICFILTER_T_HH



/// @file
/// A basic filter.

#include "../scnslConfig.hh"
#include "Filter_if_t.hh"

namespace Scnsl { namespace Tracing {

    /// @brief A basic filter.
    ///
    /// Design patterns:
    /// - Semi-regular.
    ///
    class SCNSL_EXPORT BasicFilter_t:
        public Filter_if_t
    {
    public:

        /// @brief Constructor.
        ///
        /// @param info True if traces infos.
        /// @param log True if traces logs.
        /// @param debug True if traces debugs.
        /// @param warning True if traces warnings.
        /// @param error True if traces errors.
        /// @param fatal True if traces fatals.
        ///
        BasicFilter_t(
            const level_t info,
            const level_t log,
            const level_t debug,
            const level_t warning,
            const level_t error,
            const level_t fatal
            );

        /// @brief Copy construcotr.
        BasicFilter_t( const BasicFilter_t & f );

        /// @brief Destructor.
        virtual
        ~BasicFilter_t();

        /// @brief Assignment operator.
        BasicFilter_t & operator = ( const BasicFilter_t & f );


        /// @name Inherited interface methods.
        //@{

        virtual
        bool haveToTraceInfo( const trace_data_t & data )
            const override;

        virtual
        bool haveToTraceLog( const trace_data_t & data )
            const override;

        virtual
        bool haveToTraceDebug( const trace_data_t & data )
            const override;

        virtual
        bool haveToTraceWarning( const trace_data_t & data )
            const override;

        virtual
        bool haveToTraceError( const trace_data_t & data )
            const override;

        virtual
        bool haveToTraceFatal( const trace_data_t & data )
            const override;

        //@}


    protected:

        /// @brief Tracing infos.
        level_t _info;

        /// @brief Tracing logs.
        level_t _log;

        /// @brief Tracing debugs.
        level_t _debug;

        /// @brief Tracing warnings.
        level_t _warning;

        /// @brief Tracing errors.
        level_t _error;

        /// @brief Tracing fatals.
        level_t _fatal;

    };

} }



#endif
