// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_TRACER_T_HH
#define SCNSL_TRACING_TRACER_T_HH



/// @file
/// A tracer.

#include <ostream>

#include "../scnslConfig.hh"
#include "Traceable_base_t.hh"


namespace Scnsl { namespace Tracing {

    class Filter_if_t;
    class Formatter_if_t;

  } }

namespace Scnsl { namespace Tracing {

    /// @brief A tracer.
    ///
    /// Design patterns:
    /// - Non copiable.
    /// - Non assignable.
    /// - KISS.
    ///
    class SCNSL_EXPORT Tracer_t
    {
    public:

        /// @brief Constructor.
        /// This class DOES hold the ownership of stored formatter and filter.
        ///
        /// @param filter The filter to use.
        /// @param formatter The formatter to use.
        ///
        Tracer_t( Filter_if_t * filter, Formatter_if_t * formatter );

        /// @brief Destructor.
        ~Tracer_t();

        /// @name Interface methods.
        //@{

        /// @brief Adds an output stream.
        /// This class does not owns the ownership of the output stream,
        /// nor closes it.
        ///
        /// @param os The output stream.
        ///
        void addOutput( std::ostream * os );

        /// @brief Adds a module for tracing.
        void trace( Traceable_base_t * traceable );


        //@}


        /// @name Tracing support methods.
        //@{

        /// @brief Traces infos.
        ///
        /// @param data The data to trace.
        ///
        void traceInfo( const trace_data_t & data );

        /// @brief Traces logs.
        ///
        /// @param data The data to trace.
        ///
        void traceLog( const trace_data_t & data );

        /// @brief Traces debugs.
        ///
        /// @param data The data to trace.
        ///
        void traceDebug( const trace_data_t & data );

        /// @brief Traces warnings.
        ///
        /// @param data The data to trace.
        ///
        void traceWarning( const trace_data_t & data );

        /// @brief Traces errors.
        ///
        /// @param data The data to trace.
        ///
        void traceError( const trace_data_t & data );

        /// @brief Traces fatals.
        ///
        /// @param data The data to trace.
        ///
        void traceFatal( const trace_data_t & data );

        //@}


    protected:

        /// @brief The filter to use.
        Filter_if_t * _filter;

        /// @brief The formatter to use.
        Formatter_if_t * _formatter;

    private:

        /// @brief Disabled copy constructor.
        Tracer_t( const Tracer_t & );

        /// @brief Disabled assignment operator.
        Tracer_t & operator = ( const Tracer_t & );
    };

} }



#endif
