// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_FILTER_IF_T_HH
#define SCNSL_TRACING_FILTER_IF_T_HH



/// @file
/// The filter interface.

#include "../scnslConfig.hh"
#include "tracing_data_types.hh"


namespace Scnsl { namespace Tracing {


    /// @brief The filter interface.
    ///
    /// Design patterns:
    /// - Regular.
    /// - Interface.
    ///
    class SCNSL_EXPORT Filter_if_t
    {
    public:

        /// @brief Destructor.
        virtual
        ~Filter_if_t();


        /// @name Interface methods.
        //@{

        /// @brief Check if it is required to trace infos.
        virtual
        bool haveToTraceInfo( const trace_data_t & data )
            const = 0;

        /// @brief Check if it is required to trace logs.
        virtual
        bool haveToTraceLog( const trace_data_t & data )
            const = 0;

        /// @brief Check if it is required to trace debugs.
        virtual
        bool haveToTraceDebug( const trace_data_t & data )
            const = 0;

        /// @brief Check if it is required to trace warnings.
        virtual
        bool haveToTraceWarning( const trace_data_t & data )
            const = 0;

        /// @brief Check if it is required to trace errors.
        virtual
        bool haveToTraceError( const trace_data_t & data )
            const = 0;

        /// @brief Check if it is required to trace fatals.
        virtual
        bool haveToTraceFatal( const trace_data_t & data )
            const = 0;

        //@}

    protected:

        /// @brief Constructor.
        Filter_if_t();

        /// @brief Copy constructor.
        Filter_if_t( const Filter_if_t & f );


        /// @brief Assignment operator.
        Filter_if_t & operator = ( const Filter_if_t & f );

    };

} }



#endif
