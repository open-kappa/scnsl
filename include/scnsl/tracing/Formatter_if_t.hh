// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_FORMATTER_IF_T_HH
#define SCNSL_TRACING_FORMATTER_IF_T_HH



/// @file
/// Formatter interface.

#include <ostream>
#include <list>

#include "../scnslConfig.hh"
#include "tracing_data_types.hh"

namespace Scnsl { namespace Tracing {

    /// @brief Formatter interface.
    ///
    /// Design patterns:
    /// - Interface.
    /// - Regular.
    /// - Non Virtual Interface (NVI).
    ///
    class SCNSL_EXPORT Formatter_if_t
    {
    public:


        /// @name Traits.
        //@{

        /// @brief List of output streams.
        typedef std::list< std::ostream * > OutputList_t;

        //@}

        /// @brief Impure virtual destructor.
        virtual
        ~Formatter_if_t();

        /// @name Interface methods.
        //@{

        /// @brief Adds an output stream.
        /// This class does not owns the ownership of the output stream,
        /// nor closes it.
        ///
        /// @param os The output stream.
        ///
        void addOutput( std::ostream * os );

        /// @brief Traces infos.
        ///
        /// @param data The data to trace.
        ///
        void traceInfo( const trace_data_t & data );

        /// @brief Traces logs.
        ///
        /// @param data The data to trace.
        ///
        void traceLog( const trace_data_t & data );

        /// @brief Traces debugs.
        ///
        /// @param data The data to trace.
        ///
        void traceDebug( const trace_data_t & data );

        /// @brief Traces warnings.
        ///
        /// @param data The data to trace.
        ///
        void traceWarning( const trace_data_t & data );

        /// @brief Traces errors.
        ///
        /// @param data The data to trace.
        ///
        void traceError( const trace_data_t & data );

        /// @brief Traces fatals.
        ///
        /// @param data The data to trace.
        ///
        void traceFatal( const trace_data_t & data );

        //@}

    protected:

        /// @brief Constructor.
        Formatter_if_t();

        /// @brief Copy constructor.
        Formatter_if_t( const Formatter_if_t & f );

        /// @brief Assignment operator.
        Formatter_if_t & operator = ( const Formatter_if_t & f );



        /// @name Internal formatting methods.
        //@{

        /// @brief Traces infos.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceInfo( std::ostream & os, const trace_data_t & data ) = 0;

        /// @brief Traces logs.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceLog( std::ostream & os, const trace_data_t & data ) = 0;

        /// @brief Traces debugs.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceDebug( std::ostream & os, const trace_data_t & data ) = 0;

        /// @brief Traces warnings.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceWarning( std::ostream & os, const trace_data_t & data ) = 0;

        /// @brief Traces errors.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceError( std::ostream & os, const trace_data_t & data ) = 0;

        /// @brief Traces fatals.
        ///
        /// @param os The output stream where to trace.
        /// @param data The data to trace.
        ///
        virtual
        void _traceFatal( std::ostream & os, const trace_data_t & data ) = 0;

        //@}

    private:

        /// @brief The list of output streams.
        OutputList_t _outputs;
    };

} }



#endif
