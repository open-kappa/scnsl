// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_TRACING_TRACING_MACROS_HH
#define SCNSL_TRACING_TRACING_MACROS_HH



/// @file
/// Macros to simplify tracing integration and usage.
/// These macros do not follow standard naming conventions to improve readability.
/// These macros are integrated with CMake.
///
/// Macros allowed level values (and SCNSL_L* macros) range from 1 to 5 normally.
/// During debug, can be useful to set a level of a trace to 0, to always activate it.
/// Trace macros (e.g. SCNSL_TINFO) can be 0 or 1.
/// In the code, just write something like: SCNSL_TRACE_INFO( 2, "Hello world!" );

// ////////////////////////////////////////////////////////////////
// Static configuration.
// ////////////////////////////////////////////////////////////////

// List of variables that shall be defined, with respective default values.

#ifndef SCNSL_INFO
#define SCNSL_INFO     5
#endif
#ifndef SCNSL_LOG
#define SCNSL_LOG      5
#endif
#ifndef SCNSL_DBG
#define SCNSL_DBG      5
#endif

#ifndef SCNSL_WARNING
#define SCNSL_WARNING  5
#endif
#ifndef SCNSL_ERROR
#define SCNSL_ERROR    5
#endif
#ifndef SCNSL_FATAL
#define SCNSL_FATAL    5
#endif


// ////////////////////////////////////////////////////////////////
// General support macros.
// ////////////////////////////////////////////////////////////////


/// Macro for getting the timestamp.
#define SCNSL_GET_TIME_STAMP() ( sc_core::sc_time_stamp() )
// Base macros for tracing.

#define SCNSL_TRACE_INFO_BASE( LEVEL, ... )    ( Traceable_base_t::traceInfo( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_LOG_BASE( LEVEL, ... )     ( Traceable_base_t::traceLog( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_DBG_BASE( LEVEL, ... )     ( Traceable_base_t::traceDebug( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_WARNING_BASE( LEVEL, ... ) ( Traceable_base_t::traceWarning( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_ERROR_BASE( LEVEL, ... )   ( Traceable_base_t::traceError( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_FATAL_BASE( LEVEL, ... )   ( Traceable_base_t::traceFatal( SCNSL_GET_TIME_STAMP(), __FILE__, __LINE__, LEVEL, __VA_ARGS__ ) )
// ////////////////////////////////////////////////////////////////
// Support macros for INFO levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_INFO_0( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_INFO >= 1)
#define SCNSL_TRACE_INFO_1( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_INFO_1( LEVEL, ... )
#endif

#if (SCNSL_INFO >= 2)
#define SCNSL_TRACE_INFO_2( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_INFO_2( LEVEL, ... )
#endif

#if (SCNSL_INFO >= 3)
#define SCNSL_TRACE_INFO_3( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_INFO_3( LEVEL, ... )
#endif

#if (SCNSL_INFO >= 4)
#define SCNSL_TRACE_INFO_4( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_INFO_4( LEVEL, ... )
#endif

#if (SCNSL_INFO >= 5)
#define SCNSL_TRACE_INFO_5( LEVEL, ... ) SCNSL_TRACE_INFO_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_INFO_5( LEVEL, ... )
#endif


// ////////////////////////////////////////////////////////////////
// Support macros for LOG levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_LOG_0( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_LOG >= 1)
#define SCNSL_TRACE_LOG_1( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_LOG_1( LEVEL, ... )
#endif

#if (SCNSL_LOG >= 2)
#define SCNSL_TRACE_LOG_2( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_LOG_2( LEVEL, ... )
#endif

#if (SCNSL_LOG >= 3)
#define SCNSL_TRACE_LOG_3( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_LOG_3( LEVEL, ... )
#endif

#if (SCNSL_LOG >= 4)
#define SCNSL_TRACE_LOG_4( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_LOG_4( LEVEL, ... )
#endif

#if (SCNSL_LOG >= 5)
#define SCNSL_TRACE_LOG_5( LEVEL, ... ) SCNSL_TRACE_LOG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_LOG_5( LEVEL, ... )
#endif


// ////////////////////////////////////////////////////////////////
// Support macros for DBG levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_DBG_0( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_DBG >= 1)
#define SCNSL_TRACE_DBG_1( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_DBG_1( LEVEL, ... )
#endif

#if (SCNSL_DBG >= 2)
#define SCNSL_TRACE_DBG_2( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_DBG_2( LEVEL, ... )
#endif

#if (SCNSL_DBG >= 3)
#define SCNSL_TRACE_DBG_3( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_DBG_3( LEVEL, ... )
#endif

#if (SCNSL_DBG >= 4)
#define SCNSL_TRACE_DBG_4( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_DBG_4( LEVEL, ... )
#endif

#if (SCNSL_DBG >= 5)
#define SCNSL_TRACE_DBG_5( LEVEL, ... ) SCNSL_TRACE_DBG_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_DBG_5( LEVEL, ... )
#endif

// ////////////////////////////////////////////////////////////////
// Support macros for WARNING levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_WARNING_0( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_WARNING >= 1)
#define SCNSL_TRACE_WARNING_1( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_WARNING_1( LEVEL, ... )
#endif

#if (SCNSL_WARNING >= 2)
#define SCNSL_TRACE_WARNING_2( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_WARNING_2( LEVEL, ... )
#endif

#if (SCNSL_WARNING >= 3)
#define SCNSL_TRACE_WARNING_3( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_WARNING_3( LEVEL, ... )
#endif

#if (SCNSL_WARNING >= 4)
#define SCNSL_TRACE_WARNING_4( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_WARNING_4( LEVEL, ... )
#endif

#if (SCNSL_WARNING >= 5)
#define SCNSL_TRACE_WARNING_5( LEVEL, ... ) SCNSL_TRACE_WARNING_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_WARNING_5( LEVEL, ... )
#endif

// ////////////////////////////////////////////////////////////////
// Support macros for ERROR levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_ERROR_0( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_ERROR >= 1)
#define SCNSL_TRACE_ERROR_1( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_ERROR_1( LEVEL, ... )
#endif

#if (SCNSL_ERROR >= 2)
#define SCNSL_TRACE_ERROR_2( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_ERROR_2( LEVEL, ... )
#endif

#if (SCNSL_ERROR >= 3)
#define SCNSL_TRACE_ERROR_3( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_ERROR_3( LEVEL, ... )
#endif

#if (SCNSL_ERROR >= 4)
#define SCNSL_TRACE_ERROR_4( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_ERROR_4( LEVEL, ... )
#endif

#if (SCNSL_ERROR >= 5)
#define SCNSL_TRACE_ERROR_5( LEVEL, ... ) SCNSL_TRACE_ERROR_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_ERROR_5( LEVEL, ... )
#endif

// ////////////////////////////////////////////////////////////////
// Support macros for FATAL levels.
// ////////////////////////////////////////////////////////////////


// Always prints.
#define SCNSL_TRACE_FATAL_0( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )

#if (SCNSL_FATAL >= 1)
#define SCNSL_TRACE_FATAL_1( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_FATAL_1( LEVEL, ... )
#endif

#if (SCNSL_FATAL >= 2)
#define SCNSL_TRACE_FATAL_2( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_FATAL_2( LEVEL, ... )
#endif

#if (SCNSL_FATAL >= 3)
#define SCNSL_TRACE_FATAL_3( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_FATAL_3( LEVEL, ... )
#endif

#if (SCNSL_FATAL >= 4)
#define SCNSL_TRACE_FATAL_4( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_FATAL_4( LEVEL, ... )
#endif

#if (SCNSL_FATAL >= 5)
#define SCNSL_TRACE_FATAL_5( LEVEL, ... ) SCNSL_TRACE_FATAL_BASE( LEVEL, __VA_ARGS__ )
#else
#define SCNSL_TRACE_FATAL_5( LEVEL, ... )
#endif


// ////////////////////////////////////////////////////////////////
// Tracing macros.
// ////////////////////////////////////////////////////////////////


#define SCNSL_TRACE_INFO( LEVEL, ... ) ( SCNSL_TRACE_INFO_##LEVEL( LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_LOG( LEVEL, ... ) ( SCNSL_TRACE_LOG_##LEVEL( LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_DBG( LEVEL, ... ) ( SCNSL_TRACE_DBG_##LEVEL( LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_WARNING( LEVEL, ... ) ( SCNSL_TRACE_WARNING_##LEVEL( LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_ERROR( LEVEL, ... ) ( SCNSL_TRACE_ERROR_##LEVEL( LEVEL, __VA_ARGS__ ) )
#define SCNSL_TRACE_FATAL( LEVEL, ... ) ( SCNSL_TRACE_FATAL_##LEVEL( LEVEL, __VA_ARGS__ ) )

#endif // Endif of this header file.
