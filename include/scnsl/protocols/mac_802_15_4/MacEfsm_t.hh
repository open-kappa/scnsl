// -*- SystemC -*-

//
// Copyright (C) 2008-2019
// Davide Quaglia, Francesco Stefanni.
//
// This file is part of SCNSL.
//
// SCNSL is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCNSL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCNSL, in a file named LICENSE.txt. If not, see <http://www.gnu.org/licenses/>.
//
#ifndef SCNSL_PROTOCOLS_MAC_802_15_4_MAC_EFSM_T_HH
#define SCNSL_PROTOCOLS_MAC_802_15_4_MAC_EFSM_T_HH

#include <systemc>

#include "../../scnslConfig.hh"
#include "../../tracing/Traceable_base_t.hh"

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      class MacTransitionFunctions_t;

    } } }

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      /// @brief This is a TLM 802.15.4 implementation, as FSM.
      /// This is a sort of algorithm, which uses an object implemeting
      /// the transition functions.
      ///
      /// Design patterns:
      /// - Non copyable.
      /// - Non assignable.
      ///
      class SCNSL_EXPORT MacEfsm_t :
        public sc_core::sc_module,
        public Scnsl::Tracing::Traceable_base_t
      {
      public:

          /// @name Traits.
          //@{

          /// @brief This enum holds the possible states.
          enum MAC_802_15_4_state_t
          {
              RADIO_OFF,
              RX,
              BACKOFF,
              CCA,
              TX,
              ACK_WAITING,
              SEND_ACK
              // TURN_FOR_ACK,
              // TURN_FOR_TX,
              // TURN_FOR_RX,
              // TURN_FOR_BACKOFF,
              // TO_RX2
          };

          //@}


          /// This enum holds the possible error codes. Zero means success.
          enum Mac_802_15_4_error_t
          {
              SUCCESS = 0,      ///< All fine.
              CHANNEL_ERROR,    ///< Maximum backoff retries reached.
              RETRIES_ERROR,    ///< Maximum number of retransmission reached.
              TIMEOUT_ERROR,    ///< Timeout for receiving user function.
              MAX_PAYLOAD_ERROR ///< Attempted to transmit a apcket with a payload too huge.
          };


          SC_HAS_PROCESS( MacEfsm_t );

          ///
          /// Constructor.
          ///
          /// @param modulename This module name.
          /// @param transitions The class implementing the transition functions.
          /// @param default_state The default state. It defaults on <tt>RADIOOFF</tt>
          /// @param always_on Sets The starting always on flag. default is <tt>true</tt>.
          ///
          MacEfsm_t( const sc_core::sc_module_name modulename,
                     MacTransitionFunctions_t * transitions,
                     MAC_802_15_4_state_t default_state = RX,
                     bool always_on = true);

          ///
          /// Destructor.
          ///
          ~MacEfsm_t();

          /// @name Accessors.
          //@{

          /// @brief Gets the always-on  flag value.
          ///
          /// @return The flag value.
          ///
          bool getAlwaysOn()
              const;

          ///
          /// @brief Sets the always-on  flag value.
          ///
          /// @param on the new flag value.
          ///
          void setAlwaysOn( const bool on );

          //@}

      protected:

          /// @name Internal functions.
          //@{

          ///
          /// @brief This node process. It's an <tt>SC_THREAD</tt>.
          ///
          void _nodeProcess();

          ///
          /// @brief Calculates the backoff period.
          ///
          /// @param exp The exponent to be used.
          ///
          unsigned int _drawBackoff( unsigned int exp )
              const;


          ///
          /// @brief Signal a bug in the 802.15.4 implementation.
          ///
          void _internalError();

          //@}


          /// @name State related members.
          //@{

          /// @brief The current FSM state.
          MAC_802_15_4_state_t _state;

          /// @brief The default FSM state.
          const MAC_802_15_4_state_t _default_state;

          /// @brief Set if the RF module must be always on.
          bool _always_on;

          //@}

          /// @name 802.15.4 MAC variables
          //@{

          /// @brief Number of backoffs executed.
          unsigned int _NB;

          /// @brief The backoff exponent.
          unsigned int _BE;

          /// @brief Number of retransmissions performed.
          unsigned int _NR;

          //@}

          /// @name Implementation related members.
          //@{

          /// @brief The residual time to be waited.
          sc_core::sc_time _timeToWait;

          /// @brief The timestamp before the last <tt>wait()</tt> call.
          sc_core::sc_time _previousTime;

          /// @brief Stores the node implementation.
          MacTransitionFunctions_t * _transitions;

          //@}

          /// @name Events for algorithmic management.
          //@{

          /// @brief Signals that there is a packet to be sent.
          sc_core::sc_event _haveToSendAPacketEvent;

          /// @brief Signals that the flag always_on has been set.
          sc_core::sc_event _alwaysOnEvent;

          /// @brief Signals that a new packet has arrived.
          sc_core::sc_event _newPacketArrivedEvent;

          //@}

      private:

          /// @brief Disabled.
          MacEfsm_t( const MacEfsm_t & );

          /// @brief Disabled.
          MacEfsm_t & operator = ( const MacEfsm_t & );

      };

    } } }

#endif
