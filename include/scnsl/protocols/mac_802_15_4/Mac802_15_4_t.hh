// -*-SystemC-*-

// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_PROTOCOLS_MAC_802_15_4_MAC802_15_4_T_HH
#define SCNSL_PROTOCOLS_MAC_802_15_4_MAC802_15_4_T_HH



/// @file
/// A MAC 802.15.4 communicator.

#include <systemc>

#include "../../scnslConfig.hh"
#include "../../core/data_types.hh"
#include "../../core/Communicator_if_t.hh"
#include "../../core/Packet_t.hh"
#include "../../tracing/Traceable_base_t.hh"

#include "MacEfsm_t.hh"
#include "MacTransitionFunctions_t.hh"

namespace Scnsl { namespace Core {

    class Node_t;
    class Channel_if_t;
    class TaskProxy_if_t;

  } }

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      /// @name Constants.
      //@{

      /// @brief the bitrate of 802.15.4 in b/s.
      enum Mac_802_15_4_Constants
      {
          BITRATE = 250000
      };

      //@}


    } } }

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      /// @brief A MAC 802.15.4 communicator.
      /// This class does not perform multiplexing,
      /// thus it is not possible to call bindChannel() and
      /// bindTaskProxy() methods.
      ///
      /// Design patterns:
      /// - Non copyable.
      /// - Non assignable.
      ///
      class SCNSL_EXPORT Mac802_15_4_t:
        public sc_core::sc_module,
        public Scnsl::Core::Communicator_if_t,
        public Scnsl::Tracing::Traceable_base_t
      {
      public:

          /// @name Traits.
          //@{

          /// @brief The node type.
          typedef Scnsl::Core::Node_t Node_t;

          /// @brief The channel type.
          typedef Scnsl::Core::Channel_if_t Channel_if_t;

          /// @brief The taskproxy type.
          typedef Scnsl::Core::TaskProxy_if_t TaskProxy_if_t;

          /// @brief The packet type.
          typedef Scnsl::Core::Packet_t Packet_t;

          //@}


          SC_HAS_PROCESS( Mac802_15_4_t );

          /// @brief Constructor.
          ///
          /// @param modulename The module name.
          /// @param node The host node.
          /// @param ack_required Sets if the protocol is confirmed.
          /// @param short_addresses True if using 16-bit addresses instead of 64-bit addresses.
          ///
          Mac802_15_4_t( const sc_core::sc_module_name modulename,
                         Node_t * node,
                         const bool ack_required,
                         const bool short_addresses );

          /// @brief Destructor.
          virtual
          ~Mac802_15_4_t();

          /// @name Inherited methods from Communicator.
          //@{

          virtual
          void setCarrier( const Channel_if_t * ch, const carrier_t c ) override;

          virtual
          errorcode_t send( const Packet_t & p ) override;

          virtual
          errorcode_t receive( const Packet_t & p ) override;

          virtual
          void bindTaskProxy( const TaskProxy_if_t * tp,
                              Communicator_if_t * c ) override;

          virtual
          void bindChannel( const Channel_if_t * ch,
                            Communicator_if_t * c ) override;

          //@}

          /// @name Support methods.
          //@{

          /// @brief Gets a packet from protocol to lower layers.
          void transport( Packet_t & p );

          /// @brief Inherited trace setup method.
          virtual
          void registerTracer( Scnsl::Tracing::Tracer_t * t ) override;

          //@}

      protected:

          /// @brief The transition functions.
          MacTransitionFunctions_t _transitions;

          /// @brief The MAC EFSM.
          MacEfsm_t _efsm;

          /// @brief Flag about protocol confirmed.
          const bool _ack_required;

          /// @brief Packet used to get data.
          Packet_t _receivedPacket;

      private:

          /// @name Routines.
          //@{

          /// @brief Reads a packet from the protocol, and fowards it to upper layers.
          void _readingRoutine();

          //@}

          /// @brief Disabled copy constructor.
          Mac802_15_4_t( const Mac802_15_4_t & );

          /// @brief Disabled assignment operator.
          Mac802_15_4_t & operator = ( const Mac802_15_4_t & );

      };

    } } }



#endif
