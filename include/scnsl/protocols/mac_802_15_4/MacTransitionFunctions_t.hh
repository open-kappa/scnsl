// -*- SystemC -*-

//
// Copyright (C) 2008-2019
// Davide Quaglia, Francesco Stefanni.
//
// This file is part of SCNSL.
//
// SCNSL is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SCNSL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with SCNSL, in a file named LICENSE.txt. If not, see <http://www.gnu.org/licenses/>.
//
#ifndef SCNSL_PROTOCOLS_MAC_802_15_4_MAC_TRANSITION_FUNCTIONS_T_HH
#define SCNSL_PROTOCOLS_MAC_802_15_4_MAC_TRANSITION_FUNCTIONS_T_HH

#include <map>
#include <systemc>

#include "../../scnslConfig.hh"
#include "../../core/data_types.hh"
#include "../../core/Packet_t.hh"

#include "../../tracing/Traceable_base_t.hh"

#include "MacEfsm_t.hh"

namespace Scnsl { namespace Core {

    class Node_t;

  } }

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      class Mac802_15_4_t;

    } } }

namespace Scnsl { namespace Protocols { namespace Mac_802_15_4 {

      /// @brief This is an implementation of MAC 802.15.4.
      ///
      /// Design patterns:
      /// - Non copyable.
      /// - Non assignable.
      ///
      class SCNSL_EXPORT MacTransitionFunctions_t:
        public sc_core::sc_module,
        public Scnsl::Tracing::Traceable_base_t
      {
      public:

          /// @name Traits.
          //@{

          /// @brief The node type.
          typedef Scnsl::Core::Node_t Node_t;

          /// @brief The packet type.
          typedef Scnsl::Core::Packet_t Packet_t;

          /// @brief The error code type.
          typedef Scnsl::Core::errorcode_t errorcode_t;

          /// @brief The size type.
          typedef Scnsl::Core::size_t size_t;

          /// @brief The carrier type.
          typedef Scnsl::Core::carrier_t carrier_t;

          /// @brief the errorcode type.
          typedef MacEfsm_t::Mac_802_15_4_error_t Mac_802_15_4_error_t;


          /// @brief Map from nodes to seqno.
          typedef std::map< const Node_t *, unsigned int > SeqnoMap_t;

          //@}


          SC_HAS_PROCESS( MacTransitionFunctions_t );

          /// @brief Constructor.
          ///
          /// @param modulename This module name.
          /// @param communicator The wrapping communicator.
          /// @param node The host node.
          /// @param short_addresses True if is using short addresses.
          ///
          MacTransitionFunctions_t(
              const sc_core::sc_module_name modulename,
              Mac802_15_4_t * communicator,
              Node_t * node,
              const bool short_addresses );

          /// @brief Destructor.
          ~MacTransitionFunctions_t();

          /// @name Application-oriented interface functions.
          /// The caller shall be an SC_THREAD.
          //@{

          errorcode_t send( const Packet_t & p, const bool ack_required = true );

          errorcode_t receive( Packet_t & p );

          //@}

          /// @name Communication functions from lower layers.
          //@{

          void setCarrier( const carrier_t c );

          void transport( const Packet_t & data );

          //@}


          /// @name Configuration functions.
          //@{

          /// @brief Switch the implementation from read and send modes.
          /// This is because the channel is half duplex.
          ///
          /// @param read_mode_on True if it is in read mode.
          ///
          void setReadMode( const bool read_mode_on );

          /// @brief Registers internal events to be fired.
          ///
          /// @param haveToSendAPacketEvent  Signals that there is a packet to be sent.
          /// @param newPacketArrivedEvent Signals that a new packet has arrived.
          ///
          void registerInternalEvents( sc_core::sc_event & haveToSendAPacketEvent, sc_core::sc_event & newPacketArrivedEvent );

          //@}


          /// @name Receiving functions.
          //@{

          /// @brief Reads a packet from the input port for tests.
          /// This function shall also check packet validity.
          /// If the packet is invalid, each next packet check shall return <tt>false</tt>.
          /// An eventual call to the manageReadDataPacket() with an invalid packet shall do nothing.
          /// The invalid packet will be disposed by the algorithm with a call to disposeReadPacket().
          ///
          void readPacket();

          /// @brief Manages a read <em>data</em> packet.
          /// As instance, it could push the packet into a queue.
          /// The managed packet shall not be disposed.
          ///
          void manageReadDataPacket();

          /// @brief Dispose the read packet.
          ///
          void disposeReadPacket();

          //@}


          /// @name Sending functions.
          //@{

          ///
          /// @brief Prepare a packet for tests and sending.
          ///
          void prepareOutgoingDataPacket();

          ///
          /// @brief Send the outgoing data packet.
          ///
          void sendPacket();

          ///
          /// @brief Dispose the outgoing data packet.
          ///
          void disposeSentPacket();

          ///
          /// @brief Send an ack, for the read packet.
          ///
          void sendAck();

          //@}



          /// @name Check functions.
          //@{

          ///
          /// @brief Check if there is a packet to be sent.
          ///
          /// @return True if there is a packet to be sent.
          ///
          bool haveToSendAPacket() const;

          ///
          /// @brief Check if received packet is broadcast.
          ///
          /// @return True if received packet is broadcast.
          ///
          bool isReceivedPacketBroadcast() const;

          ///
          /// @brief Check if received packet needs an ack.
          ///
          /// @return True if an ack is required.
          ///
          bool isAckRequiredForReceivedPacket() const;

          ///
          /// @brief Check if a new packet is arrived.
          ///
          /// @return True if a new packet is arrived.
          ///
          bool newPacketArrived() const;

          ///
          /// @brief Check if an ack is required for the outgoing packet.
          ///
          /// @return True if the ack is required.
          ///
          bool isAckRequiredForSentPacket() const;

          ///
          /// @brief Check if sent packet is broadcast.
          ///
          /// @return True if sent packet is broadcast.
          ///
          bool isSentPacketBroadcast() const;

          ///
          /// @brief Check if received packet is an ack.
          ///
          /// @return True if it is an ack.
          ///
          bool isAck() const;

          ///
          /// @brief Check if received ack is the one expected.
          ///
          /// @return True if it is the expected ack.
          ///
          bool isExpectedAck() const;

          ///
          /// @brief Checks if the channel is busy.
          ///
          /// @return True if the channel is busy.
          ///
          bool isChannelBusy() const;

          //@}


          /// @name Error managing functions.
          //@{

          ///
          /// @brief Signals a failure in transmision of a packet.
          ///
          /// @param error_code The error code.
          ///
          void failure( const Mac_802_15_4_error_t error_code );

          //@}


      protected:

          /// @name Constants.
          //@{

          /// @brief The maximum payload.
          const size_t _MAX_PAYLOAD;

          /// @brief The data header size.
          const size_t _HEADER_SIZE;

          //@}

          ///@name Simulation variables.
          //@{

          /// @brief The next sequence number to be used for transmission.
          unsigned int _sequence_number;

          /// @brief The last sequence number transmitted.
          unsigned int _last_sequence_number_transmitted;

          /// @brief The last error code.
          mutable errorcode_t _error_code;

          /// @brief Flag for channel status.
          bool _channel_busy;

          /// @brief The wrapping communicator.
          Mac802_15_4_t * _dataOutput;

          //@}

          /// @name Algorithmic support members.
          //@{

          /// @brief True if the incoming packet is set.
          bool _in_set;

          /// @brief True if the outgoing packet is set.
          bool _out_set;

          /// @brief True if a new packet is arrived.
          bool _new_packet_arrived;

          /// @brief True if a new ack is arrived.
          bool _new_ack_arrived;

          /// @brief True if a packet is ready to be read.
          mutable bool _packet_readable;

          /// @brief True if a new packet shall be sent.
          bool _new_packet_send;

          /// @brief Used to record the transmission state.
          bool _read_mode_on;

          /// @brief Records last seqno.
          SeqnoMap_t _recvSeqNumbers;

          /// @brief The incoming packet.
          Packet_t _inPacket;

          /// @brief The outgoing packet.
          Packet_t _outPacket;

          /// @brief The ack packet.
          Packet_t _ack;

          /// @brief The node hosting this protocol.
          Node_t * _thisNode;

          //@}


          ///@name Events.
          //@(

          /// @brief Signals that the incoming queue is not empty.
          mutable sc_core::sc_event _incomingQueueNotEmpty;

          /// @brief Signals that the outgoing queue is not full.
          sc_core::sc_event _outgoingQueueNotFull;

          /// @brief Signal that a packet has been sent.
          sc_core::sc_event _newPacketTransmission;

          /// @brief Signal that an ack waiting is completed.
          mutable sc_core::sc_event _ackWaitingCompleted;

          /// @brief Bind to the algorithmic haveToSendAPacketEvent event.
          sc_core::sc_event * _haveToSendAPacketEvent;

          /// @brief Bind to the algorithmic newPacketArrivedEvent event.
          sc_core::sc_event * _newPacketArrivedEvent;

          //@}


      private:

          /// @brief Disabled.
          MacTransitionFunctions_t( const MacTransitionFunctions_t & );

          /// @brief Disabled.
          MacTransitionFunctions_t & operator = ( const MacTransitionFunctions_t & );

      };

    } } }


#endif
