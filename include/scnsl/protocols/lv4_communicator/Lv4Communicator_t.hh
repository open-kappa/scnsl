#ifndef SCNSL_LV4_COMMUNICATOR_HH
#define SCNSL_LV4_COMMUNICATOR_HH

#include "../../core/Communicator_if_t.hh"
#include "../../core/SocketMap.hh"
#include "../../tracing/Traceable_base_t.hh"
#include "Lv4ProtocolPacket_t.hh"
#include "UdpProtocol.hh"
#include "connections/TcpConnectionFactory.hh"

///@file The communicator implementing the tcp protocol

namespace Scnsl { namespace Core {
class Node_t;
class Channel_if_t;
class TaskProxy_if_t;
}}  // namespace Scnsl::Core

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief Class implementing the tcp protocol.
/// This class allow multiple connection to be active at the same time by using
/// different Connection object to store the individual variables for each
/// session At the moment only the ByteConnection variant is supported
class SCNSL_EXPORT Lv4Communicator_t:
    public sc_core::sc_module,
    public Scnsl::Core::Communicator_if_t,
    public Scnsl::Tracing::Traceable_base_t
{
public:
    typedef Scnsl::Core::byte_t byte_t;
    typedef Scnsl::Core::Node_t Node_t;
    typedef Scnsl::Core::Channel_if_t Channel_if_t;
    typedef Scnsl::Core::TaskProxy_if_t TaskProxy_if_t;
    typedef Scnsl::Core::Task_if_t Task_if_t;
    typedef Scnsl::Core::Packet_t Packet_t;
    typedef Scnsl::Core::errorcode_t errorcode_t;
    typedef Scnsl::Core::counter_t counter_t;
    typedef Scnsl::Core::socket_properties_t socket_properties_t;

    SC_HAS_PROCESS(Lv4Communicator_t);

    ///@brief The constructor
    ///@param modulename the name of the module
    ///@param log True if the the connections created must produce log about the
    ///tcp status (no effect on other type of log)
    ///@param type the connection type used by the communicator
    Lv4Communicator_t(
        const sc_core::sc_module_name modulename,
        bool log = true,
        TcpConns type = TCP_BYTE_CONNECTION);

    ///@brief The default destructor
    virtual ~Lv4Communicator_t();

    ///@name Inherited methods from communicator interface
    //@{
    virtual void setCarrier(const Channel_if_t * ch, const carrier_t c)
        override;

    virtual errorcode_t send(const Packet_t & p) override;

    virtual errorcode_t receive(const Packet_t & p) override;

    virtual void registerTracer(Scnsl::Tracing::Tracer_t * t) override;
    //@}

    ///@name Setters
    //@{
    ///@brief Set the rcvwnd value
    ///@param rcvwnd the new value to use
    void setRcvwnd(size_t rcvwnd);

    ///@brief Set the extra bytes of the packet due to u and mac headers.
    ///Default value is 0 for both
    ///@param ip_header_size the size in bytes of the ip header (can be 0)
    ///@param mac_header_size the size in bytes of the mac header (can be 0)
    void setExtraHeaderSize(
        size_t ip_header_size = 0,
        size_t mac_header_size = 0);

    ///@brief Set the segment size for the protocol
    ///@param segmentSize the segment size in bytes
    void setSegmentSize(size_t segmentSize);

    ///@brief Set the algorithm for the slow start phase
    ///@param ssa the slow start variant to use
    void setSlowStartAlg(TcpAlgs ssa);

    ///@brief Set the algorithm for the congestion avoidance phase
    ///@param caa the congestion avoidance variant to use
    void setCongestionAvoidanceAlg(TcpAlgs caa);

    ///@brief Set the algorithm for the fast recovery phase
    ///@param fra the fast recovery variant to use
    void setFastRecoveryAlg(TcpAlgs fra);

    ///@brief Activate or deactivate the nodelay modification to the protocol
    ///@param flag true if the nodelay option must be activated
    void set_TCP_NO_DELAY(bool flag);

    ///@brief Set the maximum size in bytes of the write buffer
    void setWmemSize(size_t wmemSize);

    ///@brief set the retransmiossio timeout
    void setRto(sc_core::sc_time rto);

    ///@brief set the number of segment to responde immediately (default 0)
    void setFastAckCount(unsigned short fastAckCount);

    ///@brief set to true to count only full size segment to send ack
    void setSendAckAfterFullSegment(bool sendAckAfterFullSegment);
    //@}

private:
    ///@name parameters for future connections
    //@{
    ///@brief connection object type
    TcpConns _conn_type;

    ///@brief The protocol receive window
    size_t _rcvwnd;

    ///@brief The sum of ip + mac headers (tcp header is excluded)
    size_t _full_header_size;

    ///@brief The protocol segment size
    size_t _segment_size;

    ///@brief The slow start algorithm
    TcpAlgs _ssa;

    ///@brief The congestion avoidance algorithm
    TcpAlgs _caa;

    ///@brief The fast recovery algorithm
    TcpAlgs _fra;

    ///@brief The max size og the write buffer
    size_t _wmem_size;

    ///@brief True if the protocol must use the nodelay variant
    bool _tcp_no_delay;

    ///@brief Timeout value for retransmission (default 1 Sec)
    sc_core::sc_time _rto;

    ///@brief the number of packet from the start of the connection that will
    ///receive an immediate ack
    /// (0 by default)
    unsigned short _fast_ack_count;

    ///@brief true if the connection must send an immediate ack after two
    ///full-sized segment. If false the
    /// connection will send an ack after every other segment received
    bool _send_ack_after_full_segment;
    //@}

    ///@brief Map to bind a socket to a connection id
    std::map<socket_properties_t, unsigned int> _id_socket_map;

    ///@brief Binding between connections id and connections objects
    std::map<unsigned, TcpConnection_if_t *> * _connections;

    ///@brief Event queue for sporadic events
    EventManager_t _asynch_timeout_queue;

    ///@brief Event queue for connection that need to send data
    EventManager_t _synch_timeout_queue;

    ///@brief true if the connections created by the communicator must produce
    ///log regarding the tcp/packets status
    /// (normal log will still be produced). Default is true.
    bool _status_log;

    ///@brief Manage all the events that happen with no fixed time
    void asynchEventManager();

    ///@brief Manage all the events that need to send data at some time
    void sendEventManager();

    // disable copy constructor and assignment operator
    Lv4Communicator_t(const Lv4Communicator_t &);
    Lv4Communicator_t & operator=(const Lv4Communicator_t &);
};
}}}  // namespace Scnsl::Protocols::Network_Lv4

#endif  // !SCNSL_PROTOCOL_TCP_HPP
