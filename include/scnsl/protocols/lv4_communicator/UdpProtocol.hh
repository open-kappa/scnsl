#ifndef SCNSL_UDP_PROTOCOL_HH
#define SCNSL_UDP_PROTOCOL_HH

#include "../../core/Packet_t.hh"
#include "../../core/SimpleProtocolPacket_t.hh"
#include "../../core/SocketMap.hh"
#include "../../core/data_types.hh"
#include "Lv4ProtocolConstants.hh"
#include "Lv4ProtocolPacket_t.hh"

///@file Methods allowing for data to be sent via UDP protocol. All methods are
/// static and allow for a buffer to be wrapped or un-wrapped woth an UDP
/// segment
namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

namespace UdpProtocol {
typedef Scnsl::Core::Packet_t Packet_t;
typedef Scnsl::Core::SimpleProtocolPacket_t SimpleProtocolPacket_t;
typedef Scnsl::Core::byte_t byte_t;
typedef Scnsl::Core::size_t size_t;
typedef Scnsl::Core::socket_properties_t socket_properties_t;

///@brief Create a new UDP segment starting from a  given packet
///@param p the packet to wrap
///@param sp struct contining all informations about source and destination
///@param full_header_size the full size of the header (included of IP and MAC)
///@param log true if a log must be produced
///@return a packet containing a UDP segment
SCNSL_EXPORT Packet_t createUDPSegment(
    const Packet_t p,
    socket_properties_t sp,
    size_t full_header_size,
    bool log,
    std::string comm_name);

///@brief Unwrap a buffer stored inside a UDP segment
///@param p the packet to unwrap
///@param sp struct contining all informations about source and destination
///@param log true if a log must be produced
///@return a packet containing the data to be received
SCNSL_EXPORT Packet_t removeUDPSegment(
    const Packet_t p,
    socket_properties_t sp,
    bool log,
    std::string comm_name);

}}}}  // namespace Scnsl::Protocols::Network_Lv4::UdpProtocol

#endif
