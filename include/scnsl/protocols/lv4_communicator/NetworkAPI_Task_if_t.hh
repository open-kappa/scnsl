#ifndef SCNSL_LV4_API_TASK_IF_T_HH
#define SCNSL_LV4_API_TASK_IF_T_HH

#include "../../core/PacketCommand.hh"
#include "../../system_calls/NetworkSyscalls.hh"
#include "../../tlm/TlmTask_if_t.hh"
#include "../../tlm/tlm_data_types.hh"
#include "Lv4ProtocolConstants.hh"
#include "Socket_t.hh"

#include <chrono>
#include <queue>

///@file The Tcp API task, that allow to use all the socket function to
///communicate between two applications

using namespace Scnsl::Syscalls;

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief A Tlm Task interface that allow an application to use socket
///primitives.
/// To use this class simply extend this class overriding the main() method.
class SCNSL_EXPORT NetworkAPI_Task_if_t: public Scnsl::Tlm::TlmTask_if_t
{
public:
    typedef std::pair<ip_addr_t, port_no_t> source_pair_t;
    typedef Syscalls::fd_set fd_set;

    SC_HAS_PROCESS(NetworkAPI_Task_if_t);

    ///@brief Class constructor
    ///@param name the module name
    ///@param id the task id
    ///@param n the node the task is running on
    ///@param proxies the number of proxies this task has
    ///@param  wmem_size the size in byte of the tcp send buffer (default is
    ///4194304 bytes)
    NetworkAPI_Task_if_t(
        sc_core::sc_module_name name,
        const task_id_t id,
        Scnsl::Core::Node_t * n,
        const size_t proxies,
        size_t wmem_size = DEFAULT_WMEM);

    ///@brief Default destructor
    virtual ~NetworkAPI_Task_if_t();

    ///@name Standard API for socket communication
    //@{
    ///@brief Create a socket and return a file descriptor to it
    socketFD_t internal_socket(int domain, int type, int protocol);

    ///@brief Allow socket to pass from synchronous to asynchronous
    int internal_fcntl(socketFD_t sfd, int opt, int val);

    ///@brief Bind a socket to a specific ip address/port
    int internal_bind(
        socketFD_t sfd,
        const struct sockaddr * socket_addr,
        int addrlen);

    ///@brief Create a receive queue for pending connection of the given length
    ///@param sfd the socket file descriptor
    ///@param backlog the length of the queue
    int internal_listen(socketFD_t sfd, int backlog);

    ///@brief Wait until a connection request is received
    socketFD_t internal_accept(
        socketFD_t sfd,
        struct sockaddr * addr,
        socklen_t * addrnen);

    ///@brief Establish a connection to a specific socket
    int internal_connect(
        socketFD_t sfd,
        const struct sockaddr * socket_addr,
        int addrlen);

    ///@brief Send a buffer using the specified socket, the name is left to
    ///override the
    /// Task_if_t send method
    ///@param sfd the socket used to send data
    ///@param buffer the buffer to send
    ///@param length the number of byte to send
    ///@param flags the flags used
    int internal_send(
        socketFD_t sfd,
        const void * buffer,
        int length,
        int flags);

    ///@brief Send a buffer to a specific destination
    ///@param sfd the socket used to send data
    ///@param buffer the buffer to send
    ///@param length the number of byte to send
    ///@param flags the flags used
    ///@param dest_addr the struct with the destination address
    ///@param addrlen the size of the address structure
    int internal_sendto(
        socketFD_t sfd,
        const void * buffer,
        int length,
        int flags,
        const struct sockaddr * dest_addr,
        socklen_t addrlen);

    ///@brief Receive a buffer from the specified socket
    ///@param sfd The socket where the data are read
    ///@param buffer the buffer where the data are stored
    ///@param length the maximum number of byte read from the receive buffer
    ///@param flags the flags used
    int internal_recv(socketFD_t sfd, void * buffer, int length, int flags);

    ///@brief Receive a buffer from the specified socket
    ///@param sfd The socket where the data are read
    ///@param buffer the buffer where the data are stored
    ///@param length the maximum number of byte read from the receive buffer
    ///@param flags the flags used
    ///@param src_addr the struct with the source address
    ///@param addrlen the size of the address structure
    int internal_recvfrom(
        socketFD_t sfd,
        void * buffer,
        int length,
        int flags,
        struct sockaddr * src_addr,
        socklen_t * addrlen);

    ///@brief Block the execution until at least one of the file descriptor in
    ///the set is ready
    ///@param nfds the maximum file descriptor value in all sets +1
    ///@param readfds the set containing all the read fd to watch
    ///@param write the set containing all the write fd to watch
    ///@param exceptfds the set containing all the exceptions fd to watch
    ///@param timeout the maximum timeout to block the program
    int internal_select(
        int nfds,
        fd_set * readfds,
        fd_set * writefds,
        fd_set * exceptfds,
        struct timeval * timeout);

    ///@brief Block the execution until at least one of the file descriptor in
    ///the set is ready
    ///@param fds array of File descriptors to monitor
    ///@param nfds size of the fds array
    ///@param timeout the maximum timeout in milliseconds to block the program
    int internal_poll(
        struct pollfd *fds,
        nfds_t nfds,
        int timeout);

    ///@brief return an option from a socket. (only error option for now)
    ///@param sockfd the file descriptor
    ///@param level the level the option must be retrieved (only socket for now)
    ///@param optlen the name of the option
    ///@param optval pointer where the return value is stored
    ///@param optlen size of the element pointed by optval
    int internal_getsockopt(
        int sockfd,
        int level,
        int optname,
        void * optval,
        socklen_t * optlen);

    ///@brief Set an option from a socket. (only error option for now)
    ///@param sockfd the file descriptor
    ///@param level the level the option must be retrieved (only socket for now)
    ///@param optlen the name of the option
    ///@param optval pointer where the return value is stored
    ///@param optlen size of the element pointed by optval
    int internal_setsockopt(
        int sockfd,
        int level,
        int optname,
        const void * optval,
        socklen_t optlen);

    ///@brief return the host name
    ///@param name the buffer to store the name
    ///@param len the buffer length. If the name exceed, it is truncated
    int internal_gethostname(char * name, size_t len);

    ///@brief return the address a socket is bound
    ///@param sockfd the socket fd
    ///@param addr the struct to store the result
    ///@param addrlen the size of the struct
    int internal_getsockname(int sockfd, struct sockaddr * addr, socklen_t * addrlen);

    ///@brief returns the address of the peer connected to the socket sockfd
    ///@param sockfd the socket fd
    ///@param addr the struct to store the result
    ///@param addrlen the size of the struct
    int internal_getpeername(int sockfd, struct sockaddr * addr, socklen_t * addrlen);

    ///@brief Return a addrinfo struct containing info based on the node (ip)
    ///and service (port) specified
    /// To simplify, only one element is returned, and the hints struct, used to
    /// filter the results, is left unused
    ///@note Thi method works only with ip ad port number-like string. Service
    ///name or host name won't work
    ///@param node the string with the ip (can be NULL)
    ///@param service the string with the port
    ///@param hints struct used to "filter" the results. Not used here
    ///@param res list of result (only one since multiple ips are not managed)
    int internal_getaddrinfo(
        const char * node,
        const char * service,
        const addrinfo * hints,
        addrinfo ** res);

    ///@brief Close a socket
    ///@param sfd The socket to internal_close
    int internal_close(socketFD_t sfd);

    ///@brief Close only part of a fullduplex connection
    ///@param sockfd the socket file descriptor
    ///@param how which part of the connection must be stopped
    int internal_shutdown(int sockfd, int how);

    ///@brief return the last internal error value;
    ///@return the last internal error value
    int * getInternalError();

    ///@brief Create a linked list of `struct ifaddrs' structures, one for each
    /// network interface on the host machine. For now it returns only one element,
    /// as multiple netwrok interfaces are not allowed  
    ///@return  0 on success, -1 on error.
    int internal_getifaddrs (struct ifaddrs **__ifap);


    ///@brief Wait fo a specifica amount of time to match time spent in user
    ///mode
    virtual void waitForTime();

    ///@brief Start counting the time passed in user mode
    void initTime();

    ///@brief The main method, used to implement the user's code
    virtual void main() = 0;
    //@}

private:
    ///@brief Value for the next file descriptor generated
    static socketFD_t _next_fd;

    ///@brief Bind a socket fd to a unique Task
    static std::map<socketFD_t, NetworkAPI_Task_if_t *> * _fdMap;

    ///@brief Binding between unique id and local socket address
    std::map<const socketFD_t, Socket_t *> * _id_socket_map;

    ///@brief Allow to know if a socket is already bound to a source ip/port
    std::map<source_pair_t, Socket_t *> * _bound_sockets;

    ///@biref Map to store socket in listening mode
    std::map<const socketFD_t, Socket_t *> * _listenmap;

    ///@biref Map to store connection between taskproxys and socket objets
    std::map<Scnsl::Core::TaskProxy_if_t *, Socket_t *> * _tp_socket_map;

    ///@brief Wake the selelct when a socket is ready
    sc_core::sc_event _select_awake;

    ///@brief last error set by a call
    int _errno;

    ///@brief Last time this api returned, used to calculate time spent outside
    ///scnsl control
    std::chrono::time_point<std::chrono::high_resolution_clock> _start_time;

    ///@brief store data to know which socket must be deleted
    struct socketDeleter
    {
        socketFD_t fd;  // file descriptor
        Socket_t * socket;  // socket object
    };

    ///@brief queue to store delete order
    std::queue<socketDeleter> _deleteQueue;

    ///@brief event queue to delete closed socket
    sc_core::sc_event_queue _deleteEventQueue;

    ///@brief process to delete socket after a certain expiratin time
    void deleteSocket();

    ///@biref Send a command to a lower communicator
    ///@param tpKey the taskproxy key where te command is send
    ///@param buffer the buffer with the command to send
    ///@param s the size of the buffer
    void sendCommand(
        const std::string & tpKey,
        byte_t * buffer,
        const size_t s,
        const label_t label = 0);

    ///@brief Hide the send function from Tlm_Task interface
    void send(
        const std::string & tpKey,
        byte_t * buffer,
        const size_t s,
        const label_t label = 0);

    ///@brief Inherited method from the TlmTask_if_t interface
    virtual void b_transport(
        tlm::tlm_generic_payload & p,
        sc_core::sc_time & t);
};
}}}  // namespace Scnsl::Protocols::Network_Lv4
#endif
