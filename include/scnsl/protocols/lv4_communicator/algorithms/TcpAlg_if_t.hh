#ifndef SCNSL_TCP_ALG_IF_T_HH
#define SCNSL_TCP_ALG_IF_T_HH

#include "../../../core/data_types.hh"

///@file An interface to allow the easy implementation of all the various
///congestion control algorithms for tcp

// forward declaration
namespace Scnsl { namespace Protocols { namespace Network_Lv4 {
class TcpConnection_if_t;
}}}  // namespace Scnsl::Protocols::Network_Lv4

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief An interface to allow easy implementation of congestion control
///algorithms and easy access for class using them
class TcpAlg_if_t
{
public:
    ///@brief The sie type
    typedef Scnsl::Core::size_t size_t;

    ///@brief Constructor
    ///@param conn the connection this algorithm refers to
    TcpAlg_if_t(TcpConnection_if_t * conn);

    ///@brief Default destructor
    virtual ~TcpAlg_if_t();

    ///@brief Virtual method used to implement the chosen algorithm behaviour
    ///@param timeout_occurred true if a packet timout has been detected
    ///@param is_last_ack_duplicate true if the last ack received is a duplicate
    ///@param acknowledge_bytes the number of byte acknowledged by the last ack
    ///received
    virtual void EFSM(
        bool timeout_occurred,
        bool is_last_ack_duplicate,
        size_t acknowledge_bytes) = 0;

    ///@brief Virtual method used to set the connection's variables when
    ///switching from an algorithm to another
    virtual void initAlg() = 0;

protected:
    ///@brief The connection which the algorithm needs to manage
    TcpConnection_if_t * _conn;
};

}}}  // namespace Scnsl::Protocols::Network_Lv4
#endif  // SCNSL_TCP_ALG_IF_T_HH
