#ifndef SCNSL_TCP_ALG_FR_RENO_T_HH
#define SCNSL_TCP_ALG_FR_RENO_T_HH

#include "TcpAlg_if_t.hh"

///@file The Fast Recovery algorithm for tcp congestion control

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief The congestion control algorithm for fast recovery state
class TcpAlg_FR_Reno_t: public TcpAlg_if_t
{
public:
    ///@brief Constructor
    ///@param conn the connection this algorithm refers to
    TcpAlg_FR_Reno_t(TcpConnection_if_t * conn);

    ///@brief Default destructor
    ~TcpAlg_FR_Reno_t();

    ///@name Inherited method from interface
    //@{
    void EFSM(
        bool timeout_occurred,
        bool is_last_ack_duplicate,
        size_t acknowledge_bytes) override;

    void initAlg() override;
    //@}
};
}}}  // namespace Scnsl::Protocols::Network_Lv4

#endif  // SCNSL_TCP_ALG_FR_RENO_T_HH
