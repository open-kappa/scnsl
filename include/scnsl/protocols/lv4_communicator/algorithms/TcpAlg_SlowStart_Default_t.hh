#ifndef SCNSL_TCP_ALG_SLOWSTART_HH
#define SCNSL_TCP_ALG_SLOWSTART_HH

#include "TcpAlg_if_t.hh"

///@file The slow start algorithm for tcp congestion control

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief The congestion control algorithm based on Slow start
class TcpAlg_SlowStart_Default_t: public TcpAlg_if_t
{
public:
    ///@brief Constructor
    ///@param conn the connection this algorithm refers to
    TcpAlg_SlowStart_Default_t(TcpConnection_if_t * conn);

    ///@brief Default destructor
    ~TcpAlg_SlowStart_Default_t();

    ///@name Inherited method from interface
    //@{
    void EFSM(
        bool timeout_occurred,
        bool is_last_ack_duplicate,
        size_t acknowledge_bytes) override;

    void initAlg() override;
    //@}
};
}}}  // namespace Scnsl::Protocols::Network_Lv4
#endif  // SCNSL_TCP_ALG_SLOWSTART_HH
