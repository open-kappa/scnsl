#ifndef SCNSL_TCP_ALG_FACTORY_HH
#define SCNSL_TCP_ALG_FACTORY_HH

#include "../Lv4ProtocolConstants.hh"
#include "TcpAlg_if_t.hh"

// All congestion control algorithms must be here
#include "TcpAlg_CA_AIMD_t.hh"
#include "TcpAlg_FR_Reno_t.hh"
#include "TcpAlg_SlowStart_Default_t.hh"

///@file A factory to create all types of congestion control algorithms

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief A factory to create all kind of tcp algorithm classes
class TcpAlgFactory
{
public:
    ///@brief Create a new algorithm class based on the desired algorithm and
    ///binded to the connection
    ///@param alg the algorithm type to create
    ///@param conn the connection that will use the algorithm
    static TcpAlg_if_t * getAlgorithm(TcpAlgs alg, TcpConnection_if_t * conn);
};
}}}  // namespace Scnsl::Protocols::Network_Lv4

#endif  // SCNSL_TCP_ALG_FACTORY_HH
