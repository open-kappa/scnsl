#ifndef SCNSL_API_SOCKET_T_HH
#define SCNSL_API_SOCKET_T_HH

#include "../../scnslConfig.hh"
#include <queue>
#include <unordered_set>
#include <systemc>
#include <string>


#define NONE_SET 0
#define READ_SET 1
#define WRITE_SET 2

namespace Scnsl { namespace Core {
    struct socket_properties_t;
    typedef unsigned char byte_t;

} }

///@file A class representing a high-level socket (used at application level)
namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

    const int SOCK_STREAM = 0;
    const int SOCK_DGRAM = 1;

    ///@brief The type of opening for a socket
    enum open_type:unsigned
    {
        NOT_SET,
        PASSIVE_OPEN, //server side
        ACTIVE_OPEN //client side
    };

    typedef unsigned int socketFD_t;
    typedef unsigned ip_addr_t;
    typedef unsigned short port_no_t;

    struct messageInfo
    {
        ulong size;
        ip_addr_t src_ip;
        port_no_t src_port;
    };


    ///@brief A tcp socket class, store all the relative information for high level information
    class SCNSL_EXPORT Socket_t
    {
    public:
        typedef Scnsl::Core::byte_t byte_t ;
        typedef Scnsl::Core::socket_properties_t  socket_properties_t;

        ///@brief Create an empty socket
        ///@param fd the file descriptor for this socket
        ///@param select_wake the event to notify if a select is pending
        Socket_t(socketFD_t  fd, int type, sc_core::sc_event* select_wake);

        ///@brief Create a socket object based on a specific socket
        ///@param fd the file descriptor for this socket
        ///@param sp the socket containing all the information of this class
        ///@param select_wake the event to notify if a select is pending
        Socket_t(socketFD_t  fd, int type, Scnsl::Core::socket_properties_t sp,
                    sc_core::sc_event* select_wake);
        ///@brief Default destructor
        ~Socket_t();

        ///@name Connection request managing
        //@{
        ///@brief Create a queue to receive connection requests
        void create_queue(unsigned length);

        ///@brief Return true if the connection request queue is empty
        bool is_queue_empty();

        ///@brief Return the first connection  request in the queue;
        ///@return A pointer to the socket object with all the data about the connection
        Socket_t* getConnection();

        ///@brief Add a connection request to the queue
        void pushConnectionRequest(Socket_t* s);

        ///@brief Notify the presence of a connection request in the queue
        void notifyConnectionRequest();
        //@}

        ///@brief Move data from the receive queue to a buffer
        ///@return number of copyed bytes
        int moveDataToBuffer (byte_t* buffer,int length);

        ///@brief Move data from a buffer to the receive queue
        void moveDataFromBuffer(byte_t* buffer, int length);

        ///@brief Move a message from the receive queue to a buffer
        ///@return struct with number of copyed bytes and infos on message source
        messageInfo moveMessageToBuffer (byte_t* buffer,int length);

        ///@brief Move a message from a buffer to the receive queue
        void moveMessageFromBuffer(byte_t* buffer, int length, socket_properties_t sp);

        ///@brief Return true if the receive queue contains data
        bool isRecvQueueEmpty();

        ///@name getters-setters
        //@{
        socketFD_t getFD() const;

        void setSourceIp(const ip_addr_t &sourceIp);

        const ip_addr_t &getSourceIp() const;

        void setSourcePortNo(port_no_t sourcePortNo);

        port_no_t getSourcePortNo() const;

        void setDestIp(const ip_addr_t &destIp);

        const ip_addr_t &getDestIp() const;

        void setDestPortNo(port_no_t destPortNo);

        port_no_t getDestPortNo() const;

        void setType(open_type type);

        open_type getType() const;

        int getProtocol() const;

        void setBlock(bool blocking);

        bool isBlocking() const;

        void setTaskproxyKey(const std::string &taskproxyKey);

        const std::string &getTaskproxyKey() const;

        void setBound(bool bounded);

        bool isBound() const;

        bool isClosed();

        bool isRClosed() ;

        //set the variable _W_Closed to true
        void setRClosed();

        bool isWClosed();

        //set the variable _W_Closed to true
        void setWClosed();

        void close();

        bool hasSpaceToWrite() const;

        size_t getSpaceToWrite() const;

        void setWhichSet(unsigned short whichSet);

        void setSndBufferSize(size_t dim);

        bool isReuseport() const;

        void setReuseport(bool reuseport);

        bool isReuseaddr() const;

        void setReuseaddr(bool reuseaddr);

        void addMulticastIP(unsigned ip);

        bool hasMulticastMembership(unsigned ip);

        //@}

        ///@brief Return the event to wait on for incoming messages
        const sc_core::sc_event &getIncomeMsg() const;

        ///@brief Return the event to wait on for connection requests
        const sc_core::sc_event &getConnectionRequest() const;

        /// @name Operator overload
        //@{
        Socket_t & operator=(const Socket_t & sp);

        bool operator==(const Socket_t &rhs) const;

        bool operator<(const Socket_t &rhs) const;
        bool operator!=(const Socket_t &rhs) const;
        //@}
    private:

        ///@brief This socket file descriptor
        socketFD_t _fd;

        ///@brief Socket type (TCP/UDP)
        int _sock_type;

        ///@brief The socket source ip
        ip_addr_t _source_ip;

        ///@brief The socket source port
        port_no_t _source_port_no;

        ///@brief The socket destination ip
        ip_addr_t  _dest_ip;

        ///@brief The socket destination port
        port_no_t  _dest_port_no;

        ///@brief true if the port number assigned to the socket can be used again
        bool _reuseport;

        ///@brief true if the ip address assigned to the socket can be used again
        bool _reuseaddr;

        ///@brief The key of the taskproxy related with the socket
        std::string _taskproxy_key;

        ///@brief The buffer containing all received data
        std::vector<byte_t>* _recv_queue;

        ///@brief Queue for storing all informations about messages stored in the 
        /// buffer (size, sources)
        std::queue<messageInfo> _msg_info_queue;

        ///@brief The queue storing all the connection requests
        std::vector<Socket_t*>* _connection_queue;

        ///@brief The send buffer current dimension, updated by the communicator
        size_t _send_buffer_free_space;

        ///@brief True if the socket is marked as blocking
        bool _blocking;

        ///@brief True if the socket has been bound to a specific port/ip
        bool _bound;

        ///@brief The open type
        open_type _type;

        ///@brief The type of set in which the socket is in (for select)
        unsigned short _which_set;

        ///@brief Event to wake a blocked select
        sc_core::sc_event* _select_wake;
        ///@brief Event to wake a blocked receive
        sc_core::sc_event _income_msg;
        ///@brief Event to notify a connection request
        sc_core::sc_event _connection_request;

        ///@brief True if the socket is closed
        bool _closed;

        ///@brief True if socket cannot receive data
        bool _R_closed;

        ///@brief True if the socket annot send data
        bool _W_closed;

        ///@brief list of Multicast IP the socket is subscribed
        std::unordered_set<int> _multicast_subscriptions;
    };
}}}
#endif //SCNSL_API_SOCKET_T_HH
