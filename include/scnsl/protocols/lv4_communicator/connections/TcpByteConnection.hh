#ifndef SCNSL_TCP_BYTE_CONNECTION_HH
#define SCNSL_TCP_BYTE_CONNECTION_HH

#include "../Lv4ProtocolPacket_t.hh"
#include "TcpConnection_if_t.hh"

///@file An implementation of the TcpConnection_if_t class using
///SimpleProtocolPacket to communicate
/// this class create packets by wrapping the buffer from upper level with its
/// own buffer, structured as a tcp header this class internal_accept only
/// SimpleProtocolPacket object as input packets and produce only
/// SimpleProtocolPackets wrapped in ProtocolPackets as output

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

/// @brief A Connection class that use buffer to simulate byte-accurate packet
/// transmission with tcp protocol
class TcpByteConnection: public TcpConnection_if_t
{
public:
    typedef Scnsl::Core::byte_t byte_t;

    ///@brief Constructor
    ///@param connectionId this connection id
    ///@param timeout_queue the queue used to notify the presence of packet
    ///stored in the connection that have
    /// to be received
    ///@param wmem_size the size of the send buffer
    ///@param max_segment_size the maximum size in bytes of the segments to send
    ///@param rcvwnd the maximum number of bytes that can be send at the same
    ///time
    ///@param header_size the total size in bytes of external headers (no tcp
    ///header like ip etc)
    ///@param slow_start the slow start algorithm to use in the connection
    ///@param congestion_avoidance the congestion avoidance algorithm to use in
    ///the connection
    ///@param fast_recovery the fast recovery algorithm to use in the connection
    ///@param nodelay true if the connection must send the data as soon as
    ///possible
    TcpByteConnection(
        counter_t connectionId,
        socket_properties_t socket,
        EventManager_t * timeout_queue);

    ///@brief Default destructor
    ~TcpByteConnection() override;

    ///@name Inherited methods
    //@{
    void addPacketToBuffer(const Packet_t & p) override;

    Packet_t packetFromBuffer() override;

    bool canSend() const override;

    void managePacket(const Packet_t & p) override;

    Packet_t sendAck() override;

    Packet_t createPacketWithFlags(bool syn, bool fin, bool ack) override;

    counter_t getFirstRetransmitPacket() override;

    void printTrace(const Packet_t & p, std::string comm_name, bool isSent)
        override;

    // ovverride method to create new buffer with the specified size
    void setWmemSize(size_t wmem);
    //@}

    ///@brief static methods
    //@{
    ///@brief Return true if the protocol packet contains a Syn flag. Used
    ///before an effective connection object is created
    ///@param p the packet to inspect
    ///@return true if the packet has the syn flag set
    static bool isPacketSyn(ProtocolPacket_if_t * p);

    ///@brief Convert a byte vector into an unsigned short
    static unsigned short int vectToUShort(byte_t * arr, int start_index);

    ///@brief Convert a byte vector into an unsigned integer
    static unsigned int vectToUInt(byte_t * arr, int start_index);

    ///@brief Put an unsigned short into a byte vector starting at start_index
    ///@param value the value to insert
    ///@param arr the array in which insert the value
    ///@param start_index the position where insert the value
    ///@warning The array must have an appropriate size to avoid segmentation
    ///faults
    static void uShortInVect(
        unsigned short value,
        byte_t * arr,
        int start_index);

    ///@brief Put an unsigned integer into a byte vector starting at start_index
    ///@param value the value to insert
    ///@param arr the array in which insert the value
    ///@param start_index the position where insert the value
    ///@warning The array must have an appropriate size to avoid segmentation
    ///faults
    static void uIntInVect(unsigned int value, byte_t * arr, int start_index);
    //@}
private:
    ///@brief Pointer to the beginning of the data buffer
    byte_t * _packet_buffer;

    ///@brief Index of the current position in the data buffer
    size_t _packet_buffer_current_start;

    ///@brief Index of the end position in the data buffer
    size_t _packet_buffer_current_end;

    ///@brief Packet stored but not send to upper levels since they are out of
    ///order
    std::map<counter_t, SimpleProtocolPacket_t *> * _waiting_packets;

    ///@brief Map to store the send time of each packets
    std::map<size_t, sc_core::sc_time> * _transmission_time;

    ///@brief convert a boolean into a single byte
    ///@param x the value to convert
    byte_t boolToByte(bool x);

    ///@brief convert a byte into a boolean
    ///@param x the value to convert
    bool byteToBool(byte_t x);

    ///@brief Support function to print a data buffer
    ///@param buffer the buffer to print
    ///@param size the buffer size
    void printbuffer(byte_t * buffer, unsigned size);
};

}}}  // namespace Scnsl::Protocols::Network_Lv4

#endif  // SCNSL_TCP_ADDON_TCPBYTECONNECTION_HH
