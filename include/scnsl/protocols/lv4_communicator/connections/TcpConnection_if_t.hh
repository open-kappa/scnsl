#ifndef TCP_CONNECTION_HH
#define TCP_CONNECTION_HH

#include "../../../core/Packet_t.hh"
#include "../../../core/ProtocolPacket_if_t.hh"
#include "../../../core/SimpleProtocolPacket_t.hh"
#include "../../../core/SocketMap.hh"
#include "../../../core/Task_if_t.hh"
#include "../EventManager_t.hh"
#include "../Lv4ProtocolConstants.hh"
#include "../Lv4ProtocolPacket_t.hh"
#include "../algorithms/TcpAlgFactory.hh"

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {
class TcpAlg_if_t;
}}}  // namespace Scnsl::Protocols::Network_Lv4

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief An interface to store all the important data for a connection and
///calculate next values to use. The interface allow for different
/// implementations about the type of packets received and how these are managed
/// All the sizes are calculated in bytes
/// Each connection has a unique id

class SCNSL_EXPORT TcpConnection_if_t: public Scnsl::Tracing::Traceable_base_t
{
public:
    typedef Scnsl::Core::Node_t Node_t;
    typedef Scnsl::Core::Channel_if_t Channel;
    typedef Scnsl::Core::Task_if_t Task_if_t;
    typedef Scnsl::Core::Packet_t Packet_t;
    typedef Scnsl::Core::counter_t counter_t;
    typedef Scnsl::Core::ProtocolPacket_if_t ProtocolPacket_if_t;
    typedef Scnsl::Core::SimpleProtocolPacket_t SimpleProtocolPacket_t;
    typedef Scnsl::Core::socket_properties_t socket_properties_t;
    typedef Scnsl::Core::ip_addr_t ip_addr_t;
    typedef Scnsl::Core::port_no_t port_no_t;
    typedef Scnsl::Core::byte_t byte_t;

    ///@brief Generate a unique id each time the function is called
    static unsigned int getNextId();

    ///@brief Constructor
    ///@param connectionId the id for the created connection
    ///@param s the socket this connection represent
    ///@param timeout_queue the event queue of the communicator where submit
    ///events
    TcpConnection_if_t(
        counter_t connectionId,
        socket_properties_t s,
        EventManager_t * timeout_queue);

    ///@brief Destructor
    virtual ~TcpConnection_if_t() = 0;

    ///@brief Extract the buffer from a pakcet and add it to the send buffer
    ///@param p the packet that store the buffer to add
    virtual void addPacketToBuffer(const Packet_t & p) = 0;

    ///@brief Return true if the connection can send a data segment, must be
    ///called before the packetFromBuffer()
    ///@return true if the connection can send a segment
    virtual bool canSend() const = 0;

    ///@brief Update the send buffer, by removing the data and preparing a
    ///packet to send when the condition are met
    ///@return A single packet which contains all the data that can be send at
    ///the moment
    virtual Packet_t packetFromBuffer() = 0;

    ///@brief Process the data from a packet
    ///@param p the packet which store the data to process
    virtual void managePacket(const Packet_t & p) = 0;

    ///@brief Create an ack segment
    ///@return a Packet object containing the ack segment
    virtual Packet_t sendAck() = 0;

    ///@brief Create a segment with the given flags
    ///@param syn true if the segment must have the syn flag set
    ///@param ack true if the segment must have the ack flag set
    ///@param fin true if the segment must have the fin flag set
    ///@return a Packet object with the segment created
    virtual Packet_t createPacketWithFlags(bool syn, bool fin, bool ack) = 0;

    ///@brief Get the first unacknowledged packet number, usable in the
    ///getRetransmitPacket function
    ///@return the acknowledge number of the first unacknowledged packet
    virtual counter_t getFirstRetransmitPacket() = 0;

    ///@brief Print the information of the packet in an understandable format
    ///@param P the packet sent/received
    ///@param comm_name the name of the communicator the connection belongs to
    ///@param isSent true if the packet is sent, false if is received
    virtual void printTrace(
        const Packet_t & p,
        std::string comm_name,
        bool isSent) = 0;

    ///@brief The protocol fsm, update all the connection variables based on the
    ///event occurred
    ///@param timeout_occurred true if a retransmission timeout has expired
    ///@param rtt the round trip time of the last segment received
    ///@param is_last_ack_duplicate true if the last ack was a duplicate
    ///@param acknowledge_bytes the number of acknowledged bytes by the last ack
    ///segment
    void tcp_EFSM(
        bool timeout_occurred,
        sc_core::sc_time rtt,
        bool is_last_ack_duplicate,
        size_t acknowledge_bytes);

    ///@brief Switch the algorithm to slow start;
    void toSlowStart();

    ///@brief Switch the algorithm to congestion avoidance;
    void toCongestionAvoidance();

    ///@brief Switch the algorithm to fast recovery;
    void toFastRecovery();

    ///@brief Update the current connection state based on the received flags
    ///@param syn true if the last segment had the syn flag set
    ///@param ack true if the last segment had the ack flag set
    ///@param fin true if the last segment had the fin flag set
    void updateState(bool syn, bool ack, bool fin, bool send);

    ///@brief Ask the connection if an open request is possible
    ///@return true if a syn pakcet is allowed to be sent
    bool openRequest();

    ///@brief Inform the connect that a internal_close request has been made
    bool closeRequest();

    ///@brief Prepare a packet by calling the packetFromBuffer() method and
    ///notify the api with the new buffer size
    ///@return The Packet created by the packetFromBuffer() funciton
    ///implementation
    Packet_t preparePacket();

    ///@brief Find the segment given his ack number
    ///@param ack_number the ack number relative to the segment
    ///@return The Packet object containing the segment searched
    Packet_t getRetransmitPacket(const counter_t ack_number);

    ///@brief Return true if there is at least one packet in the receive queue
    bool hasPacketToread() const;

    ///@brief Get the first packet in the receive queue
    ///@return the packet at the top of the receive queue
    Packet_t getNextReceivePacket();

    ///@brief return true if the packet with the given ack number has already
    ///been acknowledged
    bool isPacketAcknowledge(const counter_t number) const;

    ///@name Getters and setters
    //@{
    unsigned int getId() const;

    void setId(unsigned int id);

    const sc_core::sc_time & getRto() const;

    bool isSamplePacketRdy() const;

    ///@brief Store a packet as a sample for send, to wrap future segment using
    ///the same parameters
    ///@param p the packet stored as sample
    ///@param reverse true if the sample packet has been received, hence not all
    ///parameter can be set properly
    void setSampleSendPacket(const Packet_t & p, bool reverse);

    bool isSampleReceivePacketRdy() const;

    ///@brief Set a sample packet to receive segment to upper communicator
    void setSampleReceivePacket(const Packet_t & sampleReceivePacket);

    counter_t getNextPacketNumber() const;

    counter_t getLastAckSend() const;

    short getDuplicateAck() const;

    void setDuplicateAck(short duplicateAck);

    bool needSendAck() const;

    bool needImmediateAck() const;

    bool needRetransmission() const;

    void setRetransmission(bool needRetransmission);

    bool needSynAck() const;

    bool needFin() const;

    size_t getMaxSegSize() const;

    void setMaxSegSize(size_t maxSegSize);

    size_t getCwnd() const;

    void setCwnd(size_t cwnd);

    size_t getSstresh() const;

    void setSstresh(size_t sstresh);

    size_t getRcvwnd() const;

    void setRcvwnd(size_t rcvwnd);

    void setCongestionState(Fsm_states congestionState);

    void setSlowStart(TcpAlgs slowStart);

    void setCongestionAvoidance(TcpAlgs congestionAvoidance);

    void setFastRecovery(TcpAlgs fastRecovery);

    void setFullHeaderSize(
        unsigned short ip_header = 0,
        unsigned short mac_byte = 0);

    virtual void setWmemSize(size_t wmemSize);

    size_t getAckSizeCounter() const;

    void setAckSizeCounter(size_t ackSizeCounter);

    size_t getFlightSize() const;

    void setFlightSize(size_t flightSize);

    bool hasPriorityPacket();

    Packet_t getPriorityPacket();

    ///@brief Add a packet in the priority queue, which will be send before any
    ///other segment
    ///@param p the packet to send with high priority
    void addPriorityPacket(Packet_t p);

    void setLimitedTransmissionEnabled(bool limitedTransmissionEnabled);

    void setTcpNoDelay(bool tcpNoDelay);

    connection_states getConnectionState() const;

    ///@brief Get the following sequence number
    ///@return the sequence number relative to the next segment
    counter_t getNextSendPacketAckNumber() const;

    void setFastAckCount(unsigned short fastAckCount);

    void setSendAckAfterFullSegment(bool sendAckAfterFullSegment);

    bool isBlocking() const;

    void setNonBlock(bool blocking);

    void setLog(bool log);

    void setMinRetransmissionTimeout(sc_core::sc_time rto);

    //@}
protected:
    ///@name Class field
    //@{
    ///@brief Id for the future connection
    static unsigned int _next_id;

    ///@brief Unique id for the connection.
    unsigned int _id;

    ///@brief Source port of this connection
    port_no_t _source_port;

    ///@brief Destination port of this connection
    port_no_t _dest_port;

    ///@brief Source ip of this connection
    ip_addr_t _source_ip;

    ///@brief Destination ip of this connection
    ip_addr_t _dest_ip;

    ///@brief State of the connection
    connection_states _connection_state;
    //@}

    ///@name Congestion algorithms
    //@{
    Fsm_states _congestion_state;
    TcpAlg_if_t * _slow_start;
    TcpAlg_if_t * _congestion_avoidance;
    TcpAlg_if_t * _fast_recovery;
    //@}

    ///@namw protocol fields
    //@{
    ///@brief Maximum segment size in byte
    size_t _max_seg_size;

    ///@brief Maximum dimension that can be sent in bytes
    size_t _cwnd;

    ///@brief Number of bytes currently travelling in the network
    size_t _flightSize;

    ///@brief Slow start treshold in bytes
    size_t _sstresh;

    ///@brief Receive window in bytes
    size_t _rcvwnd;

    ///@brief Number of different ack in a row (for congestion avoidance)
    size_t _ack_size_counter;

    ///@brief Smooth round trip time
    sc_core::sc_time _SRTT;

    ///@brief Retrasmission timeout
    sc_core::sc_time _RTO;

    ///@brief Minimum retransmiossion timeout (defautl 1 sec)
    sc_core::sc_time _minRTO;

    ///@brief Round trip time variation
    sc_core::sc_time _RTT_VAR;

    ///@brief True if the rtt is calculated for the first time
    bool _rtt_first_calculation;
    //@}

    ///@name Flags for communication with communicator/algorithms
    //@{

    ///@brief True if waiting for ack of retransmitted packet
    bool _retransmitting;

    ///@brief True if the connection need to send an ack
    bool _need_send_ack;

    ///@brief True if the ack must be send without waiting
    bool _immediate_ack;

    ///@brief True if the connection must retransmit a segment
    bool _need_retransmission;

    ///@brief True if the connection mus send a syn-ack segment
    bool _needSynAck;

    ///@brief True if the connection must send a fin segment
    bool _needFin;

    ///@brief true if a fin packet has been received
    bool _fin_arrived;

    ///@brief true if disconnection request has been made
    bool _close_request;

    ///@brief True if the limited transmission is enabled
    bool _limited_transmission_enabled;

    ///@brief True if the Nagle algorithm is disabled
    bool _tcp_no_delay;
    //@}

    ///@name Utility variables
    //@{
    ///@brief Next waited packet sequence number (i.e the ack number send).
    counter_t _next_packet_waited;

    ///@brief Next sequence number to use
    counter_t _next_sequence_number;

    ///@brief Last  ack number received.
    counter_t _last_ack_received;

    ///@brief Last ack number send
    counter_t _last_ack_send;

    ///@brief Number of duplicate ack
    short int _duplicate_ack;

    ///@brief the number of packet from the start of the connection that will
    ///receive an immediate ack
    /// (0 by default)
    unsigned _fast_ack_count;

    ///@brief number of received packets
    unsigned _received_packets;

    ///@brief true if the number of packet received has surpassed the
    ///_fast_ack_count varaible
    bool _ack_threshold_surpassed;

    ///@brief true if the connection must send an immediate ack after two
    ///full-sized segment. If false the
    /// connection will send an ack after every other segment received
    bool _send_ack_after_full_segment;
    //@}

    ///@name Simulation fields
    //@{
    size_t _full_header_size;

    ///@brief Send buffer current size in bytes
    size_t _wmem_current_size;

    ///@brief max write memory
    size_t _wmem_size;
    //@}

    ///@name Send fields
    //@{
    ///@brief True if sample send packet is correctly initialized
    bool _sample_send_packet_rdy;

    ///@brief Example packet, used to retrieve all the simulation fields when
    ///sending
    Packet_t _sample_send_packet;

    ///@brief True if sample receive packet is correctly initialized
    bool _sample_receive_packet_rdy;

    ///@brief Example packet, used to retrieve all the simulation fields when
    ///receiving
    Packet_t _sample_receive_packet;

    ///@brief Queue for packets with high priority
    std::queue<Packet_t> * _immediate_queue;

    ///@brief True if the connection is non blocking
    bool _noblocking;
    //@}

    ///@name Retrasmission
    //@{
    ///@brieg Packet waiting for possible retransmission
    std::map<size_t, Packet_t> * _retrasmission_map;

    //@}
    ///@name receive fields
    //@{
    ///@brief Queue for packets waiting to be send to upper communicaator
    std::queue<Packet_t> * _receive_buffer;
    //@}

    ///@brief Event queue used to notify the communicator of a packet ready to
    ///be received
    EventManager_t * _event_queue;

    ///@brief Event to notify the presence of free space in the send buffer
    sc_core::sc_event _buffer_space;

    ///@brief True to enable log regarding the tcp variables and packet
    ///transiting (other type of log won't be affected)
    bool _log;

private:
    ///@brief Prepare a commando packet for the upper communicators with the
    ///given buffer as payload
    ///@param command the commando to store in the packet
    ///@param buffer the buffer with information required by the command, can
    ///also be nullptr if command need no
    /// additional data
    ///@param length the length of the buffer passed
    void prepareCommandPacket(
        Scnsl::Core::PacketCommand command,
        byte_t * buffer,
        size_t length);
};

}}}  // namespace Scnsl::Protocols::Network_Lv4
#endif  // !TCP_CONNECTION
