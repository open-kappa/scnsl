#ifndef SCNSL_TCP_CONNECTION_FACTORY_HH
#define SCNSL_TCP_CONNECTION_FACTORY_HH

#include "../../../core/ProtocolPacket_if_t.hh"
#include "../../../core/data_types.hh"
#include "TcpConnection_if_t.hh"

// All connections type  must go here
#include "TcpByteConnection.hh"

///@file A factory to create all types of congestion control algorithms

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief A factory to create all kind of tcp connections object
class TcpConnectionFactory
{
public:
    ///@brief Create a new algorithm class based on the desired algorithm and
    ///binded to the connection
    ///@param type the connection type to create
    ///@param connectionId the id for the created connection
    ///@param s the socket the connection represent
    ///@param timeout_queue the event queue of the communicator where submit
    ///events
    static TcpConnection_if_t * getConnection(
        TcpConns type,
        Scnsl::Core::counter_t connectionId,
        Scnsl::Core::socket_properties_t socket,
        EventManager_t * timeout_queue);

    ///@brief Given a Protocol packet return the value of the syn flag by using
    ///the appropriate method
    ///@param type the connection type that which method will be called
    ///@param p the packet to check
    static bool isSyn(TcpConns type, Scnsl::Core::ProtocolPacket_if_t * p);
};
}}}  // namespace Scnsl::Protocols::Network_Lv4

#endif  // SCNSL_TCP_CONNECTION_FACTORY_HH
