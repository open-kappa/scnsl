
#ifndef SCNSL_TIMEOUT_STRUCT_T_HH
#define SCNSL_TIMEOUT_STRUCT_T_HH

#include "scnsl/core/data_types.hh"


///@file A struct containing all the info for the tcp events

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief The type of event that occur
enum EventType: unsigned
{
    DATA_SEND,
    IMMEDIATE_RETRANSMISSION,
    RETRANSMISSION_TIMEOUT,
    ACK_TIMEOUT,
    DATA_RECEIVE,
    CONNECTION_DELETE
};

///@brief Struct to contain info for a timeout event in tcp
/// By definition a socket is a tuple < ip_address, port, ip_address, port>
struct SCNSL_EXPORT TimeoutStruct_t
{
    ///@brief type of the event
    EventType _type;

    ///@brief connection id the event refers to
    unsigned int _connection_id;

    ///@brief ack number of the packet that generate the event
    Scnsl::Core::counter_t _ack_number;


    ///@brief sequence number of the packet that generate the event
    Scnsl::Core::counter_t _sequence_number;

    ///@brief Timestamp the event should trigger. Used to sort the event's priority
    sc_core::sc_time _timestamp;

    ///@brief Constructor
    ///@param type the event type
    ///@param connectionId the id of the connection that fired the event
    ///@param ackNumber the ack number of the packet this event refers to
    ///@param sequenceNumber the sequence number of the packet this event refers to
    ///@param timestamp the timestamp (absolute) this event must be triggered.
    TimeoutStruct_t(EventType type, unsigned int connectionId, Core::counter_t ackNumber,
                       Core::counter_t sequenceNumber, const sc_core::sc_time &timestamp);

    friend std::ostream &operator<<(std::ostream &os, const TimeoutStruct_t &aStruct);

};

struct SCNSL_EXPORT TimeoutStruct_comparator_t
{
    bool operator()(const TimeoutStruct_t & t1, const TimeoutStruct_t & t2);
};


}}}


#endif
