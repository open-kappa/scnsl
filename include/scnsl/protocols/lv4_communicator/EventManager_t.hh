#ifndef SCNSL_LV_4_EVENT_MANAGER_T_HH
#define SCNSL_LV_4_EVENT_MANAGER_T_HH

#include <queue>
#include "TimeoutStruct_t.hh"

///@file A re-implementation of the systemC event queue that uses the TimeoutStruct_t class for calculate the timeout
/// and to be able to fire multiple events in sequence if one ore more have been missed
namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

class SCNSL_EXPORT EventManager_t
{
    public:
        typedef std::priority_queue<TimeoutStruct_t, std::vector<TimeoutStruct_t>, TimeoutStruct_comparator_t> event_queue_t;

        ///@brief Default constructor
        EventManager_t();

        ///@brief Default destructor
        ~EventManager_t();

        ///@brief Store an event in the queue. The element at the top is te one with the nearest expiration time .
        /// The event is fired at <event_timestamp - current_timestamp>
        ///@param ev the event to store
        void notify(TimeoutStruct_t ev);

        ///@brief Get the first element of the queue
        ///@return The struct containing all the information about the event fired
        TimeoutStruct_t getTop();

        ///@brief Return true if the first element of the queue has already been triggered (and missed)
        bool isTopFired();

        ///@brief Fire the next event currently at the top of the queue. The event is fired at
        /// <event_timestamp - current_timestamp>
        void fireNext();

        ///@brief Return the event on which the process can internal_listen
        sc_core::sc_event& defaultEvent();

        ///@brief Return true if the queue is empty
        bool empty();

    private:
        ///@brief The queue of events to fire
        event_queue_t _events;
        ///@brief The event triggered on timeout
        sc_core::sc_event _default_event;
};
}}}

#endif
