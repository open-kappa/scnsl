#ifndef SCNSL_LV_4_PROTOCOL_CONSTANTS_HH
#define SCNSL_LV_4_PROTOCOL_CONSTANTS_HH
#include "../../core/data_types.hh"
namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

#define TCP_BASIC_HEADER_LENGTH 32
#define TCP_SOURCE_PORT_BYTE 0
#define TCP_DEST_PORT_BYTE 2
#define TCP_SEQ_NUMB_BYTE 4
#define TCP_ACK_NUMB_BYTE 8
#define TCP_LENGTH_BYTE 12
#define TCP_FLAGS_BYTE 13
#define MSS_INITIAL_BOUND_HIGH 2190
#define MSS_INITIAL_BOUND_MEDIUM 1095


#define UDP_HEADER_LENGTH 8
#define UDP_SOURCE_PORT_BYTE 0
#define UDP_DEST_PORT_BYTE 2
#define UDP_LENGTH_BYTE 4
#define UDP_CHECKSUM 6


///@brief The states for the congestion-control fsm
enum Fsm_states: unsigned
{
    SLOW_START,
    CONGESTION_AVOIDANCE,
    FAST_RECOVERY
};

///@brief The implemented algorithms for the congestion control problem
enum TcpAlgs:unsigned
{
    DEFAULT_SLOW_START,
    AIMD_CA,
    RENO
    //OTHER ALGORITHMS HERE ...
};

///@brief Types of connection that can be instantiated
enum TcpConns:unsigned
{
    TCP_BYTE_CONNECTION,
    //OTHER TYPES HERE ...
};

///@brief The states of the connection used in the efsm
enum connection_states:unsigned
{
    NEW,
    CLOSED,
    SYN_RCVD,
    SYN_SENT,
    ESTAB,
    FIN_WAIT_1,
    CLOSE_WAIT,
    FIN_WAIT_2,
    CLOSING
};

///@brief The maximum number of byte an ethernet PDU can use, IP and TCP headers INCLUDED
const unsigned short int MAX_ETH_SEGMENT =1500;

///@brief The standard segment size for a tcp connections
const unsigned short int TCP_DEFAULT_SEGMENT_SIZE = 536;

///@brief The minimum number of bytes composing a tcp header
const unsigned short int TCP_HEADER_MIN_SIZE = 20;

///@brief The maximum number of bytes composing a tcp header
const unsigned short int TCP_HEADER_MAX_SIZE = 60;

///@brief The minimum number of bytes composing an ip header
const unsigned short int IP_HEADER_MIN_SIZE = 20;

///@brief The maximum number of bytes composing an ip header
const unsigned short int IP_HEADER_MAX_SIZE = 24;

///@brief The extra bytes composing the mac header
const unsigned short int MAC_EXTRA_BYTES = 18;

///@brief The maximum value of the receive window from standard
const unsigned int TCP_MAX_RECEIVE_BYTE = 65535;

///@brief The maximum number of byte an ethernet PDU can use, IP and TCP headers INCLUDED
const unsigned short int MAX_UDP_SEGMENT = 65535 - UDP_HEADER_LENGTH - IP_HEADER_MIN_SIZE;

///@brief The send buffer max size
const size_t DEFAULT_WMEM = 4194304;

///@brief The threshold below which a socket is considered ready to write
const size_t NO_WRITE_TRESHOLD = 2048;

///@brief Time a connection must wait before being closed
const sc_core::sc_time CONN_CLOSE_TIME = sc_core::sc_time(60, sc_core::SC_SEC);

///@name parametrs for the retransmission timeout calculation
//@{
const sc_core::sc_time G(100, sc_core::SC_MS);
const double BETA=1.0/4;
const double ALPHA=1.0/8;
const unsigned int K= 4;
const sc_core::sc_time MIN_TIMEOUT_VALUE(1, sc_core::SC_SEC); //minimum timeout for retransmission
const sc_core::sc_time MAX_ACK_TIMEOUT(500, sc_core::SC_MS); //max timout to send an ack;
//@}
}}}
#endif