
#ifndef SCNSL_LV4_SABOTEUR_HH
#define SCNSL_LV4_SABOTEUR_HH

#include "../../core/Communicator_if_t.hh"
#include "../../core/data_types.hh"
#include "../../tracing/Traceable_base_t.hh"
#include "connections/TcpByteConnection.hh"

#include <chrono>
#include <cstdlib>
#include <queue>
#include <random>

/// @file The saboteur class, to test tcp behaviour in presence of network
/// errors for the TcpByteConnection class

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

struct Lv4Saboteur_channel_down_infos
{
    ///@brief time to wait until next channel break
    sc_core::sc_time start_time;

    ///@brief time to wait until channel repairs
    sc_core::sc_time down_time;
};

}}}  // namespace Scnsl::Protocols::Network_Lv4

namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

///@brief A saboteur to test the LV4 communicator
/// A saboteur to simulate error in the tcp protocol.
/// The errors create are:
/// -Packet out of order,
/// -Packet loss
/// -Channel failure
class SCNSL_EXPORT Lv4ByteSaboteur_t:
    public Scnsl::Core::Communicator_if_t,
    public Scnsl::Tracing::Traceable_base_t,
    public sc_core::sc_module
{
public:
    typedef Scnsl::Core::Packet_t Packet_t;

    SC_HAS_PROCESS(Lv4ByteSaboteur_t);

    /// @brief Constructor.
    /// @param name The name of the communicator.
    /// @param error_perc Percentage of error occurring
    /// @param delete_pckt True if the saboteuor should delete incoming packets
    /// @param multiple_downs True if the saboteur should simualte mutliple 
    /// network shutdowns
    /// @param timeouts Queue containing the timestamp at which network shutdowns happen
    /// @param slow_packet True if the saboteur should hold some packets to delay them
    /// @param min_delay Minimum value in millisecond to hold a delayed packet, 
    /// default is 400 ms 
    /// @param max_delay Maximum value in millisecond to hold a delayed packet, 
    /// default is 800 ms
    
    Lv4ByteSaboteur_t(
        sc_core::sc_module_name name,
        const bool slow_packet,
        const bool delete_pckt,
        const double error_perc,
        const bool multiple_downs,
        const std::queue<Lv4Saboteur_channel_down_infos> timeouts,
        unsigned min_delay = 400,
        unsigned max_delay = 800);

    /// @brief Virtual destructor.
    virtual ~Lv4ByteSaboteur_t();

    ///@brief Allow to enable/disable the loss of retransmitted packets
    void avoid_delete_retransmitted_packets(bool val);

    /// @name Communicator interface reimplemented methods.
    //@{
    virtual errorcode_t send(const Packet_t & p) override;

    virtual errorcode_t receive(const Packet_t & p) override;
    //@}

    ///@brief set the minimum delay (in milliseconds) to wait for the packet
    ///delayer
    void setMinDelay(double minDelay);

    ///@brief set the maximum delay (in milliseconds) to wait for the packet
    ///delayer
    void setMaxDelay(double maxDelay);

private:
    ///@brief number assigned to the slow-down effect
    short int _slow_down_packet_value;

    ///@brief minimum delay value in milliseconds
    unsigned _min_delay;

    ///@brief maximum delay value in milliseconds
    unsigned _max_delay;

    ///@@brief number assigned to the delete-packet effect
    short int _delete_packet_value;

    ///@brief number of possible problems that can occur
    short int _operation_number;

    ///@brief Probability percentage of an error occurring
    const double _error_perc;

    ///@name Random generator variables
    //@{
    std::default_random_engine _gen;

    std::uniform_real_distribution<double> _dist;
    //@}

    ///@brief True to simulate multiple channel-down scenarios
    const bool _multiple_downs;

    ///@brief List of period in which the channel goes down
    std::queue<Lv4Saboteur_channel_down_infos> _timeouts;

    ///@brief True if the channel is currently down
    bool _channel_down;

    ///@brief Event for the deactivation of the channel
    sc_core::sc_event_queue _go_down;

    ///@brief last sequence number observed
    Scnsl::Core::counter_t _last_seqnumb;

    ///@brief true if the saboteur should not delete retransmitted packets
    ///(default = true)
    bool _avoid_double_delete;

    ///@brief Thread to disable the channel
    void timeout();

    ///@brief Queue of delayed packet waiting to be received
    std::queue<Packet_t> _delayed_packets;

    ///@brief Event to trigger delayed packet deliver
    sc_core::sc_event_queue _receive_delayed_packet;

    ///@brief Thread to deliver delayed packets
    void delayPacket();

    /// @brief Disabled copy constructor.
    Lv4ByteSaboteur_t(const Lv4ByteSaboteur_t &);

    /// @brief Disabled assignement operator.
    Lv4ByteSaboteur_t & operator=(const Lv4ByteSaboteur_t &);
};

}}}  // namespace Scnsl::Protocols::Network_Lv4
#endif

