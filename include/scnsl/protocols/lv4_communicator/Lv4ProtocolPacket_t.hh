// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.

#ifndef SCNSL_LV_4_PROTOCOL_PACKET_HH
#define SCNSL_LV_4_PROTOCOL_PACKET_HH

///@file Protocol packet used for lv4 communication

#include "../../core/ProtocolPacket_if_t.hh"
#include "../../core/Node_t.hh"


namespace Scnsl { namespace Protocols { namespace Network_Lv4 {

enum protocol_type:ushort
{
    TCP,
    UDP
};

/// @brief Packet used for level 4 intercommunication,
/// store inside another protocol packet as  well as the information on the 
/// protocol used (TCP/UDP)
class SCNSL_EXPORT LV4ProtocolPacket_t: public Scnsl::Core::ProtocolPacket_if_t
{
public:
    typedef Scnsl::Core::Node_t Node_t;

    ///@brief Default constructor
    LV4ProtocolPacket_t();

    ///@brief copy constructor
    LV4ProtocolPacket_t(const LV4ProtocolPacket_t & p);

    ///@brief Default destructor
    ~LV4ProtocolPacket_t() override;

    ///@name Inherited method from the base class
    //@{

    size_t getPayloadSize() const override;

    void setPayload(const ProtocolPacket_if_t * packet) override;

    ProtocolPacket_if_t * getPayload() const override;

    byte_t * getInnerBuffer() const override;

    size_t getInnerSize() const override;

    LV4ProtocolPacket_t * clone() const override;

    bool operator==(const ProtocolPacket_if_t & p) const override;

    //@}

    /// @brief Assigment operator.
    LV4ProtocolPacket_t & operator=(const LV4ProtocolPacket_t & p);

    /// @brief Sets the protocol type of the packet.
    /// @param protocol The protocol used to send of the packet.
    void setProtocol(protocol_type protocol);

    /// @brief Get the protocol type.
    protocol_type  getProtocolType() const ;

private:
    /// @name Payload fields.
    //@{

    /// @brief The internal buffer.
    const ProtocolPacket_if_t * _payload;

    ///@brief Final destination of the payload
    protocol_type _type; 

    //@}
};
}}}
#endif
