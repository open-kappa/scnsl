// Copyright (C) 2008-2019
// by D. Quaglia and F. Stefanni.

// This file is part of SCNSL.

// SCNSL is free software:
// you can redistribute it and/or modify it under the terms of the
// GNU Lesser General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option)
// any later version.

// SCNSL is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with SCNSL,
// in a file named LICENSE.txt.
// If not, see <http://www.gnu.org/licenses/>.


#ifndef SCNSL_PROTOCOLS_ROUTERCOMMUNICATOR_T_HH
#define SCNSL_PROTOCOLS_ROUTERCOMMUNICATOR_T_HH



/// @file
/// The routing protocol communicator..

#include "../../scnslConfig.hh"
#include "../../core/data_types.hh"
#include "../../core/Communicator_if_t.hh"
#include "../../core/Node_t.hh"
#include "../../core/Packet_t.hh"


namespace Scnsl { namespace Protocols { namespace RouterCommunicator {

      class SCNSL_EXPORT RouterCommunicator_t:
        public Scnsl::Core::Communicator_if_t
      {
      public:

          /// @name Traits.
          //@{

          /// @brief Routing table.
          typedef Scnsl::Core::RoutingTable_t RoutingTable_t;

          //@}


          /// @brief Constructor.
          ///
          /// @param routingTable The associated routing table.
          /// @param rn The associated node.
          ///
          RouterCommunicator_t( const RoutingTable_t & routingTable,
                                Scnsl::Core::Node_t * rn );

          /// @brief Virtual destructor.
          virtual
          ~RouterCommunicator_t();


          /// @name Communicator interface reimplemented methods.
          //@{

          virtual
          errorcode_t send( const Scnsl::Core::Packet_t & p ) override;

          virtual
          errorcode_t receive( const Scnsl::Core::Packet_t & p ) override;

          //@}


      protected:

          /// @brief The current node
          Scnsl::Core::Node_t * _routerNode;

          /// @brief The routing table
          RoutingTable_t _routingTable;

          /// @brief Flag to check if it is already sending.
          bool _is_sending;

      private :

          /// @brief Disabled copy constructor.
          RouterCommunicator_t( const RouterCommunicator_t & );

          /// @brief Disabled assignement operator.
          RouterCommunicator_t & operator = ( const RouterCommunicator_t & );


      };

    } } }

#endif
