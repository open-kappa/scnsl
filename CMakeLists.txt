cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)

project(scnsl
    VERSION 2.0.1.0
    LANGUAGES CXX
    )


string(REPLACE " " "_"  BUILD_GENERATOR "${CMAKE_GENERATOR}")
string(REPLACE " " "_"  BUILD_PLATFORM "${CMAKE_GENERATOR_PLATFORM}")

set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    "${PROJECT_SOURCE_DIR}/../../mycmake/repo/cmake"
    "${PROJECT_SOURCE_DIR}/cmake"
    "${PROJECT_BINARY_DIR}/../../../kappa-projects/third_parties/systemc/build-${BUILD_GENERATOR}-${BUILD_PLATFORM}/cmake"
    "${PROJECT_BINARY_DIR}/../../../kappa-projects/third_parties/systemc/build-${BUILD_GENERATOR}/cmake"
    "${PROJECT_BINARY_DIR}/../../../kappa-projects/third_parties/systemc/build"
    )

option(MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY "Force default visibilityfor executables" ON)
mark_as_advanced(MYCMAKE_TARGETS_EXE_DEFAULT_VISIBILITY)

macro(scnsl_add_doxygen_doc)
    set(DOXYGEN_PROJECT_NAME "SCNSL")
    set(DOXYGEN_HTML_HEADER "doc/headerFile.html")
    #set(DOXYGEN_HTML_EXTRA_FILES "doc/doxygen.ico")
    mycmake_doxygen_add_generic_target(doxygen
        include
        doc/manual
        ${ARGN}
        )
endmacro(scnsl_add_doxygen_doc)

if (SCNSL_DOC_ONLY)
    find_package(MyCMakeDoxygen REQUIRED)
    scnsl_add_doxygen_doc(NO_DOC)
    return()
endif (SCNSL_DOC_ONLY)


find_package(MyCMakeTargets REQUIRED)
find_package(MyCMakeCompiler REQUIRED)
find_package(MyCMakeSystemC REQUIRED)
find_package(MyCMakeDoxygen REQUIRED)
find_package(MyCMakeCTest REQUIRED)

include (CMakeLists.deps.txt)

mycmake_add_additional_files(
    .gitignore
    .gitlab-ci.yml
    README.md
    doc/headerFile.html
    doc/manual/MainPage.md
    doc/manual/howtos.md
    doc/manual/other.md
    doc/manual/license.md
    doc/manual/copyright.md
    doc/manual/howtos/howto-install.md
    doc/manual/howtos/howto-write-main.md
    doc/manual/howtos/howto-contributing.md
    doc/manual/other/tests-description.md
    )

mycmake_configure_project(
    INSTALL_PATH "${PROJECT_BINARY_DIR}/${PROJECT_NAME}"
    BUILD_TYPE "Debug"
    )

mycmake_add_library(${PROJECT_NAME}
    SHARED STATIC
    ${SCNSL_SRCS}
    )

mycmake_target_link_libraries(${PROJECT_NAME}
    PUBLIC
        SystemC::systemc
    )
mycmake_target_include_directories(${PROJECT_NAME})

mycmake_target_compile_definitions(${PROJECT_NAME}
    PRIVATE
        -D_CRT_SECURE_NO_WARNINGS
    )

scnsl_add_doxygen_doc()

mycmake_install(${PROJECT_NAME})

add_subdirectory(tests)

mycmake_export("dev")

# EOF
